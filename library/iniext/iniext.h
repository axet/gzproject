// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/// @file
/// @author Alexey Kuznetsov (axet@nm.ru)

/// Содержит в себе набор функций для работы с Ini файлами.

/// так же набор фукнций для получения путей и работы с путями.
/// библиотека функций работы с файлами настроек и путями.
namespace IniExt
{
  ;
};

#ifndef __INIEXT_H__
#define __INIEXT_H__

#include "ini.h"
#include "path.h"
#include "splitpath.h"
#include "config.h"
#include "logger.h"

#ifdef _DEBUG
#pragma comment(lib,"iniext.lib")
#else
#pragma comment(lib,"iniext.lib")
#endif

using namespace IniExt;

#endif
