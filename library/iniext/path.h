// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#pragma once

#include <string>

namespace IniExt
{
  /// получает дирректорию до модуля
  /// возвращает его с завершающим слешем
  std::string GetModuleRunDir(void* h = 0);
  std::string GetModuleFileName(void* h = 0);
  /// имя.exe
  std::string GetModuleFullName(void* h = 0);
  /// имя
  std::string GetModuleName(void* h = 0);
  /// c:\\windows\\system32
  std::string GetSystemDirectory();
  /// получает временный путь
  std::string GetTempFilePath();
  /// удаляет рекурсивно дирректорию
  void RemoveDirectoryTree(const char* path);
  /// созадает дирректории по пути
  void CreateDirectoryTree(const char* path);
}
