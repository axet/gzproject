// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <windows.h>
#include <direct.h>
#include <io.h>

#include "path.h"
#include "splitpath.h"

std::string IniExt::GetModuleRunDir(void* h)
{
  char buf[MAX_PATH];
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char fname[_MAX_FNAME];
  char ext[_MAX_EXT];
  ::GetModuleFileName((HMODULE)h,buf,sizeof(buf));
  _splitpath(buf,drive,dir,fname,ext);
  _makepath(buf,drive,dir,0,0);
  return std::string(buf);
}

std::string IniExt::GetModuleFileName(void* h)
{
  char buf[MAX_PATH];
  ::GetModuleFileName((HMODULE)h,buf,sizeof(buf));
  return std::string(buf);
}

std::string IniExt::GetModuleFullName(void* h)
{
  char buf[MAX_PATH];
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char fname[_MAX_FNAME];
  char ext[_MAX_EXT];
  ::GetModuleFileName((HMODULE)h,buf,sizeof(buf));
  _splitpath(buf,drive,dir,fname,ext);
  _makepath(buf,0,0,fname,ext);
  return std::string(buf);
}

std::string IniExt::GetModuleName(void* h)
{
  char buf[MAX_PATH];
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char fname[_MAX_FNAME];
  char ext[_MAX_EXT];
  ::GetModuleFileName((HMODULE)h,buf,sizeof(buf));
  _splitpath(buf,drive,dir,fname,ext);
  _makepath(buf,0,0,fname,0);
  return std::string(buf);
}

std::string IniExt::GetTempFilePath()
{
  char buf[MAX_PATH];
  GetTempPath(sizeof(buf),buf);
  GetTempFileName(buf,"fafw",FALSE,buf);
  remove(buf);
  return buf;
}

std::string IniExt::GetSystemDirectory()
{
  char buf[MAX_PATH];
  ::GetSystemDirectory(buf,sizeof(buf));
  return buf;
}

void IniExt::RemoveDirectoryTree(const char* path)
{
  if(path==0||strlen(path)==0)
    return;

  std::string what(path);
  _finddata_t fd;
  long l=_findfirst((what+"\\*.*").c_str(),&fd);
  if(l!=-1)
  {
    do
    {
      if(strcmp(fd.name,".")!=0 && strcmp(fd.name,"..") !=0)
      {
        if((fd.attrib&_A_SUBDIR)==_A_SUBDIR)
          RemoveDirectoryTree((what+"\\"+fd.name).c_str());
        remove((what+"\\"+fd.name).c_str());
      }
    }while(_findnext(l,&fd)==0);
  }
  _findclose(l);
  rmdir(path);
}

void IniExt::CreateDirectoryTree(const char* path)
{
  SplitPath pt(path);
  if(pt.size()==0)
    return;

  for(SplitPath::iterator i=pt.begin()+1;i!=pt.end();i++)
  {
    CreateDirectory(SplitPath::GetPath(pt.begin(),i+1).c_str(),0);
  };
}
