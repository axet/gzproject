// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <vector>
#include <string>

namespace IniExt
{
  class SplitPath;
}

/// класс работы с путями. позволят разбить его на составляющие

class IniExt::SplitPath:public std::vector<std::string>
{
  bool isend(const char c);
public:
  SplitPath();
  SplitPath(const char* folder_path);

  /// cursor - default == end
  void Add(const char*,iterator cursor);
  void operator +=(const char*);
  SplitPath operator +(const char*);
  /// путь абсолютный?
  bool IsAbsolute();
  static std::string GetPath(const_iterator b,const_iterator e);
  std::string GetPath() const;
  /// возвращает имя файла
  std::string GetFileName();

  const char* operator =(const char*);
  operator std::string();
};
