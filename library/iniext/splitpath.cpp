// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "splitpath.h"

bool IniExt::SplitPath::isend(const char c)
{
  return c==0||c=='\\'||c=='/';
}

IniExt::SplitPath::SplitPath()
{
  ;
}

IniExt::SplitPath::SplitPath(const char* folder_path)
{
  *this=folder_path;
}

const char* IniExt::SplitPath::operator =(const char* folder_path)
{
  clear();

  while(1)
  {
    std::string str;
    while(!isend(*folder_path))
      str+=*folder_path++;

    if(!str.empty())
      push_back(str);

    if(!*folder_path)
      break;
    folder_path++;
  }
  return folder_path;
}

std::string IniExt::SplitPath::GetPath() const
{
  return GetPath(SplitPath::begin(),SplitPath::end());
}

std::string IniExt::SplitPath::GetPath(const_iterator b,const_iterator e)
{
  std::string str;

  const_iterator beforeend=e-1;

  for(const_iterator i=b;i!=e;i++)
  {
    str+=*i;
    if(i!=beforeend)
    {
      str+="\\";
    }
  }

  return str;
}

void IniExt::SplitPath::Add(const char*p, iterator cursor/* =0 */)
{
  SplitPath path(p);
  for(iterator i=path.begin();i!=path.end();i++)
  {
    cursor=insert(cursor,*i)+1;
  }
}

IniExt::SplitPath IniExt::SplitPath::operator +(const char* p)
{
  SplitPath sp;
  sp.assign(begin(),end());
  sp.Add(p,end());
  return sp;
}

void IniExt::SplitPath::operator +=(const char* p)
{
  Add(p,end());
}

IniExt::SplitPath::operator std::string()
{
  return GetPath();
}

std::string IniExt::SplitPath::GetFileName()
{
  return back();
}
