// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef __GZUNITLOG_H__
#define __GZUNITLOG_H__

#include <fstream>

/// класс для сложной журнализициии, с дополнительным именованием файла

class CUnitLog
{
  std::ofstream m_log;
  /// посделнее сосотяние журнала (включен\выключен)
  /// нужно чтобы в файле привльно добавлялись надписи Начало записи Конец записи
  bool m_laststate;
  /// показывать ли в логе надписи ЛОгЗапущен, лог остановлен
  bool m_statemessage;
  std::string m_prefix;

public:
  CUnitLog(bool statemessage = true);
  ~CUnitLog();

  /// fileprefix_firstprefix@secondprefix.log. 
  /// fileprefix may contain full path
  void SetLogFileName(const char * fileprefix= 0,const char* firstprefix = 0,const char* secondprefix = 0);
  void SetLogState(bool);
  bool GetLogState();
  void Message(const char* p,...);
  const char* GetLogPath();
};

#endif
