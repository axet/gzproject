// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <fstream>

namespace IniExt
{
  class Logger;
}

/// гласс журнала

class IniExt::Logger
{
  std::ofstream m_file;
  std::string m_path;

public:
  Logger();
  /// открыть журнал по указанному пути
  void Open(const char* path);
  /// записать сообщение , фукнция в стиле sprintf()
  void WriteMessage(const char*,...);
  /// получить путь к журналу
  const char* GetLogPath() const;
};
