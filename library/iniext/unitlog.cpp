// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "stdafx.h"
#include "unitlog.h"
#include <string>
#include <assert.h>
#include <time.h>
#include <stdarg.h>
#include "path.h"

bool CUnitLog::GetLogState()
{
  return m_laststate;
}

void CUnitLog::SetLogState(bool b)
{
  if(m_laststate==b)
    return;
  m_laststate=b;

  if(b)
  {
    m_log.open(m_prefix.c_str(),std::ios_base::app);
    if(!m_log.is_open())
      throw std::exception(("can't open '"+m_prefix+"'").c_str());
    m_log.clear();

    if(m_statemessage)
    {
      char tmpbuf[128];
      _strdate( tmpbuf );
      m_log<<tmpbuf<<" ";
      Message("// log started {\n");
    }
    assert(m_log.good());
  }else
  {
    if(m_statemessage)
    {
      char tmpbuf[128];
      _strdate( tmpbuf );
      m_log<<tmpbuf<<" ";
      Message("// } log ended\n");
    }
    m_log.close();
    m_log.clear();
  }
}

CUnitLog::~CUnitLog()
{
  SetLogState(false);
}

CUnitLog::CUnitLog(bool statemessage):m_laststate(false),m_statemessage(statemessage)
{
}

void CUnitLog::SetLogFileName(const char * fileprefix,const char* firstprefix ,const char* secondprefix)
{
  std::string pref;
  if(fileprefix)
  {
    pref=fileprefix;
  }
  if(firstprefix)
  {
    if(!pref.empty())
      pref+="_";
    pref+=firstprefix;
  }
  if(secondprefix)
  {
    if(!pref.empty())
      pref+="@";
    pref+=secondprefix;
  }
  pref+=".log";

  if(m_prefix!=pref)
  {
    if(GetLogState())
    {
      SetLogState(false);
      m_prefix=pref;
      SetLogState(true);
    }else
      m_prefix=pref;
  }
}

void CUnitLog::Message(const char* p,...)
{
  m_log.clear();

  char printbuf[16384];
  char tmpbuf[128];
  _strtime( tmpbuf );
  m_log<<tmpbuf<<" ";
  va_list args;
  va_start(args,p);
  _vsnprintf(printbuf,sizeof(printbuf),p,args);
  va_end(args);
  m_log<<printbuf;//<<std::endl;
}

const char* CUnitLog::GetLogPath()
{
  return m_prefix.c_str();
}
