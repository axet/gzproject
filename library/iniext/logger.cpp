// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "Logger.h"

#include <CSTDARG>

IniExt::Logger::Logger()
{
}

void IniExt::Logger::Open(const char* path)
{
  m_path=path;
  m_file.open(path);
}

void IniExt::Logger::WriteMessage(const char* p,...)
{
  char buf[32768];
  va_list args;
  va_start(p,args);
  _vsnprintf(buf,sizeof(buf),p,args);
  va_end(args);
  m_file<<buf;
}

const char* IniExt::Logger::GetLogPath() const
{
  return m_path.c_str();
}
