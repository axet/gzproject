// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "stdafx.h"
#include "shellnotifyicon.h"

const UINT ShellNofityIcon::WM_TASKBARCREATED=::RegisterWindowMessage(_T("TaskbarCreated"));

void ShellNofityIcon::ShowIcon(const Icons& ii)
{
  HideIcon(ii);

  for(Icons::const_iterator i=ii.begin();i!=ii.end();i++)
  {
    NOTIFYICONDATA nid=*i;
    nid.uID=std::distance(ii.begin(),i);
    Shell_NotifyIcon(NIM_ADD,&nid);
  }
}

void ShellNofityIcon::HideIcon(const Icons& ii)
{
  for(Icons::const_iterator i=ii.begin();i!=ii.end();i++)
  {
    NOTIFYICONDATA nid={0};
    nid.hWnd=i->mainwnd;
    nid.cbSize=sizeof(nid);
    nid.uID=std::distance(ii.begin(),i);
    Shell_NotifyIcon(NIM_DELETE,&nid);
  }
}

void ShellNofityIcon::UpdateIcon(const Icons& ii)
{
  for(Icons::const_iterator i=ii.begin();i!=ii.end();i++)
  {
    NOTIFYICONDATA nid=*i;
    nid.uID=std::distance(ii.begin(),i);
    Shell_NotifyIcon(NIM_MODIFY,&nid);
  }
}

LRESULT ShellNofityIcon::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
{
  OnShellNofityIconCreate();
  OnShellNofityIconShowIcons();

  return 0;
}

LRESULT ShellNofityIcon::OnTaskBarCreated(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
{
  OnShellNofityIconShowIcons();

  return 0;
}

LRESULT ShellNofityIcon::OnIconMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
{
  OnShellNofityIconProcessMessage(wParam,lParam);

  return 0;
}

ShellNofityIcon::ShellNofityIcon()
{
}

void ShellNofityIcon::ShellNofityIconCreate()
{
  Create(GetDesktopWindow(),rcDefault,0,WS_OVERLAPPEDWINDOW);
}

ShellNofityIcon::~ShellNofityIcon()
{
}

void ShellNofityIcon::ShellNofityIconDestroy()
{
  if(IsWindow())
  {
    DestroyWindow();
  }
}

LRESULT ShellNofityIcon::OnDestory(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
{
  OnShellNofityIconDestroy();

  return 0;
}
