// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <vector>

#include "../include/atl.h"

#ifdef _DEBUG
#pragma comment(lib,"shellnotifyicon.lib")
#else
#pragma comment(lib,"shellnotifyicon.lib")
#endif

class ShellNofityIcon:public CWindowImpl<ShellNofityIcon>
{
public:
  // общие методы работы

  struct Icon
  {
    HICON hicon;
    std::string text;
    HWND mainwnd;
    UINT messagecallback;
    std::string info;

    Icon():hicon(0),mainwnd(0),messagecallback(0) {};
    Icon(HINSTANCE h,UINT resid,const char* _text,HWND _mainwnd,UINT _messagecallback)
    {
      hicon=LoadIcon(h,MAKEINTRESOURCE(resid));
      text=_text;
      mainwnd=_mainwnd;
      messagecallback=_messagecallback;
    }

    Icon(HINSTANCE h,const char* resid,const char* _text,HWND _mainwnd,UINT _messagecallback)
    {
      hicon=LoadIcon(h,resid);
      text=_text;
      mainwnd=_mainwnd;
      messagecallback=_messagecallback;
    }

    operator NOTIFYICONDATA() const
    {
      NOTIFYICONDATA nid={0};
      nid.hWnd=mainwnd;
      nid.cbSize=sizeof(nid);
      nid.uCallbackMessage=messagecallback;
      nid.hIcon=hicon;
      nid.uFlags=NIF_ICON|NIF_MESSAGE|NIF_TIP;
#if(_WIN32_IE >= 0x600)
      nid.uFlags|=(info.empty()?0:NIF_INFO);
      strncpy(nid.szInfo,info.c_str(),sizeof(nid.szInfo));
      strncpy(nid.szInfoTitle,text.c_str(),sizeof(nid.szInfoTitle));
      strncpy(nid.szTip,text.c_str(),sizeof(nid.szTip));
#else
      std::string str=text;
      if(!info.empty())
      {
        str+=" (";
        str+=info;
        str+=")";
      }
      strncpy(nid.szTip,str.c_str(),sizeof(nid.szTip));
#endif
      return nid;
    }


    ~Icon()
    {
      DestroyIcon(hicon);
    }
  };
  typedef std::vector<Icon> Icons;

  static void ShowIcon(const Icons&);
  static void HideIcon(const Icons&);
  static void UpdateIcon(const Icons&);

  // внутренние события

  virtual void OnShellNofityIconCreate() {};
  virtual void OnShellNofityIconDestroy() {};
  virtual void OnShellNofityIconShowIcons() {};
  virtual void OnShellNofityIconProcessMessage(int iconnumber, UINT msg) {};

  ShellNofityIcon();
  ~ShellNofityIcon();
  void ShellNofityIconCreate();
  void ShellNofityIconDestroy();

protected:
  // обработчик сообщений

  static const UINT WM_TASKBARCREATED;

  BEGIN_MSG_MAP(ShellNofityIcon)
    MESSAGE_HANDLER(WM_CREATE,OnCreate)
    MESSAGE_HANDLER(WM_DESTROY,OnDestory)
    MESSAGE_HANDLER(WM_TASKBARCREATED,OnTaskBarCreated)
    MESSAGE_HANDLER(WM_USER+1,OnIconMessage)
  END_MSG_MAP();

  LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled);
  LRESULT OnDestory(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled);
  LRESULT OnTaskBarCreated(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled);
  LRESULT OnIconMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled);
};
