// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/// @file
/// @author Alexey Kuznetsov (axet@nm.ru)

#ifndef __CONTROLWINDOW_H__
#define __CONTROLWINDOW_H__

#include <atlbase.h>
#include <atlapp.h>
#include <comdef.h>
#include <iofluke/memmngr.h>

#ifdef _DEBUG
#pragma comment(lib,"controlwindow.lib")
#else
#pragma comment(lib,"controlwindow.lib")
#endif

/// Модуль перехвата создания окна.

/// Нужен для перехвата окна созданного другим приложением.
/// Такая необходимость возникает когда я хочу изменить заголовок окна контролируемого приложения.
/// Код должен исполняться на стороне контролируемого приложения.
namespace ControlWindow
{
  class CControlWindow;
}

/// Основной класс перехвата

class ControlWindow::CControlWindow:
  public CWindowImpl<ControlWindow::CControlWindow>,
  public IOFluke::CMemMngr
{
  /// текст который нужно добавить к заголовку онка.
  _bstr_t m_gzone;
  _bstr_t m_old;
  /// окно поддерживает уникод?
  BOOL m_unicode;

  BEGIN_MSG_MAP(ControlWindow)
    MESSAGE_HANDLER(WM_SETTEXT, OnSetText)
    MESSAGE_HANDLER(WM_CLOSE, OnClose)
    MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
  END_MSG_MAP()

  static void OnCreateWindowEx(CMemMngr*,int *retval);

public:
  /// конструктор подсоеденяеться к основному окну приложения.
  
  /// если конструктор не находит окна (оно еще не создано) то
  /// произойдет поиск и перехват фукнции которая его создаст в будующем
  CControlWindow();
  /// служит для отключения от окна к которому было подключено. и восстановлении заголовка
  virtual ~CControlWindow();

  /// вызываеться вворемя закрытия окна.
  LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled);
  LRESULT OnSetText(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled);
protected:
  /// подсоедениться к указанному хендлу
  void AttachControl(HWND h);
  /// перехватывает все сообщения контролируемог окна.
  
public:
  /// до уничтожения окна необходимо отсоедениться от него.
  LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled);
};

#endif
