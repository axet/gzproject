// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "stdafx.h"
#include "controlwindow.h"
#include "debugger/debug.h"

#include <comdef.h>

BOOL CALLBACK EnumWindowsProc(HWND hwnd,LPARAM lParam)
{
  DWORD pid;
  GetWindowThreadProcessId(hwnd,&pid);
  if(pid==GetCurrentProcessId())
  {
    char buf[1024];
    GetClassName(hwnd,buf,sizeof(buf));
    if(strcmp(buf,"ConsoleWindowClass")==0)
      return TRUE;

    *(HWND*)lParam=hwnd;
    return FALSE;
  }

  return TRUE;
}

LRESULT ControlWindow::CControlWindow::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
{
  return 0;
}

ControlWindow::CControlWindow::CControlWindow():m_gzone("GZone: ")
{
  if(m_hWnd==0)
  {
    HWND h=0;
    EnumWindows(EnumWindowsProc,(LPARAM)&h);

    if(h==0)
    {
      // Окно не найдено, возможно оно еще не создано процессом
      // необходимо дождаться его сздания. для этого я ловлю CreateWindowex
      DBGTRACE("No window found, catching...\n");

      CatchImportFunction("CreateWindowExA","user32.dll",0,OnCreateWindowEx);
      CatchImportFunction("CreateWindowExW","user32.dll",0,OnCreateWindowEx);
    }else
      AttachControl(h);
  }
}

ControlWindow::CControlWindow::~CControlWindow()
{
  if(m_hWnd==0)
    return;

  CWindow wnd;
  wnd.Attach(UnsubclassWindow());

  _bstr_t str;
  wnd.GetWindowText(str.GetBSTR());
  if(strncmp(m_gzone,str,m_gzone.length())==0)
  {
    wnd.SetWindowText(m_old);
  }

  wnd.Detach();
  DBGTRACE("control window detached:%08x\n",m_hWnd);
}

void ControlWindow::CControlWindow::OnCreateWindowEx(CMemMngr*_m,int *retval)
{
  ControlWindow::CControlWindow* current=dynamic_cast<ControlWindow::CControlWindow*>(_m);

  current->AttachControl(*(HWND*)retval);
}

void ControlWindow::CControlWindow::AttachControl(HWND h)
{
  m_unicode=::IsWindowUnicode(h);

  SubclassWindow(h);

  if(m_unicode)
  {
    OLECHAR text[1024];
    GetWindowTextW(h,text,sizeof(text));
    SetWindowTextW(h,text);
  }else
  {
    _bstr_t str;
    GetWindowText(str.GetBSTR());
    SetWindowText(str);
  }

  DBGTRACE("control window attached:%08x\n",m_hWnd);
}

LRESULT ControlWindow::CControlWindow::OnSetText(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
{
  if(m_unicode)
  {
    _bstr_t current=_bstr_t((BSTR)lParam);
    if(strstr((const char*)current,m_gzone)!=0)
    {
      SetWindowTextW(m_hWnd,(wchar_t*)_bstr_t(m_gzone+current));
    }
  }else
  {
    _bstr_t current=(const char*)lParam;
    if(strstr((const char*)current,m_gzone)!=0)
    {
      SetWindowText(m_gzone+current);
    }
  }

  return 0;
}

LRESULT ControlWindow::CControlWindow::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
{
  DBGTRACE("control window destroy:%08x\n",m_hWnd);

  delete this;

  return 0;
}
