// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/// @file
/// @author Alexey Kuznetsov (axet@nm.ru)


#ifndef __ERRORREPORT_H__
#define __ERRORREPORT_H__

#include <COMDEF.H>
#include <string>

#ifdef _DEBUG
#pragma comment(lib,"ErrorReport.lib")
#else
#pragma comment(lib,"ErrorReport.lib")
#endif

#ifdef WIN32
#include "GetLastError.h"
#endif

/// ErrorReport класс для вывода сообщения Об ошибке.

/// Модуль отладочный для вывода наглядных сообщений об ошибках
/// дейфаны чтобы возвращать в ексепшинах мулти строчки. сообщение наглядно отображаеться
/// показывая информацию на стеке, если необходимо будет использована инкапсуляция текста
/// полученного от предыдущего эксепшена, ниже по стеку. Пример:
/// <PRE>
///System
///{
///  Error on load file.
///}
/// </PRE>

namespace ErrorReports
{
  class Report
  {
    std::string m_str;
    mutable std::string m_temp;

    std::string MakeString(const char* b);
    Report(std::string &str);

  public:
    /// показ ошибки из ресурсов
    Report(unsigned int resourceid);
    /// показ тестовой ошибки
    Report(const char* help);
    /// показ ошибки в стиле "заголовок {тело}"
    Report(const char* help,const char* what);
    /// показ ошибки ком с дополнительной информацией которую можно получить из интерфейса.
    Report(const char* help,_com_error &e);

    /// сложение двух ошибок в одну, произойдет инкапсуляция
    void operator +=(const char* p);
    /// сложение двух ошибок в одну, произойдет инкапсуляция
    void operator +=(const std::string &p);
    /// сложение двух ошибок в одну, произойдет инкапсуляция
    Report operator + (const char*);
    /// получение результирующей текстовой ошибки
    operator const char*() const;
  };
}

typedef ErrorReports::Report ErrorReport;

#endif
