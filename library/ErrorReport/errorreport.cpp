// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <AFX.H>

#include "ErrorReport.h"

#define MLB "\n{"
#define MLL "\n"
#define MLE "\n}"
#define TAB "  "

using namespace ErrorReports;

Report::Report(std::string &str)
{
  m_str=str;
}

Report::Report(const char* help,const char* what)
{
  CString str(help);
  m_str+=str+MLB;
  m_str+=MLL+MakeString(what);
}

std::string Report::MakeString(const char* b)
{
  CString str(b);
  str.Replace("\n","\n  ");
  str="  "+str;
  return (const char*)str;
}

Report::Report(const char* help)
{
  CString str(help);
  m_str+=str+MLB;
}

Report::Report(unsigned int resourceid)
{
  CString str;
  str.LoadString(resourceid);
  m_str=str+MLB;
}

Report::Report(const char* help,_com_error &e)
{
  m_str=std::string(help)+
    MLB+
    MLL+TAB+"ErrorMessage :"+MakeString(e.ErrorMessage())+
    MLL+TAB+"Description :"+MakeString(e.Description())+
    MLL+TAB+"Source :"+MakeString(e.Source())+
    MLL+TAB+"HelpFile :"+MakeString(e.HelpFile());
}

void Report::operator +=(const char* p)
{
  m_str+=MLL+MakeString(p);
}

void Report::operator +=(const std::string &p)
{
  m_str+=MLL+MakeString(p.c_str());
}

Report Report::operator + (const char* p)
{
  Report er(m_str);
  er+=p;
  return er;
}

Report::operator const char*() const
{
  m_temp=m_str+MLE;
  return m_temp.c_str();
}
