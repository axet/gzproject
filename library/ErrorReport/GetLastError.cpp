#include <string>
#include <Windows.h>

#include "GetLastError.h"

std::string GetLastErrorString()
{ 
  return GetLastErrorString(GetLastError()); 
}

std::string GetLastErrorString(DWORD dw)
{ 
  TCHAR szBuf[4096]; 
  LPVOID lpMsgBuf;

  FormatMessage(
    FORMAT_MESSAGE_ALLOCATE_BUFFER | 
    FORMAT_MESSAGE_FROM_SYSTEM,
    NULL,
    dw,
    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
    (LPTSTR) &lpMsgBuf,
    0, NULL );

  wsprintf(szBuf, 
    "%d: %s", 
    dw, lpMsgBuf); 

  LocalFree(lpMsgBuf);
  return std::string(szBuf); 
}
