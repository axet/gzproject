#ifdef _DEBUG
#pragma comment(lib,"likemsdev.lib")
#else
#pragma comment(lib,"likemsdev.lib")
#endif

#include "sizecbar.h"
#include "scbarg.h"

/// Контролка в стиле msdev6.0

/// для работы:
/// необходимо создать класс, для простоты класс визардом
/// взять за основу CWnd.
/// <PRE>
/// #include "../likemsdev/likemsdev.h"
/// 
/// class CMyBar:public CSizingControlBarG
/// 
///   m_dwSCBStyle|=SCBS_SIZECHILD;
/// 
/// 
/// /////////
/// //mainfrm.h
/// 
/// CMainFrame:
/// CMyBar		m_wndMyBar;
/// 
/// ////////
/// //mainfrm.cpp
/// 
/// m_wndMyBars.Create("Frame", this, 123,WS_CHILD|WS_VISIBLE|CBRS_TOP);
/// m_wndMyBars.SetBarStyle(m_wndMyBars.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
/// 
/// EnableDocking(CBRS_ALIGN_ANY);
/// 
/// m_wndMyBars.EnableDocking(CBRS_ALIGN_ANY);
/// DockControlBar(&m_wndMyBars, AFX_IDW_DOCKBAR_RIGHT);
/// </PRE>

namespace LikeMsdev
{
}