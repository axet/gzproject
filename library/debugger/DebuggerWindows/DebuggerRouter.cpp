// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "stdafx.h"
#include "DebuggerRouter.h"

#include <comdef.h>
#include <PROCESS.H>

Debugger::Debugger():m_dr(0)
{
  ;
}

void Debugger::OnFinalMessage(HWND hWnd)
{
  if(m_dr)
    m_dr->DebuggerExit(this);
  Debug::OnFinalMessage(hWnd);
}

bool DebuggerRouter::Empty()
{
  m_cs.Lock();
  bool b=m_debuggerlist.empty();
  m_cs.Unlock();
  return b;
}

DebuggerRouter::DebuggerRouter()
{
  m_newcommand=CreateEvent(0,0,0,0);

  _beginthread((void(*)(void*))Thread,0,this);
}

void DebuggerRouter::Report(long processid,long threadid,const char* msg)
{
  m_cs.Lock();
  m_messagelist.push_back(Message(processid,threadid,msg));
  SetEvent(m_newcommand);
  m_cs.Unlock();
}

void DebuggerRouter::Thread(DebuggerRouter* _this)
{
  while(MsgWaitForMultipleObjects(1,&_this->m_newcommand,FALSE,-1,QS_ALLEVENTS|QS_POSTMESSAGE|QS_SENDMESSAGE)!=-1)
  {
    MSG msg={0};
    while(PeekMessage(&msg,0,0,0,PM_REMOVE)!=0)
      DispatchMessage(&msg);

    {
      _this->m_cs.Lock();
      for(MessageList::iterator imsg=_this->m_messagelist.begin();imsg!=_this->m_messagelist.end();imsg++)
      {
        DebuggerList::iterator i=_this->m_debuggerlist.find(DebuggerItem(imsg->processid,
          imsg->threadid));
        if(i==_this->m_debuggerlist.end())
        {
          i=_this->m_debuggerlist.insert(DebuggerList::value_type(
            DebuggerItem(imsg->processid,imsg->threadid),new Debugger()
            )).first;

          i->second->m_dr=_this;

          _Module.Lock();
        }
        (*i->second)<<imsg->text.c_str();
      }
      _this->m_messagelist.clear();
      _this->m_cs.Unlock();
    }
  }
}

void DebuggerRouter::DebuggerExit(Debugger* p)
{
  m_cs.Lock();
  for(DebuggerList::iterator i=m_debuggerlist.begin();i!=m_debuggerlist.end();i++)
  {
    if(i->second==p)
    {
      m_debuggerlist.erase(i);
      break;
    }
  }
  m_cs.Unlock();

  _Module.Unlock();
}
