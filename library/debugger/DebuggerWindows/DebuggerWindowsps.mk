
DebuggerWindowsps.dll: dlldata.obj DebuggerWindows_p.obj DebuggerWindows_i.obj
	link /dll /out:DebuggerWindowsps.dll /def:DebuggerWindowsps.def /entry:DllMain dlldata.obj DebuggerWindows_p.obj DebuggerWindows_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del DebuggerWindowsps.dll
	@del DebuggerWindowsps.lib
	@del DebuggerWindowsps.exp
	@del dlldata.obj
	@del DebuggerWindows_p.obj
	@del DebuggerWindows_i.obj
