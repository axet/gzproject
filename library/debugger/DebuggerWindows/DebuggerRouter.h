// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/// @file
/// @author Alexey Kuznetsov (axet@nm.ru)


class DebuggerRouter;

#include "DebuggerCommon.h"

#include <map>

class Debugger:public Debug
{
public:
  DebuggerRouter* m_dr;

  Debugger();
  virtual void OnFinalMessage(HWND hWnd);
};

/// Класс сервер, создаеться один экземпляр класса который сам ловит отладчиков клиентов.

/// Класс создат основное окно в котором будет выводиться информация о номере процесса который выводит сообщение
/// в отладочное окно.

class DebuggerRouter
{
  /// подписанный отладчик
  struct DebuggerItem
  {
    /// его поток
    long threadid;
    /// его процесс
    long processid;

    DebuggerItem(long l,long l2):processid(l),threadid(l2)
    {
      ;
    }

    bool operator < (const DebuggerItem & i) const
    {
      if(processid<i.processid)
        return true;
      return threadid<i.threadid;
    }
  };
  /// мап для быстрого дотсупа
  typedef std::map<DebuggerItem,Debugger*> DebuggerList;

  /// сообщение для показа

  struct Message
  {
    /// номер процесса
    long processid;
    /// номер потока
    long threadid;

    /// текст сообщения
    std::string text;

    Message(long l,long ll,const char* a):processid(l),threadid(ll),text(a) {}
  };
  typedef std::vector<Message> MessageList;

  CComAutoCriticalSection m_cs;
  /// список отладчиков которые подписваны на передачу сообщений
  DebuggerList m_debuggerlist;
  /// очередь сообщений для показа.
  MessageList m_messagelist;
  /// обьект синхронизиации потоков отображения и показа сообщений
  HANDLE m_newcommand;

  /// обработчик очереди сообщений
  static void Thread(DebuggerRouter* _this);

public:
  DebuggerRouter();
  /// сохраняет сообщение в очереди, используються критические секции m_cs чтобы предотвратить коллизии.

  /// сообщение добавляеться в m_messagelist, дополнительно информируеються главный поток о
  /// том что сообщение добавлено m_newcommand.
  void Report(long processid,long threadid,const char* msg);
  /// если кто-то отписываеться от событий отладчика он себя удаялет из списка.
  void DebuggerExit(Debugger* p);
  /// есть отладчики в спике? m_debuggerlist
  bool Empty();
};
