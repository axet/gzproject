// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// DebuggerWindows_i.cpp : Implementation of CDebuggerWindows
#include "stdafx.h"
#include "DebuggerWindows.h"
#include "DebuggerWindows_i.h"

#include "DebuggerRouter.h"

#include <COMDEF.H>

/////////////////////////////////////////////////////////////////////////////
// CDebuggerWindows

static DebuggerRouter g_debuggerrourter;

STDMETHODIMP CDebuggerWindows::Report(long processid, long threadid,BSTR message)
{
  g_debuggerrourter.Report(processid,threadid,_bstr_t(message));

  return S_OK;
}
