// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/// @file
/// @author Alexey Kuznetsov (axet@nm.ru)

#include "../../include/atl.h"

#include <atlctrls.h>
#include <AtlFrame.h>
#include <AtlMisc.h>

#include "../../iniext/iniext.h"
#include "../../ShellNotifyIcon/ShellNotifyIcon.h"

using WTL::CDialogResize;

/// Класс который хочет выводить сообщения об ошибках.
class Debug:public CWindowImpl<Debug>,
  public CDialogResize<Debug>
{
  bool m_debugger;
  ShellNofityIcon::Icons m_icons;
  WTL::CEdit m_edit;

public:

  BEGIN_MSG_MAP(Debug)
    MESSAGE_HANDLER(WM_USER+1,OnIconMsg)
    //MESSAGE_HANDLER(WM_CLOSE,OnClose);
    CHAIN_MSG_MAP(CDialogResize<Debug>)
  END_MSG_MAP()

  BEGIN_DLGRESIZE_MAP(Debug)
    DLGRESIZE_CONTROL(1,DLSZ_SIZE_X|DLSZ_SIZE_Y)
  END_DLGRESIZE_MAP()

  LRESULT OnIconMsg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled);
  LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled);

  Debug();
  ~Debug();
  Debug& operator <<(const char*);
  /// при получении сообщения о завершении работы, прячим иконку из трея.
  virtual void OnFinalMessage(HWND hWnd);
};
