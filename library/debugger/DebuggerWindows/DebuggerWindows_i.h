// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/// @file
/// @author Alexey Kuznetsov (axet@nm.ru)


// DebuggerWindows_i.h : Declaration of the CDebuggerWindows

#ifndef __DEBUGGERWINDOWS_H_
#define __DEBUGGERWINDOWS_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CDebuggerWindows

/// CDebuggerWindows COM класс, передает сообщение в центральный сервер.

class ATL_NO_VTABLE CDebuggerWindows : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CDebuggerWindows, &CLSID_DebuggerWindows>,
	public IDispatchImpl<IDebuggerWindows, &IID_IDebuggerWindows, &LIBID_DEBUGGERWINDOWSLib>
{
public:
	CDebuggerWindows()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_DEBUGGERWINDOWS1)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CDebuggerWindows)
	COM_INTERFACE_ENTRY(IDebuggerWindows)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IDebuggerWindows
public:
  /// передает сообщение в центральный сервер
	STDMETHOD(Report)(long processid, long threadid, BSTR message);
};

#endif //__DEBUGGERWINDOWS_H_
