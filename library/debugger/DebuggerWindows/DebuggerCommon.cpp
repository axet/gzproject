// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "stdafx.h"
#include "debuggercommon.h"

LRESULT Debug::OnIconMsg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
{
  switch(lParam)
  {
  case WM_LBUTTONUP:
    ShowWindow((GetWindowLong(GWL_STYLE)&WS_VISIBLE)==WS_VISIBLE?SW_HIDE:SW_SHOW);
    break;
  case WM_RBUTTONUP:
    PostMessage(WM_CLOSE);
    break;
  }
  return 0;
}

LRESULT Debug::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
{
  //ShowWindow(SW_HIDE);
  //bHandled=TRUE;
  return 0;
}

Debug::Debug():
  //m_debugger(IsDebuggerPresent()==TRUE)
  m_debugger(FALSE)
{
  if(!m_debugger)
  {
    char buf[65];
    itoa(GetCurrentThreadId(),buf,10);

    std::string processname;
    processname=GetModuleFileName();

    std::string log(processname+"_Debug_"+buf+".log");

    RECT rect;
    CWindowImpl<Debug>::Create(GetDesktopWindow(),rcDefault,log.c_str(),WS_OVERLAPPEDWINDOW);
    GetClientRect(&rect);
    m_edit.Create(m_hWnd,rect,0,WS_VISIBLE|WS_CHILDWINDOW|
      ES_MULTILINE|ES_AUTOVSCROLL|ES_READONLY|
      ES_AUTOHSCROLL|ES_NOHIDESEL|WS_VSCROLL|WS_HSCROLL,0,1);
    DlgResize_Init();

    m_icons.push_back(ShellNofityIcon::Icon(0,IDI_INFORMATION,
      log.c_str(),m_hWnd,WM_USER+1));

    ShellNofityIcon::ShowIcon(m_icons);
  }
}

Debug::~Debug()
{
}

void Debug::OnFinalMessage(HWND hWnd)
{
  ShellNofityIcon::HideIcon(m_icons);

  //delete this;
}

Debug& Debug::operator << (const char* p)
{
  if(!m_debugger)
  {
    WTL::CString str(p);
    str.Replace("\n","\r\n");
    m_edit.AppendText(str);
  }else
  {
    OutputDebugString(p);
  }

  return *this;
}
