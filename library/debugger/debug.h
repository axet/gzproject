// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/// @file
/// @author Alexey Kuznetsov (axet@nm.ru)

#ifdef _DEBUG
#pragma comment(lib,"debugger.lib")
#else
#pragma comment(lib,"debugger.lib")
#endif

/// Модуль отладочный. Содержит в себе функции для вывода отладочных сообщений.

/// Сообщения сохраняються в файле рядом с исполянемым файлом.
namespace Debugger
{
  /// вывод сообщения в отладчик
  void DbgTrace(const char*p,...);
  /// вывод ошибки в отладчик
  void DbgTraceError(const char*p,...);
  /// показ сообщения об ошибке.
  void DbgTraceErrorBox(const char*p,...);
}

using namespace Debugger;

#ifndef NDEBUG

/// если необохдимо информировать о состоянии - этот макрос
#define DBGTRACE DbgTrace
/// если происходит ошибка выводить ее нужно этим макросом
#define DBGTRACE_ERROR DbgTraceError

#else

#define DBGTRACE (void)
#define DBGTRACE_ERROR DbgTraceErrorBox

#endif
