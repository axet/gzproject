// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <windows.h>
#include <winbase.h>
#include <stdio.h>
#include <map>
#include <time.h>
#include <direct.h>

#include "debug.h"
#include "../include/atl.h"
#include "../include/hrwrap.h"
#include "../iniext/iniext.h"
#include "../ErrorReport/errorreport.h"
#include "../ShellNotifyIcon/ShellNotifyIcon.h"

#include <atlctrls.h>
#include <AtlFrame.h>
#include <AtlMisc.h>

void DbgTraceFileArgs(const char* pp,va_list args)
{
  try
  {
    char buf[32768];
    _vsnprintf(buf,sizeof(buf),pp,args);

    char debuggerdir[]="DebuggerDirectory";
    time_t t;
    time(&t);
    tm *p=localtime(&t);
    char buf2[128];
    _snprintf(buf2,sizeof(buf),"%02d.%02d.%02d_%02d.%02d_%08x_%08x.txt",
      p->tm_mday,p->tm_mon,1900+p->tm_year,
      p->tm_hour,p->tm_min,
      GetCurrentProcessId(),
      GetCurrentThreadId());

    SplitPath path=GetModuleRunDir().c_str();
    path+=debuggerdir;
    mkdir(((std::string)path).c_str());
    path+=GetModuleName().c_str();
    mkdir(path.GetPath().c_str());
    path+=buf2;
    std::ofstream file(path.GetPath().c_str(),std::ios::ate|std::ios::app);
    file<<buf;
  }
  catch(_com_error &e)
  {
    MessageBox(GetDesktopWindow(),ErrorReport("Debug::DbgTrace",e),"Debug::DbgTrace",MB_ICONHAND);
  }
  catch(std::exception &e)
  {
    MessageBox(GetDesktopWindow(),e.what(),"Debug::DbgTrace",MB_ICONHAND);
  }
  catch(...)
  {
    MessageBox(GetDesktopWindow(),"Unhalted exception","Debug::DbgTrace",MB_ICONHAND);
  }
}

void DbgTraceDbgOutputArgs(const char* pp,va_list args)
{
  try
  {
    char buf[32768];
    _vsnprintf(buf,sizeof(buf),pp,args);
    OutputDebugString(buf);
  }
  catch(_com_error &e)
  {
    MessageBox(GetDesktopWindow(),ErrorReport("Debug::DbgTrace",e),"Debug::DbgTrace",MB_ICONHAND);
  }
  catch(std::exception &e)
  {
    MessageBox(GetDesktopWindow(),e.what(),"Debug::DbgTrace",MB_ICONHAND);
  }
  catch(...)
  {
    MessageBox(GetDesktopWindow(),"Unhalted exception","Debug::DbgTrace",MB_ICONHAND);
  }
}

void DbgTraceArgs(const char* pp,va_list args)
{
  DbgTraceDbgOutputArgs(pp,args);
}

void Debugger::DbgTrace(const char* p,...)
{
  va_list args;
  va_start(args,p);
  DbgTraceArgs(p,args);
  va_end(args);
}

void Debugger::DbgTraceError(const char*p,...)
{
  DbgTrace("TRACEERROR: ");

  va_list args;
  va_start(args,p);
  DbgTraceArgs(p,args);
  va_end(args);
}

void Debugger::DbgTraceErrorBox(const char*p,...)
{
  va_list args;
  va_start(args,p);
  char buf[32768];
  _vsnprintf(buf,sizeof(buf),p,args);
  va_end(args);

  MessageBox(0,buf,"Exception",MB_ICONHAND);
}
