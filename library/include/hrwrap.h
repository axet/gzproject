// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef __HRWRAP_H__
#define __HRWRAP_H__

#include <comdef.h>
#include <exception>

#include "atl.h"

class AtlReportErrorException:public std::exception
{
public:
  AtlReportErrorException(const char* p):exception(p)
  {
    AtlReportError(CLSID_NULL,p,IID_NULL,S_FALSE);
  }
};

// оболочка для клиентских приложений

#define HRWRAP(xx) if(xx!=S_OK) throw AtlReportErrorException("Bad result " #xx)

#define HRWRAP_ISSUE(xx) if(HRESULT hr=xx)<0) _com_issue_error(hr);

// оболочка для ком-серверных приложений

#define HRWRAP_COM(xx) if(xx!=S_OK) throw exception("Bad hresult " #xx)

#endif
