////////////////////////////////////////////////////////////////////////
//
// Doc.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef ALTOVA_DOC_H_INCLUDED
#define ALTOVA_DOC_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000

#include "AltovaXMLAPI.h"

namespace altova {

class CNode;


class ALTOVAXML_DECLSPECIFIER CDoc : public CBaseObject
{
public:
	static MSXML2::IXMLDOMDocument2Ptr GetDocument();
	static MSXML2::IXMLDOMNodePtr CreateFragment();
	static void CheckDocumentCounter();

protected:
    //MSXML2::IXMLDOMDocument2Ptr			m_spDocument;
    //MSXML2::IXMLDOMDocumentFragmentPtr   m_spFragment;
    //int									m_nTmpNameCounter;
    //static __declspec(thread) MSXML2::IXMLDOMDocument2*			m_spDocument;
    //static __declspec(thread) MSXML2::IXMLDOMDocumentFragment*   m_spFragment;
    //static __declspec(thread) int									m_nTmpNameCounter;

public:
	MSXML2::IXMLDOMDocument2Ptr Load(const tstring& sFilename);
	MSXML2::IXMLDOMDocument2Ptr LoadFromString(const tstring& sXml);
	
	void SetEncoding(const tstring& sEncoding);
	void SetRootElementName(tstring sNamespaceURI, tstring sName);
	void SetSchemaLocation(const tstring& sSchemaLocation);

	void Save(const tstring& sFilename, CNode& rNode);
	tstring SaveToString(CNode& rNode);
	void Validate(CNode& rNode);

protected:
	void FinalizeRootElement(CNode& rNode);
	void InternalSetSchemaLocation(CNode& rNode);
	virtual void DeclareNamespaces(CNode& rNode) = 0;
	void DeclareNamespace(CNode& rNode, const tstring& sPrefix, const tstring& sURI);

	tstring m_sEncoding;
	tstring m_sName;
	tstring m_sNamespaceURI;
	tstring m_sSchemaLocation;
};


} // namespace altova

#endif // ALTOVA_DOC_H_INCLUDED
