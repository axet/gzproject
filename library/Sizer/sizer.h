// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef __SIZER_H
#define __SIZER_H

#include <afxtempl.h>

#pragma comment(lib,"sizer.lib")

namespace Sizer
{
  class CSizer;

  enum {S_RESIZE,S_FIXED};

  class CSizerParent
  {
    virtual int GetListCount()=0;
    virtual int GetSizeItem(int)=0;// -1 for resize
    virtual void UpdateItem(int,CSize,CRect&)=0;
    virtual int GetScaleItem(int) = 0;

  public:
    void Update(int,CRect&);
  };

  class CSizerLine:public CSizerParent
  {
  public:
    enum wndtype{sizer,window};
  private:
    struct ITEM
    {
      int type;
      CRect rect;
      wndtype wt;
      int scale;
      union
      {
        CSizer* psizer;
        HWND pWnd;
      };

      bool IsEmpty();
    };

    int m_type;
    int m_size;
    CArray<ITEM,ITEM&> m_list;

    int GetSizeItem(int);
    void UpdateItem(int,CSize,CRect&);
    int GetScaleItem(int);
  public:
    int GetListCount();
    wndtype GetItem(int nIndex,LPVOID&);
    void Create(int type);
    int GetHeight();
    void AddItem(int type,HWND,int scale = 1);
    void AddItem(CSizer*,int scale=1);
    void Update(CRect);
  };

  class CSizer:public CSizerParent
  {
    struct ITEM
    {
      CSizerLine* psizerline;
      bool IsEmpty()
      {
        return psizerline->GetListCount()==0;
      }
    };

    CArray<ITEM,ITEM&> m_list;
    CRect m_rect;

    int GetScaleItem(int);
    int GetSizeItem(int);// -1 for resize
    void UpdateItem(int,CSize,CRect&);
    CSizerLine* m_plastline;
    CSize m_size;
  public:
    CSizer();
    ~CSizer();
    void Clear();
    CSizerLine* AddLine(int type);
    CSizerLine& GetLastLine();
    void SetMinSize(CSize&);
    void Update(CRect);
    int GetListCount();
    CSizerLine* GetLine(int nIndex);
  };
}

#endif
