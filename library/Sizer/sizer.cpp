// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "stdafx.h"
#include "sizer.h"

#define FREESPACE 2

void Sizer::CSizer::Clear()
{
  for(int i=0;i<m_list.GetSize();i++)
  {
    delete m_list[i].psizerline;
  }
  m_list.RemoveAll();
}

Sizer::CSizerLine* Sizer::CSizer::AddLine(int type)
{
  Sizer::CSizerLine *ps=new Sizer::CSizerLine;
  ps->Create(type);
  ITEM item;
  item.psizerline=ps;
  m_list.Add(item);
  m_plastline=ps;
  return ps;
}

Sizer::CSizer::CSizer()
{
  SetMinSize(CSize(0,0));
}

void Sizer::CSizer::SetMinSize(CSize &size)
{
  m_size=size;
}

Sizer::CSizerLine& Sizer::CSizer::GetLastLine()
{
  return *m_plastline;
}

Sizer::CSizerLine * Sizer::CSizer::GetLine(int nIndex)
{
  for(int i=0;i<m_list.GetSize();i++)
  {
    if(m_list[i].IsEmpty())
      nIndex++;
  }

  return m_list[nIndex].psizerline;
}

Sizer::CSizer::~CSizer()
{
  Clear();
}

void Sizer::CSizer::Update(CRect rect)
{
  if(rect.Height()<m_size.cy)
    rect.bottom=rect.top+m_size.cy;
  if(rect.Width()<m_size.cx)
    rect.right=rect.left+m_size.cx;

  rect.left+=FREESPACE;
  rect.top+=FREESPACE/2;
  CSizerParent::Update(rect.Height(),rect);
}

void Sizer::CSizer::UpdateItem(int index,CSize size,CRect &rect)
{
  for(int i=0;i<m_list.GetSize();i++)
  {
    if(m_list[i].IsEmpty())
    {
      index++;
    }else if(i>=index)
    {
      break;
    }
  }

  m_list[index].psizerline->Update(
    CRect(rect.left,rect.top+size.cx,rect.right,rect.top+size.cx+size.cy));
}

int Sizer::CSizer::GetListCount()
{
  int count=0;

  for(int i=0;i<m_list.GetSize();i++)
  {
    if(!m_list[i].IsEmpty())
      count++;
  }

  return count;
}

int Sizer::CSizer::GetScaleItem(int i)
{
  return 1;
}

int Sizer::CSizer::GetSizeItem(int index)
{
  for(int i=0;i<m_list.GetSize();i++)
  {
    if(m_list[i].IsEmpty())
    {
      index++;
    }else if(i>=index)
    {
      break;
    }
  }

  ASSERT(index<m_list.GetSize());
  return m_list[index].psizerline->GetHeight();
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void Sizer::CSizerLine::Create(int type){
  m_type=type;
  if(m_type==S_FIXED)
    m_size=0;
  else
    m_size=-1;
}

Sizer::CSizerLine::wndtype Sizer::CSizerLine::GetItem(int nIndex,LPVOID&p)
{
  p=m_list[nIndex].pWnd;
  return m_list[nIndex].wt;
}

int Sizer::CSizerLine::GetHeight(){
  return m_size;
}

int Sizer::CSizerLine::GetListCount()
{
  int count=0;

  for(int i=0;i<m_list.GetSize();i++)
  {
    if(!m_list[i].IsEmpty())
      count++;
  }
  return count;
}

int Sizer::CSizerLine::GetScaleItem(int index)
{
  for(int i=0;i<m_list.GetSize();i++)
  {
    if(m_list[i].IsEmpty())
    {
      index++;
    }else if(i>=index)
    {
      break;
    }
  }

  return m_list[index].scale;
}

int Sizer::CSizerLine::GetSizeItem(int index)
{
  for(int i=0;i<m_list.GetSize();i++)
  {
    if(m_list[i].IsEmpty())
    {
      index++;
    }else if(i>=index)
    {
      break;
    }
  }

  if(m_list[index].type==S_FIXED)
    return m_list[index].rect.Width();
  else
    return -1;
}

void Sizer::CSizerLine::AddItem(int type,HWND hwnd,int scale)
{
  ASSERT(hwnd==0||IsWindow(hwnd));

  ITEM item;
  if(hwnd!=0)
  {
    GetWindowRect(hwnd,&item.rect);
    if(m_type==S_FIXED){
      if(item.rect.Height()>m_size)
        m_size=item.rect.Height();
    }
    item.pWnd=hwnd;
    item.type=type;
  }else
  {
    item.pWnd=0;
    item.type=S_RESIZE;
  }
  item.scale=scale;
  item.wt=window;
  m_list.Add(item);
}

void Sizer::CSizerLine::AddItem(CSizer*p,int scale)
{
  ASSERT(m_type!=S_FIXED);
  ITEM item;
  item.psizer=p;
  item.type=S_RESIZE;
  item.wt=sizer;
  item.scale=scale;
  m_list.Add(item);
}

void Sizer::CSizerLine::Update(CRect rect)
{
  CSizerParent::Update(rect.Width(),rect);
}

void Sizer::CSizerLine::UpdateItem(int index,CSize size,CRect &rect)
{
  for(int i=0;i<m_list.GetSize();i++)
  {
    if(m_list[i].IsEmpty())
    {
      index++;
    }else if(i>=index)
    {
      break;
    }
  }

  CRect mrect=CRect(rect.left+size.cx,rect.top,rect.left+size.cx+size.cy,rect.bottom);
  switch(m_list[index].wt)
  {
  case window:
    {
      if(m_list[index].pWnd!=0)
      {
        CWnd::FromHandle(m_list[index].pWnd)->SetWindowPos(0,
          mrect.left,mrect.top,mrect.Width(),mrect.Height(),
          m_list[index].type==S_FIXED?SWP_NOSIZE:0);
      }
    }
    break;
  case sizer:
    {
      m_list[i].psizer->Update(mrect);
    }
    break;
  }
}

void Sizer::CSizerParent::Update(int msize,CRect& mrect)
{
  if(GetListCount()==0)
    return;

  int fixedsize=0;
  int resizelinecount=0;
  for(int i=0;i<GetListCount();i++)
  {
    int size=GetSizeItem(i);
    if(size!=-1)
      fixedsize+=size;
    else
      resizelinecount+=GetScaleItem(i);
  }
  int freesize=GetListCount()*FREESPACE;
  int resizesize=msize-fixedsize-freesize;
  if(resizelinecount!=0)
    resizesize/=resizelinecount;

  i=0;
  int top=0;
  int topsize=0;
  while(i<GetListCount())
  {
    int size=GetSizeItem(i);
    if(size==-1)
      topsize=resizesize;
    else
      topsize=size;
    topsize=topsize*GetScaleItem(i)+( (GetScaleItem(i)-1) * FREESPACE);

    UpdateItem(i,CSize(top,topsize),mrect);
    top+=topsize+FREESPACE;
    i++;
  }
}

/////////////////////////////

bool Sizer::CSizerLine::ITEM::IsEmpty()
{
  switch(wt)
  {
  case window:
    {
      if(pWnd==0)
        return false;

      if((GetWindowLong(pWnd,GWL_STYLE)&WS_VISIBLE)==WS_VISIBLE)
      {
        return false;
      }
    }
    break;
  case sizer:
    {
      if(psizer->GetListCount()>0)
        return false;
    }
    break;
  }
  return true;
}
