// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <set>

#include <atlbase.h>

/// вспомогательный шаблон для преобразования строк вида "имя\имя2\имя3"
/// в деревовидную структуру, на вход попадают такие строки с привязкой
/// к внешним данным а на выход идут имена папок содержадщие уровень
/// вложенности и списки внешних данных

template <class LinkDataType> class TextParser
{
public:
  /// входное значение
  /// текстовая строчка (список папок)
  /// и привязка к внешним данным
  struct InputValue
  {
    LinkDataType data;
    std::string text;

    InputValue(const char* t,LinkDataType d):data(d),text(t) {}

    operator < (const InputValue& iv) const
    {
      return text<iv.text;
    }
  };
  typedef std::vector< InputValue > InputValueList;

  /// это строка таблицы
  /// список текстовых значений (имен папок)
  /// и привязка к данным
  struct InternalValue
  {
    SplitPath sp;
    LinkDataType data;

    InternalValue() {}
    InternalValue(const InputValue& i)
    {
      sp=i.text.c_str();
      if(sp.empty())
        sp.push_back("");
      data=i.data;
    }

    operator < (const InternalValue &si) const
    {
      return sp<si.sp;
    }
  };
  /// внутренная таблица представляет из себя ячейки в каждой из которых
  /// есть текст (название папки) и привязка всей строки таблице в внешним
  /// данным
  typedef std::vector<InternalValue> InternalValueList;

  /// это значение указаывает на один элемент моей таблицы
  struct InternalValueIterator
  {
    SplitPath::iterator i;
    SplitPath::iterator istart;
    SplitPath::iterator iend;
    int level;
    LinkDataType data;

    InternalValueIterator():level(-1)
    {
      ;
    }
    InternalValueIterator(InternalValue& _i)
    {
      i=_i.sp.begin();
      istart=_i.sp.begin();
      iend=_i.sp.end();
      level=0;
      data=_i.data;
    }

    bool IsEndStep()
    {
      if(i==iend)
        return true;

      level++;
      i++;

      return i==iend;
    }
  };
  /// список указатейлей на необработанные значения моей таблицы
  /// по мере обработки этот список будет уменьшаться
  typedef std::list<InternalValueIterator> InternalValueIteratorList;

  typedef std::vector<LinkDataType> ListDataTypeList;

  /// выходное значение
  /// описывает уровень вложенности
  /// текстовое значение (название папки)
  /// и привязку к внешним данным, которая предсатвляет из себя список
  /// внешних значений так как в одной папке их может лежать несколько
  struct OutputValue
  {
    int level;
    /// назнвание конечной папки
    std::string name;
    /// путь до текущей точки
    std::string path;
    ListDataTypeList data;

    OutputValue(InternalValueIteratorList::iterator i)
    {
      level=i->level;
      name=i->i->c_str();
      path=SplitPath::GetPath(i->istart,i->i+1);
    }
  };
  typedef std::vector< OutputValue > OutputValueList;

  static OutputValueList Parse(const InputValueList& ta)
  {
    OutputValueList ovl;

    /// сначало я преобразую входные строчки (разбиваю пути на отдельные папки)
    /// те создаю свою таблицу данных
    InternalValueList ivl;
    ivl.resize(ta.size());
    std::copy(ta.begin(),ta.end(),ivl.begin());
    /// сортирую списки для того чтобы можно было производить дальнейшую обработку
    std::sort(ivl.begin(),ivl.end());

    /// загружаю список указателей на строки моей таблицы
    /// полный список значений
    InternalValueIteratorList ivil;
    ivil.resize(ivl.size());
    std::copy(ivl.begin(),ivl.end(),ivil.begin());

    /// и обрабатываю пока таблица содержит не обработанные строки
    while(!ivil.empty())
    {
      InternalValueIteratorList::iterator i=ivil.begin();

      OutputValueList::iterator iovl=ovl.insert(ovl.end(),i);

      // ищу элементы с одинаковым именем
      InternalValueIteratorList::iterator i2=i;
      i2++;

      // i->i==i2->i) проверят если указатели на
      while(i2!=ivil.end() && i->level==i2->level && *i->i==(*i2->i) )
      {
        /// такой нашелся значит я его инкрементирую (увеличивую уровень
        /// и указательна следующий элемент в строке таблицы
        if(i2->IsEndStep())
        {
          iovl->data.push_back(i2->data);
          ivil.erase(i2++);
          continue;
        }
        i2++;
      }
      if(i->IsEndStep())
      {
        iovl->data.push_back(i->data);
        ivil.erase(i++);
        continue;
      }
    }

    return ovl;
  }
};
