// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/// на выход получает два списка строк
/// на выходе получает разницу между этими массивами

/// сравнивает первый со вторым
/// если в первом есть елемент а во втором нету то

/// для того чтобы получить индентичные списки нужно к перовму добавить
/// элемент второго (Increment) , если в пером он отсутствует нужно
/// удалить второй элемент (Decrement)

#include <algorithm>

template <class LinkDataType>
class CompareTree
{
public:
  /// входное значение
  /// текстовая строчка (список папок)
  /// и привязка к внешним данным
  struct InputValue
  {
    LinkDataType data;
    std::string text;

    InputValue(const char* t,LinkDataType d):text(t),data(d)
    {
    }

    bool operator < (const InputValue&iv) const
    {
      return text<iv.text;
    }

    bool operator == (const InputValue &iv) const
    {
      return text==iv.text;
    }

    bool operator != (const InputValue &iv) const
    {
      return text!=iv.text;
    }
  };
  typedef std::vector< InputValue > InputValueList;

  struct OutputValue
  {
    enum Action{Increment,Decrement} action;
    LinkDataType data;

    OutputValue(Action a,InputValueList::iterator i):data(i->data),action(a)
    {
    }
  };
  typedef std::vector< OutputValue > OutputValueList;

  static OutputValueList Compare(InputValueList& iv1,InputValueList &iv2)
  {
    std::sort(iv1.begin(),iv1.end());
    std::sort(iv2.begin(),iv2.end());

    OutputValueList ovl;
    {
    for(InputValueList::iterator i1=iv1.begin();i1!=iv1.end();i1++)
    {
      bool i1found=false;
      for(InputValueList::iterator i2=iv2.begin();i2!=iv2.end();i2++)
      {
        if(*i1==*i2)
        {
          i1found=true;
          break;
        }
      }
      if(!i1found)
      {
        ovl.push_back(OutputValue(OutputValue::Decrement,i1));
      }
    }
    }
    {
    for(InputValueList::iterator i2=iv2.begin();i2!=iv2.end();i2++)
    {
      bool i2found=false;
      for(InputValueList::iterator i1=iv1.begin();i1!=iv1.end();i1++)
      {
        if(*i1==*i2)
        {
          i2found=true;
          break;
        }
      }
      if(!i2found)
      {
        ovl.push_back(OutputValue(OutputValue::Increment,i2));
      }
    }
    }
    return ovl;
  }
};
