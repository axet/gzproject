// GZProject - library, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// ExToolBar.cpp : implementation file
//

#include "ExToolBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExToolBar

CExToolBar::CExToolBar()
{
  m_dwState = 0;
  m_hilOld = NULL;
}

CExToolBar::~CExToolBar()
{
}


BEGIN_MESSAGE_MAP(CExToolBar, CToolBar)
	//{{AFX_MSG_MAP(CExToolBar)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExToolBar message handlers

BOOL CExToolBar::SetButtonsBitmaps(int nIDnormal, int nIDgrayed, COLORREF cfMask, int cx, int cy, UINT nFlags)
{
  CToolBarCtrl &tbc = GetToolBarCtrl();
  CImageList *pil = tbc.GetImageList();
  int nImages = 20;
  if(pil)
  {
    nImages = pil->GetImageCount();
    m_hilOld = pil->m_hImageList;
  }

  BOOL ret;
  CBitmap bmpn, bmpg;
  ret = bmpn.LoadBitmap(nIDnormal) && bmpg.LoadBitmap(nIDgrayed);
  ASSERT(ret);

  if(cx == 0)
  {
    BITMAP bm1,bm2;
    bmpn.GetBitmap(&bm1);
    bmpg.GetBitmap(&bm2);

    cx=max(bm2.bmHeight,bm1.bmHeight);
    cy=cx;
  }

  if(ret)
  {
    ret = m_ilButtonsGrayed.Create(cx, cy, nFlags, 0, nImages);
    ret = ret && m_ilButtonsNormal.Create(cx, cy, nFlags, 0, nImages);
  }
  if(ret)
  {
    ret = (m_ilButtonsNormal.Add(&bmpn, cfMask) != -1) &&
          (m_ilButtonsGrayed.Add(&bmpg, cfMask) != -1);
  }
  if(ret)
  {
    tbc.SetImageList(&m_ilButtonsNormal);
    tbc.SetDisabledImageList(&m_ilButtonsGrayed);

    SetSizes(CSize(cx+7,cy+6),CSize(cx,cy));
  }
  return ret;
}

void CExToolBar::OnDestroy() 
{
  CToolBarCtrl &tbc = GetToolBarCtrl(); //Otherwize imagelist resource 
  tbc.SendMessage(TB_SETIMAGELIST, 0, (LPARAM)m_hilOld); //isn't freed
	
  CToolBar::OnDestroy();
}
