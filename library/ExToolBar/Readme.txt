Немного измененный проект который прижился в компании. Он поддерживает показ 
иконок тулбара не стандартного размера и цвета. Для встраивания в проект 
необходимо заменить Стандартный тулбар создаваемый визордом, на имя этого 
класса, строчку с загрузкой (LoadToolBar) нужно дополнить и добавить 
SetButtonsBitmaps. Параметры описаны в хедере. Изменения относительно 
стандартной библиотеки в том что программисту не нужно задавать размер 
изображения, оно берется из битмапки.

Пример:
	if (!m_wndToolBar.CreateEx(this) ||
	!m_wndToolBar.LoadToolBar(IDR_TOOLBAR_PARAMETERS)||
	!m_wndToolBar.SetButtonsBitmaps(IDB_PARAMETERS_BITMAP3,
		IDB_PARAMETERS_BITMAP1,RGB(198,199,198)))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	
Предварительно заменить IDR_TOOLBAR_PARAMETERS, IDB_PARAMETERS_BITMAP3, 
IDB_PARAMETERS_BITMAP1 на свои идентификаторы ресурсов.
