// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Script.h : Declaration of the CScript

#ifndef __SCRIPT_H_
#define __SCRIPT_H_

#include "resource.h"       // main symbols
#include "Site.h"

#include <comdef.h>
#include <ACTIVSCP.H>

// #import <vbscript.dll> rename("RGB","fnRGB") named_guids

DEFINE_GUID(CLSID_VBScript, 0xb54f3741, 0x5b07, 0x11cf, 0xa4, 0xb0, 0x0,
            0xaa, 0x0, 0x4a, 0x55, 0xe8); 

/////////////////////////////////////////////////////////////////////////////
// CScript
class ATL_NO_VTABLE CScript : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CScript, &CLSID_Script>,
	public ISupportErrorInfo,
	public IDispatchImpl<IScript, &IID_IScript, &LIBID_SCRIPTHOSTLib>
{
	CComPtr<IActiveScript> m_activescript;
  CComPtr< CComObject<CSite> > m_site;
  CComQIPtr<IActiveScriptParse> m_activescriptparse;

public:
  CScript():m_site(0)
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_SCRIPT)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CScript)
	COM_INTERFACE_ENTRY(IScript)
	COM_INTERFACE_ENTRY(IDispatch)
  COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IScript
public:
	STDMETHOD(SetGlobalObject)(IUnknown* i);
	STDMETHOD(RunScript)();
	STDMETHOD(LoadScript)(BSTR scriptbody);
	STDMETHOD(AddObject)(BSTR name, IUnknown* unk);
};

#endif //__SCRIPT_H_
