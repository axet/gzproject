// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Script.cpp : Implementation of CScript
#include "stdafx.h"
#include "ScriptHost.h"
#include "Script.h"
#include "../include/hrwrap.h"
#include "../errorreport/errorreport.h"

/////////////////////////////////////////////////////////////////////////////
// CScript

STDMETHODIMP CScript::AddObject(BSTR name, IUnknown *unk)
{
  try
  {
	  m_site->m_objectmap[(const char*)_bstr_t(name)]=unk;

    if(m_activescript!=0)
    {
      HRWRAP(m_activescript->AddNamedItem(name,SCRIPTITEM_ISVISIBLE));
    }
  }catch(_com_error &e)
  {
    return AtlReportError(__uuidof(IScript),ErrorReport("CScript::AddObject",e),IID_NULL,E_FAIL);
  }

	return S_OK;
}

STDMETHODIMP CScript::LoadScript(BSTR scriptbody)
{
  try
  {
    if(m_activescript==0)
    {
		  HRWRAP(CoCreateInstance(CLSID_VBScript, NULL, CLSCTX_INPROC_SERVER,
			  IID_IActiveScript, (void **)&m_activescript));
    }
    if(m_site==0)
    {
      HRWRAP(CComObject<CSite>::CreateInstance(&m_site));
      ((CSite*)m_site)->AddRef();
      HRWRAP(m_activescript->SetScriptSite(m_site));
    }
    if(m_activescriptparse==0)
    {
      m_activescriptparse=m_activescript;
    }
    HRWRAP(m_activescriptparse->InitNew());
    EXCEPINFO ei={0};
    HRWRAP(m_activescriptparse->ParseScriptText(scriptbody,0,0,0,0,0,0,0,&ei));
  }catch(_com_error &e)
  {
    return AtlReportError(__uuidof(IScript),ErrorReport("CScript::LoadScript",e),IID_NULL,E_FAIL);
  }
	return S_OK;
}

STDMETHODIMP CScript::RunScript()
{
  try
  {
    HRWRAP(m_activescript->SetScriptState(SCRIPTSTATE_CONNECTED));

    HRWRAP(m_activescript->Close());

    if(m_site->m_errorlist.size()!=0)
    {
      std::string str;
      for(CSite::ErrorList::iterator i=m_site->m_errorlist.begin();i!=m_site->m_errorlist.end();i++)
      {
        str+=(*i)+"\n";
      }
      throw exception(str.c_str());
    }
  }catch(_com_error &e)
  {
    return AtlReportError(__uuidof(IScript),ErrorReport("CScript::RunScript",e),IID_NULL,E_FAIL);
  }catch(exception &e)
  {
    return AtlReportError(__uuidof(IScript),e.what(),IID_NULL,E_FAIL);
  }
	return S_OK;
}

STDMETHODIMP CScript::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IScript
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CScript::SetGlobalObject(IUnknown *i)
{
  try
  {
    _bstr_t name="__GLOBAL_OBJECT__";

	  m_site->m_objectmap[(const char*)_bstr_t(name)]=i;

    if(m_activescript!=0)
    {
      HRWRAP(m_activescript->AddNamedItem(name,SCRIPTITEM_ISVISIBLE|SCRIPTITEM_GLOBALMEMBERS));
    }
  }catch(_com_error &e)
  {
    return AtlReportError(__uuidof(IScript),ErrorReport("CScript::SetGlobalObject",e),IID_NULL,E_FAIL);
  }
	return S_OK;
}
