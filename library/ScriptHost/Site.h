// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Site.h : Declaration of the CSite

#ifndef __SITE_H_
#define __SITE_H_

#include "resource.h"       // main symbols

#include <ACTIVSCP.H>
#include <STRING>
#include <map>
#include <VECTOR>
#include <COMDEF.H>

/////////////////////////////////////////////////////////////////////////////
// CSite
class ATL_NO_VTABLE CSite : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CSite, &CLSID_Site>,
	public ISupportErrorInfo,
  public IActiveScriptSite,
	public IDispatchImpl<ISite, &IID_ISite, &LIBID_SCRIPTHOSTLib>
{
public:
  // карта именованных обьектов
  typedef std::map<std::string,IUnknownPtr> ObjectMap;
  ObjectMap m_objectmap;
  IUnknownPtr m_objectglobal;
  typedef std::vector<std::string> ErrorList;
  ErrorList m_errorlist;

  CSite()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_SITE)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CSite)
	COM_INTERFACE_ENTRY(ISite)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
  COM_INTERFACE_ENTRY(IActiveScriptSite)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// ISite
public:

  // Методы IActiveScriptSite...
  virtual HRESULT __stdcall GetLCID(LCID *plcid) 
  {
    return S_OK;
  }

  virtual HRESULT __stdcall GetItemInfo(LPCOLESTR pstrName,
    DWORD dwReturnMask, IUnknown **ppunkItem, ITypeInfo **ppti);

  virtual HRESULT __stdcall GetDocVersionString(BSTR *pbstrVersion) 
  {
    return S_OK;
  }

  virtual HRESULT __stdcall OnScriptTerminate(
    const VARIANT *pvarResult, const EXCEPINFO *pexcepInfo) 
  {
    return S_OK;
  }

  virtual HRESULT __stdcall OnStateChange(SCRIPTSTATE ssScriptState) 
  {
    return S_OK;
  }

  virtual HRESULT __stdcall OnScriptError(IActiveScriptError *pscriptError);

  virtual HRESULT __stdcall OnEnterScript(void) 
  {
    return S_OK;
  }

  virtual HRESULT __stdcall OnLeaveScript(void) 
  {
    return S_OK;
  }
};

#endif //__SITE_H_
