// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// ScriptHostTest.cpp : Defines the entry point for the application.
//

#include "stdafx.h"

#import "scripthost.dll"
//#import "Project1.dll"

#define EOL "\n"

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
  char scriptbody[]=
    "mmm" EOL
    "Sub mmm" EOL
    "MyShow1" EOL
    "end sub";

  CoInitialize(0);
  try
  {
    SCRIPTHOSTLib::IScriptPtr script(__uuidof(SCRIPTHOSTLib::Script));
    script->LoadScript(scriptbody);

    //Project1::_Class1Ptr a(__uuidof(Project1::Class1));

    //script->SetGlobalObject(a);
    script->RunScript();
  }catch(_com_error &e)
  {
    MessageBox(0,ErrorReport("Test",e),"ScriptHostTest",MB_ICONHAND);
  }
  CoUninitialize();

	return 0;
}
