// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Site.cpp : Implementation of CSite
#include "stdafx.h"
#include "ScriptHost.h"
#include "Site.h"

/////////////////////////////////////////////////////////////////////////////
// CSite

STDMETHODIMP CSite::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_ISite
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

HRESULT __stdcall CSite::OnScriptError(IActiveScriptError *pscriptError)
{
  // Это сообщение появится в случае ошибки в скрипте.
  // Более подробная информация в pscriptError.

  DWORD sourcecontext,linenumber;
  long charpos;
  CComBSTR line;
  EXCEPINFO ei;
  pscriptError->GetSourcePosition(&sourcecontext,&linenumber,&charpos);
  pscriptError->GetSourceLineText(&line);
  pscriptError->GetExceptionInfo(&ei);

  char buf[16384];
  _snprintf(buf,sizeof(buf),"%d, line:%d, charpos: %d\n%s\n%s",
    sourcecontext,linenumber,charpos,(const char*)_bstr_t(line),
    (const char*)_bstr_t(ei.bstrDescription));

  m_errorlist.push_back(buf);
  return S_OK;
}

HRESULT __stdcall CSite::GetItemInfo(LPCOLESTR pstrName,DWORD dwReturnMask, IUnknown **ppunkItem, ITypeInfo **ppti)
{
  if(ppti) 
  {
     *ppti = NULL;
   
     // Если просят ITypeInfo... 
     if(dwReturnMask & SCRIPTINFO_ITYPEINFO) {
        // ничего не делаем;
     }
  }

  // Если Windows Scripting разместил ppunkItem...
  if(ppunkItem)
  {
     *ppunkItem = NULL;
   
     // Если Windows Scripting требует IUnknown нашего объекта...
     if(dwReturnMask & SCRIPTINFO_IUNKNOWN) 
     {
        ObjectMap::iterator i=m_objectmap.find((const char*)_bstr_t(pstrName));
        if(i==m_objectmap.end())
        {
          return TYPE_E_ELEMENTNOTFOUND;
        }
        i->second->QueryInterface(IID_IUnknown,(void**)ppunkItem);
     }
  }
  return S_OK;
}
