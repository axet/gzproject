// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/// @file
/// @author Alexey Kuznetsov (axet@nm.ru)


#ifndef ALTOVA_XMLEXCEPTIONADDIN_H_INCLUDED
#define ALTOVA_XMLEXCEPTIONADDIN_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000

#include <exception>

namespace altova {


/// Расширенный класс для детальной информации о случившимся исключении.

/// Возращает дополинетльную информацию в удобночитаемом виде.

class CXmlExceptionAddin:public CXmlException
{
public:

  CXmlExceptionAddin(MSXML2::IXMLDOMParseErrorPtr errPtr):CXmlException(0,CSchemaString(tstring()))
  {
    m_nCode=CXmlException::eError1;

    char buf[1024];
    _snprintf(buf,sizeof(buf),"Error:\n"
      "Code = 0x%x\n"
      "Source = Line : %ld; Char : %ld\n"
      "Error Description = %s\n"
      "Source Text = '%s'\n"
      "Url = '%s'\n"
      "File pos= %d",
      errPtr->errorCode,
      errPtr->line,errPtr->linepos,
      (const char*)errPtr->reason,
      (const char*)errPtr->srcText,
      (const char*)errPtr->url,
      errPtr->filepos);
  }
};


} // namespace altova

#endif // ALTOVA_XMLEXCEPTION_H_INCLUDED
