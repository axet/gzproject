#include <WinSock2.h>
#include <tchar.h>

#pragma comment(lib,"altova.lib")
#pragma comment(lib,"altovaxml.lib")

#import "msxml3.dll" named_guids no_implementation

#include "Altova.h"
#include "SchemaTypes.h"
#include "AltovaException.h"
#include "SchemaTypeString.h"
#include "SchemaTypeNumber.h"
#include "SchemaTypeCalendar.h"
#include "SchemaTypeBinary.h"
#include "../AltovaXML/XmlException.h"
#include "../AltovaXML/Doc.h"
#include "../AltovaXML/Node.h"
#include "DocAddin.h"
using namespace altova;
