#pragma once;

#include <atlcore.h>

class SentrySc
{
  CComAutoCriticalSection *m_cs;
  int m_i;

public:
  SentrySc(CComAutoCriticalSection &cs):m_cs(&cs),m_i(0)
  {
    Lock();
  }
  ~SentrySc()
  {
    Unlock();
  }
  void Lock()
  {
    m_i++;
    m_cs->Lock();
  }
  void Unlock()
  {
    if(m_i<=0)
      return;
    m_i--;
    m_cs->Unlock();
  };
};

