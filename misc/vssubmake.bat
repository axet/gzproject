set MAKEFILE=%2%
set DIR=%3%

pushd %DIR%
%NMAKE% -f %MAKEFILE% ACTION=%ACTION% CONFIG=%CONFIG% solutions || exit 1
popd
