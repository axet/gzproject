import codecs
import os
import sys
from os.path import join, getsize

utf8decoder = codecs.lookup('utf-8')[1]
windecoder = codecs.lookup('windows-1251')[1]
utfencoder = codecs.lookup('utf-8')[0]

def isutf8(string):
    try:
        utf8decoder(string)
        return 1
    except UnicodeError:
        return 0

def cp12512utf8(string):
    return utfencoder(windecoder(string)[0])[0]

def convert(name):
  try:
    f=open(name)
    lines=f.read()
    f.close()
  except IOError:
    return False
  if not isutf8(lines):
    lines=cp12512utf8(lines)
    try:
      f=open(name,"w");
      f.write(lines)
      f.close()
    except IOError:
      None
    return True;
  else:
    return False;

def parse(root):
  for root, dirs, files in os.walk(root):
      for name in files:
        (rrr,ext)=os.path.splitext(name)
        if ext in exts :
          path=join(root, name);
          if convert(path):
            print "+ "+path
          else:
            print "- "+path
      if '.svn' in dirs:
          dirs.remove('.svn')

exts=['.cpp','.h','.txt','.php']

if len(sys.argv)>1 :
  for root in sys.argv:
    parse(root)
else:
  print "Usage: toutf8.py [path]"
