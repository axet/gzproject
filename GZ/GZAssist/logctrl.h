#ifndef __LOGCTRL_H__
#define __LOGCTRL_H__

#include <likemsdev/likemsdev.h>

class CUnitLog;

class CLogCtrl:public LikeMsdev::CSizingControlBarG
{
  CRichEditCtrl m_rich;
  CFont m_font;

  virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam );
public:
  CLogCtrl();
  ~CLogCtrl();
  void AddText(const char*);
};

#endif
