#include "stdafx.h"
#include "GZAUnit.h"

void CGZAUnit::Disconnect()
{
  CGZUnit::Disconnect();
}

void CGZAUnit::Connect(const char* p)
{
  CGZUnit::Connect(0,p);
}

void CGZAUnit::Login(const char* login,const char* password,const char* charname)
{
  LInfo l;
  l.login=login;
  l.password=password;
  l.charname=charname;
  SetLInfo(l);

  PostThreadMessage(GetThreadId(),WM_USER+1,0,0);
}
