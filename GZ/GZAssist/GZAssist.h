// GZAssist.h : main header file for the GZASSIST application
//

#if !defined(AFX_GZASSIST_H__783FEFD0_EB13_4D69_B5B4_21D193C47185__INCLUDED_)
#define AFX_GZASSIST_H__783FEFD0_EB13_4D69_B5B4_21D193C47185__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CGZAssistApp:
// See GZAssist.cpp for the implementation of this class
//

#pragma warning(disable:4786)

#include <Altova/AltovaLib.h>
#include "../GZone/gzunit/gzunit.h"
#include <exception>
#include "../client/packets.h"
#include <winsock2.h>
#include "mainfrm.h"
#include "optionsmainpage.h"
#include "assistaddin.h"
#include <vector>
#include "iniext/iniext.h"
#include "GZAUnit.h"
#include "../GZone/gzunit/InterruptableUnit.h"

class CGZAssistApp : public CWinApp
{
public:
  InterruptableUnit<CGZAUnit> m_gzunit;

  struct AssistAddinItem
  {
    CAssistAddin* addin;
    bool enabled;
    AssistAddinItem(CAssistAddin*p,bool en=true) : addin(p),enabled(en) { ; }
  };
  typedef std::vector<AssistAddinItem> AssistAddinList;
  OptionsMainPage m_optionsmain;
  AssistAddinList m_addinlist;
  CUnitLog m_unitlog;

  CGZAssistApp();
  CMainFrame& MainWnd() { return *(CMainFrame*)AfxGetMainWnd(); }

private:

  CString GetIniPath();
  CString GetIni();

  void LoadModules();

public:
  virtual BOOL InitInstance();
  virtual int ExitInstance();
  virtual BOOL PreTranslateMessage(MSG* pMsg);

  afx_msg void OnAppAbout();
  afx_msg void OnFileRestart();

  DECLARE_MESSAGE_MAP()
  virtual LRESULT ProcessWndProcException(CException* e, const MSG* pMsg);
  virtual int Run();
};

inline CGZAssistApp& App() { return *(CGZAssistApp*)AfxGetApp(); }

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GZASSIST_H__783FEFD0_EB13_4D69_B5B4_21D193C47185__INCLUDED_)
