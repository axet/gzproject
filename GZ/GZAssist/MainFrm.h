// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__45D86DF7_A287_49E9_BF71_69CE8B1D3928__INCLUDED_)
#define AFX_MAINFRM_H__45D86DF7_A287_49E9_BF71_69CE8B1D3928__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "logctrl.h"

#include "../interproc_clientaddin/outofprocmasterevents.h"

class CGZAssistApp;

class CMainFrame : public CFrameWnd,public COutOfProcMasterPost
{
  ATOM m_hotkey;

  CString m_name;
  CString m_password;
  CString m_charname;

  void LogMessage(const char*);
  void Exit(unsigned int);
  void ConnectionEstablished(const char* name,const char* pass, const char* charname);
  void ConnectionLost();
  void LocalConnectionLost();

protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:
  CLogCtrl m_logwindow;

  void StartClient();

  void AppLog(const char* p,...);
  void ClientLog(const char* p,...);
  virtual void ThreadGZUnitEvent(EventThread* p);

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CReBar      m_wndReBar;
	CDialogBar      m_wndDlgBar;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnViewOptions();
	afx_msg void OnViewLog();
	afx_msg void OnUpdateViewLog(CCmdUI* pCmdUI);
	afx_msg void OnViewModules();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
    afx_msg long OnThreadEvent(unsigned,long);
	//}}AFX_MSG
  LRESULT OnHotKey(WPARAM,LPARAM);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__45D86DF7_A287_49E9_BF71_69CE8B1D3928__INCLUDED_)
