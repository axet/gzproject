#if !defined(AFX_BLOODDRINKDLG_H__FC465F48_F055_4AD9_A4DC_4FD2270A631C__INCLUDED_)
#define AFX_BLOODDRINKDLG_H__FC465F48_F055_4AD9_A4DC_4FD2270A631C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BloodDrinkDlg.h : header file
//

#include <afx.h>
#include <afxwin.h>
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CBloodDrinkDlg dialog

class CBloodDrinkDlg : public CDialog
{
// Construction
public:
	CBloodDrinkDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBloodDrinkDlg)
  enum {IDD = IDD_DIALOG_BLOODDRINK};
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBloodDrinkDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBloodDrinkDlg)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BLOODDRINKDLG_H__FC465F48_F055_4AD9_A4DC_4FD2270A631C__INCLUDED_)
