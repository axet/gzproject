// GZAssistView.cpp : implementation file
//

#include "stdafx.h"
#include "GZAssist.h"
#include "GZAssistView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGZAssistView

IMPLEMENT_DYNCREATE(CGZAssistView, CEditView)

CGZAssistView::CGZAssistView()
{
}

CGZAssistView::~CGZAssistView()
{
}


BEGIN_MESSAGE_MAP(CGZAssistView, CEditView)
	//{{AFX_MSG_MAP(CGZAssistView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGZAssistView drawing

void CGZAssistView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CGZAssistView diagnostics

#ifdef _DEBUG
void CGZAssistView::AssertValid() const
{
	CEditView::AssertValid();
}

void CGZAssistView::Dump(CDumpContext& dc) const
{
	CEditView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGZAssistView message handlers

void CGZAssistView::AddText(const char* p)
{
  GetEditCtrl().SetSel(-1,-1);
  GetEditCtrl().ReplaceSel(p);
  GetEditCtrl().ReplaceSel("\xd\xa");
}

void CGZAssistView::OnInitialUpdate() 
{
	CEditView::OnInitialUpdate();
	
	GetEditCtrl().SetReadOnly();
}

int CGZAssistView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	lpCreateStruct->style|=ES_MULTILINE;

  if (CEditView::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}
