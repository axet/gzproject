// OptionsMainPage.cpp : implementation file
//

#include "stdafx.h"
#include "GZAssist.h"
#include "OptionsMainPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptionsMainPage property page

IMPLEMENT_DYNCREATE(COptionsMainPage, CPropertyPage)

COptionsMainPage::COptionsMainPage() : CPropertyPage(COptionsMainPage::IDD)
{
	//{{AFX_DATA_INIT(COptionsMainPage)
	m_hotkey = FALSE;
	//}}AFX_DATA_INIT
}

COptionsMainPage::~COptionsMainPage()
{
}

void COptionsMainPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_HOTKEY1, m_hotkeyctrl);
    DDX_Text(pDX, IDC_EDIT_PATH, clientpath);
	DDX_Check(pDX, IDC_CHECK2, m_hotkey);
	DDX_Check(pDX, IDC_CHECK1, savelogfile);
}


BEGIN_MESSAGE_MAP(COptionsMainPage, CPropertyPage)
	//{{AFX_MSG_MAP(COptionsMainPage)
	ON_BN_CLICKED(IDC_CHECK2, OnCheck2)
    ON_BN_CLICKED(IDC_BUTTON_BROWSE, OnButtonBrowse)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionsMainPage message handlers

static void WritePrivateProfileInt(const char* app,const char* key,int i)
{
  char num[64];
  _itoa(i,num,10);
  AfxGetApp()->WriteProfileString(app,key,num);
}

void COptionsMainPage::OnCheck2() 
{
  UpdateData();
  m_hotkeyctrl.EnableWindow(m_hotkey);
}

BOOL COptionsMainPage::OnInitDialog() 
{
  CPropertyPage::OnInitDialog();

  m_hotkey=hotkey!=0;
  m_hotkeyctrl.EnableWindow(m_hotkey);
  WORD mod=HIWORD(hotkey);
  WORD m=(mod&MOD_ALT?HOTKEYF_ALT:0)|
    (mod&MOD_CONTROL?HOTKEYF_CONTROL:0)|
    (mod&MOD_SHIFT?HOTKEYF_SHIFT:0);
	m_hotkeyctrl.SetHotKey(LOWORD(hotkey),m);

  UpdateData(FALSE);

  return TRUE;
}

void COptionsMainPage::OnOK()
{
  UpdateData();

  WORD key,mod;
  m_hotkeyctrl.GetHotKey(key,mod);

  hotkey=key;
  WORD &w=*((WORD*)&hotkey+1);
  w=(mod&HOTKEYF_ALT?MOD_ALT:0)|
    (mod&HOTKEYF_CONTROL?MOD_CONTROL:0)|
    (mod&HOTKEYF_SHIFT?MOD_SHIFT:0);
}

void COptionsMainPage::OnDestroy() 
{
  CPropertyPage::OnDestroy();
}

void COptionsMainPage::OnButtonBrowse()
{
  CFileDialog dlg(TRUE,0,0,0,"Exe *.exe|*.exe||");
  if(dlg.DoModal()!=IDOK)
    return;
  clientpath=dlg.GetPathName();
  UpdateData(FALSE);
}
