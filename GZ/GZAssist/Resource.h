//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GZAssist.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_GZASSIST_FORM               101
#define IDB_SPLASH                      102
#define IDR_MAINFRAME                   128
#define IDR_GZASSITYPE                  129
#define IDD_DIALOG_OPTIONS_MAIN         130
#define IDD_DIALOG_MODULES              132
#define IDD_DIALOG_BLOODDRINK           133
#define IDC_CHECK1                      1000
#define IDC_LIST1                       1001
#define IDC_STATIC1                     1002
#define IDC_CHECK2                      1004
#define IDC_HOTKEY1                     1005
#define IDC_EDIT_PATH                   1007
#define IDC_EDIT2                       1007
#define IDC_BUTTON_BROWSE               1008
#define ID_VIEW_OPTIONS                 32771
#define ID_VIEW_LOG                     32774
#define ID_VIEW_MODULES                 32776
#define ID_FILE_RESTART                 32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
