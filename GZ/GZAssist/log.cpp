#include "stdafx.h"
#include "log.h"
#include <string>

std::string GetModuleRunDir();

bool CLog::GetLog()
{
  return m_laststate;
}

void CLog::SetLog(bool b)
{
  m_laststate=b;
  if(b)
  {
    std::string path=GetModuleRunDir()+"gzassist.log";
    m_log.open(path.c_str(),std::ios::out|std::ios::ate|std::ios::app);
    if(!m_log.is_open())
      throw exception(("can't open '"+path+"'").c_str());

    char tmpbuf[128];
    _strdate( tmpbuf );
    m_log<<tmpbuf<<" ";
    Write("// log started {");
  }else
  {
    char tmpbuf[128];
    _strdate( tmpbuf );
    m_log<<tmpbuf<<" ";
    Write("// } log ended\n");
    m_log.close();
  }
}

CLog::~CLog()
{
}

CLog::CLog():m_laststate(false)
{
}
