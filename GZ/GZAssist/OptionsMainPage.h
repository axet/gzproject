#if !defined(AFX_OPTIONSMAINPAGE_H__202672A3_49A6_4171_A266_69EF2C3639F1__INCLUDED_)
#define AFX_OPTIONSMAINPAGE_H__202672A3_49A6_4171_A266_69EF2C3639F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptionsMainPage.h : header file
//

#include <iniext/iniext.h>
#include <atlbase.h>

/////////////////////////////////////////////////////////////////////////////
// COptionsMainPage dialog

struct OptionsMainPage
{
  BOOL savelogfile;
  DWORD hotkey;
  CString clientpath;

  OptionsMainPage():savelogfile(FALSE),hotkey(0)
  {
    CRegKey key(HKEY_LOCAL_MACHINE);
    key.Open(HKEY_LOCAL_MACHINE,"SOFTWARE\\Origin Worlds Online\\Ultima Online\\1.0");
    char buf[1024];
    ULONG bufl=sizeof(buf);
    key.QueryStringValue("ExePath",buf,&bufl);
    clientpath=buf;
  }
  void Load()
  {
    savelogfile=AfxGetApp()->GetProfileInt("main","savelogfile",0)==1;
    hotkey=AfxGetApp()->GetProfileInt("main","hotkey",0);
    clientpath=AfxGetApp()->GetProfileString("main","clientpath",clientpath);
  }
  void Save()
  {
    AfxGetApp()->WriteProfileInt("main","savelogfile",savelogfile);
    AfxGetApp()->WriteProfileInt("main","hotkey",hotkey);
    AfxGetApp()->WriteProfileString("main","clientpath",clientpath);
  }
};

class COptionsMainPage : public CPropertyPage,public OptionsMainPage
{
	DECLARE_DYNCREATE(COptionsMainPage)

// Construction
public:
	COptionsMainPage();
	~COptionsMainPage();

// Dialog Data
	//{{AFX_DATA(COptionsMainPage)
	enum { IDD = IDD_DIALOG_OPTIONS_MAIN };
	CHotKeyCtrl	m_hotkeyctrl;
	BOOL	m_hotkey;
	//}}AFX_DATA

    void OnOK();

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(COptionsMainPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(COptionsMainPage)
	afx_msg void OnCheck2();
    afx_msg void OnButtonBrowse();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIONSMAINPAGE_H__202672A3_49A6_4171_A266_69EF2C3639F1__INCLUDED_)
