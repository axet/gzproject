#pragma once

#include "../GZone/gzunit/gzunit.h"

class CGZAUnit:public CGZUnit
{
public:
  CGZAUnit(const char* l = "",const char* p = "",const char *c = "",bool showwindow = false):CGZUnit(l,p,c,showwindow){};
  void Disconnect();
  void Login(const char* login,const char* password,const char* charname);
  void Connect(const char* p);
};
