#if !defined(AFX_MODULESCONFIGDLG_H__36A6ABD4_BCB2_4018_B547_3B977D5F2E51__INCLUDED_)
#define AFX_MODULESCONFIGDLG_H__36A6ABD4_BCB2_4018_B547_3B977D5F2E51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModulesConfigDlg.h : header file
//

#include <vector>

/////////////////////////////////////////////////////////////////////////////
// CModulesConfigDlg dialog

class CModulesConfigDlg : public CDialog
{
// Construction
  HWND m_prevsel;
public:
  const CGZAssistApp::AssistAddinList* m_addinlist;

	CModulesConfigDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CModulesConfigDlg)
	enum { IDD = IDD_DIALOG_MODULES };
	CCheckListBox	m_list;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModulesConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CModulesConfigDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeList1();
	afx_msg void OnDestroy();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODULESCONFIGDLG_H__36A6ABD4_BCB2_4018_B547_3B977D5F2E51__INCLUDED_)
