// ModulesConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GZAssist.h"
#include "ModulesConfigDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CModulesConfigDlg dialog


CModulesConfigDlg::CModulesConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CModulesConfigDlg::IDD, pParent),m_prevsel(0)
{
	//{{AFX_DATA_INIT(CModulesConfigDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CModulesConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CModulesConfigDlg)
	DDX_Control(pDX, IDC_LIST1, m_list);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CModulesConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CModulesConfigDlg)
	ON_LBN_SELCHANGE(IDC_LIST1, OnSelchangeList1)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CModulesConfigDlg message handlers

BOOL CModulesConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

  for(CGZAssistApp::AssistAddinList::const_iterator i=m_addinlist->begin();i!=m_addinlist->end();i++)
  {
    char buf[1024];
    ::GetWindowText(i->addin->GetWindow(),buf,sizeof(buf));
    int item=m_list.InsertString(m_list.GetCount(),buf);
    m_list.SetItemData(item,reinterpret_cast<DWORD>(&*i));
    m_list.SetCheck(item,i->enabled);
  }

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CModulesConfigDlg::OnSelchangeList1() 
{
	int i=m_list.GetCurSel();
  if(i==-1)
    return;
  CGZAssistApp::AssistAddinItem* item=reinterpret_cast<CGZAssistApp::AssistAddinItem*>(m_list.GetItemData(i));

  if(m_prevsel!=0)
  {
    ::ShowWindow(m_prevsel,SW_HIDE);
    ::SetParent(m_prevsel,0);
  }
  m_prevsel=item->addin->GetWindow();

  ::SetParent(m_prevsel,*this);
  CRect rect;
  GetDlgItem(IDC_STATIC1)->GetWindowRect(&rect);
  ScreenToClient(&rect);
  ::MoveWindow(m_prevsel,rect.left,rect.top,rect.Width(),rect.Height(),TRUE);
  ::SetWindowLong(m_prevsel,GWL_STYLE,WS_CHILD);
  ::ShowWindow(m_prevsel,SW_SHOW);
}

void CModulesConfigDlg::OnDestroy() 
{
  for(int i=0;i<m_list.GetCount();i++)
  {
    CGZAssistApp::AssistAddinItem* item=reinterpret_cast<CGZAssistApp::AssistAddinItem*>(m_list.GetItemData(i));
    item->enabled=m_list.GetCheck(i)==1;
  }

  if(m_prevsel!=0)
  {
    ::ShowWindow(m_prevsel,SW_HIDE);
    ::SetParent(m_prevsel,0);
  }

  CDialog::OnDestroy();
}

void CModulesConfigDlg::OnOK() 
{
	CDialog::OnOK();
}
