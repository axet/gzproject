// GZAssistDoc.cpp : implementation of the CGZAssistDoc class
//

#include "stdafx.h"
#include "GZAssist.h"

#include "GZAssistDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGZAssistDoc

IMPLEMENT_DYNCREATE(CGZAssistDoc, CDocument)

BEGIN_MESSAGE_MAP(CGZAssistDoc, CDocument)
	//{{AFX_MSG_MAP(CGZAssistDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGZAssistDoc construction/destruction

CGZAssistDoc::CGZAssistDoc()
{
	// TODO: add one-time construction code here

}

CGZAssistDoc::~CGZAssistDoc()
{
}

BOOL CGZAssistDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CGZAssistDoc serialization

void CGZAssistDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CGZAssistDoc diagnostics

#ifdef _DEBUG
void CGZAssistDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGZAssistDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGZAssistDoc commands

void CGZAssistDoc::OnCloseDocument() 
{
	CDocument::OnCloseDocument();
}

BOOL CGZAssistDoc::SaveModified() 
{
	SetModifiedFlag(FALSE);
	return CDocument::SaveModified();
}
