#include "stdafx.h"
#include "logctrl.h"

#include "gzassist.h"
#include "iniext/unitlog.h"

CLogCtrl::CLogCtrl()
{
}

CLogCtrl::~CLogCtrl()
{
}

LRESULT CLogCtrl::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
  switch(message)
  {
  case WM_CREATE:
    {
      m_dwSCBStyle|=SCBS_SIZECHILD;
      CSizingControlBarG::WindowProc(message,wParam,lParam);
      m_rich.Create(WS_VSCROLL|ES_READONLY|ES_AUTOVSCROLL|ES_MULTILINE|WS_VISIBLE|WS_BORDER|WS_CHILD,CRect(0,0,0,0),this,0);
      m_font.CreateFont(-10,0,0,0,0,0,0,0,0,0,0,0,0,0);
      m_rich.SetFont(&m_font);

      return TRUE;
    }
    break;
  case WM_DESTROY:
    if(App().m_unitlog.GetLogState())
      App().m_unitlog.SetLogState(false);
    break;
  }
  return CSizingControlBarG::WindowProc(message,wParam,lParam);
}

void CLogCtrl::AddText(const char* p)
{
  m_rich.HideSelection(TRUE,FALSE);
  CHARRANGE cr,cr2;
  m_rich.GetSel(cr);
  m_rich.SetSel(-1,-1);
  m_rich.GetSel(cr2);
  m_rich.ReplaceSel(p);
  m_rich.ReplaceSel("\n");
  m_rich.SetSel(cr);
  m_rich.HideSelection(FALSE,FALSE);
  if(cr.cpMin==cr2.cpMin&&cr.cpMax==cr2.cpMax)
    m_rich.SetSel(-1,-1);
  if(App().m_unitlog.GetLogState())
    App().m_unitlog.Message(p);
}
