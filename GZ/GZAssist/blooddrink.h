#include "blooddrinkdlg.h"
#include "assistaddin.h"
#include <windows.h>

struct BloodDrinkOptions
{
  BloodDrinkOptions():autouseincomingblood(true) {}

  bool autouseincomingblood;
};

class CBloodDrink:public CAssistAddin,private BloodDrinkOptions
{
  CBloodDrinkDlg m_blooddrinkdlg;
  CClientCommands* m_clientcommands;
public:
  void Create(CClientCommands*);
  HWND GetWindow();

  const char* GetName();
  void LogMessage(const char* msg) { ; }
  void FromClient(const void* buf,int bufsize) { ; }
  void FromServer(const void* buf,int bufsize);
  void Exit(unsigned int exitcode) { ; }
};
