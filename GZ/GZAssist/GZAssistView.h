#if !defined(AFX_GZASSISTVIEW_H__618568DB_0F57_4124_AAEC_2AAE2C10E009__INCLUDED_)
#define AFX_GZASSISTVIEW_H__618568DB_0F57_4124_AAEC_2AAE2C10E009__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GZAssistView.h : header file
//

#include <afxrich.h>

/////////////////////////////////////////////////////////////////////////////
// CGZAssistView view

class CGZAssistView : public CEditView
{
protected:
	CGZAssistView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CGZAssistView)

// Attributes
public:

  void AddText(const char*p);

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGZAssistView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CGZAssistView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CGZAssistView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GZASSISTVIEW_H__618568DB_0F57_4124_AAEC_2AAE2C10E009__INCLUDED_)
