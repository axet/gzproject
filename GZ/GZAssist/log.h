#include <fstream>

class CLog
{
  std::fstream m_log;
  bool m_laststate;
public:
  CLog();
  ~CLog();
  void SetLog(bool);
  bool GetLog();
  void Write(const char* p,...)
  {
    char printbuf[16384];
    char tmpbuf[128];
    _strtime( tmpbuf );
    m_log<<tmpbuf<<" ";
    va_list args;
    va_start(args,p);
    _vsnprintf(printbuf,sizeof(printbuf),p,args);
    va_end(args);
    m_log<<printbuf<<std::endl;
  }
};
