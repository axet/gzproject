// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "GZAssist.h"

#include "MainFrm.h"
#include "gzassistview.h"
#include "modulesconfigdlg.h"
#include "gzassist.h"
#include "splash.h"

#include <vector>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_VIEW_OPTIONS, OnViewOptions)
	ON_COMMAND(ID_VIEW_LOG, OnViewLog)
    ON_MESSAGE(WM_USER+2,OnThreadEvent)
	ON_UPDATE_COMMAND_UI(ID_VIEW_LOG, OnUpdateViewLog)
	ON_COMMAND(ID_VIEW_MODULES, OnViewModules)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
  ON_MESSAGE(WM_HOTKEY,OnHotKey)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
  m_hotkey=GlobalAddAtom("gzhotkey");
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

  //ModifyStyle(WS_THICKFRAME|WS_MAXIMIZEBOX,WS_DLGFRAME);

	if (!m_wndToolBar.CreateEx(this) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

  /*
	if (!m_wndDlgBar.Create(this, IDR_MAINFRAME, 
		CBRS_ALIGN_TOP, AFX_IDW_DIALOGBAR))
	{
		TRACE0("Failed to create dialogbar\n");
		return -1;		// fail to create
	}
  */

	if (!m_wndReBar.Create(this) ||
		!m_wndReBar.AddBar(&m_wndToolBar)/* ||
		!m_wndReBar.AddBar(&m_wndDlgBar)*/)
	{
		TRACE0("Failed to create rebar\n");
		return -1;      // fail to create
	}

  if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Remove this if you don't want tool tips
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);

  m_logwindow.Create("Log", this, 123,WS_CHILD|WS_VISIBLE|CBRS_TOP);
  m_logwindow.SetBarStyle(m_logwindow.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

  EnableDocking(CBRS_ALIGN_ANY);

  m_logwindow.EnableDocking(CBRS_ALIGN_ANY);
  DockControlBar(&m_logwindow, AFX_IDW_DOCKBAR_BOTTOM);

  if(App().m_optionsmain.hotkey!=0)
  {
    RegisterHotKey(*this,m_hotkey,HIWORD(App().m_optionsmain.hotkey),LOWORD(App().m_optionsmain.hotkey));
  }

  App().m_gzunit.AddSoldierAdvise(this);
  App().m_gzunit.AddAdvise(this);

  // CG: The following line was added by the Splash Screen component.
  CSplashWnd::ShowSplashScreen(this);
  return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
  cs.style&=~ FWS_ADDTOTITLE ;
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::AppLog(const char* p,...)
{
  char buf[16384];
  va_list args;
  va_start(args,p);
  _vsnprintf(buf,sizeof(buf),p,args);
  va_end(args);

  CGZAssistView* pp=(CGZAssistView*)GetActiveView();
  if(pp!=0)
  {
    CTime time=CTime::GetCurrentTime();
    pp->AddText(time.Format("%H:%M:%S ")+buf);
  }
}

void CMainFrame::ClientLog(const char* p,...)
{
  char buf[16384];
  va_list args;
  va_start(args,p);
  _vsnprintf(buf,sizeof(buf),p,args);
  va_end(args);

  m_logwindow.AddText(buf);
}

void CMainFrame::OnViewOptions() 
{
  COptionsMainPage main;
  ((OptionsMainPage&)main)=App().m_optionsmain;

	CPropertySheet dlg("Options");
  dlg.AddPage(&main);

  UnregisterHotKey(*this,m_hotkey);

  if(dlg.DoModal()!=IDOK)
    goto exit;

  if(App().m_optionsmain.savelogfile!=main.savelogfile)
  {
    App().m_unitlog.SetLogState(main.savelogfile==1);
  }

  App().m_optionsmain=main;

exit:
  RegisterHotKey(*this,m_hotkey,HIWORD(App().m_optionsmain.hotkey),LOWORD(App().m_optionsmain.hotkey));
}

void CMainFrame::OnViewLog() 
{
	ShowControlBar(&m_logwindow,!m_logwindow.IsVisible(),FALSE);
}

void CMainFrame::OnUpdateViewLog(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_logwindow.IsVisible());
}

void CMainFrame::OnViewModules() 
{
  CModulesConfigDlg dlg;

  dlg.m_addinlist=&App().m_addinlist;

  if(dlg.DoModal()!=IDOK)
    return;
}

LRESULT CMainFrame::OnHotKey(WPARAM ,LPARAM) 
{
  BOOL b=IsWindowVisible();
  ShowWindow(b?SW_HIDE:SW_SHOW);

  return 0;
}

void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CFrameWnd::OnShowWindow(bShow, nStatus);

  if(App().m_gzunit.IsConnected())
    App().m_gzunit.ShowWindow(bShow==TRUE);
}

void CMainFrame::ThreadGZUnitEvent(EventThread* p)
{
  PostMessage(WM_USER+2,(WPARAM)p);
}

long CMainFrame::OnThreadEvent(unsigned u,long)
{
  EventThread* p=(EventThread*)u;
  p->ReadEventSoldier();
  return 0;
}

void CMainFrame::ConnectionLost()
{
  AppLog("Client: ConnectionLost");
  App().m_gzunit.Disconnect();
  StartClient();
}

void CMainFrame::LocalConnectionLost()
{
  AppLog("Client: LocalConnectionLost");
  App().m_gzunit.Disconnect();
  StartClient();
}

void CMainFrame::ConnectionEstablished(const char* name,const char* pass, const char* charname)
{
  m_name=name;m_password=pass;m_charname=charname;

  App().m_unitlog.SetLogState(false);
  App().m_unitlog.SetLogFileName(m_name,m_charname,"gzassist");
  if(App().m_optionsmain.savelogfile)
    App().m_unitlog.SetLogState(true);
}
void CMainFrame::LogMessage(const char*p)
{
  ClientLog(p);
}

void CMainFrame::Exit(unsigned int w)
{
  if(w==-1)
  {
    AppLog("Client: 'unhandled exception'");
    StartClient();
  }else
    AfxGetMainWnd()->PostMessage(WM_CLOSE);
}

void CMainFrame::StartClient()
{
  try
  {
    AppLog("GZ: Start client");
    App().m_gzunit.Connect(App().m_optionsmain.clientpath);
    if(!m_name.IsEmpty())
      App().m_gzunit.Login(m_name,m_password,m_charname);
    App().m_gzunit.ShowWindow(true);
  }catch(std::exception &e)
  {
    AppLog(e.what());
  }
}
