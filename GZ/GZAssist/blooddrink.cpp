#include "stdafx.h"
#include "blooddrink.h"
#include "../client/packets.h"
#include <winsock2.h>
#include "../client/clientcommands.h"

#pragma comment(lib,"Ws2_32.lib")

void CBloodDrink::FromServer(const void* param,int paramsize)
{
  /*
  CServer::scsPack* pack=(CServer::scsPack*)param;
  switch(pack->id)
  {
  case CServer::scMoveItem:
    if(autouseincomingblood)
    {
      CServer::scsMoveItem *p=(CServer::scsMoveItem *)pack;
      int itemid=htons(p->Item_ID);
      switch(itemid)
      {
      case 0x0122a:
      case 0x0122b:
      case 0x0122c:
      case 0x0122d:
      case 0x0122e:
      case 0x0122f:
      case 0x01645:
      case 0x01cc7:
      case 0x01cf1:
        CServer::scsUseItem sc;
        sc.itemserial=p->Item_Serial;
        m_clientcommands->SendToServer(&sc,sizeof(sc));
      }
    }
    break;
  }
  */
}

void CBloodDrink::Create(CClientCommands*p)
{
  m_clientcommands=p;
  m_blooddrinkdlg.Create(CBloodDrinkDlg::IDD);
}

HWND CBloodDrink::GetWindow()
{
  return m_blooddrinkdlg;
}

const char* CBloodDrink::GetName()
{
  return "Blood drink";
}
