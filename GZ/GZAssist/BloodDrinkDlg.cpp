// BloodDrinkDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BloodDrinkDlg.h"

#include "assistaddin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBloodDrinkDlg dialog


CBloodDrinkDlg::CBloodDrinkDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBloodDrinkDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBloodDrinkDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CBloodDrinkDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBloodDrinkDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBloodDrinkDlg, CDialog)
	//{{AFX_MSG_MAP(CBloodDrinkDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBloodDrinkDlg message handlers

void CBloodDrinkDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	//CDialog::OnOK();
}

void CBloodDrinkDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	//CDialog::OnCancel();
}
