#ifndef  ASSISTADDIN_H
#define ASSISTADDIN_H

#include <windows.h>
#include "../client/clientevents.h"

class CClientCommands;

class CAssistAddin:public CClientEvents
{
public:
  virtual void Create(CClientCommands*) = 0;
  // получает от аддина заголовок окна, служит в большинстве случаев
  // окном для настроек
  virtual HWND GetWindow() = 0;

  virtual const char* GetName() = 0;
};

#endif
