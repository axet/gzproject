// under cunstraction

class CInterNetMaster
{
  unsigned m_socket;
public:
  CInterNetMaster();
  // postfix - значение аналогичное порту на машине tcp\ip содедения
  //	возможно потребуеться расширить функцию соеденения чтобы она позволяла
  //	работать по сети
  void Connect(const char* address,unsigned port = 48989);
  bool IsConnected();
  void Disconnect();
  void DoProcess();

  void Write(const void* buf,int bufsize);
  // бросает исключение в случае ошибки чтения с сокета
  void Read(unsigned char* buf,int *bufsize);
};
