#ifndef __INTERNETSLAVE_H__
#define __INTERNETSLAVE_H__

#include <winsock2.h>

class CInterNetSlave
{
  CRITICAL_SECTION m_cs;
  uintptr_t m_recvthread;

  bool m_break;

  SOCKET m_socket;
  unsigned m_postfix;

  void Break();
  bool IsBreak();

  // если возвращаеться false Thread чтения выходит
  virtual void Receive(const unsigned char* buf,int bufsize) = 0;
  static void ReceiveThread(CInterNetSlave *p);

protected:
  // когда я получаю пакет с данными сюда заносится сокет отправителя
  // чтобы можно было отправить ему ответ
  // те для работы с функцимями CInterNetSlave::Write & CInterNetSlave::Read
  SOCKET m_currentsocket;

public:
  CInterNetSlave();
  ~CInterNetSlave();
  void Listen(unsigned port = 48989);
  void Disconnect();
  void Write(const void* buf,int size);
  void Read(void* buf,int size);
};

#endif
