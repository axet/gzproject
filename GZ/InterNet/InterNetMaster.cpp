#include "internetmaster.h"

#include <winsock2.h>
#include <exception>

CInterNetMaster::CInterNetMaster():m_socket(0)
{
  WSAData wsa={0};
  WSAStartup(MAKEWORD(2,0),&wsa);
}

void CInterNetMaster::Connect(const char* address,unsigned port)
{
  m_socket=(unsigned)socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
  u_long icmd = 0;   
  int status;
  status=ioctlsocket(m_socket,FIONBIO,&icmd);
  sockaddr_in sin={0};
  sin.sin_family=AF_INET;
  sin.sin_port=htons(port);
  if(hostent *h=gethostbyname(address))
  {
    memcpy(&sin.sin_addr,h->h_addr_list[0],4);
  }
  //bind(m_socket,(sockaddr*)&sin,sizeof(sin));
  status=connect(m_socket,(sockaddr*)&sin,sizeof(sin));
  if(status==-1)
  {
    Disconnect();
	throw std::exception("can't connect");
  }
}

bool CInterNetMaster::IsConnected()
{
  return m_socket!=0;
}

void CInterNetMaster::Disconnect()
{
  if(m_socket!=0)
  {
    closesocket(m_socket);
    m_socket=0;
  }
}
void CInterNetMaster::DoProcess()
{
  ;
}

void CInterNetMaster::Write(const void* buf,int bufsize)
{
  send(m_socket,(char*)buf,bufsize,0);
}

void CInterNetMaster::Read(unsigned char* buf,int *bufsize)
{
  *bufsize=recv(m_socket,(char*)buf,*bufsize,0);
}
