#include "internetslave.h"

#include <process.h>
#include <set>

#include <debugger/debug.h>

void CInterNetSlave::ReceiveThread(CInterNetSlave *p)
{
  CoInitialize(0);

  typedef std::set<SOCKET> connected_t;
  connected_t connected;

  while(!p->IsBreak())
  {
    fd_set read={0};
    if(p->m_socket!=0)
      FD_SET(p->m_socket,&read);
    for(connected_t::iterator i=connected.begin();i!=connected.end();i++)
    {
      FD_SET(*i,&read);
    }
    int status=select(0,&read,0,0,0);
    if(status==-1)
      // не осталось открытых сокетов?
      break;
    for(int i=0;i<status;i++)
    {
      connected_t::iterator con=connected.find(read.fd_array[i]);
      if(con!=connected.end())
      {
        char buf[65535];
        int len=recv(*con,buf,sizeof(buf),0);
        if(len<=0)
        {
          closesocket(*con);
          DBGTRACE("close connection: %08x\n",*con);
          connected.erase(con);
          continue;
        }
        DBGTRACE("%d recv(%08x,%08x,%d)\n",len,*con,buf,sizeof(buf));
        p->m_currentsocket=*con;
        try
        {
          p->Receive((unsigned char*)buf,len);
        }catch(...)
        {
          closesocket(*con);
        }
      }
      if(read.fd_array[i]==p->m_socket)
      {
        sockaddr sa={0};
        int ilen=sizeof(sa);
        SOCKET sock=accept(p->m_socket,&sa,&ilen);
        if(sock==INVALID_SOCKET)
          continue;
        //u_long nonblock=0;
        //ioctlsocket(sock,FIONBIO,&nonblock);
        DBGTRACE("open connection: %08x\n",sock);
        connected.insert(connected.end(),sock);
      }
    }
  }

  for(connected_t::iterator i=connected.begin();i!=connected.end();i++)
  {
    DBGTRACE("close connection: %08x\n",*i);
    closesocket(*i);
  }

  CoUninitialize();
  _endthread();
}

CInterNetSlave::CInterNetSlave()
{
  WSADATA wd;
  WSAStartup(MAKEWORD( 1, 0 ),&wd);

  InitializeCriticalSection(&m_cs);
}

CInterNetSlave::~CInterNetSlave()
{
  Break();
  DeleteCriticalSection(&m_cs);
}

bool CInterNetSlave::IsBreak()
{
  EnterCriticalSection(&m_cs);
  bool b=m_break;
  LeaveCriticalSection(&m_cs);
  return b;
}

void CInterNetSlave::Break()
{
  EnterCriticalSection(&m_cs);
  m_break=true;
  LeaveCriticalSection(&m_cs);
  if(m_recvthread!=0)
  {
    closesocket(m_socket);

    WaitForSingleObject((HANDLE)m_recvthread,-1);
    m_recvthread=0;
  }
}

void CInterNetSlave::Listen(unsigned port)
{
  m_socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
  if(m_socket==INVALID_SOCKET)
	throw std::exception("socket error");
  u_long icmd = 0;   
  int status;
  status=ioctlsocket(m_socket,FIONBIO,&icmd);
  if(status!=0)
	throw std::exception("ioctlsocket error");
  sockaddr_in sin={0};
  sin.sin_family=AF_INET;
  sin.sin_port=htons(port);
  status=bind(m_socket,(sockaddr*)&sin,sizeof(sin));
  if(status!=0)
	throw std::exception("bind error, check port");
  status = listen(m_socket,5);
  if(status!=0)
    throw std::exception("listen error, check port");

  m_break=false;
  m_recvthread=_beginthread((void(*)(void*))ReceiveThread,0,this);
}

void CInterNetSlave::Disconnect()
{
  if(m_socket!=0)
  {
    shutdown(m_socket,SD_BOTH);
    closesocket(m_socket);
    m_socket=0;
  }

  Break();
}

void CInterNetSlave::Write(const void* buf,int size)
{
  //int bytes=send(m_currentsocket,(const char*)buf,size,0);
  //DBGTRACE("%d send(%08x,%08x,%d)\n",bytes,m_currentsocket,buf,size);

  int max=65535;
  int maxsize=sizeof(max);
  getsockopt(m_currentsocket,SOL_SOCKET ,SO_SNDBUF,(char*)&max,&maxsize);

  while(size>0)
  {
    int tosend=size>max?max:size;
    int bytes=send(m_currentsocket,(const char*)buf,tosend,0);
    DBGTRACE("%d send(%08x,%08x,%d)\n",bytes,m_currentsocket,buf,tosend);

    buf=(const char*)buf+tosend;
    size-=tosend;
  }
}

void CInterNetSlave::Read(void* buf,int size)
{
  int bytes=recv(m_currentsocket,(char*)buf,size,0);
  DBGTRACE("%d recv(%08x,%08x,%d)\n",m_currentsocket,bytes,buf,size);
}
