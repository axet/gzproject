#ifndef __INTERPROCSASLAVE_H__
#define __INTERPROCSASLAVE_H__

#include <string>
#include <map>

#include "../internet/internetslave.h"

class CInterNetSASlave:public CInterNetSlave
{
  typedef std::string buffer_t;
  typedef std::map<SOCKET,buffer_t> sockmap_t;
  sockmap_t m_sockmap;

public:
  virtual void Unload() = 0;
  virtual void ModuleCommand(std::string &name,std::string &xmlinput,std::string &xmloutput) = 0;

private:
  virtual void Receive(const unsigned char* buf,int bufsize);
};

#endif
