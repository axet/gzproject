// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__C3682DC9_33C1_460E_866C_B41AE22C0C6F__INCLUDED_)
#define AFX_STDAFX_H__C3682DC9_33C1_460E_866C_B41AE22C0C6F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <Altova/AltovaLib.h>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__C3682DC9_33C1_460E_866C_B41AE22C0C6F__INCLUDED_)
