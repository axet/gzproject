#include "stdafx.h"

#include "../serveraddin/xsd/SchemaServerAddin/SchemaServerAddin.h"

#include <string>

#include "internetsaslave.h"

using namespace ssa;

void CInterNetSASlave::Receive(const unsigned char* buf,int bufsize)
{
  buffer_t& buffer=m_sockmap[m_currentsocket];

  buffer.append(buf,buf+bufsize);

  char packet[]="</Packet>";
  const char* p=strstr(buffer.c_str(),packet);
  if(p!=0)
  {
    p+=sizeof(packet)-1+1;

    std::string::iterator end=buffer.begin()+(p-buffer.c_str());

    std::string xml(buffer.begin(),end);
    buffer.erase(buffer.begin(),end);

    CDocAddin<CSchemaServerAddinDoc> doc;
    CPacketType packet=doc.LoadXML(xml.c_str());

    if(packet.HasAddinUnload())
    {
      Unload();
      return;
    }

    if(packet.HasModuleCommand())
    {
      CModuleCommandType &modulecommand=packet.GetModuleCommand();
      CRequestType &Request=modulecommand.GetRequest();
      tstring data=Request.GetData();//CSchemaHexBinary data;
      std::string xmlinput(data.c_str(),data.c_str()+data.length());
      std::string xmloutput;
      ModuleCommand((tstring)Request.GetName(),xmlinput,xmloutput);
      //respond
      {
        CPacketType packet;
        CModuleCommandType modulecommand;
        CRespondType Respond;
        Respond.AddData(xmloutput.c_str());
        modulecommand.AddRespond(Respond);
        packet.AddModuleCommand(modulecommand);

        CDocAddin<CSchemaServerAddinDoc> doc;
	      doc.SetRootElementName(_T(""), _T("Packet"));
	      //doc.SetSchemaLocation(_T("SchemaServerAddin.xsd")); // optional
        tstring &str=doc.SaveXML(packet);
        CInterNetSlave::Write(str.c_str(),str.length());
      }
      return;
    }
  }

  if(buffer.size()>5*1024*1024)
    buffer.clear();
}
