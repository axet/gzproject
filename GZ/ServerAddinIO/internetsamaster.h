#include "../internet/internetmaster.h"

class CInterNetSAMaster:public CInterNetMaster
{
public:
  enum cmd_tag {cmdUnload};

  void Unload();
  void SendModuleCommand(const char* module,std::string &xmlinput,std::string &xmloutput);
};
