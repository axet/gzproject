#include "stdafx.h"

#include <vector>

#include "../serveraddin/xsd/SchemaServerAddin/SchemaServerAddin.h"

#include "internetsamaster.h"
#include "internetsaslave.h"

using namespace ssa;

void CInterNetSAMaster::Unload()
{
  CPacketType Packet;
  Packet.AddAddinUnload(tstring());

  CDocAddin<CSchemaServerAddinDoc> doc;
	doc.SetRootElementName(_T(""), _T("Packet"));
	//doc.SetSchemaLocation(_T("SchemaServerAddin.xsd")); // optional
  tstring &str=doc.SaveXML(Packet);
  CInterNetMaster::Write(str.c_str(),str.length());
};

void CInterNetSAMaster::SendModuleCommand(const char* module,std::string &xmlinput,std::string &xmloutput)
{
  CPacketType Packet;

  CModuleCommandType modulecommand;
  CRequestType request;
  request.AddName(module);
  request.AddData(xmlinput.c_str());
  modulecommand.AddRequest(request);
  Packet.AddModuleCommand(modulecommand);

  CDocAddin<CSchemaServerAddinDoc> doc;
	doc.SetRootElementName(_T(""), _T("Packet"));
	//doc.SetSchemaLocation(_T("SchemaServerAddin.xsd")); // optional
  tstring &str=doc.SaveXML(Packet);
  CInterNetMaster::Write(str.c_str(),str.length());

  unsigned char buf[65535];
  int bufsize=sizeof(buf);
  CInterNetMaster::Read(buf,&bufsize);
  xmloutput=std::string(buf,buf+bufsize);
}
