// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// GZone.h : main header file for the GZONE application
//

#if !defined(AFX_GZONE_H__CF850E5D_C547_46F3_BBD7_AABF59F95669__INCLUDED_)
#define AFX_GZONE_H__CF850E5D_C547_46F3_BBD7_AABF59F95669__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CGZoneApp:
// See GZone.cpp for the implementation of this class
//

CString LoadString(int i);

#include "gzunit/gzunit.h"

class CGZoneApp : public CWinApp
{
  void SaveUI();
  void LoadUI();

public:
  class UnitList:public UnitList_t
  {
  public:
    ~UnitList()
    {
      Close();
    }

    void Close()
    {
      for(UnitList_t::iterator i=begin();i!=end();i++)
      {
        delete (*i);
      }
      clear();
    }
  };

  virtual BOOL PreTranslateMessage(MSG* pMsg);
  UnitList m_unitlist;

  CGZoneApp();
  CString GetIniPath();
  CString GetIni();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGZoneApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CGZoneApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
  virtual int Run();
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GZONE_H__CF850E5D_C547_46F3_BBD7_AABF59F95669__INCLUDED_)
