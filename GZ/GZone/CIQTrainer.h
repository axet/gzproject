// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if !defined(AFX_CIQTRAINER_H__054D2F33_E275_4D2C_BB4A_2E2554C1CBD3__INCLUDED_)
#define AFX_CIQTRAINER_H__054D2F33_E275_4D2C_BB4A_2E2554C1CBD3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CIQTrainer.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCIQTrainer form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "commanderiq.h"

class CGameMap;

#include "ciqtrainerunit.h"

class CCIQTrainer : public CFormView,public CCommanderIQ,public CClientEvents
{
  enum msgs{msgSetItem=WM_USER+1,msgFinish,msgSetOption};
protected:
  std::string m_unitid;
  CCIQTrainerUnit *m_unit;
  CCommanderIQEvents* m_events;
  CGameMap* m_gamemap;
  CUnit* m_commands;
  unsigned m_useitemser,m_goodsser,m_dropcontainerser,m_backpackser;
  int m_gumpscount;
  short m_gumps[4];

  // когда одну кнопку нажали, другие нужно отжать
  void SetCheck(unsigned id);
  void UpdateUnit();

	CCIQTrainer();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCIQTrainer)

// Form Data
public:
	//{{AFX_DATA(CCIQTrainer)
	enum { IDD = IDD_DIALOG_CIQTRAINER };
	BOOL	m_start;
	BOOL	m_goods;
	BOOL	m_useitem;
	BOOL	m_dropcontainer;
	int		m_portion;
	BOOL	m_backpack;
	BOOL	m_gumpcatch;
	//}}AFX_DATA

// Attributes
public:
  static CCIQTrainer* Create();
  const char* FormGetName();
  HWND FormCreate(HWND parent,CGameMap*p,CCommanderIQEvents*pp);
  HWND FormGet();

  bool UnitAdd(CUnit*);
  bool UnitRemove(CUnit*);
  int UnitGetCount();
  CUnit* UnitGet(int index);
  const char* UnitMissing();

  std::string Save();
  void Load(const char*);

  void SelectedItem(unsigned itemserial);
  void SelectedGround(unsigned x,unsigned y,unsigned z,const Client::ItemType &);
  void SelectedOption(short itemid);
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCIQTrainer)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CCIQTrainer();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	afx_msg long OnSetItem(unsigned,long);
	afx_msg long OnFinish(unsigned,long);
	afx_msg long OnSetOption(unsigned,long);
	// Generated message map functions
	//{{AFX_MSG(CCIQTrainer)
	afx_msg void OnDestroy();
	afx_msg void OnCheckStart();
	afx_msg void OnCheckGoods();
	afx_msg void OnCheckUseItem();
	afx_msg void OnCheckDropContainer();
	afx_msg void OnCheckBackpack();
	afx_msg void OnCheckGumpId();
	afx_msg void OnChangeEditPrtion();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIQTRAINER_H__054D2F33_E275_4D2C_BB4A_2E2554C1CBD3__INCLUDED_)
