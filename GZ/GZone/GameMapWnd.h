// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef MAPWND_H
#define MAPWND_H

#include <list>
#include <vector>
#include <set>
#include <comdef.h>
#include <afxctl.h>

#include "gamemap.h"
#include "picture.h"

class CGameMapWnd:public CWnd,public CGameMap{
public:
  enum type{typeNormal,typeAction,typeNone};

  struct PIT:public ::CPoint{
    PIT():tp(typeNormal),selected(false) {};

    type tp;
    bool selected;
    void* data;
  };

  typedef std::list<PIT> PITLIST;
  typedef std::list<PITLIST> LINELIST;
  typedef std::vector<PIT*> sellist;

private:
  // Задний фон
  CPicture m_dcBG;
  typedef std::set<CGameMapEvents*> adviselist;
  adviselist m_adviselist;

  LINELIST m_linelist;

  // список выделенных точек
  sellist m_sellist;

  // точка над которой весит курсор
  PIT* m_hilightpit;

  // точку которую перетаскивуют мышкой
  PIT* m_movepoint;
  // точку перемещами
  bool m_movepointmoved;

  CBrush m_brush;

  ::CPoint m_ptPickupView;
  BOOL m_bsmothview;
  HCURSOR m_hcurHand;
  HCURSOR m_hcurOld;

  // размер ячейки на экране
  CSize m_cellsize;
  // координаты Экранные
  ::CPoint m_viewpos;
  // точка в мире
  ::CPoint m_cursor;

protected:
	DECLARE_MESSAGE_MAP()
	//{{AFX_MSG(CGameMapWnd)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG


  //подкрашивает новым цветом звависитости от номера строки
  void CreateColorPen(CPen&pen,int line);
  CRect GetPointRect(POINT&);
  void DrawPoint(CDC&dc,POINT&,int type,BOOL bselected,BOOL bhilight);
  void DrawCenter(CDC&dc);
  CRect GetCenterRect(POINT&);
  // получает из координат в мире координаты объекта на экране
  CPoint GetViewCoord(POINT&);
  CPoint GetScaleCoord(POINT&);
  // координаты точки на экране -m_viewpos
  CPoint GetPointCoord(CGameMapWnd::PIT*);
  // коориданты точки в экранных координатах
  CPoint GetPointRealCoord(CGameMapWnd::PIT*);
  void SetViewPos(POINT&);//Доработать с маштабными..
  void EnsureVisiblePoint(CGameMapWnd::PIT*);//Доработать с маштабными..
  POINT GetPoint(CGameMapWnd::PIT*);
  void SetSelect(CGameMapWnd::PIT*,bool sel = true);//когда пользователь нажимает на точке мышкой
  // из координат экрана получаю точку
  PIT* ScreenToPoint(CPoint& pt);
  // из координат экрана получаю координаты точки
  void ScreenToPoint(CPoint& pt,CPoint& ptout);

public:
  CGameMapWnd();
  void DeleteLine(line*);
  void AddAdvise(CGameMapEvents*);
  void RemoveAdvise(CGameMapEvents*);
  void SetBGImage(IPicture *);
  void Create(CWnd*pParentWnd);
  line* AddLine();//добавить новую линию, узаывается координата начала
  point* AddPoint(int x,int y,line* p = 0);//добавляет точку - есть смещение относительно предыдущей точки
  int GetPointCount(int line);//у линии, количество точек
  int GetLineCount();//возвращает кол-во линий
  void SetPointType(PIT*,type type);//устанавливает новый тип у точки (действия или нормальная)
  void SelectPoint(point*,bool center=true,bool clearother = true);//выделяет точку
  //установить координты игрока
  void SetCursor(int x,int y);
  void SetViewPos(int x,int y);
  void Clear();//удаяляет все
  void Translate(point* p,int *line,int*linepos);
  void SetPointData(point *,void*);
  void * GetPointData(point *);
  int GetSelectedPointCount();
  point * GetSelectedPoint(int index);
  line* DeletePoint(point*);
};

class CGameMap::line:public CGameMapWnd::PITLIST
{
public:
};

class CGameMap::point:public CGameMapWnd::PIT{
public:
};

#endif
