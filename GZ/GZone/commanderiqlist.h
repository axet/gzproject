#ifndef COMMANDERIQLIST_H
#define COMMANDERIQLIST_H

#include "commanderiq.h"

#include <vector>

typedef std::vector<CCommanderIQ*> CommanderIQList_t;

class CCommanderIQList:public CommanderIQList_t
{
protected:
  ~CCommanderIQList();
public:
  //грузит с указанного пути, поскольку возможны внешние модули
  //может быть равно нулю
  void Load(const char* path);
  CCommanderIQ* GetCommander(const char*);
};

#endif
