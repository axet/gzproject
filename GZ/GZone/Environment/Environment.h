#include "../JavaExt/UnitNativeManager.h"
#include "../JavaExt/GameMapManager.h"

class COInit
{
public:
  COInit();
  ~COInit();
};

extern UnitNativeManager * g_unitnativemanager;
extern GameMapManager * g_gamemapmanager;

extern "C" __declspec(dllexport) void SetUnitNativeManager(UnitNativeManager * p);
extern "C" __declspec(dllexport) void SetGameMapManager(GameMapManager * p);
