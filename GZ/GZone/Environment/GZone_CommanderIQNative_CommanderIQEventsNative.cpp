#include "stdafx.h"
#include "Environment.h"
#include "GZone_CommanderIQNative_CommanderIQEventsNative.h"

/*
 * Class:     GZone_CommanderIQNative_CommanderIQEventsNative
 * Method:    SetModified
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_GZone_CommanderIQNative_CommanderIQEventsNative_SetModified
  (JNIEnv *env, jobject unitobj, jboolean b)
{
  COInit co;
  try
  {
    CCommanderIQEvents*p=g_unitnativemanager->GetCIQEvents(unitobj);
    p->SetModified(b==JNI_TRUE);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}
