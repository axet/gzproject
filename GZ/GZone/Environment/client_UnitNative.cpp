#include "stdafx.h"
#include "client_UnitNative.h"
#include "Environment.h"

/*
 * Class:     client_UnitNative
 * Method:    AddAdvise
 * Signature: (Lclient/ClientGameEvents;)V
 */
JNIEXPORT void JNICALL Java_client_UnitNative_AddAdvise
  (JNIEnv *env, jobject unitobj, jobject appletobj)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    p->AddAdvise(g_unitnativemanager->GetClientGameEvents(appletobj));
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}

/*
* Class:     client_UnitNative
* Method:    CharMoveTo
* Signature: (Lclient/Types$WorldCord;)V
*/
JNIEXPORT void JNICALL Java_client_UnitNative_CharMoveTo
  (JNIEnv *env, jobject unitobj, jobject wobj)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    p->CharMoveTo(g_unitnativemanager->GetWorldCord(wobj));
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}

/*
 * Class:     client_UnitNative
 * Method:    CharSpeech
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_client_UnitNative_CharSpeech
  (JNIEnv *env, jobject unitobj, jstring buf)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    jboolean bufcopy=JNI_FALSE;
    const jchar* pp=env->GetStringChars(buf,&bufcopy);
    _bstr_t bb((const wchar_t*)pp);
    p->CharSpeech(bb);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}

/*
* Class:     client_UnitNative
* Method:    GetUnitPos
* Signature: ()Lclient/Types$WorldCord;
*/
JNIEXPORT jobject JNICALL Java_client_UnitNative_GetUnitPos
  (JNIEnv *env, jobject unitobj)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    WorldCord w=p->GetUnitPos();

    jclass pointclass=env->FindClass("client/Types$WorldCord");
    if(env->ExceptionOccurred()!=0)
      return 0;
    jmethodID pointinit=env->GetMethodID(pointclass,"<init>","(III)V");
    if(env->ExceptionOccurred()!=0)
      return 0;
    jobject pointobj=env->NewObject(pointclass,pointinit,(jint)w.x,(jint)w.y,(jint)w.z);
    if(env->ExceptionOccurred()!=0)
      return 0;

    return pointobj;
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     client_UnitNative
 * Method:    MoveItem
 * Signature: (III)I
 */
JNIEXPORT jint JNICALL Java_client_UnitNative_MoveItem
  (JNIEnv *env, jobject unitobj, jint i, jint i2 , jint i3)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    return p->MoveItem(i,i2,i3);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     client_UnitNative
 * Method:    MoveItemsG2G
 * Signature: (ILclient/ItemType;I)V
 */
JNIEXPORT void JNICALL Java_client_UnitNative_MoveItemsG2G__ILclient_ItemType_2I
  (JNIEnv *env, jobject unitobj, jint i, jobject itobj, jint i2)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    Client::ItemType it=g_unitnativemanager->GetItemType(itobj);
    p->MoveItemsG2G(i,it,i2);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}

/*
 * Class:     client_UnitNative
 * Method:    MoveItemsG2G
 * Signature: (ILclient/ItemType;II)V
 */
JNIEXPORT void JNICALL Java_client_UnitNative_MoveItemsG2G__ILclient_ItemType_2II
  (JNIEnv *env, jobject unitobj, jint i, jobject itobj, jint i1, jint i2)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    Client::ItemType it=g_unitnativemanager->GetItemType(itobj);
    p->MoveItemsG2G(i,it,i1,i2);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}

/*
 * Class:     client_UnitNative
 * Method:    RemoveAdvise
 * Signature: (Lclient/ClientGameEvents;)V
 */
JNIEXPORT void JNICALL Java_client_UnitNative_RemoveAdvise
  (JNIEnv *env, jobject unitobj, jobject appletobj)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    p->RemoveAdvise(g_unitnativemanager->GetClientGameEvents(appletobj));
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}

/*
 * Class:     client_UnitNative
 * Method:    SelectItem
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_client_UnitNative_SelectItem__I
  (JNIEnv *env, jobject unitobj, jint i)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    return p->SelectItem(i);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     client_UnitNative
 * Method:    SelectItem
 * Signature: (ILclient/ItemType;)I
 */
JNIEXPORT jint JNICALL Java_client_UnitNative_SelectItem__ILclient_ItemType_2
  (JNIEnv *env, jobject unitobj, jint i, jobject itobj)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    Client::ItemType it=g_unitnativemanager->GetItemType(itobj);
    return p->SelectItem(i,it);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     client_UnitNative
 * Method:    SelectItem
 * Signature: (ILclient/ItemType;I)I
 */
JNIEXPORT jint JNICALL Java_client_UnitNative_SelectItem__ILclient_ItemType_2I
  (JNIEnv *env, jobject unitobj, jint i, jobject itobj, jint i1)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    Client::ItemType it=g_unitnativemanager->GetItemType(itobj);
    return p->SelectItem(i,it,i1);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     client_UnitNative
 * Method:    StatsGetHits
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_client_UnitNative_StatsGetHits
  (JNIEnv *env, jobject unitobj)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    return p->StatsGetHits();
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     client_UnitNative
 * Method:    StatsGetMana
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_client_UnitNative_StatsGetMana
  (JNIEnv *env, jobject unitobj)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    return p->StatsGetMana();
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     client_UnitNative
 * Method:    StatsGetStam
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_client_UnitNative_StatsGetStam
  (JNIEnv *env, jobject unitobj)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    return p->StatsGetStam();
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     client_UnitNative
 * Method:    StatsGetWeight
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_client_UnitNative_StatsGetWeight
  (JNIEnv *env, jobject unitobj)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    return p->StatsGetWeight();
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     client_UnitNative
 * Method:    UseItem
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_client_UnitNative_UseItem__I
  (JNIEnv *env, jobject unitobj, jint i)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    _bstr_t s=p->UseItem(i).c_str();
    jstring o=env->NewString((const jchar*)s.GetBSTR(),s.length());
    return o;
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     client_UnitNative
 * Method:    UseItem
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_client_UnitNative_UseItem__II
  (JNIEnv *env, jobject unitobj, jint i, jint i2)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    _bstr_t s=p->UseItem(i,i2).c_str();
    jstring o=env->NewString((const jchar*)s.GetBSTR(),s.length());
    return o;
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
* Class:     client_UnitNative
* Method:    UseItem
* Signature: (ILclient/Types$WorldCord;Lclient/ItemType;)Ljava/lang/String;
*/
JNIEXPORT jstring JNICALL Java_client_UnitNative_UseItem__ILclient_Types_00024WorldCord_2Lclient_ItemType_2
  (JNIEnv *env, jobject unitobj, jint i, jobject wobj, jobject itobj)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);
    Client::ItemType it=g_unitnativemanager->GetItemType(itobj);
    WorldCord w=g_unitnativemanager->GetWorldCord(wobj);
    _bstr_t s=p->UseItem(i,w,it).c_str();
    jstring o=env->NewString((const jchar*)s.GetBSTR(),s.length());
    return o;
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     client_UnitNative
 * Method:    UseItemGump
 * Signature: (ILclient/Types/UnsignedList;Lclient/Types/ShortList;)I
 */
JNIEXPORT jint JNICALL Java_client_UnitNative_UseItemGump
  (JNIEnv *env, jobject unitobj, jint i, jobject ulobj, jobject slobj)
{
    COInit co;
  try
  {
    CUnit*p=g_unitnativemanager->GetCPPUnit(unitobj);

    UnsignedList ul=g_unitnativemanager->GetUnsignedList(ulobj);
    ShortList sl=g_unitnativemanager->GetShortList(slobj);

    return p->UseItemGump(i,ul,sl);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}
