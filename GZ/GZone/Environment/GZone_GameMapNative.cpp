#include "stdafx.h"
#include "Environment.h"
#include "GZone_GameMapNative.h"


/*
 * Class:     GZone_GameMapNative
 * Method:    AddAdvise
 * Signature: (LGZone/GameMap/GameMapEvents;)V
 */
JNIEXPORT void JNICALL Java_GZone_GameMapNative_AddAdvise
  (JNIEnv *env, jobject gamemap, jobject ptr)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemap);
    CGameMapEvents*e=g_gamemapmanager->GetCPPEvents(ptr);
    p->AddAdvise(e);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}

/*
 * Class:     GZone_GameMapNative
 * Method:    AddPoint
 * Signature: (IILGZone/GameMap/line;)LGZone/GameMap/point;
 */
JNIEXPORT jobject JNICALL Java_GZone_GameMapNative_AddPoint
  (JNIEnv *env, jobject gamemap, jint x, jint y, jobject lineobj)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemap);
    CGameMap::point *pp=p->AddPoint((int)x,(int)y,g_gamemapmanager->GetLine(lineobj));
    return g_gamemapmanager->AddPoint(pp);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}


/*
 * Class:     GZone_GameMapNative
 * Method:    DeleteLine
 * Signature: (LGZone/GameMap/line;)V
 */
JNIEXPORT void JNICALL Java_GZone_GameMapNative_DeleteLine
  (JNIEnv *env, jobject gamemapobj, jobject lineobj)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemapobj);
    CGameMap::line* line=g_gamemapmanager->GetLine(lineobj);
    g_gamemapmanager->DeleteLine(line);
    p->DeleteLine(line);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}


/*
 * Class:     GZone_GameMapNative
 * Method:    DeletePoint
 * Signature: (LGZone/GameMap/point;)LGZone/GameMap/line;
 */
JNIEXPORT jobject JNICALL Java_GZone_GameMapNative_DeletePoint
  (JNIEnv *env, jobject gamemapobj, jobject pointobj)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemapobj);
    CGameMap::point* point=g_gamemapmanager->GetPoint(pointobj);
    g_gamemapmanager->DeletePoint(point);
    CGameMap::line* line=p->DeletePoint(point);
    return g_gamemapmanager->GetLine(line);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}


/*
 * Class:     GZone_GameMapNative
 * Method:    GetPointData
 * Signature: (LGZone/GameMap/point;)I
 */
JNIEXPORT jint JNICALL Java_GZone_GameMapNative_GetPointData
  (JNIEnv *env, jobject gamemapobj, jobject pointobj)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemapobj);
    CGameMap::point* point=g_gamemapmanager->GetPoint(pointobj);
    jint i=(jint)(int)p->GetPointData(point);
    return i;
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}


/*
 * Class:     GZone_GameMapNative
 * Method:    RemoveAdvise
 * Signature: (LGZone/GameMap/GameMapEvents;)V
 */
JNIEXPORT void JNICALL Java_GZone_GameMapNative_RemoveAdvise
  (JNIEnv *env, jobject gamemap, jobject ptr)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemap);
    CGameMapEvents*e=g_gamemapmanager->GetCPPEvents(ptr);
    p->RemoveAdvise(e);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}


/*
 * Class:     GZone_GameMapNative
 * Method:    SelectPoint
 * Signature: (LGZone/GameMap/point;ZZ)V
 */
JNIEXPORT void JNICALL Java_GZone_GameMapNative_SelectPoint
  (JNIEnv *env, jobject gamemapobj, jobject pointobj, jboolean b, jboolean b2)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemapobj);
    p->SelectPoint(g_gamemapmanager->GetPoint(pointobj),b,b2);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}


/*
 * Class:     GZone_GameMapNative
 * Method:    SetPointData
 * Signature: (LGZone/GameMap/point;I)V
 */
JNIEXPORT void JNICALL Java_GZone_GameMapNative_SetPointData
  (JNIEnv *env, jobject gamemapobj, jobject pointobj, jint i)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemapobj);
    CGameMap::point* point=g_gamemapmanager->GetPoint(pointobj);
    p->SetPointData(point,(void*)i);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}


/*
 * Class:     GZone_GameMapNative
 * Method:    GetSelectedPoint
 * Signature: (I)LGZone/GameMap/point;
 */
JNIEXPORT jobject JNICALL Java_GZone_GameMapNative_GetSelectedPoint
  (JNIEnv *env, jobject gamemapobj, jint i)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemapobj);
    return g_gamemapmanager->GetPoint(p->GetSelectedPoint(i));
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     GZone_GameMapNative
 * Method:    GetSelectedPointCount
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_GZone_GameMapNative_GetSelectedPointCount
  (JNIEnv *env, jobject gamemapobj)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemapobj);
    return p->GetSelectedPointCount();
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     GZone_GameMapNative
 * Method:    Translate
 * Signature: (LGZone/GameMap/point;)Ljava/awt/Point;
 */
JNIEXPORT jobject JNICALL Java_GZone_GameMapNative_Translate
  (JNIEnv *env, jobject gamemapobj, jobject pointobj)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemapobj);
    CGameMap::point *pp=g_gamemapmanager->GetPoint(pointobj);

    int x,y;
    p->Translate(pp,&x,&y);

    jclass pointclass=env->FindClass("java/awt/Point");
    if(env->ExceptionOccurred()!=0)
      return 0;
    jmethodID pointinit=env->GetMethodID(pointclass,"<init>","(II)V");
    if(env->ExceptionOccurred()!=0)
      return 0;
    pointobj=env->NewObject(pointclass,pointinit,(jint)x,(jint)y);
    if(env->ExceptionOccurred()!=0)
      return 0;


    return pointobj;
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     GZone_GameMapNative
 * Method:    AddLine
 * Signature: ()LGZone/GameMap/line;
 */
JNIEXPORT jobject JNICALL Java_GZone_GameMapNative_AddLine
  (JNIEnv *env, jobject gamemap)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemap);
    CGameMap::line *pp=p->AddLine();
    return g_gamemapmanager->AddLine(pp);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
    return 0;
  }
}

/*
 * Class:     GZone_GameMapNative
 * Method:    Clear
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_GZone_GameMapNative_Clear
  (JNIEnv *env, jobject gamemap)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemap);
    p->Clear();
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}

/*
 * Class:     GZone_GameMapNative
 * Method:    SetCursor
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_GZone_GameMapNative_SetCursor
  (JNIEnv *env, jobject gamemap, jint x , jint y)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemap);
    p->SetCursor(x,y);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}

/*
* Class:     GZone_GameMapNative
* Method:    SetView
* Signature: (II)V
*/
JNIEXPORT void JNICALL Java_GZone_GameMapNative_SetViewPos
(JNIEnv *env, jobject gamemap, jint x , jint y)
{
  COInit co;
  try
  {
    CGameMap*p=g_gamemapmanager->GetCPPMap(gamemap);
    p->SetViewPos(x,y);
  }catch(std::exception &e)
  {
    JavaException::TrhowJavaException(env,e.what());
  }
}
