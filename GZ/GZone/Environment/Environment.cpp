// Environment.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "Environment.h"

UnitNativeManager * g_unitnativemanager;
GameMapManager *g_gamemapmanager;

extern "C" __declspec(dllexport) void SetUnitNativeManager(UnitNativeManager * p)
{
  g_unitnativemanager=p;
}

extern "C" __declspec(dllexport) void SetGameMapManager(GameMapManager * p)
{
  g_gamemapmanager=p;
}

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

COInit::COInit()
{
  CoInitialize(0);
}

COInit::~COInit()
{
  CoUninitialize();
}
