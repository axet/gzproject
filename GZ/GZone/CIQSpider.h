// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if !defined(AFX_CIQSPIDER_H__F70D35E3_9F1F_4D70_AB7C_1C03750AB01E__INCLUDED_)
#define AFX_CIQSPIDER_H__F70D35E3_9F1F_4D70_AB7C_1C03750AB01E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CIQSpider.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCIQSpider form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "commanderiq.h"

class CCIQSpider : public CFormView,public CCommanderIQ
{
  const char* FormGetName();

protected:
	CCIQSpider();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCIQSpider)

// Form Data
public:
	//{{AFX_DATA(CCIQSpider)
	enum { IDD = IDD_DIALOG_CIQSPIDER };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:
  static CCIQSpider* Create();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCIQSpider)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CCIQSpider();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CCIQSpider)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIQSPIDER_H__F70D35E3_9F1F_4D70_AB7C_1C03750AB01E__INCLUDED_)
