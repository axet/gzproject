// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// CIQGoodsUnit.cpp : implementation file
//

#include "stdafx.h"
#include "gzone.h"
#include "CIQGoodsUnit.h"
#include "ciqGoods.h"
#include "gamemap.h"
#include "AutoCloseDialog.h"

#include <errorreport/errorreport.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCIQGoodsUnit

IMPLEMENT_DYNCREATE(CCIQGoodsUnit, CWinThread)

CCIQGoodsUnit::CCIQGoodsUnit()
{
}

CCIQGoodsUnit::~CCIQGoodsUnit()
{
}

BOOL CCIQGoodsUnit::InitInstance()
{
	AfxOleInit();

	return TRUE;
}

int CCIQGoodsUnit::ExitInstance()
{
	AfxOleTerm();

	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CCIQGoodsUnit, CWinThread)
  ON_THREAD_MESSAGE(msgStart,OnStart)
	//{{AFX_MSG_MAP(CCIQGoodsUnit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIQGoodsUnit message handlers

bool CCIQGoodsUnit::IsGhost()
{
  int hits=m_commands->StatsGetHits();
  return hits == 0;
}

bool CCIQGoodsUnit::IsOverloaded()
{
  int weight=m_commands->StatsGetWeight();
  // -1 может быть тогда когда на клиента с сервера эта информация еще не приходила
  // клиент только загружен..

  //if(weight==-1)
  //  throw exception(LoadString(IDS_NOCONNECTIONTOCLIENT));
  return weight>=150;
}

void CCIQGoodsUnit::Harvest()
{
  CCIQGoods::point *pt=m_pt;

movetocurrenttarget:
  try
  {
    if(m_catchplayercords)
      m_commands->CharMoveTo(WorldCord(pt->px,pt->py,pt->pz));
    else
      m_commands->CharMoveTo(WorldCord(pt->x,pt->y,pt->z));
  }catch(ClientGameCommands::LocalOperationTimeout&)
  {
    Sleep(1000);
    return;
  }

loop:
  {
    std::string msg;
    try
    {
      unsigned gid=m_commands->GetEquippedItem(m_harvestuseitemtype,m_charname);
      if(gid==0)
        gid=m_commands->GetContainerItem(m_harvestuseitemtype,m_backpackser);

      msg=m_commands->UseItem(gid,WorldCord(pt->x,pt->y,pt->z),pt->itemid);
    }catch(ClientGameCommands::LocalOperationTimeout&)
    {
      Sleep(5000);
      return;
    }

    // неудачный результат при наличии ресурса

    // это может происходит если игрок слижком часто кликает на гору.
    // нужно немного подождать а змет продожлжить добычу
    // даже не неменого а долго, поскольку пока это сообщение клиент читает
    // игрок продолжает копать, нужно как минимум чтобы игрок это закончил делать

    // You loosen some rocks but fail to find any useable ore.
    if(stricmp(msg.c_str(),"You loosen some rocks but fail to find any useable ore.")==0)
    {
      Sleep(10000);
      goto loop;
    }
    // You hack at the tree for a while, but fail to produce any useable wood.
    if(stricmp(msg.c_str(),"You hack at the tree for a while, but fail to produce any useable wood.")==0)
    {
      Sleep(10000);
      goto loop;
    }
    // You fish a while, but fail to catch anything.
    if(stricmp(msg.c_str(),"You fish a while, but fail to catch anything.")==0)
    {
      Sleep(10000);
      goto loop;
    }

    // Конец ресурса

    // There is no ore here to mine.
    if(strstr(msg.c_str(),"There is no ore here to mine.")!=0)
      return;
    // There are no logs here to chop.
    if(strstr(msg.c_str(),"There are no logs here to chop.")!=0)
      return;
    // There are no fish here.
    if(strstr(msg.c_str(),"There are no fish here.")!=0)
      return;

    // Вес

    // You put the logs at your feet. It is too heavy..
    // You put the Iron Ore at your feet. It is too heavy..
    if(strstr(msg.c_str(),"It is too heavy..")!=0)
    {
      return;
    }

    // Успех

    // You put the Iron Ore in your pack
    if(strstr(msg.c_str(),"You put")!=0)
      goto loop;
    //You pull out a fish!
    if(strstr(msg.c_str(),"You pull out a fish!")!=0)
      goto loop;

    return;
  };
}

void CCIQGoodsUnit::Goods()
{
  unsigned id=m_commands->PinchItem(m_goodsitemtype,m_backpackser,m_count);

  unsigned gid=m_commands->GetEquippedItem(m_makeuseitemtype,m_charname);
  if(gid==0)
    gid=m_commands->GetContainerItem(m_makeuseitemtype,m_backpackser);

  UnsignedList what;
  what.push_back(id);
  m_commands->UseItemGump(gid,what,m_makeitemtype);
}

void CCIQGoodsUnit::Sell()
{
  m_commands->CharMoveToChar(m_vendorname);

  ClientGameCommands::Vendors::Items items;
  ClientGameCommands::Vendors::Item item;
  item.amount=1;
  item.item=m_makeitemtype;
  items.push_back(item);
  m_commands->VendorOffer(m_charname,items);
}

void CCIQGoodsUnit::OnStart(unsigned uipt,long)
{
  m_pt=(CCIQGoods::point*)uipt;

  try
  {
    //Harvest();
    for(int i=0;i<5;i++)
      Goods();
    Sell();
    goto getnexttarget;

  }catch(ClientGameCommands::LocalOperationTimeout &)
  {
    goto getnexttarget;
  }catch(COutOfProcMaster::ExecuteException&e)
  {
    CAutoCloseDialog dlg(ErrorReport("Client Side Execute Exception",e.what()),CWnd::FromHandle(m_parent),true);
    if(dlg.DoModal()==IDOK)
      goto getnexttarget;
    else
      goto exit;
  }catch(ExecuteAbort&)
  {
    goto exit;
  }catch(std::exception &e)
  {
    CAutoCloseDialog dlg(ErrorReport("Client Side Execute Exception",e.what()),CWnd::FromHandle(m_parent),true);
    if(dlg.DoModal()==IDOK)
      goto getnexttarget;
    else
      goto exit;
  }
  catch(_com_error &e)
  {
    AfxMessageBox(ErrorReport("Goods",e));
    goto exit;
  }

  AfxMessageBox("harvest terminated");

exit:
  ::PostMessage(m_parent,WM_USER+5,0,0);
  return;

getnexttarget:
  ASSERT(m_parent!=0);
  ::PostMessage(m_parent,WM_USER+1,0,0);
  return;
}

void CCIQGoodsUnit::Create(HWND parent,CUnit* p)
{
  m_parent=parent;
  m_commands=p;
}
