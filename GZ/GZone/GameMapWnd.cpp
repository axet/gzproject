// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "stdafx.h"
#include "gamemapwnd.h"
#include <assert.h>

CGameMapWnd::CGameMapWnd():m_movepoint(0)
{
  ;
}

BEGIN_MESSAGE_MAP(CGameMapWnd, CWnd)
	//{{AFX_MSG_MAP(CGameMapWnd)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_RBUTTONDOWN()
    ON_WM_RBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CGameMapWnd::SetBGImage(IPicture *p)
{
  m_dcBG.Create(p);
}

void CGameMapWnd::Create(CWnd*pParentWnd)
{
  m_cellsize=CSize(10,10);

  m_viewpos=::CPoint(0,0);
  m_cursor=::CPoint(0,0);

  m_hcurHand=LoadCursor(0,IDC_SIZEALL);
  m_brush.CreateSolidBrush(RGB(255,255,255));
  CWnd::Create(AfxRegisterWndClass(0,LoadCursor(0,IDC_ARROW),m_brush,0),"",WS_VISIBLE|WS_CHILD,CRect(0,0,0,0),pParentWnd,0);

  Clear();
}

CGameMap::line* CGameMapWnd::AddLine()
{
  LINELIST::iterator i=m_linelist.insert(m_linelist.end(),PITLIST());
  return (CGameMap::line*)&*i;
}

CGameMap::point* CGameMapWnd::AddPoint(int x,int y,CGameMap::line* line)
{
  if(line==0)
    line=AddLine();
  PITLIST::iterator i=line->insert(line->end(),PIT());
  i->x=x;
  i->y=y;

  RedrawWindow();

  return (CGameMap::point*)&*i;
}

void CGameMapWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting

  int count=0;
  for(LINELIST::iterator i=m_linelist.begin();i!=m_linelist.end();i++)
  {
    if(i->empty())
      continue;

    CPen pen;
    CreateColorPen(pen,count);
    dc.MoveTo(GetPointCoord(&(*i->begin())));
    CPen*ppen=dc.SelectObject(&pen);
    for(PITLIST::iterator k=i->begin();k!=i->end();k++)
    {
      CPoint pt=GetPointCoord(&*k);
      DrawPoint(dc,pt,k->tp,k->selected,m_hilightpit==&*k);
      dc.LineTo(pt);
    }
    dc.SelectObject(ppen);

    count++;
  }
  DrawCenter(dc);
}

void CGameMapWnd::SetSelect(PIT* p,bool sel)
{
  if(sel)
  {
    for(adviselist::iterator i=m_adviselist.begin();i!=m_adviselist.end();i++)
      (*i)->PointSelect(static_cast<point*>(p));

    m_sellist.push_back(p);
  }else
  {
    for(int i=0;i<m_sellist.size();i++)
    {
      if(m_sellist[i]==p)
        m_sellist.erase(m_sellist.begin()+i);
    }
  }
  p->selected=sel;
}

CPoint CGameMapWnd::GetScaleCoord(POINT&pt){
  CPoint t(pt);
  t.x*=m_cellsize.cx;
  t.x+=m_cellsize.cx/2;
  t.y*=m_cellsize.cy;
  t.y+=m_cellsize.cy/2;
  return t;
}

CPoint CGameMapWnd::GetViewCoord(POINT&pt){
  CPoint t(pt);
  t-=m_viewpos;
  return t;
}

CPoint CGameMapWnd::GetPointCoord(CGameMapWnd::PIT* p)
{
  return GetViewCoord(GetPointRealCoord(p));
}

CPoint CGameMapWnd::GetPointRealCoord(CGameMapWnd::PIT*p)
{
  return GetScaleCoord(*p);
}

void CGameMapWnd::DrawPoint(CDC&dc,POINT&pt,int type,BOOL bselected,BOOL bhilight){
  if(type==typeNone)
    return;

  CPen pen;
  if(!bhilight){
    switch(type){
    case typeNormal:
      pen.CreatePen(PS_SOLID,2,RGB(0,0,0));
      break;
    case typeAction:
      pen.CreatePen(PS_SOLID,2,RGB(255,0,255));
      break;
    }
  }else{
    pen.CreatePen(PS_SOLID,2,RGB(255,0,0));
  }
  if(bselected){
    CPen pen;
    pen.CreatePen(PS_SOLID,8,RGB(0,0,255));
    CPen *ppen=dc.SelectObject(&pen);
    dc.Ellipse(GetPointRect(pt));
    dc.SelectObject(ppen);
  }

  CPen *pbak=dc.SelectObject(&pen);
  dc.Ellipse(GetPointRect(pt));
  dc.SelectObject(pbak);
}

CRect CGameMapWnd::GetPointRect(POINT&pt){
  CRect rect;
  rect.left=pt.x-m_cellsize.cx/2;
  rect.right=pt.x+m_cellsize.cx/2;
  rect.top=pt.y-m_cellsize.cy/2;
  rect.bottom=pt.y+m_cellsize.cy/2;
  return rect;
}

void CGameMapWnd::CreateColorPen(CPen&pen,int line){
  switch(line){
  case 0:
    pen.CreatePen(PS_SOLID,1,RGB(0,0,0));
    break;
  case 1:
    pen.CreatePen(PS_SOLID,1,RGB(0,255,255));
    break;
  }
}

void CGameMapWnd::Translate(point* p,int *x,int*y)
{
  *x=p->x;
  *y=p->y;
}

void CGameMapWnd::SelectPoint(point* pt,bool bCenter,bool clearother)
{
  if(clearother)
  {
    for(LINELIST::iterator i=m_linelist.begin();i!=m_linelist.end();i++)
    {
      for(PITLIST::iterator k=i->begin();k!=i->end();k++)
        SetSelect(&*k,false);
    }
  }
  SetSelect(pt);
  if(bCenter)
    SetViewPos(GetPointRealCoord(static_cast<PIT*>(pt)));

  RedrawWindow();
}

void CGameMapWnd::SetViewPos(POINT&pt){
  CRect rect;
  GetClientRect(rect);
  m_viewpos=CPoint(pt.x-rect.Width()/2,pt.y-rect.Height()/2);
}

void CGameMapWnd::SetPointType(PIT* p,type type)
{
  p->tp=type;
}

void CGameMapWnd::EnsureVisiblePoint(PIT*pit){
  CRect rect;
  GetClientRect(rect);
  CPoint pt=GetPointCoord(pit);
  if(!rect.PtInRect(pt)){
    if(pt.y<rect.top)
      m_viewpos.y-=rect.top-pt.y;
    if(pt.x<rect.left)
      m_viewpos.x-=rect.left-pt.x;
    if(pt.y>rect.bottom)
      m_viewpos.y+=pt.y-rect.bottom;
    if(pt.x>rect.right)
      m_viewpos.x+=pt.x-rect.right;
  }
}

void CGameMapWnd::Clear(){
  m_bsmothview=0;
  m_hilightpit=0;

  m_linelist.erase(m_linelist.begin(),m_linelist.end());
}

void CGameMapWnd::SetViewPos(int x,int y)
{
  SetViewPos(GetScaleCoord(CPoint(x,y)));
  RedrawWindow();
}

void CGameMapWnd::DrawCenter(CDC&dc)
{
  CPen pen;
  pen.CreatePen(PS_SOLID,2,RGB(255,180,0));
  CPen *ppen=dc.SelectObject(&pen);
  dc.Ellipse(GetCenterRect(GetViewCoord(GetScaleCoord(m_cursor))));
  dc.SelectObject(ppen);
  CString str;
  str.Format("%d,%d",m_cursor.x,m_cursor.y);
  dc.TextOut(0,0,str);
}

void CGameMapWnd::SetCursor(int x,int y)
{
  m_cursor=CPoint(x,y);
  SetViewPos(x,y);
  RedrawWindow();
}

CRect CGameMapWnd::GetCenterRect(POINT&pt){
  CRect rect;
  rect.left=pt.x-3;
  rect.right=pt.x+3;
  rect.top=pt.y-3;
  rect.bottom=pt.y+3;
  return rect;
}

POINT CGameMapWnd::GetPoint(PIT* p)
{
  return *p;
}

BOOL CGameMapWnd::OnEraseBkgnd(CDC* pDC) 
{
  if(m_dcBG.IsValid())
  {
    CRect rect;
    GetClientRect(&rect);
    div_t imagex=div((int)m_viewpos.x,(int)m_cellsize.cx);
    div_t imagey=div((int)m_viewpos.y,(int)m_cellsize.cy);
    div_t imagecx=div((int)rect.Width(),(int)m_cellsize.cx);
    imagecx.quot+=1;
    div_t imagecy=div((int)rect.Height(),(int)m_cellsize.cy);
    imagecy.quot+=1;

    int width=rect.Width()-imagex.rem;
    int height=rect.Height()-imagey.rem;

    m_dcBG.Render(pDC,
      CRect(-imagex.rem,-imagey.rem,imagecx.quot*m_cellsize.cx-imagex.rem,imagecy.quot*m_cellsize.cy-imagey.rem),
      CRect(imagex.quot,imagey.quot,imagex.quot+imagecx.quot,imagey.quot+imagecy.quot));

    //GetClientRect(rect);

    //m_dcBG.Render(pDC,
    //  rect,
    //  CRect(500,955,586,1022));

    //pDC->StretchBlt(
    //  -imagex.rem,-imagey.rem,imagecx.quot*m_cellsize.cx,imagecy.quot*m_cellsize.cy,
    //  dcBG,imagex.quot,imagey.quot,imagecx.quot,imagecy.quot,
    //  SRCCOPY);

    if(imagex.quot<0&&0)
    {
      /*
      // изображение которое дложно печататься слева не на весь экран
      BITMAP bm;
      bm.bmWidth=m_dcBG.GetImageSize().cx;
      bm.bmHeight=m_dcBG.GetImageSize().cy;

      div_t limagex=imagex;
      limagex.quot=bm.bmWidth+imagex.quot;
      div_t limagey=imagey;
      //limagey.quot=bm.bmHeight+imagey.quot;
      div_t limagecx={0};
      limagecx.quot=-imagex.quot;
      div_t limagecy=imagecy;

      m_dcBG.Render(pDC,
        CRect(-limagex.rem,-limagey.rem,limagecx.quot*m_cellsize.cx,limagecy.quot*m_cellsize.cy),
        CRect(limagex.quot,limagey.quot,limagecx.quot,limagecy.quot));
      //pDC->StretchBlt(
      //  -limagex.rem,-limagey.rem,limagecx.quot*m_cellsize.cx,limagecy.quot*m_cellsize.cy,
      //  m_dcBG,
      //  limagex.quot,limagey.quot,limagecx.quot,limagecy.quot,
      //  SRCCOPY);
      */
    }

    return 1;
  }else
    return CWnd::OnEraseBkgnd(pDC);
}

void CGameMapWnd::AddAdvise(CGameMapEvents*p)
{
  m_adviselist.insert(p);
}

void CGameMapWnd::RemoveAdvise(CGameMapEvents*p)
{
  m_adviselist.erase(m_adviselist.find(p));
}

void CGameMapWnd::DeleteLine(line* l)
{
  for(LINELIST::iterator i=m_linelist.begin();i!=m_linelist.end();i++)
  {
    if(&*i==l)
    {
      m_linelist.erase(i);
      break;
    }
  }
  RedrawWindow();
}

void CGameMapWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
  if(m_movepoint)
  {
    if(m_movepointmoved)
    {
      for(adviselist::iterator i=m_adviselist.begin();i!=m_adviselist.end();i++)
        (*i)->PointMoveAfter(static_cast<CGameMapWnd::point*>(m_movepoint));
    }

	  ReleaseCapture();
    m_movepointmoved=false;
    m_movepoint=0;
  }
	
	CWnd::OnLButtonUp(nFlags, point);
}

CGameMapWnd::PIT* CGameMapWnd::ScreenToPoint(CPoint& mousept)
{
  for(LINELIST::iterator i=m_linelist.begin();i!=m_linelist.end();i++)
  {
    for(PITLIST::iterator k=i->begin();k!=i->end();k++)
    {
      if(GetPointRect(GetPointCoord(&*k)).PtInRect(mousept))
      {
        return &*k;
      }
    }
  }
  return 0;
}

void CGameMapWnd::ScreenToPoint(CPoint& mousept,CPoint& out)
{
  out=CPoint((mousept.x+m_viewpos.x)/m_cellsize.cx,(mousept.y+m_viewpos.y)/m_cellsize.cy);
}

void CGameMapWnd::OnLButtonDown(UINT nFlags, CPoint mousept) 
{
  PIT *pt=ScreenToPoint(mousept);
  if(pt!=0)
  {
    SelectPoint(static_cast<point*>(pt),false);
    for(adviselist::iterator i=m_adviselist.begin();i!=m_adviselist.end();i++)
    {
      if(!(*i)->PointMoveBefore(static_cast<point*>(pt)))
        goto nopickup;
    }
    m_movepoint=pt;
    m_movepointmoved=false;
    SetCapture();
nopickup:
    RedrawWindow();
  }

	CWnd::OnLButtonDown(nFlags, mousept);
}

void CGameMapWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
  if(m_movepoint)
  {
    CPoint ptout;
    ScreenToPoint(point,ptout);
    if(m_movepoint->x!=ptout.x||m_movepoint->y!=ptout.y)
    {
      m_movepointmoved=true;
      m_movepoint->x=ptout.x;
      m_movepoint->y=ptout.y;
    }
    RedrawWindow();
  }else if(m_bsmothview)
  {
    m_viewpos+=m_ptPickupView-point;
    m_ptPickupView=point;
    RedrawWindow();
  }else
  {
    PIT* p=ScreenToPoint(point);
    if(m_hilightpit!=p)
    {
      m_hilightpit=p;
      RedrawWindow();
    }
  }
	
	CWnd::OnMouseMove(nFlags, point);
}

void CGameMapWnd::OnMButtonDown(UINT nFlags, CPoint point) 
{
  CRect rect;
  GetClientRect(rect);
  CPoint pt=m_viewpos+point;
  SetViewPos(pt);
  RedrawWindow();

  CWnd::OnRButtonDown(nFlags, point);
}

void CGameMapWnd::OnMButtonUp(UINT nFlags, CPoint point) 
{
}

void CGameMapWnd::OnRButtonDown(UINT nFlags, CPoint point) 
{
  m_bsmothview=TRUE;
  m_ptPickupView=point;
  m_hcurOld=::SetCursor(m_hcurHand);
  SetCapture();

  CWnd::OnMButtonDown(nFlags, point);
}

void CGameMapWnd::OnRButtonUp(UINT nFlags, CPoint point) 
{
  m_bsmothview=FALSE;
  m_hcurOld=::SetCursor(m_hcurOld);
  ReleaseCapture();

  CWnd::OnMButtonUp(nFlags, point);
}

void CGameMapWnd::SetPointData(point * p,void* pp)
{
  p->data=pp;
}

void* CGameMapWnd::GetPointData(point *p)
{
  return p->data;
}

int CGameMapWnd::GetSelectedPointCount()
{
  return m_sellist.size();
}

CGameMap::point* CGameMapWnd::GetSelectedPoint(int index)
{
  return static_cast<point*>(m_sellist[index]);
}

CGameMap::line* CGameMapWnd::DeletePoint(point *p)
{
  if(p==0)
    return 0;

  line* ln=0;

  for(LINELIST::iterator i=m_linelist.begin();i!=m_linelist.end();i++)
  {
    for(PITLIST::iterator k=i->begin();k!=i->end();k++)
    {
      if(&*k==p)
      {
        SetSelect(&*k,false);
        i->erase(k++);
        ln=static_cast<line*>(&*i);
        break;
      }
    }
  }

  // не найдена точка для удаления!
  //assert(ln!=0);

  RedrawWindow();
  return ln;
}
