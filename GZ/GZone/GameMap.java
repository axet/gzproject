package GZone;

import java.awt.*;

public interface GameMap
{
  // ��������������� ������

  // ����� ������� (typedef void* line)
  public static class line extends Object
  {
      ;
  }
  // ����� ����� (typedef void* point)
  public static class point extends Object
  {
  }

  public static interface GameMapEvents
  {
    // ����� ��������
    public abstract void PointSelect(point p);
    // ����� �����������
    public abstract void PointMoveAfter(point p);
    // ����� ����� �����������
    public abstract boolean PointMoveBefore(point p);
    // ����� ����������
    public abstract void PointHiLight(point p);
  };
  
  // ������ � ������

  // ��������� ������ � ������� ����� ����������� �����
  public abstract line AddLine() ;
  // ��������� ����� � ��������� ������, ���� ��������� �������� ������
  // ����� ��������� ����� ������
  public abstract point AddPoint(int x,int y,line p ) ;
  // ���������� ����������� ����� ������ � ������� ��������
  public abstract void SetPointData(point p, int data) ;
  // �������� �����������
  public abstract int GetPointData(point p) ;
  // ���������� ������ �� �����������
  public abstract void SetCursor(int x,int y) ;
  // ���������� ����� ��������� �� �����������
  public abstract void SetViewPos(int x,int y) ;
  // ������� �����
  public abstract line DeletePoint(point p) ;
  // ������� ������ �������
  public abstract void DeleteLine(line l) ;
  // ������� ��� �����
  public abstract void Clear() ;
  // ��������� ��������� �����
  public abstract Point Translate(point p) ;
  // ������ ����� ����������
  public abstract void SelectPoint(point p,boolean center ,boolean clearother ) ;
  // �������������� �� ������� �� �����
  public abstract void AddAdvise(GameMapEvents c) ;
  // ������������� �� ������� �����
  public abstract void RemoveAdvise(GameMapEvents c) ;
  // ���������� ���������� ���������� �����
  public abstract int GetSelectedPointCount() ;
  // ��������� ����������� �����
  public abstract point GetSelectedPoint(int index) ;
};
