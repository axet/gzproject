// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// CIQSecurityGuard.cpp : implementation file
//

#include "stdafx.h"
#include "gzone.h"
#include "CIQSecurityGuard.h"

#include <afxpriv.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCIQSecurityGuard

IMPLEMENT_DYNCREATE(CCIQSecurityGuard, CFormView)

CCIQSecurityGuard::CCIQSecurityGuard()
	: CFormView(CCIQSecurityGuard::IDD)
{
	//{{AFX_DATA_INIT(CCIQSecurityGuard)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CCIQSecurityGuard::~CCIQSecurityGuard()
{
}

void CCIQSecurityGuard::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCIQSecurityGuard)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCIQSecurityGuard, CFormView)
	//{{AFX_MSG_MAP(CCIQSecurityGuard)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIQSecurityGuard diagnostics

#ifdef _DEBUG
void CCIQSecurityGuard::AssertValid() const
{
	CFormView::AssertValid();
}

void CCIQSecurityGuard::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCIQSecurityGuard message handlers

HWND CCIQSecurityGuard::FormCreate(HWND parent,CGameMap*p,CCommanderIQEvents*pp)
{
  //m_events=pp;
  //m_gamemap=p;
  //m_gamemap->AddAdvise(this);
  CFormView::Create(0,0,WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),FromHandle(parent),0,0);
  SendMessage(WM_INITIALUPDATE);
  //m_pDocument=g_DocumentIQ;
  return m_hWnd;
}

CCIQSecurityGuard* CCIQSecurityGuard::Create()
{
  return (CCIQSecurityGuard*)RUNTIME_CLASS(CCIQSecurityGuard)->CreateObject();
}

const char* CCIQSecurityGuard::FormGetName()
{
  return "Security Guard";
}
