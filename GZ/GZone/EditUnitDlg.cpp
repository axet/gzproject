// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// EditUnitDlg.cpp : implementation file
//

#include "stdafx.h"
#include "gzone.h"
#include "EditUnitDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditUnitDlg dialog


CEditUnitDlg::CEditUnitDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditUnitDlg::IDD, pParent),m_edit(TRUE)
{
	//{{AFX_DATA_INIT(CEditUnitDlg)
	m_login = _T("");
	m_password = _T("");
	m_charname = _T("");
	m_showwindow = FALSE;
	m_writelog = FALSE;
	//}}AFX_DATA_INIT
}


void CEditUnitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditUnitDlg)
	DDX_Text(pDX, IDC_EDIT1, m_login);
	DDX_Text(pDX, IDC_EDIT2, m_password);
	DDX_Text(pDX, IDC_EDIT3, m_charname);
	DDX_Check(pDX, IDC_CHECK_SHOWWINDOW, m_showwindow);
	DDX_Check(pDX, IDC_CHECK_WRITELOG, m_writelog);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEditUnitDlg, CDialog)
	//{{AFX_MSG_MAP(CEditUnitDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditUnitDlg message handlers

BOOL CEditUnitDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if(!m_edit)
    SetWindowText("New unit");

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
