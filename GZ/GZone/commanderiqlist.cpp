#include "Stdafx.h"
#include "commanderiqlist.h"

#include "ciqharvester.h"
#include "ciqspider.h"
#include "ciqsecurityguard.h"
#include "ciqtrainer.h"

void CCommanderIQList::Load(const char* path)
{
  push_back(CCIQHarvester::Create());
  push_back(CCIQTrainer::Create());
  //push_back(CCIQSpider::Create());
  //push_back(CCIQSecurityGuard::Create());
}

CCommanderIQList::~CCommanderIQList()
{
  for(CommanderIQList_t::iterator i=begin();i!=end();i++)
  {
    //delete (*i);
  }
}

CCommanderIQ* CCommanderIQList::GetCommander(const char*p)
{
  for(CommanderIQList_t::iterator i=begin();i!=end();i++)
  {
    if(stricmp((*i)->FormGetName(),p)==0)
      return *i;
  }
  return 0;
}
