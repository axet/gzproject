// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if !defined(AFX_CIQGoodsUNIT_H__E22F5274_EAD2_4805_80B1_BB9A0BB6164E__INCLUDED_)
#define AFX_CIQGoodsUNIT_H__E22F5274_EAD2_4805_80B1_BB9A0BB6164E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CIQGoodsUnit.h : header file
//

#include "CIQGoods.h"


/////////////////////////////////////////////////////////////////////////////
// CCIQGoodsUnit thread

#include "../client/clientevents.h"
#include <afxmt.h>

class CCIQGoodsUnit : public CWinThread
{
  bool IsGhost();
  bool IsOverloaded();

  void Harvest();
  void Goods();
  void Sell();

  DECLARE_DYNCREATE(CCIQGoodsUnit)
protected:
	CCIQGoodsUnit();           // protected constructor used by dynamic creation

// Attributes
public:

  HWND m_parent;
  CUnit* m_commands;

  bool m_catchplayercords;

  Client::ItemType m_harvestuseitemtype;
  Client::ItemType m_makeuseitemtype;
  DWORD m_backpackser;
  CString m_charname,m_vendorname;
  int m_count;
  Client::ItemType m_goodsitemtype,m_makeitemtype;

  CCIQGoods::point *m_pt;

  enum msgs{msgStart=WM_USER+1};

  void Create(HWND parent,CUnit* p);

  operator CUnit* () { return m_commands; }
  CUnit* operator -> () {return m_commands;}

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCIQGoodsUnit)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CCIQGoodsUnit();

	// Generated message map functions
	//{{AFX_MSG(CCIQGoodsUnit)
	//}}AFX_MSG
    afx_msg void OnStart(unsigned,long);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIQGoodsUNIT_H__E22F5274_EAD2_4805_80B1_BB9A0BB6164E__INCLUDED_)
