// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// CIQSpider.cpp : implementation file
//

#include "stdafx.h"
#include "gzone.h"
#include "CIQSpider.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCIQSpider

IMPLEMENT_DYNCREATE(CCIQSpider, CFormView)

CCIQSpider::CCIQSpider()
	: CFormView(CCIQSpider::IDD)
{
	//{{AFX_DATA_INIT(CCIQSpider)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CCIQSpider::~CCIQSpider()
{
}

void CCIQSpider::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCIQSpider)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCIQSpider, CFormView)
	//{{AFX_MSG_MAP(CCIQSpider)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIQSpider diagnostics

#ifdef _DEBUG
void CCIQSpider::AssertValid() const
{
	CFormView::AssertValid();
}

void CCIQSpider::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCIQSpider message handlers


CCIQSpider* CCIQSpider::Create()
{
  return (CCIQSpider*)RUNTIME_CLASS(CCIQSpider)->CreateObject();
}

const char* CCIQSpider::FormGetName()
{
  return "Spider";
}
