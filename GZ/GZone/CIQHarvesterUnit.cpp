// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// CIQHarvesterUnit.cpp : implementation file
//

#include "stdafx.h"
#include "gzone.h"
#include "CIQHarvesterUnit.h"
#include "ciqharvester.h"
#include "gamemap.h"
#include "AutoCloseDialog.h"

#include <errorreport/errorreport.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCIQHarvesterUnit

IMPLEMENT_DYNCREATE(CCIQHarvesterUnit, CWinThread)

CCIQHarvesterUnit::CCIQHarvesterUnit():m_useitemser(0),m_backpackser(0)
{
}

CCIQHarvesterUnit::~CCIQHarvesterUnit()
{
}

BOOL CCIQHarvesterUnit::InitInstance()
{
	AfxOleInit();

	return TRUE;
}

int CCIQHarvesterUnit::ExitInstance()
{
	AfxOleTerm();

	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CCIQHarvesterUnit, CWinThread)
  ON_THREAD_MESSAGE(msgStart,OnStart)
	//{{AFX_MSG_MAP(CCIQHarvesterUnit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIQHarvesterUnit message handlers

bool CCIQHarvesterUnit::IsGhost()
{
  int hits=m_commands->StatsGetHits();
  return hits == 0;
}

void CCIQHarvesterUnit::Resurrect()
{
  m_commands->CharMoveTo(WorldCord(m_resurrectionpoint.x,m_resurrectionpoint.y,m_resurrectionpoint.z));

  while(IsGhost())
    Sleep(5000);
}

bool CCIQHarvesterUnit::IsOverloaded()
{
  int weight=m_commands->StatsGetWeight();
  // -1 может быть тогда когда на клиента с сервера эта информация еще не приходила
  // клиент только загружен..

  //if(weight==-1)
  //  throw exception(LoadString(IDS_NOCONNECTIONTOCLIENT));
  return weight>=150;
}

void CCIQHarvesterUnit::DropGoods()
{
  // положить всю руду в дом

  if(m_dropcontainerserial!=0)
  {
    m_commands->CharMoveTo(WorldCord(m_dropcontainer.x,m_dropcontainer.y,m_dropcontainer.z));
    m_commands->MoveItemsG2G(m_backpackser,Client::ItemType::itOre_s,m_dropcontainerserial);
    m_commands->MoveItemsG2G(m_backpackser,Client::ItemType::itLog_s,m_dropcontainerserial);
  }
}

void CCIQHarvesterUnit::OnStart(unsigned uipt,long)
{
  CCIQHarvester::point* pt=(CCIQHarvester::point*)uipt;

  if(pt==0)
    goto getnexttarget;

  try
  {
movetocurrenttarget:

    if(m_resurrectionpoint.x!=0&&m_resurrectionpoint.y!=0)
    {
      if(IsGhost())
        Resurrect();
    }

    if(m_dropcontainer.x!=0&&m_dropcontainer.y!=0)
    {
      if(IsOverloaded())
        DropGoods();
    }

    try
    {
      if(m_catchplayercords)
        m_commands->CharMoveTo(WorldCord(pt->px,pt->py,pt->pz));
      else
        m_commands->CharMoveTo(WorldCord(pt->x,pt->y,pt->z));
    }catch(ClientGameCommands::LocalOperationTimeout&)
    {
      Sleep(1000);
      goto getnexttarget;
    }

loop:
    {
      Sleep(3000);
      
      if(m_resurrectionpoint.x!=0&&m_resurrectionpoint.y!=0)
      {
        if(IsGhost())
          Resurrect();
      }

      std::string msg;
      try
      {
        msg=m_commands->UseItem(m_useitemser,WorldCord(pt->x,pt->y,pt->z),pt->itemid);
      }catch(ClientGameCommands::LocalOperationTimeout&)
      {
        Sleep(5000);
        goto getnexttarget;
      }

      // неудачный результат при наличии ресурса

      // это может происходит если игрок слижком часто кликает на гору.
      // нужно немного подождать а змет продожлжить добычу
      // даже не неменого а долго, поскольку пока это сообщение клиент читает
      // игрок продолжает копать, нужно как минимум чтобы игрок это закончил делать

      // You loosen some rocks but fail to find any useable ore.
      if(stricmp(msg.c_str(),"You loosen some rocks but fail to find any useable ore.")==0)
      {
        Sleep(10000);
        goto loop;
      }
      // You hack at the tree for a while, but fail to produce any useable wood.
      if(stricmp(msg.c_str(),"You hack at the tree for a while, but fail to produce any useable wood.")==0)
      {
        Sleep(10000);
        goto loop;
      }
      // You fish a while, but fail to catch anything.
      if(stricmp(msg.c_str(),"You fish a while, but fail to catch anything.")==0)
      {
        Sleep(10000);
        goto loop;
      }
      
      // Конец ресурса

      // There is no ore here to mine.
      if(strstr(msg.c_str(),"There is no ore here to mine.")!=0)
        goto getnexttarget;
      // There are no logs here to chop.
      if(strstr(msg.c_str(),"There are no logs here to chop.")!=0)
        goto getnexttarget;
      // There are no fish here.
      if(strstr(msg.c_str(),"There are no fish here.")!=0)
        goto getnexttarget;

      // Вес

      // You put the logs at your feet. It is too heavy..
      // You put the Iron Ore at your feet. It is too heavy..
      if(strstr(msg.c_str(),"It is too heavy..")!=0)
      {
        DropGoods();
        goto movetocurrenttarget;
      }

      // Успех

      // You put the Iron Ore in your pack
      if(strstr(msg.c_str(),"You put")!=0)
        goto loop;
      //You pull out a fish!
      if(strstr(msg.c_str(),"You pull out a fish!")!=0)
        goto loop;

      goto getnexttarget;
    };
  }catch(ClientGameCommands::LocalOperationTimeout &)
  {
    goto getnexttarget;
  }catch(COutOfProcMaster::ExecuteException&e)
  {
    CAutoCloseDialog dlg(ErrorReport("Client Side Execute Exception",e.what()),CWnd::FromHandle(m_parent),true);
    if(dlg.DoModal()==IDOK)
      goto getnexttarget;
    else
      goto exit;
  }catch(ExecuteAbort&)
  {
    goto exit;
  }catch(std::exception &e)
  {
    CAutoCloseDialog dlg(ErrorReport("Client Side Execute Exception",e.what()),CWnd::FromHandle(m_parent),true);
    if(dlg.DoModal()==IDOK)
      goto getnexttarget;
    else
      goto exit;
  }
  catch(_com_error &e)
  {
    AfxMessageBox(ErrorReport("Harvester",e));
    goto exit;
  }

  AfxMessageBox("harvest terminated");

exit:
  ::PostMessage(m_parent,WM_USER+5,0,0);
  return;

getnexttarget:
  ASSERT(m_parent!=0);
  ::PostMessage(m_parent,WM_USER+1,0,0);
  return;
}

void CCIQHarvesterUnit::Create(HWND parent,CUnit* p)
{
  m_parent=parent;
  m_commands=p;
}
