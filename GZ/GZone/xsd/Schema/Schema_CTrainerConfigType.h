////////////////////////////////////////////////////////////////////////
//
// Schema_CTrainerConfigType.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef Schema_CTrainerConfigType_H_INCLUDED
#define Schema_CTrainerConfigType_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000




namespace tc // URI: http://gzproject.sourceforge.net/TrainerConfig
{


class Schema_DECLSPECIFIER CTrainerConfigType : public CNode
{
public:
	CTrainerConfigType() : CNode() {}
	CTrainerConfigType(CNode& rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
	CTrainerConfigType(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
	static EGroupType GetGroupType();

	//
	// string UnitId (1...1)
	//
	static int GetUnitIdMinCount();
	static int GetUnitIdMaxCount();
	int GetUnitIdCount();
	bool HasUnitId();
	void AddUnitId(CSchemaString UnitId);
	void InsertUnitIdAt(CSchemaString UnitId, int nIndex);
	void ReplaceUnitIdAt(CSchemaString UnitId, int nIndex);
	CSchemaString GetUnitIdAt(int nIndex);
	CSchemaString GetUnitId();
	MSXML2::IXMLDOMNodePtr GetStartingUnitIdCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedUnitIdCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaString GetUnitIdValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveUnitIdAt(int nIndex);
	void RemoveUnitId();

	//
	// integer UseItemSer (1...1)
	//
	static int GetUseItemSerMinCount();
	static int GetUseItemSerMaxCount();
	int GetUseItemSerCount();
	bool HasUseItemSer();
	void AddUseItemSer(CSchemaInteger UseItemSer);
	void InsertUseItemSerAt(CSchemaInteger UseItemSer, int nIndex);
	void ReplaceUseItemSerAt(CSchemaInteger UseItemSer, int nIndex);
	CSchemaInteger GetUseItemSerAt(int nIndex);
	CSchemaInteger GetUseItemSer();
	MSXML2::IXMLDOMNodePtr GetStartingUseItemSerCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedUseItemSerCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInteger GetUseItemSerValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveUseItemSerAt(int nIndex);
	void RemoveUseItemSer();

	//
	// integer GoodsSer (1...1)
	//
	static int GetGoodsSerMinCount();
	static int GetGoodsSerMaxCount();
	int GetGoodsSerCount();
	bool HasGoodsSer();
	void AddGoodsSer(CSchemaInteger GoodsSer);
	void InsertGoodsSerAt(CSchemaInteger GoodsSer, int nIndex);
	void ReplaceGoodsSerAt(CSchemaInteger GoodsSer, int nIndex);
	CSchemaInteger GetGoodsSerAt(int nIndex);
	CSchemaInteger GetGoodsSer();
	MSXML2::IXMLDOMNodePtr GetStartingGoodsSerCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedGoodsSerCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInteger GetGoodsSerValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveGoodsSerAt(int nIndex);
	void RemoveGoodsSer();

	//
	// integer DropContainerSer (1...1)
	//
	static int GetDropContainerSerMinCount();
	static int GetDropContainerSerMaxCount();
	int GetDropContainerSerCount();
	bool HasDropContainerSer();
	void AddDropContainerSer(CSchemaInteger DropContainerSer);
	void InsertDropContainerSerAt(CSchemaInteger DropContainerSer, int nIndex);
	void ReplaceDropContainerSerAt(CSchemaInteger DropContainerSer, int nIndex);
	CSchemaInteger GetDropContainerSerAt(int nIndex);
	CSchemaInteger GetDropContainerSer();
	MSXML2::IXMLDOMNodePtr GetStartingDropContainerSerCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedDropContainerSerCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInteger GetDropContainerSerValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveDropContainerSerAt(int nIndex);
	void RemoveDropContainerSer();

	//
	// integer Portion (1...1)
	//
	static int GetPortionMinCount();
	static int GetPortionMaxCount();
	int GetPortionCount();
	bool HasPortion();
	void AddPortion(CSchemaInteger Portion);
	void InsertPortionAt(CSchemaInteger Portion, int nIndex);
	void ReplacePortionAt(CSchemaInteger Portion, int nIndex);
	CSchemaInteger GetPortionAt(int nIndex);
	CSchemaInteger GetPortion();
	MSXML2::IXMLDOMNodePtr GetStartingPortionCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedPortionCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInteger GetPortionValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemovePortionAt(int nIndex);
	void RemovePortion();

	//
	// integer Gump (1...unbounded)
	//
	static int GetGumpMinCount();
	static int GetGumpMaxCount();
	int GetGumpCount();
	bool HasGump();
	void AddGump(CSchemaInteger Gump);
	void InsertGumpAt(CSchemaInteger Gump, int nIndex);
	void ReplaceGumpAt(CSchemaInteger Gump, int nIndex);
	CSchemaInteger GetGumpAt(int nIndex);
	CSchemaInteger GetGump();
	MSXML2::IXMLDOMNodePtr GetStartingGumpCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedGumpCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInteger GetGumpValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveGumpAt(int nIndex);
	void RemoveGump();

	//
	// integer Backpack (1...1)
	//
	static int GetBackpackMinCount();
	static int GetBackpackMaxCount();
	int GetBackpackCount();
	bool HasBackpack();
	void AddBackpack(CSchemaInteger Backpack);
	void InsertBackpackAt(CSchemaInteger Backpack, int nIndex);
	void ReplaceBackpackAt(CSchemaInteger Backpack, int nIndex);
	CSchemaInteger GetBackpackAt(int nIndex);
	CSchemaInteger GetBackpack();
	MSXML2::IXMLDOMNodePtr GetStartingBackpackCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedBackpackCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInteger GetBackpackValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveBackpackAt(int nIndex);
	void RemoveBackpack();
};


} // end of namespace tc

#endif // Schema_CTrainerConfigType_H_INCLUDED
