////////////////////////////////////////////////////////////////////////
//
// Schema_CGoodsUnitType.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef Schema_CGoodsUnitType_H_INCLUDED
#define Schema_CGoodsUnitType_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000




namespace goodsconf // URI: http://gzproject.sourceforge.net/GoodsConfig
{


class Schema_DECLSPECIFIER CGoodsUnitType : public CNode
{
public:
	CGoodsUnitType() : CNode() {}
	CGoodsUnitType(CNode& rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
	CGoodsUnitType(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
	static EGroupType GetGroupType();

	//
	// decimal Backpack (1...1)
	//
	static int GetBackpackMinCount();
	static int GetBackpackMaxCount();
	int GetBackpackCount();
	bool HasBackpack();
	void AddBackpack(CSchemaDecimal Backpack);
	void InsertBackpackAt(CSchemaDecimal Backpack, int nIndex);
	void ReplaceBackpackAt(CSchemaDecimal Backpack, int nIndex);
	CSchemaDecimal GetBackpackAt(int nIndex);
	CSchemaDecimal GetBackpack();
	MSXML2::IXMLDOMNodePtr GetStartingBackpackCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedBackpackCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaDecimal GetBackpackValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveBackpackAt(int nIndex);
	void RemoveBackpack();

	//
	// int HarvestItem (1...1)
	//
	static int GetHarvestItemMinCount();
	static int GetHarvestItemMaxCount();
	int GetHarvestItemCount();
	bool HasHarvestItem();
	void AddHarvestItem(CSchemaInt HarvestItem);
	void InsertHarvestItemAt(CSchemaInt HarvestItem, int nIndex);
	void ReplaceHarvestItemAt(CSchemaInt HarvestItem, int nIndex);
	CSchemaInt GetHarvestItemAt(int nIndex);
	CSchemaInt GetHarvestItem();
	MSXML2::IXMLDOMNodePtr GetStartingHarvestItemCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedHarvestItemCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInt GetHarvestItemValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveHarvestItemAt(int nIndex);
	void RemoveHarvestItem();

	//
	// string UnitId (1...1)
	//
	static int GetUnitIdMinCount();
	static int GetUnitIdMaxCount();
	int GetUnitIdCount();
	bool HasUnitId();
	void AddUnitId(CSchemaString UnitId);
	void InsertUnitIdAt(CSchemaString UnitId, int nIndex);
	void ReplaceUnitIdAt(CSchemaString UnitId, int nIndex);
	CSchemaString GetUnitIdAt(int nIndex);
	CSchemaString GetUnitId();
	MSXML2::IXMLDOMNodePtr GetStartingUnitIdCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedUnitIdCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaString GetUnitIdValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveUnitIdAt(int nIndex);
	void RemoveUnitId();

	//
	// int MakeItemType (1...unbounded)
	//
	static int GetMakeItemTypeMinCount();
	static int GetMakeItemTypeMaxCount();
	int GetMakeItemTypeCount();
	bool HasMakeItemType();
	void AddMakeItemType(CSchemaInt MakeItemType);
	void InsertMakeItemTypeAt(CSchemaInt MakeItemType, int nIndex);
	void ReplaceMakeItemTypeAt(CSchemaInt MakeItemType, int nIndex);
	CSchemaInt GetMakeItemTypeAt(int nIndex);
	CSchemaInt GetMakeItemType();
	MSXML2::IXMLDOMNodePtr GetStartingMakeItemTypeCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedMakeItemTypeCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInt GetMakeItemTypeValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveMakeItemTypeAt(int nIndex);
	void RemoveMakeItemType();

	//
	// int GoodsItemType (1...unbounded)
	//
	static int GetGoodsItemTypeMinCount();
	static int GetGoodsItemTypeMaxCount();
	int GetGoodsItemTypeCount();
	bool HasGoodsItemType();
	void AddGoodsItemType(CSchemaInt GoodsItemType);
	void InsertGoodsItemTypeAt(CSchemaInt GoodsItemType, int nIndex);
	void ReplaceGoodsItemTypeAt(CSchemaInt GoodsItemType, int nIndex);
	CSchemaInt GetGoodsItemTypeAt(int nIndex);
	CSchemaInt GetGoodsItemType();
	MSXML2::IXMLDOMNodePtr GetStartingGoodsItemTypeCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedGoodsItemTypeCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInt GetGoodsItemTypeValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveGoodsItemTypeAt(int nIndex);
	void RemoveGoodsItemType();

	//
	// int MakeItem (1...1)
	//
	static int GetMakeItemMinCount();
	static int GetMakeItemMaxCount();
	int GetMakeItemCount();
	bool HasMakeItem();
	void AddMakeItem(CSchemaInt MakeItem);
	void InsertMakeItemAt(CSchemaInt MakeItem, int nIndex);
	void ReplaceMakeItemAt(CSchemaInt MakeItem, int nIndex);
	CSchemaInt GetMakeItemAt(int nIndex);
	CSchemaInt GetMakeItem();
	MSXML2::IXMLDOMNodePtr GetStartingMakeItemCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedMakeItemCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInt GetMakeItemValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveMakeItemAt(int nIndex);
	void RemoveMakeItem();

	//
	// int Count (1...1)
	//
	static int GetCountMinCount();
	static int GetCountMaxCount();
	int GetCountCount();
	bool HasCount();
	void AddCount(CSchemaInt Count);
	void InsertCountAt(CSchemaInt Count, int nIndex);
	void ReplaceCountAt(CSchemaInt Count, int nIndex);
	CSchemaInt GetCountAt(int nIndex);
	CSchemaInt GetCount();
	MSXML2::IXMLDOMNodePtr GetStartingCountCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedCountCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInt GetCountValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveCountAt(int nIndex);
	void RemoveCount();

	//
	// string VenderName (1...1)
	//
	static int GetVenderNameMinCount();
	static int GetVenderNameMaxCount();
	int GetVenderNameCount();
	bool HasVenderName();
	void AddVenderName(CSchemaString VenderName);
	void InsertVenderNameAt(CSchemaString VenderName, int nIndex);
	void ReplaceVenderNameAt(CSchemaString VenderName, int nIndex);
	CSchemaString GetVenderNameAt(int nIndex);
	CSchemaString GetVenderName();
	MSXML2::IXMLDOMNodePtr GetStartingVenderNameCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedVenderNameCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaString GetVenderNameValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveVenderNameAt(int nIndex);
	void RemoveVenderName();

	//
	// string CharName (1...1)
	//
	static int GetCharNameMinCount();
	static int GetCharNameMaxCount();
	int GetCharNameCount();
	bool HasCharName();
	void AddCharName(CSchemaString CharName);
	void InsertCharNameAt(CSchemaString CharName, int nIndex);
	void ReplaceCharNameAt(CSchemaString CharName, int nIndex);
	CSchemaString GetCharNameAt(int nIndex);
	CSchemaString GetCharName();
	MSXML2::IXMLDOMNodePtr GetStartingCharNameCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedCharNameCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaString GetCharNameValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveCharNameAt(int nIndex);
	void RemoveCharName();
};


} // end of namespace goodsconf

#endif // Schema_CGoodsUnitType_H_INCLUDED
