////////////////////////////////////////////////////////////////////////
//
// Schema_CDataFileType.cpp
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "SchemaBase.h"
#include "Schema_CDataFileType.h"
#include "Schema_CHarvesterConfigType.h"
#include "Schema_CTrainerConfigType.h"



namespace gdf // URI: http://gzproject.sourceforge.net/GZoneDataFile
{
////////////////////////////////////////////////////////////////////////
//
// class CDataFileType
//
////////////////////////////////////////////////////////////////////////


CNode::EGroupType CDataFileType::GetGroupType()
{
	return eChoice;
}
int CDataFileType::GetCommanderMinCount()
{
	return 0;
}


int CDataFileType::GetCommanderMaxCount()
{
	return 1;
}


int CDataFileType::GetCommanderCount()
{
	return ChildCountInternal(Attribute, _T(""), _T("Commander"));
}


bool CDataFileType::HasCommander()
{
	return InternalHasChild(Attribute, _T(""), _T("Commander"));
}


void CDataFileType::AddCommander(CSchemaString Commander)
{
	if( !Commander.IsNull() )
		InternalAppend(Attribute, _T(""), _T("Commander"), Commander);
}


void CDataFileType::InsertCommanderAt(CSchemaString Commander, int nIndex)
{
	if( !Commander.IsNull() )
		InternalInsertAt(Attribute, _T(""), _T("Commander"), nIndex, Commander);
}


void CDataFileType::ReplaceCommanderAt(CSchemaString Commander, int nIndex)
{
	InternalReplaceAt(Attribute, _T(""), _T("Commander"), nIndex, Commander);
}



CSchemaString CDataFileType::GetCommanderAt(int nIndex)
{
	return (LPCTSTR)InternalGetAt(Attribute, _T(""), _T("Commander"), nIndex)->text;
	//return tstring((LPCTSTR)InternalGetAt(Attribute, _T(""), _T("Commander"), nIndex)->text);
}

MSXML2::IXMLDOMNodePtr CDataFileType::GetStartingCommanderCursor()
{
	return InternalGetFirstChild(Attribute, _T(""), _T("Commander"));
}

MSXML2::IXMLDOMNodePtr CDataFileType::GetAdvancedCommanderCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Attribute, _T(""), _T("Commander"), pCurNode);
}

CSchemaString CDataFileType::GetCommanderValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return (LPCTSTR)pCurNode->text;
}



CSchemaString CDataFileType::GetCommander()
{
	return GetCommanderAt(0);
}


void CDataFileType::RemoveCommanderAt(int nIndex)
{
	InternalRemoveAt(Attribute, _T(""), _T("Commander"), nIndex);
}


void CDataFileType::RemoveCommander()
{
	while (HasCommander())
		RemoveCommanderAt(0);
}

int CDataFileType::GetHarvesterConfigMinCount()
{
	return 1;
}


int CDataFileType::GetHarvesterConfigMaxCount()
{
	return 1;
}


int CDataFileType::GetHarvesterConfigCount()
{
	return ChildCountInternal(Element, _T("http://gzproject.sourceforge.net/HarvesterConfig"), _T("gdf:HarvesterConfig"));
}


bool CDataFileType::HasHarvesterConfig()
{
	return InternalHasChild(Element, _T("http://gzproject.sourceforge.net/HarvesterConfig"), _T("gdf:HarvesterConfig"));
}


void CDataFileType::AddHarvesterConfig(hc::CHarvesterConfigType& HarvesterConfig)
{
	InternalAppendNode(_T("http://gzproject.sourceforge.net/HarvesterConfig"), _T("gdf:HarvesterConfig"), HarvesterConfig);
}


void CDataFileType::InsertHarvesterConfigAt(hc::CHarvesterConfigType& HarvesterConfig, int nIndex)
{
	InternalInsertNodeAt(_T("http://gzproject.sourceforge.net/HarvesterConfig"), _T("gdf:HarvesterConfig"), nIndex, HarvesterConfig);
}


void CDataFileType::ReplaceHarvesterConfigAt(hc::CHarvesterConfigType& HarvesterConfig, int nIndex)
{
	InternalReplaceNodeAt(_T("http://gzproject.sourceforge.net/HarvesterConfig"), _T("gdf:HarvesterConfig"), nIndex, HarvesterConfig);
}



hc::CHarvesterConfigType CDataFileType::GetHarvesterConfigAt(int nIndex)
{
	return hc::CHarvesterConfigType(*this, InternalGetAt(Element, _T("http://gzproject.sourceforge.net/HarvesterConfig"), _T("gdf:HarvesterConfig"), nIndex));
}

MSXML2::IXMLDOMNodePtr CDataFileType::GetStartingHarvesterConfigCursor()
{
	return InternalGetFirstChild(Element, _T("http://gzproject.sourceforge.net/HarvesterConfig"), _T("gdf:HarvesterConfig"));
}

MSXML2::IXMLDOMNodePtr CDataFileType::GetAdvancedHarvesterConfigCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://gzproject.sourceforge.net/HarvesterConfig"), _T("gdf:HarvesterConfig"), pCurNode);
}

hc::CHarvesterConfigType CDataFileType::GetHarvesterConfigValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return hc::CHarvesterConfigType( *this, pCurNode );
}



hc::CHarvesterConfigType CDataFileType::GetHarvesterConfig()
{
	return GetHarvesterConfigAt(0);
}


void CDataFileType::RemoveHarvesterConfigAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://gzproject.sourceforge.net/HarvesterConfig"), _T("gdf:HarvesterConfig"), nIndex);
}


void CDataFileType::RemoveHarvesterConfig()
{
	while (HasHarvesterConfig())
		RemoveHarvesterConfigAt(0);
}

int CDataFileType::GetTrainerConfigMinCount()
{
	return 1;
}


int CDataFileType::GetTrainerConfigMaxCount()
{
	return 1;
}


int CDataFileType::GetTrainerConfigCount()
{
	return ChildCountInternal(Element, _T("http://gzproject.sourceforge.net/TrainerConfig"), _T("gdf:TrainerConfig"));
}


bool CDataFileType::HasTrainerConfig()
{
	return InternalHasChild(Element, _T("http://gzproject.sourceforge.net/TrainerConfig"), _T("gdf:TrainerConfig"));
}


void CDataFileType::AddTrainerConfig(tc::CTrainerConfigType& TrainerConfig)
{
	InternalAppendNode(_T("http://gzproject.sourceforge.net/TrainerConfig"), _T("gdf:TrainerConfig"), TrainerConfig);
}


void CDataFileType::InsertTrainerConfigAt(tc::CTrainerConfigType& TrainerConfig, int nIndex)
{
	InternalInsertNodeAt(_T("http://gzproject.sourceforge.net/TrainerConfig"), _T("gdf:TrainerConfig"), nIndex, TrainerConfig);
}


void CDataFileType::ReplaceTrainerConfigAt(tc::CTrainerConfigType& TrainerConfig, int nIndex)
{
	InternalReplaceNodeAt(_T("http://gzproject.sourceforge.net/TrainerConfig"), _T("gdf:TrainerConfig"), nIndex, TrainerConfig);
}



tc::CTrainerConfigType CDataFileType::GetTrainerConfigAt(int nIndex)
{
	return tc::CTrainerConfigType(*this, InternalGetAt(Element, _T("http://gzproject.sourceforge.net/TrainerConfig"), _T("gdf:TrainerConfig"), nIndex));
}

MSXML2::IXMLDOMNodePtr CDataFileType::GetStartingTrainerConfigCursor()
{
	return InternalGetFirstChild(Element, _T("http://gzproject.sourceforge.net/TrainerConfig"), _T("gdf:TrainerConfig"));
}

MSXML2::IXMLDOMNodePtr CDataFileType::GetAdvancedTrainerConfigCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://gzproject.sourceforge.net/TrainerConfig"), _T("gdf:TrainerConfig"), pCurNode);
}

tc::CTrainerConfigType CDataFileType::GetTrainerConfigValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return tc::CTrainerConfigType( *this, pCurNode );
}



tc::CTrainerConfigType CDataFileType::GetTrainerConfig()
{
	return GetTrainerConfigAt(0);
}


void CDataFileType::RemoveTrainerConfigAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://gzproject.sourceforge.net/TrainerConfig"), _T("gdf:TrainerConfig"), nIndex);
}


void CDataFileType::RemoveTrainerConfig()
{
	while (HasTrainerConfig())
		RemoveTrainerConfigAt(0);
}

} // end of namespace gdf
