////////////////////////////////////////////////////////////////////////
//
// Schema_CConfigType.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef Schema_CConfigType_H_INCLUDED
#define Schema_CConfigType_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000




namespace gc // URI: http://gzproject.sourceforge.net/GZoneConfig
{


class Schema_DECLSPECIFIER CConfigType : public CNode
{
public:
	CConfigType() : CNode() {}
	CConfigType(CNode& rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
	CConfigType(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
	static EGroupType GetGroupType();

	//
	// string BGImage (0...1)
	//
	static int GetBGImageMinCount();
	static int GetBGImageMaxCount();
	int GetBGImageCount();
	bool HasBGImage();
	void AddBGImage(CSchemaString BGImage);
	void InsertBGImageAt(CSchemaString BGImage, int nIndex);
	void ReplaceBGImageAt(CSchemaString BGImage, int nIndex);
	CSchemaString GetBGImageAt(int nIndex);
	CSchemaString GetBGImage();
	MSXML2::IXMLDOMNodePtr GetStartingBGImageCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedBGImageCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaString GetBGImageValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveBGImageAt(int nIndex);
	void RemoveBGImage();

	//
	// Account Account reference (0...unbounded)
	//
	static int GetAccountMinCount();
	static int GetAccountMaxCount();
	int GetAccountCount();
	bool HasAccount();
	void AddAccount(CAccountType& Account);
	void InsertAccountAt(CAccountType& Account, int nIndex);
	void ReplaceAccountAt(CAccountType& Account, int nIndex);
	CAccountType GetAccountAt(int nIndex);
	CAccountType GetAccount();
	MSXML2::IXMLDOMNodePtr GetStartingAccountCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedAccountCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CAccountType GetAccountValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveAccountAt(int nIndex);
	void RemoveAccount();

	//
	// string ClientPath (0...1)
	//
	static int GetClientPathMinCount();
	static int GetClientPathMaxCount();
	int GetClientPathCount();
	bool HasClientPath();
	void AddClientPath(CSchemaString ClientPath);
	void InsertClientPathAt(CSchemaString ClientPath, int nIndex);
	void ReplaceClientPathAt(CSchemaString ClientPath, int nIndex);
	CSchemaString GetClientPathAt(int nIndex);
	CSchemaString GetClientPath();
	MSXML2::IXMLDOMNodePtr GetStartingClientPathCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedClientPathCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaString GetClientPathValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveClientPathAt(int nIndex);
	void RemoveClientPath();

	//
	// Secure Secure (1...1)
	//
	static int GetSecureMinCount();
	static int GetSecureMaxCount();
	int GetSecureCount();
	bool HasSecure();
	void AddSecure(CSecureType Secure);
	void InsertSecureAt(CSecureType Secure, int nIndex);
	void ReplaceSecureAt(CSecureType Secure, int nIndex);
	CSecureType GetSecureAt(int nIndex);
	CSecureType GetSecure();
	MSXML2::IXMLDOMNodePtr GetStartingSecureCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedSecureCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSecureType GetSecureValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveSecureAt(int nIndex);
	void RemoveSecure();
};


} // end of namespace gc

#endif // Schema_CConfigType_H_INCLUDED
