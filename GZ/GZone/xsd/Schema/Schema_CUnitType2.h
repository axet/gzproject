////////////////////////////////////////////////////////////////////////
//
// Schema_CUnitType2.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef Schema_CUnitType2_H_INCLUDED
#define Schema_CUnitType2_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000




namespace hc // URI: http://gzproject.sourceforge.net/HarvesterConfig
{


class Schema_DECLSPECIFIER CUnitType2 : public CNode
{
public:
	CUnitType2() : CNode() {}
	CUnitType2(CNode& rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
	CUnitType2(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
	static EGroupType GetGroupType();

	//
	// decimal Backpack (1...1)
	//
	static int GetBackpackMinCount();
	static int GetBackpackMaxCount();
	int GetBackpackCount();
	bool HasBackpack();
	void AddBackpack(CSchemaDecimal Backpack);
	void InsertBackpackAt(CSchemaDecimal Backpack, int nIndex);
	void ReplaceBackpackAt(CSchemaDecimal Backpack, int nIndex);
	CSchemaDecimal GetBackpackAt(int nIndex);
	CSchemaDecimal GetBackpack();
	MSXML2::IXMLDOMNodePtr GetStartingBackpackCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedBackpackCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaDecimal GetBackpackValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveBackpackAt(int nIndex);
	void RemoveBackpack();

	//
	// int Item (1...1)
	//
	static int GetItemMinCount();
	static int GetItemMaxCount();
	int GetItemCount();
	bool HasItem();
	void AddItem(CSchemaInt Item);
	void InsertItemAt(CSchemaInt Item, int nIndex);
	void ReplaceItemAt(CSchemaInt Item, int nIndex);
	CSchemaInt GetItemAt(int nIndex);
	CSchemaInt GetItem();
	MSXML2::IXMLDOMNodePtr GetStartingItemCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedItemCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInt GetItemValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveItemAt(int nIndex);
	void RemoveItem();

	//
	// string UnitId (1...1)
	//
	static int GetUnitIdMinCount();
	static int GetUnitIdMaxCount();
	int GetUnitIdCount();
	bool HasUnitId();
	void AddUnitId(CSchemaString UnitId);
	void InsertUnitIdAt(CSchemaString UnitId, int nIndex);
	void ReplaceUnitIdAt(CSchemaString UnitId, int nIndex);
	CSchemaString GetUnitIdAt(int nIndex);
	CSchemaString GetUnitId();
	MSXML2::IXMLDOMNodePtr GetStartingUnitIdCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedUnitIdCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaString GetUnitIdValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveUnitIdAt(int nIndex);
	void RemoveUnitId();
};


} // end of namespace hc

#endif // Schema_CUnitType2_H_INCLUDED
