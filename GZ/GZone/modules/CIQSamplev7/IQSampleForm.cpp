// IQSample.cpp : implementation file
//

#include "stdafx.h"
#include "IQSampleForm.h"
#include ".\iqsampleform.h"

CString LoadString(unsigned id)
{
  CString str;
  str.LoadString(id);
  return str;
}

extern "C" __declspec(dllexport) CCommanderIQ* InitializeCommander()
{
  return CIQSampleForm::Create();
}

// CIQSampleForm

IMPLEMENT_DYNCREATE(CIQSampleForm, CFormView)

CIQSampleForm::CIQSampleForm()
	: CFormView(CIQSampleForm::IDD)
  , speech(_T("")),m_commands(0),m_work(false),m_currpointindex(0)
{
}

CIQSampleForm::~CIQSampleForm()
{
}


void CIQSampleForm::SelectedPath(unsigned x,unsigned y)
{
  m_gamemap->SetCursor(x,y);
}



void CIQSampleForm::DoDataExchange(CDataExchange* pDX)
{
  CFormView::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_RICHEDIT1, speech);
}

BEGIN_MESSAGE_MAP(CIQSampleForm, CFormView)
  ON_MESSAGE(msgEnd,OnEnd)
  ON_BN_CLICKED(IDC_BUTTON_START, OnBnClickedButtonStart)
  ON_WM_MOUSEACTIVATE()
  ON_BN_CLICKED(IDC_BUTTON_GENERATE, OnBnClickedButtonGenerate)
END_MESSAGE_MAP()


// CIQSampleForm diagnostics

#ifdef _DEBUG
void CIQSampleForm::AssertValid() const
{
	CFormView::AssertValid();
}

void CIQSampleForm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG


// CIQSampleForm message handlers
HWND CIQSampleForm::FormCreate(HWND parent,CGameMap*p,CCommanderIQEvents*pp)
{
  m_events=pp;
  m_gamemap=p;
  m_gamemap->AddAdvise(this);
  CFormView::Create(0,0,WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),FromHandle(parent),0,0);
  SendMessage(WM_INITIALUPDATE);
  return *this;
}

CIQSampleForm* CIQSampleForm::Create()
{
  return (CIQSampleForm*)RUNTIME_CLASS(CIQSampleForm)->CreateObject();
}


HWND CIQSampleForm::FormGet()
{
  return *this;
}

const char* CIQSampleForm::FormGetName()
{
  return "CIQSampleForm VS7";
}

bool CIQSampleForm::UnitAdd(CUnit* unit)
{
  if(m_commands!=0)
    return false; // reject add unit
  m_commands=unit;
  m_commands->AddAdvise(this);
  return true;
}

bool CIQSampleForm::UnitRemove(CUnit* unit)
{
  ASSERT(unit==m_commands);
  if(m_work)
    return false; // reject remove unit
  m_commands->RemoveAdvise(this);
  m_commands=0;
  return true;
}

int CIQSampleForm::UnitGetCount()
{
  return m_commands!=0;
}

CUnit* CIQSampleForm::UnitGet(int index)
{
  ASSERT(index==0);
  return m_commands;
}

void CIQSampleForm::OnBnClickedButtonStart()
{
  UpdateData();

  if(!m_work)
  {
    if(m_pointlist.GetSize()==0)
      goto exit;

    m_unit->PostThreadMessage(CIQSampleUnit::msgStart,0,0);
    m_work=true;
  }else
  {
    m_unit->PostThreadMessage(CIQSampleUnit::msgStop,0,0);
    m_work=false;
  }

exit:
  SetWorkStatus(m_work);
}

void CIQSampleForm::SetWorkStatus(bool work)
{
  SetDlgItemText(IDC_BUTTON_START,work?LoadString(IDS_STRING_STOP):LoadString(IDS_STRING_START));

  GetDlgItem(IDC_RICHEDIT1)->EnableWindow(!work);
  GetDlgItem(IDC_RADIO_THREE)->EnableWindow(!work);
  GetDlgItem(IDC_RADIO_SIX)->EnableWindow(!work);
  GetDlgItem(IDC_BUTTON_GENERATE)->EnableWindow(!work);
}

bool CIQSampleForm::PointMoveBefore(CGameMap::point*)
{
  return !m_work;
}

void CIQSampleForm::PointMoveAfter(CGameMap::point* p)
{
  CPoint* point=(CPoint*)m_gamemap->GetPointData(p);
  m_gamemap->Translate(p,(int*)&point->x,(int*)&point->y);
}

CUnit& CIQSampleForm::GetCommands()
{
  ASSERT(m_commands!=0);
  return *m_commands;
}

BOOL CIQSampleForm::GetNextTarget(CPoint &pt, CString &sp)
{
  if(!m_work)
    return FALSE;

  if(m_currpointindex==m_pointlist.GetSize())
    m_currpointindex=0;

  pt=m_pointlist[m_currpointindex];
  sp=speech;

  m_currpointindex++;

  return TRUE;
}

void CIQSampleForm::MakePath(int stepcount)
{
  ASSERT(stepcount>=0);
  int x,y;
  m_commands->GetUnitPos(&x,&y);

  m_pointlist.SetSize(0,6);

  const int steplen=3;

  int direction=rand()>RAND_MAX/2?-1:1;

  while(stepcount--)
  {
    x+=direction*(rand()+1)/((float)RAND_MAX+1)*steplen;
    y+=direction*(rand()+1)/((float)RAND_MAX+1)*steplen;
    m_pointlist.Add(CPoint(x,y));
  }
}

void CIQSampleForm::ReloadMap()
{
  m_gamemap->DeleteLine(m_pointlistline);
  m_pointlistline=m_gamemap->AddLine();

  for(int i=0;i<m_pointlist.GetSize();i++)
  {
    CGameMap::point *point=m_gamemap->AddPoint(m_pointlist[i].x,m_pointlist[i].y,m_pointlistline);
    m_gamemap->SetPointData(point,&m_pointlist[i]);
  }
}


int CIQSampleForm::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
  return MA_NOACTIVATE;

  return __super::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

void CIQSampleForm::OnBnClickedButtonGenerate()
{
  if(m_commands==0)
    return;

  int x,y;
  m_commands->GetUnitPos(&x,&y);

  int step=0;
  if(((CButton*)GetDlgItem(IDC_RADIO_THREE))->GetCheck())
    step=3;
  if(((CButton*)GetDlgItem(IDC_RADIO_SIX))->GetCheck())
    step=6;

  MakePath(step);

  ReloadMap();
}

void CIQSampleForm::OnInitialUpdate()
{
  __super::OnInitialUpdate();

  m_unit=(CIQSampleUnit*)AfxBeginThread(RUNTIME_CLASS(CIQSampleUnit));
  m_unit->m_main=this;

  m_pointlistline=m_gamemap->AddLine();	

}

LRESULT CIQSampleForm::OnEnd(WPARAM,LPARAM)
{
  m_unit->PostThreadMessage(CIQSampleUnit::msgStart,0,0);
  return 0;
}

void CIQSampleForm::PostNcDestroy()
{
  // TODO: Add your specialized code here and/or call the base class

  //__super::PostNcDestroy();
}
