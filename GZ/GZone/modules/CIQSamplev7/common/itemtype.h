#ifndef __ITEMTYPE_H__
#define __ITEMTYPE_H__

#include "types.h"

class ItemType:public ShortList
{
  unsigned short m_itemtype;

public:
  enum BuildInType {itOre_s,itLog_s};

  ItemType();
  ItemType(BuildInType);
  ItemType(unsigned short);

  bool operator == (unsigned short s) const;
  bool onof(unsigned short s) const ;
  operator unsigned short() const;
};

#endif
