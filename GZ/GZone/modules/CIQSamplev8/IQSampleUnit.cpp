// IQSampleUnit.cpp : implementation file
//

#include "stdafx.h"
#include "IQSampleForm.h"
#include "IQSampleUnit.h"
#include ".\iqsampleunit.h"


// CIQSampleUnit

IMPLEMENT_DYNCREATE(CIQSampleUnit, CWinThread)

CIQSampleUnit::CIQSampleUnit()
{
}

CIQSampleUnit::~CIQSampleUnit()
{
}

BOOL CIQSampleUnit::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	return TRUE;
}

int CIQSampleUnit::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CIQSampleUnit, CWinThread)
  ON_THREAD_MESSAGE(msgStart,OnStart)
END_MESSAGE_MAP()


// CIQSampleUnit message handlers
void CIQSampleUnit::OnStart(WPARAM, LPARAM)
{
  CPoint point;
  CString speech;
  if(!m_main->GetNextTarget(point,speech))
    return;

  try
  {
    m_main->GetCommands().CharMoveTo(point.x,point.y);
    m_main->GetCommands().CharSpeech(speech);
    Sleep(5000);
    goto getnexttarget;
  }catch(ClientGameCommands::LocalOperationTimeout&)
  {
    goto getnexttarget;
  }catch(std::exception &e)
  {
    AfxMessageBox(e.what());
    goto stop;
  }

stop:
  return;
getnexttarget:
  m_main->PostMessage(CIQSampleForm::msgEnd);
}

int CIQSampleUnit::Run()
{
  AfxOleInit();

  return CWinThread::Run();
}
