#pragma once

#include "common/common.h"
#include "resource.h"
#include "IQSampleUnit.h"

#include <afxtempl.h>
#include <afxpriv.h>

// CIQSampleForm form view

class CIQSampleForm : public CFormView,public CCommanderIQ,public CClientGameEvents
{
	DECLARE_DYNCREATE(CIQSampleForm)

protected:
	CIQSampleForm();           // protected constructor used by dynamic creation
	virtual ~CIQSampleForm();

public:
	enum { IDD = IDD_IQSAMPLE };

  enum {msgEnd=WM_USER+1};

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

  bool m_work;
  CGameMap* m_gamemap;
  CCommanderIQEvents *m_events;
  CIQSampleUnit* m_unit;
  CUnit *m_commands;
  CGameMap::line* m_pointlistline;
  CArray<CPoint,CPoint&> m_pointlist;
  int m_currpointindex;

  BOOL GetNextTarget(CPoint&pt,CString&speech);
  CUnit& GetCommands();
  static CIQSampleForm* Create();
  void SetWorkStatus(bool work);

  void MakePath(int stepcount);
  void ReloadMap();

  HWND FormCreate(HWND parent,CGameMap*,CCommanderIQEvents*);
  HWND FormGet();
  const char* FormGetName();

  bool UnitAdd(CUnit*);
  bool UnitRemove(CUnit*);
  int UnitGetCount();
  CUnit* UnitGet(int index);

  bool PointMoveBefore(CGameMap::point*);
  void PointMoveAfter(CGameMap::point*);

  virtual void SelectedPath(unsigned x,unsigned y);

  LRESULT OnEnd(WPARAM,LPARAM);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedButtonStart();
  CString speech;
  afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
  afx_msg void OnBnClickedButtonGenerate();
  virtual void OnInitialUpdate();
protected:
  virtual void PostNcDestroy();
};


