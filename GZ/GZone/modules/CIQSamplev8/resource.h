//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CIQSample.rc
//
#define IDS_STRING_STOP                 1000
#define IDS_STRING_START                1001
#define IDD_IQSAMPLE                    1007
#define IDC_BUTTON_START                2000
#define IDC_RICHEDIT1                   2001
#define IDC_RADIO_THREE                 2003
#define IDC_RADIO_SIX                   2004
#define IDC_BUTTON_GENERATE             2005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1001
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           1008
#endif
#endif
