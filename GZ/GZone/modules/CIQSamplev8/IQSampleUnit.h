#pragma once

class CIQSampleForm; 

// CIQSampleUnit

class CIQSampleUnit : public CWinThread
{
	DECLARE_DYNCREATE(CIQSampleUnit)

protected:
	CIQSampleUnit();           // protected constructor used by dynamic creation
	virtual ~CIQSampleUnit();

public:

  CIQSampleForm* m_main;
  enum {msgStart=WM_USER+1,msgStop};

  void OnStart(WPARAM, LPARAM);

  virtual BOOL InitInstance();
	virtual int ExitInstance();

protected:
	DECLARE_MESSAGE_MAP()
public:
  virtual int Run();
};


