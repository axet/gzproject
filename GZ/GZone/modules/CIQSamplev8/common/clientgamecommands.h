#ifndef CLIENTGAMECOMMANDS_H
#define CLIENTGAMECOMMANDS_H

#include <exception>

#include "types.h"
#include "itemtype.h"

namespace ClientGameCommands
{
  // испольнение операции
  class LocalOperationTimeout:public std::exception
  {
  public:
    LocalOperationTimeout(const char* p):std::exception(p) {}
  };

  // персонаж
  class Character
  {
  public:
    // перемещение персонажа в указанную точку
    virtual void CharMoveTo(int x,int y) = 0;
    // персонаж произносит слова
    virtual void CharSpeech(const char* buf) = 0;
  };

  // работа (использование) с предметами
  class ObjectUsage
  {
  public:
    // постое использование предмета
    virtual std::string UseItem(unsigned itemserial) = 0;
    // использование предмета на другом предмете
    virtual std::string UseItem(unsigned itemserialtouse,unsigned itemserialonuse) = 0;
    // использованине предмета на земле, для статических обьектов передаеться еще itemid
    // для того чтобы добывать руду в скале достаточно только координат
    virtual std::string UseItem(unsigned itemserialtouse, unsigned x, unsigned y, const ItemType &it = 0) = 0;
    // исползьует предмет на другом предмете (предметах) для появления гампа,
    // в котором последовательно выдберает itemid возвращет серийный нового предмета
    virtual unsigned UseItemGump(unsigned itemserialtouse,const UnsignedList &itemserialtoselect, const ShortList &itemslist) = 0;

    // перемещение предметов

    // перемещает предметы определнного типа из указанного
    // контейнера в дрйгой контейнер
    // Move Items Gump to Gump
    virtual void MoveItemsG2G(unsigned containerserialfrom,const ItemType &it,unsigned containerserialto) = 0;
    // функция находит контейнер, в нем предмет определенного типа и количества
    // и перемещает этот предмет в другой
    virtual void MoveItemsG2G(unsigned containerserialfrom,const ItemType &it,int count, unsigned containerserialto) = 0;
    // перемещает предмет по серийному номеру в контейнер
    // возвращает номер новго предмета (когда мы перетаскиваем часть кучки он может
    // поменяться)
    virtual unsigned MoveItem(unsigned itemwhat,unsigned containerserialto, int count = -1) = 0;

    // поиск предметов

    // поиск любого предмета в контейнере
    virtual unsigned SelectItem(unsigned containerserialwhere) = 0;
    // поиск по типу
    virtual unsigned SelectItem(unsigned containerserialwhere,const ItemType &it) = 0;
    // поиск по количеству
    virtual unsigned SelectItem(unsigned containerserialfrom,const ItemType &it,int count) = 0;
  };

  // работа со статсами
  class Particle
  {
  public:
    // возвращает количество хитов
    virtual int StatsGetHits() = 0;
    virtual int StatsGetMana() = 0;
    virtual int StatsGetStam() = 0;
    // количество нагрузки
    virtual int StatsGetWeight() = 0;
  };

  class Control:public Particle, public ObjectUsage,public Character
  {
    ;
  };
}

#endif
