// CIQSampleForm.cpp : implementation file
//

#include "stdafx.h"
#include "CIQSample.h"
#include "CIQSampleForm.h"
#include <afxpriv.h>
#include "ciqsampleunit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCIQSampleForm

CString LoadString(unsigned id)
{
  CString str;
  str.LoadString(id);
  return str;
}

IMPLEMENT_DYNCREATE(CCIQSampleForm, CFormView)

CCIQSampleForm::CCIQSampleForm()
	: CFormView(CCIQSampleForm::IDD),m_work(false),m_unit(0),m_commands(0),m_currpointindex(0)
{
	//{{AFX_DATA_INIT(CCIQSampleForm)
	m_speech = _T("");
	//}}AFX_DATA_INIT
}

CCIQSampleForm::~CCIQSampleForm()
{
}

void CCIQSampleForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCIQSampleForm)
	DDX_Text(pDX, IDC_RICHEDIT1, m_speech);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCIQSampleForm, CFormView)
  ON_MESSAGE(msgEnd,OnEnd)
	//{{AFX_MSG_MAP(CCIQSampleForm)
	ON_BN_CLICKED(IDC_BUTTON_START, OnButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_GENERATE, OnButtonGenerate)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIQSampleForm diagnostics

#ifdef _DEBUG
void CCIQSampleForm::AssertValid() const
{
	CFormView::AssertValid();
}

void CCIQSampleForm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCIQSampleForm message handlers

CCIQSampleForm* CCIQSampleForm::Create()
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());

  return (CCIQSampleForm*)RUNTIME_CLASS(CCIQSampleForm)->CreateObject();
}

HWND CCIQSampleForm::FormCreate(HWND parent,CGameMap*p,CCommanderIQEvents*pp)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());

  m_events=pp;
  m_gamemap=p;
  m_gamemap->AddAdvise(this);
  CFormView::Create(0,0,WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),FromHandle(parent),0,0);
  SendMessage(WM_INITIALUPDATE);
  return *this;
}

HWND CCIQSampleForm::FormGet()
{
  return *this;
}

const char* CCIQSampleForm::FormGetName()
{
  return "CCIQSampleForm VS6";
}

void CCIQSampleForm::PostNcDestroy() 
{
  // disable -> delete this;

	// CFormView::PostNcDestroy();
}

void CCIQSampleForm::OnButtonStart() 
{
  UpdateData();

  if(!m_work)
  {
    if(m_pointlist.GetSize()==0)
      goto exit;

    m_unit->PostThreadMessage(CCIQSampleUnit::msgStart,0,0);
    m_work=true;
  }else
  {
    m_unit->PostThreadMessage(CCIQSampleUnit::msgStop,0,0);
    m_work=false;
  }

exit:
  SetWorkStatus(m_work);
}

void CCIQSampleForm::SetWorkStatus(bool work)
{
  SetDlgItemText(IDC_BUTTON_START,work?LoadString(IDS_STRING_STOP):LoadString(IDS_STRING_START));

  GetDlgItem(IDC_RICHEDIT1)->EnableWindow(!work);
  GetDlgItem(IDC_RADIO_THREE)->EnableWindow(!work);
  GetDlgItem(IDC_RADIO_SIX)->EnableWindow(!work);
  GetDlgItem(IDC_BUTTON_GENERATE)->EnableWindow(!work);
}

void CCIQSampleForm::OnButtonGenerate() 
{
  if(m_commands==0)
    return;

  int x,y;
	m_commands->GetUnitPos(&x,&y);

  int step=0;
  if(((CButton*)GetDlgItem(IDC_RADIO_THREE))->GetCheck())
    step=3;
  if(((CButton*)GetDlgItem(IDC_RADIO_SIX))->GetCheck())
    step=6;

  MakePath(step);

  ReloadMap();
}

bool CCIQSampleForm::UnitAdd(CUnit* unit)
{
  if(m_commands!=0)
    return false; // reject add unit
  m_commands=unit;
  m_commands->AddAdvise(this);
  return true;
}

bool CCIQSampleForm::UnitRemove(CUnit* unit)
{
  ASSERT(unit==m_commands);
  if(m_work)
    return false; // reject add unit
  m_commands->RemoveAdvise(this);
  m_commands=0;
  return true;
}

int CCIQSampleForm::UnitGetCount()
{
  return m_commands!=0;
}

CUnit* CCIQSampleForm::UnitGet(int index)
{
  ASSERT(index==0);
  return m_commands;
}

void CCIQSampleForm::ReloadMap()
{
  m_gamemap->DeleteLine(m_pointlistline);
  m_pointlistline=m_gamemap->AddLine();

  for(int i=0;i<m_pointlist.GetSize();i++)
  {
    CGameMap::point *point=m_gamemap->AddPoint(m_pointlist[i].x,m_pointlist[i].y,m_pointlistline);
    m_gamemap->SetPointData(point,&m_pointlist[i]);
  }
}

void CCIQSampleForm::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
  m_unit=(CCIQSampleUnit*)AfxBeginThread(RUNTIME_CLASS(CCIQSampleUnit));
  m_unit->m_main=this;

  m_pointlistline=m_gamemap->AddLine();	

  ((CButton*)GetDlgItem(IDC_RADIO_THREE))->SetCheck(1);
}

void CCIQSampleForm::MakePath(int stepcount)
{
  ASSERT(stepcount>=0);
  int x,y;
  m_commands->GetUnitPos(&x,&y);

  m_pointlist.SetSize(0,6);

  const int steplen=3;

  int direction=rand()>RAND_MAX/2?-1:1;

  while(stepcount--)
  {
    x+=direction*(rand()+1)/((float)RAND_MAX+1)*steplen;
    y+=direction*(rand()+1)/((float)RAND_MAX+1)*steplen;
    m_pointlist.Add(CPoint(x,y));
  }
}

void CCIQSampleForm::PointMoveAfter(CGameMap::point* p)
{
  CPoint* point=(CPoint*)m_gamemap->GetPointData(p);
  m_gamemap->Translate(p,(int*)&point->x,(int*)&point->y);
}

BOOL CCIQSampleForm::GetNextTarget(CPoint &pt, CString &speech)
{
  if(!m_work)
    return FALSE;

  if(m_currpointindex==m_pointlist.GetSize())
    m_currpointindex=0;

  pt=m_pointlist[m_currpointindex];
  speech=m_speech;

  m_currpointindex++;

  return TRUE;
}

LRESULT CCIQSampleForm::OnEnd(WPARAM,LPARAM)
{
  m_unit->PostThreadMessage(CCIQSampleUnit::msgStart,0,0);
  return 0;
}

void CCIQSampleForm::OnDestroy() 
{
  m_unit->PostThreadMessage(WM_QUIT,0,0);
  WaitForSingleObject(m_unit->m_hThread,-1);

	CFormView::OnDestroy();
}

void CCIQSampleForm::SelectPath(unsigned x,unsigned y)
{
  m_gamemap->SetCursor(x,y);
}

CUnit& CCIQSampleForm::GetCommands()
{
  ASSERT(m_commands!=0);
  return *m_commands;
}

bool CCIQSampleForm::PointMoveBefore(CGameMap::point*)
{
  return !m_work;
}
