//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CIQSample.rc
//
#define IDS_STRING_START                1
#define IDS_STRING_STOP                 2
#define IDD_DIALOG_MAINDLG              2000
#define IDC_BUTTON_START                2000
#define IDC_RICHEDIT1                   2001
#define IDC_RADIO_THREE                 2003
#define IDC_RADIO_SIX                   2004
#define IDC_BUTTON_GENERATE             2005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2001
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         2006
#define _APS_NEXT_SYMED_VALUE           2000
#endif
#endif
