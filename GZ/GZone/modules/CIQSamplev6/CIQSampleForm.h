#if !defined(AFX_CIQSAMPLEFORM_H__5ED7D50B_87C3_4207_8E04_F5DFDEAAE3FF__INCLUDED_)
#define AFX_CIQSAMPLEFORM_H__5ED7D50B_87C3_4207_8E04_F5DFDEAAE3FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CIQSampleForm.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCIQSampleForm form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include <afxtempl.h>

class CCIQSampleUnit;

#include "common/common.h"

class CCIQSampleForm : public CFormView,public CCommanderIQ,public CClientGameEvents
{
protected:
	CCIQSampleForm();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCIQSampleForm)

// Form Data
public:
	//{{AFX_DATA(CCIQSampleForm)
	enum { IDD = IDD_DIALOG_MAINDLG };
	CString	m_speech;
	//}}AFX_DATA

// Attributes
public:

// Operations
public:
  enum {msgEnd=WM_USER+1};

	BOOL GetNextTarget(CPoint&pt,CString&speech);
  CUnit& GetCommands();
	static CCIQSampleForm* Create();

  HWND FormCreate(HWND parent,CGameMap*,CCommanderIQEvents*);
  HWND FormGet();
  const char* FormGetName();

  bool UnitAdd(CUnit*);
  bool UnitRemove(CUnit*);
  int UnitGetCount();
  CUnit* UnitGet(int index);

  bool PointMoveBefore(CGameMap::point*);
  void PointMoveAfter(CGameMap::point*);

  void SelectPath(unsigned x,unsigned y);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCIQSampleForm)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	void ReloadMap();
  void MakePath(int stepcount);
	virtual ~CCIQSampleForm();
  void SetWorkStatus(bool);
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CCIQSampleForm)
	afx_msg void OnButtonStart();
	afx_msg void OnButtonGenerate();
	afx_msg void OnDestroy();
	//}}AFX_MSG
  LRESULT OnEnd(WPARAM,LPARAM);
	DECLARE_MESSAGE_MAP()

private:
  bool m_work;
	CGameMap* m_gamemap;
  CCommanderIQEvents *m_events;
  CCIQSampleUnit* m_unit;
  CUnit *m_commands;
  CGameMap::line* m_pointlistline;
  CArray<CPoint,CPoint&> m_pointlist;
  int m_currpointindex;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIQSAMPLEFORM_H__5ED7D50B_87C3_4207_8E04_F5DFDEAAE3FF__INCLUDED_)
