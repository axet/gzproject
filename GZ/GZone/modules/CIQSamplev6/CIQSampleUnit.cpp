// CIQSampleUnit.cpp : implementation file
//

#include "stdafx.h"
#include "CIQSample.h"
#include "CIQSampleUnit.h"
#include "ciqsampleform.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCIQSampleUnit

IMPLEMENT_DYNCREATE(CCIQSampleUnit, CWinThread)

CCIQSampleUnit::CCIQSampleUnit()
{
}

CCIQSampleUnit::~CCIQSampleUnit()
{
}

BOOL CCIQSampleUnit::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	return TRUE;
}

int CCIQSampleUnit::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CCIQSampleUnit, CWinThread)
  ON_THREAD_MESSAGE(msgStart,OnStart)
	//{{AFX_MSG_MAP(CCIQSampleUnit)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIQSampleUnit message handlers

void CCIQSampleUnit::OnStart()
{
  CPoint point;
  CString speech;
  if(!m_main->GetNextTarget(point,speech))
    return;

  try
  {
    m_main->GetCommands().CharMoveTo(point.x,point.y);
    m_main->GetCommands().CharSpeech(speech);
    Sleep(5000);
    goto getnexttarget;
  }catch(ClientGameCommands::LocalOperationTimeout &)
  {
    goto getnexttarget;
  }catch(std::exception &e)
  {
    AfxMessageBox(e.what());
    goto stop;
  }

stop:
  return;
getnexttarget:
  m_main->PostMessage(CCIQSampleForm::msgEnd);
}
