// CIQSample.h : main header file for the CIQSAMPLE DLL
//

#if !defined(AFX_CIQSAMPLE_H__0BA8FB01_AA6E_42FC_B4FC_B1BD4A1A79BB__INCLUDED_)
#define AFX_CIQSAMPLE_H__0BA8FB01_AA6E_42FC_B4FC_B1BD4A1A79BB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCIQSampleApp
// See CIQSample.cpp for the implementation of this class
//

class CCIQSampleApp : public CWinApp
{
public:
	CCIQSampleApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCIQSampleApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCIQSampleApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIQSAMPLE_H__0BA8FB01_AA6E_42FC_B4FC_B1BD4A1A79BB__INCLUDED_)
