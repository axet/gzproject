#if !defined(AFX_CIQSAMPLEUNIT_H__27BB1377_4002_4E88_A114_F06A5FE386F6__INCLUDED_)
#define AFX_CIQSAMPLEUNIT_H__27BB1377_4002_4E88_A114_F06A5FE386F6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CIQSampleUnit.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCIQSampleUnit thread
class CCIQSampleForm;

class CCIQSampleUnit : public CWinThread
{
	DECLARE_DYNCREATE(CCIQSampleUnit)
protected:
	CCIQSampleUnit();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
	CCIQSampleForm* m_main;
	enum {msgStart=WM_USER+1,msgStop};

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCIQSampleUnit)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CCIQSampleUnit();

	// Generated message map functions
	//{{AFX_MSG(CCIQSampleUnit)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	void OnStart();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIQSAMPLEUNIT_H__27BB1377_4002_4E88_A114_F06A5FE386F6__INCLUDED_)
