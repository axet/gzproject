#ifndef COMMANDERIQ_H
#define COMMANDERIQ_H

#include "gamemap.h"

class CUnit;

// командир класс получает команды от основного приложения
//   такие как список пользователей, указатели на другие интерфейсы
//   работа с окнами.
// допольнительно класс командира получает события от карты.
//   такие как перемещение на ней объектов, точек, возможно создание
//   таких объектов, выбор пунктов меню.. это все события от карты.

class CCommanderIQEvents;

class CCommanderIQ: public CGameMapEvents
{
public:
  virtual ~CCommanderIQ() {};

  // работа с интерфейсом пользователя (человека)

  // требует от командира заголовка она, это окно являеться
  // окном управления. через него пользователь будет управлять
  // программой
  virtual HWND FormCreate(HWND parent,CGameMap*,CCommanderIQEvents*) {return 0;}
  // повторный запрос
  virtual HWND FormGet() {return 0;}
  // возвращает имя этого модуля, то как оно будет отображаться
  //   в идалоге выбора командира
  virtual const char* FormGetName() {return 0;}

  // работа с персонажами

  // добавляет пользователя в подченение командиру
  virtual bool UnitAdd(CUnit* ) {return false;}
  // удаляет пользователя
  virtual bool UnitRemove(CUnit* ) {return false;}
  // возвращает поличество пользователей на контроле данного командира
  virtual int UnitGetCount() {return 0;}
  // возвращает пользователя по номеру
  virtual CUnit* UnitGet(int index) {return 0;}

  // обмен данными

  // сохранение/чтение настроек (когда формируеться сейф файл)
  virtual std::string Save() {return std::string();}
  virtual void Load(const char*) {}
};

// события от этого командира, которые он хочет передать основному приложению

class CCommanderIQEvents
{
public:
  //установить состояния изменение, пользователю будет предложено
  // сохранить изменения, вызоветься CCommanderIQ::Serialize("..",true);
  virtual void SetModified(bool b = true) = 0;
};

#endif
