#include <vector>

typedef std::vector<unsigned> UnsignedList;
typedef std::vector< std::pair<unsigned,unsigned> > NumPairList;
typedef std::vector<short> ShortList;
