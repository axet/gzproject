#ifndef __CLIENTGAMEEVENTS__H__
#define __CLIENTGAMEEVENTS__H__

class ItemType;

class CClientGameEvents
{
public:
  // игрок хочет выбрать для ГЗ приложения предмет
  virtual void SelectedItem(unsigned itemserial) {}
  // игрок выберает клеточку на земле
  virtual void SelectedGround(unsigned x,unsigned y, const ItemType &) {}
  // игрок выберает  путь персонажа
  virtual void SelectedPath(unsigned x,unsigned y) {};
  // выбор из окошка создания предмета
  virtual void SelectedOption(const ItemType &) {};
};


#endif
