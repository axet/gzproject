#ifndef __UNIT_H_
#define __UNIT_H_

#include "clientgamecommands.h"

class CClientGameEvents;

class CUnit:public ClientGameCommands::Control
{
public:
  // подписаться на события
  virtual void AddAdvise(CClientGameEvents*) {}
  // отписаться
  virtual void RemoveAdvise(CClientGameEvents*) {};
  // получить координаты игрока
  virtual void GetUnitPos(int *x,int *y) {};
};

#endif
