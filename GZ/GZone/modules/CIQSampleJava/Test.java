/*
 * Test.java
 *
 * Created on 5 ������ 2005 �., 21:47
 */

package GZone.modules.CIQSampleJava;

import java.net.*;
import java.util.jar.*;
import java.applet.*;
import java.io.*;

/**
 *
 * @author  Axet
 */
public class Test {
    
    /** Creates a new instance of Test */
    public Test() {
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try
        {
            URL env=Test.class.getResource("../../../_debug/Environment.jar");
            URL url=Test.class.getResource("../../../_debug/CIQSampleJava.jar");
            URLClassLoader uc=new URLClassLoader(new URL[]{env,url});
            JarFile f=new JarFile(new URI(url.getPath()).getPath());
            Manifest m=f.getManifest();
            Attributes a=m.getMainAttributes();
            String mm=a.getValue("Main-Class");
            Class c=uc.loadClass(mm);
            Applet o=(Applet)c.newInstance();

            sun.awt.windows.WEmbeddedFrame frame=new sun.awt.windows.WEmbeddedFrame(0);
            frame.show();
            frame.add(o);
            o.init();
            o.start();
            frame.setSize(300, 300);
            frame.doLayout();
        }catch(Exception e)
        {
            ByteArrayOutputStream out=new ByteArrayOutputStream();
            PrintStream ps=new PrintStream(out);
            e.printStackTrace(ps);
            String sm=out.toString();
            System.out.print(sm);
            e.printStackTrace();
        }
    }
    
}
