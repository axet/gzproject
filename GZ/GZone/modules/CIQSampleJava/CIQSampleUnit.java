/*
 * CIQSampleUnit.java
 *
 * Created on 6 ������ 2005 �., 20:04
 */

package GZone.modules.CIQSampleJava;

import java.awt.Point;
import java.util.ArrayList;

import client.Types;
import client.Unit;

/**
 *
 * @author  Axet
 */
public class CIQSampleUnit implements Runnable {
    
	ArrayList m_pl;
	Unit m_unit;
	String m_s;
	
    /** Creates a new instance of CIQSampleUnit */
    public CIQSampleUnit(Unit u,ArrayList p,String s) {
    	m_pl=p;
    	m_unit=u;
    	m_s=s;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    public void run() {
    	for(int i=0;i<m_pl.size();i++)
    	{
    		Types.WorldCord w=m_unit.GetUnitPos();
    		Point p=(Point)m_pl.get(i);
    		m_unit.CharMoveTo(new Types.WorldCord(p.x,p.y,w.z));
    		m_unit.CharSpeech(m_s);
    	}
    }
    
}
