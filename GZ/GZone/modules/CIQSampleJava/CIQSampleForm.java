package GZone.modules.CIQSampleJava;

import java.applet.Applet;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;

import GZone.CommanderIQ;
import GZone.GameMap;
import client.ClientGameEvents;
import client.Types;
import client.Unit;

import java.awt.GridBagConstraints;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;
import java.awt.GridBagLayout;

public class CIQSampleForm extends Applet implements CommanderIQ,GameMap.GameMapEvents,ClientGameEvents{

	GameMap m_gamemap;
    Unit m_unit;  //  @jve:decl-index=0:
    CommanderIQ.CommanderIQEvents m_iqevents;
    ArrayList m_pointlist;
    Random m_random=new Random();
    GameMap.line m_pointlistline;
    ButtonGroup m_group;
    Thread m_th=null;
	
	private JButton jButton = null;
	private JLabel jLabel = null;
	private JTextField jTextField = null;
	private JRadioButton jRadioButton = null;
	private JRadioButton jRadioButton1 = null;
	private JButton jButton1 = null;
	
	/**
	 * This is the default constructor
	 */
	public CIQSampleForm() {
		super();

		init();
		
		jTextField.setText("Hello World!");
	
        m_group=new ButtonGroup();
		this.setLayout(new GridBagLayout());
	    m_group.add(jRadioButton);
	    m_group.add(jRadioButton1);
	    
	    jRadioButton.setSelected(true);
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	public void init() {
		GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
		gridBagConstraints5.gridx = 2;
		gridBagConstraints5.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints5.gridwidth = 1;
		gridBagConstraints5.weightx = 1.0;
		gridBagConstraints5.weighty = 1.0;
		gridBagConstraints5.insets = new java.awt.Insets(5,5,5,5);
		gridBagConstraints5.gridy = 3;
		GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
		gridBagConstraints4.gridx = 1;
		gridBagConstraints4.weightx = 1.0;
		gridBagConstraints4.weighty = 1.0;
		gridBagConstraints4.gridy = 3;
		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.gridx = 0;
		gridBagConstraints3.weightx = 1.0;
		gridBagConstraints3.weighty = 1.0;
		gridBagConstraints3.gridy = 3;
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints2.gridy = 2;
		gridBagConstraints2.weightx = 1.0;
		gridBagConstraints2.gridwidth = 3;
		gridBagConstraints2.weighty = 1.0;
		gridBagConstraints2.insets = new java.awt.Insets(5,5,5,5);
		gridBagConstraints2.gridx = 0;
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.gridx = 0;
		gridBagConstraints1.gridwidth = 3;
		gridBagConstraints1.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints1.weightx = 1.0;
		gridBagConstraints1.weighty = 1.0;
		gridBagConstraints1.insets = new java.awt.Insets(5,5,5,5);
		gridBagConstraints1.gridy = 1;
		jLabel = new JLabel();
		jLabel.setText("Speech text");
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridwidth = 3;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(5,5,5,5);
		gridBagConstraints.gridy = 0;
		this.setLayout(new GridBagLayout());
		this.setBackground(java.awt.SystemColor.control);
		this.setBounds(new java.awt.Rectangle(0,0,314,206));
		this.add(getJButton(), gridBagConstraints);
		this.add(jLabel, gridBagConstraints1);
		this.add(getJTextField(), gridBagConstraints2);
		this.add(getJRadioButton(), gridBagConstraints3);
		this.add(getJRadioButton1(), gridBagConstraints4);
		this.add(getJButton1(), gridBagConstraints5);
	}

	void MakePath()
	{
		client.Types.WorldCord p=m_unit.GetUnitPos();
        
        int step=0;
        if(jRadioButton.isSelected())
            step=3;
        if(jRadioButton1.isSelected())
            step=6;

        MakePath(step);

        ReloadMap();
    }
	
	void MakePath(int stepcount)
    {
        client.Types.WorldCord p=m_unit.GetUnitPos();

        m_pointlist=new ArrayList();

        int steplen=10;

        int direction=m_random.nextBoolean()?-1:1;

        while(stepcount-- > 0)
        {
            Point pp=new Point();
            pp.x=p.x;
            pp.y=p.y;
            pp.x+=direction*m_random.nextInt(steplen)+1;
            pp.y+=direction*m_random.nextInt(steplen)+1;
            m_pointlist.add(pp);
        }
    }
    
    void ReloadMap()
    {
        if(m_pointlistline!=null)
            m_gamemap.DeleteLine(m_pointlistline);
        m_pointlistline=m_gamemap.AddLine();

        for(int i=0;i<m_pointlist.size();i++)
        {
            Point pp=(Point)m_pointlist.get(i);
            GameMap.point point=m_gamemap.AddPoint(pp.x,pp.y,m_pointlistline);
            m_gamemap.SetPointData(point,i);
        }
    }

    public void start()
    {
        super.start();
        
        repaint();
    }
    
    public void FormCreate(java.awt.Component parent, GameMap g, CommanderIQ.CommanderIQEvents c) throws Exception{
        m_gamemap=g;
        m_gamemap.AddAdvise(this);
        m_iqevents=c;
    }
    
    public String FormGetName() {
        return "CIQSampleForm Java";
    }
    
    public void Load(String s) throws Exception {
        if(s.compareTo("SaveFormDataInXml")!=0)
            throw new Exception("Bad Data");
    }
    
    public void PointHiLight(GZone.GameMap.point p) {
    }
    
    public void PointMoveAfter(GZone.GameMap.point p) {
    }
    
    public boolean PointMoveBefore(GZone.GameMap.point p) {
        return false;
    }
    
    public void PointSelect(GZone.GameMap.point p) {
    }
    
    public String Save() {
        return "SaveFormDataInXml";
    }
    
    public boolean UnitAdd(client.Unit u) {
        if(m_unit==null)
        {
            m_unit=u;
            m_unit.AddAdvise(this);
            client.Types.WorldCord w=m_unit.GetUnitPos();
            m_gamemap.SetViewPos(w.x,w.y);
            return true;
        }
        return false;
    }
    
    public client.Unit UnitGet(int index) {
        if(index==0)
            return m_unit;
        return null;
    }
    
    public int UnitGetCount() {
        return m_unit!=null?1:0;
    }
    
    public boolean UnitRemove(client.Unit u) {
        if(m_unit==u)
        {
        	m_unit.RemoveAdvise(this);
            m_unit=null;
            return true;
        }
        return false;
    }
    
    public void SelectedGround(Types.WorldCord w, client.ItemType itemType) {
    }
    
    public void SelectedItem(int param) {
    }
    
    public void SelectedOption(client.ItemType itemType) {
    }
    
    public void SelectedPath(Types.WorldCord w) {
    	m_gamemap.SetCursor(w.x,w.y);
    }

	/**
	 * This method initializes jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton() {
		if (jButton == null) {
			jButton = new JButton();
			jButton.setText("Start");
			jButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
			        if(m_th!=null)
			        	Thread.yield();
			        m_th=new Thread(new CIQSampleUnit(m_unit,m_pointlist,jTextField.getText()));
			        m_th.start();
				}
			});
		}
		return jButton;
	}

	/**
	 * This method initializes jTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextField() {
		if (jTextField == null) {
			jTextField = new JTextField();
		}
		return jTextField;
	}

	/**
	 * This method initializes jRadioButton	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButton() {
		if (jRadioButton == null) {
			jRadioButton = new JRadioButton();
			jRadioButton.setText("3");
		}
		return jRadioButton;
	}

	/**
	 * This method initializes jRadioButton1	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioButton1() {
		if (jRadioButton1 == null) {
			jRadioButton1 = new JRadioButton();
			jRadioButton1.setText("6");
		}
		return jRadioButton1;
	}

	/**
	 * This method initializes jButton1	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new JButton();
			jButton1.setText("Make Path");
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					MakePath();
				}
			});
		}
		return jButton1;
	}
	
}  //  @jve:decl-index=0:visual-constraint="10,10"
