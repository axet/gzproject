// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// SoldierListView.cpp : implementation file
//

#include "stdafx.h"
#include "GZone.h"
#include "SoldierListView.h"
#include "editunitdlg.h"
#include "gzonedoc.h"
#include "MainFrm.h"
#include "OptionsDlg.h"
#include "gzunit/InterruptableUnit.h"

extern OptionsDlg g_optionsdlg;

#include <exception>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSoldierListView

CSoldierListView::CSoldierListView():m_updatelist(false)
{
}

CSoldierListView::~CSoldierListView()
{
  for(UnitList_t::iterator u=App().m_unitlist.begin();u!=App().m_unitlist.end();u++)
  {
    (*u)->RemoveAdvise(this);
  }
}

BEGIN_MESSAGE_MAP(CSoldierListView,CSizingControlBarG)
  ON_NOTIFY(LVN_ITEMCHANGING,0,OnItemChanging)
  ON_MESSAGE(WM_USER+1,OnUpdateUnitList)
  ON_WM_CONTEXTMENU()
  ON_COMMAND(ID_UNIT_NEW, OnUnitNew)
  ON_COMMAND(ID_UNIT_RUNCLIENT, OnUnitRunclient)
  ON_COMMAND(ID_UNIT_DELETE, OnUnitDelete)
  ON_COMMAND(ID_UNIT_CLOSECLIENT, OnUnitCloseclient)
  ON_COMMAND(ID_UNIT_PROPERTIES, OnUnitProperties)
  ON_COMMAND(ID_UNIT_SHOWCLIENT, OnUnitShowclient)
  ON_COMMAND(ID_UNIT_HIDECLIENT, OnUnitHideclient)
  ON_UPDATE_COMMAND_UI(ID_UNIT_DELETE, OnUpdateUnitDelete)
  ON_UPDATE_COMMAND_UI(ID_UNIT_CLOSECLIENT, OnUpdateUnitCloseclient)
  ON_UPDATE_COMMAND_UI(ID_UNIT_RUNCLIENT, OnUpdateUnitRunclient)
  ON_UPDATE_COMMAND_UI(ID_UNIT_NEW, OnUpdateUnitNew)
  ON_UPDATE_COMMAND_UI(ID_UNIT_SHOWCLIENT, OnUpdateShowClient)
  ON_UPDATE_COMMAND_UI(ID_UNIT_HIDECLIENT, OnUpdateHideClient)
  ON_UPDATE_COMMAND_UI(ID_UNIT_PROPERTIES, OnUpdateUnitProperties)
  ON_WM_LBUTTONDBLCLK()
  ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

void CSoldierListView::Create(CWnd* p,int i)
{
  m_dwSCBStyle|=SCBS_SIZECHILD;

  CSizingControlBarG::Create("Units list", p, i);
  m_list.Create(WS_VISIBLE|WS_CHILD|LVS_SMALLICON,CRect(0,0,0,0),this,0);
  m_list.ModifyStyleEx(0,WS_EX_CLIENTEDGE);

  m_menu.LoadMenu(IDR_MENU_UNIT);

  SetMenu(&m_menu);

  m_bm.LoadBitmap(IDB_BITMAP_UNIT);
  m_il.Create(16,16,ILC_COLORDDB|ILC_MASK,0,0);
  m_il.Add(&m_bm,RGB(255,255,255));

  m_list.SetImageList(&m_il,LVSIL_SMALL);
  m_list.SetExtendedStyle(LVS_EX_CHECKBOXES);

  for(UnitList_t::iterator u=App().m_unitlist.begin();u!=App().m_unitlist.end();u++)
  {
    (*u)->AddAdvise(this);
  }

  ReloadList();
}

void CSoldierListView::ReloadList()
{
  m_list.DeleteAllItems();
  for(UnitList_t::iterator i=App().m_unitlist.begin();i!=App().m_unitlist.end();i++)
  {
    AddItem((*i));
  }
}

LRESULT CSoldierListView::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
  return CSizingControlBarG::WindowProc(message,wParam,lParam);
}

/////////////////////////////////////////////////////////////////////////////
// CSoldierListView message handlers

void CSoldierListView::OnUnitNew() 
{
  CEditUnitDlg dlg;
  dlg.m_edit=FALSE;
  if(dlg.DoModal()!=IDOK)
    return;

  CGZUnit *p=new CGZUnit(dlg.m_login,dlg.m_password,dlg.m_charname,dlg.m_showwindow==1);
  p->AddAdvise(this);
  p->AddAdvise((CMainFrame*)AfxGetMainWnd());
  App().m_unitlist.push_back(p);
  AddItem(p);
}

void CSoldierListView::OnUnitRunclient() 
{
  int i=-1;
  while((i=m_list.GetNextItem(i,LVNI_SELECTED))!=-1)
  {
    CGZUnit* p=reinterpret_cast<CGZUnit*>(m_list.GetItemData(i));
    if(p==0)
      continue;
    if(p->IsConnected())
      continue;

    p->Login(0,0,0,(const char*)g_optionsdlg.clientpath);

    //UpdateItem(i,p);
  }
}

void CSoldierListView::OnUnitDelete() 
{
  int i=-1;
  while((i=m_list.GetNextItem(i,LVNI_SELECTED))!=-1)
  {
    CGZUnit* p=reinterpret_cast<CGZUnit*>(m_list.GetItemData(i));
    if(p==0)
      continue;
    if(IsUserInUse(p))
    {
      AfxMessageBox("Login in use");
      continue;
    }

    for(UnitList_t::iterator i=App().m_unitlist.begin();i!=App().m_unitlist.end();i++)
    {
      if(*i==p)
      {
        delete *i;
        App().m_unitlist.erase(i);
        break;
      }
    }
  }

  ReloadList();
}

void CSoldierListView::OnUnitCloseclient() 
{
  int i=-1;
  while((i=m_list.GetNextItem(i,LVNI_SELECTED))!=-1)
  {
    CGZUnit* p=reinterpret_cast<CGZUnit*>(m_list.GetItemData(i));
    if(p==0)
      continue;
    if(!p->IsConnected())
      continue;
    p->Close();

    UpdateItem(i,p);
  }
}

void CSoldierListView::OnContextMenu(CWnd* pWnd, CPoint pt) 
{
  CMenu *p=m_menu.GetSubMenu(0);

  for(int i=0;i<p->GetMenuItemCount();i++)
  {
    CCmdUI ui;
    ui.m_pMenu=p;
    ui.m_nID=p->GetMenuItemID(i);
    ui.m_nIndex=i;
    ui.m_nIndexMax=i+1;
    ui.DoUpdate(this,TRUE);
  }
  p->TrackPopupMenu(TPM_RIGHTBUTTON,pt.x,pt.y,this,0);
}

void CSoldierListView::OnUpdateUnitDelete(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(FALSE);
  int i=-1;
  while((i=m_list.GetNextItem(i,LVNI_SELECTED))!=-1)
  {
    CGZUnit* p=reinterpret_cast<CGZUnit*>(m_list.GetItemData(i));
    if(!p->IsConnected())
      pCmdUI->Enable(TRUE);
  }
}

void CSoldierListView::OnUpdateUnitCloseclient(CCmdUI* pCmdUI) 
{
  int i=-1;
  pCmdUI->Enable(FALSE);
  while((i=m_list.GetNextItem(i,LVNI_SELECTED))!=-1)
  {
    CGZUnit* p=reinterpret_cast<CGZUnit*>(m_list.GetItemData(i));
    if(p->IsConnected())
      pCmdUI->Enable(TRUE);
  }
}

void CSoldierListView::OnUpdateUnitRunclient(CCmdUI* pCmdUI) 
{
  int i=-1;
  pCmdUI->Enable(FALSE);
  while((i=m_list.GetNextItem(i,LVNI_SELECTED))!=-1)
  {
    CGZUnit* p=reinterpret_cast<CGZUnit*>(m_list.GetItemData(i));
    if(!p->IsConnected())
      pCmdUI->Enable(TRUE);
  }
}

void CSoldierListView::OnUpdateUnitNew(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CSoldierListView::AddItem(CGZUnit *p)
{
  m_updatelist=true;
  int item=m_list.InsertItem(m_list.GetItemCount(),0,0);
  UpdateItem(item,p);
  m_updatelist=false;
}

long CSoldierListView::OnUpdateUnitList(unsigned,long)
{
  UpdateList();
  return 0;
}

bool CSoldierListView::IsUserInUse(CGZUnit *p)
{
  POSITION doctemplpos=App().GetFirstDocTemplatePosition();
  while(doctemplpos)
  {
    CDocTemplate* doctempl=App().GetNextDocTemplate(doctemplpos);
    POSITION docpos=doctempl->GetFirstDocPosition();
    while(docpos!=0)
    {
      CGZoneDoc* doc=(CGZoneDoc*)doctempl->GetNextDoc(docpos);
      for(int i=0;i<doc->m_commander->UnitGetCount();i++)
      {
        if(doc->m_commander->UnitGet(i)==p)
          return true;
      }
    }
  }
  return false;
}

void CSoldierListView::UpdateItem(int item,CGZUnit* p)
{
  LVITEM lvi={0};
  lvi.iItem=item;
  lvi.iImage=GetImage(p);
  CGZUnit::LInfo l=p->GetLInfo();
  std::string name=std::string(l.login.c_str())+" ["+l.charname.c_str()+"]";
  lvi.pszText=const_cast<char*>(name.c_str());
  lvi.mask=LVIF_TEXT|LVIF_PARAM|LVIF_IMAGE;
  lvi.lParam=reinterpret_cast<DWORD>(p);
  m_list.SetItem(&lvi);
}

int CSoldierListView::GetImage(CGZUnit*p)
{
  if(p->IsLoggedIn())
    return 2;
  if(p->IsConnected())
    return 1;
  return 0;
}

void CSoldierListView::OnUnitProperties() 
{
  int i=-1;
  while((i=m_list.GetNextItem(i,LVNI_SELECTED))!=-1)
  {
    CGZUnit* p=reinterpret_cast<CGZUnit*>(m_list.GetItemData(i));
    if(p==0)
      continue;

    CEditUnitDlg dlg;
    dlg.m_edit=TRUE;

    CGZUnit::LInfo l=p->GetLInfo();
    dlg.m_login=l.login.c_str();
    dlg.m_password=l.password.c_str();
    dlg.m_charname=l.charname.c_str();
    dlg.m_showwindow=p->IsWindowVisible();

    if(dlg.DoModal()!=IDOK)
      continue;

    p->SetShowWindow(dlg.m_showwindow==1);
    l.password=dlg.m_password;
    l.login=dlg.m_login;
    l.charname=dlg.m_charname;

    p->SetLInfo(l);

    UpdateItem(i,p);
  }
}

void CSoldierListView::OnUpdateUnitProperties(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(m_list.GetSelectedCount()==1);
}

void CSoldierListView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	OnUnitProperties();
	//CSizingControlBarG::OnLButtonDblClk(nFlags, point);
}

void CSoldierListView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CSizingControlBarG::OnLButtonUp(nFlags, point);
}

void CSoldierListView::OnItemChanging(NMHDR* ui, LRESULT* ui2)
{
  NMLISTVIEW * p=( NMLISTVIEW*)ui;

  CMDIFrameWnd* frame=(CMDIFrameWnd*)AfxGetMainWnd();
  if(frame==0)
    return;
  CMDIChildWnd *mdi=frame->MDIGetActive();
  CGZoneDoc* doc;
  if(mdi==0)
    goto cancel;
  doc=(CGZoneDoc*)mdi->GetActiveDocument();
  if(doc==0)
    goto cancel;
  if(doc->m_commander==0)
    goto cancel;

  if(!m_updatelist)
  {
    if(p->uChanged&LVIF_STATE&&p->uNewState&0x2000)
    {// установлена галачка
      CGZUnit* gzunit=(CGZUnit*)p->lParam;
      if(!doc->UnitToCommander(gzunit))
        goto cancel;
    }else if(p->uChanged&LVIF_STATE&&p->uNewState&0x1000)
    {
      CGZUnit* gg=(CGZUnit*)p->lParam;
      if(!doc->UnitFromCommander(gg))
        goto cancel;
    }
  }
  return;

cancel:
  if(m_updatelist)
    return;

  if((*p).uChanged&LVIF_STATE&&p->uNewState&0x2000)
  {// установлена галачка
    *ui2=1;
  }else if((*p).uChanged&LVIF_STATE&&p->uNewState&0x1000)
  {// снята галочка
    *ui2=1;
  }
}

void CSoldierListView::UpdateList()
{
  CMDIFrameWnd *frame=(CMDIFrameWnd*)AfxGetMainWnd();
  if(frame==0)
    return;
  CMDIChildWnd *mdi=(frame)->MDIGetActive();

  CGZoneDoc* doc=0;
  if(mdi!=0)
    doc=(CGZoneDoc*)mdi->GetActiveDocument();

  m_updatelist=true;
  for(int i=0;i<m_list.GetItemCount();i++)
  {
    BOOL b=FALSE;
    if(doc!=0&&doc->m_commander!=0)
    {
      CGZUnit* iunit=(CGZUnit*)m_list.GetItemData(i);

      for(int k=0;k<doc->m_commander->UnitGetCount()&&!b;k++)
      {
        b=doc->m_commander->UnitGet(k)==iunit;
      }
    }
    UpdateItem(i,(CGZUnit*)m_list.GetItemData(i));
    m_list.SetCheck(i,b);
  }
  m_updatelist=false;
}

void CSoldierListView::OnUnitHideclient() 
{
  int i=-1;
  while((i=m_list.GetNextItem(i,LVNI_SELECTED))!=-1)
  {
    CGZUnit* p=reinterpret_cast<CGZUnit*>(m_list.GetItemData(i));
    if(p==0)
      continue;
    p->ShowWindow(false);
  }
}

void CSoldierListView::OnUnitShowclient() 
{
  int i=-1;
  while((i=m_list.GetNextItem(i,LVNI_SELECTED))!=-1)
  {
    CGZUnit* p=reinterpret_cast<CGZUnit*>(m_list.GetItemData(i));
    if(p==0)
      continue;
    p->ShowWindow(true);
  }
}

void CSoldierListView::OnUpdateShowClient(CCmdUI* pCmdUI)
{
  int visiblecount=0;
  int connectedcount=0;

  int i=-1;
  while((i=m_list.GetNextItem(i,LVNI_SELECTED))!=-1)
  {
    CGZUnit* p=reinterpret_cast<CGZUnit*>(m_list.GetItemData(i));
    if(p==0)
      continue;
    if(p->IsConnected())
      connectedcount++;
    if(p->IsWindowVisible())
      visiblecount++;
  }
  if(connectedcount==0)
    pCmdUI->Enable(FALSE);
  else
  {
    switch(visiblecount)
    {
    case 0:
      pCmdUI->Enable(1);
      break;
    default:
      pCmdUI->Enable(0);
      break;
    }
  }
}

void CSoldierListView::OnUpdateHideClient(CCmdUI* pCmdUI)
{
  int visiblecount=0;
  int connectedcount=0;

  int i=-1;
  while((i=m_list.GetNextItem(i,LVNI_SELECTED))!=-1)
  {
    CGZUnit* p=reinterpret_cast<CGZUnit*>(m_list.GetItemData(i));
    if(p==0)
      continue;
    if(p->IsConnected())
      connectedcount++;
    if(p->IsWindowVisible())
      visiblecount++;
  }
  if(connectedcount==0)
    pCmdUI->Enable(FALSE);
  else
  {
    switch(visiblecount)
    {
    case 0:
      pCmdUI->Enable(0);
      break;
    default:
      pCmdUI->Enable(1);
      break;
    }
  }
}

void CSoldierListView::Connected()
{
  UpdateList();
}

void CSoldierListView::LocalConnectionLost()
{
  UpdateList();
}

void CSoldierListView::ConnectionEstablished(const char* name,const char* pass, const char* charname)
{
  UpdateList();
}
