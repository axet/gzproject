#pragma comment(lib,"jvm.lib")
#pragma comment(lib,"jawt.lib")

#include "JavaLoader.h"
#include "JavaCommanderIQ.h"
#include <atlbase.h>

JavaLoader::JavaLoader(const char* envr)
{
  __JNI_CreateJavaVM=0;
  __JAWT_GetAWT=0;
  hmjvm=0;
  hmjawt=0;

  {
    char buf[1024];
    CRegKey key(HKEY_LOCAL_MACHINE);
    const char* JP="SOFTWARE\\JavaSoft\\Java Runtime Environment";
    if(key.Open(HKEY_LOCAL_MACHINE,JP)==ERROR_SUCCESS)
    {
      ULONG bufl=sizeof(buf);
      key.QueryStringValue("CurrentVersion",buf,&bufl);
      std::string jrepath=std::string(JP)+"\\"+buf;
      if(key.Open(HKEY_LOCAL_MACHINE,jrepath.c_str())==ERROR_SUCCESS)
      {
        ULONG bufl=sizeof(buf);
        key.QueryStringValue("RuntimeLib",buf,&bufl);
        m_jvmdll=buf;
        bufl=sizeof(buf);
        key.QueryStringValue("JavaHome",buf,&bufl);
        m_awtdll=std::string(buf)+"\\bin\\jawt.dll";
      }
    }
  }

  hmjvm=LoadLibrary(m_jvmdll.c_str());
  if(hmjvm==0)
    throw JavaLoadExceptionNoLibFound();
  __JNI_CreateJavaVM=(_JNI_CreateJavaVM)GetProcAddress(hmjvm,"JNI_CreateJavaVM");
  if(__JNI_CreateJavaVM==0)
    throw JavaLoadException();

  //hmenv=LoadLibrary("Environment.dll");

  hmjawt=LoadLibrary(m_awtdll.c_str());
  if(hmjawt==0)
    throw JavaLoadExceptionNoLibFound();
  __JAWT_GetAWT=(_JAWT_GetAWT)GetProcAddress(hmjawt,"_JAWT_GetAWT@8");
  if(__JAWT_GetAWT==0)
    throw JavaLoadException();

  // java parsing command line bug
  std::string str="-Xrunjdwp:transport=dt_socket,server=y,address=8000,suspend=n";

  JavaVMOption options[]=
  {
    {"-Xdebug",0},
    {"-Xnoagent",0},
    {"-Djava.compiler=NONE",0},
    {(char*)str.c_str(),0}
  };

  JavaVMInitArgs vm_args;
  vm_args.version = JNI_VERSION_1_4;
  vm_args.options = options;
  vm_args.nOptions = sizeof(options)/sizeof(JavaVMOption);
  vm_args.ignoreUnrecognized = TRUE;

  jint res=__JNI_CreateJavaVM(&vm, (void **)&m_base.env, &vm_args);
  if (res  < 0)
    throw JavaLoadException();

  jboolean result;
  awt.version = JAWT_VERSION_1_4;
  result = __JAWT_GetAWT(m_base.env, &awt);
  if(result == JNI_FALSE)
    throw JavaLoadException();

  m_base.awt=&awt;
  m_base.environment=&m_environment;

  m_environment.Create(m_base.env,envr);
  m_gamemapmanager.Create(&m_base);
  m_unitnativemanager.Create(&m_base);

  jclass environmentclass=m_environment.LoadClass("GZone.Environment.Environment");
  jmethodID environmentmain=m_base.env->GetStaticMethodID(environmentclass,"main","()V");
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);
  m_base.env->CallStaticVoidMethod(environmentclass,environmentmain);
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);

  HMODULE h=GetModuleHandle("Environment.dll");
  typedef void  (*_SetUnitNativeManager)(class UnitNativeManager *);
  _SetUnitNativeManager SetUnitNativeManager=(_SetUnitNativeManager)GetProcAddress(h,"SetUnitNativeManager");
  SetUnitNativeManager(&m_unitnativemanager); 
  //NO NEED FreeLibrary (h);

  typedef void  (*_SetGameMapManager)(class GameMapManager *);
  _SetGameMapManager SetGameMapManager=(_SetGameMapManager)GetProcAddress(h,"SetGameMapManager");
  SetGameMapManager(&m_gamemapmanager); 
}

JavaLoader::~JavaLoader()
{
  FreeLibrary(hmjvm);
  FreeLibrary(hmjawt);
  FreeLibrary(hmenv);
}

CCommanderIQ* JavaLoader::LoadJar(const char*jar)
{
  _bstr_t jarpathbstr=_bstr_t(jar);
  jstring jarpathstr=m_base.env->NewString((jchar*)jarpathbstr.GetBSTR(),jarpathbstr.length());
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);

  //JarFile f=new JarFile(url.getFile());
  jclass fclass=m_base.env->FindClass("java/util/jar/JarFile");
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);
  jmethodID finit=m_base.env->GetMethodID(fclass,"<init>","(Ljava/lang/String;)V");
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);
  jobject fobj=m_base.env->NewObject(fclass,finit,jarpathstr);
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);

  //Manifest m=f.getManifest();
  jclass mclass=m_base.env->FindClass("java/util/jar/Manifest");
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);
  jmethodID fgetManifest=m_base.env->GetMethodID(fclass,"getManifest","()Ljava/util/jar/Manifest;");
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);
  jobject mobj=m_base.env->CallObjectMethod(fobj,fgetManifest);
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);
  if(mobj==0)
    throw JavaProcessException("NO Manifest file");

  //Attributes a=m.getMainAttributes();
  jclass aclass=m_base.env->FindClass("java/util/jar/Attributes");
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);
  jmethodID mgetMainAttributes=m_base.env->GetMethodID(mclass,"getMainAttributes","()Ljava/util/jar/Attributes;");
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);
  jobject aobj=m_base.env->CallObjectMethod(mobj,mgetMainAttributes);
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);

  //String mm=a.getValue("Main-Class");
  jmethodID agetValue=m_base.env->GetMethodID(aclass,"getValue","(Ljava/lang/String;)Ljava/lang/String;");
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);
  jstring mcstr=m_base.env->NewStringUTF("Main-Class");
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);
  jstring mmstr=(jstring)m_base.env->CallObjectMethod(aobj,agetValue,mcstr);
  if(m_base.env->ExceptionOccurred()!=0)
    throw JavaProcessException(m_base.env);
  if(mmstr==0)
    throw JavaProcessException("No Main-Class Attribute in Manifest file");
  jboolean mmstrcopy=JNI_FALSE;
  _bstr_t bb((const wchar_t*)m_base.env->GetStringChars(mmstr,&mmstrcopy));

  return new JavaCommanderIQ(&m_base,jar,bb,&m_unitnativemanager,&m_gamemapmanager);
};
