#ifndef __JAVABASE_H__
#define __JAVABASE_H__

#include <jni.h>
#include <jni_md.h>
#include <jawt_md.h>
#include <jawt.h>

#include "JavaException.h"
#include "JarLoader.h"

struct JavaBase
{
  JNIEnv *env;
  JAWT*awt;
  jobject appletobj;
  jclass    classloaderclass;
  jobject   classloaderobj;
  jmethodID classloaderloadClass;
  JarLoader *environment;
};

#endif
