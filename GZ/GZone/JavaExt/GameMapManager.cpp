#include "GameMapManager.h"

void GameMapManager::Create(JavaBase * b)
{
  m_base=b;
  gamemapnativeclass=m_base->environment->LoadClass("GZone.GameMapNative");
  gamemapnativeinit=m_base->env->GetMethodID(gamemapnativeclass,"<init>","()V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  lineclass=m_base->env->FindClass("java/lang/Object");
  lineinit=m_base->env->GetMethodID(lineclass,"<init>","()V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
}

GameMapManager::~GameMapManager()
{
}

jobject GameMapManager::GetJavaMap(CGameMap *u)
{
  map::iterator i=m_map.find(u);
  if(i==m_map.end())
  {
    jobject gamemapobj=m_base->env->NewObject(gamemapnativeclass,gamemapnativeinit);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    //gamemapobj=m_base->env->NewGlobalRef(gamemapobj);
    //if(m_base->env->ExceptionOccurred()!=0)
    //  throw JavaException(m_base->env);

    m_map.insert(map::value_type(u,gamemapobj));
    return gamemapobj;
  }else
  {
    return i->second;
  }
}

CGameMap* GameMapManager::GetCPPMap(jobject o)
{
  for(map::iterator i=m_map.begin();i!=m_map.end();i++)
  {
    if(m_base->env->IsSameObject(i->second,o)==JNI_TRUE)
    {
      return i->first;
    }
  }
  throw std::exception("no map found");
}

CGameMapEvents* GameMapManager::GetCPPEvents(jobject o)
{
  for(mapev::iterator i=m_mapev.begin();i!=m_mapev.end();i++)
  {
    if(m_base->env->IsSameObject(i->second,o)==JNI_TRUE)
    {
      return i->first;
    }
  }
  throw std::exception("no mapev found");
}

void GameMapManager::AddCPPEvents(CGameMapEvents*p,jobject o)
{
  m_mapev.insert(mapev::value_type(p,o));
}

jobject GameMapManager::AddLine(CGameMap::line*l)
{
  jobject lineobj=m_base->env->NewObject(lineclass,lineinit);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  m_mapline.insert(mapline::value_type(l,lineobj));
  return lineobj;
}

jobject GameMapManager::AddPoint(CGameMap::point*l)
{
  jobject lineobj=m_base->env->NewObject(lineclass,lineinit);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  mappoint::iterator i=m_mappoint.find(l);
  if(i==m_mappoint.end())
  {
    m_mappoint.insert(mappoint::value_type(l,lineobj));
  }else
  {
    m_base->env->DeleteLocalRef(i->second);
    i->second=lineobj;
  }
  return lineobj;
}

CGameMap::line* GameMapManager::GetLine(jobject o)
{
  for(mapline::iterator i=m_mapline.begin();i!=m_mapline.end();i++)
  {
    if(m_base->env->IsSameObject(i->second,o)==JNI_TRUE)
    {
      return i->first;
    }
  }
  throw std::exception("no mapline found");
}

CGameMap::point* GameMapManager::GetPoint(jobject o)
{
  for(mappoint::iterator i=m_mappoint.begin();i!=m_mappoint.end();i++)
  {
    if(m_base->env->IsSameObject(i->second,o)==JNI_TRUE)
    {
      return i->first;
    }
  }
  throw std::exception("no mappoint found");
}

jobject GameMapManager::GetPoint(CGameMap::point *pp)
{
  mappoint::iterator i=m_mappoint.find(pp);
  if(i==m_mappoint.end())
  {
    throw std::exception("no mappoint found");
  }else
  {
    return i->second;
  }
}

void GameMapManager::DeleteLine(CGameMap::line* line)
{
  mapline::iterator i=m_mapline.find(line);
  if(i==m_mapline.end())
  {
    throw std::exception("no DeleteLine found");
  }else
  {
    m_base->env->DeleteLocalRef(i->second);
    m_mapline.erase(i);
  }
}

jobject GameMapManager::GetLine(CGameMap::line* line)
{
  mapline::iterator i=m_mapline.find(line);
  if(i==m_mapline.end())
  {
    throw std::exception("no DeleteLine found");
  }else
  {
    //m_base->env->DeleteLocalRef(i->second);
    //m_mapline.erase(i);
    return i->second;
  }
}

void GameMapManager::DeletePoint(CGameMap::point* point)
{
  mappoint::iterator i=m_mappoint.find(point);
  if(i==m_mappoint.end())
  {
    throw std::exception("no Deletepoint found");
  }else
  {
    m_base->env->DeleteLocalRef(i->second);
    m_mappoint.erase(i);
  }
}
