#include "../commanderiq.h"

#include <jni.h>
#include <jni_md.h>
#include <jawt_md.h>
#include <jawt.h>

#include "JavaBase.h"
#include "JarLoader.h"
#include "UnitNativeManager.h"
#include "GameMapManager.h"
#include "../../client/clientgameevents.h"

class JavaCommanderIQ:public CCommanderIQ,public JarLoader,public CClientGameEvents
{
  JavaBase *m_base;

  std::string m_formname;
  HWND m_wnd;

  jclass    commanderiqclass;
  jclass    gamemapeventsclass;
  jclass clientgameeventsclass;
  jobject appletobj;
  UnitNativeManager *m_unitnativemanager;
  GameMapManager* m_gm;
  CCommanderIQEvents* m_cev;

public:
  JavaCommanderIQ(JavaBase * b, const char* jar,const char* classname,UnitNativeManager *p,GameMapManager*gm);
  virtual ~JavaCommanderIQ();

  // работа с интерфейсом пользователя (человека)

  // требует от командира заголовка она, это окно являеться
  // окном управления. через него пользователь будет управлять
  // программой
  virtual HWND FormCreate(HWND parent,CGameMap*,CCommanderIQEvents*);
  // повторный запрос
  virtual HWND FormGet();
  // возвращает имя этого модуля, то как оно будет отображаться
  //   в идалоге выбора командира
  virtual const char* FormGetName();

  // работа с персонажами

  // добавляет пользователя в подченение командиру
  virtual bool UnitAdd(CUnit* );
  // удаляет пользователя
  virtual bool UnitRemove(CUnit* );
  // возвращает поличество пользователей на контроле данного командира
  virtual int UnitGetCount();
  // возвращает пользователя по номеру
  virtual CUnit* UnitGet(int index);

  // обмен данными

  // сохранение/чтение настроек (когда формируеться сейф файл)
  virtual std::string Save();
  virtual void Load(const char*);


  // точку выделена
  virtual void PointSelect(CGameMap::point*);
  // точку переместили
  virtual void PointMoveAfter(CGameMap::point*);
  // точку хотят переместить
  virtual bool PointMoveBefore(CGameMap::point*);
  // точка подсвечена
  virtual void PointHiLight(CGameMap::point*);

  virtual void SelectedItem(unsigned itemserial);
  // игрок выберает клеточку на земле
  virtual void SelectedGround(WorldCord w, const Client::ItemType &);
  // игрок выберает  путь персонажа
  virtual void SelectedPath(WorldCord w);
  // выбор из окошка создания предмета
  virtual void SelectedOption(const Client::ItemType &);
};
