#ifndef __GameMapManager_H__
#define __GameMapManager_H__

#include "JavaBase.h"

#include <map>

#include "../gamemap.h"

class GameMapManager
{
  JavaBase * m_base;

  jclass gamemapnativeclass;
  jmethodID gamemapnativeinit;

  jclass lineclass;
  jmethodID lineinit;

  typedef std::map< CGameMap *,jobject > map;
  map m_map;

  typedef std::map< CGameMapEvents *,jobject > mapev;
  mapev m_mapev;

  typedef std::map< CGameMap::line*,jobject > mapline;
  mapline m_mapline;

  typedef std::map< CGameMap::point*,jobject > mappoint;
  mappoint m_mappoint;

public:
  void Create(JavaBase * b);
  ~GameMapManager();
  jobject GetJavaMap(CGameMap *);
  CGameMap* GetCPPMap(jobject);
  CGameMapEvents* GetCPPEvents(jobject);
  jobject AddLine(CGameMap::line*);
  jobject GetLine(CGameMap::line*);
  CGameMap::line* GetLine(jobject j);
  void DeleteLine(CGameMap::line*);
  void DeletePoint(CGameMap::point*);
  jobject AddPoint(CGameMap::point*);
  jobject GetPoint(CGameMap::point *pp);
  CGameMap::point *GetPoint(jobject);
  void AddCPPEvents(CGameMapEvents*p,jobject);
};

#endif
