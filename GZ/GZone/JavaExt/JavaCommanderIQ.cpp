// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "stdafx.h"
#include "JavaCommanderIQ.h"
#include "JavaLoader.h"

JavaCommanderIQ::JavaCommanderIQ(JavaBase * b,const char* jar,const char* classname,UnitNativeManager *p,GameMapManager*gm):
  m_base(b),appletobj(0),m_wnd(0),m_unitnativemanager(p),m_gm(gm)
{
  Create(m_base->env,jar,m_base->environment);
  jclass c=LoadClass(classname);
  appletobj=NewInstance(c);

  gamemapeventsclass=m_base->environment->LoadClass("GZone.GameMap$GameMapEvents");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);

  clientgameeventsclass=m_base->environment->LoadClass("client.ClientGameEvents");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);

  commanderiqclass=m_base->environment->LoadClass("GZone.CommanderIQ");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);

  jmethodID commanderiqFormGetName=m_base->env->GetMethodID(commanderiqclass,"FormGetName","()Ljava/lang/String;");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jstring formnamestr=(jstring)m_base->env->CallObjectMethod(appletobj,commanderiqFormGetName);
  jboolean formnamecopy=JNI_FALSE;

  _bstr_t bb((const wchar_t*)m_base->env->GetStringChars(formnamestr,&formnamecopy));
  m_formname=bb;

  m_base->env->DeleteLocalRef(formnamestr);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
};

JavaCommanderIQ::~JavaCommanderIQ()
{
  m_base->env->DeleteLocalRef(commanderiqclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);

  m_base->env->DeleteLocalRef(appletobj);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);  
}

HWND JavaCommanderIQ::FormCreate(HWND parent,CGameMap*p,CCommanderIQEvents*cev)
{
  m_cev=cev;

  jclass embeddedwinclass=m_base->env->FindClass("sun/awt/windows/WEmbeddedFrame");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jmethodID embeddedwininit=m_base->env->GetMethodID(embeddedwinclass,"<init>","(J)V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jobject embeddedwinobj=m_base->env->NewObject(embeddedwinclass,embeddedwininit,(jint)parent);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);

  jmethodID embeddedwinadd=m_base->env->GetMethodID(embeddedwinclass,"add","(Ljava/awt/Component;)Ljava/awt/Component;");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jobject componentobj=m_base->env->CallObjectMethod(embeddedwinobj,embeddedwinadd,appletobj);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);

  jclass appletclass=m_base->env->FindClass("java/applet/Applet");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jmethodID appletinit=m_base->env->GetMethodID(appletclass,"init","()V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  m_base->env->CallVoidMethod(appletobj,appletinit);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jmethodID appletstart=m_base->env->GetMethodID(appletclass,"start","()V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  m_base->env->CallVoidMethod(appletobj,appletstart);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  m_base->env->DeleteLocalRef(appletclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);

  jmethodID embeddedwinshow=m_base->env->GetMethodID(embeddedwinclass,"show","()V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  m_base->env->CallObjectMethod(embeddedwinobj,embeddedwinshow);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);

  m_base->env->DeleteLocalRef(embeddedwinclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);


  JAWT_DrawingSurface* ds;
  JAWT_DrawingSurfaceInfo* dsi;
  JAWT_Win32DrawingSurfaceInfo* dsi_win;
  jboolean result;
  jint lock;

  ds = m_base->awt->GetDrawingSurface(m_base->env, embeddedwinobj);
  if(ds==NULL)
    throw JavaLoader::JavaProcessException();
  lock = ds->Lock(ds);
  if((lock & JAWT_LOCK_ERROR) != 0)
    throw JavaLoader::JavaProcessException();
  dsi = ds->GetDrawingSurfaceInfo(ds);
  if((lock & JAWT_LOCK_ERROR) != 0)
    throw JavaLoader::JavaProcessException();

  dsi_win = (JAWT_Win32DrawingSurfaceInfo*)dsi->platformInfo;
  m_wnd=dsi_win->hwnd;

  ds->FreeDrawingSurfaceInfo(dsi);
  if((lock & JAWT_LOCK_ERROR) != 0)
    throw JavaLoader::JavaProcessException();
  ds->Unlock(ds);
  if((lock & JAWT_LOCK_ERROR) != 0)
    throw JavaLoader::JavaProcessException();
  m_base->awt->FreeDrawingSurface(ds);
  if((lock & JAWT_LOCK_ERROR) != 0)
    throw JavaLoader::JavaProcessException();

  //jclass gamemapnativeclass=m_base->environment->LoadClass("GZone.GameMapNative");
  //if(m_base->env->ExceptionOccurred()!=0)
  //  throw JavaLoader::JavaProcessException(m_base->env);
  //jmethodID gamemapnativeinit=m_base->env->GetMethodID(gamemapnativeclass,"<init>","()V");
  //if(m_base->env->ExceptionOccurred()!=0)
  //  throw JavaLoader::JavaProcessException(m_base->env);
  //jobject gamemapnativeobj=m_base->env->NewObject(gamemapnativeclass,gamemapnativeinit);
  //if(m_base->env->ExceptionOccurred()!=0)
  //  throw JavaLoader::JavaProcessException(m_base->env);
  //gamemapnativeobj=m_base->env->NewGlobalRef(gamemapnativeobj);
  //if(m_base->env->ExceptionOccurred()!=0)
  //  throw JavaLoader::JavaProcessException(m_base->env);
  jobject gamemapnativeobj=m_gm->GetJavaMap(p);

  m_gm->AddCPPEvents(this,appletobj);
  m_unitnativemanager->AddClientGameEvents(this,appletobj);
  m_unitnativemanager->AddCIQEvents(cev,appletobj);

  jclass commanderiqeventsnativeclass=m_base->environment->LoadClass("GZone.CommanderIQNative$CommanderIQEventsNative");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jmethodID commanderiqeventsnativeinit=m_base->env->GetMethodID(commanderiqeventsnativeclass,"<init>","()V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jobject commanderiqeventsnativeobj=m_base->env->NewObject(commanderiqeventsnativeclass,commanderiqeventsnativeinit);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  commanderiqeventsnativeobj=m_base->env->NewGlobalRef(commanderiqeventsnativeobj);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  m_base->env->DeleteLocalRef(commanderiqeventsnativeclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);

  jmethodID commanderiqFormCreate=m_base->env->GetMethodID(commanderiqclass,"FormCreate","(Ljava/awt/Component;LGZone/GameMap;LGZone/CommanderIQ$CommanderIQEvents;)V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  m_base->env->CallVoidMethod(appletobj,commanderiqFormCreate,embeddedwinobj,gamemapnativeobj,commanderiqeventsnativeobj);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);

  return m_wnd;
}

HWND JavaCommanderIQ::FormGet()
{
  return m_wnd;
}

const char* JavaCommanderIQ::FormGetName()
{
  return m_formname.c_str();
}

bool JavaCommanderIQ::UnitAdd(CUnit* u)
{
  jobject unitobj=m_unitnativemanager->GetJavaUnit(u);
  jmethodID commanderiqUnitAdd=m_base->env->GetMethodID(commanderiqclass,"UnitAdd","(Lclient/Unit;)Z");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jboolean b=m_base->env->CallBooleanMethod(appletobj,commanderiqUnitAdd,unitobj);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  return b==JNI_TRUE;
}

bool JavaCommanderIQ::UnitRemove(CUnit* u)
{
  jobject unitobj=m_unitnativemanager->GetJavaUnit(u);
  jmethodID commanderiqUnitRemove=m_base->env->GetMethodID(commanderiqclass,"UnitRemove","(Lclient/Unit;)Z");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jboolean b=m_base->env->CallBooleanMethod(appletobj,commanderiqUnitRemove,unitobj);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  return b==JNI_TRUE;
}

int JavaCommanderIQ::UnitGetCount()
{
  jmethodID commanderiqUnitCount=m_base->env->GetMethodID(commanderiqclass,"UnitGetCount","()I");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jint count=m_base->env->CallIntMethod(appletobj,commanderiqUnitCount);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  return (int)count;
}

CUnit* JavaCommanderIQ::UnitGet(int index)
{
  jmethodID commanderiqUnitGet=m_base->env->GetMethodID(commanderiqclass,"UnitGet","(I)Lclient/Unit;");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jobject unitobj=m_base->env->CallObjectMethod(appletobj,commanderiqUnitGet,(jint)index);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  return m_unitnativemanager->GetCPPUnit(unitobj);
}

std::string JavaCommanderIQ::Save()
{
  jmethodID commanderiqSave=m_base->env->GetMethodID(commanderiqclass,"Save","()Ljava/lang/String;");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jstring unitobj=(jstring)m_base->env->CallObjectMethod(appletobj,commanderiqSave);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jboolean unitcopy=JNI_FALSE;
  _bstr_t bs((const wchar_t*)m_base->env->GetStringChars(unitobj,&unitcopy));
  return std::string((const char*)bs);
}

void JavaCommanderIQ::Load(const char*p)
{
  jmethodID commanderiqLoad=m_base->env->GetMethodID(commanderiqclass,"Load","(Ljava/lang/String;)V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jboolean unitcopy=JNI_FALSE;
  _bstr_t bs(p);
  jstring strobj=m_base->env->NewString((const jchar*)bs.GetBSTR(),bs.length());
  m_base->env->CallVoidMethod(appletobj,commanderiqLoad,strobj);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  m_base->env->DeleteLocalRef(strobj);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
}

// точку выделена
void JavaCommanderIQ::PointSelect(CGameMap::point*pp)
{
  jmethodID commanderiqPointSelect=m_base->env->GetMethodID(gamemapeventsclass,"PointSelect","(LGZone/GameMap$point;)V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jobject p=m_gm->GetPoint(pp);
  m_base->env->CallVoidMethod(appletobj,commanderiqPointSelect,p);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
};

// точку переместили
void JavaCommanderIQ::PointMoveAfter(CGameMap::point*pp)
{
  jmethodID commanderiqPointMoveAfter=m_base->env->GetMethodID(gamemapeventsclass,"PointMoveAfter","(LGZone/GameMap$point;)V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jobject p=m_gm->GetPoint(pp);
  m_base->env->CallVoidMethod(appletobj,commanderiqPointMoveAfter,p);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
};

// точку хотят переместить
bool JavaCommanderIQ::PointMoveBefore(CGameMap::point*pp)
{
  jmethodID commanderiqPointMoveBefore=m_base->env->GetMethodID(gamemapeventsclass,"PointMoveBefore","(LGZone/GameMap$point;)Z");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jobject p=m_gm->GetPoint(pp);
  jboolean z=m_base->env->CallBooleanMethod(appletobj,commanderiqPointMoveBefore,p);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  return z==JNI_TRUE;
}
// точка подсвечена
void JavaCommanderIQ::PointHiLight(CGameMap::point*pp)
{
  jmethodID commanderiqPointHiLight=m_base->env->GetMethodID(gamemapeventsclass,"PointHiLight","(LGZone/GameMap$point;)V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jobject p=m_gm->GetPoint(pp);
  m_base->env->CallVoidMethod(appletobj,commanderiqPointHiLight,p);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
};

void JavaCommanderIQ::SelectedItem(unsigned itemserial)
{
  jmethodID commanderiqSelectedItem=m_base->env->GetMethodID(clientgameeventsclass,"SelectedItem","(I)V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  m_base->env->CallVoidMethod(appletobj,commanderiqSelectedItem,(jint)itemserial);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
}
// игрок выберает клеточку на земле
void JavaCommanderIQ::SelectedGround(WorldCord w,const Client::ItemType &it)
{
  jmethodID commanderiqSelectedGround=m_base->env->GetMethodID(clientgameeventsclass,"SelectedGround","(Lclient/Types$WorldCord;Lclient/ItemType;)V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jobject itobj;
  itobj=m_unitnativemanager->GetItemType(it);
  m_base->env->CallVoidMethod(appletobj,commanderiqSelectedGround,m_unitnativemanager->GetWorldCord(w),itobj);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
}
// игрок выберает  путь персонажа
void JavaCommanderIQ::SelectedPath(WorldCord w)
{
  jmethodID commanderiqSelectedItem=m_base->env->GetMethodID(clientgameeventsclass,"SelectedPath","(Lclient/Types$WorldCord;)V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  m_base->env->CallVoidMethod(appletobj,commanderiqSelectedItem,m_unitnativemanager->GetWorldCord(w));
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
};
// выбор из окошка создания предмета
void JavaCommanderIQ::SelectedOption(const Client::ItemType &it)
{
  jmethodID commanderiqSelectedGround=m_base->env->GetMethodID(clientgameeventsclass,"SelectedOption","(Lclient/ItemType;)V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
  jobject itobj;
  itobj=m_unitnativemanager->GetItemType(it);
  m_base->env->CallVoidMethod(appletobj,commanderiqSelectedGround,itobj);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaLoader::JavaProcessException(m_base->env);
};
