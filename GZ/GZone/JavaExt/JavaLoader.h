#include "../commanderiq.h"

#include <jni.h>
#include <jni_md.h>
#include <jawt_md.h>
#include <jawt.h>

#include "JavaBase.h"
#include "JavaException.h"
#include "JarLoader.h"
#include "UnitNativeManager.h"
#include "GameMapManager.h"

#include <ErrorReport/errorreport.h>

class JavaLoader
{
  typedef jint (JNICALL *_JNI_CreateJavaVM)(JavaVM **pvm, void **penv, void *args);
  typedef jboolean (JNICALL *_JAWT_GetAWT)(JNIEnv* env, JAWT* awt);

  JavaBase m_base;
  UnitNativeManager m_unitnativemanager;
  GameMapManager m_gamemapmanager;
  JavaVM *vm;
  JAWT awt;
  HMODULE hmjvm;
  HMODULE hmjawt;
  HMODULE hmenv;

  JarLoader m_environment;

  std::string m_jvmdll,m_awtdll;

public:

  _JNI_CreateJavaVM __JNI_CreateJavaVM;
  _JAWT_GetAWT __JAWT_GetAWT;

  class JavaLoadException:public std::exception
  {
  public:
    const char*what()const
    {
      return "JavaLoadException";
    }
  };

  class JavaLoadExceptionNoLibFound:public std::exception
  {
  public:
    const char*what()const
    {
      return "No lib jvm.dll,jawt.dll found";
    }
  };

  class JavaProcessException:public JavaException
  {
  public:
    JavaProcessException(JNIEnv *env):JavaException(env)
    {
    }
    JavaProcessException()
    {
    }
    JavaProcessException(const char*s):JavaException(s)
    {
    }
  };

  JavaLoader(const char* envr);
  ~JavaLoader();
  CCommanderIQ* LoadJar(const char*j);
};