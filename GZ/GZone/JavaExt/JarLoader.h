#ifndef __JARLOADER_H__
#define __JARLOADER_H__

#include <jni.h>
#include <jni_md.h>
#include <jawt_md.h>
#include <jawt.h>

class JarLoader
{
  JNIEnv*env;
  jclass classloaderclass;
  jobject classloaderobj;

public:
  ~JarLoader();
  void Create(JNIEnv*env, const char* jarfile,JarLoader* parent=0);
  jclass LoadClass(const char*p);
  jobject NewInstance(jclass );
};

#endif
