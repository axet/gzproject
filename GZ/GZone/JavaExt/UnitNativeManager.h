#ifndef __UNITNATIVEMANAGER_H__
#define __UNITNATIVEMANAGER_H__

#include "JavaBase.h"

#include <map>

#include "../../client/unit.h"
#include "../../client/itemtype.h"
#include "../commanderiq.h"

class UnitNativeManager
{
  JavaBase * m_base;

  jclass unitnativeclass;
  jmethodID unitnativeinit;

  typedef std::map< CUnit *,jobject > map;
  map m_map;

  typedef std::map< CClientGameEvents *,jobject > mapev;
  mapev m_mapev;

  typedef std::map< CCommanderIQEvents *,jobject > mapcev;
  mapcev m_mapcev;

public:

  void Create(JavaBase * b);
  ~UnitNativeManager();
  jobject GetJavaUnit(CUnit *);
  CUnit* GetCPPUnit(jobject);
  Client::ItemType GetItemType(jobject j);
  jobject GetItemType(const Client::ItemType& it);
  WorldCord GetWorldCord(jobject j);
  jobject GetWorldCord(WorldCord w);
  UnsignedList GetUnsignedList(jobject j);
  ShortList GetShortList(jobject j);
  void AddClientGameEvents(CClientGameEvents *,jobject o);
  void AddCIQEvents(CCommanderIQEvents *,jobject o);
  CClientGameEvents *GetClientGameEvents(jobject o);
  CCommanderIQEvents *GetCIQEvents(jobject o);
};

#endif
