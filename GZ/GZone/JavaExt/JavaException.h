#ifndef __JAVAEXCEPTION_H__
#define __JAVAEXCEPTION_H__

#include <jni.h>
#include <jni_md.h>
#include <jawt_md.h>
#include <jawt.h>

#include <exception>
#include <string>

#include <ErrorReport/errorreport.h>

class JavaException:public std::exception
{
  std::string m_str;

public:
  JavaException(JNIEnv *env)
  {
    jobject throwableobj=env->ExceptionOccurred();
    env->ExceptionClear();
    jclass throwableclass=env->FindClass("java/lang/Throwable");

    //ByteArrayOutputStream out=new ByteArrayOutputStream();
    jclass outclass=env->FindClass("java/io/ByteArrayOutputStream");
    jmethodID outinit=env->GetMethodID(outclass,"<init>","()V");
    jobject outobj=env->NewObject(outclass,outinit);
    //PrintStream ps=new PrintStream(out);
    jclass psclass=env->FindClass("java/io/PrintStream");
    jmethodID psinit=env->GetMethodID(psclass,"<init>","(Ljava/io/OutputStream;)V");
    jobject psobj=env->NewObject(psclass,psinit,outobj);
    //e.printStackTrace(ps);
    jmethodID throwableprintStackTrace=env->GetMethodID(throwableclass,"printStackTrace","(Ljava/io/PrintStream;)V");
    env->CallVoidMethod(throwableobj,throwableprintStackTrace,psobj);
    //String sm=out.toString();

    jmethodID throwabletoString=env->GetMethodID(throwableclass,"toString","()Ljava/lang/String;");
    jstring str=(jstring)env->CallObjectMethod(throwableobj,throwabletoString);
    jboolean strcopy=JNI_FALSE;
    const jchar* s=env->GetStringChars(str,&strcopy);
    jsize len=env->GetStringLength(str);
    m_str=ErrorReport("JavaException",_bstr_t((const wchar_t*)s));
  }
  JavaException()
  {
    m_str="JavaException";
  }

  static void TrhowJavaException(JNIEnv* env,const char*w)
  {
    jclass exclass=env->FindClass("java/lang/Exception");
    jmethodID exinit=env->GetMethodID(exclass,"<init>","(Ljava/lang/String;)V");
    _bstr_t ww(w);
    jstring s=env->NewString((const jchar*)ww.GetBSTR(),ww.length());
    jthrowable exobj=(jthrowable)env->NewObject(exclass,exinit,s);
    env->Throw(exobj);
  };
  JavaException(const char*s)
  {
    m_str=s;
  }
  const char*what()const
  {
    return m_str.c_str();
  }
};

#endif
