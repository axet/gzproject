#include "UnitNativeManager.h"

void UnitNativeManager::Create(JavaBase * b)
{
  m_base=b;
  unitnativeclass=m_base->environment->LoadClass("client.UnitNative");
  unitnativeinit=m_base->env->GetMethodID(unitnativeclass,"<init>","()V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
}

UnitNativeManager::~UnitNativeManager()
{
}

jobject UnitNativeManager::GetJavaUnit(CUnit *u)
{
  map::iterator i=m_map.find(u);
  if(i==m_map.end())
  {
    jobject unitobj=m_base->env->NewObject(unitnativeclass,unitnativeinit);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    //unitobj=m_base->env->NewGlobalRef(unitobj);
    //if(m_base->env->ExceptionOccurred()!=0)
    //  throw JavaException(m_base->env);

    m_map.insert(map::value_type(u,unitobj));
    return unitobj;
  }else
  {
    return i->second;
  }
}

CUnit* UnitNativeManager::GetCPPUnit(jobject o)
{
  for(map::iterator i=m_map.begin();i!=m_map.end();i++)
  {
    if(m_base->env->IsSameObject(i->second,o)==JNI_TRUE)
    {
      return i->first;
    }
  }
  throw std::exception("no unit found");
}

void UnitNativeManager::AddClientGameEvents(CClientGameEvents *p,jobject o)
{
  m_mapev.insert(mapev::value_type(p,o));
}

void UnitNativeManager::AddCIQEvents(CCommanderIQEvents *p,jobject o)
{
  m_mapcev.insert(mapcev::value_type(p,o));
}

CClientGameEvents *UnitNativeManager::GetClientGameEvents(jobject o)
{
  for(mapev::iterator i=m_mapev.begin();i!=m_mapev.end();i++)
  {
    if(m_base->env->IsSameObject(i->second,o)==JNI_TRUE)
    {
      return i->first;
    }
  }
  throw std::exception("no CClientGameEvents found");
}

CCommanderIQEvents *UnitNativeManager::GetCIQEvents(jobject o)
{
  for(mapcev::iterator i=m_mapcev.begin();i!=m_mapcev.end();i++)
  {
    if(m_base->env->IsSameObject(i->second,o)==JNI_TRUE)
    {
      return i->first;
    }
  }
  throw std::exception("no GetCIQEvents found");
}

Client::ItemType UnitNativeManager::GetItemType(jobject j)
{
  jclass alclass=m_base->env->FindClass("java/util/ArrayList");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID alget=m_base->env->GetMethodID(alclass,"get","(I)Ljava/lang/Object;");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID alsize=m_base->env->GetMethodID(alclass,"size","()I");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jclass intclass=m_base->env->FindClass("java/lang/Integer");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID intintvalue=m_base->env->GetMethodID(intclass,"intValue","()I");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  int size=m_base->env->CallIntMethod(j,alsize);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  Client::ItemType it;

  for(int i=0;i<size;i++)
  {
    jobject intobj=m_base->env->CallObjectMethod(j,alget,i);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    int ii=m_base->env->CallIntMethod(intobj,intintvalue);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    m_base->env->DeleteLocalRef(intobj);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    it.push_back(ii);
  }

  m_base->env->DeleteLocalRef(alclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  m_base->env->DeleteLocalRef(intclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  return it;
}

jobject UnitNativeManager::GetItemType(const Client::ItemType& it)
{
  jclass itclass=m_base->environment->LoadClass("client.ItemType");
  jmethodID itinit=m_base->env->GetMethodID(itclass,"<init>","()V");

  jclass alclass=m_base->env->FindClass("java/util/ArrayList");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID aladd=m_base->env->GetMethodID(alclass,"add","(Ljava/lang/Object;)Z");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID alinit=m_base->env->GetMethodID(alclass,"<init>","()V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jclass intclass=m_base->env->FindClass("java/lang/Integer");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID intinit=m_base->env->GetMethodID(intclass,"<init>","(I)V");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  jobject itobj=m_base->env->NewObject(itclass,itinit);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  for(int i=0;i<it.size();i++)
  {
    jobject intobj=m_base->env->NewObject(intclass,intinit,it[i]);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    jboolean z=m_base->env->CallBooleanMethod(itobj,aladd,intobj);
  }

  m_base->env->DeleteLocalRef(alclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  m_base->env->DeleteLocalRef(intclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  return itobj;
}

WorldCord UnitNativeManager::GetWorldCord(jobject j)
{
  jclass alclass=m_base->environment->LoadClass("client.Types$WorldCord");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jfieldID fx=m_base->env->GetFieldID(alclass,"x","I");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jfieldID fy=m_base->env->GetFieldID(alclass,"y","I");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jfieldID fz=m_base->env->GetFieldID(alclass,"z","I");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  int x=m_base->env->GetIntField(j,fx);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  int y=m_base->env->GetIntField(j,fy);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  int z=m_base->env->GetIntField(j,fz);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  m_base->env->DeleteLocalRef(alclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  return WorldCord(x,y,z);
}

jobject UnitNativeManager::GetWorldCord(WorldCord w)
{
  jclass pointclass=m_base->environment->LoadClass("client.Types$WorldCord");

  if(m_base->env->ExceptionOccurred()!=0)
    return 0;
  jmethodID pointinit=m_base->env->GetMethodID(pointclass,"<init>","(III)V");
  if(m_base->env->ExceptionOccurred()!=0)
    return 0;
  jobject pointobj=m_base->env->NewObject(pointclass,pointinit,(jint)w.x,(jint)w.y,(jint)w.z);
  if(m_base->env->ExceptionOccurred()!=0)
    return 0;

  m_base->env->DeleteLocalRef(pointclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  return pointobj;
}


UnsignedList UnitNativeManager::GetUnsignedList(jobject j)
{
  jclass alclass=m_base->env->FindClass("java/util/ArrayList");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID alget=m_base->env->GetMethodID(alclass,"get","(I)Ljava/lang/Object;");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID alsize=m_base->env->GetMethodID(alclass,"size","()I");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jclass intclass=m_base->env->FindClass("java/lang/Integer");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID intintvalue=m_base->env->GetMethodID(intclass,"intValue","()I");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  int size=m_base->env->CallIntMethod(j,alsize);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  UnsignedList it;

  for(int i=0;i<size;i++)
  {
    jobject intobj=m_base->env->CallObjectMethod(j,alget,i);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    int ii=m_base->env->CallIntMethod(intobj,intintvalue);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    m_base->env->DeleteLocalRef(intobj);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    it.push_back(ii);
  }

  m_base->env->DeleteLocalRef(alclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  m_base->env->DeleteLocalRef(intclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  return it;
}


ShortList UnitNativeManager::GetShortList(jobject j)
{
  jclass alclass=m_base->env->FindClass("java/util/ArrayList");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID alget=m_base->env->GetMethodID(alclass,"get","(I)Ljava/lang/Object;");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID alsize=m_base->env->GetMethodID(alclass,"size","()I");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jclass intclass=m_base->env->FindClass("java/lang/Integer");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  jmethodID intintvalue=m_base->env->GetMethodID(intclass,"intValue","()I");
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  int size=m_base->env->CallIntMethod(j,alsize);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  ShortList it;

  for(int i=0;i<size;i++)
  {
    jobject intobj=m_base->env->CallObjectMethod(j,alget,i);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    int ii=m_base->env->CallIntMethod(intobj,intintvalue);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    m_base->env->DeleteLocalRef(intobj);
    if(m_base->env->ExceptionOccurred()!=0)
      throw JavaException(m_base->env);
    it.push_back(ii);
  }

  m_base->env->DeleteLocalRef(alclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);
  m_base->env->DeleteLocalRef(intclass);
  if(m_base->env->ExceptionOccurred()!=0)
    throw JavaException(m_base->env);

  return it;
}
