#include "JarLoader.h"
#include "JavaException.h"

#include <comdef.h>

void JarLoader::Create(JNIEnv*e,const char*envr,JarLoader* parent)
{
  env=e;

  _bstr_t environmentfilebstr="file:/"+_bstr_t(envr);
  jstring environmentfilestr=env->NewString((jchar*)environmentfilebstr.GetBSTR(),environmentfilebstr.length());
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);

  jclass urlclass=env->FindClass("java/net/URL");
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);
  jmethodID urlinit=env->GetMethodID(urlclass,"<init>","(Ljava/lang/String;)V");
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);

  jobject envobj=env->NewObject(urlclass,urlinit,environmentfilestr);
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);

  //URL urls[]=new URL[]{url}
  jobjectArray urlsobjar=env->NewObjectArray(1,urlclass,0);
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);
  env->SetObjectArrayElement(urlsobjar,0,envobj);
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);

  if(parent==0)
  {
    //URLClassLoader uc=new URLClassLoader(urls);
    classloaderclass=env->FindClass("java/net/URLClassLoader");
    if(env->ExceptionOccurred()!=0)
      throw JavaException(env);
    jmethodID ucinit=env->GetMethodID(classloaderclass,"<init>","([Ljava/net/URL;)V");
    if(env->ExceptionOccurred()!=0)
      throw JavaException(env);
    classloaderobj=env->NewObject(classloaderclass,ucinit,urlsobjar);
    if(env->ExceptionOccurred()!=0)
      throw JavaException(env);
  }else
  {
    //URLClassLoader uc=new URLClassLoader(urls,parent);
    classloaderclass=env->FindClass("java/net/URLClassLoader");
    if(env->ExceptionOccurred()!=0)
      throw JavaException(env);
    jmethodID ucinit=env->GetMethodID(classloaderclass,"<init>","([Ljava/net/URL;Ljava/lang/ClassLoader;)V");
    if(env->ExceptionOccurred()!=0)
      throw JavaException(env);
    classloaderobj=env->NewObject(classloaderclass,ucinit,urlsobjar,parent->classloaderobj);
    if(env->ExceptionOccurred()!=0)
      throw JavaException(env);
  }

  env->DeleteLocalRef(environmentfilestr);
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);

  env->DeleteLocalRef(urlclass);
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);

  env->DeleteLocalRef(envobj);
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);

  env->DeleteLocalRef(urlsobjar);
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);
}

JarLoader::~JarLoader()
{
  if(classloaderobj!=0)
    env->DeleteLocalRef(classloaderobj);
  if(env!=0)
  {
    if(env->ExceptionOccurred()!=0)
      throw JavaException(env);
    env->DeleteLocalRef(classloaderclass);
    if(env->ExceptionOccurred()!=0)
      throw JavaException(env);
  }
}

jclass JarLoader::LoadClass(const char*p)
{
  _bstr_t mmbstr=_bstr_t(p);
  jstring mmstr=env->NewString((jchar*)mmbstr.GetBSTR(),mmbstr.length());
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);

  //Class c=uc.loadClass(mm);
  jmethodID ucloadClass=env->GetMethodID(classloaderclass,"loadClass","(Ljava/lang/String;)Ljava/lang/Class;");
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);
  jclass cclass=(jclass)env->CallObjectMethod(classloaderobj,ucloadClass,mmstr);
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);
  env->DeleteLocalRef(mmstr);
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);
  return cclass;
}

jobject JarLoader::NewInstance(jclass cls)
{
  //Object o=c.newInstance();
  jclass cclass=env->FindClass("java/lang/Class");
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);
  jmethodID cnewInstance=env->GetMethodID(cclass,"newInstance","()Ljava/lang/Object;");
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);
  jobject oobj=env->CallObjectMethod(cls,cnewInstance);
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);
  env->DeleteLocalRef(cclass);
  if(env->ExceptionOccurred()!=0)
    throw JavaException(env);
  return oobj;
};