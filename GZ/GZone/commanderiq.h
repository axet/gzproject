// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef COMMANDERIQ_H
#define COMMANDERIQ_H

#include "gamemap.h"

#include <wtypes.h>
#include <string>

class CUnit;


class CCommanderIQEvents;

/// командир класс получает команды от основного приложения

///   такие как список пользователей, указатели на другие интерфейсы
///   работа с окнами.
///   допольнительно класс командира получает события от карты.
///   такие как перемещение на ней объектов, точек, возможно создание
///   таких объектов, выбор пунктов меню.. это все события от карты.

class CCommanderIQ: public CGameMapEvents
{
public:
  virtual ~CCommanderIQ() {};

  // работа с интерфейсом пользователя (человека)

  /// требует от командира заголовка окна.
  
  /// это окно являеться
  /// окном управления. через него пользователь будет управлять
  /// программой
  virtual HWND FormCreate(HWND parent,CGameMap*,CCommanderIQEvents*) {return 0;}
  /// повторный запрос
  virtual HWND FormGet() {return 0;}
  /// возвращает имя этого модуля, то как оно будет отображаться в далоге выбора командира
  virtual const char* FormGetName() {return 0;}

  // работа с персонажами

  /// добавляет пользователя в подченение командиру
  virtual bool UnitAdd(CUnit* ) {return false;}
  /// удаляет пользователя
  virtual bool UnitRemove(CUnit* ) {return false;}
  /// возвращает поличество пользователей на контроле данного командира
  virtual int UnitGetCount() {return 0;}
  /// возвращает пользователя по номеру
  virtual CUnit* UnitGet(int index) {return 0;}
  /// возвращает игровой аккаунт который должен быть подгружен в командира
  virtual const char* UnitMissing() {return 0;};

  // обмен данными

  /// сохранение/чтение настроек (когда формируеться сейф файл)
  virtual std::string Save() {return std::string();}
  /// сохранение/чтение настроек (когда формируеться сейф файл)
  virtual void Load(const char*) {}
};

/// события от этого командира, которые он хочет передать основному приложению

class CCommanderIQEvents
{
public:
  ///установить состояния изменение, пользователю будет предложено
  /// сохранить изменения, вызоветься CCommanderIQ::Serialize("..",true);
  virtual void SetModified(bool b = true) = 0;
};

#endif
