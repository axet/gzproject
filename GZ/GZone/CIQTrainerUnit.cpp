// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// CIQTrainerUnit.cpp : implementation file
//

#include "stdafx.h"
#include "gzone.h"
#include "CIQTrainerUnit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCIQTrainerUnit

IMPLEMENT_DYNCREATE(CCIQTrainerUnit, CWinThread)

CCIQTrainerUnit::CCIQTrainerUnit()
{
}

CCIQTrainerUnit::~CCIQTrainerUnit()
{
}

BOOL CCIQTrainerUnit::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	return TRUE;
}

int CCIQTrainerUnit::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CCIQTrainerUnit, CWinThread)
  ON_THREAD_MESSAGE(msgStart,OnStart)
	//{{AFX_MSG_MAP(CCIQTrainerUnit)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIQTrainerUnit message handlers

void CCIQTrainerUnit::OnStart(WPARAM u,LPARAM)
{
  try
  {
    unsigned newserial;
    int count=0;

    m_client->MoveItemsG2G(m_backpackser,Client::ItemType::itLog_s,m_goodsser);

    while(m_gumps[count])
      count++;

    ShortList items;
    items.assign(&m_gumps[0],&m_gumps[count]);
    if((newserial=m_client->UseItemGump(m_useitemser,UnsignedList(),items))==0)
    {
      Sleep(7000);
      goto getnexttarget;
    }
    m_client->MoveItem(newserial,m_dropcontainerser);
    m_client->MoveItemsG2G(m_dropcontainerser,Client::ItemType::itLog_s,m_goodsser);

    goto getnexttarget;
  }catch(ClientGameCommands::LocalOperationTimeout &)
  {
    goto getnexttarget;
  }catch(std::exception &e)
  {
    AfxMessageBox(e.what());
    goto exit;
  }

  AfxMessageBox("trainer terminated");

exit:
  return;
getnexttarget:
  ::PostMessage((HWND)u,WM_USER+2,0,0);
  return;
}
