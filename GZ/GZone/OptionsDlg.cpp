// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// OptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "gzone.h"
#include "OptionsDlg.h"
#include ".\optionsdlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptionsDlg dialog


COptionsDlg::COptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COptionsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COptionsDlg)
	//}}AFX_DATA_INIT
}


void COptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionsDlg)
	DDX_Text(pDX, IDC_EDIT1, pathbgimage);
  DDX_Text(pDX, IDC_EDIT7, clientpath);
  DDX_Text(pDX, IDC_COMBO1, clientsecure);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptionsDlg, CDialog)
	//{{AFX_MSG_MAP(COptionsDlg)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE, OnButtonBrowse)
	//}}AFX_MSG_MAP
  ON_BN_CLICKED(IDC_BUTTON_BROWSE2, OnBnClickedButtonBrowse2)
  ON_CBN_EDITCHANGE(IDC_COMBO1, OnCbnEditchangeCombo1)
  ON_CBN_SELENDOK(IDC_COMBO1, OnCbnSelendokCombo1)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionsDlg message handlers

void COptionsDlg::OnButtonBrowse() 
{
  CFileDialog dlg(TRUE,0,0,0,"Bitmap *.bmp|*.bmp||");
  if(dlg.DoModal()!=IDOK)
    return;
  pathbgimage=dlg.GetPathName();
  UpdateData(FALSE);
}

BOOL COptionsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
  UpdateData(FALSE);
  
  ((CComboBox*)GetDlgItem(IDC_COMBO1))->SelectString(0,clientsecure);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void COptionsDlg::OnBnClickedButtonBrowse2()
{
  CFileDialog dlg(TRUE,0,0,0,"Exe *.exe|*.exe||");
  if(dlg.DoModal()!=IDOK)
    return;
  clientpath=dlg.GetPathName();
  UpdateData(FALSE);
}

void COptionsDlg::OnCbnEditchangeCombo1()
{
  UpdateData(TRUE);
}

void COptionsDlg::OnCbnSelendokCombo1()
{
  UpdateData(TRUE);
}
