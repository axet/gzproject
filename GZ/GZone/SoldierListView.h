// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if !defined(AFX_SOLDIERLISTVIEW_H__E8729FEA_B460_4D22_834C_CE77F3702F7B__INCLUDED_)
#define AFX_SOLDIERLISTVIEW_H__E8729FEA_B460_4D22_834C_CE77F3702F7B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SoldierListView.h : header file
//

#include "likemsdev/likemsdev.h"
#include "../interproc_clientaddin/outofprocmasterevents.h"
#include "../interproc_clientaddin/outofprocmasterpost.h"

class CGZoneApp;
class CGZUnit;

/////////////////////////////////////////////////////////////////////////////
// CSoldierListView view

class CSoldierListView : public LikeMsdev::CSizingControlBarG,public CClientEvents
{
  CMenu m_menu;
  CBitmap m_bm;
  CImageList m_il;
  CListCtrl m_list;
  bool m_updatelist;

  inline CGZoneApp& App() {return *(CGZoneApp*)AfxGetApp(); }

  bool IsUserInUse(CGZUnit*p);
  void ReloadList();
  void AddItem(CGZUnit*);
  void UpdateItem(int item,CGZUnit*);
  int GetImage(CGZUnit*);
  virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam );
  
  virtual void Connected();
  virtual void LocalConnectionLost();
  virtual void ConnectionEstablished(const char* name,const char* pass, const char* charname);

public:
  CSoldierListView();
  ~CSoldierListView();
  void Create(CWnd* p,int i);
  void UpdateList();

protected:
  afx_msg void OnItemChanging(NMHDR*, LRESULT*);
  afx_msg long OnUpdateUnitList(unsigned,long);
  afx_msg void OnUnitNew();
  afx_msg void OnUnitRunclient();
  afx_msg void OnUnitDelete();
  afx_msg void OnUnitCloseclient();
  afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
  afx_msg void OnUpdateUnitDelete(CCmdUI* pCmdUI);
  afx_msg void OnUpdateUnitCloseclient(CCmdUI* pCmdUI);
  afx_msg void OnUpdateUnitRunclient(CCmdUI* pCmdUI);
  afx_msg void OnUpdateUnitNew(CCmdUI* pCmdUI);
  afx_msg void OnUpdateShowClient(CCmdUI* pCmdUI);
  afx_msg void OnUpdateHideClient(CCmdUI* pCmdUI);
  afx_msg void OnUnitProperties();
  afx_msg void OnUpdateUnitProperties(CCmdUI* pCmdUI);
  afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnUnitHideclient();
  afx_msg void OnUnitShowclient();
  DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOLDIERLISTVIEW_H__E8729FEA_B460_4D22_834C_CE77F3702F7B__INCLUDED_)
