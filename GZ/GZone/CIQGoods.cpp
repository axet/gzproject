// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



#include "stdafx.h"
#include "gzone.h"
#include "CIQGoods.h"

#include <afxpriv.h>
#include <iniext/iniext.h>

#include "../client/clientcommands.h"
#include "xsd/Schema/Schema.h"
#include ".\ciqGoods.h"
#include "CIQGoodsUnit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CDocument *g_DocumentIQ;

using namespace goodsconf;

/////////////////////////////////////////////////////////////////////////////
// CCIQGoods

// CCIQGoods {

//IMPLEMENT_DYNCREATE(CCIQGoods, CFormView)

CCIQGoods::CCIQGoods()
	: CFormView(CCIQGoods::IDD),m_hunit(0),m_unit(0),m_gamemap(0)
    , m_vendorname(_T(""))
    , m_itemcount(0)
    , m_charname(_T(""))
{
  m_pointtarget=m_pointlist.end();
  m_backpackser=0;

	//{{AFX_DATA_INIT(CCIQGoods)
	m_catchmapoint = FALSE;
	//}}AFX_DATA_INIT
}

CCIQGoods::~CCIQGoods()
{
  m_pDocument=0;
}

void CCIQGoods::DoDataExchange(CDataExchange* pDX)
{
  CFormView::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_CHECK_CATCHBACKPAK, m_catchbackpack);
  DDX_Check(pDX, IDC_CHECK_CATCHMAPPOINT, m_catchmapoint);
  DDX_Check(pDX, IDC_CHECKPLAYERCORDS, m_catchplayercoords);
  DDX_Text(pDX, IDC_EDIT_VENDORNAME, m_vendorname);
  DDX_Control(pDX, IDC_CHECK_CATCHMAKEITEMTYPE, m_catchmakeitemtype);
  DDX_Text(pDX, IDC_EDIT_MAKEITEMCOUNT, m_itemcount);
  DDX_Text(pDX, IDC_EDIT_CHARNAME, m_charname);
  DDX_Control(pDX, IDC_COMBO_MAKEUSEITEM, m_control_makeuseitem);
  DDX_Control(pDX, IDC_COMBO_HARVESTUSEITEM, m_control_harvestuseitem);
  DDX_Control(pDX, IDC_COMBO_MAKEITEMTYPE, m_control_goodsitemtype);
}


BEGIN_MESSAGE_MAP(CCIQGoods, CFormView)
    ON_MESSAGE(WM_USER+1,OnUnitFinish) // finish target
    ON_MESSAGE(WM_USER+5,OnUnitStop) // set player coords
	ON_BN_CLICKED(IDC_BUTTON_START, OnButtonStart)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BUTTON_SHOWMAP, OnButtonShowmap)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
	ON_BN_CLICKED(IDC_CHECK_CATCHMAPPOINT, OnCheckCatchmappoint)
	ON_BN_CLICKED(IDC_BUTTON_DELETEALL, OnButtonDeleteall)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK1, OnCheckWokItem)
	ON_BN_CLICKED(IDC_CHECK2, OnCheckBackPack)
    ON_BN_CLICKED(IDC_CHECKPLAYERCORDS, OnBnClickedCheckPlayerCords)
    ON_WM_ACTIVATE()
    ON_WM_MOUSEACTIVATE()
    ON_CBN_SELENDOK(IDC_COMBO_HARVESTUSEITEM, &CCIQGoods::OnCbnSelendokComboHarvestuseitem)
    ON_CBN_SELENDOK(IDC_COMBO_MAKEUSEITEM, &CCIQGoods::OnCbnSelendokComboMakeuseitem)
    ON_CBN_SELENDOK(IDC_COMBO_GOODSITEMTYPE, &CCIQGoods::OnCbnSelendokComboGoodsitemtype)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIQGoods diagnostics

#ifdef _DEBUG
void CCIQGoods::AssertValid() const
{
	CFormView::AssertValid();
}

void CCIQGoods::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCIQGoods message handlers

HWND CCIQGoods::FormCreate(HWND parent,CGameMap*p,CCommanderIQEvents*pp)
{
  m_events=pp;
  m_gamemap=p;
  m_gamemap->AddAdvise(this);
  CFormView::Create(0,0,WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),FromHandle(parent),0,0);
  //SendMessage(WM_INITIALUPDATE);
  //m_pDocument=g_DocumentIQ;
  return m_hWnd;
}

HWND CCIQGoods::FormGet()
{
  return m_hWnd;
}

const char* CCIQGoods::FormGetName()
{
  static CString Goods;
  Goods="Goods";
  return Goods;
}

CCIQGoods* CCIQGoods::Create()
{
  //return (CCIQGoods*)RUNTIME_CLASS(CCIQGoods)->CreateObject();
  return new(CCIQGoods);
}

void CCIQGoods::OnInitialUpdate() 
{
  CFormView::OnInitialUpdate();
	

  static Client::ItemType logs;
  logs.clear();
  logs=Client::ItemType(Client::ItemType::itLog_s);

  static Client::ItemType dagger;
  dagger.clear();
  dagger.push_back(0x0f51);
  dagger.push_back(0x0f52);

  static Client::ItemType pickaxe;
  pickaxe.clear();
  pickaxe.push_back(0x0e85);
  pickaxe.push_back(0x0e86);

  static Client::ItemType hatchet;
  hatchet.clear();
  hatchet.push_back(0x0f43);
  hatchet.push_back(0x0f44);

  int i;

  i=m_control_makeuseitem.AddString("Dagger");
  m_control_makeuseitem.SetItemData(i,(DWORD_PTR)&dagger);

  i=m_control_harvestuseitem.AddString("Pickaxe");
  m_control_harvestuseitem.SetItemData(i,(DWORD_PTR)&pickaxe);
  i=m_control_harvestuseitem.AddString("Hatchet");
  m_control_harvestuseitem.SetItemData(i,(DWORD_PTR)&hatchet);

  i=m_control_goodsitemtype.AddString("Logs");
  m_control_goodsitemtype.SetItemData(i,(DWORD_PTR)&logs);

  ReloadLinePoints();

  UpdateData(FALSE);

  UpdateUnit();

  if(m_pointlist.size()>0)
    m_gamemap->SetViewPos(m_pointlist.front().x,m_pointlist.front().y);
  else
  {
    if(m_unit!=0)
    {
      WorldCord w=m_unit->GetUnitPos();
      m_gamemap->SetCursor(w.x,w.y);
    }
  }

  ResizeParentToFit();
  GetParentFrame()->RecalcLayout();
}

std::string CCIQGoods::Save()
{
  UpdateData();

  CDocAddin<CSchemaDoc> doc;

  CGoodsConfigType goodscofnig;
  if(m_unit!=0)
  {
    CGoodsUnitType unit;
    unit.AddBackpack(m_backpackser);
    for(Client::ItemType::iterator i=m_harvestuseitemtype.begin();i!=m_harvestuseitemtype.end();i++)
    {
      unit.AddHarvestItem(*i);
    }
    for(Client::ItemType::iterator i=m_makeitemtype.begin();i!=m_makeitemtype.end();i++)
    {
      unit.AddMakeItemType(*i);
    }
    for(Client::ItemType::iterator i=m_goodsitemtype.begin();i!=m_goodsitemtype.end();i++)
    {
      unit.AddGoodsItemType(*i);
    }
    for(Client::ItemType::iterator i=m_makeuseitemtype.begin();i!=m_makeuseitemtype.end();i++)
    {
      unit.AddMakeItem(*i);
    }
    unit.AddCount(m_itemcount);
    unit.AddVenderName((const char*)m_vendorname);
    unit.AddCharName((const char*)m_charname);
    unit.AddUnitId(m_unitid.c_str());
    goodscofnig.AddGoodsUnit(unit);
  }

  for(pointlist::iterator m=m_pointlist.begin();m!=m_pointlist.end();m++)
  {
    CGoodsGroundPointType gp;
    gp.AddItemType(m->itemid);
    /*Schema::*/CGoodsScalePoint pt;
    pt.AddX(m->x);
    pt.AddY(m->y);
    pt.AddZ(m->z);
    gp.AddUsePoint(pt);
    /*Schema::*/CGoodsScalePoint ppt;
    ppt.AddX(m->px);
    ppt.AddY(m->py);
    ppt.AddZ(m->pz);
    gp.AddPlayerPoint(ppt);
    goodscofnig.AddGoodsGroundPoint(gp);
  }

  CGoodsHarvesterType Goodstype;
  Goodstype.AddCatchPlayerPoint(m_catchplayercoords);

  goodscofnig.AddGoodsHarvester(Goodstype);

  doc.SetRootElementName("http://gzproject.sourceforge.net/GoodsConfig","GoodsConfig");
  return (tstring)doc.SaveXML(goodscofnig);
}

void CCIQGoods::Load(const char*p)
{
  CDocAddin<CSchemaDoc> doc;
  CGoodsConfigType goodscofnig=doc.LoadXML(p);

  if(goodscofnig.HasGoodsUnit())
  {
    CGoodsUnitType unit=goodscofnig.GetGoodsUnit();
    m_harvestuseitemtype.clear();
    for(int i=0;i<unit.GetHarvestItemCount();i++)
    {
      m_harvestuseitemtype.push_back(unit.GetHarvestItemAt(i));
    }
    m_makeuseitemtype.clear();
    for(int i=0;i<unit.GetMakeItemCount();i++)
    {
      m_makeuseitemtype.push_back(unit.GetMakeItemAt(i));
    }
    m_backpackser=unit.GetBackpack();
    m_itemcount=unit.GetCount();

    m_makeitemtype.clear();
    for(int i=0;i<unit.GetMakeItemTypeCount();i++)
    {
      m_makeitemtype.push_back(unit.GetMakeItemTypeAt(i));
    }

    m_goodsitemtype.clear();
    for(int i=0;i<unit.GetGoodsItemTypeCount();i++)
    {
      m_goodsitemtype.push_back(unit.GetGoodsItemTypeAt(i));
    }

    m_unitid=unit.GetUnitId();
    m_vendorname=((tstring)unit.GetVenderName()).c_str();
    m_charname=((tstring)unit.GetCharName()).c_str();
  }

  for(int i=0;i<goodscofnig.GetGoodsGroundPointCount();i++)
  {
    CGoodsGroundPointType gp=goodscofnig.GetGoodsGroundPointAt(i);
    /*Schema::*/CGoodsScalePoint pt=gp.GetUsePoint();
    /*Schema::*/CGoodsScalePoint ppt=gp.GetPlayerPoint();
    m_pointlist.push_back(point(pt.GetX(),pt.GetY(),pt.GetZ(),ppt.GetX(),ppt.GetY(),ppt.GetZ(),gp.GetItemType()));
  }

  CGoodsHarvesterType harvester=goodscofnig.GetGoodsHarvester();

  m_catchplayercoords=harvester.GetCatchPlayerPoint();
}

const char* CCIQGoods::UnitMissing()
{
  if(m_unit==0)
    return m_unitid.c_str();
  return 0;
}

bool CCIQGoods::UnitAdd(CUnit* p)
{
  if(m_unit!=0)
    return false;
//  if(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==1)
//    return false;

  m_unit=p;

  m_unit->AddAdvise(this);
  m_unitid=m_unit->GetUnitId();

  UpdateUnit();

  if(m_gamemap!=0)
  {
    WorldCord w=m_unit->GetUnitPos();
    if(w.x!=-1||w.y!=-1)
      m_gamemap->SetCursor(w.x,w.y);
  }

  return true;
}

void CCIQGoods::SetUnit()
{
  UpdateData();
  if(m_hunit!=0)
  {
    m_hunit->m_harvestuseitemtype=m_harvestuseitemtype;
    m_hunit->m_backpackser=m_backpackser;
    m_hunit->m_catchplayercords=m_catchplayercoords;
    m_hunit->m_makeitemtype=m_makeitemtype;
    m_hunit->m_makeuseitemtype=m_makeuseitemtype;
    m_hunit->m_vendorname=m_vendorname;
    m_hunit->m_charname=m_charname;
    m_hunit->m_count=m_itemcount;
    m_hunit->m_goodsitemtype=m_goodsitemtype;
  }
}

void CCIQGoods::Stop()
{
  if(m_hunit!=0)
  {
    m_hunit->PostThreadMessage(WM_QUIT,0,0);
    m_unit->Abort();
    WaitForSingleObject(m_hunit->m_hThread,-1);
    m_hunit=0;
  }
}

void CCIQGoods::StopGUI()
{
  ((CButton*)GetDlgItem(IDC_BUTTON_START))->SetCheck(0);

  CString start,stop;
  start.LoadString(IDS_STRING_START);
  stop.LoadString(IDS_STRING_STOP);
  GetDlgItem(IDC_BUTTON_START)->SetWindowText(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==0?start:stop);

  SetCheck(IDC_BUTTON_START);
}

bool CCIQGoods::UnitRemove(CUnit* p)
{
  if(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==1)
    return false;

  Stop();

  if(m_unit!=0)
  {
    m_unit->RemoveAdvise(this);
    m_unit=0;
  }

  m_events->SetModified();
  UpdateUnit();

  return true;
}

void CCIQGoods::UpdateUnit()
{
  if(!IsWindow(*this))
    return;

  CString str;
  str.LoadString(IDS_STRING_CATCH);
  SetDlgItemText(IDC_EDIT_BACKPACK,m_backpackser?str:"");
  SetDlgItemText(IDC_EDIT_MAKEITEMTYPE,m_makeitemtype.size()>0?str:"");

  for(int i=0;i<m_control_makeuseitem.GetCount();i++)
  {
    Client::ItemType *it=(Client::ItemType *)m_control_makeuseitem.GetItemData(i);
    if(m_makeuseitemtype==*it)
    {
      m_control_makeuseitem.SetCurSel(i);
    }
  }
  for(int i=0;i<m_control_harvestuseitem.GetCount();i++)
  {
    Client::ItemType *it=(Client::ItemType *)m_control_harvestuseitem.GetItemData(i);
    if(m_harvestuseitemtype==*it)
    {
      m_control_harvestuseitem.SetCurSel(i);
    }
  }
  for(int i=0;i<m_control_goodsitemtype.GetCount();i++)
  {
    Client::ItemType *it=(Client::ItemType *)m_control_goodsitemtype.GetItemData(i);
    if(m_makeitemtype==*it)
    {
      m_control_goodsitemtype.SetCurSel(i);
    }
  }
  
  UINT items[]=
  {
    IDC_BUTTON_START,
      IDC_BUTTON_SHOWMAP,IDC_CHECK_CATCHMAPPOINT,IDC_BUTTON_DELETE,IDC_BUTTON_DELETEALL,IDC_CHECKPLAYERCORDS,
      IDC_EDIT_VENDORNAME
  };
  for(int i=0;i<sizeof(items)/sizeof(UINT);i++)
    GetDlgItem(items[i])->EnableWindow(m_unit!=0);
}

bool CCIQGoods::SetUnitItem(CCIQGoodsUnit* p,unsigned u)
{
  if(m_catchitemuse.GetCheck()!=1)
    return false;

  m_events->SetModified();
  m_catchitemuse.SetCheck(0);
  return true;
}

bool CCIQGoods::SetUnitBackpack(CCIQGoodsUnit* p,unsigned u)
{
  if(m_catchbackpack.GetCheck()!=1)
    return false;

  m_events->SetModified();
  m_catchbackpack.SetCheck(0);

  return true;
}

void CCIQGoods::PointSelect(CGameMap::point* p)
{
  CString str;
  {
    point* pp=reinterpret_cast<point*>(m_gamemap->GetPointData(p));
    if(pp!=0)
    {
      CString path;
      path.LoadString(IDS_STRING_PATH);
      str.Format("%s\xd\xa%d.%d",path,pp->x,pp->y);
      if(m_catchplayercoords)
      {
        CString ppath;
        ppath.LoadString(IDS_STRING_PATHPLAYER);
        CString s;
        s.Format("\xd\xa%s\xd\xa%d.%d",ppath,pp->px,pp->py);
        str+=s;
      }
    }
  }

  GetDlgItem(IDC_EDIT_MAPSTATUS)->SetWindowText(str);
}

void CCIQGoods::PointHiLight(CGameMap::point*)
{
  ;
}

void CCIQGoods::AddPoint(int x,int y,int z,int px,int py,int pz,short itemid)
{
  if(!m_catchmapoint)
    return;

  for(pointlist::iterator m=m_pointlist.begin();m!=m_pointlist.end();m++)
  {
    if(m->x==x&&m->y==y)
      return;
  }

  pointlist::iterator i=m_pointlist.insert(m_pointlist.end(),point(x,y,z,px,py,pz,itemid));
  if(m_catchplayercoords)
  {
    i->ppt=m_gamemap->AddPoint(px,py,m_playerlistline);
    m_gamemap->SetPointData(i->ppt,&*i);
  }
  i->pt=m_gamemap->AddPoint(x,y,m_pointlistline);
  m_gamemap->SetPointData(i->pt,&*i);
  m_pointtarget=i;
}

void CCIQGoods::OnButtonStart() 
{
  try
  {
    CheckClient();
  }catch(std::exception&)
  {
    ((CButton*)GetDlgItem(IDC_BUTTON_START))->SetCheck(0);
    throw;
  }

  if(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==1)
  {
    m_hunit=(CCIQGoodsUnit*)AfxBeginThread(RUNTIME_CLASS(CCIQGoodsUnit));
    SetUnit();
    m_hunit->Create(m_hWnd,m_unit);
    UpdateUnit();
    m_hunit->PostThreadMessage(CCIQGoodsUnit::msgStart,(unsigned)GetNextTarget(),0);

    CString start,stop;
    start.LoadString(IDS_STRING_START);
    stop.LoadString(IDS_STRING_STOP);
    GetDlgItem(IDC_BUTTON_START)->SetWindowText(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==0?start:stop);
    SetCheck(IDC_BUTTON_START);
  }else
  {
    BeginWaitCursor();

    Stop();
    StopGUI();
  }
}

int CCIQGoods::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

int CCIQGoods::UnitGetCount()
{
  return m_unit!=0;
}

CUnit* CCIQGoods::UnitGet(int index)
{
  return m_unit;
}

void CCIQGoods::OnButtonShowmap() 
{
  if(m_pointlist.empty())
    return;
	m_gamemap->SelectPoint(m_pointlist.begin()->pt);
}

CCIQGoods::point* CCIQGoods::GetNextTarget()
{
  if(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==0)
    return 0;

  if(m_pointtarget==m_pointlist.end())
  {
    m_pointtarget=m_pointlist.begin();
    if(m_pointtarget==m_pointlist.end())
      return 0;
  }
  return &*m_pointtarget++;
}

bool CCIQGoods::PointMoveBefore(CGameMap::point*p)
{
  for(pointlist::iterator i=m_pointlist.begin();i!=m_pointlist.end();i++)
  {
    if(*i==p)
    {
      return p==i->ppt;
    }
  }
  return false;
}

void CCIQGoods::PointMoveAfter(CGameMap::point*p)
{
  int x,y;
  m_gamemap->Translate(p,&x,&y);
  point* pp=reinterpret_cast<point*>(m_gamemap->GetPointData(p));
  if(pp!=0)
  {
    if(p==pp->pt)
    {
      pp->x=x;
      pp->y=y;
    }else if(p==pp->ppt)
    {
      pp->px=x;
      pp->py=y;
    }
    m_events->SetModified();

    SetUnit();
    UpdateUnit();
  }
}

void CCIQGoods::OnButtonDelete() 
{
  for(int i=0;i<m_gamemap->GetSelectedPointCount();i++)
  {
    point* pp=reinterpret_cast<point*>(m_gamemap->GetPointData(m_gamemap->GetSelectedPoint(i)));
    m_gamemap->DeletePoint(pp->pt);
    if(m_catchplayercoords)
    {
      m_gamemap->DeletePoint(pp->ppt);
    }
    {
      if(m_pointtarget!=m_pointlist.end())
      {
        if(pp==&*m_pointtarget)
          m_pointtarget++;
      }
      m_pointlist.remove(*pp);
      m_events->SetModified();
    }
  }
}

void CCIQGoods::OnCheckCatchmappoint() 
{
  SetCheck(IDC_CHECK_CATCHMAPPOINT);
}

void CCIQGoods::OnButtonDeleteall() 
{
  if(AfxMessageBox(IDS_ARE_YOU_SURE,MB_YESNO)!=IDYES)
    return;

  m_events->SetModified();
  if(m_pointlistline!=0)
    m_gamemap->DeleteLine(m_pointlistline);
  m_pointlistline=m_gamemap->AddLine();
  if(m_playerlistline!=0)
    m_gamemap->DeleteLine(m_playerlistline);
  if(m_catchplayercoords)
    m_playerlistline=m_gamemap->AddLine();
  m_pointlist.erase(m_pointlist.begin(),m_pointlist.end());
  m_pointtarget=m_pointlist.end();
}

void CCIQGoods::OnDestroy() 
{
  if(m_hunit!=0)
  {
    m_hunit->PostThreadMessage(WM_QUIT,0,0);
    m_unit->Abort();
    WaitForSingleObject(m_hunit->m_hThread,-1);
  }
  if(m_unit!=0)
  {
    m_unit->RemoveAdvise(this);
    m_unit=0;
  }
  m_gamemap->RemoveAdvise(this);

  CFormView::OnDestroy();
}

long CCIQGoods::OnUnitFinish(unsigned, long)
{
  if(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==1)
  {
    m_hunit->PostThreadMessage(CCIQGoodsUnit::msgStart,(unsigned)GetNextTarget(),0);
  }
  return 0;
}

long CCIQGoods::OnUnitStop(unsigned, long)
{
  if(m_hunit!=0)
  {
    m_hunit->PostThreadMessage(WM_QUIT,0,0);
    WaitForSingleObject(m_hunit->m_hThread,-1);
    m_hunit=0;
  }

  StopGUI();

  return 0;
}

void CCIQGoods::SetCheck(unsigned id)
{
  unsigned ids[]={IDC_BUTTON_START,IDC_CHECK_CATCHMAPPOINT};
  for(int i=0;i<sizeof(ids)/sizeof(unsigned);i++)
  {
    if(ids[i]==id)
      continue;
    ((CButton*)GetDlgItem(ids[i]))->SetCheck(0);
  }
  //if(id)
  //  ((CButton*)GetDlgItem(id))->SetCheck(!((CButton*)GetDlgItem(id))->GetCheck());

  UpdateData();
}

void CCIQGoods::OnCheckWokItem() 
{
  SetCheck(IDC_CHECK1);	
}

void CCIQGoods::OnCheckBackPack() 
{
  SetCheck(IDC_CHECK2);
}

void CCIQGoods::PostNcDestroy() 
{
	;
	//CFormView::PostNcDestroy();
}

void CCIQGoods::CheckClient()
{
  if(m_unit==0)
    throw std::exception(LoadString(IDS_STRING_ADDCLIENT));
}

void CCIQGoods::OnBnClickedCheckPlayerCords()
{
  UpdateData();

  SetUnit();
  m_events->SetModified(true);

  ReloadLinePoints();
}

void CCIQGoods::ReloadLinePoints()
{
  if(m_pointlistline!=0)
    m_gamemap->DeleteLine(m_pointlistline);
  m_pointlistline=m_gamemap->AddLine();
  if(m_playerlistline!=0)
    m_gamemap->DeleteLine(m_playerlistline);
  if(m_catchplayercoords)
    m_playerlistline=m_gamemap->AddLine();
  for(pointlist::iterator m=m_pointlist.begin();m!=m_pointlist.end();m++)
  {
    m->pt=m_gamemap->AddPoint(m->x,m->y,m_pointlistline);
    m_gamemap->SetPointData(m->pt,&*m);
  }
  if(m_catchplayercoords)
  {
    for(pointlist::iterator m=m_pointlist.begin();m!=m_pointlist.end();m++)
    {
      m->ppt=m_gamemap->AddPoint(m->px,m->py,m_playerlistline);
      m_gamemap->SetPointData(m->ppt,&*m);
    }
  }
}

void CCIQGoods::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
  __super::OnActivate(nState, pWndOther, bMinimized);

  // TODO: Add your message handler code here
}

int CCIQGoods::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
  return MA_NOACTIVATE;

  return __super::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

void CCIQGoods::SelectedItem(unsigned itemserial)
{
  if(m_catchbackpack.GetCheck())
  {
    m_backpackser=itemserial;
    m_catchbackpack.SetCheck(0);
    m_events->SetModified(true);
  }

  UpdateUnit();
}

void CCIQGoods::SelectedGround(WorldCord w,const Client::ItemType& it)
{
  if(m_catchmapoint)
  {
    WorldCord ww=m_unit->GetUnitPos();
    AddPoint(w.x,w.y,w.z,ww.x,ww.y,ww.z,it);
    m_events->SetModified(true);
  }
}

void CCIQGoods::SelectedPath(WorldCord w)
{
  m_gamemap->SetCursor(w.x,w.y);
}

void CCIQGoods::SelectedOption(const Client::ItemType &it)
{
  if(m_catchmakeitemtype.GetCheck())
  {
    m_makeitemtype=it;
    m_catchmakeitemtype.SetCheck(0);
    m_events->SetModified(true);
  }
  UpdateUnit();
}

void CCIQGoods::OnCbnSelendokComboHarvestuseitem()
{
  Client::ItemType* p=(Client::ItemType*)m_control_harvestuseitem.GetItemData(m_control_harvestuseitem.GetCurSel());
  m_harvestuseitemtype=*p;
  m_events->SetModified(true);
}

void CCIQGoods::OnCbnSelendokComboMakeuseitem()
{
  Client::ItemType* p=(Client::ItemType*)m_control_makeuseitem.GetItemData(m_control_makeuseitem.GetCurSel());
  m_makeuseitemtype=*p;
  m_events->SetModified(true);
}

void CCIQGoods::OnCbnSelendokComboGoodsitemtype()
{
  Client::ItemType* p=(Client::ItemType*)m_control_goodsitemtype.GetItemData(m_control_goodsitemtype.GetCurSel());
  m_goodsitemtype=*p;
  m_events->SetModified(true);
}
