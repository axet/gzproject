package GZone;

import java.awt.*;

public class GameMapNative implements GameMap
{

  public native void AddAdvise(GZone.GameMap.GameMapEvents c) ;
  
  public native GZone.GameMap.point AddPoint(int x, int y, GZone.GameMap.line p) ;
  
  public native void DeleteLine(GZone.GameMap.line l) ;
  
  public native GZone.GameMap.line DeletePoint(GZone.GameMap.point p) ;
  
  public native int GetPointData(GZone.GameMap.point p) ;
  
  public native void RemoveAdvise(GZone.GameMap.GameMapEvents c) ;
  
  public native void SelectPoint(GZone.GameMap.point p, boolean center, boolean clearother) ;
  
  public native void SetPointData(GZone.GameMap.point p, int data);

  public native point GetSelectedPoint(int index) ;
  
  public native int GetSelectedPointCount() ;
  
  public native Point Translate(GameMap.point p) ;
  
  public native GameMap.line AddLine() ;
  
  public native void Clear() ;
  
  public native void SetCursor(int x, int y) ;
  
  public native void SetViewPos(int x, int y) ;
};
