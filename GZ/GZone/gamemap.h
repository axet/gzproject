// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef GAMEMAP_H
#define GAMEMAP_H

class CGameMapEvents;

class CGameMap
{
public:
  // вспомогательные классы

  // класс строчки (typedef void* line)
  class line;
  // класс точки (typedef void* point)
  class point;

  // работа с картой

  // добавляет срочку в которой будут содержаться точки
  virtual line* AddLine() = 0;
  // добавляет точку в указанную строку, если последний параметр опущен
  // будет добавлена новая строка
  virtual point* AddPoint(int x,int y,line* p = 0) = 0;
  // установить зависимость между точкой и внешним объектом
  virtual void SetPointData(point*, void* data) = 0;
  // получить зависимость
  virtual void * GetPointData(point*) = 0;
  // установить курсор по координатам
  virtual void SetCursor(int x,int y) = 0;
  // установить точку просмотра
  virtual void SetViewPos(int x,int y) = 0;
  // удаляет точку
  virtual line* DeletePoint(point*) = 0;
  // удаляет строку целиком
  virtual void DeleteLine(line*) = 0;
  // очищает всю карту
  virtual void Clear() = 0;
  // получение координат точки
  virtual void Translate(point*,int *x,int*y) = 0;
  // делает точку выделенной
  virtual void SelectPoint(point*,bool center = true,bool clearother = true) = 0;
  // подписываеться на события от карты
  virtual void AddAdvise(CGameMapEvents*) = 0;
  // отписываеться от событей карты
  virtual void RemoveAdvise(CGameMapEvents*) = 0;
  // возвращает количество выделенных точек
  virtual int GetSelectedPointCount() = 0;
  // возвращет выдлеленную точку
  virtual point * GetSelectedPoint(int index) = 0;
};

class CGameMapEvents{
public:
  // точку выделена
  virtual void PointSelect(CGameMap::point*) {};
  // точку переместили
  virtual void PointMoveAfter(CGameMap::point*) {};
  // точку хотят переместить
  virtual bool PointMoveBefore(CGameMap::point*) {return true;}
  // точка подсвечена
  virtual void PointHiLight(CGameMap::point*) {};
};

#endif
