// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



#if !defined(AFX_CIQHARVESTER_H__6E31806B_8283_40C5_B5C6_292BF31116E4__INCLUDED_)
#define AFX_CIQHARVESTER_H__6E31806B_8283_40C5_B5C6_292BF31116E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CIQHarvester.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCIQHarvester form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "commanderiq.h"
#include "resource.h"
#include <map>
#include "ciqharvesterunit.h"
#include <list>

/// Класс соберателя дров.

class CCIQHarvester : public CFormView,public CCommanderIQ,public CClientGameEvents
{
public:

  /// Точка на карте, где нужно рубить.
  struct point
  {
    /// координаты мира х,у
    int x,y,z;
    /// координаты игрока
    int px,py,pz;
    /// тип предмета (дерево)
    short itemid;
    /// точка на карте
    CGameMap::point *pt;
    /// точка на карте с координатами
    CGameMap::point *ppt;

    point(int xx,int yy,int zz,int pxx,int pyy,int pzz,short iitemid):x(xx),y(yy),z(zz),
      px(pxx),py(pyy),pz(pzz),itemid(iitemid)
    {
    }
    point():x(0),y(0),z(0),px(0),py(0),pz(0),itemid(0),pt(0) {}

    bool operator == (const point& ppt)
    {
      return pt==ppt.pt;
    }

    bool operator == (const CGameMap::point* t)
    {
      return pt==t||ppt==t;
    }

    operator CPoint()
    {
      return CPoint(x,y);
    }
    operator CCIQHarvesterUnit::ZPoint()
    {
      return CCIQHarvesterUnit::ZPoint(x,y,z);
    }
  };
  /// список точек
  typedef std::list<point> pointlist;
  /// точка где нужно сбрасывать груз
  struct dropcont_tag
  {
    /// серийник контейнера где нужно сбрасывать
    DWORD dropcontainerserial;
    /// точка на карте
    point dropcontainer;

    dropcont_tag():dropcontainerserial(0) {;}
  };

  /// игрок хочет выбрать для ГЗ приложения предмет
  virtual void SelectedItem(unsigned itemserial);
  /// игрок выберает клеточку на земле
  virtual void SelectedGround(WorldCord w, const Client::ItemType &);
  /// игрок выберает  путь персонажа
  virtual void SelectedPath(WorldCord w);

private:
  CUnit *m_unit;
  /// персонаж для управления
  CCIQHarvesterUnit* m_hunit;
  /// список точек на которых идет сбор леса
  pointlist m_pointlist;
  /// линия карты для точек использования (мест рубки).
  CGameMap::line* m_pointlistline;
  /// линия карты для координат игрока.
  CGameMap::line* m_playerlistline;
  /// события о состоянии документа, (Modified)
  CCommanderIQEvents* m_events;

  /// серийники.
  unsigned m_useitemser,m_backpackser;
  std::string m_unitid;

  /// точка на которой работает игрок
  pointlist::iterator m_pointtarget;

  /// serial контейнера куда бросать
  dropcont_tag m_dropcont;
  /// точка на карте где можно восскреситься
  point m_resurrectionpoint;

  HWND FormCreate(HWND parent,CGameMap*,CCommanderIQEvents*);
  HWND FormGet();
  const char* FormGetName();
  // несколько качей не допустимы
  void SetCheck(unsigned id);
  void SetUnit();

  bool UnitAdd(CUnit*);
  bool UnitRemove(CUnit*);
  int UnitGetCount();
  CUnit* UnitGet(int index);
  const char* UnitMissing();

  bool PointMoveBefore(CGameMap::point*);
  void PointMoveAfter(CGameMap::point*);
  void PointSelect(CGameMap::point*);
  void PointHiLight(CGameMap::point*);

  void CheckDropUpdate();
  void CheckRessUpdate();
  void StopGUI();
  void Stop();
  
  void CheckClient();
  void UpdateDropPoint();
  void UpdateResPoint();
  void ReloadLinePoints();

  std::string Save();
  void Load(const char*);

public:
	CCIQHarvester();

public:
  enum { IDD = IDD_DIALOG_CIQHARVESTER };
  CButton	m_catchdropcontainer;
  CButton	m_catchitemuse;
  CButton	m_catchbackpack;
  CString	m_dropcontainerstatus;
  BOOL	m_catchmapoint;
  BOOL	m_catchdrop;
  BOOL	m_catchres;
  BOOL	m_catchplayercoords;

// Attributes
public:
  CGameMap *m_gamemap;

  void UpdateUnit();
  bool SetUnitItem(CCIQHarvesterUnit*,unsigned);
  bool SetUnitBackpack(CCIQHarvesterUnit*,unsigned);
  void SetDropContainer(CUnit*p,unsigned itemserial);
  static CCIQHarvester* Create();
  void AddPoint(int x,int y,int z,int px,int py,int pz,short itemid);
  point* GetNextTarget();

public:
  virtual void OnInitialUpdate();
  protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void PostNcDestroy();

protected:
	virtual ~CCIQHarvester();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

  long OnUnitFinish(unsigned, long);
  long OnUnitStop(unsigned, long);

  afx_msg void OnButtonStart();
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnButtonShowmap();
  afx_msg void OnButtonDelete();
  afx_msg void OnCheckCatchmappoint();
  afx_msg void OnButtonDeleteall();
  afx_msg void OnDestroy();
  afx_msg void OnButtonResurrectionpoint();
  afx_msg void OnCheckWokItem();
  afx_msg void OnCheckBackPack();
  afx_msg void OnCheckDropItem();
  DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedCheckress();
  afx_msg void OnBnClickedCheckdrop();
  afx_msg void OnBnClickedCheckPlayerCords();
  afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
  afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIQHARVESTER_H__6E31806B_8283_40C5_B5C6_292BF31116E4__INCLUDED_)
