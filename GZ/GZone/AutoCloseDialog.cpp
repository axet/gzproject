// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// AutoCloseDialog.cpp : implementation file
//

#include "stdafx.h"
#include "GZone.h"
#include "AutoCloseDialog.h"
#include ".\autoclosedialog.h"

extern CGZoneApp theApp;

// CAutoCloseDialog dialog

IMPLEMENT_DYNAMIC(CAutoCloseDialog, CDialog)
CAutoCloseDialog::CAutoCloseDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CAutoCloseDialog::IDD, pParent)
{
}

CAutoCloseDialog::CAutoCloseDialog(const char* p,CWnd* pParent /*=NULL*/,bool cancel)
: CDialog(CAutoCloseDialog::IDD, pParent)
{
  m_text=p;
  m_show=cancel;
  if(!m_show)
    DoModal();
}

CAutoCloseDialog::~CAutoCloseDialog()
{
}

void CAutoCloseDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAutoCloseDialog, CDialog)
  ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
  ON_BN_CLICKED(IDOK, OnBnClickedOk)
  ON_WM_CLOSE()
  ON_WM_TIMER()
  ON_WM_LBUTTONDBLCLK()
  ON_WM_LBUTTONDOWN()
  ON_EN_SETFOCUS(IDC_STATIC_TEXT, OnEnSetfocusStaticText)
END_MESSAGE_MAP()


// CAutoCloseDialog message handlers

void CAutoCloseDialog::OnBnClickedCancel()
{
  // TODO: Add your control notification handler code here
  OnCancel();
}

void CAutoCloseDialog::EnableCancel()
{
  m_show=true;
}

void CAutoCloseDialog::OnBnClickedOk()
{
  // TODO: Add your control notification handler code here
  OnOK();
}

void CAutoCloseDialog::OnClose()
{
  // TODO: Add your message handler code here and/or call default

  CDialog::OnClose();
}

BOOL CAutoCloseDialog::OnInitDialog()
{
  CDialog::OnInitDialog();

  CString s;
  theApp.GetMainWnd()->GetWindowText(s);
  SetWindowText(s);
  m_text.Replace("\r\n","\n");
  m_text.Replace("\r","\n");
  m_text.Replace("\n","\r\n");
  GetDlgItem(IDC_STATIC_TEXT)->SetWindowText(m_text);
  m_timer=10;
  SetTimer(0,1000,0);
  UpdateTimer();

  if(m_show)
    GetDlgItem(IDCANCEL)->ShowWindow(SW_SHOW);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CAutoCloseDialog::UpdateTimer()
{
  CString s;
  s.Format("OK %d",m_timer);
  GetDlgItem(IDOK)->SetWindowText(s);
}

void CAutoCloseDialog::OnTimer(UINT nIDEvent)
{
  m_timer--;

  if(m_timer<=0)
    OnOK();

  UpdateTimer();

  CDialog::OnTimer(nIDEvent);
}

void CAutoCloseDialog::KillTimer()
{
  CDialog::KillTimer(0);
  CString s;
  s.Format("OK");
  GetDlgItem(IDOK)->SetWindowText(s);
}

void CAutoCloseDialog::OnLButtonDblClk(UINT nFlags, CPoint point)
{
  KillTimer();
  CDialog::OnLButtonDblClk(nFlags, point);
}

void CAutoCloseDialog::OnLButtonDown(UINT nFlags, CPoint point)
{
  KillTimer();

  CDialog::OnLButtonDown(nFlags, point);
}

void CAutoCloseDialog::OnEnSetfocusStaticText()
{
  KillTimer();
}
