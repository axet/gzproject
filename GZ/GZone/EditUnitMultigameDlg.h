#pragma once
#include "afxcmn.h"


// EditUnitMultigameDlg dialog

class EditUnitMultigameDlg : public CDialog
{
	DECLARE_DYNAMIC(EditUnitMultigameDlg)

public:
	EditUnitMultigameDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~EditUnitMultigameDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_EDITUNIT_MULTIGAME };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  virtual INT_PTR DoModal();
public:
  CTabCtrl m_tab;
};
