// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if !defined(AFX_COMMANDERSELECTDLG_H__F7417A56_3F1E_491C_BD50_13FD89F2A86A__INCLUDED_)
#define AFX_COMMANDERSELECTDLG_H__F7417A56_3F1E_491C_BD50_13FD89F2A86A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommanderSelectDlg.h : header file
//

#include "gzonedoc.h"

/////////////////////////////////////////////////////////////////////////////
// CCommanderSelectDlg dialog

class CCommanderSelectDlg : public CDialog
{
// Construction
public:
  DWORD m_itemdata;
  CCommanderIQList* m_ciqlist;

	CCommanderSelectDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCommanderSelectDlg)
	enum { IDD = IDD_DIALOG_COMMANDERSELECT };
	CListBox	m_list;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommanderSelectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCommanderSelectDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkList1();
	afx_msg void OnSelchangeList1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMANDERSELECTDLG_H__F7417A56_3F1E_491C_BD50_13FD89F2A86A__INCLUDED_)
