// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "GZone.h"

#include "MainFrm.h"
#include "Splash.h"

#include <exception>
#include <include/hrwrap.h>
#include ".\mainfrm.h"

#include <io.h>
#include <iniext/iniext.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
  ON_WM_CREATE()
  ON_COMMAND(ID_VIEW_UNITSLIST, OnViewUnitslist)
  ON_UPDATE_COMMAND_UI(ID_VIEW_UNITSLIST, OnUpdateViewUnitslist)
  ON_COMMAND(ID_FILE_OPTIONS, OnFileOptions)
  ON_WM_DESTROY()
  ON_MESSAGE(WM_USER+2,OnThreadEvent)
  ON_UPDATE_COMMAND_UI(ID_TTTT_DFDAF, OnUpdateTtttDfdaf)
  ON_UPDATE_COMMAND_UI(ID_TTTT_TTTT, OnUpdateTtttTttt)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

  //if (!m_wndDlgBar.Create(this, IDR_MAINFRAME, CBRS_ALIGN_TOP, AFX_IDW_DIALOGBAR)){TRACE0("Failed to create dialogbar\n");return -1;		// fail to create}

	if (!m_wndReBar.Create(this) ||
		!m_wndReBar.AddBar(&m_wndToolBar)
    //|| !m_wndReBar.AddBar(&m_wndDlgBar)
    )
	{
		TRACE0("Failed to create rebar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Remove this if you don't want tool tips
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);

  m_wndSoldierListView.Create(this, 123);
  m_wndSoldierListView.SetBarStyle(m_wndSoldierListView.GetBarStyle() |
		  CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

  EnableDocking(CBRS_ALIGN_ANY);

  m_wndSoldierListView.EnableDocking(CBRS_ALIGN_ANY);
  DockControlBar(&m_wndSoldierListView, AFX_IDW_DOCKBAR_RIGHT);

	// CG: The following line was added by the Splash Screen component.
	CSplashWnd::ShowSplashScreen(this);

  for(UnitList_t::iterator u=App()->m_unitlist.begin();u!=App()->m_unitlist.end();u++)
  {
    (*u)->AddAdvise(this);
    (*u)->AddSoldierAdvise(this);
  }

  return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::OnViewUnitslist() 
{
  if(m_wndSoldierListView.IsVisible())
    ShowControlBar(&m_wndSoldierListView,FALSE,FALSE);
  else
    ShowControlBar(&m_wndSoldierListView,TRUE,FALSE);
}

void CMainFrame::OnUpdateViewUnitslist(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_wndSoldierListView.IsVisible());
}

void CMainFrame::OnFileOptions() 
{
	COptionsDlg dlg;
  static_cast<OptionsDlg&>(dlg)=g_optionsdlg;

  if(dlg.DoModal()!=IDOK)
    return;

  g_optionsdlg=dlg;

  LoadBGImage();
}

void CMainFrame::OnDestroy() 
{
  for(UnitList_t::iterator u=App()->m_unitlist.begin();u!=App()->m_unitlist.end();u++)
  {
    (*u)->RemoveAdvise(this);
    (*u)->RemoveSoldierAdvise(this);
  }

  CMDIFrameWnd::OnDestroy();
}

IPicturePtr CMainFrame::GetWorldMap()
{
  try
  {
    if(m_worldmap==0)
      LoadBGImage();
  }catch(std::exception &)
  {
  }

  return m_worldmap;
}

void CMainFrame::LoadBGImage()
{
  if(m_worldmap)
    m_worldmap.Release();

  if(g_optionsdlg.pathbgimage.IsEmpty())
    return;

  CString path;
  if(access(g_optionsdlg.pathbgimage,04)==0)
    path=g_optionsdlg.pathbgimage;
  if(access((GetModuleRunDir()+(const char*)g_optionsdlg.pathbgimage).c_str(),04)==0)
    path=(GetModuleRunDir()+(const char*)g_optionsdlg.pathbgimage).c_str();

  if(OleLoadPicturePath(_bstr_t(path),0,0,0,__uuidof(m_worldmap),(LPVOID*)&m_worldmap)!=S_OK)
  {
    throw std::exception(ErrorReport(IDS_NOIMAGEFOUND)+(const char*)g_optionsdlg.pathbgimage);
  }
}

LRESULT CMainFrame::WindowProc(UINT message,WPARAM wparam,LPARAM lparam)
{
  return CWnd::WindowProc(message,wparam,lparam);
}
void CMainFrame::OnUpdateTtttDfdaf(CCmdUI *pCmdUI)
{
  // TODO: Add your command update UI handler code here
}

void CMainFrame::OnUpdateTtttTttt(CCmdUI *pCmdUI)
{
  // TODO: Add your command update UI handler code here
}

void CMainFrame::ThreadGZUnitEvent(EventThread* p)
{
  PostMessage(WM_USER+2,(WPARAM)p);
}

long CMainFrame::OnThreadEvent(unsigned u,long)
{
  EventThread* p=(EventThread*)u;
  p->ReadEventSoldier();
  return 0;
}
