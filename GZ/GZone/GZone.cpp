// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// GZone.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "GZone.h"
#include "MainFrm.h"
#include "ChildFrm.h"
#include "GZoneDoc.h"
#include "GZoneView.h"
#include "Splash.h"

#include <direct.h>
#include <iniext/iniext.h>
#include <errorreport/errorreport.h>
#include <algorithm>

#include "xsd/Schema/Schema.h"
#include ".\gzone.h"
#include "optionsdlg.h"
#include "gzunit//InterruptableUnit.h"
#include "afxwin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace gc;

CString LoadString(int i)
{
  CString str;
  str.LoadString(i);
  return str;
}

OptionsDlg g_optionsdlg;

/////////////////////////////////////////////////////////////////////////////
// CGZoneApp

BEGIN_MESSAGE_MAP(CGZoneApp, CWinApp)
	//{{AFX_MSG_MAP(CGZoneApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGZoneApp construction

CGZoneApp::CGZoneApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGZoneApp object

CGZoneApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CGZoneApp initialization

BOOL CGZoneApp::InitInstance()
{
	// CG: The following block was added by the Splash Screen component.
	{
		CCommandLineInfo cmdInfo;
		ParseCommandLine(cmdInfo);
		CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowSplash);
	}
	AfxEnableControlContainer();

  AfxInitRichEdit();

  CoInitialize(0);

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("GZone"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(
		IDR_GZONETYPE,
		RUNTIME_CLASS(CGZoneDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CGZoneView));
	AddDocTemplate(pDocTemplate);


  try
  {
    LoadUI();

    // create main MDI Frame window
    CMainFrame* pMainFrame = new CMainFrame;
    if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
	    return FALSE;
    m_pMainWnd = pMainFrame;

    // Parse command line for standard shell commands, DDE, file open
    CCommandLineInfo cmdInfo;
    ParseCommandLine(cmdInfo);
    (cmdInfo).m_nShellCommand=CCommandLineInfo::FileNothing;

    // Dispatch commands specified on the command line
    if (!ProcessShellCommand(cmdInfo))
	    return FALSE;

    // The main window has been initialized, so show and update it.
    pMainFrame->ShowWindow(m_nCmdShow);
    pMainFrame->UpdateWindow();
  }
  catch(CXmlException &e)
  {
    AfxMessageBox(ErrorReport(IDS_XMLEXCEPTION)+e.GetInfo().c_str());
    return FALSE;
  }catch(COutOfProcMaster::ExecuteException &e)
  {
    AfxMessageBox(ErrorReport(IDS_ERRORONCLIENT)+e.what());
    return FALSE;
  }catch(std::exception &e)
  {
    AfxMessageBox(ErrorReport(IDS_SYSTEMERROR)+e.what());
    return FALSE;
  }catch(_com_error &e)
  {
    AfxMessageBox(ErrorReport(LoadString(IDS_SYSTEMERROR),e));
    return FALSE;
  }

  return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  CEdit gpl;
public:
  CString m_gpltext;
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
, m_gpltext(_T(""))
{
  m_gpltext=
   "GZProject, Ultima Online utils." "\xd\xa"
   "Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net" "\xd\xa"
   "" "\xd\xa"
   "This program is free software; you can redistribute it and/or" "\xd\xa"
   "modify it under the terms of the GNU General Public License" "\xd\xa"
   "as published by the Free Software Foundation; either version 2" "\xd\xa"
   "of the License, or (at your option) any later version." "\xd\xa"
   "" "\xd\xa"
   "This program is distributed in the hope that it will be useful," "\xd\xa"
   "but WITHOUT ANY WARRANTY; without even the implied warranty of" "\xd\xa"
   "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the" "\xd\xa"
   "GNU General Public License for more details." "\xd\xa"
   "" "\xd\xa"
   "You should have received a copy of the GNU General Public License" "\xd\xa"
   "along with this program; if not, write to the Free Software" "\xd\xa"
   "Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA." "\xd\xa";

	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CAboutDlg)
  //}}AFX_DATA_MAP
  DDX_Control(pDX, IDC_EDIT2, gpl);
  DDX_Text(pDX, IDC_EDIT2, m_gpltext);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CGZoneApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CGZoneApp message handlers


int CGZoneApp::ExitInstance() 
{
  try
  {
    SaveUI();
  }
  catch(CXmlException &e)
  {
    AfxMessageBox(ErrorReport(IDS_XMLEXCEPTION)+e.GetInfo().c_str());
    return FALSE;
  }catch(COutOfProcMaster::ExecuteException &e)
  {
    AfxMessageBox(ErrorReport(IDS_ERRORONCLIENT)+e.what());
    return FALSE;
  }catch(std::exception &e)
  {
    AfxMessageBox(ErrorReport(IDS_SYSTEMERROR)+e.what());
    return FALSE;
  }catch(_com_error &e)
  {
    AfxMessageBox(ErrorReport(LoadString(IDS_SYSTEMERROR),e));
    return FALSE;
  }

  CoUninitialize();

  return CWinApp::ExitInstance();
}

void CGZoneApp::LoadUI()
{
  CSchemaDoc doc;
  try
  {
    CConfigType config=doc.Load((const char*)GetIni());
    for(int i=0;i<config.GetAccountCount();i++)
    {
      CAccountType ac=config.GetAccountAt(i);
      m_unitlist.push_back(new InterruptableUnit<CGZUnit>(((tstring)ac.GetLogin()).c_str(),
        ((tstring)ac.GetPassword()).c_str(),
        ((tstring)ac.GetCharName()).c_str(),
        ac.GetShowWindow()));
    }
    g_optionsdlg.Load(config);
  }
  catch(CXmlException &)
  {
  }
  catch(_com_error &e)
  {
    throw std::exception(ErrorReport(LoadString(IDS_ERRORSETTINGS),e));
  }
  catch(std::exception &)
  {
    ;
  }
}

CString CGZoneApp::GetIniPath()
{
  char buf[MAX_PATH];
  GetEnvironmentVariableA("APPDATA",buf,sizeof(buf));
  std::string path=buf;
  path+="\\GZone\\";
  return path.c_str();
}

CString CGZoneApp::GetIni()
{
  CString s=GetIniPath();
  return (s+"gzone.config");
}

void CGZoneApp::SaveUI()
{
  mkdir(GetIniPath());

  CSchemaDoc doc;

  CConfigType config;

  for(UnitList_t::iterator i=m_unitlist.begin();i!=m_unitlist.end();i++)
  {
    CAccountType ac;
    CGZUnit::LInfo l=(*i)->GetLInfo();
    ac.AddLogin(l.login.c_str());
    ac.AddCharName(l.charname.c_str());
    ac.AddPassword(l.password.c_str());
    ac.AddShowWindow((*i)->IsWindowVisible());
    config.AddAccount(ac);
  }

  g_optionsdlg.Save(config);

  doc.SetRootElementName("http://gzproject.sourceforge.net/GZoneConfig","Config");
  doc.Save((const char*)GetIni(),config);
}

BOOL CGZoneApp::PreTranslateMessage(MSG* pMsg)
{
	// CG: The following lines were added by the Splash Screen component.
	if (CSplashWnd::PreTranslateAppMessage(pMsg))
		return TRUE;

	return CWinApp::PreTranslateMessage(pMsg);
}

int CGZoneApp::Run()
{
  CoInitialize(0);

  while(1)
  {
    try
    {
      return CWinApp::Run();
    }
    catch(CXmlException &e)
    {
      AfxMessageBox(ErrorReport(IDS_XMLEXCEPTION)+e.GetInfo().c_str());
    }catch(COutOfProcMaster::ExecuteException &e)
    {
      AfxMessageBox(ErrorReport(IDS_ERRORONCLIENT)+e.what());
    }catch(std::exception &e)
    {
      AfxMessageBox(ErrorReport(IDS_SYSTEMERROR)+e.what());
    }catch(_com_error &e)
    {
      AfxMessageBox(ErrorReport(LoadString(IDS_SYSTEMERROR),e));
    }
    ((CMainFrame*)AfxGetMainWnd())->m_wndSoldierListView.UpdateList();
  }

  CoUninitialize();
}
