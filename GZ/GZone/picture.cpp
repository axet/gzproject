// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

////////////////////////////////////////////////////////////////
// MSDN Magazine — October 2001
// If this code works, it was written by Paul DiLascia.
// If not, I don't know who wrote it.
// Compiles with Visual C++ 6.0 for Windows 98 and probably Windows 2000 
// too.
// Set tabsize = 3 in your editor.
//
#include "StdAfx.h"
#include "Picture.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////////////////////////
// CPicture implementation
//

CPicture::CPicture()
{
}

CPicture::~CPicture()
{
}

//////////////////
// Load from resource. Looks for "IMAGE" type.
//
BOOL CPicture::Load(UINT nIDRes)
{
  // find resource in resource file
  HINSTANCE hInst = AfxGetResourceHandle();
  HRSRC hRsrc = ::FindResource(hInst,
    MAKEINTRESOURCE(nIDRes),
    "IMAGE"); // type
  if (!hRsrc)
    return FALSE;

  // load resource into memory
  DWORD len = SizeofResource(hInst, hRsrc);
  BYTE* lpRsrc = (BYTE*)LoadResource(hInst, hRsrc);
  if (!lpRsrc)
    return FALSE;

  // create memory file and load it
  CMemFile file(lpRsrc, len);
  BOOL bRet = Load(file);
  FreeResource(hRsrc);
  return bRet;
}

//////////////////
// Load from path name.
//
BOOL CPicture::Load(LPCTSTR pszPathName)
{
  CFile file;
  if (!file.Open(pszPathName, CFile::modeRead|CFile::shareDenyWrite))
    return FALSE;
  BOOL bRet = Load(file);
  file.Close();
  return bRet;
}

//////////////////
// Load from CFile
//
BOOL CPicture::Load(CFile& file)
{
  CArchive ar(&file, CArchive::load | CArchive::bNoFlushOnDelete);
  return Load(ar);
}

//////////////////
// Load from archive—create stream and load from stream.
//
BOOL CPicture::Load(CArchive& ar)
{
  CArchiveStream arcstream(&ar);
  return Load((IStream*)&arcstream);
}

//////////////////
// Load from stream (IStream). This is the one that really does it: call
// OleLoadPicture to do the work.
//
BOOL CPicture::Load(IStream* pstm)
{
  Free();
  HRESULT hr = OleLoadPicture(pstm, 0, FALSE,
    IID_IPicture, (void**)&m_spIPicture);
  ASSERT(SUCCEEDED(hr) && m_spIPicture); 
  return TRUE;
}

//////////////////
// Render to device context. Covert to HIMETRIC for IPicture.
//
BOOL CPicture::Render(CDC* pDC, CRect rc, LPCRECT prcMFBounds) const
{
  ASSERT(pDC);

  if (rc.IsRectNull()) {
    CSize sz = GetImageSize(pDC);
    rc.right = sz.cx;
    rc.bottom = sz.cy;
  }
  long hmWidth,hmHeight; // HIMETRIC units
  GetHIMETRICSize(hmWidth, hmHeight);
  m_spIPicture->Render(*pDC, rc.left, rc.top, rc.Width(), rc.Height(),
    0, hmHeight, hmWidth, -hmHeight, prcMFBounds);

  return TRUE;
}

BOOL CPicture::Render(CDC* pDC, CRect rc,CRect rcImage) const
{
  ASSERT(pDC);

  if (rc.IsRectNull()) {
    CSize sz = GetImageSize(pDC);
    rc.right = sz.cx;
    rc.bottom = sz.cy;
  }

  long hmWidth,hmHeight; // HIMETRIC units
  GetHIMETRICSize(hmWidth, hmHeight);

  CRect himetricsource;

  if(rcImage.IsRectNull())
  {
    himetricsource.left=0;
    himetricsource.top=0;
    himetricsource.right=hmWidth;
    himetricsource.bottom=hmHeight;
  }else
  {
    int nPixelsPerInchX;    // Pixels per logical inch along width
    int nPixelsPerInchY;    // Pixels per logical inch along height

    HDC hDCScreen = GetDC(NULL);
    ATLASSERT(hDCScreen != NULL);
    nPixelsPerInchX = GetDeviceCaps(hDCScreen, LOGPIXELSX);
    nPixelsPerInchY = GetDeviceCaps(hDCScreen, LOGPIXELSY);
    ReleaseDC(NULL, hDCScreen);

    int left = MAP_PIX_TO_LOGHIM(rcImage.left, nPixelsPerInchX);
    int right = MAP_PIX_TO_LOGHIM(rcImage.right, nPixelsPerInchX);
    int top = MAP_PIX_TO_LOGHIM(rcImage.top, nPixelsPerInchY);
    int bottom = MAP_PIX_TO_LOGHIM(rcImage.bottom, nPixelsPerInchY);
    himetricsource=CRect(left,hmHeight-top,right,hmHeight-bottom);
  }

  m_spIPicture->Render(*pDC, rc.left, rc.top, rc.Width(), rc.Height(),
    himetricsource.left, himetricsource.top, himetricsource.Width(), himetricsource.Height(),
    0);

  //if(rcImage.left<0)
  //{
  //  int left=hmWidth+himetricsource.left;
  //  int right=left+himetricsource.Width();
  //  CRect image=CRect(left,himetricsource.top,right, himetricsource.bottom);
  //  m_spIPicture->Render(*pDC, rc.left, rc.top, rc.Width(), rc.Height(),
  //    image.left, image.top, image.Width(), image.Height(),
  //    0);
  //}

  //if(rcImage.top<0)
  //{
  //  int top=himetricsource.top-hmHeight;
  //  int bottom=top+himetricsource.Height();
  //  CRect image=CRect(himetricsource.left,top,himetricsource.right, bottom);
  //  m_spIPicture->Render(*pDC, rc.left, rc.top, rc.Width(), rc.Height(),
  //    image.left, image.top, image.Width(), image.Height(),
  //    0);
  //}

  //if(rcImage.top<0&&rcImage.left<0)
  //{
  //  int left=hmWidth+himetricsource.left;
  //  int right=left+himetricsource.Width();
  //  int top=himetricsource.top-hmHeight;
  //  int bottom=top+himetricsource.Height();
  //  CRect image=CRect(left,top,right, bottom);
  //  m_spIPicture->Render(*pDC, rc.left, rc.top, rc.Width(), rc.Height(),
  //    image.left, image.top, image.Width(), image.Height(),
  //    0);
  //}

  return TRUE;
}

//////////////////
// Get image size in pixels. Converts from HIMETRIC to device coords.
//
CSize CPicture::GetImageSize(CDC* pDC) const
{
  if (!m_spIPicture)
    return CSize(0,0);

  LONG hmWidth, hmHeight; // HIMETRIC units
  m_spIPicture->get_Width(&hmWidth);
  m_spIPicture->get_Height(&hmHeight);
  CSize sz(hmWidth,hmHeight);
  if (pDC==NULL) {
    CWindowDC dc(NULL);
    dc.HIMETRICtoDP(&sz); // convert to pixels
  } else {
    pDC->HIMETRICtoDP(&sz);
  }
  return sz;
}

void CPicture::Create(IPicture *p)
{
  m_spIPicture=p;
}

BOOL CPicture::IsValid()
{
  return m_spIPicture!=0;
}
