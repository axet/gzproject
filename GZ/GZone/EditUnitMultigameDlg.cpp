// EditUnitMultigameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GZone.h"
#include "EditUnitMultigameDlg.h"


// EditUnitMultigameDlg dialog

IMPLEMENT_DYNAMIC(EditUnitMultigameDlg, CDialog)

EditUnitMultigameDlg::EditUnitMultigameDlg(CWnd* pParent /*=NULL*/)
	: CDialog(EditUnitMultigameDlg::IDD, pParent)
{

}

EditUnitMultigameDlg::~EditUnitMultigameDlg()
{
}

void EditUnitMultigameDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_TAB1, m_tab);
}


BEGIN_MESSAGE_MAP(EditUnitMultigameDlg, CDialog)
END_MESSAGE_MAP()


// EditUnitMultigameDlg message handlers

INT_PTR EditUnitMultigameDlg::DoModal()
{
  m_tab.InsertItem(0,"Ultima");
  m_tab.InsertItem(1,"Diablo2");

  return CDialog::DoModal();
}
