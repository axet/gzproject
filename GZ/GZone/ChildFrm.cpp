// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// ChildFrm.cpp : implementation of the CChildFrame class
//

#include "stdafx.h"
#include "GZone.h"

#include "ChildFrm.h"
#include "nullview.h"
#include "mainfrm.h"
#include "gzonedoc.h"

#include "commanderiq.h"
#include ".\childfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CChildFrame)
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame construction/destruction

CChildFrame::CChildFrame()
{
	// TODO: add member initialization code here
	
}

CChildFrame::~CChildFrame()
{
}

BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CChildFrame diagnostics

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChildFrame message handlers

BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	m_wndSplitter.CreateStatic(this,2,1);
  m_wndSplitter.CreateView(0,0,RUNTIME_CLASS(CNullView),CSize(100,100),pContext);

  m_pnull=m_wndSplitter.GetPane(0,0);

  m_map.Create(&m_wndSplitter);
  m_map.SetBGImage(((CMainFrame*)AfxGetMainWnd())->GetWorldMap());

  m_map.SetDlgCtrlID(m_wndSplitter.IdFromRowCol(1,0));

  m_wndSplitter.SetRowInfo(0,200,0);

	return TRUE;//CMDIChildWnd::OnCreateClient(lpcs, pContext);
}

void CChildFrame::SwitchCommander()
{
  CGZoneDoc* doc=(CGZoneDoc*)GetActiveDocument();
  CCommanderIQ *p=doc->m_commander;

  try
  {
    CWnd *pwnd=0;
    pwnd=m_wndSplitter.GetPane(0,0);
    pwnd->SetDlgCtrlID(0);
    pwnd->ShowWindow(SW_HIDE);
    CWnd* pcomwnd=0;
    pcomwnd=FromHandle(p->FormGet());
    if(pcomwnd==0)
    {
      //CNullView*pv;
      //pv=(CNullView*)RUNTIME_CLASS(CNullView)->CreateObject();
      //pv->Create(0,0,WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),&m_wndSplitter,m_wndSplitter.IdFromRowCol(0,0),&m_Context);
      //CWnd *formwnd=FromHandle(p->FormCreate(*pv,&m_map,doc));
      //formwnd->SetDlgCtrlID(AFX_IDW_PANE_FIRST);
      //pcomwnd=pv;
      //(*((CView*)pcomwnd)).m_pDocument=doc;
      CWnd *formwnd=FromHandle(p->FormCreate(m_wndSplitter,&m_map,doc));
      formwnd->SetDlgCtrlID(m_wndSplitter.IdFromRowCol(0,0));
      pcomwnd=formwnd;
    }
    if(p!=0&&pcomwnd!=0)
      pwnd=pcomwnd;
    else
      pwnd=m_pnull;
    //pwnd->ShowWindow(SW_SHOW);
    //pwnd->SetDlgCtrlID(m_wndSplitter.IdFromRowCol(0,0));
    m_wndSplitter.RecalcLayout();
  }catch(std::exception &e)
  {
    throw std::exception(ErrorReport(p->FormGetName(),e.what()));
  }
}

void CChildFrame::ActivateFrame(int nCmdShow) 
{
	CMDIChildWnd::ActivateFrame(nCmdShow);
}

void CChildFrame::OnSetFocus(CWnd* pOldWnd) 
{
	CMDIChildWnd::OnSetFocus(pOldWnd);
	
  ((CMainFrame*)AfxGetMainWnd())->m_wndSoldierListView.UpdateList();
}

BOOL CChildFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext) 
{
	m_Context=*pContext;
	
	if(!CMDIChildWnd::LoadFrame(nIDResource, dwDefaultStyle, pParentWnd, pContext))
    return FALSE;
  return TRUE;
}

void CChildFrame::OnDestroy() 
{
	CMDIChildWnd::OnDestroy();
	
  ((CMainFrame*)AfxGetMainWnd())->m_wndSoldierListView.UpdateList();
	
}

CDocument* CChildFrame::GetActiveDocument()
{
  //return GetActiveDocument();

  return CMDIChildWnd::GetActiveDocument();
}
