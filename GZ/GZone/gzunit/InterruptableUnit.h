#pragma once

#include "../../client/unit.h"
#include "../../interproc_clientaddin/outofprocmasterevents.h"

class COutOfProcMasterEvents;
class CClientGameEvents;
class CGZUnit;

template <class T> class InterruptableUnit:public T
{
  HANDLE m_abortevent;

  class AbortMode:public COutOfProcMasterEvents::AbortHandle
  {
    HANDLE ha;
  public:
    AbortMode(COutOfProcMasterEvents*q,HANDLE h):AbortHandle(q,h),ha(h)
    {
    }
    ~AbortMode()
    {
      ResetEvent(ha);
    }
  };

public:
  InterruptableUnit(const char* l = "",const char* p = "",const char *c = "",bool showwindow = false):T(l,p,c,showwindow)
  {
    m_abortevent=CreateEvent(NULL,TRUE,FALSE,NULL);
  }

  ~InterruptableUnit()
  {
    CloseHandle(m_abortevent);
  }

  void AddAdvise(CClientEvents*p)
  {
    AbortMode ah(this,m_abortevent);
    T::AddAdvise(p);
  }

  void RemoveAdvise(CClientEvents*p)
  {
    AbortMode ah(this,m_abortevent);
    T::RemoveAdvise(p);
  }

  void AddAdvise(CClientGameEvents*p)
  {
    AbortMode ah(this,m_abortevent);
    T::AddAdvise(p);
  }

  void RemoveAdvise(CClientGameEvents*p)
  {
    AbortMode ah(this,m_abortevent);
    T::RemoveAdvise(p);
  }

  void Abort()
  {
    SetEvent(m_abortevent);
  };

  const char* GetUnitId()
  {
    return T::GetUnitId();
  }

  WorldCord GetUnitPos()
  {
    AbortMode ah(this,m_abortevent);
    return T::GetUnitPos();
  };

  void CharMoveTo(WorldCord w)
  {
    AbortMode ah(this,m_abortevent);
    T::CharMoveTo(w);
  };

  void CharSpeech(const char* buf)
  {
    AbortMode ah(this,m_abortevent);
    T::CharSpeech(buf);
  }

  std::string UseItem(unsigned itemserial)
  {
    AbortMode ah(this,m_abortevent);
    return T::UseItem(itemserial);
  }

  std::string UseItem(unsigned itemserialtouse,unsigned itemserialonuse)
  {
    AbortMode ah(this,m_abortevent);
    return T::UseItem(itemserialtouse,itemserialonuse);
  }

  std::string UseItem(unsigned itemserialtouse, WorldCord w, const Client::ItemType &it)
  {
    AbortMode ah(this,m_abortevent);
    return T::UseItem(itemserialtouse,w,it);
  }

  unsigned UseItemGump(unsigned itemserialtouse,const UnsignedList &itemserialtoselect, const ShortList &itemslist)
  {
    AbortMode ah(this,m_abortevent);
    return T::UseItemGump(itemserialtouse,itemserialtoselect,itemslist);
  }

  void MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType &it,unsigned containerserialto)
  {
    AbortMode ah(this,m_abortevent);
    T::MoveItemsG2G(containerserialfrom,it,containerserialto);
  }

  void MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType &it,int count, unsigned containerserialto) 
  {
    AbortMode ah(this,m_abortevent);
    T::MoveItemsG2G(containerserialfrom,it,count,containerserialto);
  }

  unsigned PinchItem(const Client::ItemType &it,unsigned containerserialfrom,int count)
  {
    AbortMode ah(this,m_abortevent);
    return T::PinchItem(it,containerserialfrom,count);
  }

  unsigned MoveItem(unsigned itemwhat,unsigned containerserialto, int count) 
  {
    AbortMode ah(this,m_abortevent);
    return T::MoveItem(itemwhat,containerserialto,count);
  }
  unsigned GetContainerItem(const Client::ItemType &it,unsigned containerserialfrom)
  {
    AbortMode ah(this,m_abortevent);
    return T::GetContainerItem(it,containerserialfrom);
  }
  unsigned GetEquippedItem(const Client::ItemType &it,const char* name)
  {
    AbortMode ah(this,m_abortevent);
    return T::GetEquippedItem(it,name);
  }

  unsigned SelectItem(unsigned containerserialwhere) 
  {
    AbortMode ah(this,m_abortevent);
    return T::SelectItem(containerserialwhere);
  }

  unsigned SelectItem(unsigned containerserialwhere,const Client::ItemType &it) 
  {
    AbortMode ah(this,m_abortevent);
    return T::SelectItem(containerserialwhere,it);
  }

  unsigned SelectItem(unsigned containerserialfrom,const Client::ItemType &it,int count) 
  {
    AbortMode ah(this,m_abortevent);
    return T::SelectItem(containerserialfrom,it,count);
  }

  int StatsGetHits() 
  {
    AbortMode ah(this,m_abortevent);
    return T::StatsGetHits();
  }

  int StatsGetMana() 
  {
    AbortMode ah(this,m_abortevent);
    return T::StatsGetMana();
  }

  int StatsGetStam() 
  {
    AbortMode ah(this,m_abortevent);
    return T::StatsGetStam();
  }

  int StatsGetWeight() 
  {
    AbortMode ah(this,m_abortevent);
    return T::StatsGetWeight();
  }

  bool operator == (CGZUnit*p) const
  {
    return m_unit==p;
  }
};
