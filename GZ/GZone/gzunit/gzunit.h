// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#pragma warning(disable:4786)

#ifndef __GZUNIT_H__
#define __GZUNIT_H__

#include <string>
#include <list>
#include <vector>
#include <set>
#include <memory>
#include <queue>

#include "../../include/mfc.h"
#include <Altova/AltovaLib.h>
#include "../../interproc_clientaddin/outofprocmaster.h"
#include "../../interproc_clientaddin/outofprocmasterevents.h"
#include "../../interproc_clientaddin/eventaggregate.h"
#include <iniext/unitlog.h>
#include "EventThread.h"

class CSoldierListView;

class CGZUnit:public COutOfProcMaster,public EventThread
{
public:
  struct LInfo
  {
    std::string login,password,charname,app;
  };

protected:
  class EventFinally
  {
    HANDLE hh;
  public:
    EventFinally(HANDLE h):hh(h)
    {
    }
    ~EventFinally()
    {
      SetEvent(hh);
    }
  };

protected:
  CInterProcMaster m_ipm;

private:
  CComAutoCriticalSection m_cs;
  LInfo m_linfo;
  // состояние когда клиент зашел на сервер
  bool m_connectionestablished;
  // нужно перезапускать клиент в случае падения
  bool m_autorun;
  bool m_showwindow;
  HANDLE m_receivethread;
  DWORD m_threadid;
  std::string m_name;
  HANDLE m_defaultabort;

  static void ReceiveThread(CGZUnit *p);

protected:
  DWORD GetThreadId();
  void CloseOutproc();
  void Disconnect();

  virtual void Connected();
  virtual void LocalConnectionLost();
  virtual void ConnectionEstablished(const char* name,const char* pass, const char* charname);
  virtual void ConnectionLost();
  virtual void Exit(unsigned int exitcode);

  virtual void Connect(unsigned long clientprocessid = 0,const char*app=0);

public:
  LInfo GetLInfo();
  void SetLInfo(LInfo&l);
  virtual bool IsWindowVisible();

  const char* GetUnitId();

  void SetShowWindow(bool b);
  void Abort();
  
  void AddAdvise(CClientGameEvents*p);
  void RemoveAdvise(CClientGameEvents*p);
  void AddAdvise(CClientEvents*);
  void RemoveAdvise(CClientEvents*);

  /// клиент успешно соеденен с игровым шардом
  bool IsLoggedIn();
  /// успешное соеденение с локальным клиентом по MailSlot
  bool IsConnected();

  virtual void ShowWindow(bool b);
  virtual bool Login(const char*name = 0,const char*pass = 0, const char* charname = 0,const char* app=0);
  void Close();

  CGZUnit(const char* l = "",const char* p = "",const char *c = "",bool showwindow = false);
  virtual ~CGZUnit();
  COutOfProcMaster::MsgType CGZUnit::Receive(const unsigned char* buf,int bufsize);
};

typedef std::vector<CGZUnit*> UnitList_t;

#endif
