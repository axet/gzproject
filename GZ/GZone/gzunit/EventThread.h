#pragma once

#include <list>

#include "../../interproc_clientaddin/outofprocmasterpost.h"
#include "../../interproc_clientaddin/eventaggregate.h"

class CSoldierListView;
class COutOfProcMasterEvents;

class EventThread
{
  class EventParser:public COutOfProcMasterData
  {
  public:
  };

  typedef std::list<COutOfProcMasterPost*> SL;

  CComAutoCriticalSection m_cs;
  HANDLE m_eventadvise;
  SL m_soldier;
  DWORD m_threadid;
  HANDLE m_thread;
  COutOfProcMasterEvents* m_ipm;
  EventsAggregate<EventParser> m_ep;

  bool WaitThread();
  static void WaitEventThread(EventThread *p);

protected:
  void Create(COutOfProcMasterEvents*);
  void Close();
  DWORD GetThreadId();
  HANDLE GetEvent();

  void AddFakeAdvise(CClientGameEvents*p);
  void RemoveFakeAdvise(CClientGameEvents*p);
  void AddFakeAdvise(CClientEvents*);
  void RemoveFakeAdvise(CClientEvents*);

public:
  EventThread();
  ~EventThread();
  void AddSoldierAdvise(COutOfProcMasterPost*);
  void RemoveSoldierAdvise(COutOfProcMasterPost*);
  virtual void ReadEventSoldier();
};
