// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Все данные которые передаються между основным приложением и клиентом переходят через экземпляр класса
// CGZUnit. В его основе лежит поток который служит для мгновенного реагирования на прибытие данных со
// стороны игрового клиента. Это реализовано посредством создания потока который принадлежит непосредственно
// экземпляру данного класса.

// Класс CGZUnit Должен создаваться на каждого клиента который есть в списке запуска.

// Индикатор в списке солдат это наличие соединения, если клиент падает 
// то соединение должно закрываться и индикатор должен гаснуть.
// За состоянием индикатора должен следить этот класс. Установка и удаление соединения
// должно оповещать базывый класс об изменении состояния через:
//if(m_soldier!=0)
//  m_soldier->PostMessage(WM_USER+1);
// Состоянием подключения должно быть наличие потока, отсутсвие потока - должно означать
// что все Хендлы закрыты. Поэтому необходимо добавить четкости в работу фукнций
// Connect & Disconnect.
//
// Из документа GZone-InterProc.txt

// Места синхронизации:
// - Запуск потока
// - Завершения потока
// - Чтение данных из слота
// - Передача события в приложение

// Все указанные места синхронизации используються в этом модуле.

// Запуск потока не вызвает сложностей.

// Чтение данных из слота синхронизируються в двух местах :  

// bool COutOfProcMaster::GetData()
// {
//   SentrySc cs(m_cs);
//   ...
// }

// void COutOfProcMaster::Receive(const unsigned char* buf,int bufsize)
// {
//   SentrySc cs(m_cs);
//   ...
// }

// Передача события в приложени
//


#include "stdafx.h"
#include "gzunit.h"
#include "../gzone.h"
#include "../AutoCloseDialog.h"

#include <fluke/interprocexception.h>
#include "../../ClientAddin/clientfluke/clientfluke.h"

#include <ErrorReport/errorreport.h>
#include <debugger/debug.h>
#include <misc/SentrySc.h>

#include "../SoldierListView.h"

CGZUnit::CGZUnit(const char* l,const char* p,const char *c,bool showwindow):m_receivethread(INVALID_HANDLE_VALUE)
{
  SetIPM(&m_ipm);

  m_linfo.login=l;
  m_linfo.password=p;
  m_linfo.charname=c;
  m_showwindow=showwindow;
  m_connectionestablished=false;
  m_autorun=false;

  m_name=std::string(l)+"-"+c;
  m_defaultabort=GetAbortHandle();
}

// BEGIN CLIENT EVENTS

void CGZUnit::Connected()
{
}

void CGZUnit::LocalConnectionLost()
{
  {
    SentrySc sc(m_cs);
    m_connectionestablished=false;
  }
  m_ipm.Disconnect();

  COutOfProcMaster::LocalConnectionLost();
}

void CGZUnit::ConnectionEstablished(const char* name,const char* pass, const char* charname)
{
  m_connectionestablished=true;
  m_autorun=true;
}

void CGZUnit::ConnectionLost()
{
  {
    SentrySc sc(m_cs);
    m_connectionestablished=false;
  }
  m_ipm.Disconnect();

  DBGTRACE("Client: connection lost\n");
}

void CGZUnit::Exit(unsigned int exitcode)
{
  {
    SentrySc sc(m_cs);
    m_connectionestablished=false;
  }
  m_ipm.Disconnect();

  COutOfProcMaster::Exit(exitcode);

  DBGTRACE("Client: exitcode %d\n",exitcode);
}

// END CLIENT EVENTS


void CGZUnit::SetShowWindow(bool b)
{
  SentrySc sc(m_cs);
  m_showwindow=b;
}

bool CGZUnit::IsWindowVisible()
{
  SentrySc sc(m_cs);
  return m_showwindow;
}

CGZUnit::~CGZUnit()
{
  Close();
  Disconnect();
}

bool CGZUnit::Login(const char*name,const char*pass, const char* charname,const char*app)
{
  LInfo l;
  
  {
    SentrySc sc(m_cs);
    if(app==0)
      app=m_linfo.app.c_str();
    else
      m_linfo.app=app;

    if(name==0)
      name=m_linfo.login.c_str();
    else
      m_linfo.login=name;
    if(pass==0)
      pass=m_linfo.password.c_str();
    else
      m_linfo.password=pass;
    if(charname==0)
      charname=m_linfo.charname.c_str();
    else
      m_linfo.charname=charname;

    l=m_linfo;
    m_name=std::string(m_linfo.login)+"-"+m_linfo.charname;
  }

  Disconnect();

  Connect(0,l.app.c_str());

  PostThreadMessage(GetThreadId(),WM_USER+1,0,0);

  return true;
}

DWORD CGZUnit::GetThreadId()
{
  SentrySc sc(m_cs);
  return m_threadid;
}

void CGZUnit::Connect(unsigned long clientprocessid ,const char*app)
{
  DBGTRACE("Client: start\n");

  if(IsConnected())
  {
    Disconnect();
  }

  if(clientprocessid==0)
  {
#ifdef _DEBUG
    clientprocessid=ClientFluke::AttachClient();
    if(clientprocessid==0)
      clientprocessid=ClientFluke::CreateClient("clientaddin.dll",app);
#else
    clientprocessid=ClientFluke::CreateClient("clientaddin.dll",app);
#endif
  }
  
  m_ipm.Connect(clientprocessid);
  ResetEvent(GetAbortHandle());
  
  try
  {
    ica::CPacketMasterType packet;
    cac::CConfigType config;
    config.AddEmulation("");
    config.AddSecure("");
    config.AddClientPath("");
    packet.AddConfig(config);
    Send(packet);
  }catch(UnknownClient&e)
  {
    Disconnect();
    throw;
  }

  Connected();

  EventThread::Create(this);

  if(m_receivethread==INVALID_HANDLE_VALUE)
    m_receivethread=CreateThread(0,0,(LPTHREAD_START_ROUTINE)ReceiveThread,this,0,&m_threadid);
  if(m_receivethread==0)
    throw Fluke::interprocexception(ErrorReport("error create receive thread for master",GetLastErrorString().c_str()));

  while(!PostThreadMessage(m_threadid,WM_USER,0,0))
    Sleep(500);
}

void CGZUnit::Disconnect()
{
  SentrySc sc(m_cs);
  if(m_receivethread!=INVALID_HANDLE_VALUE)
  {
    Abort();
    SetEvent(m_defaultabort);
  }
  sc.Unlock();

  EventThread::Close();

  sc.Lock();
  m_threadid=0;
  if(m_receivethread!=INVALID_HANDLE_VALUE)
  {
    sc.Unlock();
    WaitForSingleObject(m_receivethread,-1);
    sc.Lock();
    CloseHandle(m_receivethread);
    m_receivethread=INVALID_HANDLE_VALUE;
    sc.Unlock();
  }
  m_ipm.Disconnect();
  ResetEvent(m_defaultabort);
}

CGZUnit::MsgType CGZUnit::Receive(const unsigned char* buf,int bufsize)
{
  return COutOfProcMaster::Receive(buf,bufsize);
}

bool CGZUnit::IsConnected()
{
  return m_ipm.IsConnected();
}

void CGZUnit::Close()
{
  m_autorun=false;
  PostThreadMessage(GetThreadId(),WM_USER+2,0,0);
}

bool CGZUnit::IsLoggedIn()
{
  SentrySc sc(m_cs);
  return m_connectionestablished;
}

CGZUnit::LInfo CGZUnit::GetLInfo()
{
  SentrySc sc(m_cs);
  return m_linfo;
}

void CGZUnit::SetLInfo(LInfo &l)
{
  SentrySc sc(m_cs);
  m_linfo=l;
}

void CGZUnit::ReceiveThread(CGZUnit *p)
{
  CoInitialize(0);

  MSG msg;

  while( p->GetThreadId()!=0 )
  {
    try
    {
      if(PeekMessage( &msg, NULL, 0, 0 ,PM_REMOVE))
      {
        switch(msg.message)
        {
        case WM_USER+1:
          {
            p->ShowWindow(p->IsWindowVisible());
            LInfo l=p->GetLInfo();
            p->COutOfProcMaster::Login(l.login.c_str(),l.password.c_str(),l.charname.c_str());

            break;
          }
        case WM_USER+2:
          {
            if(p->IsConnected())
            {
              p->CloseClient();
            }
            break;
          }
        case WM_USER+3:
          {
            p->COutOfProcMaster::ShowWindow(p->IsWindowVisible());
            break;
          }
        }
        TranslateMessage(&msg); 
        DispatchMessage(&msg);
      }

      p->Poll();
    }catch(CInterProc::IPMLocalConnectionLost &e)
    {
      DBGTRACE(ErrorReport("Poll exception",e.what()));

      bool b;
      LInfo l;

      {
        SentrySc sc(p->m_cs);
        l=p->m_linfo;
        b=p->m_autorun;
      }

      p->m_ipm.Disconnect();

      if(b)
      {
        p->Connect(0,l.app.c_str());
        {
          SentrySc sc(p->m_cs);
          p->m_showwindow=true;
        }
        PostThreadMessage(p->GetThreadId(),WM_USER+1,0,0);
      }else
      {
        SentrySc sc(p->m_cs);
        p->m_threadid=0;
      }
    }catch(ExecuteAbort &e)
    {
      DBGTRACE(ErrorReport("ExecuteAbort",e.what()));
    }catch(std::exception &e)
    {
      DBGTRACE(ErrorReport("Poll exception",e.what()));
      AfxMessageBox(ErrorReport("Poll exception",e.what()));
    }
  }

  CoUninitialize();
}

void CGZUnit::ShowWindow(bool b)
{
  SentrySc sc(m_cs);
  m_showwindow=b;
  PostThreadMessage(m_threadid,WM_USER+3,0,0);
}

void CGZUnit::AddAdvise(CClientGameEvents*p)
{
  AddFakeAdvise(p);
}

void CGZUnit::RemoveAdvise(CClientGameEvents*p)
{
  RemoveFakeAdvise(p);
}

void CGZUnit::AddAdvise(CClientEvents*p)
{
  AddFakeAdvise(p);
}

void CGZUnit::RemoveAdvise(CClientEvents*p)
{
  RemoveFakeAdvise(p);
}

void CGZUnit::Abort()
{
  SetEvent(m_defaultabort);
  SetEvent(COutOfProcMaster::GetCurrentAbort());
}

const char* CGZUnit::GetUnitId()
{
  return m_name.c_str();
}
