#include "StdAfx.h"
#include "EventThread.h"
#include "../SoldierListView.h"
#include <ErrorReport/errorreport.h>
#include <debugger/debug.h>
#include <misc/SentrySc.h>
#include "../GZone.h"
#include "../MainFrm.h"
#include "../../interproc_clientaddin/outofprocmasterpost.h"

EventThread::EventThread()
{
  m_eventadvise=INVALID_HANDLE_VALUE;
  m_threadid=0;
  m_thread=INVALID_HANDLE_VALUE;
  m_ipm=0;
}

void EventThread::Create(COutOfProcMasterEvents*p)
{
  SentrySc sc(m_cs);
  m_ipm=p;
  if(m_thread==INVALID_HANDLE_VALUE)
    m_thread=CreateThread(0,0,(LPTHREAD_START_ROUTINE)WaitEventThread,this,0,&m_threadid);
  if(m_thread==0)
    throw std::exception(ErrorReport("error create wait thread for master",GetLastErrorString().c_str()));
}

EventThread::~EventThread()
{
  Close();
}

DWORD EventThread::GetThreadId()
{
  SentrySc sc(m_cs);
  return m_threadid;
}

void EventThread::AddSoldierAdvise(COutOfProcMasterPost*p)
{
  SentrySc sc(m_cs);
  m_soldier.insert(m_soldier.end(),p);
}

void EventThread::RemoveSoldierAdvise(COutOfProcMasterPost*p)
{
  SentrySc sc(m_cs);
  m_soldier.remove(p);
}

HANDLE EventThread::GetEvent()
{
  SentrySc sc(m_cs);
  return m_eventadvise;
}

bool EventThread::WaitThread()
{
  if(GetThreadId()==0)
    return false;
  WaitForSingleObject(GetEvent(),-1);
  return GetThreadId()!=0;
}

void EventThread::WaitEventThread(EventThread *p)
{
  CoInitialize(0);

  try
  {
    SentrySc sc(p->m_cs);
    p->m_eventadvise=p->m_ipm->EventAdvise();
    sc.Unlock();

    while( p->WaitThread() )
    {
      sc.Lock();
      for(SL::iterator i=p->m_soldier.begin();i!=p->m_soldier.end();i++)
      {
        (*i)->ThreadGZUnitEvent(p);
      }
      sc.Unlock();
    }

    sc.Lock();
    p->m_ipm->EventRemove(p->m_eventadvise);
    p->m_eventadvise=INVALID_HANDLE_VALUE;
    sc.Unlock();

  }catch(std::exception &e)
  {
    DBGTRACE(ErrorReport("Event exception",e.what()));
    AfxMessageBox(ErrorReport("Event exception",e.what()));
  }

  DBGTRACE("Event Thread Stop");

  CoUninitialize();
}

void EventThread::ReadEventSoldier()
{
  try
  {
    COutOfProcMasterEvents *i;
    COutOfProcMasterData *p;
    {
      SentrySc sc(m_cs);
      i=m_ipm;
      p=&m_ep;
    }
    i->ReadEvent(p,GetEvent());
  }catch(COutOfProcMasterEvents::EventExpose&)
  {
  }
}

void EventThread::Close()
{
  if(m_thread!=INVALID_HANDLE_VALUE)
  {
    {
      SentrySc sc(m_cs);
      m_threadid=0;
      SetEvent(m_eventadvise);
    }
    WaitForSingleObject(m_thread,-1);
    CloseHandle(m_thread);
    m_thread=INVALID_HANDLE_VALUE;
  }
  if(m_eventadvise!=INVALID_HANDLE_VALUE)
  {
    m_ipm->EventRemove(m_eventadvise);
    m_eventadvise=INVALID_HANDLE_VALUE;
  }
}

void EventThread::AddFakeAdvise(CClientGameEvents*p)
{
  m_ep.AddAdvise(p);
}

void EventThread::RemoveFakeAdvise(CClientGameEvents*p)
{
  m_ep.RemoveAdvise(p);
}

void EventThread::AddFakeAdvise(CClientEvents*p)
{
  m_ep.AddAdvise(p);
}

void EventThread::RemoveFakeAdvise(CClientEvents*p)
{
  m_ep.RemoveAdvise(p);
}
