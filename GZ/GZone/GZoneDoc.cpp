// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// GZoneDoc.cpp : implementation of the CGZoneDoc class
//

#include "stdafx.h"
#include "GZone.h"

#include "GZoneDoc.h"
#include "commanderselectdlg.h"

#include "ciqharvester.h"
#include "ciqspider.h"
#include "ciqsecurityguard.h"
#include "ciqtrainer.h"
#include "CIQGoods.h"

#include "xsd/Schema/Schema.h"

#include "JavaExt/JavaLoader.h"

#include <iniext/iniext.h>

#include "gzunit/InterruptableUnit.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace gdf;

/////////////////////////////////////////////////////////////////////////////
// CGZoneDoc

IMPLEMENT_DYNCREATE(CGZoneDoc, CDocument)

BEGIN_MESSAGE_MAP(CGZoneDoc, CDocument)
	//{{AFX_MSG_MAP(CGZoneDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// поскольку MFC чудит с одним модулем ("так и задумано") необходимо передать в
// этот MFC класс активный документ
// Делать это нужно через глобальный параметр в момент переключения форм

CDocument *g_DocumentIQ;

/////////////////////////////////////////////////////////////////////////////
// CGZoneDoc construction/destruction

CGZoneDoc::CGZoneDoc():m_unit(0),m_commander(0)
{
  g_DocumentIQ=this;

  push_back(CCIQHarvester::Create());
  push_back(CCIQTrainer::Create());
  push_back(CCIQGoods::Create());

  LoadExtensions();
  LoadJava();
}

void CGZoneDoc::LoadJava()
{
  try
  {
    static const char env[]="Environment.jar";
    static JavaLoader jl((GetModuleRunDir()+env).c_str());

    {
      CFileFind find;
      BOOL work=find.FindFile((GetModuleRunDir()+"*.jar").c_str());
      while(work)
      {
        work=find.FindNextFile();
        if(find.GetFilePath().Right(sizeof(env)-1)==env)
          continue;
        CCommanderIQ* cq=jl.LoadJar(find.GetFilePath());
        push_back(cq);
      };
    }

    {
      CFileFind find;
      BOOL work=find.FindFile((GetModuleRunDir()+"*.jini").c_str());
      while(work)
      {
        work=find.FindNextFile();
        CFile f(find.GetFilePath(),CFile::modeRead);
        CArchive ar(&f,CArchive::load);
        CString ss;
        ar.ReadString(ss);
        CCommanderIQ* cq=jl.LoadJar(ss);
        push_back(cq);
      };
    }

  }catch(JavaLoader::JavaLoadExceptionNoLibFound &)
  {
  }
  catch(JavaLoader::JavaLoadException&)
  {
  }
}

void CGZoneDoc::LoadExtensions()
{
  {
    CFileFind find;
    BOOL work=find.FindFile((GetModuleRunDir()+"*.ciq").c_str());
    while(work)
    {
      work=find.FindNextFile();
      HMODULE h=LoadLibrary(find.GetFilePath());
      typedef CCommanderIQ* (*_InitializeCommander)();
      _InitializeCommander InitializeCommander=(_InitializeCommander)GetProcAddress(h,"InitializeCommander");
      if(InitializeCommander!=0)
      {
        push_back(InitializeCommander());
      }
    };
  }

  {
    CFileFind find;
    BOOL work=find.FindFile((GetModuleRunDir()+"*.cini").c_str());
    while(work)
    {
      work=find.FindNextFile();
      CFile f(find.GetFilePath(),CFile::modeRead);
      CArchive ar(&f,CArchive::load);
      CString ss;
      ar.ReadString(ss);

      HMODULE h=LoadLibrary(ss);
      typedef CCommanderIQ* (*_InitializeCommander)();
      _InitializeCommander InitializeCommander=(_InitializeCommander)GetProcAddress(h,"InitializeCommander");
      if(InitializeCommander!=0)
      {
        push_back(InitializeCommander());
      }
    };
  }
}

CGZoneDoc::~CGZoneDoc()
{
  for(CCommanderIQList::iterator i=begin();i!=end();i++)
  {
    delete (*i);
  }
}

BOOL CGZoneDoc::OnNewDocument()
{
  if(!CDocument::OnNewDocument())
	return FALSE;

  CCommanderSelectDlg dlg;
  dlg.m_ciqlist=GetCmdIQList();

  if(dlg.DoModal()!=IDOK)
    return FALSE;

  m_commander=(CCommanderIQ*)dlg.m_itemdata;
  //SetTitle(m_commander->FormGetName());

  return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CGZoneDoc serialization

void CGZoneDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
    CString &path=ar.GetFile()->GetFilePath();
		ar.GetFile()->Close();
	}
	else
	{
    //CString &path=ar.GetFile()->GetFilePath();
		//ar.Close();
    //m_commander->Serialize(path,false);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CGZoneDoc diagnostics

#ifdef _DEBUG
void CGZoneDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGZoneDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGZoneDoc commands

CCommanderIQList* CGZoneDoc::GetCmdIQList()
{
  return this;
}

void CGZoneDoc::OnCloseDocument() 
{
  //CChildFrame* p=(CChildFrame*)((CView*)m_viewList.GetHead())->GetParent()->GetParent()->GetParent();
  //p->m_map.RemoveAdvise(m_commander);
	
	CDocument::OnCloseDocument();
}

BOOL CGZoneDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
  DeleteFile(lpszPathName);
  std::string &s=m_commander->Save();

  CSchemaDoc doc;
  gdf::CDataFileType datafile;
  
  datafile.AddCommander(m_commander->FormGetName());

  MSXML2::IXMLDOMDocument2Ptr rdoc=doc.GetDocument();
  if(!rdoc->loadXML(s.c_str()))
  {
    MSXML2::IXMLDOMParseErrorPtr spError = rdoc->parseError;
    throw std::exception(ErrorReport("Failed to load " ,(LPCTSTR)spError->reason));
  }
  datafile.GetDOMNode()->appendChild(rdoc->documentElement);

  doc.SetRootElementName("http://gzproject.sourceforge.net/GZoneDataFile","DataFile");
  doc.Save(lpszPathName,datafile);

  SetModifiedFlag(FALSE);

  return TRUE;
}

void CGZoneDoc::LoadUnit(const char*p)
{
  for(UnitList_t::iterator i=App()->m_unitlist.begin();i!=App()->m_unitlist.end();i++)
  {
    std::string s=(*i)->GetUnitId();
    if(s==p)
    {
      if(!UnitToCommander((*i)))
        throw std::exception("unit rejected");
      return;
    }
  }
  throw std::exception(ErrorReport("no unit found",p));
}

BOOL CGZoneDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
  CSchemaDoc doc;
  CDataFileType datafile;
  datafile=doc.Load(lpszPathName);

  m_commander=GetCommander(((tstring)datafile.GetCommander()).c_str());
  if(m_commander==0)
    return FALSE;

  // MSXML BUG, may be wrong parsing xml-vellformed, tabular files
  // MSXML add surplus elements (text type)
  m_commander->Load(datafile.GetDOMNode()->firstChild->xml);

  const char* p;
  while((p=m_commander->UnitMissing())!=0)
  {
    LoadUnit(p);
  }

  return TRUE;
}

void CGZoneDoc::SetModified(bool b)
{
  CDocument::SetModifiedFlag(b);
}

bool CGZoneDoc::UnitToCommander(CGZUnit* p)
{
  return m_commander->UnitAdd(p);
}

bool CGZoneDoc::UnitFromCommander(CGZUnit* p)
{
  return m_commander->UnitRemove(p);
}
