// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



#include "stdafx.h"
#include "gzone.h"
#include "CIQHarvester.h"

#include <afxpriv.h>
#include <iniext/iniext.h>

#include "../client/clientcommands.h"
#include "xsd/Schema/Schema.h"
#include ".\ciqharvester.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CDocument *g_DocumentIQ;

using namespace hc;

/////////////////////////////////////////////////////////////////////////////
// CCIQHarvester

// CCIQHarvester {

//IMPLEMENT_DYNCREATE(CCIQHarvester, CFormView)

CCIQHarvester::CCIQHarvester()
	: CFormView(CCIQHarvester::IDD),m_hunit(0),m_unit(0),m_gamemap(0)
{
  m_pointtarget=m_pointlist.end();
  m_useitemser=0;
  m_backpackser=0;

	//{{AFX_DATA_INIT(CCIQHarvester)
	m_dropcontainerstatus = _T("");
	m_catchmapoint = FALSE;
	//}}AFX_DATA_INIT
}

CCIQHarvester::~CCIQHarvester()
{
  m_pDocument=0;
}

void CCIQHarvester::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK3, m_catchdropcontainer);
	DDX_Control(pDX, IDC_CHECK1, m_catchitemuse);
	DDX_Control(pDX, IDC_CHECK2, m_catchbackpack);
	DDX_Check(pDX, IDC_CHECK_CATCHMAPPOINT, m_catchmapoint);
    DDX_Check(pDX, IDC_CHECKRESS, m_catchres);
    DDX_Check(pDX, IDC_CHECKDROP, m_catchdrop);
    DDX_Check(pDX, IDC_CHECKPLAYERCORDS, m_catchplayercoords);
}


BEGIN_MESSAGE_MAP(CCIQHarvester, CFormView)
    ON_MESSAGE(WM_USER+1,OnUnitFinish) // finish target
    ON_MESSAGE(WM_USER+5,OnUnitStop) // set player coords
	ON_BN_CLICKED(IDC_BUTTON_START, OnButtonStart)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BUTTON_SHOWMAP, OnButtonShowmap)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
	ON_BN_CLICKED(IDC_CHECK_CATCHMAPPOINT, OnCheckCatchmappoint)
	ON_BN_CLICKED(IDC_BUTTON_DELETEALL, OnButtonDeleteall)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_RESURRECTIONPOINT, OnButtonResurrectionpoint)
	ON_BN_CLICKED(IDC_CHECK1, OnCheckWokItem)
	ON_BN_CLICKED(IDC_CHECK2, OnCheckBackPack)
	ON_BN_CLICKED(IDC_CHECK3, OnCheckDropItem)
    ON_BN_CLICKED(IDC_CHECKRESS, OnBnClickedCheckress)
    ON_BN_CLICKED(IDC_CHECKDROP, OnBnClickedCheckdrop)
    ON_BN_CLICKED(IDC_CHECKPLAYERCORDS, OnBnClickedCheckPlayerCords)
    ON_WM_ACTIVATE()
    ON_WM_MOUSEACTIVATE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIQHarvester diagnostics

#ifdef _DEBUG
void CCIQHarvester::AssertValid() const
{
	CFormView::AssertValid();
}

void CCIQHarvester::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCIQHarvester message handlers

HWND CCIQHarvester::FormCreate(HWND parent,CGameMap*p,CCommanderIQEvents*pp)
{
  m_events=pp;
  m_gamemap=p;
  m_gamemap->AddAdvise(this);
  if(m_pointlist.size()>0)
    m_gamemap->SetViewPos(m_pointlist.front().x,m_pointlist.front().y);
  CFormView::Create(0,0,WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),FromHandle(parent),0,0);
  SendMessage(WM_INITIALUPDATE);
  //m_pDocument=g_DocumentIQ;
  return m_hWnd;
}

HWND CCIQHarvester::FormGet()
{
  return m_hWnd;
}

const char* CCIQHarvester::FormGetName()
{
  static CString harvester;
  harvester.LoadString(IDS_STRING_HARVESTER);
  return harvester;
}

CCIQHarvester* CCIQHarvester::Create()
{
  //return (CCIQHarvester*)RUNTIME_CLASS(CCIQHarvester)->CreateObject();
  return new(CCIQHarvester);
}

void CCIQHarvester::UpdateDropPoint()
{
  if(m_dropcont.dropcontainerserial!=0)
  {
    if(m_catchdrop)
    {
      if(m_dropcont.dropcontainer.pt!=0)
        m_gamemap->DeletePoint(m_dropcont.dropcontainer.pt);
      CGameMap::point *p=m_gamemap->AddPoint(m_dropcont.dropcontainer.x,m_dropcont.dropcontainer.y);
      m_dropcont.dropcontainer.pt=p;
      m_gamemap->SetPointData(p,&m_dropcont.dropcontainer);
    }else
    {
      if(m_dropcont.dropcontainer.pt!=0)
      {
        m_gamemap->DeletePoint(m_dropcont.dropcontainer.pt);
        m_dropcont.dropcontainer.pt=0;
      }
    }
  }
}

void CCIQHarvester::UpdateResPoint()
{
  if(m_resurrectionpoint.x!=0&&m_resurrectionpoint.y!=0)
  {
    if(m_catchres)
    {
      if(m_resurrectionpoint.pt!=0)
        m_gamemap->DeletePoint(m_resurrectionpoint.pt);
      CGameMap::point *p=m_gamemap->AddPoint(m_resurrectionpoint.x,m_resurrectionpoint.y);
      m_resurrectionpoint.pt=p;
      m_gamemap->SetPointData(p,&m_resurrectionpoint);
    }else
    {
      if(m_resurrectionpoint.pt!=0)
      {
        m_gamemap->DeletePoint(m_resurrectionpoint.pt);
        m_resurrectionpoint.pt=0;
      }
   }
  }

}

void CCIQHarvester::OnInitialUpdate() 
{
  CFormView::OnInitialUpdate();
	

  ReloadLinePoints();

  UpdateDropPoint();
  UpdateResPoint();

  UpdateData(FALSE);

  CheckDropUpdate();
  CheckRessUpdate();

  UpdateUnit();

  ResizeParentToFit();
  GetParentFrame()->RecalcLayout();
}

std::string CCIQHarvester::Save()
{
  CDocAddin<CSchemaDoc> doc;

  CHarvesterConfigType harvestercofnig;
  if(m_unit!=0)
  {
    CUnitType unit;
    unit.AddBackpack(m_backpackser);
    unit.AddItem(m_useitemser);
    unit.AddUnitId(m_unitid.c_str());
    harvestercofnig.AddUnit(unit);
  }

  for(pointlist::iterator m=m_pointlist.begin();m!=m_pointlist.end();m++)
  {
    CGroundPointType gp;
    gp.AddItemType(m->itemid);
    /*Schema::*/CScalePoint pt;
    pt.AddX(m->x);
    pt.AddY(m->y);
    pt.AddZ(m->z);
    gp.AddUsePoint(pt);
    /*Schema::*/CScalePoint ppt;
    ppt.AddX(m->px);
    ppt.AddY(m->py);
    ppt.AddZ(m->pz);
    gp.AddPlayerPoint(ppt);
    harvestercofnig.AddGroundPoint(gp);
  }

  CHarvesterType harvestertype;
  harvestertype.AddCatchPlayerPoint(m_catchplayercoords);
  harvestertype.AddUseDropContainer(m_catchdrop);
  harvestertype.AddDropContainerSerial(m_dropcont.dropcontainerserial);
  {
    /*Schema::*/CScalePoint pt;
    pt.AddX(m_dropcont.dropcontainer.x);
    pt.AddY(m_dropcont.dropcontainer.y);
    pt.AddZ(m_dropcont.dropcontainer.z);
    harvestertype.AddDropContainerXY(pt);
  }
  {
    harvestertype.AddUseRessurectionPoint(m_catchres);
    /*Schema::*/CScalePoint pt;
    pt.AddX(m_resurrectionpoint.x);
    pt.AddY(m_resurrectionpoint.y);
    pt.AddZ(m_resurrectionpoint.z);
    harvestertype.AddRessurectionPoint(pt);
  }

  harvestercofnig.AddHarvester(harvestertype);

  doc.SetRootElementName("http://gzproject.sourceforge.net/HarvesterConfig","HarvesterConfig");
  return (tstring)doc.SaveXML(harvestercofnig);
}

void CCIQHarvester::Load(const char*p)
{
  CDocAddin<CSchemaDoc> doc;
  CHarvesterConfigType harvestercofnig=doc.LoadXML(p);

  if(harvestercofnig.HasUnit())
  {
    CUnitType unit=harvestercofnig.GetUnit();
    m_useitemser=unit.GetItem();
    m_backpackser=unit.GetBackpack();
    m_unitid=unit.GetUnitId();
  }

  for(int i=0;i<harvestercofnig.GetGroundPointCount();i++)
  {
    CGroundPointType gp=harvestercofnig.GetGroundPointAt(i);
    /*Schema::*/CScalePoint pt=gp.GetUsePoint();
    /*Schema::*/CScalePoint ppt=gp.GetPlayerPoint();
    m_pointlist.push_back(point(pt.GetX(),pt.GetY(),pt.GetZ(),ppt.GetX(),ppt.GetY(),ppt.GetZ(),gp.GetItemType()));
  }

  CHarvesterType harvester=harvestercofnig.GetHarvester();

  m_catchplayercoords=harvester.GetCatchPlayerPoint();
  m_dropcont.dropcontainerserial=harvester.GetDropContainerSerial();
  /*Schema::*/CScalePoint pt=harvester.GetDropContainerXY();
  m_catchdrop=harvester.GetUseDropContainer();
  m_dropcont.dropcontainer.x=pt.GetX();
  m_dropcont.dropcontainer.y=pt.GetY();
  m_dropcont.dropcontainer.z=pt.GetZ();
  pt=harvester.GetRessurectionPoint();
  m_catchres=harvester.GetUseRessurectionPoint();
  m_resurrectionpoint.x=pt.GetX();
  m_resurrectionpoint.y=pt.GetY();
  m_resurrectionpoint.z=pt.GetZ();
}

const char* CCIQHarvester::UnitMissing()
{
  if(m_unit==0)
    return m_unitid.c_str();
  return 0;
}

bool CCIQHarvester::UnitAdd(CUnit* p)
{
  if(m_unit!=0)
    return false;
//  if(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==1)
//    return false;

  m_unit=p;

  m_unit->AddAdvise(this);
  m_unitid=m_unit->GetUnitId();

  UpdateUnit();

  if(m_gamemap!=0)
  {
    WorldCord w=m_unit->GetUnitPos();
    if(w.x!=-1||w.y!=-1)
      m_gamemap->SetCursor(w.x,w.y);
  }

  return true;
}

void CCIQHarvester::SetUnit()
{
  if(m_hunit!=0)
  {
    m_hunit->m_useitemser=m_useitemser;
    m_hunit->m_backpackser=m_backpackser;
    m_hunit->m_catchplayercords=m_catchplayercoords;

    if(m_catchres)
      m_hunit->m_resurrectionpoint=CCIQHarvesterUnit::ZPoint(m_resurrectionpoint.x,m_resurrectionpoint.y,m_resurrectionpoint.z);
    else
      m_hunit->m_resurrectionpoint=CCIQHarvesterUnit::ZPoint();

    m_hunit->m_catchplayercords=m_catchplayercoords;

    if(m_catchdrop)
    {
      m_hunit->m_dropcontainer=m_dropcont.dropcontainer;
      m_hunit->m_dropcontainerserial=m_dropcont.dropcontainerserial;
    }else
    {
      m_hunit->m_dropcontainer=CCIQHarvesterUnit::ZPoint();
      m_hunit->m_dropcontainerserial=0;
    }
  }
}

void CCIQHarvester::Stop()
{
  if(m_hunit!=0)
  {
    m_hunit->PostThreadMessage(WM_QUIT,0,0);
    m_unit->Abort();
    WaitForSingleObject(m_hunit->m_hThread,-1);
    m_hunit=0;
  }
}

void CCIQHarvester::StopGUI()
{
  Stop();

  ((CButton*)GetDlgItem(IDC_BUTTON_START))->SetCheck(0);

  CString start,stop;
  start.LoadString(IDS_STRING_START);
  stop.LoadString(IDS_STRING_STOP);
  GetDlgItem(IDC_BUTTON_START)->SetWindowText(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==0?start:stop);

  SetCheck(IDC_BUTTON_START);
}

bool CCIQHarvester::UnitRemove(CUnit* p)
{
  if(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==1)
    return false;

  Stop();

  if(m_unit!=0)
  {
    m_unit->RemoveAdvise(this);
    m_unit=0;
  }

  m_events->SetModified();
  UpdateUnit();

  return true;
}

void CCIQHarvester::UpdateUnit()
{
  if(!IsWindow(*this))
    return;

  CString str;
  str.LoadString(IDS_STRING_CATCH);
  SetDlgItemText(IDC_EDIT_WORKITEM,m_useitemser?str:"");
  SetDlgItemText(IDC_EDIT_BACKPACK,m_backpackser?str:"");
  SetDlgItemText(IDC_EDIT_RESURRECTIONPOINT,m_resurrectionpoint.pt?str:"");
  SetDlgItemText(IDC_EDIT_DROPCONTAINER,m_dropcont.dropcontainerserial?str:"");
  
  UINT items[]=
  {
    IDC_BUTTON_START,IDC_CHECK1,IDC_CHECK2,IDC_CHECKDROP,IDC_CHECKRESS,IDC_CHECK3,IDC_BUTTON_RESURRECTIONPOINT,
      IDC_BUTTON_SHOWMAP,IDC_CHECK_CATCHMAPPOINT,IDC_BUTTON_DELETE,IDC_BUTTON_DELETEALL,IDC_CHECKPLAYERCORDS
  };
  for(int i=0;i<sizeof(items)/sizeof(UINT);i++)
    GetDlgItem(items[i])->EnableWindow(m_unit!=0);
}

bool CCIQHarvester::SetUnitItem(CCIQHarvesterUnit* p,unsigned u)
{
  if(m_catchitemuse.GetCheck()!=1)
    return false;

  m_events->SetModified();
  m_catchitemuse.SetCheck(0);
  return true;
}

bool CCIQHarvester::SetUnitBackpack(CCIQHarvesterUnit* p,unsigned u)
{
  if(m_catchbackpack.GetCheck()!=1)
    return false;

  m_events->SetModified();
  m_catchbackpack.SetCheck(0);

  return true;
}

void CCIQHarvester::PointSelect(CGameMap::point* p)
{
  CString str;
  if(p==m_resurrectionpoint.pt)
  {
    CString res;
    res.LoadString(IDS_STRING_RESSURECTPOINT);
    str.Format("%s \xd\xa%d.%d",res,m_dropcont.dropcontainer.x,m_dropcont.dropcontainer.y);
  }else if(p==m_dropcont.dropcontainer.pt)
  {
    CString drop;
    drop.LoadString(IDS_STRING_DROPPOINT);
    str.Format("%s\xd\xa%d.%d",drop,m_dropcont.dropcontainer.x,m_dropcont.dropcontainer.y);
  }else
  {
    point* pp=reinterpret_cast<point*>(m_gamemap->GetPointData(p));
    if(pp!=0)
    {
      CString path;
      path.LoadString(IDS_STRING_PATH);
      str.Format("%s\xd\xa%d.%d",path,pp->x,pp->y);
      if(m_catchplayercoords)
      {
        CString ppath;
        ppath.LoadString(IDS_STRING_PATHPLAYER);
        CString s;
        s.Format("\xd\xa%s\xd\xa%d.%d",ppath,pp->px,pp->py);
        str+=s;
      }
    }
  }

  GetDlgItem(IDC_EDIT_MAPSTATUS)->SetWindowText(str);
}

void CCIQHarvester::PointHiLight(CGameMap::point*)
{
  ;
}

void CCIQHarvester::AddPoint(int x,int y,int z,int px,int py,int pz,short itemid)
{
  if(!m_catchmapoint)
    return;

  for(pointlist::iterator m=m_pointlist.begin();m!=m_pointlist.end();m++)
  {
    if(m->x==x&&m->y==y)
      return;
  }

  pointlist::iterator i=m_pointlist.insert(m_pointlist.end(),point(x,y,z,px,py,pz,itemid));
  if(m_catchplayercoords)
  {
    i->ppt=m_gamemap->AddPoint(px,py,m_playerlistline);
    m_gamemap->SetPointData(i->ppt,&*i);
  }
  i->pt=m_gamemap->AddPoint(x,y,m_pointlistline);
  m_gamemap->SetPointData(i->pt,&*i);
  m_pointtarget=i;
}

void CCIQHarvester::OnButtonStart() 
{
  try
  {
    CheckClient();
  }catch(std::exception&)
  {
    ((CButton*)GetDlgItem(IDC_BUTTON_START))->SetCheck(0);
    throw;
  }

  if(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==1)
  {
    m_hunit=(CCIQHarvesterUnit*)AfxBeginThread(RUNTIME_CLASS(CCIQHarvesterUnit));
    SetUnit();
    m_hunit->Create(m_hWnd,m_unit);
    UpdateUnit();
    m_hunit->PostThreadMessage(CCIQHarvesterUnit::msgStart,(unsigned)GetNextTarget(),0);

    CString start,stop;
    start.LoadString(IDS_STRING_START);
    stop.LoadString(IDS_STRING_STOP);
    GetDlgItem(IDC_BUTTON_START)->SetWindowText(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==0?start:stop);
    SetCheck(IDC_BUTTON_START);
  }else
  {
    BeginWaitCursor();

    StopGUI();
  }
}

int CCIQHarvester::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

int CCIQHarvester::UnitGetCount()
{
  return m_unit!=0;
}

CUnit* CCIQHarvester::UnitGet(int index)
{
  return m_unit;
}

void CCIQHarvester::OnButtonShowmap() 
{
  if(m_pointlist.empty())
    return;
	m_gamemap->SelectPoint(m_pointlist.begin()->pt);
}

CCIQHarvester::point* CCIQHarvester::GetNextTarget()
{
  if(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==0)
    return 0;

  if(m_pointtarget==m_pointlist.end())
    m_pointtarget=m_pointlist.begin();
  return &*m_pointtarget++;
}

void CCIQHarvester::SetDropContainer(CUnit*p,unsigned itemserial)
{
  if(m_catchdropcontainer.GetCheck()==1)
  {
    m_events->SetModified();

    int z;

    m_dropcont.dropcontainerserial=itemserial;
    WorldCord w=p->GetUnitPos();
    m_dropcont.dropcontainer.x=w.x;
    m_dropcont.dropcontainer.y=w.y;
    m_dropcont.dropcontainer.z=w.z;
    m_dropcont.dropcontainer.pt=m_gamemap->AddPoint(m_dropcont.dropcontainer.x,m_dropcont.dropcontainer.y);

    m_catchdropcontainer.SetCheck(0);
  }
}

bool CCIQHarvester::PointMoveBefore(CGameMap::point*p)
{
  for(pointlist::iterator i=m_pointlist.begin();i!=m_pointlist.end();i++)
  {
    if(*i==p)
    {
      return p==i->ppt;
    }
  }
  return false;
}

void CCIQHarvester::PointMoveAfter(CGameMap::point*p)
{
  int x,y;
  m_gamemap->Translate(p,&x,&y);
  point* pp=reinterpret_cast<point*>(m_gamemap->GetPointData(p));
  if(pp!=0)
  {
    if(p==pp->pt)
    {
      pp->x=x;
      pp->y=y;
    }else if(p==pp->ppt)
    {
      pp->px=x;
      pp->py=y;
    }
    m_events->SetModified();

    SetUnit();
    UpdateUnit();
  }
}

void CCIQHarvester::OnButtonDelete() 
{
  for(int i=0;i<m_gamemap->GetSelectedPointCount();i++)
  {
    point* pp=reinterpret_cast<point*>(m_gamemap->GetPointData(m_gamemap->GetSelectedPoint(i)));
    m_gamemap->DeletePoint(pp->pt);
    if(m_catchplayercoords)
    {
      m_gamemap->DeletePoint(pp->ppt);
    }
    if(pp->pt==m_dropcont.dropcontainer.pt)
    {
      m_dropcont.dropcontainerserial=0;
      m_events->SetModified();
      UpdateUnit();
    }else
    {
      if(m_pointtarget!=m_pointlist.end())
      {
        if(pp==&*m_pointtarget)
          m_pointtarget++;
      }
      m_pointlist.remove(*pp);
      m_events->SetModified();
    }
  }
}

void CCIQHarvester::OnCheckCatchmappoint() 
{
  SetCheck(IDC_CHECK_CATCHMAPPOINT);
}

void CCIQHarvester::OnButtonDeleteall() 
{
  if(AfxMessageBox(IDS_ARE_YOU_SURE,MB_YESNO)!=IDYES)
    return;

  m_events->SetModified();
  if(m_pointlistline!=0)
    m_gamemap->DeleteLine(m_pointlistline);
  m_pointlistline=m_gamemap->AddLine();
  if(m_playerlistline!=0)
    m_gamemap->DeleteLine(m_playerlistline);
  if(m_catchplayercoords)
    m_playerlistline=m_gamemap->AddLine();
  m_pointlist.erase(m_pointlist.begin(),m_pointlist.end());
  m_pointtarget=m_pointlist.end();
}

void CCIQHarvester::OnDestroy() 
{
  if(m_hunit!=0)
  {
    m_hunit->PostThreadMessage(WM_QUIT,0,0);
    m_unit->Abort();
    WaitForSingleObject(m_hunit->m_hThread,-1);
  }
  if(m_unit!=0)
  {
    m_unit->RemoveAdvise(this);
    m_unit=0;
  }
  m_gamemap->RemoveAdvise(this);

  CFormView::OnDestroy();
}

long CCIQHarvester::OnUnitFinish(unsigned, long)
{
  if(((CButton*)GetDlgItem(IDC_BUTTON_START))->GetCheck()==1)
  {
    m_hunit->PostThreadMessage(CCIQHarvesterUnit::msgStart,(unsigned)GetNextTarget(),0);
  }
  return 0;
}

long CCIQHarvester::OnUnitStop(unsigned, long)
{
  StopGUI();
  return 0;
}

void CCIQHarvester::OnButtonResurrectionpoint() 
{
  WorldCord w=m_unit->GetUnitPos();
  m_resurrectionpoint.x=w.x;
  m_resurrectionpoint.y=w.y;
  m_resurrectionpoint.z=w.z;

  m_resurrectionpoint.pt=m_gamemap->AddPoint(m_resurrectionpoint.x,m_resurrectionpoint.y,m_gamemap->DeletePoint(m_resurrectionpoint.pt));
  m_gamemap->SetPointData(m_resurrectionpoint.pt,&m_resurrectionpoint);

  m_events->SetModified(true);

  UpdateUnit();
}

void CCIQHarvester::SetCheck(unsigned id)
{
  unsigned ids[]={IDC_BUTTON_START,IDC_CHECK1,IDC_CHECK3,IDC_CHECK2,IDC_CHECK_CATCHMAPPOINT};
  for(int i=0;i<sizeof(ids)/sizeof(unsigned);i++)
  {
    if(ids[i]==id)
      continue;
    ((CButton*)GetDlgItem(ids[i]))->SetCheck(0);
  }
  //if(id)
  //  ((CButton*)GetDlgItem(id))->SetCheck(!((CButton*)GetDlgItem(id))->GetCheck());

  UpdateData();
}

void CCIQHarvester::OnCheckWokItem() 
{
  SetCheck(IDC_CHECK1);	
}

void CCIQHarvester::OnCheckBackPack() 
{
  SetCheck(IDC_CHECK2);
}

void CCIQHarvester::OnCheckDropItem() 
{
  SetCheck(IDC_CHECK3);
}

void CCIQHarvester::PostNcDestroy() 
{
	;
	//CFormView::PostNcDestroy();
}

void CCIQHarvester::CheckClient()
{
  if(m_unit==0)
    throw std::exception(LoadString(IDS_STRING_ADDCLIENT));
}

void CCIQHarvester::OnBnClickedCheckress()
{
  CheckClient();
  UpdateData();
  CheckRessUpdate();
  UpdateResPoint();
}

void CCIQHarvester::CheckDropUpdate()
{
  GetDlgItem(IDC_EDIT_DROPCONTAINER)->EnableWindow(m_catchdrop);
  GetDlgItem(IDC_CHECK3)->EnableWindow(m_catchdrop);
}

void CCIQHarvester::CheckRessUpdate()
{
  GetDlgItem(IDC_EDIT_RESURRECTIONPOINT)->EnableWindow(m_catchres);
  GetDlgItem(IDC_BUTTON_RESURRECTIONPOINT)->EnableWindow(m_catchres);
}

void CCIQHarvester::OnBnClickedCheckdrop()
{
  CheckClient();
  UpdateData();
  CheckDropUpdate();
  UpdateDropPoint();
}

void CCIQHarvester::OnBnClickedCheckPlayerCords()
{
  UpdateData();

  SetUnit();
  m_events->SetModified(true);

  ReloadLinePoints();
}

void CCIQHarvester::ReloadLinePoints()
{
  if(m_pointlistline!=0)
    m_gamemap->DeleteLine(m_pointlistline);
  m_pointlistline=m_gamemap->AddLine();
  if(m_playerlistline!=0)
    m_gamemap->DeleteLine(m_playerlistline);
  if(m_catchplayercoords)
    m_playerlistline=m_gamemap->AddLine();
  for(pointlist::iterator m=m_pointlist.begin();m!=m_pointlist.end();m++)
  {
    m->pt=m_gamemap->AddPoint(m->x,m->y,m_pointlistline);
    m_gamemap->SetPointData(m->pt,&*m);
  }
  if(m_catchplayercoords)
  {
    for(pointlist::iterator m=m_pointlist.begin();m!=m_pointlist.end();m++)
    {
      m->ppt=m_gamemap->AddPoint(m->px,m->py,m_playerlistline);
      m_gamemap->SetPointData(m->ppt,&*m);
    }
  }
}

void CCIQHarvester::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
  __super::OnActivate(nState, pWndOther, bMinimized);

  // TODO: Add your message handler code here
}

int CCIQHarvester::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
  return MA_NOACTIVATE;

  return __super::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

void CCIQHarvester::SelectedItem(unsigned itemserial)
{
  if(m_catchdropcontainer.GetCheck())
  {
    m_dropcont.dropcontainerserial=itemserial;
    WorldCord w=m_unit->GetUnitPos();
    m_dropcont.dropcontainer.x=w.x;
    m_dropcont.dropcontainer.y=w.y;
    m_dropcont.dropcontainer.z=w.z;
    m_catchdropcontainer.SetCheck(0);
    m_events->SetModified(true);
    UpdateDropPoint();
  }else if(m_catchitemuse.GetCheck())
  {
    m_useitemser=itemserial;
    m_catchitemuse.SetCheck(0);
    m_events->SetModified(true);
  }else if(m_catchbackpack.GetCheck())
  {
    m_backpackser=itemserial;
    m_catchbackpack.SetCheck(0);
    m_events->SetModified(true);
  }

  UpdateUnit();
}

void CCIQHarvester::SelectedGround(WorldCord w,const Client::ItemType& it)
{
  if(m_catchmapoint)
  {
    WorldCord ww=m_unit->GetUnitPos();
    AddPoint(w.x,w.y,w.z,ww.x,ww.y,ww.z,it);
    m_events->SetModified(true);
  }
}

void CCIQHarvester::SelectedPath(WorldCord w)
{
  m_gamemap->SetCursor(w.x,w.y);
}
