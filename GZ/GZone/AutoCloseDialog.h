// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#pragma once


// CAutoCloseDialog dialog

class CAutoCloseDialog : public CDialog
{
	DECLARE_DYNAMIC(CAutoCloseDialog)

  CString m_text;
  long m_timer;
  bool m_show;

public:
  CAutoCloseDialog(CWnd* pParent = NULL);   // standard constructor
  CAutoCloseDialog(const char*p,CWnd* pParent = NULL,bool cancel=false);
  virtual ~CAutoCloseDialog();
  void UpdateTimer();
  void KillTimer();
  void EnableCancel();

// Dialog Data
	enum { IDD = IDD_DIALOG_AUTOCLOSE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedCancel();
  afx_msg void OnBnClickedOk();
  afx_msg void OnClose();
  virtual BOOL OnInitDialog();
  afx_msg void OnTimer(UINT nIDEvent);
  afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnEnSetfocusStaticText();
};
