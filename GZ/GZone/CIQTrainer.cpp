// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// CIQTrainer.cpp : implementation file
//

#include "stdafx.h"
#include "gzone.h"
#include "CIQTrainer.h"
#include "gamemap.h"

#include <iniext/iniext.h>
#include <afxpriv.h>

#include "xsd/Schema/Schema.h"

extern CDocument *g_DocumentIQ;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace tc;

/////////////////////////////////////////////////////////////////////////////
// CCIQTrainer

IMPLEMENT_DYNCREATE(CCIQTrainer, CFormView)

CCIQTrainer::CCIQTrainer()
	: CFormView(CCIQTrainer::IDD),m_commands(0),m_gumpscount(0),m_unit(0)
{
  //{{AFX_DATA_INIT(CCIQTrainer)
	m_start = FALSE;
	m_goods = FALSE;
	m_useitem = FALSE;
	m_dropcontainer = FALSE;
	m_portion = 0;
	m_backpack = FALSE;
	m_gumpcatch = FALSE;
	//}}AFX_DATA_INIT
}

CCIQTrainer::~CCIQTrainer()
{
  if(m_commands!=0)
    m_commands->RemoveAdvise(this);
  // инчае он пытаеться отключить CView (this) от документа m_pDocument
  m_pDocument=0;
}

void CCIQTrainer::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCIQTrainer)
	DDX_Check(pDX, IDC_CHECK1, m_start);
	DDX_Check(pDX, IDC_CHECK2, m_goods);
	DDX_Check(pDX, IDC_CHECK4, m_useitem);
	DDX_Check(pDX, IDC_CHECK5, m_dropcontainer);
	DDX_Text(pDX, IDC_EDIT_PRTION, m_portion);
	DDX_Check(pDX, IDC_CHECK6, m_backpack);
	DDX_Check(pDX, IDC_CHECK7, m_gumpcatch);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCIQTrainer, CFormView)
  ON_MESSAGE(msgSetItem,OnSetItem)
  ON_MESSAGE(msgFinish,OnFinish)
  ON_MESSAGE(msgSetOption,OnSetOption)
	//{{AFX_MSG_MAP(CCIQTrainer)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK1, OnCheckStart)
	ON_BN_CLICKED(IDC_CHECK2, OnCheckGoods)
	ON_BN_CLICKED(IDC_CHECK4, OnCheckUseItem)
	ON_BN_CLICKED(IDC_CHECK5, OnCheckDropContainer)
	ON_BN_CLICKED(IDC_CHECK6, OnCheckBackpack)
	ON_BN_CLICKED(IDC_CHECK7, OnCheckGumpId)
	ON_EN_CHANGE(IDC_EDIT_PRTION, OnChangeEditPrtion)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCIQTrainer diagnostics

#ifdef _DEBUG
void CCIQTrainer::AssertValid() const
{
	CFormView::AssertValid();
}

void CCIQTrainer::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCIQTrainer message handlers

CCIQTrainer* CCIQTrainer::Create()
{
  return (CCIQTrainer*)RUNTIME_CLASS(CCIQTrainer)->CreateObject();
}

const char* CCIQTrainer::FormGetName()
{
  return "Trainer";
}

HWND CCIQTrainer::FormCreate(HWND parent,CGameMap*p,CCommanderIQEvents*pp)
{
  m_events=pp;
  m_gamemap=p;
  m_gamemap->AddAdvise(this);
  CFormView::Create(0,0,WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),FromHandle(parent),0,0);
  SendMessage(WM_INITIALUPDATE);
  //m_pDocument=g_DocumentIQ;
  return m_hWnd;
}

HWND CCIQTrainer::FormGet()
{
  return m_hWnd;
}

void CCIQTrainer::OnDestroy() 
{
  if(m_unit!=0)
  {
    m_unit->PostThreadMessage(WM_QUIT,0,0);
    WaitForSingleObject(m_unit->m_hThread,-1);
  }

  CFormView::OnDestroy();

  m_gamemap->RemoveAdvise(this);
}

void CCIQTrainer::OnCheckStart() 
{
  SetCheck(IDC_CHECK1);

  if(m_start)
  {
    m_unit->m_useitemser=m_useitemser;
    m_unit->m_goodsser=m_goodsser;
    m_unit->m_dropcontainerser=m_dropcontainerser;
	memcpy(m_unit->m_gumps,m_gumps,sizeof(m_gumps));
    m_unit->m_backpackser=m_backpackser;
	m_unit->m_portion=m_portion;
    m_unit->PostThreadMessage(CCIQTrainerUnit::msgStart,(DWORD)m_hWnd,0);
  }
}

void CCIQTrainer::OnCheckGoods() 
{
  SetCheck(IDC_CHECK2);
}

void CCIQTrainer::OnCheckUseItem() 
{
  SetCheck(IDC_CHECK4);
}

void CCIQTrainer::OnCheckDropContainer() 
{
  SetCheck(IDC_CHECK5);
}

bool CCIQTrainer::UnitAdd(CUnit* p)
{
  if(m_commands!=0)
    return false;

  m_commands=p;
  m_unit=(CCIQTrainerUnit*)AfxBeginThread(RUNTIME_CLASS(CCIQTrainerUnit));
  m_unit->m_client=p;
  m_commands->AddAdvise(this);
  return true;
}

bool CCIQTrainer::UnitRemove(CUnit* p)
{
  m_commands->RemoveAdvise(this);
  m_commands=0;
  return true;
}

const char* CCIQTrainer::UnitMissing()
{
  if(m_commands==0)
    return m_unitid.c_str();
  else
    return 0;
}

int CCIQTrainer::UnitGetCount()
{
  return m_commands!=0;
}

CUnit* CCIQTrainer::UnitGet(int index)
{
  return m_commands;
}

long CCIQTrainer::OnSetItem(unsigned itemserial,long)
{
  if(m_useitem)
  {
    m_useitemser=itemserial;
    m_useitem=FALSE;
    m_events->SetModified();
    UpdateUnit();
  }else if(m_goods)
  {
    m_goodsser=itemserial;
    m_goods=FALSE;
    m_events->SetModified();
    UpdateUnit();
  }else if(m_dropcontainer)
  {
    m_dropcontainerser=itemserial;
    m_dropcontainer=FALSE;
    m_events->SetModified();
    UpdateUnit();
  }else if(m_backpack)
  {
    m_backpackser=itemserial;
    m_backpack=FALSE;
    m_events->SetModified();
    UpdateUnit();
  }

  UpdateData(FALSE);
  return 0;
}

void CCIQTrainer::SelectedItem(unsigned itemserial)
{
  PostMessage(msgSetItem,itemserial);
}

void CCIQTrainer::SelectedGround(unsigned x,unsigned y,unsigned z,const Client::ItemType &it)
{
  ;
}

void CCIQTrainer::SelectedOption(short itemid)
{
  PostMessage(msgSetOption,itemid);
}

std::string CCIQTrainer::Save()
{
  UpdateData();

  CDocAddin<CSchemaDoc> doc;
  CTrainerConfigType trainertconfig;

  trainertconfig.AddUseItemSer(m_useitemser);
  trainertconfig.AddGoodsSer(m_goodsser);
  trainertconfig.AddDropContainerSer(m_dropcontainerser);
  trainertconfig.AddPortion(m_portion);
  trainertconfig.AddGump(m_gumps[0]);
  trainertconfig.AddGump(m_gumps[1]);
  trainertconfig.AddGump(m_gumps[2]);
  trainertconfig.AddGump(m_gumps[3]);
  trainertconfig.AddBackpack(m_backpackser);

  doc.SetRootElementName("","TrainerConfig");
  return ((tstring)doc.SaveXML(trainertconfig)).c_str();
}

void CCIQTrainer::Load(const char* p)
{
  CDocAddin<CSchemaDoc> doc;
  CTrainerConfigType trainertconfig=doc.LoadXML(p);

  m_useitemser=trainertconfig.GetUseItemSer();
  m_goodsser=trainertconfig.GetGoodsSer();
  m_dropcontainerser=trainertconfig.GetDropContainerSer();
  m_portion=trainertconfig.GetPortion();
  m_gumps[0]=trainertconfig.GetGumpAt(0);
  m_gumps[1]=trainertconfig.GetGumpAt(1);
  m_gumps[2]=trainertconfig.GetGumpAt(2);
  m_gumps[3]=trainertconfig.GetGumpAt(3);
  while(m_gumps[m_gumpscount])
    m_gumpscount++;
  m_backpackser=trainertconfig.GetBackpack();
}

void CCIQTrainer::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();

  UpdateUnit();

  UpdateData(FALSE);
}

void CCIQTrainer::UpdateUnit()
{
  SetDlgItemText(IDC_EDIT3,m_useitemser?"Catched!":"");
  SetDlgItemText(IDC_EDIT_GOODS,m_goodsser?"Catched!":"");
  SetDlgItemText(IDC_EDIT6,m_dropcontainerser?"Catched!":"");
  SetDlgItemText(IDC_EDIT2,m_backpackser?"Catched!":"");

  CString str;
  str.Format("%d gumps",m_gumpscount);
  SetDlgItemText(IDC_EDIT4,str);
}

void CCIQTrainer::OnCheckBackpack() 
{
  SetCheck(IDC_CHECK6);
}

long CCIQTrainer::OnFinish(unsigned,long)
{
  if(m_start)
    m_unit->PostThreadMessage(CCIQTrainerUnit::msgStart,(DWORD)m_hWnd,0);
  return 0;
}

long CCIQTrainer::OnSetOption(unsigned u,long)
{
  if(m_gumpcatch)
  {
    m_gumps[m_gumpscount++]=u;
    if(m_gumpscount>=4)
    {
      m_gumpcatch=FALSE;
    }
    m_events->SetModified();

    UpdateUnit();
  }
  UpdateData(FALSE);
  return 0;
}

void CCIQTrainer::SetCheck(unsigned id)
{
  unsigned ids[]={IDC_CHECK1,IDC_CHECK2,IDC_CHECK6,IDC_CHECK4,IDC_CHECK7,IDC_CHECK5};
  for(int i=0;i<sizeof(ids)/sizeof(unsigned);i++)
  {
    if(ids[i]==id)
      continue;
    ((CButton*)GetDlgItem(ids[i]))->SetCheck(0);
  }
  //if(id)
  //  ((CButton*)GetDlgItem(id))->SetCheck(!((CButton*)GetDlgItem(id))->GetCheck());

  UpdateData();
}

void CCIQTrainer::OnCheckGumpId() 
{
  SetCheck(IDC_CHECK7);

  if(m_gumpcatch)
  {
    m_gumpscount=0;
    memset(m_gumps,0,sizeof(m_gumps));
    UpdateUnit();
  }
}

void CCIQTrainer::OnChangeEditPrtion() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	SetCheck(0);
  m_events->SetModified();
}

void CCIQTrainer::PostNcDestroy() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	//CFormView::PostNcDestroy();
}
