// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if !defined(AFX_OPTIONSDLG_H__EF637F75_BA88_4038_986E_3DEEF74F07BF__INCLUDED_)
#define AFX_OPTIONSDLG_H__EF637F75_BA88_4038_986E_3DEEF74F07BF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptionsDlg.h : header file
//

#include <Altova/AltovaLib.h>
#include "xsd/Schema/Schema.h"

/////////////////////////////////////////////////////////////////////////////
// COptionsDlg dialog

struct OptionsDlg
{
  CString pathbgimage;
  CString clientpath;
  CString clientsecure;

  OptionsDlg()
  {
    CRegKey key(HKEY_LOCAL_MACHINE);
    key.Open(HKEY_LOCAL_MACHINE,"SOFTWARE\\Origin Worlds Online\\Ultima Online\\1.0");
    char buf[1024];
    ULONG bufl=sizeof(buf);
    key.QueryStringValue("ExePath",buf,&bufl);
    clientpath=buf;

    pathbgimage="MAP25166902-1.BMP";
  }

  void Load(gc::CConfigType &config)
  {
    pathbgimage=((tstring)config.GetBGImage()).c_str();
    clientpath=((tstring)config.GetClientPath()).c_str();
    clientsecure=((tstring)config.GetSecure()).c_str();
  }
  void Save(gc::CConfigType &config)
  {
    config.RemoveBGImage();
    config.AddBGImage((const char*)pathbgimage);
    config.AddClientPath((const char*)clientpath);
    config.AddSecure((const char*)clientsecure);
  }
};

class COptionsDlg : public CDialog,public OptionsDlg
{
// Construction
public:
	COptionsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COptionsDlg)
	enum { IDD = IDD_DIALOG_OPTIONS };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COptionsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COptionsDlg)
	afx_msg void OnButtonBrowse();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedButtonBrowse2();
  afx_msg void OnCbnEditchangeCombo1();
  afx_msg void OnCbnSelendokCombo1();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIONSDLG_H__EF637F75_BA88_4038_986E_3DEEF74F07BF__INCLUDED_)
