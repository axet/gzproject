// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__DCE3F632_1B28_41DF_B984_85253E05D40F__INCLUDED_)
#define AFX_MAINFRM_H__DCE3F632_1B28_41DF_B984_85253E05D40F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "soldierlistview.h"
#include "optionsdlg.h"
#include "../interproc_clientaddin/outofprocmasterpost.h"

extern OptionsDlg g_optionsdlg;

class CMainFrame : public CMDIFrameWnd,public COutOfProcMasterPost
{
  IPicturePtr m_worldmap;

  inline CGZoneApp* App() { return (CGZoneApp*)AfxGetApp(); }

  void LoadBGImage();

  DECLARE_DYNAMIC(CMainFrame)

public:

  CMainFrame();
  void ThreadGZUnitEvent(EventThread* p);
  IPicturePtr GetWorldMap();

// Attributes
public:

// Operations
public:
  CSoldierListView m_wndSoldierListView;
  LRESULT WindowProc(UINT message,WPARAM wparam,LPARAM lparam);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CReBar      m_wndReBar;
	CDialogBar      m_wndDlgBar;

// Generated message map functions
protected:
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnViewUnitslist();
  afx_msg void OnUpdateViewUnitslist(CCmdUI* pCmdUI);
  afx_msg void OnFileOptions();
  afx_msg void OnDestroy();
  afx_msg long OnThreadEvent(unsigned,long);
  DECLARE_MESSAGE_MAP()

public:
  afx_msg void OnUpdateTtttDfdaf(CCmdUI *pCmdUI);
  afx_msg void OnUpdateTtttTttt(CCmdUI *pCmdUI);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__DCE3F632_1B28_41DF_B984_85253E05D40F__INCLUDED_)
