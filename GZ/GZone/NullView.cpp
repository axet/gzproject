// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// NullView.cpp : implementation file
//

#include "stdafx.h"
#include "GZone.h"
#include "NullView.h"
#include "commanderiq.h"
#include "childfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNullView

IMPLEMENT_DYNCREATE(CNullView, CView)

CNullView::CNullView()
{
}

CNullView::~CNullView()
{
}


BEGIN_MESSAGE_MAP(CNullView, CView)
	//{{AFX_MSG_MAP(CNullView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNullView drawing

void CNullView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CNullView diagnostics

#ifdef _DEBUG
void CNullView::AssertValid() const
{
	CView::AssertValid();
}

void CNullView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CNullView message handlers

void CNullView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
  if(lHint==0 && GetDlgItem(AFX_IDW_PANE_FIRST)==0)
  {
    CChildFrame* p=(CChildFrame*)GetParent()->GetParent();
    p->SwitchCommander();
  }
}

void CNullView::OnSize(UINT nType, int cx, int cy) 
{
	CView::OnSize(nType, cx, cy);
	CWnd*p=GetDlgItem(AFX_IDW_PANE_FIRST);
  if(p!=0)
  {
    p->MoveWindow(0,0,cx,cy);
  }
}

void CNullView::OnDestroy() 
{
	CView::OnDestroy();

  ;
}

int CNullView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	;
	return 0;
}
