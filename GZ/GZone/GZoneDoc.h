// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// GZoneDoc.h : interface of the CGZoneDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GZONEDOC_H__38E57EC3_BC6B_4DF7_A1D1_BBEB90278B36__INCLUDED_)
#define AFX_GZONEDOC_H__38E57EC3_BC6B_4DF7_A1D1_BBEB90278B36__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "commanderiq.h"
#include <vector>

class CCommanderIQList:public std::vector<CCommanderIQ*>
{
public:
  CCommanderIQ* GetCommander(const char* p)
  {
    for(CCommanderIQList::iterator i=begin();i!=end();i++)
    {
      if(stricmp((*i)->FormGetName(),p)==0)
        return *i;
    }
    return 0;
  }
};

class CGZoneDoc : public CDocument,private CCommanderIQList,public CCommanderIQEvents
{
  CGZoneApp* App() { return (CGZoneApp*)AfxGetApp(); }

  virtual void SetModified(bool b = true);

protected: // create from serialization only
	CGZoneDoc();
	DECLARE_DYNCREATE(CGZoneDoc)

// Attributes
public:
  CGZUnit* m_unit;
  CCommanderIQ* m_commander;

// Operations
public:
  CCommanderIQList* GetCmdIQList();
  void LoadExtensions();
  void LoadJava();

  bool UnitToCommander(CGZUnit* p);
  bool UnitFromCommander(CGZUnit* p);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGZoneDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void OnCloseDocument();
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGZoneDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

  void LoadUnit(const char*p);

// Generated message map functions
protected:
	//{{AFX_MSG(CGZoneDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GZONEDOC_H__38E57EC3_BC6B_4DF7_A1D1_BBEB90278B36__INCLUDED_)
