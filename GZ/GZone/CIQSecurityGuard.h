// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if !defined(AFX_CIQSECURITYGUARD_H__DDB6B0A2_4432_4D5A_91B1_A7FFB39B2E69__INCLUDED_)
#define AFX_CIQSECURITYGUARD_H__DDB6B0A2_4432_4D5A_91B1_A7FFB39B2E69__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CIQSecurityGuard.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCIQSecurityGuard form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "commanderiq.h"

class CCIQSecurityGuard : public CFormView,public CCommanderIQ
{
  HWND FormCreate(HWND parent,CGameMap*,CCommanderIQEvents*);
  const char* FormGetName();

protected:
	CCIQSecurityGuard();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCIQSecurityGuard)

// Form Data
public:
	//{{AFX_DATA(CCIQSecurityGuard)
	enum { IDD = IDD_DIALOG_CIQSECURITYGUARD };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:
  static CCIQSecurityGuard* Create();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCIQSecurityGuard)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CCIQSecurityGuard();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CCIQSecurityGuard)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIQSECURITYGUARD_H__DDB6B0A2_4432_4D5A_91B1_A7FFB39B2E69__INCLUDED_)
