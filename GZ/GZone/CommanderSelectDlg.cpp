// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// CommanderSelectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "gzone.h"
#include "CommanderSelectDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCommanderSelectDlg dialog


CCommanderSelectDlg::CCommanderSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCommanderSelectDlg::IDD, pParent),m_ciqlist(0)
{
	//{{AFX_DATA_INIT(CCommanderSelectDlg)
	//}}AFX_DATA_INIT
}


void CCommanderSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCommanderSelectDlg)
	DDX_Control(pDX, IDC_LIST1, m_list);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCommanderSelectDlg, CDialog)
	//{{AFX_MSG_MAP(CCommanderSelectDlg)
	ON_LBN_DBLCLK(IDC_LIST1, OnDblclkList1)
	ON_LBN_SELCHANGE(IDC_LIST1, OnSelchangeList1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCommanderSelectDlg message handlers

void CCommanderSelectDlg::OnOK() 
{
	m_itemdata=m_list.GetItemData(m_list.GetCurSel());
  if(m_itemdata==-1)
    return;
	
	CDialog::OnOK();
}

BOOL CCommanderSelectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

  if(m_ciqlist!=0)
  {
    for(int i=0;i<m_ciqlist->size();i++)
    {
      const char* p=(*m_ciqlist)[i]->FormGetName();
      int item=m_list.InsertString(m_list.GetCount(),p?p:"!bad form name!");
      m_list.SetItemData(item,(DWORD)(*m_ciqlist)[i]);
    }
  }
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCommanderSelectDlg::OnDblclkList1() 
{
  OnOK();
}

void CCommanderSelectDlg::OnSelchangeList1() 
{
	GetDlgItem(IDOK)->EnableWindow(m_list.GetCurSel()!=-1);
}
