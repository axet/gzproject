// GZProject, Ultima Online utils.
// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if !defined(AFX_CIQTRAINERUNIT_H__9E93D84C_5D91_4EBB_B5D8_DB483988E8E5__INCLUDED_)
#define AFX_CIQTRAINERUNIT_H__9E93D84C_5D91_4EBB_B5D8_DB483988E8E5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CIQTrainerUnit.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CCIQTrainerUnit thread

class CCIQTrainerUnit : public CWinThread
{
	DECLARE_DYNCREATE(CCIQTrainerUnit)
protected:
	CCIQTrainerUnit();           // protected constructor used by dynamic creation

// Attributes
public:
  enum msg{msgStart};

  CUnit* m_client;
  unsigned m_useitemser,m_goodsser,m_dropcontainerser,m_backpackser;
	short m_gumps[4];
	int		m_portion;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCIQTrainerUnit)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CCIQTrainerUnit();

  afx_msg void OnStart(WPARAM,LPARAM);
	// Generated message map functions
	//{{AFX_MSG(CCIQTrainerUnit)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIQTRAINERUNIT_H__9E93D84C_5D91_4EBB_B5D8_DB483988E8E5__INCLUDED_)
