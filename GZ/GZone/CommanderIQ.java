package GZone;

import client.*;

// �������� ����� �������� ������� �� ��������� ����������
//   ����� ��� ������ �������������, ��������� �� ������ ����������
//   ������ � ������.
// �������������� ����� ��������� �������� ������� �� �����.
//   ����� ��� ����������� �� ��� ��������, �����, �������� ��������
//   ����� ��������, ����� ������� ����.. ��� ��� ������� �� �����.

public interface CommanderIQ extends GameMap.GameMapEvents
{

  // ������� �� ����� ���������, ������� �� ����� �������� ��������� ����������

  public static interface CommanderIQEvents
  {
    //���������� ��������� ���������, ������������ ����� ����������
    // ��������� ���������, ���������� CCommanderIQ::Serialize("..",true);
    public abstract void SetModified(boolean b);
  };

  // ������ � ����������� ������������ (��������)

  // ������� �� ��������� ��������� ���, ��� ���� ���������
  // ����� ����������. ����� ���� ������������ ����� ���������
  // ����������
  abstract void FormCreate(java.awt.Component parent,GameMap g,CommanderIQEvents c) throws Exception;
  // ���������� ��� ����� ������, �� ��� ��� ����� ������������
  //   � ������� ������ ���������
  abstract String FormGetName() throws Exception;

  // ������ � �����������

  // ��������� ������������ � ���������� ���������
  abstract boolean UnitAdd(Unit u) throws Exception;
  // ������� ������������
  abstract boolean UnitRemove(Unit u) throws Exception;
  // ���������� ���������� ������������� �� �������� ������� ���������
  abstract int UnitGetCount() throws Exception;
  // ���������� ������������ �� ������
  abstract Unit UnitGet(int index) throws Exception;

  // ����� �������

  // ����������/������ �������� (����� ������������ ���� ����)
  abstract String Save() throws Exception;
  abstract void Load(String s) throws Exception;
};
