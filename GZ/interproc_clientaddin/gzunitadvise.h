#ifndef __GZUNITADVISE_H__
#define __GZUNITADVISE_H__

#include "../client/unit.h"

class CGZUnitAdvise:public CUnit
{
  COutOfProcMaster* m_p;
  COutOfProcMaster::uniteventslist_t m_uniteventslist;
  COutOfProcMaster::unitgameeventslist_t m_unitgameeventslist;

public:
  CGZUnitAdvise():m_p(0)
  {
  }

  class NoClientConnected:public std::exception
  {
  public:
    const char* what()const 
    {
      return "No Client Connected";
    }
  };

  void ClearAdvise()
  {
    m_p=0;
  }

  void CopyAdvise(COutOfProcMaster* p)
  {
    m_p=p;
    m_p->CopyAdvise(m_uniteventslist,m_unitgameeventslist);
  }

  // перемещение персонажа в указанную точку
  virtual void CharMoveTo(int x,int y)
  {
    if(m_p==0)
      throw NoClientConnected();
    m_p->CharMoveTo(x,y);
  };

  // персонаж произносит слова
  virtual void CharSpeech(const char* buf)
  {
    if(m_p==0)
      throw NoClientConnected();
    m_p->CharSpeech(buf);
  };

  // работа (использование) с предметами

  // постое использование предмета
  virtual std::string UseItem(unsigned itemserial)
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->UseItem(itemserial);
  };
  // использование предмета на другом предмете
  virtual std::string UseItem(unsigned itemserialtouse,unsigned itemserialonuse)
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->UseItem(itemserialtouse,itemserialonuse);
  };
  // использованине предмета на земле, для статических обьектов передаеться еще itemid
  // для того чтобы добывать руду в скале достаточно только координат
  virtual std::string UseItem(unsigned itemserialtouse, unsigned x, unsigned y, const Client::ItemType &it = 0)
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->UseItem(itemserialtouse,x,y,it);
  }
  // исползьует предмет на другом предмете (предметах) для появления гампа,
  // в котором последовательно выдберает itemid возвращет серийный нового предмета
  virtual unsigned UseItemGump(unsigned itemserialtouse,const UnsignedList &itemserialtoselect, const ShortList &itemslist)
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->UseItemGump(itemserialtouse,itemserialtoselect,itemslist);
  }

  // перемещение предметов

  // перемещает предметы определнного типа из указанного
  // контейнера в дрйгой контейнер
  // Move Items Gump to Gump
  virtual void MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType &it,unsigned containerserialto)
  {
    if(m_p==0)
      throw NoClientConnected();
    m_p->MoveItemsG2G(containerserialfrom,it,containerserialto);
  };
  // функция находит контейнер, в нем предмет определенного типа и количества
  // и перемещает этот предмет в другой
  virtual void MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType &it,int count, unsigned containerserialto)
  {
    if(m_p==0)
      throw NoClientConnected();
    m_p->MoveItemsG2G(containerserialfrom,it,count,containerserialto);
  }
  // перемещает предмет по серийному номеру в контейнер
  // возвращает номер новго предмета (когда мы перетаскиваем часть кучки он может
  // поменяться)
  virtual unsigned MoveItem(unsigned itemwhat,unsigned containerserialto, int count = -1)
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->MoveItem(itemwhat,containerserialto,count);
  };

  // поиск предметов

  // поиск любого предмета в контейнере
  virtual unsigned SelectItem(unsigned containerserialwhere)
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->SelectItem(containerserialwhere);
  }
  // поиск по типу
  virtual unsigned SelectItem(unsigned containerserialwhere,const Client::ItemType &it)
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->SelectItem(containerserialwhere,it);
  };
  // поиск по количеству
  virtual unsigned SelectItem(unsigned containerserialfrom,const Client::ItemType &it,int count)
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->SelectItem(containerserialfrom,it,count);
  };

  // работа со статсами

  // возвращает количество хитов
  virtual int StatsGetHits()
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->StatsGetHits();
  };

  virtual void GetUnitPos(int *x,int *y)
  {
    if(m_p==0)
      throw NoClientConnected();
    m_p->GetUnitPos(x,y);
  };

  virtual int StatsGetMana()
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->StatsGetMana();
  }
  virtual int StatsGetStam()
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->StatsGetStam();
  }
  // количество нагрузки
  virtual int StatsGetWeight()
  {
    if(m_p==0)
      throw NoClientConnected();
    return m_p->StatsGetWeight();
  }

  void AddAdvise(CClientGameEvents*p)
  {
    m_unitgameeventslist.insert(p);
    if(m_p!=0)
      m_p->AddAdvise(p);
  }

  void RemoveAdvise(CClientGameEvents*p)
  {
    m_unitgameeventslist.erase(p);
    if(m_p!=0)
      m_p->RemoveAdvise(p);
  }

  void AddAdvise(CClientEvents* p)
  {
    m_uniteventslist.push_back(p);
    if(m_p!=0)
      m_p->AddAdvise(p);
  }

  void RemoveAdvise(CClientEvents*p)
  {
    for(int i=0;i<m_uniteventslist.size();i++)
    {
      if(m_uniteventslist[i]==p)
      {
        m_uniteventslist.erase(m_uniteventslist.begin()+i);
        break;
      }
    }
    if(m_p!=0)
      m_p->RemoveAdvise(p);
  }
};


#endif