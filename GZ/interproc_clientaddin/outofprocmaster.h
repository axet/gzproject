#ifndef __OUTOFPROCMASTER_H__
#define __OUTOFPROCMASTER_H__

#pragma comment(lib,"wsock32.lib")

#include <string>
#include <vector>
#include <exception>
#include <set>
#include <map>
#include <algorithm>
#include <errorreport/errorreport.h>

#include "interprocclientaddin.h"
#include "../client/clientevents.h"
#include "../client/clientcommands.h"
#include "../client/clientgamecommands.h"
#include "../client/unit.h"
#include "../interproc/interprocmaster.h"
#include "outofprocmasterevents.h"

class COutOfProcMaster:public CInterProcClientAddin,public CUnit,public COutOfProcMasterEvents
{
public:
  class ExecuteException:public std::exception
  {
  public:
    ExecuteException(const char*p):exception(p) {};
  };

  /////////////////////////// SEND {

  // commands
  void Login(const char*name = 0,const char*pass = 0, const char* charname = 0);
  void CloseClient();
  void ShowWindow(bool b);
  bool IsWindowVisible();
  int StatsGetHits();
  int StatsGetWeight();
  int StatsGetMana();
  int StatsGetStam();

  unsigned SelectItem(unsigned containerserialwhere);
  unsigned SelectItem(unsigned containerserialwhere,const Client::ItemType &it);
  unsigned SelectItem(unsigned containerserialfrom,const Client::ItemType &it,int count);

  // game commands
  void CharMoveTo(WorldCord w);
  void CharMoveToChar(const char* name);
  void CharSpeech(const char* b);
  std::string UseItem(unsigned itemserial);
  std::string UseItem(unsigned itemserialtouse, unsigned itemserialon);
  std::string UseItem(unsigned itemserialtouse, WorldCord w,const Client::ItemType &it = 0);
  unsigned UseItemGump(unsigned itemserialtouse,const UnsignedList &itemserialtoselect, const ShortList &itemslist);
  void MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType &it,unsigned containerserialto);
  void MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType &it,int count, unsigned containerserialto);
  unsigned MoveItem(unsigned itemwhat,unsigned containerserialto, int count = -1);
  unsigned PinchItem(const Client::ItemType &it,unsigned containerserialfrom,int count);
  unsigned GetContainerItem(const Client::ItemType &it,unsigned containerserialfrom);
  unsigned GetEquippedItem(const Client::ItemType &it,const char* name);

  virtual void VendorOffer(const char* name,const ClientGameCommands::Vendors::Items& items);
  /////////////////////////// } SEND

  /////////////////////////// } EVENTS

  virtual void ConnectionLost();
  virtual void ConnectionEstablished(const char* name,const char* pass, const char* charname);
  virtual void Exit(unsigned exitcode);
  virtual void SelectedItem(unsigned itemserial);
  virtual void SelectedGround(WorldCord w,const Client::ItemType& it);
  virtual void SelectedPath(WorldCord w);
  virtual void SelectedOption(short itemid);

  /////////////////////////// } EVENTS

  COutOfProcMaster():m_x(-1),m_y(-1){};
  ~COutOfProcMaster(){}

  WorldCord GetUnitPos();

protected:
  //имя окна клиента
  std::string m_clientname;
  int m_x,m_y,m_z;

  Data Send(ica::CPacketMasterType &pmt);
};

#endif
