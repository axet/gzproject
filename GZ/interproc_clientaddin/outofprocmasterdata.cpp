#include "StdAfx.h"
#include "outofprocmasterdata.h"
#include <misc/SentrySc.h>

using namespace ica;

COutOfProcMasterData::MsgType COutOfProcMasterData::Receive(const unsigned char* buf,int bufsize)
{
  CDocAddin<CInterprocClientAddinDoc> doc;
  CPacketSlaveType packet=doc.LoadXML(std::string((const char*)buf));

  if(packet.HasException())
  {
    SentrySc sc(m_csdata);
    m_dataholder=std::auto_ptr<COutOfProcMasterData::DataHolder>(new COutOfProcMasterData::COutOfProcMasterException(((tstring)packet.GetException()).c_str()));
    return MsgRespond;
  }else if(packet.HasResult())
  {
    SentrySc sc(m_csdata);
    CSchemaBase64Binary bb=packet.GetResult();
    m_dataholder=std::auto_ptr<COutOfProcMasterData::DataHolder>(new COutOfProcMasterData::COutOfProcMasterRespond(bb.GetData(),bb.GetSize()));
    return MsgRespond;
  }else if(packet.HasSelectOption())
  {
    SelectedOption(CInterProcClientAddin::ItemTypeConvertor(packet.GetSelectOption().GetItemType()));
    return MsgEvent;
  }else if(packet.HasLogMessage())
  {
    LogMessage(((tstring)packet.GetLogMessage()).c_str());
    return MsgEvent;
  }else if (packet.HasExit())
  {
    Exit(packet.GetExit());
    return MsgEvent;
  }else if(packet.HasSelectItem())
  {
    SelectedItem(packet.GetSelectItem());
    return MsgEvent;
  }else if(packet.HasClientConnected())
  {
    CClientConnectedType &t=packet.GetClientConnected();
    ConnectionEstablished(((tstring)t.GetName()).c_str(),
      ((tstring)t.GetPassword()).c_str(),
      ((tstring)t.GetCharName()).c_str());
    return MsgEvent;
  }else if(packet.HasClientDisconnected())
  {
    ConnectionLost();
    return MsgEvent;
  }else if(packet.HasSelectGround())
  {
    CSelectGroundType &t=packet.GetSelectGround();
    WorldCord w=CInterProcClientAddin::WorldCordConvertor(t.GetWorldCord());
    SelectedGround(w,CInterProcClientAddin::ItemTypeConvertor(t.GetItemType()));
    return MsgEvent;
  }else if(packet.HasSelectPath())
  {
    CSelectPathType &t=packet.GetSelectPath();
    SelectedPath(CInterProcClientAddin::WorldCordConvertor(t.GetWorldCord()));
    return MsgEvent;
  }else if(packet.HasSetNewPlayersCoords())
  {
    CSetNewPlayersCoordsType &t=packet.GetSetNewPlayersCoords();
    SelectedPath(CInterProcClientAddin::WorldCordConvertor(t.GetWorldCord()));
    return MsgEvent;
  }else if(packet.HasUnknownClient())
  {
    throw UnknownClient();
  }
  throw std::exception("unknown packet");
}
