#ifndef __OUTOFPROCMASTERDATA_h__
#define __OUTOFPROCMASTERDATA_h__

#include "interprocclientaddin.h"
#include "../interproc/interprocmaster.h"
#include "../client/clientevents.h"

class COutOfProcMasterData:public CClientEvents
{
public:
  class UnknownClient:public std::exception
  {
  public:
    const char* what()const throw()
    {
      return "UnknownClient";
    }
  };

  class DataException:public std::exception
  {
    std::string str;
  public:
    DataException(const char*p):str(p){}
    const char* what()const throw()
    {
      return str.c_str();
    }
  };

  typedef std::vector<unsigned char> Data;

  enum MsgType
  {
    MsgEvent,
    MsgRespond,
    MsgNone
  };

  class DataHolder
  {
  public:
    operator const Data&()
    {
      if(!exception.empty())
        throw DataException(exception.c_str());
      return data;
    }
  protected:

    Data data;
    std::string exception;
  };

  class COutOfProcMasterRespond:public DataHolder
  {
  public:
    COutOfProcMasterRespond(unsigned char* buf,int bufsize)
    {
      data.assign(buf,buf+bufsize);
    }
  };

  class COutOfProcMasterException:public DataHolder
  {
  public:
    COutOfProcMasterException(const char* buf)
    {
      exception=buf;
    }
  };

  CComAutoCriticalSection m_csdata;

  std::auto_ptr<DataHolder> m_dataholder;

  MsgType Receive(const unsigned char* buf,int bufsize);
};

#endif
