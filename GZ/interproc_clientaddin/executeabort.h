#pragma once

class ExecuteAbort:public std::exception
{
  std::string str;
public:
  ExecuteAbort(const char*p="EA")
  {
    str=p;
  }
  const char* what()const throw()
  {
    return str.c_str();
  }
};
