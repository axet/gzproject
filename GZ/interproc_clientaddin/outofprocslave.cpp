#include "stdafx.h"
#include "outofprocslave.h"

#include <errorreport/errorreport.h>

using namespace ica;

/// размер буфера чтения, в случае принятия пакета большей длины происходит прерывание связи.
const static int g_bufsize=16384;

CPacketSlaveType PacketSlaveConvertor(const char* u)
{
  CPacketSlaveType packet;
  CSchemaBase64Binary bb((const unsigned char*)u,strlen(u)+1);
  packet.AddResult(bb);
  return packet;
}

CPacketSlaveType PacketSlaveConvertor(unsigned u)
{
  CPacketSlaveType packet;
  CSchemaBase64Binary bb("");
  bb.SetData((LPBYTE)&u,sizeof(u));
  packet.AddResult(bb);
  return packet;
}

CPacketSlaveType PacketSlaveConvertor()
{
  CPacketSlaveType packet;
  CSchemaBase64Binary bb("");
  packet.AddResult(bb);
  return packet;
}

COutOfProcSlave::COutOfProcSlave()
{
}

COutOfProcSlave::~COutOfProcSlave()
{
}

void COutOfProcSlave::Send(CPacketSlaveType &p)
{
  CDocAddin<CInterprocClientAddinDoc> doc;
  doc.SetRootElementName("","PacketSlave");
  std::string &str=doc.SaveXML(p);
  Write((const unsigned char*)str.c_str(),str.length()+1);
}

void COutOfProcSlave::Receive(const unsigned char* p,int psize)
{
  try
  {
    CDocAddin<CInterprocClientAddinDoc> doc;
    CPacketMasterType packet=doc.LoadXML(std::string((const char*)p));

    if(packet.HasClientIsWindowVisible())
    {
      Send(PacketSlaveConvertor(IsWindowVisible()));
    }else if(packet.HasSelectItemByS())
    {
      Send(PacketSlaveConvertor(SelectItem(packet.GetSelectItemByS())));
    }else if(packet.HasSelectItemBySIT())
    {
      CSelectItemBySITType s=packet.GetSelectItemBySIT();
      Send(PacketSlaveConvertor(SelectItem(s.GetSerial(),
        ItemTypeConvertor(s.GetItemType()))));
    }else if(packet.HasSelectItemBySITC())
    {
      CSelectItemBySITCType s=packet.GetSelectItemBySITC();
      Send(PacketSlaveConvertor(SelectItem(s.GetSerial(),
        ItemTypeConvertor(s.GetItemType()),s.GetCount())));
    }else if(packet.HasCharSpeech())
    {
      CharSpeech(((tstring)packet.GetCharSpeech()).c_str());
      Send(PacketSlaveConvertor());
    }else if(packet.HasMoveItem())
    {
      CMoveItemType &m=packet.GetMoveItem();
      Send(PacketSlaveConvertor(MoveItem(m.GetItemSerialToUse(),
        m.GetContainerSerialTo(),m.GetCount())));
    }else if(packet.HasPinchItem())
    {
      CPinchItemType &m=packet.GetPinchItem();
      Send(PacketSlaveConvertor(PinchItem(CInterProcClientAddin::ItemTypeConvertor(m.GetItemType()),
        m.GetContainerSerialFrom(),m.GetAmount())));
    }else if(packet.HasGetContainerItem())
    {
      CGetContainerItemType &m=packet.GetGetContainerItem();
      Send(PacketSlaveConvertor(GetContainerItem(CInterProcClientAddin::ItemTypeConvertor(m.GetItemType()),
        m.GetContainerSerialFrom())));
    }else if(packet.HasGetEquippedItem())
    {
      CGetEquippedItemType &m=packet.GetGetEquippedItem();
      Send(PacketSlaveConvertor(GetEquippedItem(CInterProcClientAddin::ItemTypeConvertor(m.GetItemType()),
        ((tstring)m.GetName()).c_str())));
    }else if(packet.HasMoveItemsG2G())
    {
      CMoveItemsG2GType &m=packet.GetMoveItemsG2G();
      MoveItemsG2G(m.GetContainerSerialFrom(),
        ItemTypeConvertor(m.GetItemType()),
        m.GetContainerSerialTo());
      Send(PacketSlaveConvertor());
    }else if(packet.HasCharMoveTo())
    {
      CCharMoveToType &m=packet.GetCharMoveTo();
      CharMoveTo(CInterProcClientAddin::WorldCordConvertor(m.GetWorldCord()));
      Send(PacketSlaveConvertor());
    }else if(packet.HasCharMoveToChar())
    {
      CCharMoveToCharType &m=packet.GetCharMoveToChar();
      CharMoveToChar(((tstring)m.GetName()).c_str());
      Send(PacketSlaveConvertor());
    }else if(packet.HasStatsGetWeight())
    {
      Send(PacketSlaveConvertor(StatsGetWeight()));
    }else if(packet.HasStatsGetHits())
    {
      Send(PacketSlaveConvertor(StatsGetHits()));
    }else if(packet.HasUseItemGump())
    {
      CUseItemGumpType &u=packet.GetUseItemGump();

      ShortList ii;
      for(int i=0;i<u.GetItemTypeIdCount();i++)
      {
        ii.push_back((short)u.GetItemTypeIdAt(i));
      }
      UnsignedList iii;
      for(int i=0;i<u.GetItemSerialToSelectCount();i++)
      {
        iii.push_back(u.GetItemSerialToSelectAt(i));
      }
      Send(PacketSlaveConvertor(
        UseItemGump(u.GetItemSerialToUse(),iii,
        ii)));
    }else if(packet.HasUseItem())
    {
      CUseItemType &u=packet.GetUseItem();
      Send(PacketSlaveConvertor(
        UseItem(u.GetItemSerialToUse()).c_str()));
    }else if(packet.HasUseItemOn())
    {
      CUseItemOnType &u=packet.GetUseItemOn();
      Send(PacketSlaveConvertor(
        UseItem(u.GetItemSerialToUse(),u.GetItemSerialOnUse()).c_str()));
    }else if(packet.HasUseItemGround())
    {
      CUseItemGroundType &u=packet.GetUseItemGround();
      CWorldCord wc=u.GetWorldCord();
      Send(PacketSlaveConvertor(
        UseItem(u.GetItemSerialToUse(),WorldCord(wc.GetX(),wc.GetY(),wc.GetZ()),
        ItemTypeConvertor(u.GetItemType())).c_str()));
    }else if(packet.HasClientLogin())
    {
      CClientLoginType &l=packet.GetClientLogin();
      Login(((tstring)l.GetName()).c_str(),
        ((tstring)l.GetPassword()).c_str(),
        ((tstring)l.GetCharName()).c_str());
      Send(PacketSlaveConvertor());
    }else if(packet.HasClientClose())
    {
      Send(PacketSlaveConvertor());
      CloseClient();
    }else if(packet.HasClientShowWindow())
    {
      ShowWindow(packet.GetClientShowWindow()==1);
      Send(PacketSlaveConvertor());
    }else if(packet.HasVendorOffer())
    {
      CVendorOfferType &v=packet.GetVendorOffer();
      ClientGameCommands::Vendors::Items items;
      for(int i=0;i<v.GetOfferItemCount();i++)
      {
        COfferItemType item=v.GetOfferItemAt(i);
        ClientGameCommands::Vendors::Item im;
        im.item=CInterProcClientAddin::ItemTypeConvertor(item.GetItemType());
        im.amount=(int)item.GetAmount();
        items.push_back(im);
      }
      VendorOffer(((tstring)v.GetVendorName()).c_str(),items);
      Send(PacketSlaveConvertor());
    }
  }
  catch(CClientUOCommands::SentryCommand::NotAnticipatedCommand&e)
  {
    CPacketSlaveType packet;
    packet.AddException(e.what());
    Send(packet);
  }
  catch(CClientUOCommands::SentryCommand::SentryCommandTimeout &e)
  {
    CPacketSlaveType packet;
    packet.AddException(CSchemaString(ErrorReport("Server packet time out")+e.what()));
    Send(packet);
  }
  catch(std::exception &e)
  {
    CPacketSlaveType packet;
    packet.AddException(e.what());
    Send(packet);
  }
  catch(_com_error &e)
  {
    CPacketSlaveType packet;
    packet.AddException((const char*)(e.Description()+"\n"+e.ErrorMessage()));
    Send(packet);
  }
}

void COutOfProcSlave::SelectedItem(unsigned itemserial)
{
  CPacketSlaveType packet;
  packet.AddSelectItem(itemserial);
  Send(packet);
}

void COutOfProcSlave::SelectedGround(WorldCord w,const Client::ItemType &it)
{
  CPacketSlaveType packet;
  CSelectGroundType t;
  t.AddWorldCord(CInterProcClientAddin::WorldCordConvertor(w));
  t.AddItemType(ItemTypeConvertor(it));
  packet.AddSelectGround(t);
  Send(packet);
}

void COutOfProcSlave::SelectedPath(WorldCord w)
{
  CPacketSlaveType packet;
  CSelectPathType t;
  t.AddWorldCord(CInterProcClientAddin::WorldCordConvertor(w));
  packet.AddSelectPath(t);
  Send(packet);
}

void COutOfProcSlave::SetNewPlayersCoords(WorldCord w)
{
  CPacketSlaveType packet;
  CSetNewPlayersCoordsType t;
  t.AddWorldCord(CInterProcClientAddin::WorldCordConvertor(w));
  packet.AddSetNewPlayersCoords(t);
  Send(packet);
}

void COutOfProcSlave::SelectedOption(const Client::ItemType& it)
{
  CPacketSlaveType packet;
  CSelectOptionType t;
  t.AddItemType(ItemTypeConvertor(it));
  packet.AddSelectOption(t);
  Send(packet);
}

void COutOfProcSlave::ConnectionLost()
{
  CPacketSlaveType packet;
  packet.AddClientDisconnected(CSchemaString(tstring()));
  Send(packet);
}

void COutOfProcSlave::ConnectionEstablished(const char* name,const char* pass, const char* charname)
{
  CPacketSlaveType packet;
  CClientConnectedType cc;
  cc.AddCharName(charname);
  cc.AddPassword(pass);
  cc.AddName(name);
  packet.AddClientConnected(cc);
  Send(packet);
}

void COutOfProcSlave::Exit(unsigned i)
{
  CPacketSlaveType packet;
  packet.AddExit(i);
  Send(packet);
}

void COutOfProcSlave::LogMessage(const char* msg)
{
  CPacketSlaveType packet;
  packet.AddLogMessage(msg);
  Send(packet);
}

void COutOfProcSlave::Poll()
{
  unsigned char buf[g_bufsize];
  int r=Read(buf,sizeof(buf));
  if(r>0)
    Receive(buf,sizeof(buf));
}
