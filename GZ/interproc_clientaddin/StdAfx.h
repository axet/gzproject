#include "../include/mfc.h"

#include <winsock2.h>
#include <atlbase.h>
#include <exception>
#include <sstream>
#include <algorithm>

#include <Altova/AltovaLib.h>

#include "../clientaddin/clientfluke/clientfluke.h"
#include "fluke/interprocexception.h"
#include "fluke/flukemaster.h"
#include "xsd/InterprocClientAddin/InterprocClientAddin.h"
#include "fluke/sync.h"
#include "../clientaddin/clientfluke/clientfluke.h"
#include "../clientaddin/clientsuck/clientuocommands.h"
#include "../clientaddin/clientsuck/clientuo.h"
#include "../clientaddin/clientfluke/clientfluke.h"
