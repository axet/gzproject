#ifndef __OUTOFPROCSLAVE_H
#define __OUTOFPROCSLAVE_H

#include "interprocclientaddin.h"
#include <string>
#include "../client/clientcommands.h"
#include "../client/clientevents.h"
#include "../client/packets.h"
#include "../interproc/interprocslave.h"

class COutOfProcSlave:
  public CInterProcSlave,
  public CInterProcClientAddin,
  public CClientEvents,
  public CClientCommands
{
protected:
  virtual void Receive(const unsigned char*p,int psize);

public:
  COutOfProcSlave();
  ~COutOfProcSlave();

  void Send(ica::CPacketSlaveType &);
  void Exit(unsigned exitcode);
  virtual void SelectedItem(unsigned itemserial);
  virtual void SelectedGround(WorldCord w,const Client::ItemType &it);
  virtual void SelectedPath(WorldCord w);
  virtual void SetNewPlayersCoords(WorldCord w);
  virtual void SelectedOption(const Client::ItemType& it);
  virtual void ConnectionLost();
  virtual void LogMessage(const char* msg);
  virtual void ConnectionEstablished(const char* name,const char* pass, const char* charname);

  void Poll();
};

#endif
