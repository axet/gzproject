#pragma once

#include <map>
#include <algorithm>

#include <misc/SentrySc.h>

#include "OutOfProcMasterdata.h"
#include "../InterProc/interprocmaster.h"
#include "executeabort.h"

#include <ErrorReport/errorreport.h>

/// размер буфера чтения
const int g_bufsize=16384;

class COutOfProcMasterEvents:public COutOfProcMasterData
{
public:
  /// Класс очереди событий.

  /// Содержит в себе пары евентов, один
  /// евент для потока ожидающего, второй евент для рабочего потока.
  /// Рабочий поток должен оповещать ожидающие потоки о наличии события и
  /// ждать пока все ожидающие потоки обработали
  /// событие которое пришло по маилслоту. Ожидающие потоки
  /// в свою очкередь должны ждать прихода события и информировать рабочий поток
  /// о том что событие обработоно.
  class EventHandleQueue:public std::map<HANDLE,HANDLE>
  {
    class WaitPrepare
    {
    public:
      void operator ( ) ( std::pair<const HANDLE,HANDLE> & elem ) const
      {
        ResetEvent(elem.second);
        SetEvent(elem.first);
      }
    };

    class DoResetEvents
    {
    public:
      void operator ( ) ( std::pair<const HANDLE,HANDLE> & elem ) const
      {
        ResetEvent(elem.second);
      }
    };

    class DoSetEvents
    {
    public:
      void operator ( ) ( std::pair<const HANDLE,HANDLE> & elem ) const
      {
        SetEvent(elem.first);
      }
    };

    class CloseHandles
    {
    public:
      void operator ( ) ( std::pair<const HANDLE,HANDLE> & elem ) const
      {
        CloseHandle(elem.first);
        CloseHandle(elem.second);
      }
    };

    void AddEvent(HANDLE h)
    {
      SentrySc sc(m_cs);
      HANDLE hh=::CreateEvent(NULL,TRUE,TRUE,NULL);
      if(hh==0)
        throw std::exception(GetLastErrorString().c_str());
      if(h==0)
        throw std::exception("bad handle");
      (*this)[h]=hh;
    };

    CComAutoCriticalSection m_cs;

  public:
    ~EventHandleQueue()
    {
      Close();
    }
    void ResetEvents()
    {
      SentrySc sc(m_cs);
      std::for_each(begin(),end(),DoResetEvents());
    }
    /// фукнция должна устанавливать все евенты для ожидающих потоков чтобы они
    /// начали обработку событий, и сбрасывать евенты для рабочего котока
    /// чтобы он ждал завершения события
    void WaitForMultipleObjects(HANDLE abortevent)
    {
      SentrySc sc(m_cs);
      std::for_each(begin(),end(),DoSetEvents());
      //std::for_each(begin(),end(),WaitPrepare());
      std::vector<HANDLE> h;
      h.push_back(abortevent);
      for(iterator i=begin();i!=end();i++)
        h.push_back(i->second);
      sc.Unlock();
      while(h.size()>1)
      {
        DWORD ret=::WaitForMultipleObjects(h.size(),&h.front(),FALSE,INFINITE);
        if(ret==WAIT_OBJECT_0+0)
          throw ExecuteAbort("Wait for other thread");
        h.erase(h.begin()+(ret-WAIT_OBJECT_0));
      }
    }
    void Close()
    {
      SentrySc sc(m_cs);
      std::for_each(begin(),end(),CloseHandles());
    }
    HANDLE CreateEvent()
    {
      // должен быть автоматическим евентом чтобы не сбрасывать при вызове EventParsed()
      // i погашеным потому что надо ждать первого включения от рабочего потока
      HANDLE h=::CreateEvent(NULL,FALSE,FALSE,NULL);
      AddEvent(h);
      return h;
    }
    void CloseEvent(HANDLE h)
    {
      SentrySc sc(m_cs);
      SetEvent((*this)[h]);
      CloseHandle((*this)[h]);
      erase(h);
      CloseHandle(h);
    }
    void EventParsed(HANDLE h)
    {
      SentrySc sc(m_cs);
      iterator i=find(h);
      if(i==end())
        throw EventExpose();
      SetEvent(i->second);
    }
  };

  class EventExpose:public std::exception
  {
  public:
    const char* what()const throw()
    {
      return "EventExpose";
    };
  };

  class WFC
  {
    COutOfProcMasterEvents* m_p;
    HANDLE m_event;
    // хендл прерывания на момент запуска команды
    HANDLE m_runaborthandle;

    void PollEvent(const unsigned char* buf,int bufsize)
    {
      m_p->m_buffer.assign(buf,buf+bufsize);
      m_p->m_equeue.WaitForMultipleObjects(m_runaborthandle);
      m_p->m_buffer.clear();
    }
    bool PollLock()
    {
      HANDLE h[]={m_runaborthandle,m_p->m_cscontrol,m_event};
      DWORD d=::WaitForMultipleObjects(sizeof(h)/sizeof(HANDLE),h,FALSE,INFINITE);
      switch(d)
      {
      case WAIT_OBJECT_0+0:
        throw ExecuteAbort("Work Queue abort");
      case WAIT_OBJECT_0+1:
        return false;
      case WAIT_OBJECT_0+2:
        return true;
      }
      throw std::exception("bad wait for object");
    }

  public:
    WFC(COutOfProcMasterEvents* p)
    {
      m_p=p;
      
      {
        SentrySc sc(m_p->m_csaborthandle);
        m_runaborthandle=m_p->m_currentaborthandle;
      }

      try
      {
        m_event=m_p->EventAdvise();

        while(PollLock())
        {
          m_p->ReadEvent(m_p,m_event);
        }

        KillEvent();

        SentrySc sc(m_p->m_csdata);
        m_p->m_dataholder.reset();
      }catch(ExecuteAbort&)
      {
        KillEvent();
        throw;
      }
    }
    void KillEvent()
    {
      if(m_event!=INVALID_HANDLE_VALUE)
      {
        m_p->EventRemove(m_event);
        m_event=INVALID_HANDLE_VALUE;
      }
    }
    MsgType Poll()
    {
      try
      {
        unsigned char buf[g_bufsize];
        int i=m_p->m_p->Read(buf,sizeof(buf));
        if(i>=sizeof(buf))
          throw std::exception("buffer overflow");
        if(i>0)
        {
          SentrySc sc(m_p->m_cs);
          m_p->m_equeue.ResetEvents();
          sc.Unlock();
          if(m_p->Receive(buf,i)==MsgEvent)
          {
            PollEvent(buf,i);
            return MsgEvent;
          }else
          {
            sc.Lock();
            if(m_p->m_abortcount>0)
            {
              m_p->m_abortcount--;
              m_p->m_buffer.clear();
              sc.Unlock();
              SentrySc ssc(m_p->m_csdata);
              m_p->m_dataholder.reset();
              return MsgNone;
            }
            return MsgRespond;
          }
        }else
        {
          if(WaitForSingleObject(m_runaborthandle,0)!=WAIT_TIMEOUT)
            throw ExecuteAbort("Receive abort");
        }
        return MsgNone;
      }catch(ExecuteAbort&)
      {
        {
          SentrySc sc(m_p->m_csaborthandle);
          m_p->m_abortcount++;
        }
        throw;
      }catch(CInterProc::IPMLocalConnectionLost&)
      {
        {
          SentrySc sc(m_p->m_cs);
          m_p->m_buffer.clear();
        }
        // prepare others
        m_p->m_equeue.ResetEvents();
        // parse event
        m_p->LocalConnectionLost();
        // wait others
        m_p->m_equeue.WaitForMultipleObjects(m_runaborthandle);
        throw;
      }
    }
    ~WFC()
    {
      KillEvent();
      SetEvent(m_p->m_cscontrol);
    } 
    const COutOfProcMasterData::Data& GetResult()
    {
      SentrySc sc(m_p->m_csdata);
      while(!m_p->m_dataholder.get()!=0)
      {
        sc.Unlock();
        Poll();
        sc.Lock();
      }
      return *m_p->m_dataholder;
    }
    void GetEvent()
    {
      if(Poll()!=MsgEvent)
        throw std::exception("wrong packet");
    }
  };

private:
  CInterProcMaster *m_p;
  CComAutoCriticalSection m_csaborthandle;
  HANDLE m_cscontrol,m_csevent;
  EventHandleQueue m_equeue;
  HANDLE m_currentaborthandle,m_defaultaborthandle;
  /// количество абортов произошеших в Pool() равно количеству MsgRespond которое надо проигнорировать
  int m_abortcount;
  CComAutoCriticalSection m_cs;
  std::vector<unsigned char> m_buffer;

protected:
  void SetIPM(CInterProcMaster *p);
  HANDLE GetAbortHandle();
  HANDLE GetCurrentAbort();

public:
  class AbortHandle
  {
    COutOfProcMasterEvents*p;
  public:
    AbortHandle(COutOfProcMasterEvents*q,HANDLE h)
    {
      p=q;
      // only for AbortHandleClass
      p->m_csaborthandle.Lock();
      p->m_currentaborthandle=h;
    }
    ~AbortHandle()
    {
      p->m_currentaborthandle=p->m_defaultaborthandle;
      p->m_csaborthandle.Unlock();
    }
  };

  COutOfProcMasterEvents();
  // проверят на события
  void Poll();
  // ждет события
  void ReadEvent();
  // обработка события, происходит для всех потоков в этом месте
  void ReadEvent(COutOfProcMasterData*,HANDLE h);
  // отправляет команду и ждет ответа\события
  COutOfProcMasterData::Data Send(const unsigned char* buf,int bufsize);
  HANDLE EventAdvise();
  void EventRemove(HANDLE h);
};
