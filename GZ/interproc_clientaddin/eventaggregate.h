#pragma once

#include <set>
#include <list>

template <class T> class EventsAggregate:public T
{
  CComAutoCriticalSection m_cs;

public:
  //CUnitEvents, CCommanderIQ
  typedef std::vector<CClientEvents*> uniteventslist_t;
  typedef std::set<CClientGameEvents*> unitgameeventslist_t;

  typedef std::list<CClientGameEvents*> gameeventslist_t;
  gameeventslist_t m_gameevents;
  typedef std::list<CClientEvents*> clienteventslist_t;
  clienteventslist_t m_clientevents;

  // BEGIN CLIENT GAME EVENTS
  void AddAdvise(CClientGameEvents*p)
  {
    SentrySc sc(m_cs);
    m_gameevents.insert(m_gameevents.end(),p);
  }
  void RemoveAdvise(CClientGameEvents*p)
  {
    SentrySc sc(m_cs);
    m_gameevents.remove(p);
  }
  void AddAdvise(CClientEvents*p)
  {
    SentrySc sc(m_cs);
    m_clientevents.insert(m_clientevents.end(),p);
  }
  void RemoveAdvise(CClientEvents*p)
  {
    SentrySc sc(m_cs);
    m_clientevents.remove(p);
  }
  void SelectedItem(unsigned itemserial)
  {
    SentrySc sc(m_cs);
    for(gameeventslist_t::const_iterator i=m_gameevents.begin();i!=m_gameevents.end();i++)
    {
      (*i)->SelectedItem(itemserial);
    }
  }
  void SelectedGround(WorldCord w,const Client::ItemType& it)
  {
    SentrySc sc(m_cs);
    for(gameeventslist_t::const_iterator i=m_gameevents.begin();i!=m_gameevents.end();i++)
    {
      (*i)->SelectedGround(w,it);
    }
  }
  void SelectedPath(WorldCord w)
  {
    SentrySc sc(m_cs);
    for(gameeventslist_t::const_iterator i=m_gameevents.begin();i!=m_gameevents.end();i++)
    {
      (*i)->SelectedPath(w);
    }
  }
  void SelectedOption(const Client::ItemType &item)
  {
    SentrySc sc(m_cs);
    for(gameeventslist_t::const_iterator i=m_gameevents.begin();i!=m_gameevents.end();i++)
    {
      (*i)->SelectedOption(item);
    }
  }
  // END CLIENT GAME EVENTS

  // BEGIN CLIENT EVENTS
  void Connected()
  {
    SentrySc sc(m_cs);
    for(clienteventslist_t::const_iterator i=m_clientevents.begin();i!=m_clientevents.end();i++)
    {
      (*i)->Connected();
    }
  }
  void LocalConnectionLost()
  {
    SentrySc sc(m_cs);
    for(clienteventslist_t::const_iterator i=m_clientevents.begin();i!=m_clientevents.end();i++)
    {
      (*i)->LocalConnectionLost();
    }
  }
  void LogMessage(const char* msg)
  {
    SentrySc sc(m_cs);
    for(clienteventslist_t::const_iterator i=m_clientevents.begin();i!=m_clientevents.end();i++)
    {
      (*i)->LogMessage(msg);
    }
  }
  void ConnectionEstablished(const char* name,const char* pass, const char* charname)
  {
    SentrySc sc(m_cs);
    for(clienteventslist_t::const_iterator i=m_clientevents.begin();i!=m_clientevents.end();i++)
    {
      (*i)->ConnectionEstablished(name,pass,charname);
    }
  }
  void ConnectionLost()
  {
    SentrySc sc(m_cs);
    for(clienteventslist_t::const_iterator i=m_clientevents.begin();i!=m_clientevents.end();i++)
    {
      (*i)->ConnectionLost();
    }
  }
  void Exit(unsigned int exitcode)
  {
    SentrySc sc(m_cs);
    for(clienteventslist_t::const_iterator i=m_clientevents.begin();i!=m_clientevents.end();i++)
    {
      (*i)->Exit(exitcode);
    }
  }
  // END CLIENT EVENTS
};
