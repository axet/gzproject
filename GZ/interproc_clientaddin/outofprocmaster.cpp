#include "stdafx.h"
#include "outofprocmaster.h"

#pragma comment(lib,"wsock32.lib")

using namespace ica;

static std::string SaveMaster(CPacketMasterType &packet)
{
  CDocAddin<CInterprocClientAddinDoc> doc;
  doc.SetRootElementName("","PacketMaster");
  return doc.SaveXML(packet);
}

void COutOfProcMaster::Login(const char*name,const char*pass, const char* charname)
{
  CPacketMasterType packet;
  CClientLoginType login;
  login.AddName(name);
  login.AddPassword(pass);
  login.AddCharName(charname);
  packet.AddClientLogin(login);
  Send(packet);
}
void COutOfProcMaster::CloseClient()
{
  CPacketMasterType packet;
  packet.AddClientClose(CSchemaString(""));
  Send(packet);
  //wfc.GetResult();
}
void COutOfProcMaster::ShowWindow(bool b)
{
  CPacketMasterType packet;
  packet.AddClientShowWindow(b);
  Send(packet);
}
bool COutOfProcMaster::IsWindowVisible()
{
  CPacketMasterType packet;
  packet.AddClientIsWindowVisible(CSchemaString(tstring()));
  return (*(BOOL*)&Send(packet).front())==1;
}
int COutOfProcMaster::StatsGetHits()
{
  CPacketMasterType packet;
  packet.AddStatsGetHits(CSchemaString(tstring()));
  return *(int*)&Send(packet).front();
}
int COutOfProcMaster::StatsGetWeight()
{
  CPacketMasterType packet;
  packet.AddStatsGetWeight(CSchemaString(tstring()));
  return *(int*)&Send(packet).front();
}
int COutOfProcMaster::StatsGetMana()
{
  CPacketMasterType packet;
  packet.AddStatsGetMana(CSchemaString(tstring()));
  return *(int*)&Send(packet).front();
}
int COutOfProcMaster::StatsGetStam()
{
  CPacketMasterType packet;
  packet.AddStatsGetStam(CSchemaString(tstring()));
  return *(int*)&Send(packet).front();
}

unsigned COutOfProcMaster::SelectItem(unsigned containerserialwhere)
{
  CPacketMasterType packet;
  packet.AddSelectItemByS(containerserialwhere);
  return *(unsigned*)&Send(packet).front();
}
unsigned COutOfProcMaster::SelectItem(unsigned containerserialwhere,const Client::ItemType &it)
{
  CPacketMasterType packet;
  CSelectItemBySITType ii;
  ii.AddSerial(containerserialwhere);
  ii.AddItemType(ItemTypeConvertor(it));
  packet.AddSelectItemBySIT(ii);
  return *(unsigned*)&Send(packet).front();
}
unsigned COutOfProcMaster::SelectItem(unsigned containerserialfrom,const Client::ItemType &it,int count)
{
  CPacketMasterType packet;
  CSelectItemBySITCType ii;
  ii.AddSerial(containerserialfrom);
  ii.AddItemType(ItemTypeConvertor(it));
  ii.AddCount(count);
  packet.AddSelectItemBySITC(ii);
  return *(unsigned*)&Send(packet).front();
}

// game commands
void COutOfProcMaster::CharMoveTo(WorldCord w)
{
  CPacketMasterType packet;
  CCharMoveToType charmoveto;
  charmoveto.AddWorldCord(WorldCordConvertor(w));
  packet.AddCharMoveTo(charmoveto);
  Send(packet);
}

void COutOfProcMaster::CharMoveToChar(const char* name)
{
  CPacketMasterType packet;
  CCharMoveToCharType charmoveto;
  charmoveto.AddName(name);
  packet.AddCharMoveToChar(charmoveto);
  Send(packet);
}

void COutOfProcMaster::CharSpeech(const char* b)
{
  CPacketMasterType packet;
  packet.AddCharSpeech(b);
  Send(packet);
}
std::string COutOfProcMaster::UseItem(unsigned itemserial)
{
  CPacketMasterType packet;
  CUseItemType m;
  m.AddItemSerialToUse(itemserial);
  packet.AddUseItem(m);
  return (const char*)&Send(packet).front();
}
std::string COutOfProcMaster::UseItem(unsigned itemserialtouse, unsigned itemserialon)
{
  CUseItemOnType m;
  m.AddItemSerialToUse(itemserialtouse);
  m.AddItemSerialOnUse(itemserialon);
  CPacketMasterType packet;
  packet.AddUseItemOn(m);
  return (const char*)&Send(packet).front();
}
std::string COutOfProcMaster::UseItem(unsigned itemserialtouse, WorldCord w,const Client::ItemType &it)
{
  CUseItemGroundType g;
  g.AddItemSerialToUse(itemserialtouse);
  g.AddWorldCord(WorldCordConvertor(w));
  g.AddItemType(ItemTypeConvertor(it));
  CPacketMasterType packet;
  packet.AddUseItemGround(g);
  return (const char*)&Send(packet).front();
}
unsigned COutOfProcMaster::UseItemGump(unsigned itemserialtouse,const UnsignedList &itemserialtoselect, const ShortList &itemslist)
{
  CUseItemGumpType g;

  for(UnsignedList::const_iterator i=itemserialtoselect.begin();i!=itemserialtoselect.end();i++)
  {
    g.AddItemSerialToSelect(*i);
  }

  g.AddItemSerialToUse(itemserialtouse);

  for(ShortList::const_iterator i=itemslist.begin();i!=itemslist.end();i++)
  {
    g.AddItemTypeId(*i);
  }
  CPacketMasterType packet;
  packet.AddUseItemGump(g);
  return *(int*)&Send(packet).front();
}
void COutOfProcMaster::MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType &it,unsigned containerserialto)
{
  CPacketMasterType packet;
  CMoveItemsG2GType m;
  m.AddContainerSerialFrom(containerserialfrom);
  m.AddContainerSerialTo(containerserialto);
  m.AddItemType(ItemTypeConvertor(it));
  packet.AddMoveItemsG2G(m);
  Send(packet);
}
void COutOfProcMaster::MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType &it,int count, unsigned containerserialto)
{
  CPacketMasterType packet;
  CMoveItemsG2GSelectType m;
  m.AddContainerSerialFrom(containerserialfrom);
  m.AddContainerSerialTo(containerserialto);
  m.AddCount(count);
  m.AddItemType(ItemTypeConvertor(it));
  packet.AddMoveItemsG2GSelect(m);
  Send(packet);
}
unsigned COutOfProcMaster::MoveItem(unsigned itemwhat,unsigned containerserialto, int count)
{
  CMoveItemType mt;
  mt.AddItemSerialToUse(itemwhat);
  mt.AddContainerSerialTo(containerserialto);
  mt.AddCount(count);
  CPacketMasterType packet;
  packet.AddMoveItem(mt);
  return *(int*)&Send(packet).front();
}

unsigned COutOfProcMaster::PinchItem(const Client::ItemType &it,unsigned containerserialfrom,int count)
{
  CPinchItemType mt;
  mt.AddContainerSerialFrom(containerserialfrom);
  mt.AddItemType(CInterProcClientAddin::ItemTypeConvertor(it));
  mt.AddAmount(count);
  CPacketMasterType packet;
  packet.AddPinchItem(mt);
  return *(int*)&Send(packet).front();
}

unsigned COutOfProcMaster::GetContainerItem(const Client::ItemType &it,unsigned containerserialfrom)
{
  CGetContainerItemType mt;
  mt.AddContainerSerialFrom(containerserialfrom);
  mt.AddItemType(CInterProcClientAddin::ItemTypeConvertor(it));
  CPacketMasterType packet;
  packet.AddGetContainerItem(mt);
  return *(int*)&Send(packet).front();
}

unsigned COutOfProcMaster::GetEquippedItem(const Client::ItemType &it,const char* name)
{
  CGetEquippedItemType mt;
  mt.AddName(name);
  mt.AddItemType(CInterProcClientAddin::ItemTypeConvertor(it));
  CPacketMasterType packet;
  packet.AddGetEquippedItem(mt);
  return *(int*)&Send(packet).front();
}

/////////////////////////// } SEND

/////////////////////////// } EVENTS

void COutOfProcMaster::ConnectionLost()
{
  CClientEvents::ConnectionLost();
}
void COutOfProcMaster::ConnectionEstablished(const char* name,const char* pass, const char* charname)
{
  CClientEvents::ConnectionEstablished(name,pass,charname);
  Connected();
}
void COutOfProcMaster::Exit(unsigned exitcode)
{
  CClientEvents::Exit(exitcode);
}
void COutOfProcMaster::SelectedItem(unsigned itemserial)
{
}
void COutOfProcMaster::SelectedGround(WorldCord w,const Client::ItemType& it)
{
}
void COutOfProcMaster::SelectedPath(WorldCord w)
{
  m_x=w.x;
  m_y=w.y;
  m_z=w.z;
}
void COutOfProcMaster::SelectedOption(short itemid)
{
}

WorldCord COutOfProcMaster::GetUnitPos()
{
  WorldCord w;
  w.x=m_x;
  w.y=m_y;
  w.z=m_z;
  return w;
}

COutOfProcMaster::Data COutOfProcMaster::Send(CPacketMasterType &pmt)
{
  std::string &str=SaveMaster(pmt);
  return COutOfProcMasterEvents::Send((const unsigned char*)str.c_str(),str.length()+1);
}

void COutOfProcMaster::VendorOffer(const char* name,const ClientGameCommands::Vendors::Items& items)
{
  CPacketMasterType packet;
  CVendorOfferType msg;
  msg.AddVendorName(name);
  for(ClientGameCommands::Vendors::Items::const_iterator i=items.begin();i!=items.end();i++)
  {
    COfferItemType item;
    item.AddItemType(CInterProcClientAddin::ItemTypeConvertor(i->item));
    item.AddAmount(i->amount);
    msg.AddOfferItem(item);
  }
  packet.AddVendorOffer(msg);
  Send(packet);
}
