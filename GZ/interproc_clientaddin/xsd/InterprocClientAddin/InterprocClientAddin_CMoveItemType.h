////////////////////////////////////////////////////////////////////////
//
// InterprocClientAddin_CMoveItemType.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef InterprocClientAddin_CMoveItemType_H_INCLUDED
#define InterprocClientAddin_CMoveItemType_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000




namespace ica // URI: http://gzproject.sourceforge.net/InterprocClientAddin
{


class InterprocClientAddin_DECLSPECIFIER CMoveItemType : public CNode
{
public:
	CMoveItemType() : CNode() {}
	CMoveItemType(CNode& rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
	CMoveItemType(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
	static EGroupType GetGroupType();

	//
	// int ItemSerialToUse (1...1)
	//
	static int GetItemSerialToUseMinCount();
	static int GetItemSerialToUseMaxCount();
	int GetItemSerialToUseCount();
	bool HasItemSerialToUse();
	void AddItemSerialToUse(CSchemaInt ItemSerialToUse);
	void InsertItemSerialToUseAt(CSchemaInt ItemSerialToUse, int nIndex);
	void ReplaceItemSerialToUseAt(CSchemaInt ItemSerialToUse, int nIndex);
	CSchemaInt GetItemSerialToUseAt(int nIndex);
	CSchemaInt GetItemSerialToUse();
	MSXML2::IXMLDOMNodePtr GetStartingItemSerialToUseCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedItemSerialToUseCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInt GetItemSerialToUseValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveItemSerialToUseAt(int nIndex);
	void RemoveItemSerialToUse();

	//
	// int ContainerSerialTo (1...1)
	//
	static int GetContainerSerialToMinCount();
	static int GetContainerSerialToMaxCount();
	int GetContainerSerialToCount();
	bool HasContainerSerialTo();
	void AddContainerSerialTo(CSchemaInt ContainerSerialTo);
	void InsertContainerSerialToAt(CSchemaInt ContainerSerialTo, int nIndex);
	void ReplaceContainerSerialToAt(CSchemaInt ContainerSerialTo, int nIndex);
	CSchemaInt GetContainerSerialToAt(int nIndex);
	CSchemaInt GetContainerSerialTo();
	MSXML2::IXMLDOMNodePtr GetStartingContainerSerialToCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedContainerSerialToCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInt GetContainerSerialToValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveContainerSerialToAt(int nIndex);
	void RemoveContainerSerialTo();

	//
	// int Count (1...1)
	//
	static int GetCountMinCount();
	static int GetCountMaxCount();
	int GetCountCount();
	bool HasCount();
	void AddCount(CSchemaInt Count);
	void InsertCountAt(CSchemaInt Count, int nIndex);
	void ReplaceCountAt(CSchemaInt Count, int nIndex);
	CSchemaInt GetCountAt(int nIndex);
	CSchemaInt GetCount();
	MSXML2::IXMLDOMNodePtr GetStartingCountCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedCountCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInt GetCountValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveCountAt(int nIndex);
	void RemoveCount();
};


} // end of namespace ica

#endif // InterprocClientAddin_CMoveItemType_H_INCLUDED
