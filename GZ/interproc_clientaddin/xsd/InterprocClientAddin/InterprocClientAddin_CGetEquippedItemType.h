////////////////////////////////////////////////////////////////////////
//
// InterprocClientAddin_CGetEquippedItemType.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef InterprocClientAddin_CGetEquippedItemType_H_INCLUDED
#define InterprocClientAddin_CGetEquippedItemType_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000




namespace ica // URI: http://gzproject.sourceforge.net/InterprocClientAddin
{


class InterprocClientAddin_DECLSPECIFIER CGetEquippedItemType : public CNode
{
public:
	CGetEquippedItemType() : CNode() {}
	CGetEquippedItemType(CNode& rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
	CGetEquippedItemType(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
	static EGroupType GetGroupType();

	//
	// string Name (1...1)
	//
	static int GetNameMinCount();
	static int GetNameMaxCount();
	int GetNameCount();
	bool HasName();
	void AddName(CSchemaString Name);
	void InsertNameAt(CSchemaString Name, int nIndex);
	void ReplaceNameAt(CSchemaString Name, int nIndex);
	CSchemaString GetNameAt(int nIndex);
	CSchemaString GetName();
	MSXML2::IXMLDOMNodePtr GetStartingNameCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedNameCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaString GetNameValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveNameAt(int nIndex);
	void RemoveName();

	//
	// ItemType ItemType (1...1)
	//
	static int GetItemTypeMinCount();
	static int GetItemTypeMaxCount();
	int GetItemTypeCount();
	bool HasItemType();
	void AddItemType(CItemType& ItemType);
	void InsertItemTypeAt(CItemType& ItemType, int nIndex);
	void ReplaceItemTypeAt(CItemType& ItemType, int nIndex);
	CItemType GetItemTypeAt(int nIndex);
	CItemType GetItemType();
	MSXML2::IXMLDOMNodePtr GetStartingItemTypeCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedItemTypeCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CItemType GetItemTypeValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveItemTypeAt(int nIndex);
	void RemoveItemType();
};


} // end of namespace ica

#endif // InterprocClientAddin_CGetEquippedItemType_H_INCLUDED
