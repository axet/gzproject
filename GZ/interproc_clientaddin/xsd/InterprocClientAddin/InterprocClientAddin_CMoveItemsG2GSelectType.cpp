////////////////////////////////////////////////////////////////////////
//
// InterprocClientAddin_CMoveItemsG2GSelectType.cpp
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "InterprocClientAddinBase.h"
#include "InterprocClientAddin_CMoveItemsG2GSelectType.h"
#include "InterprocClientAddin_CItemType.h"



namespace ica // URI: http://gzproject.sourceforge.net/InterprocClientAddin
{
////////////////////////////////////////////////////////////////////////
//
// class CMoveItemsG2GSelectType
//
////////////////////////////////////////////////////////////////////////


CNode::EGroupType CMoveItemsG2GSelectType::GetGroupType()
{
	return eSequence;
}
int CMoveItemsG2GSelectType::GetContainerSerialFromMinCount()
{
	return 1;
}


int CMoveItemsG2GSelectType::GetContainerSerialFromMaxCount()
{
	return 1;
}


int CMoveItemsG2GSelectType::GetContainerSerialFromCount()
{
	return ChildCountInternal(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialFrom"));
}


bool CMoveItemsG2GSelectType::HasContainerSerialFrom()
{
	return InternalHasChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialFrom"));
}


void CMoveItemsG2GSelectType::AddContainerSerialFrom(CSchemaInt ContainerSerialFrom)
{
	if( !ContainerSerialFrom.IsNull() )
		InternalAppend(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialFrom"), ContainerSerialFrom);
}


void CMoveItemsG2GSelectType::InsertContainerSerialFromAt(CSchemaInt ContainerSerialFrom, int nIndex)
{
	if( !ContainerSerialFrom.IsNull() )
		InternalInsertAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialFrom"), nIndex, ContainerSerialFrom);
}


void CMoveItemsG2GSelectType::ReplaceContainerSerialFromAt(CSchemaInt ContainerSerialFrom, int nIndex)
{
	InternalReplaceAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialFrom"), nIndex, ContainerSerialFrom);
}



CSchemaInt CMoveItemsG2GSelectType::GetContainerSerialFromAt(int nIndex)
{
	return (LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialFrom"), nIndex)->text;
	//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialFrom"), nIndex)->text);
}

MSXML2::IXMLDOMNodePtr CMoveItemsG2GSelectType::GetStartingContainerSerialFromCursor()
{
	return InternalGetFirstChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialFrom"));
}

MSXML2::IXMLDOMNodePtr CMoveItemsG2GSelectType::GetAdvancedContainerSerialFromCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialFrom"), pCurNode);
}

CSchemaInt CMoveItemsG2GSelectType::GetContainerSerialFromValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return (LPCTSTR)pCurNode->text;
}



CSchemaInt CMoveItemsG2GSelectType::GetContainerSerialFrom()
{
	return GetContainerSerialFromAt(0);
}


void CMoveItemsG2GSelectType::RemoveContainerSerialFromAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialFrom"), nIndex);
}


void CMoveItemsG2GSelectType::RemoveContainerSerialFrom()
{
	while (HasContainerSerialFrom())
		RemoveContainerSerialFromAt(0);
}

int CMoveItemsG2GSelectType::GetItemTypeMinCount()
{
	return 1;
}


int CMoveItemsG2GSelectType::GetItemTypeMaxCount()
{
	return 1;
}


int CMoveItemsG2GSelectType::GetItemTypeCount()
{
	return ChildCountInternal(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ItemType"));
}


bool CMoveItemsG2GSelectType::HasItemType()
{
	return InternalHasChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ItemType"));
}


void CMoveItemsG2GSelectType::AddItemType(CItemType& ItemType)
{
	InternalAppendNode(_T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ItemType"), ItemType);
}


void CMoveItemsG2GSelectType::InsertItemTypeAt(CItemType& ItemType, int nIndex)
{
	InternalInsertNodeAt(_T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ItemType"), nIndex, ItemType);
}


void CMoveItemsG2GSelectType::ReplaceItemTypeAt(CItemType& ItemType, int nIndex)
{
	InternalReplaceNodeAt(_T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ItemType"), nIndex, ItemType);
}



CItemType CMoveItemsG2GSelectType::GetItemTypeAt(int nIndex)
{
	return CItemType(*this, InternalGetAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ItemType"), nIndex));
}

MSXML2::IXMLDOMNodePtr CMoveItemsG2GSelectType::GetStartingItemTypeCursor()
{
	return InternalGetFirstChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ItemType"));
}

MSXML2::IXMLDOMNodePtr CMoveItemsG2GSelectType::GetAdvancedItemTypeCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ItemType"), pCurNode);
}

CItemType CMoveItemsG2GSelectType::GetItemTypeValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return CItemType( *this, pCurNode );
}



CItemType CMoveItemsG2GSelectType::GetItemType()
{
	return GetItemTypeAt(0);
}


void CMoveItemsG2GSelectType::RemoveItemTypeAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ItemType"), nIndex);
}


void CMoveItemsG2GSelectType::RemoveItemType()
{
	while (HasItemType())
		RemoveItemTypeAt(0);
}

int CMoveItemsG2GSelectType::GetCountMinCount()
{
	return 1;
}


int CMoveItemsG2GSelectType::GetCountMaxCount()
{
	return 1;
}


int CMoveItemsG2GSelectType::GetCountCount()
{
	return ChildCountInternal(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Count"));
}


bool CMoveItemsG2GSelectType::HasCount()
{
	return InternalHasChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Count"));
}


void CMoveItemsG2GSelectType::AddCount(CSchemaInt Count)
{
	if( !Count.IsNull() )
		InternalAppend(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Count"), Count);
}


void CMoveItemsG2GSelectType::InsertCountAt(CSchemaInt Count, int nIndex)
{
	if( !Count.IsNull() )
		InternalInsertAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Count"), nIndex, Count);
}


void CMoveItemsG2GSelectType::ReplaceCountAt(CSchemaInt Count, int nIndex)
{
	InternalReplaceAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Count"), nIndex, Count);
}



CSchemaInt CMoveItemsG2GSelectType::GetCountAt(int nIndex)
{
	return (LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Count"), nIndex)->text;
	//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Count"), nIndex)->text);
}

MSXML2::IXMLDOMNodePtr CMoveItemsG2GSelectType::GetStartingCountCursor()
{
	return InternalGetFirstChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Count"));
}

MSXML2::IXMLDOMNodePtr CMoveItemsG2GSelectType::GetAdvancedCountCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Count"), pCurNode);
}

CSchemaInt CMoveItemsG2GSelectType::GetCountValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return (LPCTSTR)pCurNode->text;
}



CSchemaInt CMoveItemsG2GSelectType::GetCount()
{
	return GetCountAt(0);
}


void CMoveItemsG2GSelectType::RemoveCountAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Count"), nIndex);
}


void CMoveItemsG2GSelectType::RemoveCount()
{
	while (HasCount())
		RemoveCountAt(0);
}

int CMoveItemsG2GSelectType::GetContainerSerialToMinCount()
{
	return 1;
}


int CMoveItemsG2GSelectType::GetContainerSerialToMaxCount()
{
	return 1;
}


int CMoveItemsG2GSelectType::GetContainerSerialToCount()
{
	return ChildCountInternal(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialTo"));
}


bool CMoveItemsG2GSelectType::HasContainerSerialTo()
{
	return InternalHasChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialTo"));
}


void CMoveItemsG2GSelectType::AddContainerSerialTo(CSchemaInt ContainerSerialTo)
{
	if( !ContainerSerialTo.IsNull() )
		InternalAppend(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialTo"), ContainerSerialTo);
}


void CMoveItemsG2GSelectType::InsertContainerSerialToAt(CSchemaInt ContainerSerialTo, int nIndex)
{
	if( !ContainerSerialTo.IsNull() )
		InternalInsertAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialTo"), nIndex, ContainerSerialTo);
}


void CMoveItemsG2GSelectType::ReplaceContainerSerialToAt(CSchemaInt ContainerSerialTo, int nIndex)
{
	InternalReplaceAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialTo"), nIndex, ContainerSerialTo);
}



CSchemaInt CMoveItemsG2GSelectType::GetContainerSerialToAt(int nIndex)
{
	return (LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialTo"), nIndex)->text;
	//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialTo"), nIndex)->text);
}

MSXML2::IXMLDOMNodePtr CMoveItemsG2GSelectType::GetStartingContainerSerialToCursor()
{
	return InternalGetFirstChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialTo"));
}

MSXML2::IXMLDOMNodePtr CMoveItemsG2GSelectType::GetAdvancedContainerSerialToCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialTo"), pCurNode);
}

CSchemaInt CMoveItemsG2GSelectType::GetContainerSerialToValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return (LPCTSTR)pCurNode->text;
}



CSchemaInt CMoveItemsG2GSelectType::GetContainerSerialTo()
{
	return GetContainerSerialToAt(0);
}


void CMoveItemsG2GSelectType::RemoveContainerSerialToAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ContainerSerialTo"), nIndex);
}


void CMoveItemsG2GSelectType::RemoveContainerSerialTo()
{
	while (HasContainerSerialTo())
		RemoveContainerSerialToAt(0);
}

} // end of namespace ica
