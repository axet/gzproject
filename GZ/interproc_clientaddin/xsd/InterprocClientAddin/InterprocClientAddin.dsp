# Microsoft Developer Studio Project File - Name="InterprocClientAddin" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104


CFG=InterprocClientAddin - Win32 Unicode Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "InterprocClientAddin.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "InterprocClientAddin.mak" CFG="InterprocClientAddin - Win32 Unicode Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "InterprocClientAddin - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "InterprocClientAddin - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "InterprocClientAddin - Win32 Unicode Release" (based on "Win32 (x86) Static Library")
!MESSAGE "InterprocClientAddin - Win32 Unicode Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "InterprocClientAddin - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /D "_LIB" /D "_AFXDLL" /MD /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /D "_LIB" /D "_AFXDLL" /MD /nologo /W3 /GR /GX /O2 /I "..\Altova" /I "..\AltovaXML" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c

# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo


!ELSEIF  "$(CFG)" == "InterprocClientAddin - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /D "_LIB" /D "_AFXDLL" /MDd /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /D "_LIB" /D "_AFXDLL" /MDd /nologo /W3 /Gm /GR /GX /ZI /Od /I "..\Altova" /I "..\AltovaXML" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c

# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo


!ELSEIF  "$(CFG)" == "InterprocClientAddin - Win32 Unicode Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "UnicodeRelease"
# PROP BASE Intermediate_Dir "UnicodeRelease"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "UnicodeRelease"
# PROP Intermediate_Dir "UnicodeRelease"
# PROP Target_Dir ""
# ADD BASE CPP /D "_LIB" /D "_AFXDLL" /MD /nologo /W3 /GR /GX /O2 /I "..\Altova" /I "..\AltovaXML" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /D "_LIB" /D "_AFXDLL" /MD /nologo /W3 /GR /GX /O2 /I "..\Altova" /I "..\AltovaXML" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_UNICODE" /D "UNICODE" /Yu"stdafx.h" /FD /c

# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo


!ELSEIF  "$(CFG)" == "InterprocClientAddin - Win32 Unicode Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "UnicodeDebug"
# PROP BASE Intermediate_Dir "UnicodeDebug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "UnicodeDebug"
# PROP Intermediate_Dir "UnicodeDebug"
# PROP Target_Dir ""
# ADD BASE CPP /D "_LIB" /D "_AFXDLL" /MDd /nologo /W3 /Gm /GR /GX /ZI /Od /I "..\Altova" /I "..\AltovaXML" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /D "_LIB" /D "_AFXDLL" /MDd /nologo /W3 /Gm /GR /GX /ZI /Od /I "..\Altova" /I "..\AltovaXML" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_UNICODE" /D "UNICODE" /FR /Yu"stdafx.h" /FD /GZ /c

# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo


!ENDIF 

# Begin Target

# Name "InterprocClientAddin - Win32 Release"
# Name "InterprocClientAddin - Win32 Debug"
# Name "InterprocClientAddin - Win32 Unicode Release"
# Name "InterprocClientAddin - Win32 Unicode Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\InterprocClientAddin.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CItemType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CWorldCord.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CEmulationType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSecureType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CConfigType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CCharMoveToType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CCharMoveToCharType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CUseItemType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CUseItemOnType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CUseItemGroundType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CUseItemGumpType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CMoveItemsG2GType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CPinchItemType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CGetContainerItemType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CGetEquippedItemType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CMoveItemType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CMoveItemsG2GSelectType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CClientLoginType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSelectItemBySITType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSelectItemBySITCType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_COfferItemType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CVendorOfferType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CPacketMasterType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSelectGroundType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSelectPathType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSelectOptionType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSetNewPlayersCoordsType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CClientConnectedType.cpp
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CPacketSlaveType.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\InterprocClientAddin.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddinBase.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CItemType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CWorldCord.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CEmulationType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSecureType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CConfigType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CCharMoveToType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CCharMoveToCharType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CUseItemType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CUseItemOnType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CUseItemGroundType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CUseItemGumpType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CMoveItemsG2GType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CPinchItemType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CGetContainerItemType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CGetEquippedItemType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CMoveItemType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CMoveItemsG2GSelectType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CClientLoginType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSelectItemBySITType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSelectItemBySITCType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_COfferItemType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CVendorOfferType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CPacketMasterType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSelectGroundType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSelectPathType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSelectOptionType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CSetNewPlayersCoordsType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CClientConnectedType.h
# End Source File
# Begin Source File

SOURCE=.\InterprocClientAddin_CPacketSlaveType.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# End Target
# End Project
