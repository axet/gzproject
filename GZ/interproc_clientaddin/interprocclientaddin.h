#ifndef __INTERPROCCLIENTADDIN_H
#define __INTERPROCCLIENTADDIN_H

#include "../client/itemtype.h"

#include "xsd/InterprocClientAddin/InterprocClientAddin.h"

#pragma comment(lib,"interproc_clientaddin.lib")

class CInterProcClientAddin
{
public:
  static Client::ItemType ItemTypeConvertor (ica::CItemType &itt)
  {
    Client::ItemType it;
    for(int i=0;i<itt.GetItemTypeIdCount();i++)
    {
      it.push_back((short)itt.GetItemTypeIdAt(i));
    }
    return it;
  }

  static ica::CItemType ItemTypeConvertor (const Client::ItemType &it)
  {
    ica::CItemType itt;
    if(!it.empty())
    {
      for(Client::ItemType::const_iterator i=it.begin();i!=it.end();i++)
      {
        itt.AddItemTypeId(*i);
      }
    }else
    {
      itt.AddItemTypeId((unsigned short)it);
    }
    return itt;
  }

  static ica::CWorldCord WorldCordConvertor(WorldCord w)
  {
    ica::CWorldCord wc;
    wc.AddX(w.x);
    wc.AddY(w.y);
    wc.AddZ(w.z);
    return wc;
  }

  static WorldCord WorldCordConvertor(ica::CWorldCord w)
  {
    WorldCord wc;
    wc.x=w.GetX();
    wc.y=w.GetY();
    wc.z=w.GetZ();
    return wc;
  }

  enum cmdItem{
    cmdNone,
    // команды управления cmd слейвом
    mstSetLog, // устанавливает записо событий лога bool*
    mstSetDebug,  //отладочная информацию в лог
    mstSetTrafic, // должна ли информация клиент-сервер передаваться в управляющее приложение
    // операции с клиентом
    mstUseItem,
    mstUseItemGump,
    mstCloseClient,
    msgSendToServer,
    mstLogin,mstGetLoginData,
    mstShowWindow,
    mstMoveTo,
    mstMoveItemG2G,mstMoveItemsG2G,mstMoveItem,
    mstCharSpeech,
    mstGetHits,mstGetWeight,
    // ответы слейва rcv
    slvLogMessage, slvFromClient, slvFromServer,slvExit,
    slvMstResult, // результат выполнения команды
    slvSelectItem,
    slvSelectGround, // выбор причелом координат на земле
    slvSelectPath,slvSetNewPlayersCoords,
    slvClientDisconnected,slvClientConnected,
    slvSelectOption
  };
  struct cmdSelectGround
  {
    unsigned x,y;
    unsigned short itemid;
  };
  struct cmdSelectPath
  {
    unsigned x,y;
  };
  struct cmdSetNewPlayersCoords
  {
    unsigned x,y;
  };
  struct cmdSelectOption
  {
    short itemid;
  };
  struct cmdLogin
  {
    cmdLogin():null(false){}
    // если я не задаю логин\пароль\чарнайм, и хочу чтобы клиент исползовал
    // предыдущие настройки
    bool null;
    char name[30];
    char pass[30];
    char charname[30];
  };
  struct cmdUseItemGump
  {
    unsigned itemserialtouse;
    short items[4];
  };
  struct cmdUseItem
  {
    cmdUseItem():t(tsimple) {;}
    //simple - простое исползьзование предмета
    //grownd исползьование предмета на координаты мира x,y 
    //itemserial - использование предмета на предмет
    enum type{tsimple,tground,titemserial} t;

    unsigned itemserial;
    union
    {
      struct ground_tag{ int x,y;unsigned short itemid; } ground;
      unsigned itemserialon;
    };
  };
  struct cmdMoveItemsG2G
  {
    unsigned containerserialfrom;
    int itemid;
    unsigned containerserialto;
  };
  struct cmdMoveItemG2G
  {
    unsigned containerserialfrom;
    int itemid;
    int count;
    unsigned containerserialto;
  };
  struct cmdMoveItem
  {
    unsigned itemwhat;
    unsigned containerserialto;
    int count;
  };
  struct cmdMstResult
  {
    cmdItem ci;
    int res;
  };
  struct cmdMstResultMsg:public cmdMstResult
  {
    char msg[1];
  };
  struct cmdMoveTo
  {
    int x,y;
  };
  struct cmdClientConnected
  {
    char name[64];
    char pass[64];
    char charname[64];
  };
  template <class T> struct retData
  {
    const T *data;

    retData(const void* p)
    {
      data=reinterpret_cast<const T*>((int)p+sizeof(cmdMstResult));
    }

    const T* operator ->() const
    {
      return data;
    }
  };
};

#endif
