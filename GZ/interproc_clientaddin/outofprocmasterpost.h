#pragma once;

#include "outofprocmasterdata.h"

class EventThread;

class COutOfProcMasterPost:public COutOfProcMasterData
{
public:
  virtual void ThreadGZUnitEvent(EventThread* p) = 0;
};
