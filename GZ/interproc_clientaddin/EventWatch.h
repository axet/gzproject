template <class T> class EventWatch
{
  HANDLE h;
  T* p;
public:
  EventWatch(T*w):p(w)
  {
    h=p->EventAdvise();
  };
  ~EventWatch()
  {
    p->EventRemove(h);
  }
  void EventParsed()
  {
    SetEvent(h);
  };
  void WaitForSingleObject()
  {
    ::WaitForSingleObject(h,-1);
  }
};
