#include "stdafx.h"
#include "OutOfProcMasterEvents.h"

void COutOfProcMasterEvents::SetIPM(CInterProcMaster *p)
{
  SentrySc sc(m_cs);
  m_p=p;
}

COutOfProcMasterEvents::COutOfProcMasterEvents()
{
  SentrySc sc(m_cs);
  m_cscontrol=CreateEvent(NULL,FALSE,TRUE,NULL);
  m_csevent=CreateEvent(NULL,TRUE,FALSE,NULL);
  m_defaultaborthandle=CreateEvent(NULL,TRUE,FALSE,NULL);
  m_currentaborthandle=m_defaultaborthandle;
  m_abortcount=0;
}

void COutOfProcMasterEvents::ReadEvent()
{
  WFC wfc(this);
  wfc.GetEvent();
}

void COutOfProcMasterEvents::ReadEvent(COutOfProcMasterData* p,HANDLE h)
{
  SentrySc sc(m_cs);
  if(m_buffer.empty())
    p->LocalConnectionLost();
  else
    p->Receive(&m_buffer.front(),m_buffer.size());;
  m_equeue.EventParsed(h);
}

COutOfProcMasterEvents::Data COutOfProcMasterEvents::Send(const unsigned char* buf,int bufsize)
{
  WFC wfc(this);
  m_p->Write(buf,bufsize);
  return wfc.GetResult();
}

HANDLE COutOfProcMasterEvents::EventAdvise()
{
  return m_equeue.CreateEvent();
}

void COutOfProcMasterEvents::EventRemove(HANDLE h)
{
  m_equeue.CloseEvent(h);
}

void COutOfProcMasterEvents::Poll()
{
  WFC wfc(this);
  wfc.Poll();
}

HANDLE COutOfProcMasterEvents::GetAbortHandle()
{
  SentrySc sc(m_cs);
  return m_defaultaborthandle;
}

HANDLE COutOfProcMasterEvents::GetCurrentAbort()
{
  SentrySc sc(COutOfProcMasterEvents::m_csaborthandle);
  return m_currentaborthandle;
}
