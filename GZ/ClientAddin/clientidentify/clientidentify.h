#ifndef __CLIENTIDENTIFY_H__
#define __CLIENTIDENTIFY_H__

#include <exception>

namespace ClientIdentify
{
  enum clientVesion{
    clientUnknown,client1264a,client200,client203,client300g,
    clientff,client308s,uotd308s,clientSphereClient,
    client400e,uotd400e};
  // clientUnknown не возвращает
  clientVesion GetClientVersion();
};

#endif
