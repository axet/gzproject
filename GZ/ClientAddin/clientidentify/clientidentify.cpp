#include "stdafx.h"

#include <fstream>
#include <windows.h>

#include "clientidentify.h"
#include "iniext/path.h"

unsigned calcule_crc(const char* filename)
{
  std::ifstream input(filename,std::ios::binary);
  if(!input.is_open())
    return 0;

  unsigned char inputbuf[4096];
  int read;
  int crc=0;
  while((read=input.read(reinterpret_cast<char*>(inputbuf),sizeof(inputbuf)).gcount())>0)
  {
    unsigned char *ptr=inputbuf;
    while(read-->0)
      crc+=*ptr++;
  }
  return crc;
}

ClientIdentify::clientVesion ClientIdentify::GetClientVersion()
{
  char buf[1024];
  GetModuleFileName(0,buf,sizeof(buf));
  switch(calcule_crc(buf))
  {
  case 0:
    throw std::exception("can't cacluce crc");
  case 0x6419221:
    return client1264a;
  case 0x066c567c:
    return client200;
  case 0x082c1583:
    return client203;
  case 0x8ac31b0:
    return client300g;
  case 0x083070b9:
    return clientff;
  case 0x0b1e76dc:
    return client308s;
  case 0x0b6a1faf:
    return uotd308s;
  case 0xb26afcf:
    return client400e;
  case 0xb821837:
    return uotd400e;
  default:
    return clientUnknown;
  }
}
