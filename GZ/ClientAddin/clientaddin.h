class CClientUO;

#include <exception>
#include <sstream>

#include "../include/atl.h"
#include "fluke/flukeslave.h"
#include "controlwindow/controlwindow.h"

class CClientAddin:public ControlWindow::CControlWindow
{
  CClientUO * m_clientuo;

  CClientUO* CreateClient();
public:
  CClientAddin();
  ~CClientAddin();
  void Poll();
  void InitInstance();
  void Exit(unsigned int);
};
