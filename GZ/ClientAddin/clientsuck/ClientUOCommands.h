#ifndef CLIENTUOCOMMANDS_H
#define CLIENTUOCOMMANDS_H

#include <atlbase.h>

#include "iofluke/memmngr.h"
#include "../../client/clientuosecure/clientuosecure.h"
#include "../../client/clientcommands.h"
#include "../../client/packets.h"
#include "../../interproc_clientaddin/outofprocslave.h"
#include "ClientObjects.h"

#include <set>
#include <assert.h>
#include <list>

class sentrysc
{
  CComAutoCriticalSection &m_p;

public:
  sentrysc(CComAutoCriticalSection &p):m_p(p)
  {
    m_p.Lock();
  }
  ~sentrysc()
  {
    m_p.Unlock();
  }
};

class CClientUOCommands: public virtual ClientUOSecure::CClientUOSecure,
  public virtual COutOfProcSlave,public virtual ClientObjects
{
public:
  class SentryCommand;
  class CommandControl;
  typedef std::set<Packets::EthCommand> CmdSet;

  CClientUOCommands();
  ~CClientUOCommands();

  SentryCommand* CreateInterceptor(const CmdSet&);
  SentryCommand* CreateInterceptor(Packets::EthCommand e);
  void RemoveInterceptor(SentryCommand*);

  virtual HWND GetWindow() = 0;
  virtual WorldCord GetCharCoord() = 0;
  static void GetCharStats(int* hits,int* mana,int* stam);

  int StatsGetHits();
  int StatsGetWeight();
  int StatsGetMana();
  int StatsGetStam();
  bool GetLoginData(char*name,int namemax,char* pass,int passmax,char* charname,int charnamemax);

protected:
  typedef std::list<SentryCommand*> SentrySet;

  //hits
  int m_mshits,m_msstam,m_msmana,m_msweight;
  // login data;
  std::string m_password,m_login,m_charname;

  //retval
  bool m_retvalclear;
  unsigned char m_retvalbak;
  unsigned char* m_retvalbuf;

  SentrySet m_sentryset;
  CComAutoCriticalSection m_sentrysetcs;

  void HaveCommand(Server::ServerByte* buf,int bufsize);

  static void FromClient(CMemMngr*,va_list args);
  void FromClient(Server::ServerByte* buf,int bufsize);
  static void FromServer(CMemMngr*,va_list args);
  static void FromServerLeave(CMemMngr*,int* retval);

private:
  // включает\выключает обработку пакетов на клиентеs
  virtual void ProcessPacket(bool);
};

// для того чтобы из клиента можно было дождаться опеделенной команды
//   существует этот класс.
// когда команда получена, uoaddin ждет уничтожения этого класса, это
//   момент когда буфер команды больше не требуеться клиенту.
//   либо команда чтения следующей команды

class CClientUOCommands::SentryCommand
{
  CClientUOCommands* m_p;

  void* m_cmdbuf;
  CmdSet m_cmdset;
  CComAutoCriticalSection m_cmdcs;

  //одждидание для этого класса, когда я жду команду с сервера
  HANDLE m_waitforClient;
  // когда сервер ждет меня, пока я просмотрю команду
  HANDLE m_waitforServer;

  bool m_clear;
  Packets::Packet m_replacepacket;

  void Initialize(CClientUOCommands* p);

public:
  class SentryCommandTimeout:public std::exception
  {
  public:
    SentryCommandTimeout(CmdSet&);
  };
  class NotAnticipatedCommand:public std::exception
  {
  public:
    NotAnticipatedCommand(Packets::EthCommand anticipatedcmd,Packets::EthCommand receivedcmd,CmdSet&);
  };

  SentryCommand(CClientUOCommands* p,const CmdSet &);
  SentryCommand(CClientUOCommands* p,Packets::EthCommand e);
  ~SentryCommand();
  Packets::EthCommand Catch(Packets::EthCommand anticipatedcommand = Packets::scNone);
  // когда с сервера приходит команда, он вызывает эту функцию
  // если команда успешно перехвачена, требуеться удалить RemoveAdvise
  
  // сказка такая. когда пришидшию с клиента комаду проверяют на перехват
  // и создан такой перехватываемый класс требуеться правильно выставлять
  // его состояние, состояние готов к перехвату выставляеться сразу после
  // создания класса, и убераеться когда происходит выход из функции Get()
  // смена состояния очень тонкий процесс, если его нарушить то происходит
  // дедлок. именно из за того что начинают проверять команду когда класс
  // начинает уничтожаться.
  // когда у класса не стоит флага готов "к перехвату" его нужно выкидывать
  // из списка перехватчиков основного обьекта перехвата.
  bool HaveCommand(void* cmdbuf);
  Server::ServerByte* Buffer();
  bool NeedClear();
  // игнорировать пакеты удобно, когда я не хочу чтобы на клиенте показывались
  // левые окошки, которые важны для моих операций к примеру показ гмпа для
  // создания предмета
  void SetClear();
  bool NeedReplace();
  void SetReplace(Packets::Packet&);
  const Packets::Packet& GetReplace();
  // отпускает команду перехваченную с килента
  void Release();
  // перед изменением состояния нужно его лочить\разлочивать
  void Lock();
  void Unlock();
};

class CClientUOCommands::CommandControl
{
  SentryCommand *m_sc;

public:
  CommandControl(SentryCommand * s);
  ~CommandControl();
  void Release();

  CClientUOCommands::SentryCommand* operator ->();
};
#endif
