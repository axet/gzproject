#include "clientuo.h"

/// Класс управления клиентом UOTD

class UOTD400e:public CClientUO
{
public:
  virtual void PathClient();
  virtual HWND GetWindow();
  void DisableClientCrypt();
};
