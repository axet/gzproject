class ClientObjects
{
public:
  struct Object
  {
    Client::ItemType it;
    WorldCord w;
    int amount;
    int stackid;
    int dir;
    int hue;
    int status;

    Object()
    {
    }

    Object(const Client::ItemType &it,WorldCord w,int amount,int stackid,int dir,int hue,int status)
    {
      this->it=it;
      this->w=w;
      this->amount=amount;
      this->stackid=stackid;
      this->dir=dir;
      this->hue=hue;
      this->status=status;
    }
  };

  typedef std::map<unsigned,Object> Objects;

  struct MOBPaper
  {
    Client::ItemType item;
    int amount;
    WorldCord w;
    int direction;
    int skincolor;
    int status;
    int notoriety;

    struct Item
    {
      unsigned serial;
      Client::ItemType item;
      int layer;
      int color;
    };

    typedef std::vector<Item> Items;
    Items items;
  };

  struct MOBMove
  {
    Client::ItemType item;
    WorldCord w;
    int dir;
    int skincol;
    int status;
    int noto;

    MOBMove(){}
    MOBMove(Client::ItemType item,WorldCord w,int dir,int skincol,int status,int noto)
    {
      this->item=item;
      this->w=w;
      this->dir=dir;
      this->skincol=skincol;
      this->status=status;
      this->noto=noto;
    }
  };

  struct MOB
  {
    std::string charname;
    int hits;
    int maxhits;
    bool grender;
    int str;
    int dex;
    int inta;
    int stam;
    int maxstam;
    int mana;
    int maxmana;
    int gold;
    int armor;
    int weight;

    MOB()
    {
    }

    MOB(const char* charname,int hits,int maxhits,bool grender,int str,int dex,int inta,int stam,int maxstam,int mana,int maxmana,int gold,int armor,int weight)
    {
      this->charname=charname;
      this->hits=hits;
      this->maxhits=maxhits;
      this->grender=grender;
      this->str=str;
      this->dex=dex;
      this->inta=inta;
      this->stam=stam;
      this->maxstam=maxstam;
      this->mana=mana;
      this->maxmana=maxmana;
      this->gold=gold;
      this->armor=armor;
      this->weight=weight;
    }
  };

  struct mobinfo
  {
    MOBPaper paper;
    MOBMove move;
    MOB mob;
  };
  
  typedef std::map<unsigned,mobinfo> MOBs;

protected:
  Objects m_objs;
  MOBs m_mobs;

public:
  void UpdateObject(unsigned serial,const Client::ItemType &it,WorldCord w,int amount,int stackid,int dir,int hue,int status);
  void UpdateObject(unsigned serial,const MOB&);
  void UpdateObject(unsigned serial,const MOBPaper&);
  void UpdateObject(unsigned serial,const MOBMove&);
  void UpdateObject(unsigned serial,int charid, const char* name);
  Objects FindItem(const Client::ItemType &it);
  mobinfo* FindChar(const char* name);
  void RemoveObject(unsigned serial);
};
