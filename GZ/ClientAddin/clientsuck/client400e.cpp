#include "stdafx.h"
#include "Client400e.h"

#define PATCH_B(x) \
{ \
  unsigned long l=x; \
  unsigned char yy[]=

#define PATCH_E ; \
  PatchProgram(l,yy,sizeof(yy)); \
}

#define PATCH(x,y) {unsigned char yy[]={y};PatchProgram(x,yy,sizeof(yy));}

void Client400e::ZeroFirstPacket()
{
  // first packet
  //.text:00417FB2 8B 0D A4 23 DC 00     mov    ecx, dword_DC23A4
  //.text:00417FB8 51                    push   ecx
  //.text:00417FB9 E8 8C 0A 10 00        call   WSOCK32_8
  PATCH_B(0x417fb2) {0x31,0xc0,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90} PATCH_E;
}

void Client400e::DisableClientCrypt()
{
  // login crypt
  //.text:0041A2DF 32 D0                 xor    dl, al
  PATCH_B(0x0041A2DF) {0x88,0xc2} PATCH_E;

  // game crypt
  //004313E8 32 D0            xor         dl,al 
  PATCH_B(0x004313E8) {0x90,0x90} PATCH_E;

  // from server - decrypt
  //004197A5 32 DA            xor         bl,dl 
  PATCH_B(0x004197A5) {0x90,0x90} PATCH_E;

  CClientUO::DisableClientCrypt();
}

void Client400e::PathClient()
{
}

HWND Client400e::GetWindow()
{
  return *(HWND*)0x00E57380;
}
