#include "stdafx.h"
#include "gui.h"

CGui::CGui():m_keystatenewhandler(false),m_showwindow(false)
{
  memset(m_keystates,0,sizeof(m_keystates));
  m_clientinit=CreateEvent(0,TRUE,FALSE,0);
}

void CGui::SetFocusDisable(CMemMngr* mem,va_list args)
{
  CGui* _this=dynamic_cast<CGui*>(mem);
  unsigned &hwnd=va_arg(args,unsigned);
  if(!_this->m_showwindow)
    hwnd=0;
}

void CGui::SetWindowPosDisable(CMemMngr* mem,va_list args)
{
  CGui* _this=dynamic_cast<CGui*>(mem);
  unsigned &hwnd=va_arg(args,unsigned);
  if(!_this->m_showwindow)
    hwnd=0;
}

void CGui::SetForegroundWindowDisable(CMemMngr* mem,va_list args)
{
  CGui* _this=dynamic_cast<CGui*>(mem);
  unsigned &hwnd=va_arg(args,unsigned);
  if(!_this->m_showwindow)
    hwnd=0;
}

void CGui::SetWindowPlacementDisable(CMemMngr* mem,va_list args)
{
  CGui* _this=dynamic_cast<CGui*>(mem);
  unsigned &hwnd=va_arg(args,unsigned);
  if(!_this->m_showwindow)
    hwnd=0;
}

void CGui::ShowWindowDisable(CMemMngr* mem,va_list args)
{
  CGui* _this=dynamic_cast<CGui*>(mem);
  unsigned &hwnd=va_arg(args,unsigned);
  if(!_this->m_showwindow)
    hwnd=0;

  SetEvent(_this->m_clientinit);
}

void CGui::SendText(const char* p)
{
  CWindow wnd(GetWindow());
  while(*p!=0)
    wnd.SendMessage(WM_CHAR,*p++,1);
}

void CGui::SendClick(int x,int y)
{
  CWindow wnd(GetWindow());
  wnd.PostMessage(WM_LBUTTONDOWN,0,MAKELONG(x,y));
  wnd.PostMessage(WM_LBUTTONUP,0,MAKELONG(x,y));
}

void CGui::SendRClick(int x,int y)
{
  CWindow wnd(GetWindow());
  wnd.PostMessage(WM_RBUTTONDOWN,0,MAKELONG(x,y));
  wnd.PostMessage(WM_RBUTTONUP,0,MAKELONG(x,y));
}

void CGui::SendRDClick(int x,int y)
{
  CWindow wnd(GetWindow());
  wnd.PostMessage(WM_RBUTTONDOWN,0,MAKELONG(x,y));
  wnd.PostMessage(WM_RBUTTONUP,0,MAKELONG(x,y));
  wnd.PostMessage(WM_RBUTTONDOWN,0,MAKELONG(x,y));
  wnd.PostMessage(WM_RBUTTONUP,0,MAKELONG(x,y));
}

void CGui::SendKey(unsigned key,int count,bool ctrl,bool alt,bool shift)
{
  sentrypresskey press(this,ctrl?VK_CONTROL:0);
  sentrypresskey press1(this,alt?VK_MENU:0);
  sentrypresskey press2(this,shift?VK_SHIFT:0);

  m_keystatenewhandler=true;

  CWindow wnd(GetWindow());
  while(count-- > 0)
  {
    wnd.SendMessage(WM_KEYDOWN,key,1);
    switch(key)
    {
    case VK_TAB:
      SendText("\x9");
      break;
    case VK_RETURN:
      SendText("\xd");
      break;
    case VK_BACK:
      SendText("\x08");
      break;
    }
    wnd.SendMessage(WM_KEYUP,key,1);
  }

  m_keystatenewhandler=false;
}


void CGui::GetKeyStateEnter(CMemMngr* client,va_list args)
{
  CGui* pclt=dynamic_cast<CGui*>(client);
  if(pclt==0)
    return;

  if(pclt->m_keystatenewhandler)
  {
    int key=va_arg(args,int);
    pclt->m_keystatereturn=pclt->m_keystates[key];
  }
}


void CGui::GetKeyStateLeave(CMemMngr* client,int *retval)
{
  CGui* pclt=dynamic_cast<CGui*>(client);
  if(pclt==0)
    return;

  if(pclt->m_keystatenewhandler)
    *retval=pclt->m_keystatereturn;
}

void CGui::WindowProc(CMemMngr* mem,va_list args)
{
  CGui* _this=dynamic_cast<CGui*>(mem);

  unsigned &hwnd=va_arg(args,unsigned);
  unsigned &message=va_arg(args,unsigned);
  unsigned &wparam=va_arg(args,unsigned);
  unsigned &lparam=va_arg(args,unsigned);

  switch(message)
  {
  case WM_CREATE:
    {
      CREATESTRUCT *p=(CREATESTRUCT*)lparam;
      p->style&=~WS_VISIBLE;
    }
    break;
  }
}

bool CGui::IsWindowVisible()
{
  return m_showwindow;
}

void CGui::ShowWindow(bool b)
{
  m_showwindow=b;
  if(GetWindow()!=0)
    ::ShowWindow(GetWindow(),b?SW_SHOW:SW_HIDE);
}

void CGui::WaitForClientInit()
{
  if(GetWindow()==0)
    WaitForSingleObject(m_clientinit,-1);
}

void CGui::CatchWindowCreate(CMemMngr* client,int *retval)
{
  CGui* pclt=dynamic_cast<CGui*>(client);

  SetEvent(pclt->m_clientinit);

  DBGTRACE("CGui::CatchWindowCreate\n");
}
