#include "stdafx.h"
#include "login.h"
#include "setup.h"

ClientUO::CLogin::CLogin():m_loggedin(false)
{
}

void ClientUO::CLogin::Login(const char*name,const char*pass,const char* charname)
{
  WaitForClientInit();

  if(name==0)
    name=m_login.c_str();
  if(pass==0)
    pass=m_password.c_str();
  if(charname==0)
    charname=m_charname.c_str();

  sentry_controlwindow scw(GetWindow());

  if(strlen(name)==0)
    throw std::exception("account name is empty");
  if(strlen(pass)==0)
    throw std::exception("password is empty");
  if(strlen(charname)==0)
    throw std::exception("char name is empty");

  SendKey(VK_TAB);
  SendKey(VK_TAB);

  int charindex=-1;
  {
    CmdSet cs;
    cs.insert(Packets::scPostLogin);
    CommandControl secondlogin(CreateInterceptor(cs));
    {
      cs.insert(Packets::scBritaniaList);
      CommandControl britanialist(CreateInterceptor(cs));
      {
        cs.insert(Packets::scLogin);
        CommandControl firstlogin(CreateInterceptor(cs));
        SendClick(615,455); // next
        firstlogin->Catch(Packets::scLogin);
        firstlogin->SetReplace((Packets::Packet)Packets::scsLogin(name,pass));
      }
      britanialist->Catch(Packets::scBritaniaList);
      SendClick(615,455); // next
    }
    secondlogin->Catch(Packets::scPostLogin);
    Packets::scsPostLogin *p=(Packets::scsPostLogin *)secondlogin->Buffer();
    secondlogin->SetReplace((Packets::Packet)Packets::scsPostLogin(p->authid,name,pass));
  }

  {
    CClientUOCommands::CmdSet cs;
    cs.insert(Packets::scCitiesAndChars);
    cs.insert(Packets::scLoginComplite);
    cs.insert(Packets::scAcctLoginFail);
    CommandControl cc(CreateInterceptor(cs));
    SendClick(615,455); // next
    switch(cc->Catch())
    {
    case Packets::scAcctLoginFail:
      {
        throw LoginError(Packets::scsAcctLoginFail::GetErrorText(cc->Buffer()));
      }
      break;
    case Packets::scCitiesAndChars:
      {
        Packets::scsCitiesAndChars *p=(Packets::scsCitiesAndChars *)cc->Buffer();
        Packets::scsCitiesAndChars::Chars_tag &chars=p->GetChars();
        Packets::scsCitiesAndChars::Cities_tag &cities=p->GetCities();
        for(int i=0;i<chars.numchars;i++)
        {
          if(stricmp(chars.Char[i].CharName,charname)==0&&charindex==-1)
            charindex=i;
        }
      }
      break;
    case Packets::scLoginComplite: //login correct
      return;
    }
  }
  if(charindex==-1)
  {
    ShowWindow(true);
    throw std::exception((std::string("char name not found '")+charname+"'").c_str());
  }

  SendClick(365,165+charindex*40); // first char

  {
    CommandControl cc(CreateInterceptor(Packets::scLoginConfirm));
    SendClick(615,455); // next
    cc->Catch();
  }
}

void ClientUO::CLogin::LoggedInPass(CMemMngr* client,va_list args)
{
  ClientUO::CLogin* thisclass=dynamic_cast<ClientUO::CLogin*>(client);
  thisclass->m_loggedin=true;
  //thisclass->ConnectionEstablished();
}

void ClientUO::CLogin::ClientDisconnected(CMemMngr* memmngr,va_list args)
{
  ClientUO::CLogin* thisclass=dynamic_cast<ClientUO::CLogin*>(memmngr);
  thisclass->ConnectionLost();
}
