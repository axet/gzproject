#include "stdafx.h"
#include "UOTD308S.h"

#define PATCH_B(x) \
{ \
  unsigned long l=x; \
  unsigned char yy[]=

#define PATCH_E ; \
  PatchProgram(l,yy,sizeof(yy)); \
}

void UOTD308S::PathClient()
{
  CClientUO::PatchClient();
}

void UOTD308S::DisableClientCrypt()
{
  // first packet
  //.text:00482AFF                 mov     ecx, dword_103BBF0
  //.text:00482B05                 push    ecx
  //.text:00482B06                 call    WSOCK32_8
  //PATCH_B(0x00482AFF) {0x31,0xc0,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90} PATCH_E;

  // login crypt
  //00483C36 32 D0            xor         dl,al 
  PATCH_B(0x00483C36) {0x88,0xc2} PATCH_E;

  // game crypt
  //.text:00495F59                 xor     dl, al
  PATCH_B(0x00495F59) {0x90,0x90} PATCH_E;

  // from server - decrypt
  //.text:0048313B                 xor     [ecx], dl
  PATCH_B(0x0048313B) {0x90,0x90} PATCH_E;

  CClientUO::DisableClientCrypt();
}

HWND UOTD308S::GetWindow()
{
  return *(HWND*)0x00C70B60;
}
