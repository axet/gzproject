#include "stdafx.h"
#include "clientff.h"

const unsigned g_CatchLogUni=0x004C4990+0x3ff0;
const unsigned g_CatchLog=0x004C4A00+0x3ff0;
const unsigned g_FromServer=0x004C20C0-0x9B330;
const unsigned g_FromClient=0x004C0E30-0x9B310;

const unsigned g_CatchWindowCreate=0x4D7180; //0x004D5C60+0x1520;
const unsigned g_SetNewPlayersCoord=0x00477C80+0x36640;

const unsigned g_ClientDisconnected=0x004C1110-0x9B290;
const unsigned g_LoggedInPass=0x00491030-0x806F0;

const unsigned g_multiuo1=0x004D5457+0x1520;
const unsigned g_multiuo2=0x004D56DD+0x1520;

const unsigned g_uosend0=0x00CC5164+0x47F8;
const unsigned g_uosend1=0x30028-0x4;
const unsigned g_uosend2=0x10020-0x4;

const unsigned g_send=0xCC516C+0x47F8;
const unsigned g_sendproc=0x4C0E30-0x9B310;

const unsigned g_window=0xD58724+0x1510;

const unsigned g_PathFindingTerminate=0x004A64F0+0x1BA30;
const unsigned g_PathFindingNetCall=0x0047DD10+0x36640;
const unsigned g_PathFindingNetCallTerminate=0x00541B20+0x4DB8;
const unsigned g_PathFindingNetCallRetCode=0xCC9380+0x47D0;

const unsigned g_WindowProc=0x004D6380+0x1520;

CClientFF::CClientFF()
{
}

CClientFF::~CClientFF()
{
}

void CClientFF::SendToServer(const void* buf,int bufsize)
{
  __asm
  {
    mov eax,g_send;
    mov ecx,[eax];
    lea eax,bufsize;
    push [eax];
    lea eax,buf;
    push [eax];

    mov eax,g_sendproc;
    call eax;
  };
}

void CClientFF::ProcessPacket(bool enable)
{
  int adr=0x004C2219;
  int adr2=0x004C21F8;
  int adr3=0x004C20C0;

  DWORD old;
  VirtualProtect((void*)adr,2,PAGE_EXECUTE_READWRITE,&old);
  VirtualProtect((void*)adr2,2,PAGE_EXECUTE_READWRITE,&old);
  //VirtualProtect((void*)adr3,4,PAGE_EXECUTE_READWRITE,&old);

  if(enable)
  {
    *(unsigned short*)adr=0xb53D;
    *(unsigned short*)adr2=0xb53D;
    //*(unsigned*)adr3=0x004c818b;
  }else
  {//off
    *(unsigned short*)adr=0x003D;
    *(unsigned short*)adr2=0x003D;
    //*(unsigned*)adr3=0x000004c2;
  }
}

HWND CClientFF::GetWindow()
{
  return *(HWND*)g_window;
}

WorldCord CClientFF::GetCharCoord()
{
  WorldCord w;
  w.x=*(unsigned*)0x00CC0804;
  w.y=*(unsigned*)0x00CC0800;
  w.z=*(unsigned*)0x00CC07FC;
  return w;
}

void CClientFF::PathFinding(WorldCord w)
{
  typedef void (__cdecl *_PathFindingNetCall) (int x,int y,int z);
  _PathFindingNetCall PathFindingNetCall=(_PathFindingNetCall)g_PathFindingNetCall;
  *(int*)g_PathFindingNetCallTerminate=1;
  PathFindingNetCall(w.x,w.y,w.z);
}

bool CClientFF::PathFindingIsStarted()
{
  return *(unsigned*)g_PathFindingNetCallRetCode!=0;
}

void CClientFF::PathClient()
{
  CatchRetFunction(g_FromClient,FromClient);
  CatchRetFunction(g_SetNewPlayersCoord,0,SetNewPlayersCoord);
  CatchRetFunction(g_CatchLogUni,CatchLogUni);
  CatchRetFunction(g_CatchLog,CatchLog);
  PatchProgram(g_multiuo1,"\xeb",1);
  PatchProgram(g_multiuo2,"\xeb",1);

  //gui
  CatchRetFunction(g_CatchWindowCreate,0,CatchWindowCreate);
  CatchImportFunction("GetKeyState","User32.dll",GetKeyStateEnter,GetKeyStateLeave);
  CatchImportFunction("ShowWindow","User32.dll",ShowWindowDisable,0);
  CatchImportFunction("SetForegroundWindow","User32.dll",SetForegroundWindowDisable,0);
  CatchImportFunction("SetWindowPlacement","User32.dll",SetWindowPlacementDisable,0);
  CatchImportFunction("SetWindowPos","User32.dll",SetWindowPosDisable,0);
  CatchImportFunction("SetFocusPlacement","User32.dll",SetFocusDisable,0);

  //uo commands
  CatchRetFunction(g_FromServer,FromServer,FromServerLeave);

  //login
  CatchRetFunction(g_ClientDisconnected,ClientDisconnected);
  CatchRetFunction(g_LoggedInPass,LoggedInPass);

  //path finding
  CatchRetFunction(g_PathFindingTerminate,PathFindingTerminate);

  //slepping task
  CatchRetFunction(g_WindowProc,WindowProc);
}
