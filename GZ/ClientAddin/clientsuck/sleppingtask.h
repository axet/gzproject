#ifndef __SLEPPING_TASK_H
#define __SLEPPING_TASK_H

#include "iofluke/memmngr.h"
#include "gui.h"

class CSleppingTask:public virtual ClientUOSecure::CClientUOSecure,public virtual CGui
{
public:
  typedef void (*MYCALL)(DWORD);

  CSleppingTask();
  void SleppingTask(MYCALL fp,DWORD fpprm);

protected:
  MYCALL m_fp;
  DWORD m_fpprm;

  static void WindowProc(CMemMngr*,va_list args);
};

#endif
