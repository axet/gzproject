#include "stdafx.h"
#include "sleppingtask.h"

CSleppingTask::CSleppingTask():m_fp(0)
{
}

void CSleppingTask::SleppingTask(MYCALL fp,DWORD d)
{
  m_fp=fp;
  m_fpprm=d;
  ::SendMessage(GetWindow(),WM_NULL,0,0);
  m_fp=0;
}

void CSleppingTask::WindowProc(CMemMngr* memmngr,va_list args)
{
  CSleppingTask* thisclass=dynamic_cast<CSleppingTask*>(memmngr);
  if(thisclass->m_fp!=0)
    thisclass->m_fp(thisclass->m_fpprm);

  CGui::WindowProc(memmngr,args);
}
