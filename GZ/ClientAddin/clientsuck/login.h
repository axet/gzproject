#ifndef __LOGIN_H__
#define __LOGIN_H__

#include <winsock2.h>

#include "gui.h"
#include "iofluke/memmngr.h"

namespace ClientUO
{
  class CLogin;
};

class ClientUO::CLogin:public virtual ClientUOSecure::CClientUOSecure,
  public virtual CGui
{
protected:
  // состояние залогинился ли клиент?
  bool m_loggedin;

  // когда клиент успешно логиниться на сервер эта функция вызываеться
  static void LoggedInPass(CMemMngr*,va_list args);
  static void ClientDisconnected(CMemMngr*,va_list args);
public:
  class LoginError:public std::exception
  {
  public:
    LoginError(const char*p):exception(p) {}
  };

  CLogin();
  void Login(const char*name = 0,const char*pass = 0,const char* charname = 0);
};

#endif