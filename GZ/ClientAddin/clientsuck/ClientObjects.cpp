#include "stdafx.h"
#include "ClientObjects.h"

void ClientObjects::UpdateObject(unsigned serial,const Client::ItemType &it,WorldCord w,int amount,int stackid,int dir,int hue,int status)
{
  m_objs[serial]=Object(it,w,amount,stackid,dir,hue,status);
}

void ClientObjects::RemoveObject(unsigned serial)
{
  m_objs.erase(serial);
}

ClientObjects::Objects ClientObjects::FindItem(const Client::ItemType &it)
{
  Objects sl;
  for(Objects::iterator i=m_objs.begin();i!=m_objs.end();i++)
  {
    if(it.oneof(i->second.it))
      sl[i->first]=i->second;
  }
  return sl;
}

ClientObjects::mobinfo* ClientObjects::FindChar(const char* name)
{
  Objects sl;
  for(MOBs::iterator i=m_mobs.begin();i!=m_mobs.end();i++)
  {
    if(i->second.mob.charname==name)
      return &i->second;
  }
  return 0;
}

void ClientObjects::UpdateObject(unsigned serial,const MOB& m)
{
  m_mobs[serial].mob=m;
}

void ClientObjects::UpdateObject(unsigned serial,const MOBPaper& m)
{
  m_mobs[serial].paper=m;
}

void ClientObjects::UpdateObject(unsigned serial,const MOBMove&m)
{
  m_mobs[serial].move=m;
}

void ClientObjects::UpdateObject(unsigned serial,int charid, const char* name)
{
  m_mobs[serial].mob.charname=name;
}
