#include "stdafx.h"
#include "clientunknown.h"

#include "../../interproc_clientaddin/xsd/InterprocClientAddin/InterprocClientAddin.h"
#include "../xsd/clientaddinconfig/ClientAddinConfigBase.h"

using namespace ica;
using namespace cac;

void ClientUnknown::SendToServer(const void* buf,int bufsize)
{
  throw std::exception("SendToServer unsuported");
}

void ClientUnknown::DisableClientCrypt()
{
  throw std::exception("DisableClientCrypt unsuported");
}

void ClientUnknown::ProcessPacket(bool enable)
{
  throw std::exception("ProcessPacket unsuported");
}

HWND ClientUnknown::GetWindow()
{
  throw std::exception("GetWindow unsuported");
}

WorldCord ClientUnknown::GetCharCoord()
{
  throw std::exception("GetCharCoord unsuported");
}

void ClientUnknown::PathFinding(WorldCord w)
{
  throw std::exception("PathFinding unsuported");
}

bool ClientUnknown::PathFindingIsStarted()
{
  throw std::exception("PathFindingIsStarted unsuported");
}

void ClientUnknown::PathFindingTerminate(CMemMngr* client,va_list args)
{
  throw std::exception("PathFindingTerminate unsuported");
}

void ClientUnknown::PatchClient()
{
  CClientUO::PatchClient();
}

void ClientUnknown::ReadConfig(const char* buf)
{
  {
    CDocAddin<CInterprocClientAddinDoc> doc;
    CPacketMasterType packet=doc.LoadXML(buf);

    // must be
    CConfigType &config=packet.GetConfig();
  }
  //{
  //  CPacketSlaveType packet;
  //  packet.AddException("Client version unsupported");
  //  Send(packet);
  //}
  {
    CPacketSlaveType packet;
    packet.AddUnknownClient("");
    Send(packet);
  }
}
