#ifndef GUI_H
#define GUI_H

#include <winsock2.h>

#include "iofluke/memmngr.h"
#include "../../client/clientcommands.h"
#include "clientUOcommands.h"

class CGui:public virtual ClientUOSecure::CClientUOSecure,
  public virtual CClientUOCommands
{
protected:
  //евент зажигаеться когда клиент готов к работе
  HANDLE m_clientinit;
  //если я хочу обрабатывать состояния клавишь = true
  bool m_keystatenewhandler;
  //массив состояний кнопок, исползьзуеться только для контрольных кнопок (ALT CTRL SHIFT)
  unsigned int m_keystates[256];
  int m_keystatereturn;
  // можно ли показывать окно клиента
  bool m_showwindow;

  static void ShowWindowDisable(CMemMngr*,va_list args);
  static void SetForegroundWindowDisable(CMemMngr*,va_list args);
  static void SetFocusDisable(CMemMngr* mem,va_list args);
  static void SetWindowPosDisable(CMemMngr* mem,va_list args);
  static void SetWindowPlacementDisable(CMemMngr*,va_list args);
  static void GetKeyStateEnter(CMemMngr*,va_list args);
  static void GetKeyStateLeave(CMemMngr*,int *retval);
  static void WindowProc(CMemMngr*,va_list args);
  static void CatchWindowCreate(CMemMngr*,int *retval);

  //для управления нажатием функциональных клашишь
  class sentrypresskey
  {
    int m_key;
    CGui* m_p;
  public:
    sentrypresskey(CGui* p, int key)
    {
      m_p=p;

      m_p->m_keystatenewhandler=true;
      m_p->m_keystates[key]=0xffffff81;
      m_key=key;
    }
    ~sentrypresskey()
    {
      m_p->m_keystatenewhandler=false;
      m_p->m_keystates[m_key]=0;
    }
  };
  friend sentrypresskey;

  class sentry_controlwindow
  {
    HWND m_h;
  public:
    sentry_controlwindow(HWND h):m_h(h)
    {
      ::EnableWindow(m_h,FALSE);
    }
    ~sentry_controlwindow()
    {
      ::EnableWindow(m_h,TRUE);
    }
  };

public:
  CGui();
  bool IsWindowVisible();
  void SendText(const char*);
  void SendClick(int x, int y);
  void SendRClick(int x, int y);
  void SendRDClick(int x, int y);
  void SendKey(unsigned key,int count = 1,bool ctrl = false,bool alt=false,bool shift=false);
  void ShowWindow(bool b);
  virtual void WaitForClientInit();
};

#endif
