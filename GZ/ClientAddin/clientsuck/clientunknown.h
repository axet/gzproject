#include "clientuo.h"

/// Класс управления клиентом 

class ClientUnknown:public CClientUO
{
public:

  virtual void ReadConfig(const char*);

  void SendToServer(const void* buf,int bufsize);
  void DisableClientCrypt();
  void ProcessPacket(bool enable);
  HWND GetWindow();
  WorldCord GetCharCoord();
  void PathFinding(WorldCord w);
  bool PathFindingIsStarted();
  void PathFindingTerminate(CMemMngr* client,va_list args);
  void PatchClient();
};
