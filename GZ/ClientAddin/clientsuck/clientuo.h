#ifndef __CLIENTUO_H
#define __CLIENTUO_H

#include "iofluke/memmngr.h"
#include "../../client/clientcommands.h"
#include "../../client/clientgamecommands.h"
#include "../../interproc_clientaddin/outofprocslave.h"
#include "pathfinding.h"
#include "clientuocommands.h"
#include "gui.h"
#include "sleppingtask.h"
#include "login.h"
#include "../../client/clientuosecure/clientuosecure.h"

class CClientEvents;

#pragma warning(disable:4250)

class CClientUO:
  public CPathFinding, public virtual CClientUOCommands,public virtual CGui,
  public virtual CSleppingTask,public ClientUO::CLogin,public virtual COutOfProcSlave,
  public virtual ClientUOSecure::CClientUOSecure
{
protected:
  using CClientUOCommands::SentryCommand;

  CComAutoCriticalSection m_clientUOCS;
  // имя окна
  char m_windowname[128];

  // catch's

  static void SetNewPlayersCoord(CMemMngr*,int *retval);
  static void CatchLogDebugMsg(CMemMngr*,va_list args);
  static void CatchLog(CMemMngr*,va_list args);
  static void CatchLogUni(CMemMngr*,va_list args);

  void Lock();
  void Unlock();

public:
  CClientUO();
  ~CClientUO();
  virtual void SendToServer(const void* buf,int bufsize);
  void CloseClient();
  virtual void PatchClient();

  virtual void ReadConfig(const char*);

  using CPathFinding::CharMoveTo;
  using CClientUOCommands::StatsGetHits;
  using CClientUOCommands::StatsGetWeight;

  virtual unsigned SelectItem(unsigned containerserialwhere);
  virtual unsigned SelectItem(unsigned containerserialwhere,const Client::ItemType &it);
  virtual unsigned SelectItem(unsigned containerserialfrom,const Client::ItemType &it,int count);

  std::string UseItem(unsigned itemserial);
  std::string UseItem(unsigned itemserialtouse, unsigned itemserialon);
  std::string UseItem(unsigned itemserialtouse, WorldCord w,const Client::ItemType &);
  unsigned UseItemGump(unsigned itemserialtouse,const ShortList &);
  unsigned UseItemGump(unsigned itemserialtouse,const UnsignedList &, const ShortList &);
  void MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType& it,unsigned containerserialto);
  void MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType& it,int count,unsigned containerserialto);
  unsigned MoveItemG2G(unsigned containerserialfrom,const Client::ItemType& it,unsigned containerserialto, int count = -1);
  unsigned MoveItem(unsigned itemwhat,unsigned containerserialto, int count = -1);
  unsigned PinchItem(const Client::ItemType &it,unsigned containerserialfrom,int count);
  unsigned GetContainerItem(const Client::ItemType &it,unsigned containerserialfrom);
  unsigned GetEquippedItem(const Client::ItemType &it,const char* name);
  void Cancel();
  void CharSpeech(const char*);
  const char* GetClientName();

  void VendorOffer(const char*name,const ClientGameCommands::Vendors::Items&);
};

#endif
