#ifndef __CLIENT_FF_HH
#define __CLIENT_FF_HH

#include "client203.h"

class CClientFF:public CClient203
{
  virtual void SendToServer(const void* buf,int bufsize);
  virtual void ProcessPacket(bool enable);
  virtual HWND GetWindow();
  virtual void PathClient();
  virtual WorldCord GetCharCoord();
  virtual bool PathFindingIsStarted();
  virtual void PathFinding(WorldCord w);
public:
  CClientFF();
  ~CClientFF();
};

#endif
