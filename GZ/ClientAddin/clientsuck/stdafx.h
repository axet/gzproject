// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#include "../../client/clientcommands.h"
#include "../../client/clientevents.h"
#include "../../client/clientgamecommands.h"
#include "../../client/clientuosecure/clientuosecure.h"
#include "../../client/ethinterface.h"
#include "../../client/packets.h"
#include "../../client/client.h"
#include "../../include/atl.h"
#include <Altova/AltovaLib.h>
#include "../../interproc_clientaddin/outofprocslave.h"
#include "debugger/debug.h"
#include "iofluke/memmngr.h"

#include <atlbase.h>
#include <atlapp.h>

#include <assert.h>
#include <comdef.h>
#include <complex>
#include <process.h>
#include <sstream>
#include <string>
#include <windows.h>
#include <winsock2.h>



// TODO: reference additional headers your program requires here
