#ifndef __PATHFINDING_UO
#define __PATHFINDING_UO

#include "iofluke/memmngr.h"
#include "clientUOcommands.h"
#include "../../client/clientcommands.h"
#include "gui.h"
#include "sleppingtask.h"
#include <string>

#pragma warning(disable:4250)

class CPathFinding:public virtual ClientUOSecure::CClientUOSecure,
  public virtual CClientUOCommands,
  public virtual CGui, public virtual CSleppingTask,public virtual ClientObjects
{
  class PathNotFounded:public std::exception
  {
  public:
    const char* what() const throw()
    {
      return "Path Not Founded!";
    }
  };
protected:
  //pathfinding wait;
  HANDLE m_pathfinding;
  int m_x,m_y,m_z;

  int linelen(int x1,int y1,int x2,int y2);
  void DoMoveTo(WorldCord w);
  virtual void PathFinding(WorldCord w) = 0;
  virtual bool PathFindingIsStarted() = 0;

  static void PathFindingRun(CPathFinding*p);
  void PathFindingTerminate();

public:
  CPathFinding();
  ~CPathFinding();
  void CharMoveTo(WorldCord w);
  void CharMoveToChar(const char* name);
};

#endif
