#include "stdafx.h"
#include "client200.h"

void Client200::DisableClientCrypt()
{
  unsigned char path[]={0x90,0x90};

  PatchProgram(0x0040F79C,path,sizeof(path));
  PatchProgram(0x004BAF22,path,sizeof(path));

  CClientUO::DisableClientCrypt();
}
