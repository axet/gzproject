#include "client203.h"

/// Класс управления клиентом 

class Client308S:public CClientUO
{
  HANDLE m_readyforlogin;

  //virtual void WaitForClientInit();
  static void CatchReadyForLogin(CMemMngr*,int *retval);
  static void CatchFromClient(CMemMngr*,va_list args);

public:
  Client308S();
  ~Client308S();
  virtual void PatchClient();
  virtual void ZeroFirstPacket();
  virtual void DisableClientCrypt();
  virtual void GetCharCoord(int*x,int*y,int *z);
  virtual HWND GetWindow();
};
