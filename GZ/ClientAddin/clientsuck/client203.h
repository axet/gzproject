#ifndef __CLIENT203_H
#define __CLIENT203_H

#include "clientuo.h"

/// Класс управления клиентом 

class CClient203:public CClientUO
{
protected:
  virtual void DisableClientCrypt();
  virtual void SendToServer(const void* buf,int bufsize);
  virtual void ProcessPacket(bool enable);
  virtual HWND GetWindow();
  virtual void PatchClient();
  virtual WorldCord GetCharCoord();
  virtual bool PathFindingIsStarted();
  virtual void PathFinding(WorldCord w);
  static void PathFindingTerminate(CMemMngr* client,va_list args);
public:
  CClient203();
  ~CClient203();
};

#endif
