#include "stdafx.h"
#include "UOTD400e.h"

#define PATCH_B(x) \
{ \
  unsigned long l=x; \
  unsigned char yy[]=

#define PATCH_E ; \
  PatchProgram(l,yy,sizeof(yy)); \
}

void UOTD400e::PathClient()
{
}

void UOTD400e::DisableClientCrypt()
{
  // login crypt
  //.text:00485BF6                 xor     dl, al
  PATCH_B(0x00485BF6) {0x88,0xc2} PATCH_E;

  // game crypt
  //.text:00498049                 xor     dl, al
  PATCH_B(0x00498049) {0x90,0x90} PATCH_E;

  // from server - decrypt
  //.text:0048313B                 xor     [ecx], dl
  PATCH_B(0x004850A2) {0x90,0x90} PATCH_E;

  CClientUO::DisableClientCrypt();
}

HWND UOTD400e::GetWindow()
{
  return *(HWND*)0x00C6B580;
}
