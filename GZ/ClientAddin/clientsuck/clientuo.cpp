#include "stdafx.h"
#include "clientuo.h"
#include "setup.h"

#include <errorreport/errorreport.h>
#include "../../interproc_clientaddin/xsd/InterprocClientAddin/InterprocClientAddin.h"
#include "../xsd/clientaddinconfig/ClientAddinConfigBase.h"

/// время которое замедляет выполнение команд, торговли и перекладывания предметов
const int g_slowexchange=2000;


using namespace ica;
using namespace cac;

#pragma comment(lib,"Ws2_32.lib")

CClientUO::CClientUO()
{
  Connect(GetCurrentProcessId());
};

void CClientUO::PatchClient()
{
  CatchImportFunction("GetKeyState","User32.dll",GetKeyStateEnter,GetKeyStateLeave);
  CatchImportFunction("ShowWindow","User32.dll",ShowWindowDisable,0);
  CatchImportFunction("SetForegroundWindow","User32.dll",SetForegroundWindowDisable,0);
  CatchImportFunction("SetWindowPlacement","User32.dll",SetWindowPlacementDisable,0);
  CatchImportFunction("SetWindowPos","User32.dll",SetWindowPosDisable,0);
  CatchImportFunction("SetFocusPlacement","User32.dll",SetFocusDisable,0);

  CatchImportFunction("CreateWindowExA","User32.dll",0,CatchWindowCreate);
  CatchImportFunction("CreateWindowEx","User32.dll",0,CatchWindowCreate);
  CatchImportFunction("CreateWindowExW","User32.dll",0,CatchWindowCreate);
}

CClientUO::~CClientUO()
{
}

void CClientUO::SetNewPlayersCoord(CMemMngr* client,int *retval)
{
  try
  {
    CClientUO* pclt=dynamic_cast<CClientUO*>(client);
    if(pclt==0)
      return;

    WorldCord w;
    w=pclt->GetCharCoord();
    pclt->SelectedPath(w);
    pclt->SetNewPlayersCoords(w);
  }catch(std::exception &e)
  {
    DBGTRACE("%s\n",ErrorReport("CClientUO::SetNewPlayersCoord")+e.what());
  }
}

void CClientUO::CatchLog(CMemMngr* client,va_list args)
{
  CClientUO* pclt=dynamic_cast<CClientUO*>(client);

  char* p;
  p=va_arg(args,char*);
  pclt->LogMessage(p);
}

void CClientUO::CatchLogDebugMsg(CMemMngr* client,va_list args)
{
  CClientUO* pclt=dynamic_cast<CClientUO*>(client);

  char* p;
  p=va_arg(args,char*);
  p=va_arg(args,char*);
  p=va_arg(args,char*);
  pclt->LogMessage(p);
}

void CClientUO::CatchLogUni(CMemMngr* client,va_list args)
{
  CClientUO* pclt=dynamic_cast<CClientUO*>(client);

  BSTR p;
  p=va_arg(args,BSTR);
  pclt->LogMessage(_bstr_t(p));
}

void CClientUO::SendToServer(const void* buf,int bufsize)
{
  throw Client::OperationNotSupported("CClientUO::SendToServer");
}

void CClientUO::CloseClient()
{
  ::PostMessage(GetWindow(),WM_QUIT,0,0);
  //exit(0);
  //::PostMessage(GetWindow(),WM_CLOSE,0,0);
}

std::string CClientUO::UseItem(unsigned itemserial)
{
  Packets::scsUseItem item;
  item.itemserial=itemserial;
  CommandControl ssc(CreateInterceptor(Packets::scSpeaking));
  SendToServer(&item,sizeof(item));
  ssc->Catch();
  Packets::scsSpeaking pack((unsigned char*)ssc->Buffer());
  return pack.text();
}

std::string CClientUO::UseItem(unsigned itemserialtouse, unsigned itemserialon)
{
  throw Client::OperationNotSupported("CClientUO::UseItem");
}

std::string CClientUO::UseItem(unsigned itemserialtouse, WorldCord w, const Client::ItemType&it)
{
  Packets::scsUseItem item;
  item.itemserial=itemserialtouse;

  Packets::scsTarget trg;
  {
    CommandControl ssc(CreateInterceptor(Packets::scTarget));
    SendToServer(&item,sizeof(item));
    ssc->Catch();
    trg=*(Packets::scsTarget *)ssc->Buffer();
    trg.worldx=w.x;
    trg.worldy=w.y;
    trg.worldz=w.z;
    trg.type=1;// ground
    trg.itemid=(unsigned short)it;
  }

  CommandControl ssc(CreateInterceptor(Packets::scSpeaking));
  SendToServer(&trg,sizeof(trg));
  ssc->Catch();
  Packets::scsSpeaking pack((unsigned char*)ssc->Buffer());
  return pack.text();
}

void CClientUO::Cancel()
{
  throw Client::OperationNotSupported("CClientUO::Cancel");
}

const char* CClientUO::GetClientName()
{
  ::GetWindowText(GetWindow(),m_windowname,sizeof(m_windowname));
  return m_windowname;
}

void CClientUO::MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType& it,int count,unsigned containerserialto)
{
  Packets::scsOpenGump gump;
  {
    Packets::scsUseItem ui;
    ui.itemserial=containerserialfrom;
    CommandControl ssc(CreateInterceptor(Packets::scOpenGump));
    SendToServer(&ui,sizeof(ui));
    ssc->Catch();
    ssc->SetClear();
    gump=ssc->Buffer();
  }

  for(int i=0;i<gump.GetItemsCount();i++)
  {
    Packets::scsOpenGump::item_tag &item=*gump.GetItem(i);
    if(it==item.itemid&&item.itemamount<=count)
    {
      Packets::scsReqObjGet cmd1;
      cmd1.itemserial=item.itemserial;
      cmd1.amount=item.itemamount;
      SendToServer(&cmd1,sizeof(cmd1));
      Packets::scsReqObjDrop cmd;
      cmd.itemserial=item.itemserial;
      cmd.z=0;
      cmd.x=-1;
      cmd.y=-1;
      cmd.containerserial=containerserialto;
      SendToServer(&cmd,sizeof(cmd));
    }
  }
}

unsigned CClientUO::SelectItem(unsigned containerserialwhere)
{
  Packets::scsOpenGump gump;
  {
    Packets::scsUseItem ui;
    ui.itemserial=containerserialwhere;
    CommandControl ssc(CreateInterceptor(Packets::scOpenGump));
    SendToServer(&ui,sizeof(ui));
    ssc->Catch();
    ssc->SetClear();
    gump=ssc->Buffer();
  }

  for(int i=0;i<gump.GetItemsCount();i++)
  {
    Packets::scsOpenGump::item_tag &item=*gump.GetItem(i);
    return item.itemserial;
  }
  return 0;
}

unsigned CClientUO::SelectItem(unsigned containerserialwhere,const Client::ItemType &it)
{
  Packets::scsOpenGump gump;
  {
    Packets::scsUseItem ui;
    ui.itemserial=containerserialwhere;
    CommandControl ssc(CreateInterceptor(Packets::scOpenGump));
    SendToServer(&ui,sizeof(ui));
    ssc->Catch();
    ssc->SetClear();
    gump=ssc->Buffer();
  }

  for(int i=0;i<gump.GetItemsCount();i++)
  {
    Packets::scsOpenGump::item_tag &item=*gump.GetItem(i);
    if(item.itemid==it)
      return item.itemserial;
  }
  return 0;
}

unsigned CClientUO::SelectItem(unsigned containerserialwhere,const Client::ItemType &it,int count)
{
  Packets::scsOpenGump gump;
  {
    Packets::scsUseItem ui;
    ui.itemserial=containerserialwhere;
    CommandControl ssc(CreateInterceptor(Packets::scOpenGump));
    SendToServer(&ui,sizeof(ui));
    ssc->Catch();
    ssc->SetClear();
    gump=ssc->Buffer();
  }

  for(int i=0;i<gump.GetItemsCount();i++)
  {
    Packets::scsOpenGump::item_tag &item=*gump.GetItem(i);
    if(item.itemid=it&&item.itemamount==count)
      return item.itemserial;
  }
  return 0;
}

void CClientUO::MoveItemsG2G(unsigned containerserialfrom,const Client::ItemType &it,unsigned containerserialto)
{
  Packets::scsOpenGump gump;
  {
    Packets::scsUseItem ui;
    ui.itemserial=containerserialfrom;
    CommandControl ssc(CreateInterceptor(Packets::scOpenGump));
    SendToServer(&ui,sizeof(ui));
    ssc->Catch();
    ssc->SetClear();
    gump=ssc->Buffer();
  }

  for(int i=0;i<gump.GetItemsCount();i++)
  {
    if(it.oneof((unsigned short)gump.GetItem(i)->itemid))
    {
      Packets::scsReqObjGet cmd1;
      cmd1.itemserial=gump.GetItem(i)->itemserial;
      cmd1.amount=gump.GetItem(i)->itemamount;
      SendToServer(&cmd1,sizeof(cmd1));
      Packets::scsReqObjDrop cmd;
      cmd.itemserial=gump.GetItem(i)->itemserial;
      cmd.z=0;
      cmd.x=-1;
      cmd.y=-1;
      cmd.containerserial=containerserialto;
      SendToServer(&cmd,sizeof(cmd));
    }
  }
}

unsigned CClientUO::UseItemGump(unsigned itemserialtouse, const UnsignedList &serials, const ShortList &items)
{
  //todo сделать чтобы short items являлось типом предмета , и определять его
  //  позицию для SelecOption через открытый гамп

  //25 - new item
  // 7c - display gump
  // 7d chouse

  Packets::scsDisplayGump gump;

  Packets::scsUseItem ui;
  ui.itemserial=itemserialtouse;

  Packets::scsTarget target;
  Server::ServerByte * ppd=(Server::ServerByte *)&ui;
  int  ppdlen=sizeof(ui);

  // 6c 00 00 00 00 2c 01 40 00 1d 34 00 64 00 64 00 00 1b dd 01 00 00 00 00 a5 b4 43 00 64 00 59 01  l....,.@..4.d.d...Э.....ҐґC.d.Y.
  // a1 00 00 00 b8 cc 5e 01 c0 af 47 00 81 00 00 00 c7 af 47 00 00 00 00 00 a9 b2 66 01 ac 92 80 7c  Ў...ёМ^.АЇG.Ѓ...ЗЇG.....©Іf.¬’Ђ|
  // 00 00 00 00 81 00 00 00 6f 00 00 00 5f f8 47 00 10 4e 4c 21 a9 b2 66 01 77 b2 66 01 fd ce d4 77  ....Ѓ...o..._шG..NL!©Іf.wІf.эОФw

  for(UnsignedList::const_iterator i=serials.begin();i!=serials.end();i++)
  {
    {
      CommandControl ssc(CreateInterceptor(Packets::scTarget));
      Sleep(g_slowexchange);
      SendToServer(ppd,ppdlen);
      ssc->Catch();
      target=ssc->Buffer();
      ssc->SetClear();
    }
    target.type=0;
    target.charid=0x2c;
    target.crime=0;
    target.itemserial=*i;
    target.worldx=0;
    target.worldy=0;
    target.byte=0;
    target.worldz=0;
    target.itemid=0;
    ppd=(Server::ServerByte *)&target;
    ppdlen=sizeof(target);
  }

  {
    CommandControl ssc(CreateInterceptor(Packets::scDisplayGump));
    Sleep(g_slowexchange);
    SendToServer(ppd,ppdlen);
    ssc->Catch();
    gump=ssc->Buffer();
    ssc->SetClear();
  }

  ShortList::const_iterator i=items.begin();
  while(1)
  {
    Packets::scsChoiseOption cmd;
    cmd.gumpser=gump.GetGumpSer();
    cmd.gumpid=gump.GetGumpId();
    cmd.itemindex=gump.FindIndex(*i);
    cmd.itemid=*i;

    // не найден такой выбор
    if(cmd.itemindex==-1)
      return 0;

    if(i==items.end()-1)
    {// жду результат
      Packets::scsAddObject addobj;
      {
        CommandControl ssc(CreateInterceptor(Packets::scAddObject));
        Sleep(g_slowexchange);
        SendToServer(&cmd,sizeof(cmd));
        switch((Server::ServerInt)ssc->Catch())
        {
        case Packets::scAddObject:
          addobj=*(Packets::scsAddObject *)ssc->Buffer();
          break;
        }
      }
      return addobj.itemserial;
    }else
    {// если открывать еще что-то нужно то жду открытия
      CommandControl ssc(CreateInterceptor(Packets::scDisplayGump));
      Sleep(g_slowexchange);
      SendToServer(&cmd,sizeof(cmd));
      ssc->Catch();
      gump=ssc->Buffer();
      ssc->SetClear();
    }
    i++;
  }

}

unsigned CClientUO::MoveItemG2G(unsigned containerserialfrom,const Client::ItemType &it,unsigned containerserialto, int count)
{
  Packets::scsOpenGump pack;
  {
    Packets::scsUseItem ui;
    ui.itemserial=containerserialfrom;
    CommandControl ssc(CreateInterceptor(Packets::scOpenGump));
    SendToServer(&ui,sizeof(ui));
    ssc->Catch();
    ssc->SetClear();
    pack=ssc->Buffer();
  }

  for(int i=0;i<pack.GetItemsCount();i++)
  {
    if(it==pack.GetItem(i)->itemid)
    {
      Packets::scsReqObjGet cmd1;
      cmd1.itemserial=pack.GetItem(i)->itemserial;
      if(count==-1)
        cmd1.amount=pack.GetItem(i)->itemamount;
      else
        cmd1.amount=count;
      SendToServer(&cmd1,sizeof(cmd1));
      Packets::scsReqObjDrop cmd;
      cmd.itemserial=pack.GetItem(i)->itemserial;
      cmd.z=0;
      cmd.x=0;
      cmd.y=0;
      cmd.containerserial=containerserialto;
      SendToServer(&cmd,sizeof(cmd));

      return pack.GetItem(i)->itemserial;
    }
  }

  return 0;
}

unsigned CClientUO::MoveItem(unsigned itemwhat,unsigned containerserialto, int count)
{
  Packets::scsReqObjGet cmd1;
  cmd1.itemserial=itemwhat;
  cmd1.amount=count;
  SendToServer(&cmd1,sizeof(cmd1));
  Packets::scsReqObjDrop cmd;
  cmd.itemserial=itemwhat;
  cmd.z=0;
  cmd.x=-1;
  cmd.y=-1;
  cmd.containerserial=containerserialto;
  SendToServer(&cmd,sizeof(cmd));
  return 0;
}

unsigned CClientUO::PinchItem(const Client::ItemType &it,unsigned containerserialfrom,int count)
{
  Packets::scsOpenGump gump;
  {
    Packets::scsUseItem ui;
    ui.itemserial=containerserialfrom;
    CommandControl ssc(CreateInterceptor(Packets::scOpenGump));
    Sleep(g_slowexchange);
    SendToServer(&ui,sizeof(ui));
    ssc->Catch();
    ssc->SetClear();
    gump=ssc->Buffer();
  }

  unsigned itemwhat=0;
  for(int i=0;i<gump.GetItemsCount();i++)
  {
    if(it.oneof(gump.GetItem(i)->itemid) && gump.GetItem(i)->itemamount>=count)
    {
      itemwhat=gump.GetItem(i)->itemserial;
      break;
    }
  }

  Packets::scsReqObjGet cmd1;
  cmd1.itemserial=itemwhat;
  cmd1.amount=count;
  Sleep(g_slowexchange);
  SendToServer(&cmd1,sizeof(cmd1));
  Packets::scsReqObjDrop cmd;
  cmd.itemserial=itemwhat;
  cmd.z=0;
  cmd.x=100;
  cmd.y=100;
  cmd.containerserial=containerserialfrom;

  Packets::scsAddObject pack1;
  {
    CommandControl ssc(CreateInterceptor(Packets::scAddObject));
    Sleep(g_slowexchange);
    SendToServer(&cmd,sizeof(cmd));
    ssc->Catch();
    ssc->SetClear();
    pack1=ssc->Buffer();
  }
  return pack1.itemserial;
}

unsigned CClientUO::GetContainerItem(const Client::ItemType &it,unsigned containerserialfrom)
{
  Packets::scsOpenGump gump;
  {
    Packets::scsUseItem ui;
    ui.itemserial=containerserialfrom;
    CommandControl ssc(CreateInterceptor(Packets::scOpenGump));
    SendToServer(&ui,sizeof(ui));
    ssc->Catch();
    ssc->SetClear();
    gump=ssc->Buffer();
  }

  unsigned itemwhat=0;
  for(int i=0;i<gump.GetItemsCount();i++)
  {
    if(it.oneof(gump.GetItem(i)->itemid))
    {
      itemwhat=gump.GetItem(i)->itemserial;
      break;
    }
  }


  return itemwhat;
}

void CClientUO::CharSpeech(const char* p)
{
  WaitForClientInit();

  SendText(p);
  SendKey(VK_RETURN);
  return;
}

void CClientUO::ReadConfig(const char* buf)
{
  {
    CDocAddin<CInterprocClientAddinDoc> doc;
    CPacketMasterType packet=doc.LoadXML((const char*)buf);

    // must be
    CConfigType &config=packet.GetConfig();
    std::string value=config.GetEmulation();
    if(value=="client203")
      EmulateCrypt(ClientIdentify::client203);
    else if(value=="nocrypt")
      DisableClientCrypt();
    else if(value=="clientSphereClient")
      EmulateCrypt(ClientIdentify::clientSphereClient);

    value=config.GetSecure();
    if(value=="enable")
      EnableSecure();

    PatchClient();
  }
  {
    CPacketSlaveType packet;
    CSchemaBase64Binary bb("");
    packet.AddResult(bb);
    Send(packet);
  }
}

void CClientUO::VendorOffer(const char*name,const ClientGameCommands::Vendors::Items&itms)
{
  Packets::scsShopSell pack;
  {
    Packets::scsSpeakingUnicode ui((std::string(name)+" sell").c_str());
    CommandControl ssc(CreateInterceptor(Packets::scShopSell));
    Sleep(g_slowexchange);
    SendToServer(&ui.m_packet.front(),ui.m_packet.size());
    ssc->Catch();
    ssc->SetClear();
    pack=ssc->Buffer();
  }

  unsigned serial=pack.m_serial;
  Packets::scsShopOffer::Items items;

  for(int i=0;i<pack.m_items.size();i++)
  {
    const Packets::scsShopSell::Item &sell=pack.m_items[i];
    for(ClientGameCommands::Vendors::Items::const_iterator i=itms.begin();i!=itms.end();i++)
    {
      if(i->item.oneof(sell.item))
      {
        Packets::scsShopOffer::Item item;
        item.Item_Amount=i->amount;
        item.Item_Serial=sell.serial;
        items.push_back(item);
      }
    }
  }

  Packets::scsShopOffer cmd1(serial,items);
  const Packets::Packet & pk=cmd1;
  Sleep(g_slowexchange);
  SendToServer(&pk.front(),pk.size());
}

unsigned CClientUO::GetEquippedItem(const Client::ItemType &it,const char* name)
{
  mobinfo* m=FindChar(name);
  if(m==0)
    return 0;
  for(MOBPaper::Items::iterator i=m->paper.items.begin();i!=m->paper.items.end();i++)
  {
    if(it.oneof(i->item))
    {
      return i->serial;
    }
  }
  return 0;
}
