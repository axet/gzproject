#include "clientuo.h"

/// Класс управления клиентом 

class Client400e:public CClientUO
{
public:
  virtual void PathClient();
  virtual void ZeroFirstPacket();
  virtual void DisableClientCrypt();
  virtual HWND GetWindow();
};
