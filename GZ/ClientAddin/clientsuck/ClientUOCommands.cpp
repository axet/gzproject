#include "stdafx.h"
#include "clientuocommands.h"

#include <errorreport/errorreport.h>

/// время которое даеться клиенту на перехват команды
const int g_catchcommandwait=30000;

// CClientUOCommands {

CClientUOCommands::CClientUOCommands():m_retvalclear(false),m_mshits(-1),m_msweight(-1)
{
}

CClientUOCommands::~CClientUOCommands()
{ 
}

int CClientUOCommands::StatsGetMana()
{
  return m_msmana;
}

int CClientUOCommands::StatsGetStam()
{
  return m_msstam;
}

void CClientUOCommands::FromServer(CMemMngr* client,va_list args)
{
  CClientUOCommands* _this=dynamic_cast<CClientUOCommands*>(client);

  Server::ServerByte* p;
  p=va_arg(args,Server::ServerByte*);

  _this->HaveCommand(p,0);

  switch(*(unsigned char*)p)
  {
  case Packets::scOpenGump:
    {
      ;
    }
    break;
  case Packets::scLoginConfirm:
    {
      Packets::scsLoginConfirm* pack=(Packets::scsLoginConfirm*)p;
      _this->SelectedPath(WorldCord(pack->x,pack->y,0));
    }
    break;
  case Packets::scMoveItem:
    {
      Packets::scsMoveItem *pack=(Packets::scsMoveItem *)p;
      switch(pack->Packet_Size)
      {
      case sizeof(Packets::scsMoveItem):
        {
          Packets::scsMoveItem *pack=(Packets::scsMoveItem *)p;
          _this->UpdateObject(pack->Item_Serial,Client::ItemType(pack->Item_ID),WorldCord((unsigned short)pack->worldx,(unsigned short)pack->worldy,(unsigned short)pack->worldz),pack->Item_Amount,pack->stack,pack->direction,pack->hue,pack->status);
        }
        break;
      case sizeof(Packets::scsMoveItemSmall):
        {
          Packets::scsMoveItemSmall *pack=(Packets::scsMoveItemSmall *)p;
          _this->UpdateObject(pack->Item_Serial,Client::ItemType(pack->Item_ID),WorldCord((unsigned short)pack->worldx,(unsigned short)pack->worldy,(unsigned short)pack->worldz),0,0,0,0,0);
        }
        break;
      }
    }
    break;
  case Packets::scDestroyObject:
    {
      Packets::scsDestroyObject *pack=(Packets::scsDestroyObject *)p;
      _this->RemoveObject(pack->serial);
    }
    break;
  case Packets::scMobileStats:
    {
      Packets::scsMobileStats* pack=(Packets::scsMobileStats*)p;

      _this->UpdateObject(pack->serial,MOB(pack->charname,pack->hits,pack->maxhits,pack->grender,
        pack->str,pack->dex,pack->inta,pack->stam,pack->maxstam,pack->mana,pack->maxmana,pack->gold,
        pack->armor,pack->weight));

      _this->m_mshits=pack->hits;
      _this->m_msweight=pack->weight;
      _this->m_msstam=pack->stam;
      _this->m_msmana=pack->mana;
      if(_this->m_charname.empty())
      {
        _this->m_charname=pack->charname;

        _this->ConnectionEstablished(_this->m_login.c_str(),_this->m_password.c_str(),_this->m_charname.c_str());
      }
    }
    break;
  case Packets::scEquippedMOB:
    {
      Packets::scsEquippedMOB pack((const char*)p);
      MOBPaper pp;
      pp.amount=pack.Item_Amount;
      pp.direction=pack.Direction;
      pp.item=pack.Item_ID;
      pp.notoriety=pack.Notoriety;
      pp.skincolor=pack.Skin_Color;
      pp.status=pack.Status;
      pp.w=WorldCord(pack.X,pack.Y,pack.Z);
      for(std::vector<Packets::scsEquippedMOB::Item>::iterator i=pack.m_items.begin();i!=pack.m_items.end();i++)
      {
        MOBPaper::Item it;
        it.color=i->Item_Color;
        it.serial=i->Serial;
        it.item=i->Item_ID;
        it.layer=i->Item_Layer;
        pp.items.push_back(it);
      }
      _this->UpdateObject(pack.Item_Serial,pp);
    }
    break;
  case Packets::scRenameMOB:
    break;
  case Packets::scMOBName:
    break;
  case Packets::scSpeaking:
    {
      Packets::scsSpeaking::packet1_tag *pack=(Packets::scsSpeaking::packet1_tag *)p;
      _this->UpdateObject(pack->charserial,pack->charid,pack->name);
    }
    break;
  case Packets::scNakedMOB:
    {
      Packets::scsNakedMOB* pack=(Packets::scsNakedMOB*)p;
      _this->UpdateObject(pack->serial,MOBMove(Client::ItemType(pack->id),WorldCord(pack->x,pack->y,pack->z),
        pack->dir,pack->skincol,pack->status,pack->noto));
    }
    break;
  }
}

void CClientUOCommands::FromServerLeave(CMemMngr* client,int* retval)
{
  CClientUOCommands* _this=dynamic_cast<CClientUOCommands*>(client);
  if(_this->m_retvalclear)
  {
    _this->m_retvalclear=false;
    _this->ProcessPacket(true);
    *retval=1;
  }
}

CClientUOCommands::SentryCommand* CClientUOCommands::CreateInterceptor(const CmdSet& c)
{
  sentrysc sc(m_sentrysetcs);
  SentryCommand *ss=new SentryCommand(this,c);
  m_sentryset.insert(m_sentryset.end(),ss);
  return ss;
}

CClientUOCommands::SentryCommand* CClientUOCommands::CreateInterceptor(Packets::EthCommand e)
{
  sentrysc sc(m_sentrysetcs);
  SentryCommand *ss=new SentryCommand(this,e);
  m_sentryset.insert(m_sentryset.end(),ss);
  return ss;
}

void CClientUOCommands::RemoveInterceptor(SentryCommand* ss)
{
  m_sentrysetcs.Lock();
  for(SentrySet::iterator i=m_sentryset.begin();i!=m_sentryset.end();i++)
  {
    if(*i==ss)
    {
      m_sentryset.erase(i++);
      delete ss;
      break;
    }
  }
  m_sentrysetcs.Unlock();
}

int CClientUOCommands::StatsGetHits()
{
  return m_mshits;
}

int CClientUOCommands::StatsGetWeight()
{
  return m_msweight;
}

void CClientUOCommands::ProcessPacket(bool enable)
{
  throw std::exception("ProcessPacket not supported");
}

void CClientUOCommands::HaveCommand(Server::ServerByte* buf,int bufsize)
{
  m_sentrysetcs.Lock();
  SentrySet sentryset=m_sentryset;
  //for(SentrySet::iterator i=sentryset.begin();i!=sentryset.end();i++)
  //  (*i)->Lock();
  m_sentrysetcs.Unlock();
  for(SentrySet::reverse_iterator i=sentryset.rbegin();i!=sentryset.rend();i++)
  {
    SentryCommand *sc=*i;
    if(sc->HaveCommand(buf))
    {
      sc->Lock();
      if(sc->NeedClear())
      {
        m_retvalclear=true;
        ProcessPacket(false);
      }
      if(sc->NeedReplace())
      {
        const Packets::Packet &p=sc->GetReplace();
        std::copy(p.begin(),p.end(),buf);
      }
      sc->Unlock();
    }
  }
}

void CClientUOCommands::FromClient(unsigned char* buf,int bufsize)
{
  HaveCommand(buf,bufsize);

  switch(buf[0])
  {
  case Packets::scGodCommand:
    {
      ;
    }
    break;
  case Packets::scChoiseOption:
    {
      Packets::scsChoiseOption *pack=(Packets::scsChoiseOption *)buf;
      SelectedOption((unsigned short)pack->itemid);
    }
    break;
  case Packets::scUseItem:// item use
    {
      Packets::scsUseItem *pack=(Packets::scsUseItem *)buf;
      SelectedItem((unsigned int)pack->itemserial);
    }
    break;
  case Packets::scTarget:// target
    {
      Packets::scsTarget *pack=(Packets::scsTarget *)buf;
      switch((*pack).type)
      {
      case 1:
        SelectedGround(WorldCord((unsigned short)pack->worldx,(unsigned short)pack->worldy,(unsigned short)pack->worldz),(unsigned int)pack->itemid);
        break;
      case 0:
        SelectedItem((unsigned int)pack->itemserial);
        break;
      }
    }
    break;
  case Packets::scMoveItem:
    {
      Packets::scsMoveItem *pack=(Packets::scsMoveItem *)buf;
      SelectedItem((unsigned int)pack->Item_Serial);
    }
    break;
  case Packets::scReqObjDrop:
    {
      Packets::scsReqObjDrop *pack=(Packets::scsReqObjDrop *)buf;
      SelectedItem((unsigned int)pack->containerserial);
    }
    break;
  case Packets::scLogin:
    {
      Packets::scsLogin *pack=(Packets::scsLogin *)buf;
      m_login=pack->name;
      m_password=pack->password;
    }
    break;
  case Packets::scPreLogin:
    {
      Packets::scsPreLogin *pack=(Packets::scsPreLogin*)buf;
      m_charname=pack->Character_Name;

      ConnectionEstablished(m_login.c_str(),m_password.c_str(),m_charname.c_str());
    }
    break;
  }
}

void CClientUOCommands::FromClient(CMemMngr* client,va_list args)
{
  CClientUOCommands* pclt=dynamic_cast<CClientUOCommands*>(client);

  unsigned char* input=va_arg(args,unsigned char*);
  int size=va_arg(args,int);

  pclt->CClientUOCommands::FromClient(input,size);
}


bool CClientUOCommands::GetLoginData(char*name,int namemax,char* pass,int passmax,char* charname,int charnamemax)
{
  if(m_login.empty()&&m_password.empty())
    return false;

  strncpy(name,m_login.c_str(),namemax);
  strncpy(pass,m_password.c_str(),passmax);
  strncpy(charname,m_charname.c_str(),charnamemax);

  return true;
}

// } CClientUOCommands


// CClientUOCommands::SentryCommand {

bool CClientUOCommands::SentryCommand::HaveCommand(void* cmdbuf)
{
  {
    sentrysc sc(m_cmdcs);
    CmdSet::iterator i=m_cmdset.find(Packets::EthCommand(cmdbuf));
    if(i==m_cmdset.end())
      return false;
  }
  m_cmdbuf=cmdbuf;
  SetEvent(m_waitforClient);
  WaitForSingleObject(m_waitforServer,-1);
  return true;
}

const Packets::Packet& CClientUOCommands::SentryCommand::GetReplace()
{
  return m_replacepacket;
}

Packets::EthCommand CClientUOCommands::SentryCommand::Catch(Packets::EthCommand anticipatedcommand)
{
  if(WaitForSingleObject(m_waitforClient,g_catchcommandwait)==WAIT_TIMEOUT)
    throw SentryCommandTimeout(m_cmdset);

  if(anticipatedcommand!=Packets::scNone)
  {
    if(anticipatedcommand!=Packets::EthCommand(m_cmdbuf))
      throw NotAnticipatedCommand(anticipatedcommand,Packets::EthCommand(m_cmdbuf),m_cmdset);  
  }
  return Packets::EthCommand(m_cmdbuf);
}

Server::ServerByte* CClientUOCommands::SentryCommand::Buffer()
{
  return (Server::ServerByte*)m_cmdbuf;
}

CClientUOCommands::SentryCommand::~SentryCommand()
{
  Lock();

  CloseHandle(m_waitforClient);
  CloseHandle(m_waitforServer);

  Unlock();
}

CClientUOCommands::SentryCommand::SentryCommand(CClientUOCommands* p,Packets::EthCommand e)
{
  sentrysc sc(m_cmdcs);
  Initialize(p);
  m_cmdset.insert(m_cmdset.end(),e);
}

CClientUOCommands::SentryCommand::SentryCommand(CClientUOCommands* p,const CmdSet & cmd)
{
  sentrysc sc(m_cmdcs);
  Initialize(p);
  m_cmdset=cmd;
}

void CClientUOCommands::SentryCommand::Initialize(CClientUOCommands* p)
{
  m_p=p;
  m_clear=false;
  m_waitforClient=CreateEvent(0,TRUE,FALSE,0);
  m_waitforServer=CreateEvent(0,TRUE,FALSE,0);
}

bool CClientUOCommands::SentryCommand::NeedClear()
{
  return m_clear;
}

void CClientUOCommands::SentryCommand::SetClear()
{
  m_clear=true;
}

void CClientUOCommands::SentryCommand::SetReplace(Packets::Packet& p)
{
  m_replacepacket=p;
}

bool CClientUOCommands::SentryCommand::NeedReplace()
{
  return m_replacepacket.IsValidPakcet();
}

void CClientUOCommands::SentryCommand::Lock()
{
  m_cmdcs.Lock();
}

void CClientUOCommands::SentryCommand::Unlock()
{
  m_cmdcs.Unlock();
}

void CClientUOCommands::SentryCommand::Release()
{
  SetEvent(m_waitforServer);
  m_p->RemoveInterceptor(this);
}

// } CClientUOCommands::SentryCommand

// SentryCommandTimeout {

CClientUOCommands::SentryCommand::SentryCommandTimeout::SentryCommandTimeout(CmdSet& s)
{
  std::string str;
  for(CmdSet::iterator i=s.begin();i!=s.end();i++)
  {
    str+=EthInterface::m_messages[(Server::ServerInt)*i].name;
    str+="\n";
  }
  exception::operator =(exception(str.c_str()));
}

// } SentryCommandTimeout

// SentryCommandTimeout {

CClientUOCommands::SentryCommand::NotAnticipatedCommand::NotAnticipatedCommand(Packets::EthCommand e,Packets::EthCommand e2,CmdSet& s)
{
  ErrorReport er("Not anticipated command");
  er+=_bstr_t("Anticipated command: ")+EthInterface::m_messages[e].name;
  er+=_bstr_t("Received command: ")+EthInterface::m_messages[e2].name;
  _bstr_t lst;
  for(CmdSet::iterator i=s.begin();i!=s.end();i++)
  {
    lst+=EthInterface::m_messages[(Server::ServerInt)*i].name;
    lst+="; ";
  }
  er+=_bstr_t("In list: ")+lst;
  exception::operator =(exception(er));
}

// } SentryCommandTimeout


// CommandControl {

CClientUOCommands::CommandControl::CommandControl(SentryCommand *p)
{
  m_sc=p;
}

CClientUOCommands::CommandControl::~CommandControl()
{
  Release();
}

void CClientUOCommands::CommandControl::Release()
{
  if(m_sc!=0)
  {
    m_sc->Release();
    m_sc=0;
  }
}

CClientUOCommands::SentryCommand* CClientUOCommands::CommandControl::operator ->()
{
  return m_sc;
}

// } CommandControl
