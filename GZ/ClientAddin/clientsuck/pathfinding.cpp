#include "stdafx.h"
#include "pathfinding.h"

#include "setup.h"

/// время в течении которого клиент ждет подтверждения о завершении перемещения персонажа
const int g_pathfindingtimeout=30000;

typedef void (__stdcall *_PathFinding) (int x,int y,int z);

CPathFinding::CPathFinding()
{
  m_pathfinding=CreateEvent(0,TRUE,TRUE,0);

}

CPathFinding::~CPathFinding()
{
  CloseHandle(m_pathfinding);
}

void CPathFinding::PathFindingTerminate()
{
  SetEvent(m_pathfinding);
}

void CPathFinding::PathFindingRun(CPathFinding*p)
{
  p->PathFinding(WorldCord(p->m_x,p->m_y,p->m_z));
}

int CPathFinding::linelen(int x1,int y1,int x2,int y2)
{
  return (int)sqrt(pow((double)x1-x2,2)+pow((double)y1-y2,2));
}

void CPathFinding::CharMoveTo(WorldCord w2)
{
  int step=5;

  WorldCord w1=GetCharCoord();

  try
  {
    int startlinelen;
    while((startlinelen=linelen(w1.x,w1.y,w2.x,w2.y))>step)
    {
      int x,y,z;

      float d=((float)step)/startlinelen;
      x=(int)(w1.x+(w2.x-w1.x)*d);
      y=(int)(w1.y+(w2.y-w1.y)*d);
      z=w2.z;

      DoMoveTo(WorldCord(x,y,z));

      // получилось сделать хоть один шаг?
      WorldCord wnew;
      wnew=GetCharCoord();
      if(abs(w1.x-wnew.x)==0&&abs(w1.y-wnew.y)==0)
      {// нет, увеличивую шаг
        step=(int)(step*1.5);
        if(step>=startlinelen || // и если он уже равен длинне исходного пути, делаю последний...
          step >= 40) // или он больше 40 клеток, это больше 4 экранов.
          break;
      }

      w1.x=wnew.x;
      w1.y=wnew.y;
    }

    DoMoveTo(w2);
  }catch(PathNotFounded&)
  {
    Objects sl=FindItem(Client::ItemType(Client::ItemType::itDoorClose_s));
    for(Objects::iterator i=sl.begin();i!=sl.end();i++)
    {
      try
      {
        WorldCord wcur;
        wcur=GetCharCoord();
        int len=std::sqrt(std::pow((double)i->second.w.x-wcur.x,2)+std::pow((double)i->second.w.x-wcur.x,2));
        if(len<=8)
        {
          DoMoveTo(i->second.w);
          UseItem(i->first);
        }
      }catch(PathNotFounded&)
      {
      }
    }
    CharMoveTo(w2);
  }
}

void CPathFinding::DoMoveTo(WorldCord w)
{
  ResetEvent(m_pathfinding);

  m_x=w.x;m_y=w.y;m_z=w.z;

  WorldCord wstart;
  wstart=GetCharCoord();

  SleppingTask((CSleppingTask::MYCALL)PathFindingRun,(DWORD)this);

  if(!PathFindingIsStarted())
  {
    throw PathNotFounded();// маршрут не найден
  }
  if(WaitForSingleObject(m_pathfinding,g_pathfindingtimeout)==WAIT_TIMEOUT)
  {
    throw std::exception("WaitForSingleObject g_packetreceivetimeout");
  }
  WorldCord wend;
  wend=GetCharCoord();

  if(wstart.x==wend.x&&wstart.y==wend.y)
    return;

  // если персонаж сместился но не дошел еще раз вызывать поиск пути

  if(abs(w.x-wend.x)>1||abs(w.y-wend.y)>1)
  {
    DoMoveTo(w);
  }
}

void CPathFinding::CharMoveToChar(const char*name)
{
  ClientObjects::mobinfo* q=FindChar(name);
  CharMoveTo(q->move.w);
}
