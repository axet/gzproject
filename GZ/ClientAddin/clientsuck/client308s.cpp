#include "stdafx.h"
#include "client308s.h"

#define PATCH_B(x) \
{ \
  unsigned long l=x; \
  unsigned char yy[]=

#define PATCH_E ; \
  PatchProgram(l,yy,sizeof(yy)); \
}

#define PATCH(x,y) {unsigned char yy[]={y};PatchProgram(x,yy,sizeof(yy));}

Client308S::Client308S()
{
  m_readyforlogin=CreateEvent(0,FALSE,FALSE,0);
}

Client308S::~Client308S()
{
  CloseHandle(m_readyforlogin);
}

//void Client308S::WaitForClientInit()
//{
//  WaitForSingleObject(m_readyforlogin,-1);
//}

void Client308S::ZeroFirstPacket()
{
  // first packet
  //.text:00417FB2 8B 0D A4 23 DC 00     mov    ecx, dword_DC23A4
  //.text:00417FB8 51                    push   ecx
  //.text:00417FB9 E8 8C 0A 10 00        call   WSOCK32_8
  PATCH_B(0x417fb2) {0x31,0xc0,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90} PATCH_E;
}

void Client308S::DisableClientCrypt()
{
  // login crypt
  //.text:004190AF 32 D0                 xor    dl, al
  PATCH_B(0x004190AF) {0x88,0xc2} PATCH_E;

  // game crypt
  //00430308 32 D0            xor         dl,al 
  PATCH_B(0x00430308) {0x90,0x90} PATCH_E;

  // from server - decrypt
  //004185B3 32 DA            xor         bl,dl 
  PATCH_B(0x004185B3) {0x90,0x90} PATCH_E;

  CClientUO::DisableClientCrypt();
}

HWND Client308S::GetWindow()
{
  return *(HWND*)0x00E52B70;
}

void Client308S::CatchFromClient(CMemMngr* memmngr,va_list args)
{
  Client308S* _this=dynamic_cast<Client308S*>(memmngr);

  typedef unsigned short (*GetPacketLength_sub_42E450)(const unsigned char* p);
  GetPacketLength_sub_42E450 sub_42E450=(GetPacketLength_sub_42E450)0x0042E450;

  unsigned char* buf;
  buf=va_arg(args,unsigned char*);

  unsigned short bufsize=sub_42E450(buf);

  _this->FromClient(buf,bufsize);
}


void Client308S::GetCharCoord(int*x,int*y,int *z)
{
  *x=*(unsigned*)0x00D9BA3c;
  *y=*(unsigned*)0x00D9BA38;
}

void Client308S::CatchReadyForLogin(CMemMngr* memmngr,int *retval)
{
  Client308S* _this=dynamic_cast<Client308S*>(memmngr);

  SetEvent(_this->m_readyforlogin);
}

void Client308S::PatchClient()
{
  CatchRetFunction(0x004D6C70,0,SetNewPlayersCoord);

  CatchRetFunction(0x5031c0,WindowProc);
  CatchRetFunction(0x00419140,FromServer,FromServerLeave);

  CatchRetFunction(0x004FFE00,0,CatchReadyForLogin);

  CatchRetFunction(0x00419000,CatchFromClient);

  CClientUO::PatchClient();
}
