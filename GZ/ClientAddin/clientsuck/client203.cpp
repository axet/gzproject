#include "stdafx.h"
#include "client203.h"

CClient203::CClient203()
{
}

CClient203::~CClient203()
{
}

void CClient203::SendToServer(const void* buf,int bufsize)
{
  __asm
  {
    mov eax,0xCC516C;
    mov ecx,[eax];
    lea eax,bufsize;
    push [eax];
    lea eax,buf;
    push [eax];

    mov eax,0x4C0E30;
    call eax;
  };
}

void CClient203::DisableClientCrypt()
{
  unsigned char path[]={0x90,0x90};

  PatchProgram(0x004C0E90,path,sizeof(path));
  PatchProgram(0x0040F87C,path,sizeof(path));
  PatchProgram(0x0040F96C,path,sizeof(path));
  PatchProgram(0x004C0E8E,"\x8a\x13",2);
  PatchProgram(0x004C888C,path,sizeof(path));
}

void CClient203::ProcessPacket(bool enable)
{
  int adr=0x004C2219;
  int adr2=0x004C21F8;
  int adr3=0x004C20C0;

  DWORD old;
  VirtualProtect((void*)adr,2,PAGE_EXECUTE_READWRITE,&old);
  VirtualProtect((void*)adr2,2,PAGE_EXECUTE_READWRITE,&old);
  //VirtualProtect((void*)adr3,4,PAGE_EXECUTE_READWRITE,&old);

  if(enable)
  {
    *(unsigned short*)adr=0xb53D;
    *(unsigned short*)adr2=0xb53D;
    //*(unsigned*)adr3=0x004c818b;
  }else
  {//off
    *(unsigned short*)adr=0x003D;
    *(unsigned short*)adr2=0x003D;
    //*(unsigned*)adr3=0x000004c2;
  }
}

HWND CClient203::GetWindow()
{
  return *(HWND*)0xD58724;
}

WorldCord CClient203::GetCharCoord()
{
  WorldCord w;
  w.x=*(unsigned*)0x00CC0804;
  w.y=*(unsigned*)0x00CC0800;
  w.z=*(unsigned*)0x00CC07FC;
  return w;
}

void CClient203::PathFinding(WorldCord w)
{
  typedef void (__cdecl *_PathFindingNetCall) (int x,int y,int z);
  _PathFindingNetCall PathFindingNetCall=(_PathFindingNetCall)0x0047DD10;
  // 0x00541B20 - PathFindingNetCallTerminate
  *(int*)0x00541B20=1;
  PathFindingNetCall(w.x,w.y,w.z);
}

bool CClient203::PathFindingIsStarted()
{
  // 0xCC9380 - PathFindingNetCallRetCode
  return *(unsigned*)0xCC9380!=0;
}

void CClient203::PathFindingTerminate(CMemMngr* client,va_list args)
{
  CClient203* pclt=dynamic_cast<CClient203*>(client);
  pclt->CPathFinding::PathFindingTerminate();
}

void CClient203::PatchClient()
{
  CatchRetFunction(0x004C0E30,FromClient);
  CatchRetFunction(0x00477C80,0,SetNewPlayersCoord);
  CatchRetFunction(0x004C4990,CatchLogUni);
  CatchRetFunction(0x004C4A00,CatchLog);

  PatchProgram(0x004D5457,"\xeb",1);
  PatchProgram(0x004D56DD,"\xeb",1);

  //gui
  CatchRetFunction(0x004D5C60,0,CatchWindowCreate);

  // искать  "cmp    byte ptr [eax], 33h"
  // найдеться процедура ProcessPacket
  
  // требуеться для отлова данных с сервера
  // подключена возможность игнорирования пакетов для клиента
  CatchRetFunction(0x004C20C0,FromServer,FromServerLeave);

  //login
  CatchRetFunction(0x004C1110,ClientDisconnected);
  CatchRetFunction(0x00491030,LoggedInPass);

  //path finding
  CatchRetFunction(0x004A64F0,PathFindingTerminate);

  // искать через spy
  CatchRetFunction(0x004D6380,CSleppingTask::WindowProc);

  CClientUO::PatchClient();
}
