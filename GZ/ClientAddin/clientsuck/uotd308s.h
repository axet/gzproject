#include "clientuo.h"

/// Класс управления клиентом UOTD

class UOTD308S:public CClientUO
{
public:
  virtual void PathClient();
  virtual HWND GetWindow();
  void DisableClientCrypt();
};
