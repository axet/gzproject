# Microsoft Developer Studio Project File - Name="ClientAddinConfig" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104


CFG=ClientAddinConfig - Win32 Unicode Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ClientAddinConfig.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ClientAddinConfig.mak" CFG="ClientAddinConfig - Win32 Unicode Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ClientAddinConfig - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "ClientAddinConfig - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "ClientAddinConfig - Win32 Unicode Release" (based on "Win32 (x86) Static Library")
!MESSAGE "ClientAddinConfig - Win32 Unicode Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ClientAddinConfig - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /D "_LIB" /D "_AFXDLL" /MD /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /D "_LIB" /D "_AFXDLL" /MD /nologo /W3 /GR /GX /O2 /I "..\Altova" /I "..\AltovaXML" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c

# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo


!ELSEIF  "$(CFG)" == "ClientAddinConfig - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /D "_LIB" /D "_AFXDLL" /MDd /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /D "_LIB" /D "_AFXDLL" /MDd /nologo /W3 /Gm /GR /GX /ZI /Od /I "..\Altova" /I "..\AltovaXML" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c

# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo


!ELSEIF  "$(CFG)" == "ClientAddinConfig - Win32 Unicode Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "UnicodeRelease"
# PROP BASE Intermediate_Dir "UnicodeRelease"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "UnicodeRelease"
# PROP Intermediate_Dir "UnicodeRelease"
# PROP Target_Dir ""
# ADD BASE CPP /D "_LIB" /D "_AFXDLL" /MD /nologo /W3 /GR /GX /O2 /I "..\Altova" /I "..\AltovaXML" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /D "_LIB" /D "_AFXDLL" /MD /nologo /W3 /GR /GX /O2 /I "..\Altova" /I "..\AltovaXML" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_UNICODE" /D "UNICODE" /Yu"stdafx.h" /FD /c

# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo


!ELSEIF  "$(CFG)" == "ClientAddinConfig - Win32 Unicode Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "UnicodeDebug"
# PROP BASE Intermediate_Dir "UnicodeDebug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "UnicodeDebug"
# PROP Intermediate_Dir "UnicodeDebug"
# PROP Target_Dir ""
# ADD BASE CPP /D "_LIB" /D "_AFXDLL" /MDd /nologo /W3 /Gm /GR /GX /ZI /Od /I "..\Altova" /I "..\AltovaXML" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /D "_LIB" /D "_AFXDLL" /MDd /nologo /W3 /Gm /GR /GX /ZI /Od /I "..\Altova" /I "..\AltovaXML" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_UNICODE" /D "UNICODE" /FR /Yu"stdafx.h" /FD /GZ /c

# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo


!ENDIF 

# Begin Target

# Name "ClientAddinConfig - Win32 Release"
# Name "ClientAddinConfig - Win32 Debug"
# Name "ClientAddinConfig - Win32 Unicode Release"
# Name "ClientAddinConfig - Win32 Unicode Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ClientAddinConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\ClientAddinConfig_CEmulationType.cpp
# End Source File
# Begin Source File

SOURCE=.\ClientAddinConfig_CSecureType.cpp
# End Source File
# Begin Source File

SOURCE=.\ClientAddinConfig_CConfigType.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\ClientAddinConfig.h
# End Source File
# Begin Source File

SOURCE=.\ClientAddinConfigBase.h
# End Source File
# Begin Source File

SOURCE=.\ClientAddinConfig_CEmulationType.h
# End Source File
# Begin Source File

SOURCE=.\ClientAddinConfig_CSecureType.h
# End Source File
# Begin Source File

SOURCE=.\ClientAddinConfig_CConfigType.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# End Target
# End Project
