// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__67D6FF91_7E60_4FCC_BCC3_15EE458E1951__INCLUDED_)
#define AFX_STDAFX_H__67D6FF91_7E60_4FCC_BCC3_15EE458E1951__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <string>
#include <atlbase.h>
#include <sstream>
#include <io.h>

#include "fluke/flukemaster.h"
#include "iniext/iniext.h"
#include "debugger/debug.h"

// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__67D6FF91_7E60_4FCC_BCC3_15EE458E1951__INCLUDED_)
