#ifndef __CLIENT_H__
#define __CLIENT_H__

#include <exception>
#include <string>

namespace ClientFluke
{
  unsigned long CreateClient(const char* p = "clientaddin.dll",const char* app = 0);
  unsigned long AttachClient(const char* p = "clientaddin.dll");
}

#endif
