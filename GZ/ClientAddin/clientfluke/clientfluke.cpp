#include "stdafx.h"
#include "clientfluke.h"

DWORD ClientFluke::CreateClient(const char* p,const char* app)
{
  char buf[MAX_PATH];

  DWORD dw;
  std::string uoaddindll=GetModuleRunDir()+p;
  if(access(uoaddindll.c_str(),0)==-1)
	throw std::exception(("Addin '"+uoaddindll+"' not found").c_str());

  if(app==0||strlen(app)==0)
  {
    CRegKey key;
    if(key.Open(HKEY_LOCAL_MACHINE,"SOFTWARE\\Origin Worlds Online\\Ultima Online\\1.0")!=ERROR_SUCCESS&&
      key.Open(HKEY_LOCAL_MACHINE,"SOFTWARE\\Origin Worlds Online\\Ultima Online Third Dawn\\1.0")!=ERROR_SUCCESS)
      throw std::exception("can't get registry info");
    DWORD bufsize=sizeof(buf);
    key.QueryValue(buf,"ExePath",&bufsize);
  }else
    strcpy(buf,app);

  if((dw=Fluke::CFlukeMaster::flukemaster_instant(buf,"",(uoaddindll).c_str()))==0)
    throw std::exception("error create process client.exe");

  return dw;
}

namespace ClientFluke
{
  struct Enum
  {
    DWORD processid;
    const char*p;
  };

  BOOL CALLBACK EnumWindowsProc(HWND hwnd,LPARAM lParam )
  {
    if(!IsWindowVisible(hwnd))
      return TRUE;

    Enum *e=(Enum*)lParam;

    char buf[1024];
    GetClassName(hwnd,buf,sizeof(buf));
    if(strcmp(buf,"Ultima Online")==0||strcmp(buf,"Ultima Online Third Dawn")==0)
    {
      DWORD threadid=GetWindowThreadProcessId(hwnd,&e->processid);

      if(Fluke::CFlukeMaster::flukemaster(threadid,e->p))
        return FALSE;
    }
    e->processid=0;
    return TRUE;
  }
}

DWORD ClientFluke::AttachClient(const char* p)
{
  DBGTRACE("AttachClient\n");

  Enum e;
  e.p=p;

  EnumWindows(EnumWindowsProc,(LPARAM)&e);

  return e.processid;
}
