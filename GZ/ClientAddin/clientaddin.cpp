// uoaddin.cpp : Defines the entry point for the DLL application.
//

#include "Stdafx.h"
#include "clientaddin.h"

#undef _USRDLL
#include "altova/altovalib.h"
#include "clientsuck/clientunknown.h"
#include "clientsuck/client203.h"
#include "clientsuck/clientff.h"
#include "clientsuck/client308s.h"
#include "clientsuck/uotd308s.h"
#include "clientsuck/client400e.h"
#include "clientsuck/uotd400e.h"
#include "../client/packets.h"
#include "iniext/iniext.h"
#include "../clientaddin/clientidentify/clientidentify.h"
#include "debugger/debug.h"
#include "errorreport/errorreport.h"

using namespace cac;
using namespace ica;

/// Размер буфера чтения для конфига
const int g_bufsize=16384;

/// поток создаеться чтобы как можно быстрей вернуть нормальный ход работы основному процессу
/// и избежать головной боли непонимая почему процесс завис. (см описание ниже про SetWindowsHook)

/// BOOL CMyApp::InitInstance()
/// int CMyApp::ExitInstance()
/// Интересная особенность обнаружена в момент подключения и отключения библиотеки.
/// В момент подключения бибилотеки по SetWindowsHook из другого процесса не выполняються
/// никакие другие потоки в процессе к которому происходит подключение. Это вызывает зависание
/// инфицируемого процесса в случае если я жду корректное завершение потока (CInterProcSlave::ReceiveThread)
/// такой ошибки не происходит при нормальном завершении работы процесса если билиотека была
/// загружена ествественно (LoadLibrary)

class CMyApp: public CFlukeSlave
{
  virtual void FlukeInit(HMODULE);
public:
  CMyApp();
  ~CMyApp();
};

CClientAddin *g_clientaddin=0;
HANDLE g_createthread=0;
HMODULE g_hModule=0;
CMyApp g_myapp;
DWORD g_dwThreadId=0;

LONG WINAPI UnhandledExceptionFilter2(struct _EXCEPTION_POINTERS *ExceptionInfo )
{
  DBGTRACE_ERROR("UnhandledExceptionFilter2\n");

  g_clientaddin->Exit(-1);

  ExitProcess(-1);
  return 0;
}

//

CClientAddin::CClientAddin():m_clientuo(0)
{
  SetUnhandledExceptionFilter(UnhandledExceptionFilter2);
}

void CClientAddin::Exit(unsigned int code)
{
  if(m_clientuo!=0)
  {
    try
    {
      m_clientuo->Exit(code);
    }catch(CInterProc::BadWrite &)
    {
    }
    m_clientuo->Disconnect();
    delete m_clientuo;
    m_clientuo=0;
  }
}

CClientUO* CClientAddin::CreateClient()
{
  CClientUO* p=0;

  switch(ClientIdentify::GetClientVersion())
  {
  case ClientIdentify::client203:
    p=new CClient203();
    break;
  case ClientIdentify::clientff:
    p=new CClientFF();
    break;
  case ClientIdentify::client308s:
    //p=new Client308S();
    //break;
  case ClientIdentify::uotd308s:
    //p=new UOTD308S();
    //break;
  case ClientIdentify::client400e:
    //p=new Client400e();
    //break;
  case ClientIdentify::uotd400e:
    //p=new UOTD400e();
    //break;
  case ClientIdentify::clientUnknown:
  default:
    p=new ClientUnknown();
    break;
  }

  unsigned char buf[g_bufsize];
  int r=0;

  while((r=p->Read(buf,sizeof(buf)))==0)
    ;

  p->ReadConfig((const char*)buf);

  return p;
}

CClientAddin::~CClientAddin()
{
  Exit(0);

  g_clientaddin=0;

  SetUnhandledExceptionFilter(0);
}

void CClientAddin::InitInstance()
{
  m_clientuo=CreateClient();
}

void CClientAddin::Poll()
{
  m_clientuo->Poll();
}

static unsigned __stdcall ClientAddinCreate(void *)
{
  CoInitialize(0);
  try
  {
    while(g_hModule!=0)
      g_clientaddin->Poll();
  }catch(std::exception &e)
  {
    DBGTRACE_ERROR("%s\n",e.what());
    PostThreadMessage(g_dwThreadId,WM_QUIT,0,0);
  }
  try
  {
    delete g_clientaddin;
    g_clientaddin=0;
  }catch(std::exception &e)
  {
    DBGTRACE_ERROR("%s\n",e.what());
    PostThreadMessage(g_dwThreadId,WM_QUIT,0,0);
  }
  CoUninitialize();
  return 0;
}

CMyApp::~CMyApp()
{
  try
  {
    g_hModule=0;
    if(g_createthread!=0)
    {
      WaitForSingleObject(g_createthread,-1);
      CloseHandle(g_createthread);
      g_createthread=0;
    }
  }catch(std::exception &e)
  {
    DBGTRACE_ERROR(ErrorReport("Delete Fluke",e.what()));
  }
}

void CMyApp::FlukeInit(HMODULE h)
{
  if(g_createthread==0)
  {
    g_hModule=h;

    CoInitialize(0);
    g_clientaddin=new CClientAddin();
    g_clientaddin->InitInstance();
    CoUninitialize();

    g_createthread=(HANDLE)_beginthreadex(NULL,0,ClientAddinCreate,h,0,NULL);
  }
}

CMyApp::CMyApp()
{
  try
  {
    CMyApp::Create();
    g_dwThreadId=GetCurrentThreadId();
  }catch(std::exception &e)
  {
    DBGTRACE_ERROR(ErrorReport("Create Fluke",e.what()));
  }
}
