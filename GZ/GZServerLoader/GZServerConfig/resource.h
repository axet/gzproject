//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GZServerAddin.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_GZSERVERADDIN_DIALOG        102
#define IDR_HTML_GZSERVERADDIN_DIALOG   104
#define IDR_MAINFRAME                   128
#define IDR_GIF1                        129
#define IDR_GIF2                        130
#define IDD_DIALOG_WINDOWS              131
#define IDC_LIST1                       1000
#define IDC_BUTTON_UNLOAD               1003
#define IDC_EDIT_PORT                   1007
#define IDC_COMBO1                      1008
#define IDC_BUTTON_DELETE               1009
#define IDC_BUTTON_SAVE                 1010
#define IDC_BUTTON_ATTACH               1011
#define IDC_BUTTON_LOAD                 1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
