#pragma once
#include "afxwin.h"


// CDialogWindows dialog

class CDialogWindows : public CDialog
{
	DECLARE_DYNAMIC(CDialogWindows)

  HWND m_wnd;

public:
	CDialogWindows(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDialogWindows();

  HWND GetWnd();

// Dialog Data
	enum { IDD = IDD_DIALOG_WINDOWS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CListBox m_listbox;
  virtual BOOL OnInitDialog();
  afx_msg void OnLbnDblclkList1();
  afx_msg void OnBnClickedOk();
};
