#include <iniext/path.h>
#include <fluke/flukemaster.h>
#include "../../serveraddinio/internetsamaster.h"

class CGZAdministratorApp : public CInterNetSAMaster
{
public:
  bool ServerAttach();
  void ServerDetach();
};
