#include "stdafx.h"
#include "gzadministratorapp.h"

#include <iniext/iniext.h>

#include "resource.h"
#include "dialogwindows.h"

CGZAdministratorApp g_app;

bool CGZAdministratorApp::ServerAttach()
{
  CDialogWindows dlg;
  HWND h=dlg.GetWnd();
  if(h==0)
    return false;

  unsigned long processid;
  unsigned threadid=GetWindowThreadProcessId(h,&processid);
  CFlukeMaster::flukemaster(threadid,(IniExt::GetModuleRunDir()+"ServerAddin.dll").c_str());
  return true;
}

void CGZAdministratorApp::ServerDetach()
{
  if(!IsConnected())
    Connect("localhost");
  Unload();
  Disconnect();
}
