// DialogWindows.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DialogWindows.h"
#include ".\dialogwindows.h"

#include <vector>

#include <winuser.h>

typedef std::vector<HWND> list_t;

BOOL CALLBACK EnumWindowsProc(          HWND hwnd,
    LPARAM lParam
)
{
  if(!IsWindowVisible(hwnd))
    return TRUE;

  char buf[1024];
  GetClassName(hwnd,buf,sizeof(buf));
  if(strcmp(buf,"SphereSvr")==0||strcmp(buf,"ConsoleWindowClass")==0)
    ((list_t*)lParam)->push_back(hwnd);
  return TRUE;
}

// CDialogWindows dialog

IMPLEMENT_DYNAMIC(CDialogWindows, CDialog)
CDialogWindows::CDialogWindows(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogWindows::IDD, pParent)
{
}

CDialogWindows::~CDialogWindows()
{
}

void CDialogWindows::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_LIST1, m_listbox);
}


BEGIN_MESSAGE_MAP(CDialogWindows, CDialog)
  ON_LBN_DBLCLK(IDC_LIST1, OnLbnDblclkList1)
  ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


HWND CDialogWindows::GetWnd()
{
  if(DoModal()!=IDOK)
    return 0;

  return m_wnd;
}
// CDialogWindows message handlers

BOOL CDialogWindows::OnInitDialog()
{
  CDialog::OnInitDialog();

  list_t list;

  EnumWindows(EnumWindowsProc,(LPARAM)&list);

  if(list.size()==0)
    throw std::exception("No server window found");
  if(list.size()==1)
  {
    m_wnd=list.front();
    EndDialog(IDOK);
  }

  for(int i=0;i<list.size();i++)
  {
    CString name;
    FromHandle(list[i])->GetWindowText(name);
    int item=m_listbox.AddString(name);
    m_listbox.SetItemData(item,(DWORD)list[i]);
  }

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CDialogWindows::OnLbnDblclkList1()
{
  OnBnClickedOk();
}

void CDialogWindows::OnBnClickedOk()
{
  int sel=m_listbox.GetCurSel();
  m_wnd=(HWND)m_listbox.GetItemData(sel);

  OnOK();
}
