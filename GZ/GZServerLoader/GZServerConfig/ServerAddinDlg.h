#pragma once

#include <Altova/AltovaLib.h>
#include "../xsd/ServerConfig/ServerConfig.h"
#include "../../ServerAddin/xsd/SchemaServerConfig/SchemaServerConfig.h"
#include "afxwin.h"

// ServerAddinDlg dialog

class ServerAddinDlg : public CDialog
{
	DECLARE_DYNAMIC(ServerAddinDlg)

public:
	ServerAddinDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~ServerAddinDlg();

// Dialog Data
	enum { IDD = IDD_GZSERVERADDIN_DIALOG };

protected:
  HICON m_hIcon;
  tstring m_configpath;
  CServerConfigDoc m_doc;
  sc::CConfigType m_config;

  tstring m_addinconfigpath;
  CSchemaServerConfigDoc m_addindoc;
  ssa::CConfigType m_addinconfig;

  void Load();
  void Save();
  void LoadConfig(const char*);
  void SaveConfig();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedOk();
  afx_msg void OnBnClickedCancel();
  afx_msg void OnBnClickedButtonUnload();
  virtual BOOL OnInitDialog();
  afx_msg void OnCbnDropdownCombo1();
  afx_msg void OnCbnSelendokCombo1();
  CComboBox m_serverlist;
  int m_port;
  afx_msg void OnBnClickedButtonSave();
  afx_msg void OnBnClickedButtonDelete();
  afx_msg void OnBnClickedButtonLoad();
  afx_msg void OnClose();
};
