// ServerAddinDlg.cpp : implementation file
//

#include "stdafx.h"
#include "gzServeraddin.h"
#include "gzadministratorapp.h"
#include "ServerAddinDlg.h"
#include ".\serveraddindlg.h"

#include <io.h>
#include <direct.h>

extern CGZAdministratorApp g_app;

// ServerAddinDlg dialog

IMPLEMENT_DYNAMIC(ServerAddinDlg, CDialog)
ServerAddinDlg::ServerAddinDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ServerAddinDlg::IDD, pParent)
  , m_port(0)
{
}

ServerAddinDlg::~ServerAddinDlg()
{
}

void ServerAddinDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_COMBO1, m_serverlist);
  DDX_Text(pDX, IDC_EDIT_PORT, m_port);
}


BEGIN_MESSAGE_MAP(ServerAddinDlg, CDialog)
  ON_BN_CLICKED(IDOK, OnBnClickedOk)
  ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
  ON_BN_CLICKED(IDC_BUTTON_UNLOAD, OnBnClickedButtonUnload)
  ON_CBN_DROPDOWN(IDC_COMBO1, OnCbnDropdownCombo1)
  ON_CBN_SELENDOK(IDC_COMBO1, OnCbnSelendokCombo1)
  ON_BN_CLICKED(IDC_BUTTON_SAVE, OnBnClickedButtonSave)
  ON_BN_CLICKED(IDC_BUTTON_DELETE, OnBnClickedButtonDelete)
  ON_BN_CLICKED(IDC_BUTTON_LOAD, OnBnClickedButtonLoad)
  ON_WM_CLOSE()
END_MESSAGE_MAP()


// ServerAddinDlg message handlers

void ServerAddinDlg::OnBnClickedOk()
{
  try
  {
    Save();
    CWaitCursor wc;
    if(g_app.ServerAttach()==0)
      return;
  }catch(std::exception &e)
  {
    AfxMessageBox(e.what());
    return;
  }

  OnOK();
}

void ServerAddinDlg::OnBnClickedCancel()
{
  Save();
  OnCancel();
}

void ServerAddinDlg::OnBnClickedButtonUnload()
{
  try
  {
    CWaitCursor wc;
    g_app.ServerDetach();
  }catch(std::exception &e)
  {
    AfxMessageBox(e.what());
  }
}

BOOL ServerAddinDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

  try
  {
    Load();

    CString str;
    m_serverlist.GetWindowText(str);
    m_serverlist.SelectString(-1,str);
    OnCbnSelendokCombo1();
  }catch(std::exception &e)
  {
    AfxMessageBox(e.what());
  }

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void ServerAddinDlg::Load()
{
  m_configpath="GZServerConfig.config";
  if(access(m_configpath.c_str(),0)!=-1)
  {
    m_config=m_doc.Load(m_configpath);
    for(int i=0;i<m_config.GetServerPathCount();i++)
    {
      m_serverlist.AddString(((tstring)m_config.GetServerPathAt(i)).c_str());
    }
    m_serverlist.SetWindowText(((tstring)m_config.GetActiveServer()).c_str());
  }else
    m_doc.SetRootElementName("","Config");

  CString str;
  m_serverlist.GetWindowText(str);
  //m_serverlist.ResetContent();
  int i=m_serverlist.FindString(-1,"Browse..");
  if(i!=-1)
    m_serverlist.DeleteString(i);
  m_serverlist.InsertString(-1,"Browse..");
  m_serverlist.SetWindowText(str);
}

void ServerAddinDlg::OnCbnDropdownCombo1()
{
}

void ServerAddinDlg::OnCbnSelendokCombo1()
{
  SaveConfig();

  int sel=m_serverlist.GetCurSel();
  if(sel==-1)
  {
    m_addinconfigpath.clear();
    return;
  }

  CString str;
  m_serverlist.GetLBText(sel,str);
  if(str=="Browse..")
  {
    CFileDialog dlg(1);
    int i=-1;
    if(dlg.DoModal()==IDOK)
    {
      i=m_serverlist.FindString(-1,dlg.GetPathName());
      if(i==-1)
        i=m_serverlist.InsertString(0,dlg.GetPathName());
    }

    m_serverlist.SetCurSel(i);
    OnCbnSelendokCombo1();
    return;
  }

  try
  {
    LoadConfig(str+".serveraddin.config");
  }catch(std::exception &e)
  {
    AfxMessageBox(e.what());
  }
}

void ServerAddinDlg::LoadConfig(const char*p)
{
  m_addinconfigpath=p;

  m_port=49049;

  if(access(p,0)!=-1)
  {
    m_addinconfig=m_addindoc.Load(p);
    m_port=m_addinconfig.GetPort();
  }else
    m_addindoc.SetRootElementName("","Config");

  UpdateData(FALSE);
}

void ServerAddinDlg::OnBnClickedButtonSave()
{
  SaveConfig();
}

void ServerAddinDlg::SaveConfig()
{
  if(m_addinconfigpath.empty())
    return;

  UpdateData();

  m_addinconfig.RemovePort();
  m_addinconfig.AddPort(m_port);
  m_addindoc.Save(m_addinconfigpath,m_addinconfig);
}

void ServerAddinDlg::OnBnClickedButtonDelete()
{
  int sel=m_serverlist.GetCurSel();
  if(sel==-1)
    return;
  m_serverlist.DeleteString(sel);

  remove(((tstring)m_addinconfigpath).c_str());
}

void ServerAddinDlg::OnBnClickedButtonLoad()
{
  SaveConfig();
  WinExec("GZServerLoader.exe",SW_SHOW);

  OnBnClickedCancel();
}

void ServerAddinDlg::OnClose()
{
  SaveConfig();
  Save();

  CDialog::OnClose();
}

void ServerAddinDlg::Save()
{
  if(m_configpath.empty())
    return;

  m_config.RemoveServerPath();
  for(int i=0;i<m_serverlist.GetCount();i++)
  {
    CString str;
    m_serverlist.GetLBText(i,str);
    m_config.AddServerPath((const char*)str);
  }
  CString str;
  m_serverlist.GetWindowText(str);
  m_config.RemoveActiveServer();
  m_config.AddActiveServer((const char*)str);
  m_doc.Save(m_configpath,m_config);
}
