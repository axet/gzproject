// GZServerAddinLoader.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "GZServerAddinLoader.h"
#include "GZServerAddinLoaderDlg.h"
#include <altova/altovalib.h>
#include "xsd/ServerConfig/ServerConfig.h"
#include <iniext/iniext.h>
#include <fluke/flukemaster.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGZServerAddinLoaderApp

BEGIN_MESSAGE_MAP(CGZServerAddinLoaderApp, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


// CGZServerAddinLoaderApp construction

CGZServerAddinLoaderApp::CGZServerAddinLoaderApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CGZServerAddinLoaderApp object

CGZServerAddinLoaderApp theApp;


// CGZServerAddinLoaderApp initialization

BOOL CGZServerAddinLoaderApp::InitInstance()
{
	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Guard Zone"));

  CoInitialize(0);
  try
  {
    CServerConfigDoc doc;
    sc::CConfigType config=doc.Load("GZServerConfig.config");

    tstring path=config.GetActiveServer();
    CFlukeMaster::flukemaster_instant(path.c_str(),"",(GetModuleRunDir()+"ServerAddin.dll").c_str());
  }catch(std::exception &e)
  {
    AfxMessageBox(e.what());
  }catch(CXmlException &e)
  {
    AfxMessageBox(e.GetInfo().c_str());
  }

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
