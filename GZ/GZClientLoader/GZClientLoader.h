// GZClientLoader.h : main header file for the GZCLIENTLOADER application
//

#if !defined(AFX_GZCLIENTLOADER_H__0BA9147F_640D_4A96_AC85_1C7D4929C752__INCLUDED_)
#define AFX_GZCLIENTLOADER_H__0BA9147F_640D_4A96_AC85_1C7D4929C752__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CGZClientLoaderApp:
// See GZClientLoader.cpp for the implementation of this class
//

class CGZClientLoaderApp : public CWinApp
{
  void WriteLog(const char*);

public:
	CGZClientLoaderApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGZClientLoaderApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CGZClientLoaderApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GZCLIENTLOADER_H__0BA9147F_640D_4A96_AC85_1C7D4929C752__INCLUDED_)
