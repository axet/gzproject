#include "stdafx.h"
#include "GZClientConfig.h"
#include "GZClientConfigDlg.h"
#include ".\gzclientconfigdlg.h"
#include <iniext/iniext.h>

#include <exception>
#include <atlbase.h>
#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace cac;

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);

protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CGZClientConfigDlg №пёЬ¤и¶ф



CGZClientConfigDlg::CGZClientConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGZClientConfigDlg::IDD, pParent)
  , m_emulation(_T(""))
  , m_secure(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CGZClientConfigDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_CBString(pDX, IDC_COMBO1, m_emulation);
  DDX_Check(pDX, IDC_CHECK1, m_secure);
  DDX_Control(pDX, IDC_COMBO2, m_clientlist);
}

BEGIN_MESSAGE_MAP(CGZClientConfigDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
  ON_BN_CLICKED(IDOK, OnBnClickedOk)
  ON_BN_CLICKED(IDC_SAVE, OnBnClickedSave)
  ON_BN_CLICKED(IDC_RADIO3, OnBnClickedRadio3)
  ON_BN_CLICKED(IDC_RADIO2, OnBnClickedRadio3)
  ON_BN_CLICKED(IDC_RADIO1, OnBnClickedRadio3)
  ON_CBN_SELENDOK(IDC_COMBO2, OnCbnSelendokCombo2)
  ON_CBN_DROPDOWN(IDC_COMBO2, OnCbnDropdownCombo2)
END_MESSAGE_MAP()


// CGZClientConfigDlg °T®§іBІz±`¦Ў

BOOL CGZClientConfigDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// ±N "Гц©у..." Ґ\ЇаЄнҐ[¤JЁtІОҐ\ЇаЄнЎC

	// IDM_ABOUTBOX ҐІ¶·¦bЁtІО©RҐOЅdіт¤§¤¤ЎC
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// і]©w¦№№пёЬ¤и¶фЄє№ПҐЬЎC·нАіҐОµ{¦ЎЄєҐDµшµЎ¤Ј¬O№пёЬ¤и¶ф®ЙЎA
	// ®Ш¬[·|¦Ы°К±qЁЖ¦№§@·~
	SetIcon(m_hIcon, TRUE);			// і]©w¤j№ПҐЬ
	SetIcon(m_hIcon, FALSE);		// і]©w¤p№ПҐЬ

	//ShowWindow(SW_MINIMIZE);

  try
  {
    m_path=(GetModuleRunDir()+"clientaddin.config").c_str();
    if(access(m_path,0)!=-1)
    {
      m_config=m_doc.Load((const char*)m_path);

      CString emulation=((tstring)m_config.GetEmulation()).c_str();
      if(emulation=="disable")
        ((CButton*)GetDlgItem(IDC_RADIO1))->SetCheck(1);
      else if(emulation=="nocrypt")
        ((CButton*)GetDlgItem(IDC_RADIO2))->SetCheck(1);
      else
      {
        m_emulation=emulation;
        ((CButton*)GetDlgItem(IDC_RADIO3))->SetCheck(1);
        GetDlgItem(IDC_COMBO1)->EnableWindow(TRUE);
      }
      m_secure=(tstring)m_config.GetSecure()=="enable";
      m_clientlist.SetWindowText(((tstring)m_config.GetClientPath()).c_str());
    }else
    {
      m_doc.SetRootElementName("","Config");
    }
  }catch(std::exception &e)
  {
    AfxMessageBox(e.what());
  }

  UpdateData(FALSE);

  CenterWindow();

	return TRUE;  // ¶З¦^ TRUEЎA°Ј«D±z№п±±Ёо¶µі]©wµJВI
}

void CGZClientConfigDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// ¦pЄG±NіМ¤p¤Ж«ц¶sҐ[¤J±zЄє№пёЬ¤и¶фЎA±z»Э­n¤U¦CЄєµ{¦ЎЅXЎAҐH«KГё»s№ПҐЬЎC
// №п©уЁПҐО¤еҐу/АЛµшјТ¦ЎЄє MFC АіҐОµ{¦ЎЎA®Ш¬[·|¦Ы°К§№¦Ё¦№§@·~ЎC

void CGZClientConfigDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Гё»sЄєёЛёm¤є®e

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ±N№ПҐЬёm¤¤©уҐО¤бєЭЇx§О
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ґyГё№ПҐЬ
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//·нЁПҐОЄМ©м¦ІіМ¤p¤ЖµшµЎ®ЙЎAЁtІО©IҐsіo­УҐ\ЇаЁъ±oґејРЕгҐЬЎC
HCURSOR CGZClientConfigDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CGZClientConfigDlg::OnBnClickedOk()
{
  try
  {
    OnBnClickedSave();
  }catch(std::exception &e)
  {
    AfxMessageBox(e.what());
  }

  WinExec("GZClientLoader.exe",SW_SHOW);

  OnOK();
}

void CGZClientConfigDlg::OnBnClickedSave()
{
  UpdateData();

  m_config.RemoveSecure();
  m_config.RemoveEmulation();
  if(((CButton*)GetDlgItem(IDC_RADIO3))->GetCheck())
    m_config.AddEmulation(CEmulationType(m_emulation));
  else if(((CButton*)GetDlgItem(IDC_RADIO2))->GetCheck())
    m_config.AddEmulation(CEmulationType("nocrypt"));
  else
    m_config.AddEmulation(CEmulationType("disable"));
  m_config.AddSecure(CSecureType(m_secure?"enable":"disable"));
  CString str;
  m_clientlist.GetWindowText(str);
  m_config.RemoveClientPath();
  m_config.AddClientPath((const char*)str);
  m_doc.Save((const char*)m_path,m_config);
}

void CGZClientConfigDlg::OnBnClickedRadio3()
{
  GetDlgItem(IDC_COMBO1)->EnableWindow(((CButton*)GetDlgItem(IDC_RADIO3))->GetCheck()==1);
}

void CGZClientConfigDlg::OnBnClickedButton1()
{
  ;
}

void CGZClientConfigDlg::OnCbnSelendokCombo2()
{
  int sel=m_clientlist.GetCurSel();
  if(sel==-1)
    return;
  CString str;
  m_clientlist.GetLBText(sel,str);
  if(str=="Browse..")
  {
    CFileDialog dlg(1);
    if(dlg.DoModal()!=IDOK)
      return;

    int i=m_clientlist.InsertString(m_clientlist.GetCount(),dlg.GetPathName());
    m_clientlist.SetCurSel(i);
  }
}

void CGZClientConfigDlg::OnCbnDropdownCombo2()
{
  CString str;
  m_clientlist.GetWindowText(str);
  m_clientlist.ResetContent();

  char buf[MAX_PATH];
  DWORD bufsize=sizeof(buf);

  CRegKey key;
  if(key.Open(HKEY_LOCAL_MACHINE,"SOFTWARE\\Origin Worlds Online\\Ultima Online\\1.0")==ERROR_SUCCESS)
  {
    key.QueryValue(buf,"ExePath",&bufsize);
    m_clientlist.InsertString(m_clientlist.GetCount(),buf);
  }
  if(key.Open(HKEY_LOCAL_MACHINE,"SOFTWARE\\Origin Worlds Online\\Ultima Online Third Dawn\\1.0")==ERROR_SUCCESS)
  {
    key.QueryValue(buf,"ExePath",&bufsize);
    m_clientlist.InsertString(m_clientlist.GetCount(),buf);
  }

  m_clientlist.InsertString(m_clientlist.GetCount(),"Browse..");

  m_clientlist.SetWindowText(str);
}
