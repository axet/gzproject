// GZClientLoader.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "GZClientLoader.h"
#include "GZClientLoaderDlg.h"
#include "../clientaddin/clientfluke/clientfluke.h"
#include "../interproc_clientaddin/outofprocmaster.h"
#include <debugger/debug.h>
#include "../clientaddin/xsd/ClientAddinConfig/ClientAddinConfigBase.h"
#include "../interproc_clientaddin/eventaggregate.h"
#include <iniext/iniext.h>

#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace cac;

/////////////////////////////////////////////////////////////////////////////
// CGZClientLoaderApp

BEGIN_MESSAGE_MAP(CGZClientLoaderApp, CWinApp)
	//{{AFX_MSG_MAP(CGZClientLoaderApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGZClientLoaderApp construction

CGZClientLoaderApp::CGZClientLoaderApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGZClientLoaderApp object

CGZClientLoaderApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CGZClientLoaderApp initialization

class MyParam:public CCommandLineInfo
{
public:
  CString m_client;
  virtual void ParseParam(
    const char* pszParam,
    BOOL bFlag,
    BOOL bLast
  )
  {
    m_client=pszParam;
  }
};

template <class T> class EBA:public T
{
  CInterProcMaster ipm;

public:
  EBA()
  {
    SetIPM(&ipm);
  }
  void Abort(){};
  const char* GetUnitId(){return 0;}
  
  CInterProcMaster* operator ->()
  {
    return &ipm;
  }
};

BOOL CGZClientLoaderApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

  AfxInitRichEdit();

	SetRegistryKey(_T("Guard Zone"));

  bool errors=false;

  DBGTRACE("begin dialog\n");
	CGZClientLoaderDlg dlg;
	m_pMainWnd = &dlg;
  dlg.Create(CGZClientLoaderDlg::IDD);

  EBA< EventsAggregate<COutOfProcMaster> > oopm;

  CoInitialize(0);

  try
  {
    tstring clientpath;
    tstring path=(GetModuleRunDir()+"clientaddin.config").c_str();
    if(access(path.c_str(),0)!=-1)
    {
      CClientAddinConfigDoc doc;
      CConfigType config;
      config=doc.Load(path);
      clientpath=config.GetClientPath();
    }

    DBGTRACE("try\n");
    MyParam info;
    info.m_client=clientpath.c_str();
    DBGTRACE("try2\n");
    ParseCommandLine(info);
    unsigned long clientprocessid=0;
    DBGTRACE("try3\n");
#ifndef NDEBUG
    if(!info.m_client.IsEmpty()||(clientprocessid=ClientFluke::AttachClient())==0)
      clientprocessid=ClientFluke::CreateClient("clientaddin.dll",info.m_client);
#else
    clientprocessid=ClientFluke::CreateClient("clientaddin.dll",info.m_client);
#endif
    DBGTRACE("try4\n");
    oopm->Connect(clientprocessid);
    oopm.ShowWindow(true);
    DBGTRACE("try end\n");
  }catch(std::exception &e)
  {
    WriteLog(e.what());
    errors=true;
  }

  if(errors)
    dlg.RunModalLoop();

  CoUninitialize();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

void CGZClientLoaderApp::WriteLog(const char*p)
{
  CGZClientLoaderDlg* pp=(CGZClientLoaderDlg*)m_pMainWnd;
  pp->m_log.SetSel(-1,-1);
  pp->m_log.ReplaceSel(p);
}
