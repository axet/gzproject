#ifndef __CLIENTSECURE_H__
#define __CLIENTSECURE_H__

#include "../client/clientnetevents.h"
#include "../serveraddin/serveraddinmodule.h"
#include "../serveraddin/firewall/firewallmodule.h"
#include "secureconnection.h"

namespace ClientSecure
{
  class CClientSecure;
}

class ClientSecure::CClientSecure:public CServerAddinModule,public CFirewallModule,
  public SecureConnection
{
  const char* GetName()
  {
    return "ClientSecure";
  }
public:
  void handle_receive_data(const void* buf,int bufsize);
};

#endif
