#ifndef __SECURECONNECTION_H__
#define __SECURECONNECTION_H__

namespace ClientSecure
{
  class SecureConnection;
};

class ClientSecure::SecureConnection
{
protected:
  // m_secureenabled - пакеты шифруються
  bool m_secureenabled;
public:
  SecureConnection();
  void Encrypt(unsigned char* begin,unsigned char* end);
  void Decrypt(unsigned char* begin,unsigned char* end);
};

#endif
