#include "clientsecure.h"

void ClientSecure::CClientSecure::handle_receive_data(const void* buf,int bufsize)
{
  unsigned char* begin=(unsigned char*)buf,*end=(unsigned char*)buf+bufsize;

  Decrypt(begin,end);
}
