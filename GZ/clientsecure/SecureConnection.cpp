#include "secureconnection.h"

ClientSecure::SecureConnection::SecureConnection():m_secureenabled(false)
{
  ;
}

void ClientSecure::SecureConnection::Encrypt(unsigned char* begin,unsigned char* end)
{
  unsigned char *cbegin=begin,*cend=end;
  while(cbegin!=end)
  {
    *cbegin=(*cbegin+5)^'Х';

    cbegin++;
    cend--;
  }
}

void ClientSecure::SecureConnection::Decrypt(unsigned char* begin,unsigned char* end)
{
  unsigned char *cbegin=begin,*cend=end;
  while(cbegin!=end)
  {
    *cbegin=(*cbegin^'Х')-5;

    cbegin++;
    cend--;
  }
}
