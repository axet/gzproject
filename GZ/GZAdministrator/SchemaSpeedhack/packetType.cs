//
// PacketType.cs.cs
//
// This file was generated by XMLSPY 5 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSPY Documentation for further details.
// http://www.altova.com/xmlspy
//


using System;
using System.Xml;
using Altova.Types;

namespace SchemaSpeedHack
{
	public class PacketType : Altova.Node
	{
		#region Forward constructors
		public PacketType() : base() {}
		public PacketType(XmlDocument doc) : base(doc) {}
		public PacketType(XmlNode node) : base(node) {}
		public PacketType(Altova.Node node) : base(node) {}
		#endregion // Forward constructors

		#region GetProperty accessor methods
		public int GetGetPropertyMinCount()
		{
			return 1;
		}

		public int GetGetPropertyMaxCount()
		{
			return 1;
		}

		public int GetGetPropertyCount()
		{
			return DomChildCount(NodeType.Element, "", "GetProperty");
		}

		public bool HasGetProperty()
		{
			return HasDomChild(NodeType.Element, "", "GetProperty");
		}

		public GetPropertyType GetGetPropertyAt(int index)
		{
			return new GetPropertyType(GetDomChildAt(NodeType.Element, "", "GetProperty", index));
		}

		public GetPropertyType GetGetProperty()
		{
			return GetGetPropertyAt(0);
		}

		public void RemoveGetPropertyAt(int index)
		{
			RemoveDomChildAt(NodeType.Element, "", "GetProperty", index);
		}

		public void RemoveGetProperty()
		{
			while (HasGetProperty())
				RemoveGetPropertyAt(0);
		}

		public void AddGetProperty(GetPropertyType newValue)
		{
			AppendDomElement("", "GetProperty", newValue);
		}

		public void InsertGetPropertyAt(GetPropertyType newValue, int index)
		{
			InsertDomElementAt("", "GetProperty", index, newValue);
		}

		public void ReplaceGetPropertyAt(GetPropertyType newValue, int index)
		{
			ReplaceDomElementAt("", "GetProperty", index, newValue);
		}
		#endregion // GetProperty accessor methods

		#region SetProperty accessor methods
		public int GetSetPropertyMinCount()
		{
			return 1;
		}

		public int GetSetPropertyMaxCount()
		{
			return 1;
		}

		public int GetSetPropertyCount()
		{
			return DomChildCount(NodeType.Element, "", "SetProperty");
		}

		public bool HasSetProperty()
		{
			return HasDomChild(NodeType.Element, "", "SetProperty");
		}

		public SetPropertyType GetSetPropertyAt(int index)
		{
			return new SetPropertyType(GetDomChildAt(NodeType.Element, "", "SetProperty", index));
		}

		public SetPropertyType GetSetProperty()
		{
			return GetSetPropertyAt(0);
		}

		public void RemoveSetPropertyAt(int index)
		{
			RemoveDomChildAt(NodeType.Element, "", "SetProperty", index);
		}

		public void RemoveSetProperty()
		{
			while (HasSetProperty())
				RemoveSetPropertyAt(0);
		}

		public void AddSetProperty(SetPropertyType newValue)
		{
			AppendDomElement("", "SetProperty", newValue);
		}

		public void InsertSetPropertyAt(SetPropertyType newValue, int index)
		{
			InsertDomElementAt("", "SetProperty", index, newValue);
		}

		public void ReplaceSetPropertyAt(SetPropertyType newValue, int index)
		{
			ReplaceDomElementAt("", "SetProperty", index, newValue);
		}
		#endregion // SetProperty accessor methods
	}
}
