using System;

using Altova.Types;
using Altova;
using SchemaServerAddin;

using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace ServerAddinIO
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Socket
	{
		TcpClient m_socket;

		public void Connect(string address,int port)
		{
			m_socket=new TcpClient();
			m_socket.Connect(address,port);
		}

		public void Disconnect()
		{
			m_socket.Close();
		}

		public void Unload()
		{
			//SchemaServerAddinDoc doc;
		}

		public void ModuleCommand(string modulename,string xmlinput,out string xmloutput)
		{
			try
			{
				SchemaServerAddinDoc doc=new SchemaServerAddinDoc();
				PacketType packet=new PacketType();
				RequestType request=new RequestType();

				ModuleCommandType modulecommand=new ModuleCommandType();
				request.AddName(new SchemaString(modulename));
				request.AddData(new SchemaString(xmlinput));
				modulecommand.AddRequest(request);
				packet.AddModuleCommand(modulecommand);
				doc.SetRootElementName("","Packet");
				string xmlsend=doc.SaveXML(packet);
				Byte[] bytes=Encoding.ASCII.GetBytes(xmlsend);
				m_socket.GetStream().Write(bytes,0,bytes.Length);
				Byte[] inbytes=new Byte[1024*1024];
				//inbytes[0]=(Byte)m_socket.GetStream().ReadByte();
				//while(inbytes
				string buf="";
			{
				string str="";
				while(str.IndexOf("</Packet>")==-1)
				{
					int count=m_socket.GetStream().Read(inbytes,0,inbytes.Length);
					str=Encoding.ASCII.GetString(inbytes,0,count);
					buf+=str;
				}
			}
				packet=new PacketType(doc.LoadXML(buf));
				xmloutput=packet.GetModuleCommand().GetRespond().GetData().ToString();
			}
			catch(System.Exception e)
			{
				string s=e.Message;
				throw (new ConnectionLost());
			}
			catch
			{
				throw (new ConnectionLost());
			}
		}
	}

	public class ConnectionLost:Exception
	{
	}
}
