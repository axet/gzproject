using System;

namespace GZAdministrator
{
	/// <summary>
	/// Summary description for ModuleSheet.
	/// </summary>
	public class ModuleSheet:System.Windows.Forms.Form
	{
		bool m_changed;

		public ModuleSheet()
		{
			//
			// TODO: Add constructor logic here
			//

			m_changed=false;
		}

		public virtual System.Windows.Forms.Form GetForm()
		{
			return null;
		}

		public virtual void ServerLoad()
		{
		}

		public virtual void ServerSave()
		{
		}

		public bool IsChanged()
		{
			return m_changed;
		}

		private void InitializeComponent()
		{
			// 
			// ModuleSheet
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(528, 291);
			this.Name = "ModuleSheet";

		}

		public void Changed(bool b)
		{
			m_changed=b;
		}
	}
}
