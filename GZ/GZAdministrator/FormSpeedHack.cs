using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using SpeedHackIO;
using SchemaSpeedHack;

namespace GZAdministrator
{
	/// <summary>
	/// Summary description for Form2.
	/// </summary>
	public class FormSpeedHack : ModuleSheet
	{
		ServerAddinIO.Socket m_socket;

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxWalkCount;
		private System.Windows.Forms.TextBox textBoxWalkSpeed;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textBoxCountItems;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public override System.Windows.Forms.Form GetForm()
		{
			return this;
		}

		public override void ServerSave()
		{
			SpeedHackIO.Socket speedhack=new SpeedHackIO.Socket(ref m_socket);
			SchemaSpeedHack.SetPropertyType property= new SchemaSpeedHack.SetPropertyType();
			property.AddWalkCount(new Altova.Types.SchemaInteger(textBoxWalkCount.Text));
			property.AddWalkSpeed(new Altova.Types.SchemaInteger(textBoxWalkSpeed.Text));
			property.AddCountItems(new Altova.Types.SchemaInteger(textBoxCountItems.Text));
			speedhack.SetProperty(property);

			Changed(false);
		}

		public FormSpeedHack(ServerAddinIO.Socket socket)
		{
			m_socket=socket;

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxWalkCount = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxWalkSpeed = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.buttonSave = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.textBoxCountItems = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 24);
			this.label1.TabIndex = 0;
			this.label1.Text = "WalkCount";
			// 
			// textBoxWalkCount
			// 
			this.textBoxWalkCount.Location = new System.Drawing.Point(152, 24);
			this.textBoxWalkCount.Name = "textBoxWalkCount";
			this.textBoxWalkCount.TabIndex = 1;
			this.textBoxWalkCount.Text = "textBox1";
			this.textBoxWalkCount.TextChanged += new System.EventHandler(this.ValueChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 80);
			this.label2.Name = "label2";
			this.label2.TabIndex = 2;
			this.label2.Text = "Speed";
			this.label2.Click += new System.EventHandler(this.label2_Click);
			// 
			// textBoxWalkSpeed
			// 
			this.textBoxWalkSpeed.Location = new System.Drawing.Point(152, 80);
			this.textBoxWalkSpeed.Name = "textBoxWalkSpeed";
			this.textBoxWalkSpeed.TabIndex = 3;
			this.textBoxWalkSpeed.Text = "textBox2";
			this.textBoxWalkSpeed.TextChanged += new System.EventHandler(this.ValueChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(272, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(120, 24);
			this.label3.TabIndex = 4;
			this.label3.Text = "20 default";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(152, 112);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(184, 24);
			this.label4.TabIndex = 5;
			this.label4.Text = "riderless: 400 - walk, 200 - run";
			// 
			// buttonSave
			// 
			this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonSave.Location = new System.Drawing.Point(16, 16);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(88, 32);
			this.buttonSave.TabIndex = 6;
			this.buttonSave.Text = "Save";
			this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.buttonSave);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 347);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(448, 64);
			this.panel1.TabIndex = 7;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.textBoxWalkCount);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.textBoxWalkSpeed);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(16, 24);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(416, 176);
			this.groupBox1.TabIndex = 8;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Walk limits";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.textBoxCountItems);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Location = new System.Drawing.Point(16, 216);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(416, 80);
			this.groupBox2.TabIndex = 9;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Using limits";
			// 
			// textBoxCountItems
			// 
			this.textBoxCountItems.Location = new System.Drawing.Point(152, 32);
			this.textBoxCountItems.Name = "textBoxCountItems";
			this.textBoxCountItems.TabIndex = 2;
			this.textBoxCountItems.Text = "textBox1";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(272, 32);
			this.label6.Name = "label6";
			this.label6.TabIndex = 1;
			this.label6.Text = "2 default";
			this.label6.Click += new System.EventHandler(this.label6_Click);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(24, 32);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(112, 23);
			this.label5.TabIndex = 0;
			this.label5.Text = "Count items per sec";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(152, 144);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(184, 23);
			this.label7.TabIndex = 6;
			this.label7.Text = "rider: 200 - walk, 100 - run";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(264, 80);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(120, 24);
			this.label8.TabIndex = 7;
			this.label8.Text = "Set maximum speed";
			// 
			// FormSpeedHack
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(448, 411);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.panel1);
			this.Name = "FormSpeedHack";
			this.Text = "SpeedHack";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.FormSpeedHack_Closing);
			this.Load += new System.EventHandler(this.FormSpeedHack_Load);
			this.panel1.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void label2_Click(object sender, System.EventArgs e)
		{
		
		}

		private void buttonSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				ServerSave();
			}
			catch(ServerAddinIO.ConnectionLost)
			{
				MessageBox.Show(this,"Connection not established",this.Text,MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}

		public override void ServerLoad()
		{
			SpeedHackIO.Socket speedhack=new SpeedHackIO.Socket(ref m_socket);
			SchemaSpeedHack.RespondType respond=speedhack.GetProperty();

			textBoxWalkCount.Text=respond.GetWalkCount().ToString();
			textBoxWalkSpeed.Text=respond.GetWalkSpeed().ToString();
			textBoxCountItems.Text=respond.GetCountItems().ToString();
			Changed(false);
		}

		private void ValueChanged(object sender, System.EventArgs e)
		{
			Changed(true);
		}

		private void label6_Click(object sender, System.EventArgs e)
		{
		
		}

		private void FormSpeedHack_Load(object sender, System.EventArgs e)
		{
		
		}

		private void FormSpeedHack_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel=true;

			if(!((FormMain)Parent.Parent).PreCloseDocument(this))
				return;

			Hide();
		}
	}
}
