using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using SchemaAccountManager;
using Altova.Types;

namespace GZAdministrator
{
	/// <summary>
	/// Summary description for FormAccountManager.
	/// </summary>
	public class FormAccountManager : ModuleSheet
	{
		ServerAddinIO.Socket m_socket;
		SortedList m_groups;
		bool m_loading;

		private System.Windows.Forms.TreeView treeView1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Button buttonAddGroup;
		private System.Windows.Forms.Button buttonRemoveAccount;
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.TextBox textBoxPrivileges;
		private System.Windows.Forms.Button buttonRemovePrivileges;
		private System.Windows.Forms.Button buttonAddPrivileges;
		private System.Windows.Forms.ListView listViewPrivileges;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonRemoveIpProtection;
		private System.Windows.Forms.Button buttonAddIpProtection;
		private System.Windows.Forms.CheckBox checkBoxAllCommandPrivileges;
		private System.Windows.Forms.Button buttonAddAccount;

		public FormAccountManager(ServerAddinIO.Socket socket)
		{
			m_socket=socket;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		void LoadTree(ItemsType items)
		{
			m_groups = new SortedList();
			treeView1.Nodes.Clear();

			// ����� ��������� ��� ������ ������ ������ ��������
			bool defaultgroup=false;
			for(int i=0;i<items.GetItemCount();i++)
			{
				ItemType item=items.GetItemAt(i);
				if(item.HasIsGroup())
				{
					if(item.GetName().ToString()=="")
						defaultgroup=true;
				}
				AddItem(item);
			}

			// �� �� ������ � �������, ��������
			if(!defaultgroup)
			{
				AddGroup("");
			}
		}

		public override void ServerLoad()
		{
			Cursor.Current=Cursors.WaitCursor;
			SchemaAccountManager.PacketType packet=new SchemaAccountManager.PacketType();
			packet.AddGetUsersList_Request(new SchemaString(""));
			SchemaAccountManagerDoc doc=new SchemaAccountManagerDoc();
			string output="";
			doc.SetRootElementName("","Packet");
			m_socket.ModuleCommand("AccountManager",doc.SaveXML(packet),out output);
			
			packet=new SchemaAccountManager.PacketType(doc.LoadXML(output));
			SchemaAccountManager.GetUsersList_RespondType respond=packet.GetGetUsersList_Respond();

			LoadTree(respond.GetItems());

			Changed(false);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel1 = new System.Windows.Forms.Panel();
			this.buttonSave = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.listView1 = new System.Windows.Forms.ListView();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.buttonRemoveIpProtection = new System.Windows.Forms.Button();
			this.buttonAddIpProtection = new System.Windows.Forms.Button();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.checkBoxAllCommandPrivileges = new System.Windows.Forms.CheckBox();
			this.listViewPrivileges = new System.Windows.Forms.ListView();
			this.textBoxPrivileges = new System.Windows.Forms.TextBox();
			this.buttonRemovePrivileges = new System.Windows.Forms.Button();
			this.buttonAddPrivileges = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.buttonAddAccount = new System.Windows.Forms.Button();
			this.buttonRemoveAccount = new System.Windows.Forms.Button();
			this.buttonAddGroup = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// treeView1
			// 
			this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView1.FullRowSelect = true;
			this.treeView1.HideSelection = false;
			this.treeView1.HotTracking = true;
			this.treeView1.ImageIndex = -1;
			this.treeView1.Location = new System.Drawing.Point(0, 0);
			this.treeView1.Name = "treeView1";
			this.treeView1.SelectedImageIndex = -1;
			this.treeView1.Size = new System.Drawing.Size(440, 200);
			this.treeView1.Sorted = true;
			this.treeView1.TabIndex = 0;
			this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
			// 
			// splitter1
			// 
			this.splitter1.BackColor = System.Drawing.SystemColors.Highlight;
			this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
			this.splitter1.Location = new System.Drawing.Point(0, 264);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(440, 3);
			this.splitter1.TabIndex = 1;
			this.splitter1.TabStop = false;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.buttonSave);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 467);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(440, 64);
			this.panel1.TabIndex = 2;
			// 
			// buttonSave
			// 
			this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonSave.Location = new System.Drawing.Point(16, 24);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(80, 24);
			this.buttonSave.TabIndex = 15;
			this.buttonSave.Text = "Save";
			this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(0, 0);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			// 
			// tabControl1
			// 
			this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.HotTrack = true;
			this.tabControl1.ItemSize = new System.Drawing.Size(100, 31);
			this.tabControl1.Location = new System.Drawing.Point(0, 267);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.Padding = new System.Drawing.Point(20, 5);
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.ShowToolTips = true;
			this.tabControl1.Size = new System.Drawing.Size(440, 200);
			this.tabControl1.TabIndex = 5;
			// 
			// tabPage1
			// 
			this.tabPage1.AutoScroll = true;
			this.tabPage1.Controls.Add(this.listView1);
			this.tabPage1.Controls.Add(this.checkBox1);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.textBox1);
			this.tabPage1.Controls.Add(this.buttonRemoveIpProtection);
			this.tabPage1.Controls.Add(this.buttonAddIpProtection);
			this.tabPage1.Location = new System.Drawing.Point(4, 35);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(432, 161);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "IP protection";
			this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
			// 
			// listView1
			// 
			this.listView1.Enabled = false;
			this.listView1.FullRowSelect = true;
			this.listView1.GridLines = true;
			this.listView1.Location = new System.Drawing.Point(16, 80);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(384, 112);
			this.listView1.TabIndex = 22;
			this.listView1.View = System.Windows.Forms.View.List;
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(16, 16);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(112, 24);
			this.checkBox1.TabIndex = 21;
			this.checkBox1.Text = "Enabled";
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 16);
			this.label2.TabIndex = 20;
			this.label2.Text = "IP List:";
			// 
			// textBox1
			// 
			this.textBox1.Enabled = false;
			this.textBox1.Location = new System.Drawing.Point(16, 208);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(128, 20);
			this.textBox1.TabIndex = 16;
			this.textBox1.Text = "";
			// 
			// buttonRemoveIpProtection
			// 
			this.buttonRemoveIpProtection.Enabled = false;
			this.buttonRemoveIpProtection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonRemoveIpProtection.Location = new System.Drawing.Point(264, 208);
			this.buttonRemoveIpProtection.Name = "buttonRemoveIpProtection";
			this.buttonRemoveIpProtection.Size = new System.Drawing.Size(80, 24);
			this.buttonRemoveIpProtection.TabIndex = 19;
			this.buttonRemoveIpProtection.Text = "Remove";
			this.buttonRemoveIpProtection.Click += new System.EventHandler(this.buttonRemoveIpProtection_Click);
			// 
			// buttonAddIpProtection
			// 
			this.buttonAddIpProtection.Enabled = false;
			this.buttonAddIpProtection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonAddIpProtection.Location = new System.Drawing.Point(168, 208);
			this.buttonAddIpProtection.Name = "buttonAddIpProtection";
			this.buttonAddIpProtection.Size = new System.Drawing.Size(80, 24);
			this.buttonAddIpProtection.TabIndex = 18;
			this.buttonAddIpProtection.Text = "Add";
			this.buttonAddIpProtection.Click += new System.EventHandler(this.buttonAddIpProtection_Click);
			// 
			// tabPage2
			// 
			this.tabPage2.AutoScroll = true;
			this.tabPage2.Controls.Add(this.checkBoxAllCommandPrivileges);
			this.tabPage2.Controls.Add(this.listViewPrivileges);
			this.tabPage2.Controls.Add(this.textBoxPrivileges);
			this.tabPage2.Controls.Add(this.buttonRemovePrivileges);
			this.tabPage2.Controls.Add(this.buttonAddPrivileges);
			this.tabPage2.Location = new System.Drawing.Point(4, 35);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(432, 161);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Privilegues";
			// 
			// checkBoxAllCommandPrivileges
			// 
			this.checkBoxAllCommandPrivileges.Location = new System.Drawing.Point(16, 16);
			this.checkBoxAllCommandPrivileges.Name = "checkBoxAllCommandPrivileges";
			this.checkBoxAllCommandPrivileges.TabIndex = 22;
			this.checkBoxAllCommandPrivileges.Text = "All commands";
			this.checkBoxAllCommandPrivileges.CheckedChanged += new System.EventHandler(this.checkBoxAllCommandPrivileges_CheckedChanged);
			// 
			// listViewPrivileges
			// 
			this.listViewPrivileges.Location = new System.Drawing.Point(16, 48);
			this.listViewPrivileges.Name = "listViewPrivileges";
			this.listViewPrivileges.Size = new System.Drawing.Size(384, 144);
			this.listViewPrivileges.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.listViewPrivileges.TabIndex = 21;
			this.listViewPrivileges.View = System.Windows.Forms.View.List;
			// 
			// textBoxPrivileges
			// 
			this.textBoxPrivileges.Location = new System.Drawing.Point(16, 208);
			this.textBoxPrivileges.Name = "textBoxPrivileges";
			this.textBoxPrivileges.Size = new System.Drawing.Size(128, 20);
			this.textBoxPrivileges.TabIndex = 20;
			this.textBoxPrivileges.Text = "";
			// 
			// buttonRemovePrivileges
			// 
			this.buttonRemovePrivileges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonRemovePrivileges.Location = new System.Drawing.Point(264, 208);
			this.buttonRemovePrivileges.Name = "buttonRemovePrivileges";
			this.buttonRemovePrivileges.Size = new System.Drawing.Size(80, 24);
			this.buttonRemovePrivileges.TabIndex = 19;
			this.buttonRemovePrivileges.Text = "Remove";
			this.buttonRemovePrivileges.Click += new System.EventHandler(this.buttonRemovePrivileges_Click);
			// 
			// buttonAddPrivileges
			// 
			this.buttonAddPrivileges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonAddPrivileges.Location = new System.Drawing.Point(168, 208);
			this.buttonAddPrivileges.Name = "buttonAddPrivileges";
			this.buttonAddPrivileges.Size = new System.Drawing.Size(80, 24);
			this.buttonAddPrivileges.TabIndex = 18;
			this.buttonAddPrivileges.Text = "Add";
			this.buttonAddPrivileges.Click += new System.EventHandler(this.buttonAddPrivileges_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.treeView1);
			this.panel2.Controls.Add(this.panel3);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(440, 264);
			this.panel2.TabIndex = 6;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.buttonAddAccount);
			this.panel3.Controls.Add(this.buttonRemoveAccount);
			this.panel3.Controls.Add(this.buttonAddGroup);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel3.Location = new System.Drawing.Point(0, 200);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(440, 64);
			this.panel3.TabIndex = 2;
			// 
			// buttonAddAccount
			// 
			this.buttonAddAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonAddAccount.Location = new System.Drawing.Point(120, 16);
			this.buttonAddAccount.Name = "buttonAddAccount";
			this.buttonAddAccount.Size = new System.Drawing.Size(96, 23);
			this.buttonAddAccount.TabIndex = 3;
			this.buttonAddAccount.Text = "Add Account";
			this.buttonAddAccount.Click += new System.EventHandler(this.buttonAddAccount_Click);
			// 
			// buttonRemoveAccount
			// 
			this.buttonRemoveAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonRemoveAccount.Location = new System.Drawing.Point(224, 16);
			this.buttonRemoveAccount.Name = "buttonRemoveAccount";
			this.buttonRemoveAccount.Size = new System.Drawing.Size(96, 23);
			this.buttonRemoveAccount.TabIndex = 2;
			this.buttonRemoveAccount.Text = "Remove";
			this.buttonRemoveAccount.Click += new System.EventHandler(this.buttonRemoveAccount_Click);
			// 
			// buttonAddGroup
			// 
			this.buttonAddGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonAddGroup.Location = new System.Drawing.Point(16, 16);
			this.buttonAddGroup.Name = "buttonAddGroup";
			this.buttonAddGroup.Size = new System.Drawing.Size(96, 23);
			this.buttonAddGroup.TabIndex = 1;
			this.buttonAddGroup.Text = "Add group";
			this.buttonAddGroup.Click += new System.EventHandler(this.buttonAddGroup_Click);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(0, 0);
			this.button6.Name = "button6";
			this.button6.TabIndex = 0;
			// 
			// FormAccountManager
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(440, 531);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panel2);
			this.Name = "FormAccountManager";
			this.Text = "Account Manager";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.FormAccountManager_Closing);
			this.panel1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonAddGroup_Click(object sender, System.EventArgs e)
		{
			FormGroupEdit dlg=new FormGroupEdit();
			if(dlg.ShowDialog(this)!=DialogResult.OK)
				return;

			string str=dlg.textBox1.Text.ToString();
			if(str.Length==0)
				return;

			AddGroup(str);
			Changed(true);
		}

		void AddGroup(string name)
		{
			ItemType item=new ItemType();
			item.AddIsGroup(new SchemaString(""));
			item.AddName(new SchemaString(name));
			item.AddGroup(new SchemaString(""));
			IpProtectionType ipprotection=new IpProtectionType();
			ipprotection.AddEnableIpProtection(new SchemaBoolean(false));
			item.AddIpProtection(ipprotection);
			item.AddPrivileges(new PrivilegesType());
			AddItem(item);
		}


		TreeNode AddVisibleGroup(string name)
		{
			// ��� ������� ������
			string groupname=name;
			if(groupname.Length==0)
			{
				groupname="Others(null)";
			}
			TreeNode node=treeView1.Nodes.Add(groupname);
			m_groups.Add(name,node);
			return node;
		}

		/// <summary>
		/// ��������� ������� � ������ �����������
		/// � ����� ������ �������� �� ������������ �� ��������� �������
		/// 
		/// ��������� ������ � ����� ������ ��� �������� ������
		/// </summary>
		void AddItem(ItemType item)
		{
			if(item.HasIsGroup())
			{
				int index=m_groups.IndexOfKey(item.GetName().ToString());
				TreeNode node;
				if(index!=-1)
					node=(TreeNode)m_groups[item.GetName().ToString()];
				else
				{
					node=AddVisibleGroup(item.GetName().ToString());
				}
				node.Tag=item;
				// � ��� �����������
			}
			else
			{
				TreeNode root;
				int index=m_groups.IndexOfKey(item.GetGroup().ToString());
				if(index==-1)
				{
					root=AddVisibleGroup(item.GetGroup().ToString());
					// ��� � �� ������������ Tag, ��������� �� ����������
					// � ������ ����� XML ������� �������� �� ��� ������
				}
				else
				{
					root=(TreeNode)m_groups.GetByIndex(index);
				}

				TreeNode node=root.Nodes.Add(item.GetName().ToString());
				node.Tag=(Object)item;
			}
		}

		private void buttonRemoveAccount_Click(object sender, System.EventArgs e)
		{
			TreeNode node=treeView1.SelectedNode;
			if(node==null)
				return;

			ItemType item=(ItemType)node.Tag;

			if(node.Nodes.Count>0)
			{
				if(item!=null&&item.GetName().ToString()=="")
					return;

				foreach(TreeNode childnode in node.Nodes)
				{
					ItemType childitem=(ItemType)childnode.Tag;
					if(childitem!=null)
					{
						childitem.RemoveGroup();
						childitem.AddGroup(new SchemaString(""));
						AddItem(childitem);
					}
				}
			}
			else
			{
				if(item!=null)
				{
					item.RemoveGroup();
					item.AddGroup(new SchemaString(""));
					AddItem(item);
				}
			}

			node.Remove();
			Changed(true);
		}

		class EachNode:IEnumerable
		{
			class Node:IEnumerator
			{
				TreeNodeCollection m_collection;
				Stack stack=new Stack();
				TreeNodeCollection m_currentcollection;
				int m_currentindex;

				public Node(TreeNodeCollection collection)
				{
					m_collection=collection;

					Reset();
				}

				#region IEnumerator Members

				public void Reset()
				{
					stack.Clear();
					stack.Push(m_collection.GetEnumerator());
				}

				public object Current
				{
					get
					{
						return ((IEnumerator)stack.Peek()).Current;
					}
				}

				public bool MoveNext()
				{
					IEnumerator e=(IEnumerator)stack.Peek();
					if(!e.MoveNext())
					{
						stack.Pop();
						if(stack.Count<=0)
							return false;
					}
					TreeNode node=(TreeNode)e.Current;
					if(node.Nodes.Count>0)
					{
						IEnumerator ee=node.Nodes.GetEnumerator();
						stack.Push(ee);
						ee.MoveNext();
						return true;
					}
					return true;
				}

				#endregion
			};

			TreeNodeCollection m_tree;
			public EachNode(TreeNodeCollection tree)
			{
				m_tree=tree;
			}
			#region IEnumerable Members

			public IEnumerator GetEnumerator()
			{
				return new Node(m_tree);
			}

			#endregion
		}

		private void buttonAddAccount_Click(object sender, System.EventArgs e)
		{
			Cursor.Current=Cursors.WaitCursor;

			FormAddAccount dlg=new FormAddAccount();

			foreach(TreeNode node in new EachNode(treeView1.Nodes))
			{
				ItemType item=(ItemType)node.Tag;
				if(!item.HasIsGroup())
				{
					ListViewItem lvi=dlg.listView1.Items.Add(item.GetName().ToString());
					lvi.Tag=item;
				}
			}

			if(dlg.ShowDialog(this)!=DialogResult.OK)
				return;

			string groupname=((ItemType)treeView1.SelectedNode.Tag).GetName().ToString();

			foreach(ListViewItem item in dlg.listView1.Items)
			{
				if(item.Checked)
				{
					AddToGroup(groupname,(ItemType)item.Tag);
				}
			}

			Changed(true);
		}

		void RemoveFromTree(TreeNodeCollection nodes,object obj)
		{
			// ��� � ������ � ������ ��� ������
			for(int i=nodes.Count-1;i>=0;i--)
			{
				RemoveFromTree(nodes[i].Nodes,obj);
				ItemType item=(ItemType)nodes[i].Tag;
				if(!item.HasIsGroup())
				{
					if(item.GetName().ToString()==((ItemType)obj).GetName().ToString())
					{
						nodes.RemoveAt(i);
					}
				}
			}
		}

		void AddToGroup(string groupname,ItemType item)
		{
			foreach(TreeNode node in new EachNode(treeView1.Nodes))
			{
				ItemType treeitem=(ItemType)node.Tag;
				if(!treeitem.HasIsGroup())
				{
					if(treeitem.GetName().ToString()==item.GetName().ToString())
					{
						node.Remove();
					}
				}
			}

			item.RemoveGroup();
			item.AddGroup(new SchemaString(groupname));
			AddItem(item);
		}

		private void treeView1_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			m_loading=true;

			ItemType item=(ItemType)e.Node.Tag;

			tabControl1.Enabled=item!=null;
			if(item==null)
			{
				return;
			}
			
		{
			IpProtectionType ipprotection=item.GetIpProtection();
			listView1.Tag=ipprotection;
			checkBox1.Tag=ipprotection;
			checkBox1.Checked=ipprotection.GetEnableIpProtection().Value;
			listView1.Items.Clear();
			for(int i=0;i<ipprotection.GetIpAddressCount();i++)
			{
				AddIpAddress(ipprotection.GetIpAddressAt(i).Value);
			}
		}

		{
			PrivilegesType privileges=item.GetPrivileges();
			listViewPrivileges.Tag=privileges;
			checkBoxAllCommandPrivileges.Tag=privileges;
			
			checkBoxAllCommandPrivileges.Checked=privileges.HasDisableProtection();
			listViewPrivileges.Items.Clear();
			for(int i=0;i<privileges.GetValidCommandsCount();i++)
			{
				listViewPrivileges.Items.Add(privileges.GetValidCommandsAt(i).ToString());
			}
		}

			m_loading=false;
		}

		private void checkBox1_CheckedChanged(object sender, System.EventArgs e)
		{
			IpProtectionType ipprotection=(IpProtectionType)checkBox1.Tag;
			ipprotection.RemoveEnableIpProtection();
			ipprotection.AddEnableIpProtection(new SchemaBoolean(checkBox1.Checked));

			listView1.Enabled=checkBox1.Checked;
			textBox1.Enabled=checkBox1.Checked;
			buttonAddIpProtection.Enabled=checkBox1.Checked;
			buttonRemoveIpProtection.Enabled=checkBox1.Checked;

			if(!m_loading)
				Changed(true);
		}

		void AddIpAddress(int ip)
		{
			System.Net.IPAddress ip_val=new System.Net.IPAddress(ip);
			listView1.Items.Add(ip_val.ToString());
		}

		private void buttonAddIpProtection_Click(object sender, System.EventArgs e)
		{
			try
			{
				IpProtectionType ipprotection=(IpProtectionType)listView1.Tag;

				System.Net.IPHostEntry ip=System.Net.Dns.Resolve(textBox1.Text);
				int ip_val=(int)((System.Net.IPAddress)ip.AddressList.GetValue(0)).Address;
				ipprotection.AddIpAddress(new SchemaInteger(ip_val));

				AddIpAddress(ip_val);

				textBox1.Text="";
			}
			catch
			{
			}
			Changed(true);
		}

		private void buttonRemoveIpProtection_Click(object sender, System.EventArgs e)
		{
			IpProtectionType ipprotection=(IpProtectionType)listView1.Tag;

			ipprotection.RemoveIpAddress();

			foreach(ListViewItem lvi in listView1.SelectedItems)
			{
				for(int i=ipprotection.GetIpAddressCount()-1;i>=0;i++)
				{
					if(ipprotection.GetIpAddressAt(i).ToString()==lvi.Text)
					{
						ipprotection.RemoveIpAddressAt(i);
					}
				}
				lvi.Remove();
			}
			Changed(true);
		}

		private void buttonAddPrivileges_Click(object sender, System.EventArgs e)
		{
			PrivilegesType privileges=(PrivilegesType)listViewPrivileges.Tag;

			listViewPrivileges.Items.Add(textBoxPrivileges.Text);
			privileges.AddValidCommands(new SchemaString(textBoxPrivileges.Text));
			textBoxPrivileges.Text="";
			Changed(true);
		}

		private void buttonRemovePrivileges_Click(object sender, System.EventArgs e)
		{
			PrivilegesType privileges=(PrivilegesType)listViewPrivileges.Tag;

			privileges.RemoveValidCommands();

			foreach(ListViewItem lvi in listViewPrivileges.SelectedItems)
			{
				for(int i=privileges.GetValidCommandsCount()-1;i>=0;i++)
				{
					if(privileges.GetValidCommandsAt(i).ToString()==lvi.Text)
					{
						privileges.RemoveValidCommandsAt(i);
					}
				}
				lvi.Remove();
			}
			Changed(true);
		}

		private void buttonSave_Click(object sender, System.EventArgs e)
		{
			ServerSave();
		}

		public override void ServerSave()
		{
			Cursor.Current=Cursors.WaitCursor;

			SchemaAccountManager.ItemsType items=new SchemaAccountManager.ItemsType();
			foreach(TreeNode node in new EachNode(treeView1.Nodes))
				items.AddItem((ItemType)node.Tag);

			SchemaAccountManager.PacketType packet=new SchemaAccountManager.PacketType();
			SetUsersListType request=new SetUsersListType();
			request.AddItems(items);
			packet.AddSetUsersList(request);
			SchemaAccountManagerDoc doc=new SchemaAccountManagerDoc();
			string output="";
			doc.SetRootElementName("","Packet");
			m_socket.ModuleCommand("AccountManager",doc.SaveXML(packet),out output);

			Changed(false);
		}

		private void tabPage1_Click(object sender, System.EventArgs e)
		{
		
		}

		private void FormAccountManager_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel=true;
			if(!((FormMain)Parent.Parent).PreCloseDocument(this))
				return;
			Hide();
		}

		private void checkBoxAllCommandPrivileges_CheckedChanged(object sender, System.EventArgs e)
		{
			PrivilegesType privileges=(PrivilegesType)checkBoxAllCommandPrivileges.Tag;

			listViewPrivileges.Enabled=!checkBoxAllCommandPrivileges.Checked;
			textBoxPrivileges.Enabled=!checkBoxAllCommandPrivileges.Checked;
			buttonAddPrivileges.Enabled=!checkBoxAllCommandPrivileges.Checked;
			buttonRemovePrivileges.Enabled=!checkBoxAllCommandPrivileges.Checked;

			if(!m_loading)
			{
				privileges.RemoveDisableProtection();
				if(checkBoxAllCommandPrivileges.Checked)
					privileges.AddDisableProtection(new SchemaString(""));

				Changed(true);
			}
		}
	}
}
