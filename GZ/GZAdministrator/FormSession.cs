using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace GZAdministrator
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormSession : ModuleSheet
	{
		private System.Windows.Forms.Button buttonConnect;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxHost;
		private System.Windows.Forms.TextBox textBoxPort;

		ServerAddinIO.Socket m_socket;

		public FormSession(ServerAddinIO.Socket socket)
		{
			m_socket=socket;

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonConnect = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.textBoxHost = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxPort = new System.Windows.Forms.TextBox();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonConnect
			// 
			this.buttonConnect.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.buttonConnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonConnect.Location = new System.Drawing.Point(16, 16);
			this.buttonConnect.Name = "buttonConnect";
			this.buttonConnect.Size = new System.Drawing.Size(88, 32);
			this.buttonConnect.TabIndex = 0;
			this.buttonConnect.Text = "Connect";
			this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
			// 
			// buttonCancel
			// 
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.buttonCancel.Location = new System.Drawing.Point(120, 16);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(88, 32);
			this.buttonCancel.TabIndex = 7;
			this.buttonCancel.Text = "Disconnect";
			this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
			// 
			// textBoxHost
			// 
			this.textBoxHost.Location = new System.Drawing.Point(24, 56);
			this.textBoxHost.Name = "textBoxHost";
			this.textBoxHost.Size = new System.Drawing.Size(184, 20);
			this.textBoxHost.TabIndex = 5;
			this.textBoxHost.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 16);
			this.label1.TabIndex = 4;
			this.label1.Text = "Host";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.buttonConnect);
			this.panel1.Controls.Add(this.buttonCancel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 207);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(360, 64);
			this.panel1.TabIndex = 8;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(240, 24);
			this.label2.Name = "label2";
			this.label2.TabIndex = 9;
			this.label2.Text = "Port";
			// 
			// textBoxPort
			// 
			this.textBoxPort.Location = new System.Drawing.Point(240, 56);
			this.textBoxPort.Name = "textBoxPort";
			this.textBoxPort.Size = new System.Drawing.Size(80, 20);
			this.textBoxPort.TabIndex = 10;
			this.textBoxPort.Text = "48989";
			// 
			// FormSession
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(360, 271);
			this.Controls.Add(this.textBoxPort);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.textBoxHost);
			this.Controls.Add(this.label1);
			this.Name = "FormSession";
			this.Text = "Session";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.FormSession_Closing);
			this.Load += new System.EventHandler(this.FormSession_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void FormSession_Load(object sender, System.EventArgs e)
		{
		
		}

		private void buttonConnect_Click(object sender, System.EventArgs e)
		{
			try
			{
				Cursor.Current=Cursors.WaitCursor;
				m_socket.Connect(textBoxHost.Text,System.Convert.ToInt32(textBoxPort.Text));
			}
			catch
			{
				MessageBox.Show(this,"can't connect","Connect",MessageBoxButtons.OK,MessageBoxIcon.Hand);
			}
		}

		private void buttonCancel_Click(object sender, System.EventArgs e)
		{
			try
			{
				m_socket.Disconnect();
			}
			catch
			{
			}
		}

		private void FormSession_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel=true;
			Hide();
		}
	}
}
