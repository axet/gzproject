//
// ItemType.cs.cs
//
// This file was generated by XMLSPY 5 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSPY Documentation for further details.
// http://www.altova.com/xmlspy
//


using System;
using System.Xml;
using Altova.Types;

namespace SchemaAccountManager
{
	public class ItemType : Altova.Node
	{
		#region Forward constructors
		public ItemType() : base() {}
		public ItemType(XmlDocument doc) : base(doc) {}
		public ItemType(XmlNode node) : base(node) {}
		public ItemType(Altova.Node node) : base(node) {}
		#endregion // Forward constructors

		#region Name accessor methods
		public int GetNameMinCount()
		{
			return 1;
		}

		public int GetNameMaxCount()
		{
			return 1;
		}

		public int GetNameCount()
		{
			return DomChildCount(NodeType.Element, "", "Name");
		}

		public bool HasName()
		{
			return HasDomChild(NodeType.Element, "", "Name");
		}

		public SchemaString GetNameAt(int index)
		{
			return new SchemaString(GetDomNodeValue(GetDomChildAt(NodeType.Element, "", "Name", index)));
		}

		public SchemaString GetName()
		{
			return GetNameAt(0);
		}

		public void RemoveNameAt(int index)
		{
			RemoveDomChildAt(NodeType.Element, "", "Name", index);
		}

		public void RemoveName()
		{
			while (HasName())
				RemoveNameAt(0);
		}

		public void AddName(SchemaString newValue)
		{
			AppendDomChild(NodeType.Element, "", "Name", newValue.ToString());
		}

		public void InsertNameAt(SchemaString newValue, int index)
		{
			InsertDomChildAt(NodeType.Element, "", "Name", index, newValue.ToString());
		}

		public void ReplaceNameAt(SchemaString newValue, int index)
		{
			ReplaceDomChildAt(NodeType.Element, "", "Name", index, newValue.ToString());
		}
		#endregion // Name accessor methods

		#region IpProtection accessor methods
		public int GetIpProtectionMinCount()
		{
			return 1;
		}

		public int GetIpProtectionMaxCount()
		{
			return 1;
		}

		public int GetIpProtectionCount()
		{
			return DomChildCount(NodeType.Element, "", "IpProtection");
		}

		public bool HasIpProtection()
		{
			return HasDomChild(NodeType.Element, "", "IpProtection");
		}

		public IpProtectionType GetIpProtectionAt(int index)
		{
			return new IpProtectionType(GetDomChildAt(NodeType.Element, "", "IpProtection", index));
		}

		public IpProtectionType GetIpProtection()
		{
			return GetIpProtectionAt(0);
		}

		public void RemoveIpProtectionAt(int index)
		{
			RemoveDomChildAt(NodeType.Element, "", "IpProtection", index);
		}

		public void RemoveIpProtection()
		{
			while (HasIpProtection())
				RemoveIpProtectionAt(0);
		}

		public void AddIpProtection(IpProtectionType newValue)
		{
			AppendDomElement("", "IpProtection", newValue);
		}

		public void InsertIpProtectionAt(IpProtectionType newValue, int index)
		{
			InsertDomElementAt("", "IpProtection", index, newValue);
		}

		public void ReplaceIpProtectionAt(IpProtectionType newValue, int index)
		{
			ReplaceDomElementAt("", "IpProtection", index, newValue);
		}
		#endregion // IpProtection accessor methods

		#region Privileges accessor methods
		public int GetPrivilegesMinCount()
		{
			return 1;
		}

		public int GetPrivilegesMaxCount()
		{
			return 1;
		}

		public int GetPrivilegesCount()
		{
			return DomChildCount(NodeType.Element, "", "Privileges");
		}

		public bool HasPrivileges()
		{
			return HasDomChild(NodeType.Element, "", "Privileges");
		}

		public PrivilegesType GetPrivilegesAt(int index)
		{
			return new PrivilegesType(GetDomChildAt(NodeType.Element, "", "Privileges", index));
		}

		public PrivilegesType GetPrivileges()
		{
			return GetPrivilegesAt(0);
		}

		public void RemovePrivilegesAt(int index)
		{
			RemoveDomChildAt(NodeType.Element, "", "Privileges", index);
		}

		public void RemovePrivileges()
		{
			while (HasPrivileges())
				RemovePrivilegesAt(0);
		}

		public void AddPrivileges(PrivilegesType newValue)
		{
			AppendDomElement("", "Privileges", newValue);
		}

		public void InsertPrivilegesAt(PrivilegesType newValue, int index)
		{
			InsertDomElementAt("", "Privileges", index, newValue);
		}

		public void ReplacePrivilegesAt(PrivilegesType newValue, int index)
		{
			ReplaceDomElementAt("", "Privileges", index, newValue);
		}
		#endregion // Privileges accessor methods

		#region Group accessor methods
		public int GetGroupMinCount()
		{
			return 1;
		}

		public int GetGroupMaxCount()
		{
			return 1;
		}

		public int GetGroupCount()
		{
			return DomChildCount(NodeType.Element, "", "Group");
		}

		public bool HasGroup()
		{
			return HasDomChild(NodeType.Element, "", "Group");
		}

		public SchemaString GetGroupAt(int index)
		{
			return new SchemaString(GetDomNodeValue(GetDomChildAt(NodeType.Element, "", "Group", index)));
		}

		public SchemaString GetGroup()
		{
			return GetGroupAt(0);
		}

		public void RemoveGroupAt(int index)
		{
			RemoveDomChildAt(NodeType.Element, "", "Group", index);
		}

		public void RemoveGroup()
		{
			while (HasGroup())
				RemoveGroupAt(0);
		}

		public void AddGroup(SchemaString newValue)
		{
			AppendDomChild(NodeType.Element, "", "Group", newValue.ToString());
		}

		public void InsertGroupAt(SchemaString newValue, int index)
		{
			InsertDomChildAt(NodeType.Element, "", "Group", index, newValue.ToString());
		}

		public void ReplaceGroupAt(SchemaString newValue, int index)
		{
			ReplaceDomChildAt(NodeType.Element, "", "Group", index, newValue.ToString());
		}
		#endregion // Group accessor methods

		#region IsGroup accessor methods
		public int GetIsGroupMinCount()
		{
			return 0;
		}

		public int GetIsGroupMaxCount()
		{
			return 1;
		}

		public int GetIsGroupCount()
		{
			return DomChildCount(NodeType.Element, "", "IsGroup");
		}

		public bool HasIsGroup()
		{
			return HasDomChild(NodeType.Element, "", "IsGroup");
		}

		public SchemaString GetIsGroupAt(int index)
		{
			return new SchemaString(GetDomNodeValue(GetDomChildAt(NodeType.Element, "", "IsGroup", index)));
		}

		public SchemaString GetIsGroup()
		{
			return GetIsGroupAt(0);
		}

		public void RemoveIsGroupAt(int index)
		{
			RemoveDomChildAt(NodeType.Element, "", "IsGroup", index);
		}

		public void RemoveIsGroup()
		{
			while (HasIsGroup())
				RemoveIsGroupAt(0);
		}

		public void AddIsGroup(SchemaString newValue)
		{
			AppendDomChild(NodeType.Element, "", "IsGroup", newValue.ToString());
		}

		public void InsertIsGroupAt(SchemaString newValue, int index)
		{
			InsertDomChildAt(NodeType.Element, "", "IsGroup", index, newValue.ToString());
		}

		public void ReplaceIsGroupAt(SchemaString newValue, int index)
		{
			ReplaceDomChildAt(NodeType.Element, "", "IsGroup", index, newValue.ToString());
		}
		#endregion // IsGroup accessor methods
	}
}
