using System;
//using 
using SchemaSpeedHack;

namespace SpeedHackIO
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Socket
	{
		const string m_modulename="SpeedHack";
		ServerAddinIO.Socket m_socket;

		public Socket(ref ServerAddinIO.Socket socket)
		{
			m_socket=socket;	
		}

		public SchemaSpeedHack.RespondType GetProperty()
		{
			string xmloutput;

			SchemaSpeedHack.SchemaSpeedHackDoc doc=new SchemaSpeedHack.SchemaSpeedHackDoc();
			SchemaSpeedHack.PacketType packet=new SchemaSpeedHack.PacketType();
			SchemaSpeedHack.GetPropertyType property=new SchemaSpeedHack.GetPropertyType();
			property.AddRequest(new Altova.Types.SchemaString(""));
			packet.AddGetProperty(property);
			doc.SetRootElementName("","Packet");
			string xmlinput=doc.SaveXML(packet);
			m_socket.ModuleCommand(m_modulename,xmlinput,out xmloutput);

			packet=new SchemaSpeedHack.PacketType(doc.LoadXML(xmloutput));
			return packet.GetGetProperty().GetRespond();
		}

		public void SetProperty(SchemaSpeedHack.SetPropertyType property)
		{
			SchemaSpeedHack.SchemaSpeedHackDoc doc=new SchemaSpeedHack.SchemaSpeedHackDoc();
			SchemaSpeedHack.PacketType packet=new SchemaSpeedHack.PacketType();

			packet.AddSetProperty(property);
			doc.SetRootElementName("","Packet");
			string xmlinput=doc.SaveXML(packet);
			string xmloutput;
			m_socket.ModuleCommand(m_modulename,xmlinput,out xmloutput);
		}
	}
}
