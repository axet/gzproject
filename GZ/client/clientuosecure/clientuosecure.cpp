#include "clientuosecure.h"

#include "../../client/packets.h"
#include "debugger/debug.h"

ClientUOSecure::CClientUOSecure::CClientUOSecure():
  m_newclient(true),
  m_firstpackage(false),
  m_enablesecure(false),
  m_emulatecrypt(ClientIdentify::clientUnknown)
{
  CatchImportFunction("accept","wsock32.dll",0,hook_accept_leave);
  CatchImportFunction("recv","wsock32.dll",hook_recv,hook_recv_leave);
  CatchImportFunction("send","wsock32.dll",hook_send,hook_send_leave);
  CatchImportFunction("closesocket","wsock32.dll",hook_closesocket);
  CatchImportFunction("connect","wsock32.dll",hook_connect,hook_connect_leave);
}

void ClientUOSecure::CClientUOSecure::hook_closesocket(CMemMngr* mngr,va_list args)
{
  CClientUOSecure* current=dynamic_cast<CClientUOSecure*>(mngr);
}

void ClientUOSecure::CClientUOSecure::hook_accept(CMemMngr* mngr,va_list args)
{
  CClientUOSecure* current=dynamic_cast<CClientUOSecure*>(mngr);
}

void ClientUOSecure::CClientUOSecure::hook_accept_leave(CMemMngr* mngr,int *retval)
{
  CClientUOSecure* current=dynamic_cast<CClientUOSecure*>(mngr);
}

void ClientUOSecure::CClientUOSecure::hook_recv(CMemMngr* mngr,va_list args)
{
  CClientUOSecure* current=dynamic_cast<CClientUOSecure*>(mngr);
}

void ClientUOSecure::CClientUOSecure::hook_recv_leave(CMemMngr* mngr,int *retval)
{
  CClientUOSecure* current=dynamic_cast<CClientUOSecure*>(mngr);
}

CClientCrypt::crypt_tag ClientUOSecure::CClientUOSecure::Choise(char* buf,crypt_tag x,crypt_tag y)
{
  return ((unsigned char)*buf)==Packets::scPostLogin?y:
    ((unsigned char)*buf)==Packets::scLogin?x:
    throw std::exception("bad first data package");
}

void ClientUOSecure::CClientUOSecure::hook_send(CMemMngr* mngr,va_list args)
{
  try
  {
    CClientUOSecure* current=dynamic_cast<CClientUOSecure*>(mngr);

    SOCKET s=va_arg(args,SOCKET);
    char* buf=va_arg(args,char*);
    int len=va_arg(args,int);
    int flags=va_arg(args,int);

    if(current->m_newclient)
    {
      if(current->m_emulatecrypt==ClientIdentify::clientSphereClient)
        *reinterpret_cast<unsigned*>(buf)=0;

      current->SetKey(*reinterpret_cast<unsigned*>(buf));
      buf+=sizeof(unsigned);
      len-=sizeof(unsigned);
      current->m_newclient=false;
    }

    // установка шифрования
    if(len>0&&!current->m_firstpackage&&!current->m_newclient)
    {
      crypt_tag newcrypt;
      switch(current->m_emulatecrypt)
      {
      case ClientIdentify::clientSphereClient:
        newcrypt=cryptNone;
        break;
      case ClientIdentify::client200:
        newcrypt=Choise(buf,crypt200login,crypt200game);
        break;
      case ClientIdentify::client203:
        newcrypt=Choise(buf,crypt203login,crypt203game);
        break;
      case ClientIdentify::clientUnknown:
        newcrypt=cryptUnknown;
        break;
      default:
        throw std::exception("not supported emulation");
      };
      current->SetEncryption(newcrypt);
      current->m_firstpackage=true;
      current->m_secureenabled=current->m_enablesecure;
    }

    current->CClientCrypt::Encrypt(buf,len);

    if(current->m_secureenabled)
      current->SecureConnection::Encrypt((unsigned char*)buf,(unsigned char*)buf+len);
  }catch(std::exception &e)
  {
    DBGTRACE_ERROR("%s\n",e.what());
  }
} 

void ClientUOSecure::CClientUOSecure::hook_send_leave(CMemMngr* mngr,int *retval)
{
  CClientUOSecure* current=dynamic_cast<CClientUOSecure*>(mngr);
}

void ClientUOSecure::CClientUOSecure::hook_connect(CMemMngr* mngr,va_list args)
{
  CClientUOSecure* current=dynamic_cast<CClientUOSecure*>(mngr);
}

void ClientUOSecure::CClientUOSecure::hook_connect_leave(CMemMngr* mngr,int *retval)
{
  CClientUOSecure* current=dynamic_cast<CClientUOSecure*>(mngr);

  if(*retval==0)
  {
    current->m_newclient=true;
    current->m_firstpackage=false;
  }
}

void ClientUOSecure::CClientUOSecure::EmulateCrypt(ClientIdentify::clientVesion ct)
{
  DisableClientCrypt();
  m_emulatecrypt=ct;
}

void ClientUOSecure::CClientUOSecure::EnableSecure()
{
  m_enablesecure=true;
}
