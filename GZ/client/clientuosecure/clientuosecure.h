#ifndef __CLIENTUOSECURE_H__
#define __CLIENTUOSECURE_H__

#include <winsock2.h>

#include "../../clientsecure/clientsecure.h"
#include "iofluke/memmngr.h"
#include "../../client/clientcrypt/clientcrypt.h"
#include "../../clientaddin/clientidentify/clientidentify.h"
#include "../../clientsecure/secureconnection.h"

namespace ClientUOSecure
{
  class CClientUOSecure;
}

class ClientUOSecure::CClientUOSecure:
  public IOFluke::CMemMngr,private CClientCrypt,
  public ClientSecure::SecureConnection
{
  static void hook_closesocket(CMemMngr* mngr,va_list args);
  static void hook_accept(CMemMngr* mngr,va_list args);
  static void hook_accept_leave(CMemMngr* mngr,int *retval);
  static void hook_recv(CMemMngr* mngr,va_list args);
  static void hook_recv_leave(CMemMngr* mngr,int *retval);
  static void hook_send(CMemMngr* mngr,va_list args);
  static void hook_send_leave(CMemMngr* mngr,int *retval);
  static void hook_connect(CMemMngr* mngr,va_list args);
  static void hook_connect_leave(CMemMngr* mngr,int *retval);

  static crypt_tag Choise(char* buf,crypt_tag login,crypt_tag postlogin);

protected:
  // новое соеденение, еще не одного пакета не послано, m_newclient=true
  // еще не получен пакет с ключем
  bool m_newclient;
  // первый пакет с данными еще не получен - m_firstpackage=false
  // Но уже получен пакет с ключем (первый пакет)
  bool m_firstpackage;
  // m_enablesecure - начать шифрование при первой возможности
  bool m_enablesecure;

  ClientIdentify::clientVesion m_emulatecrypt;

public:
  CClientUOSecure();

  virtual void DisableClientCrypt() = 0;
  void EmulateCrypt(ClientIdentify::clientVesion ct);
  void EnableSecure();
};

#endif
