#include <winsock2.h>

#include "packets.h"

// {

const char* Packets::scsAcctLoginFail::GetErrorText(Server::ServerByte* p)
{
  scsAcctLoginFail *pp=(scsAcctLoginFail*)p;
  switch(pp->error)
  {
  case 0:
    return "Login Failed: No Account";
  case 1:
    return "Login Failed: Account in Use";
  case 2:
    return "Login Failed: Account Blocked";
  case 3:
    return "Login Failed: No Password";
  default:
    return "Login Failed: Server communicate error";
  }
}

Packets::scsOpenGump::scsOpenGump()
{
  ;
}

Server::ServerInt Packets::scsOpenGump::GetGumpSer()
{
  return m_gump.gumpser;
}

int Packets::scsOpenGump::GetItemsCount()
{
  return m_list.size();
}

void Packets::scsOpenGump::operator = (const void* p)
{
  scsOpenGump_tag *gump;

  if(((scsOpenGumpExt_tag*)p)->unk==0x33)
    gump=(scsOpenGump_tag*)((char*)p+12);
  else
    gump=(scsOpenGump_tag*)((char*)p+10);

  m_gump=((scsOpenGumpExt_tag*)p)->gump;

  for(int i=0;i<gump->numberofitems;i++)
  {
    m_list.push_back(gump->item[i]);
  }
}

Packets::scsOpenGump::item_tag* Packets::scsOpenGump::GetItem(int i)
{
  return &m_list[i];
}

// }

// {

Packets::scsDisplayGump::scsDisplayGump()
{
  ;
}

Packets::scsDisplayGump::scsDisplayGump(const void* p)
{
  scsDisplayGump_tag* gump=(scsDisplayGump_tag*)p;
  m_gumpinfo=gump->gumpinfo;
  m_name=std::string(gump->txt,gump->txtlen);

  p=gump->txt+gump->txtlen;
  char numoflines=*(char*)p;
  scsDisplayGump_tag::line_tag* line=(scsDisplayGump_tag::line_tag*)((char*)p+1);
  while(numoflines--)
  {
    m_lines.push_back(line_tag(line->id,line->checked,line->txt,line->txtlen));
    line=(scsDisplayGump_tag::line_tag*)(line->txt+line->txtlen);
  }
}

Server::ServerInt Packets::scsDisplayGump::GetGumpSer()
{
  return m_gumpinfo.gumpserial;
}

Server::ServerShort Packets::scsDisplayGump::GetGumpId()
{
  return m_gumpinfo.gumpid;
}

int Packets::scsDisplayGump::GetLineCount()
{
  return m_lines.size();
}

Packets::scsDisplayGump::line_tag* Packets::scsDisplayGump::GetLine(int index)
{
  return &m_lines[index];
}

int Packets::scsDisplayGump::FindIndex(Server::ServerShort itemid)
{
  for(int i=0;i<(short)m_lines.size();i++)
  {
    if(m_lines[i].itemid==itemid)
      return i+1;
  }
  return -1;
}

Packets::scsSpeaking::scsSpeaking(unsigned char* buf)
{
  packet1_tag *p1=(packet1_tag*)buf;
  packet.assign(buf,buf+p1->size);
}

const char *Packets::scsSpeaking::text()
{
  packet1_tag *p1=(packet1_tag*)&packet.front();
  return (char*)++p1;
}

void Packets::scsSpeaking::operator = (const char* text)
{
  int textlen=strlen(text)+1;
  packet.resize(sizeof(packet1_tag)+textlen);
  packet1_tag *p1=(packet1_tag *)&packet.front();
  packet2_tag *p2=(packet2_tag *)(p1+1);
  memset(p1,0,packet.size());

  p1->p.id=scSpeaking;
  p1->size=packet.size();
  p1->color=0xb203;
  p1->font=0x0300;
  char System[]="System";
  memcpy(p1->name,System,sizeof(System));
  memcpy(p2,text,textlen);
}

inline WORD EXCH(WORD w)
{
  __asm
  {
    mov ax,w
    xchg al,ah;
    mov w,ax;
  }

  return w;
}

Packets::scsSpeakingUnicode::scsSpeakingUnicode(const unsigned char* buf,int bufsize)
{
  int len=(bufsize-sizeof(info))/2;
  BSTR b=SysAllocStringLen(0,len);
  WORD *input=(WORD*)(buf+sizeof(info));
  WORD *output=(WORD*)b;
  while(*output++=EXCH(*input++))
    ;

  text.Attach(b);
}

Packets::scsClientTalk::scsClientTalk(const unsigned char* buf,int bufsize)
{
  text=(char*)buf+sizeof(info_tag);
}

//

Server::ServerShort::ServerShort()
{
  ;
}

Server::ServerShort::ServerShort(unsigned short s)
{
  m_value=htons(s);
}

Server::ServerShort::operator unsigned short()
{
  return htons(m_value);
}

Server::ServerShort& Server::ServerShort::operator +=(int i)
{
  m_value=htons(htons(m_value)+i);
  return *this;
}

Server::ServerInt::ServerInt()
{
  ;
}

Server::ServerInt::ServerInt(unsigned int s)
{
  m_value=htonl(s);
}

bool Server::ServerInt::operator <(const ServerInt &s)const
{
  return m_value<s.m_value;
}

Server::ServerInt::operator unsigned int()
{
  return htonl(m_value);
}

Packets::scsCitiesAndChars::Chars_tag& Packets::scsCitiesAndChars::GetChars()
{
  return Chars;
}

Packets::scsCitiesAndChars::Cities_tag& Packets::scsCitiesAndChars::GetCities()
{
  return *(Cities_tag*)&Chars.Char[Chars.numchars];
}

int Packets::scsCitiesAndChars::GetCharCount()
{
  return Chars.numchars;
}

Packets::scsCitiesAndChars::Chars_tag::Char_tag& Packets::scsCitiesAndChars::GetCharInfo(int index)
{
  return Chars.Char[index];
}

int Packets::scsCitiesAndChars::GetCitieCount()
{
  return GetCities().numsites;
}

Packets::scsCitiesAndChars::Cities_tag::Citie_tag& Packets::scsCitiesAndChars::GetCitieInfo(int index)
{
  return GetCities().Cite[index];
}

Packets::scsEquippedMOB::scsEquippedMOB(const char*p)
{
  scsEquippedMOB* q=(scsEquippedMOB*)p;

  Item *i;

  if(q->Item_Serial&0x80000000)
  {
    this->p.id=q->p.id;
    this->Packet_Size=q->Packet_Size;
    this->Item_Serial=q->Item_Serial;
    this->Item_ID=q->Item_ID;
    this->Item_Amount=q->Item_Amount;
    this->X=q->X;
    this->Y=q->Y;
    this->Z=q->Z;
    this->Direction=q->Direction;
    this->Skin_Color=q->Skin_Color;
    this->Status=q->Status;
    this->Notoriety=q->Notoriety;
    i=(Item *)&q->m_items;
  }else
  {
    MOBSmall* q=(MOBSmall*)p;
    this->p.id=q->p.id;
    this->Packet_Size=q->Packet_Size;
    this->Item_Serial=q->Item_Serial;
    this->Item_ID=q->Item_ID;
    this->X=q->X;
    this->Y=q->Y;
    this->Z=q->Z;
    this->Direction=q->Direction;
    this->Skin_Color=q->Skin_Color;
    this->Status=q->Status;
    this->Notoriety=q->Notoriety;
    i=(Item *)(&q->Notoriety+1);
  }

  while(i->Serial!=0)
  {
    m_items.push_back(*i);
    if(i->Item_ID&0x8000)
      i++;
    else
      i=(Item*)(((ItemSmall*)i)+1);
  };
}

Packets::scsShopSell::scsShopSell(Server::ServerByte*p)
{
  (*this)=p;
}

void Packets::scsShopSell::operator = (Server::ServerByte*p)
{
  tag1 *q=(tag1*)p;
  m_serial=q->Serial;

  tag2 *qq=(tag2*)(q+1);
  for(int i=0;i<q->Number_of_Items;i++)
  {
    Item m;
    m.serial=qq->Item_Serial;
    m.item=qq->Item_ID;
    m.color=qq->Item_Color;
    m.amount=qq->Item_Amount;
    m.value=qq->Value;
    m.name=std::string(&qq->Name,&qq->Name+qq->Name_Length);
    qq=(tag2*)(&qq->Name+qq->Name_Length);
    m_items.push_back(m);
  }
}

Packets::scsShopOffer::scsShopOffer(unsigned serial,const Items& items)
{
  tag1 t;
  t.pack.id=scShopOffer;
  t.Packet_Size=sizeof(t);
  t.Vendor_Serial=serial;
  t.Unknown=0;
  t.Number_of_Items=items.size();
  m_pack.insert(m_pack.end(),(const char*)&t,(const char*)&t+sizeof(t));

  for(Items::const_iterator i=items.begin();i!=items.end();i++)
  {
    tag1* t=((tag1*)&m_pack.front());
    t->Packet_Size=t->Packet_Size+sizeof(Item);
    m_pack.insert(m_pack.end(),(const char*)&*i,(const char*)&*i+sizeof(Item));
  }
}

Packets::scsShopOffer::operator Packets::Packet()
{
  return m_pack;
}


//ad 00 1e 00 02 b2 00 03 52 55 53 00 00 61 00 6c 00 6c 00 20 00 73 00 65 00 6c 00 6c 00 00 00 00  ­....І..RUS..a.l.l. .s.e.l.l....
//08 30 55 01 00 00 00 00 00 00 00 00 7c 78 55 01 70 78 55 01 3f 00 00 00 14 00 00 00 00 00 00 00  .0U.........|xU.pxU.?...........

Packets::scsSpeakingUnicode::scsSpeakingUnicode(const char*text)
{
  info i;
  i.p.id=Packets::scSpeakingUnicode;
  i.PacketLength=sizeof(i);
  i.Mode=0;
  i.TextColor=0x02b2;
  i.Font=0x0003;
  Server::ServerByte rus[]="RUS";
  memcpy(i.Language,rus,sizeof(rus));
  m_packet.insert(m_packet.end(),(const char*)&i,(const char*)&i+sizeof(i));
  _bstr_t t(text);
  BSTR b=t.GetBSTR();
  for(int i=0;i<=t.length();i++)
  {
    Server::ServerShort qq=*(short*)b++;
    m_packet.insert(m_packet.end(),(const char*)&qq,(const char*)&qq+2);
    ((info*)&m_packet.front())->PacketLength+=(int)2;
  }
}

Packets::scsMultiObj::scsMultiObj(Server::ServerByte*p)
{
  *this=p;
}

void Packets::scsMultiObj::operator =(Server::ServerByte*p)
{
  tag1 *q=(tag1*)p;
  Item *item=(Item*)(q+1);
  for(int i=0;i<q->Number_of_Items;i++)
  {
    m_items.push_back(*item++);
  }
}
