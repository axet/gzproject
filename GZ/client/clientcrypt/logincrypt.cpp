#include "logincrypt.h"

// Load long from address C into LL with bytes swapped.
#define N2L(C, LL) \
    LL  = ((unsigned int)(*((C)++))) << 24, \
    LL |= ((unsigned int)(*((C)++))) << 16, \
    LL |= ((unsigned int)(*((C)++))) << 8, \
    LL |= ((unsigned int)(*((C)++)))
// Store long LL at address C with bytes swapped.
#define L2N(LL, C) \
    *((C)++) = (unsigned char)(((LL) >> 24) & 0xff), \
    *((C)++) = (unsigned char)(((LL) >> 16) & 0xff), \
    *((C)++) = (unsigned char)(((LL) >> 8) & 0xff), \
    *((C)++) = (unsigned char)(((LL)) & 0xff)

LoginCrypt::LoginCrypt()
{
}

LoginCrypt::~LoginCrypt()
{
}

// private
// Used for both encryption and decryption
void LoginCrypt::encrypt(unsigned char * in, unsigned char * out, int len)
{
    for(int i = 0; i < len; i++)
    {
        out[i] = in[i] ^ static_cast<unsigned char>(m_key[0]);

        unsigned int table0 = m_key[0];
        unsigned int table1 = m_key[1];

        m_key[1] =
            (
                (
                    (
                        ((table1 >> 1) | (table0 << 31))
                        ^ m_k1
                    )
                    >> 1
                )
                | (table0 << 31)
            ) ^ m_k1;
        m_key[0] = ((table0 >> 1) | (table1 << 31)) ^ m_k2;
    }
}

void LoginCrypt::init(unsigned char * pseed, unsigned int k1, unsigned int k2)
{
    unsigned int seed;
    N2L(pseed, seed);

    m_key[0] =
            (((~seed) ^ 0x00001357) << 16)
        |   ((seed ^ 0xffffaaaa) & 0x0000ffff);
    m_key[1] =
            ((seed ^ 0x43210000) >> 16)
        |   (((~seed) ^ 0xabcdffff) & 0xffff0000);

    m_k1 = k1;
    m_k2 = k2;
}
