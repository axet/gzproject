typedef struct crypt203_tag
{
  struct crypt203first_tag
  {
    unsigned int key1[4],key2[4];
    unsigned char unk1;
    unsigned char unk2;
  };
  crypt203first_tag crypt203first;
  struct crypt203second_tag
  {
    unsigned char unk[0x124e];
  };
  crypt203second_tag crypt203second;
}crypt203_t;

extern "C" void __stdcall Crypt203SeedInit(crypt203_t* key,unsigned ip=0);
extern "C" void __stdcall Crypt203SeedEncrypt(crypt203_t* crypt,const unsigned char* input,unsigned char* output,int size);
extern "C" void __stdcall Crypt203SeedDecrypt(crypt203_t* crypt,const unsigned char* input,unsigned char* output,int size);
