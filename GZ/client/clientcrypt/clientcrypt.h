#ifndef __CLIENTCRYPT_H
#define __CLIENTCRYPT_H

#include "logincrypt.h"

class CGameCrypt
{
public:
  virtual ~CGameCrypt() {};

  virtual void Init() =0;
  virtual void Encrypt(unsigned char * in, unsigned char * out, int len) = 0;
  virtual void Decrypt(unsigned char * in, unsigned char * out, int len) = 0;
};

class CClientCrypt
{
public:
  enum crypt_tag{
    cryptNone,
    cryptUnknown,
    crypt1264login,
    crypt200login,crypt200game,
    crypt203login,crypt203game,
    crypt305login,
    crypt306j,crypt306jlogin};

  CClientCrypt();
  ~CClientCrypt();
  void SetKey(unsigned);
  unsigned GetKey();
  void SetEncryption(crypt_tag);
  crypt_tag GetEncryption(const void* buf, int bufsize);
  // возвращает текущую криптование
  crypt_tag GetEncryption();
  void Encrypt(void* buf,int bufsize);
  void Decrypt(void* buf,int bufsize);

private:
  struct loginkey_tag
  {
    crypt_tag crypt;
    int key1,key2;
  };

  crypt_tag m_crypt;
  unsigned m_key;
  static loginkey_tag m_loginkey[];
  LoginCrypt *m_logincrypt;
  CGameCrypt* m_gamecrypt;

  loginkey_tag* GetLoginKey(crypt_tag crypt);
  void CloseCrypt();
};

#endif
