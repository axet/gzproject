#include "clientcrypt.h"
#include "crypt203seed.h"

class CCrypt203:public CGameCrypt
{
  unsigned m_ip;
  crypt203_t m_key;
public:
  CCrypt203(unsigned ip);

  virtual void Init();
  // шифрует данные защитой 203
  virtual void Encrypt(unsigned char * in, unsigned char * out, int len);
  // дешефрует данные защитой 203
  virtual void Decrypt(unsigned char * in, unsigned char * out, int len);
};
