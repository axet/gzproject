; ��� ���������� ������ ���������� ��������� ��� ������������� �������
; � �����������, �������� ������� ����� �������� client203.idc � �����
; ����������������� � ������������� ����

.586

.model flat,stdcall

; Crypt203SeedInit - ��������������
;   sub_410320
; Crypt203SeedEncrypt - ����������
;   1.first crypt sub_4103F0
;   2.second crypt sub_4C8800
; Crypt203SeedDecrypt - ������������
;   1.Decrupt_sub_40F7A0

crypt203first_tag struct
  key1 DWORD 4 dup(<>)
  key2 DWORD 4 dup(<>)
  var_20h BYTE ?
  var_21h BYTE ?
crypt203first_tag ends

crypt203second_tag struct
  BYTE 0124eh dup(?)
crypt203second_tag ends

crypt203_tag struct
  ;��� ������ ����� ������ �����
  crypt203first crypt203first_tag <>
  ; ��� ������������� ��� ������� ����������
  crypt203second crypt203second_tag <>
crypt203_tag ends

.code

Crypt203SeedEncrypt proc uses eax ecx,
  key:ptr crypt203_tag,
  input:ptr dword,
  output:ptr dword,
  inputsize:dword
  
  ret
Crypt203SeedEncrypt endp

Crypt203SeedDecrypt proc uses eax ecx ,
  key:ptr crypt203_r,
  input:ptr dword,
  output:ptr dword,
  inputsize:dword

  ret
Crypt203SeedDecrypt endp

Crypt203SeedInit proc,
  crypt203:ptr crypt203_tag,
  ip:DWORD

  ret
Crypt203SeedInit endp

.data?

unk_607F48  db 19708h dup(?)

dword_D58DA0 dd ?
  db 0400h-4 dup(?)
  ;D591A0h-D58DA0h
dword_D591A0 dd ?
; D595A0h-D591A0h
  db 0400h-4 dup(?)
dword_D595A0 dd ?
  ; D599A0h-D595A0h
  db 0400h-4 dup(?)
dword_D599A0 dd ?
  db 0400h-4 dup(?)

.data
dword_54F360  dd ?

byte_507290   db ?

dword_54F364  dd ?

byte_54F160 db ?

byte_54F260 db  ?

unk_4F14B8  db  ?

unk_4F1648  db ?

unk_4F1420  db  ?

unk_4F14B6  db   ?

unk_506248  db  ?

.code

sub_4103F0 proc near
 retn
sub_4103F0 endp

Decrypt_sub_4103F0 proc near
 retn
Decrypt_sub_4103F0 endp

Decrupt_sub_40F7A0 proc near
 retn
Decrupt_sub_40F7A0 endp

sub_410320 proc near
 retn
sub_410320 endp

sub_40F7A0 proc near
 retn
sub_40F7A0 endp

sub_40F9A0 proc near
 retn
sub_40F9A0 endp

sub_410220 proc near
 retn
sub_410220 endp

CryptSecond_Init_sub_4C8770 proc near   ; ...
 retn 
CryptSecond_Init_sub_4C8770 endp

sub_4DCBE0 proc near                    ; ...
 retn
sub_4DCBE0 endp

sub_4DCC60 proc near                    ; ...
 retn
sub_4DCC60 endp

sub_4DBFA0 proc near                    ; ...
 retn
sub_4DBFA0 endp

sub_4DCCC0 proc near                    ; ...
 retn
sub_4DCCC0 endp

sub_4DB990 proc near                    ; ...
 retn
sub_4DB990 endp

sub_4DBAE0 proc near                    ; ...
 retn
sub_4DBAE0 endp

sub_4DBA50 proc near                    ; ...
 retn
sub_4DBA50 endp

sub_4DBCB0 proc near                    ; ...
 retn
sub_4DBCB0 endp

sub_4DBC60 proc near                    ; ...
 retn
sub_4DBC60 endp

CryptSecond_Game_sub_4C8800 proc near   ; ...
 retn
CryptSecond_Game_sub_4C8800 endp

end
