#include <windows.h>

#include "crypt203.h"

CCrypt203::CCrypt203(unsigned ip)
{
  m_ip=ip;
  Init();
}

void CCrypt203::Encrypt(unsigned char * in, unsigned char * out, int len)
{
  if(len>0)
    Crypt203SeedEncrypt(&m_key,in,out,len);
}

void CCrypt203::Decrypt(unsigned char * in, unsigned char * out, int len)
{
  if(len>0)
    Crypt203SeedDecrypt(&m_key,in,out,len);
}

void CCrypt203::Init()
{
  Crypt203SeedInit(&m_key,m_ip);
}
