#include "clientcrypt.h"
#include "../packets.h"
#include "../clientcrypt/crypt203.h"

CClientCrypt::loginkey_tag CClientCrypt::m_loginkey[]=
{
  {crypt1264login,0x32750719,0x0a2d100b},
  {crypt200login,0x2d13a5fd,0xa39d527f},
  {crypt203login,0x2DBBB7CD,0xA3C95E7F},
  {crypt305login,0x2c8b97ad,0xa350de7f},
  {crypt306jlogin,0x2cc3ed9d,0xa374227f}
};

CClientCrypt::CClientCrypt():
  m_logincrypt(0),m_gamecrypt(0),m_crypt(cryptUnknown),
  m_key(0)
{
  ;
}

CClientCrypt::~CClientCrypt()
{
  CloseCrypt();
}

CClientCrypt::crypt_tag CClientCrypt::GetEncryption()
{
  return m_crypt;
}

CClientCrypt::crypt_tag CClientCrypt::GetEncryption(const void* buf, int bufsize)
{
  crypt_tag crypt=cryptNone;

  std::vector<unsigned char> tempbuf;

  {
    for(int i=0;i<sizeof(m_loginkey)/sizeof(loginkey_tag);i++)
    {
      tempbuf.assign(reinterpret_cast<const unsigned char*>(buf),reinterpret_cast<const unsigned char*>(buf)+bufsize);
      LoginCrypt login_crypt;
      login_crypt.init(reinterpret_cast<unsigned char*>(&m_key),m_loginkey[i].key1,m_loginkey[i].key2);
      login_crypt.encrypt(&tempbuf.front(),&tempbuf.front(),tempbuf.size());
      Packets::scsLogin *l=(Packets::scsLogin *)&tempbuf.front();
      if(l->p.id==Packets::scLogin&&l->unk==0xff)
      {
        return m_loginkey[i].crypt;
      }
    }
  }

  {
    tempbuf.assign(reinterpret_cast<const unsigned char*>(buf),reinterpret_cast<const unsigned char*>(buf)+bufsize);
    CCrypt203 crypt(m_key);
    crypt.Decrypt(&tempbuf.front(),&tempbuf.front(),tempbuf.size());

    if(tempbuf.front()==Packets::scPostLogin)
    {
      return crypt203game;
    }
  }

  /*
  // bad code, не работает
  {
    tempbuf.assign(reinterpret_cast<const unsigned char*>(buf),reinterpret_cast<const unsigned char*>(buf)+bufsize);
    NewGameCrypt crypt((uint8*)&m_key);
    crypt.init();
    crypt.encrypt(tempbuf.begin(),tempbuf.begin(),tempbuf.size());
    if(tempbuf.front()==Server::scPostLogin)
    {
      return crypt203game;
    }
  }
  */

  return cryptUnknown;
}

void CClientCrypt::Encrypt(void* buf,int bufsize)
{
  if(m_gamecrypt!=0)
  {
    m_gamecrypt->Encrypt(reinterpret_cast<unsigned char*>(buf),reinterpret_cast<unsigned char*>(buf),bufsize);
  }else if(m_logincrypt!=0)
  {
    m_logincrypt->encrypt(reinterpret_cast<unsigned char*>(buf),reinterpret_cast<unsigned char*>(buf),bufsize);
  }
}

void CClientCrypt::Decrypt(void* buf,int bufsize)
{
  if(m_gamecrypt!=0)
  {
    m_gamecrypt->Decrypt(reinterpret_cast<unsigned char*>(buf),reinterpret_cast<unsigned char*>(buf),bufsize);
  }else if(m_logincrypt!=0)
  {
    m_logincrypt->encrypt(reinterpret_cast<unsigned char*>(buf),reinterpret_cast<unsigned char*>(buf),bufsize);
  }
}

void CClientCrypt::CloseCrypt()
{
  if(m_gamecrypt!=0)
  {
    delete m_gamecrypt;
    m_gamecrypt=0;
  }
  if(m_logincrypt!=0)
  {
    delete m_logincrypt;
    m_logincrypt=0;
  }

  m_crypt=cryptUnknown;
}

void CClientCrypt::SetEncryption(crypt_tag crypt)
{
  CloseCrypt();

  m_crypt=crypt;

  loginkey_tag *loginkey;
  switch(crypt)
  {
  case crypt200login:
    m_logincrypt=new LoginCrypt;
    loginkey=GetLoginKey(crypt200login);
    m_logincrypt->init(reinterpret_cast<unsigned char*>(&m_key),loginkey->key1,loginkey->key2);
    break;
  case crypt200game:
    throw std::exception("bad crypt200game encryption");
    //m_gamecrypt=new OldGameCrypt;
    //m_gamecrypt->init();
    break;
  case crypt203login:
    m_logincrypt=new LoginCrypt;
    loginkey=GetLoginKey(crypt203login);
    m_logincrypt->init(reinterpret_cast<unsigned char*>(&m_key),loginkey->key1,loginkey->key2);
    break;
  case crypt203game:
    m_gamecrypt=new CCrypt203(m_key);
    break;
  case cryptUnknown:
    break;
  default:
    throw std::exception("bad new encryption");
  }
}

CClientCrypt::loginkey_tag* CClientCrypt::GetLoginKey(crypt_tag crypt)
{
  for(int i=0;i<sizeof(m_loginkey)/sizeof(loginkey_tag);i++)
  {
    if(m_loginkey[i].crypt==crypt)
      return &m_loginkey[i];
  }
  return 0;
}

void CClientCrypt::SetKey(unsigned u)
{
  m_key=u;
}

unsigned CClientCrypt::GetKey()
{
  return m_key;
}
