class LoginCrypt
{
private:
    unsigned int m_key[2];
    unsigned int m_k1, m_k2;

public:
    LoginCrypt();
    ~LoginCrypt();

    void encrypt(unsigned char * in, unsigned char * out, int len);
    void init(unsigned char * pseed, unsigned int k1, unsigned int k2);
};
