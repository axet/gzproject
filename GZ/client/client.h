#ifndef __CLIENT_H
#define __CLIENT_H

#include <winsock2.h>
#include "clientcommands.h"
#include "unit.h"

#include "clientnetevents.h"

/// класс нового лиента, замена client.exe

/// только в планы...
/// это будет клиент который подсоеденяеться к серверу.
/// отличается от clientXXX тем что тот присасываеться к существующему
/// уо кленту и работает в другом пространстве памяти
/// 
/// либ содержит в себе код как для клиентской так и для серверной части.
/// но все равно для независимого клиента

namespace Client
{
  class OperationNotSupported:public std::exception
  {
  public:
    OperationNotSupported(const char* p):exception(p)
    {
      ;
    }
  };
}

#endif
