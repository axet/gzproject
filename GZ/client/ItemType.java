package client;

public class ItemType extends Types.ShortList
{
	public static class BuildInType
	{
            int type;
            
            public BuildInType(int i)
            {
                type=i;
            }
            
            public static final int itOre_s = 0;
            public static final int itLog_s = 1;
	};

	public ItemType()
	{
            super();
	};
        
	public ItemType(BuildInType bi)
	{
		switch(bi.type)
		{
			case BuildInType.itOre_s:
				{
				int items[]={0x019b7,0x019b8,0x019b9,0x019ba};
				for(int i=0;i<items.length;i++)
				{
					add(new Integer(items[i]));
				}
			}
				break;
			case BuildInType.itLog_s:
				{
				int items[]={0x01bde,0x01bdd ,0x01bdf,0x01be0,0x01be1,0x01be2};
				for(int i=0;i<items.length;i++)
				{
					add(new Integer(items[i]));
				}
			}
				break;
		}
	}
	public ItemType(int i)
	{
		add(new Integer(i));
	}

	public boolean equal (int s)
	{
		return onof(s);
	}
	public boolean onof(int s)
	{
		for(int i=0;i<size();i++)
		{
			int k=((Integer)get(i)).intValue();
			if(k==s)
				return true;
		}
		return false;
	};
};
