#ifndef __PACKETS_H
#define __PACKETS_H

///
/// !!!
/// файл не должен изменяться, исползьоваться он постепенно переходит
///  в ServerPackets.h
/// !!!

#include <string>
#include <vector>
#include <comdef.h>

#include "servertypes.h"
#include "itemtype.h"

// Server {

/// знает пакеты которые нужно посылать версии сервера. ксати, они не меняються

/// сервер это некоторый неймспейс для пакетов общения с сервером.
/// каждый покет должен иметь возможность инициалзиироваться из буфера данных
/// ограниченного длинной, чотбы не заниматься работой в стиле "с"

namespace Packets
{

  /// Пакет с командой
  class Packet:public std::vector<Server::ServerByte>
  {
    bool m_valid;

  public:
    Packet():m_valid(false) {};
    Packet(const_iterator ibegin,const_iterator iend):m_valid(true)
    {
      assign(ibegin,iend);
    }
    Packet(Server::ServerByte* buf,int bufsize):m_valid(true)
    {
      assign(buf,buf+bufsize);
    }
    bool IsValidPakcet() const
    {
      if(!m_valid)
        return size()!=0;
      else
        return true;
    }
  };

#pragma pack(push,1)
  enum EthCommandEnumerate {scNone=-1,scLogin =0x80,scPreLogin=0x5d,scUseItem=0x6,scMoveItem = 0x1a,
    scCitiesAndChars=0xa9,scTarget = 0x6c, scSpeaking=0x1c,scOpenGump = 0x24,
    scReqObjDrop = 0x8,scReqObjGet=0x7,
    scClientTalk=0x03,
    scMobileStats = 0x11,
    scDisplayGump = 0x7c, scChoiseOption = 0x7d,
    scAddObject = 0x25, scBritaniaList = 0xa8, scLoginComplite = 0x55,
    scPing=0x73,scDestroyObject=0x1D,scLoginConfirm = 0x1b,scBritaniaSelect = 0xa0,
    scRealyServer =0x8c,scPostLogin =0x91,
    scWalkReq = 0x2,scSpeakingUnicode = 0xad,
    scDenyMoveItem=0x27,
    scAcctLoginFail=0x82,
    scEquippedMOB=0x78,scRenameMOB=0x75,scMOBName=0x98,scNakedMOB=0x77,
    scGodCommand=0x12,
    scShopSell=0x9e,scShopOffer=0x9f,
    scMultiObj=0x3c
  };

  struct scsPack
  {
    Server::ServerByte id;

    scsPack() {}
    scsPack(EthCommandEnumerate sc):id(sc) {}
    operator Server::ServerByte()
    {
      return id;
    }
  };
  struct scsMultiObj
  {
    struct tag1
    {
      scsPack id;
      Server::ServerShort	Packet_Size	;
      Server::ServerShort	Number_of_Items	;
    };

    struct Item
    {
      Server::ServerInt	Item_Serial	;
      Server::ServerShort	Item_ID	;
      Server::ServerByte	bt	;
      Server::ServerShort	Item_Amount	;
      Server::ServerShort	Item_X	;
      Server::ServerShort	Item_Y	;
      Server::ServerInt	Container_Serial	;
      Server::ServerShort	Item_Color	;
    };

    typedef std::vector<Item> Items;

    Items m_items;

    scsMultiObj(){}
    scsMultiObj(Server::ServerByte*p);
    void operator =(Server::ServerByte*p);
  };
  struct scsShopSell
  {
    struct tag1
    {
      scsPack pack;
      Server::ServerShort Packet_Size	;
      Server::ServerInt Serial	;
      Server::ServerShort Number_of_Items	;
    };
    struct tag2
    {
      Server::ServerInt	Item_Serial	;
      Server::ServerShort	Item_ID	;
      Server::ServerShort	Item_Color	;
      Server::ServerShort	Item_Amount	;
      Server::ServerShort	Value	;
      Server::ServerShort	Name_Length	;
      char Name	;
    };

    struct Item
    {
      int serial;
      Client::ItemType item;
      int color;
      int amount;
      int value;
      std::string name;
    };

    typedef std::vector<Item> Items;
    Items m_items;
    unsigned m_serial;

    scsShopSell(){};
    scsShopSell(Server::ServerByte*);
    void operator = (Server::ServerByte*);
  };
  struct scsShopOffer
  {
    struct tag1
    {
      scsPack pack;
      Server::ServerShort	Packet_Size	;
      Server::ServerInt	Vendor_Serial	;
      Server::ServerByte	Unknown;
      Server::ServerByte	Number_of_Items;;
    };
    struct Item
    {
      Server::ServerInt	Item_Serial	;
      Server::ServerShort	Item_Amount;
    };
    typedef std::vector<Item> Items;

    Packet m_pack;

    scsShopOffer(unsigned serial,const Items& itmes);
    operator Packet();
  };
  struct scsDenyMoveItem
  {
    Packet packet;

    scsDenyMoveItem()
    {
      packet.push_back(scDenyMoveItem);
      packet.push_back(0);
    }

    operator Packet&()
    {
      return packet;
    }
  };
  struct scsAcctLoginFail
  {
    scsPack pack;
    Server::ServerByte error;

    static const char* GetErrorText(Server::ServerByte* p);
  };
  struct scsWalkReq
  {
    scsPack pack;
    Server::ServerByte direction;
    Server::ServerByte sqn;
    Server::ServerInt fast;
  };
  struct scsPostLogin
  {
    scsPostLogin()
    {
      pack.id=scPostLogin;
    }
    scsPostLogin(Server::ServerInt a,const Server::ServerChar* _name,const Server::ServerChar*_pass)
    {
      pack.id=scPostLogin;
      authid=a;
      strcpy(name,_name);
      strcpy(pass,_pass);
    }
    operator Packet()
    {
      return Packet((Server::ServerByte*)this,sizeof(scsPostLogin));
    }
    scsPack pack;

    Server::ServerInt authid;
    Server::ServerChar name[30];
    Server::ServerChar pass[30];
  };
  struct scsDestroyObject
  {
    scsDestroyObject()
    {
      pack.id=scDestroyObject;
    }
    operator Packet()
    {
      return Packet((Server::ServerByte*)this,sizeof(scsPostLogin));
    }
    scsPack pack;

    Server::ServerInt serial;
  };
  struct scsRealyServer
  {
    scsRealyServer()
    {
      pack.id=scRealyServer;
    }
    scsPack pack;
    Server::ServerInt serverip;
    Server::ServerShort serverport;
    Server::ServerInt authid;
  };
  struct scsBritaniaSelect
  {
    scsBritaniaSelect(){pack.id=scBritaniaSelect;}

    scsPack pack;
    Server::ServerShort serverindex;
  };
  struct scsLoginConfirm
  {
    scsPack p;
    Server::ServerInt serial;
    Server::ServerInt unk1;
    Server::ServerShort id;
    Server::ServerShort x;
    Server::ServerShort y;
    Server::ServerByte unk2;
  };
  struct scsMobileStats
  {
    scsPack p;
    unsigned short size;
    Server::ServerInt serial;
    Server::ServerChar charname[30];
    Server::ServerShort hits;
    Server::ServerShort maxhits;
    bool alownamechange;
    bool validstats;
    bool grender;
    Server::ServerShort str;
    Server::ServerShort dex;
    Server::ServerShort inta;
    Server::ServerShort stam;
    Server::ServerShort maxstam;
    Server::ServerShort mana;
    Server::ServerShort maxmana;
    unsigned gold;
    Server::ServerShort armor;
    Server::ServerShort weight;
  };
  struct scsNakedMOB
  {
    scsPack p;
    Server::ServerInt serial;
    Server::ServerShort id;
    Server::ServerShort x;
    Server::ServerShort y;
    Server::ServerByte z;
    Server::ServerByte dir;
    Server::ServerShort skincol;
    Server::ServerByte status;
    Server::ServerByte noto;
  };
  struct scsSpeaking
  {
    struct packet1_tag
    {
      scsPack p;
      Server::ServerShort size;
      Server::ServerInt charserial;
      Server::ServerShort charid;
      Server::ServerByte type;
      Server::ServerShort color;
      Server::ServerShort font;
      Server::ServerChar name[30];
    };
    struct packet2_tag
    {
      Server::ServerChar text[1];
    };

    // пакет приходит от севера, можно ожидать валидный пакет
    scsSpeaking(Server::ServerByte* buf);
    scsSpeaking(const Server::ServerChar* text)
    {
      *this=text;
    }

    void operator = (const Server::ServerChar* text);
    void operator = (Packet &ppacket)
    {
      packet=ppacket;
    }
    operator Packet&()
    {
      return packet;
    }

    const Server::ServerChar *text();

  private:
    Packet packet;
  };
  struct scsClientTalk
  {
    struct info_tag
    {
      scsPack p;
      Server::ServerShort PacketLength;
      Server::ServerByte Mode;// (0=say,2=emote,8=whisper,9=yell) 
      Server::ServerShort TextColor;
      Server::ServerShort Font;
    };

    struct text_tag
    {
      Server::ServerChar Text[1];
    };

    std::string text;
    scsClientTalk(const Server::ServerByte* buf,int bufsize);
  };
  struct scsSpeakingUnicode
  {
    struct info
    {
      scsPack p;
      Server::ServerShort PacketLength;
      Server::ServerByte Mode;// (0=say,2=emote,8=whisper,9=yell) 
      Server::ServerShort TextColor;
      Server::ServerShort Font;
      Server::ServerByte Language[4];
    };

    struct text_tag
    {
      Server::ServerChar Text[1];
    };

    _bstr_t text;

    Packet m_packet;

    scsSpeakingUnicode(const Server::ServerByte* buf,int bufsize);
    scsSpeakingUnicode(const char*);
  };
  struct scsTarget
  {
    scsTarget()
    {
      p.id=scTarget;
      type=0;
      charid=0;
      itemserial=0;
      crime=0;
      worldx=0;
      worldy=0;
      byte=0;
      worldz=0;
      itemid=0x0;
    }

    scsPack p;
    Server::ServerByte type;// 1 земля 0 итем
    Server::ServerInt charid;
    Server::ServerByte crime;
    Server::ServerInt itemserial;
    Server::ServerShort worldx;
    Server::ServerShort worldy;
    Server::ServerByte byte;
    Server::ServerByte worldz;
    Server::ServerShort itemid;

    void operator = (Server::ServerByte *p)
    {
      memcpy(this,p,sizeof(scsTarget));
    }
  };
  struct scsEquippedMOB
  {
    scsEquippedMOB(const char*);

    scsPack p;
    unsigned short Packet_Size;
    Server::ServerInt Item_Serial;
    Server::ServerShort Item_ID;
    Server::ServerShort Item_Amount;
    Server::ServerShort X	;
    Server::ServerShort Y	;
    Server::ServerByte Z	;
    Server::ServerByte Direction	;
    Server::ServerShort Skin_Color;
    Server::ServerByte Status	;
    Server::ServerByte Notoriety	;

    struct MOBSmall
    {
      scsPack p;
      unsigned short Packet_Size;
      Server::ServerInt Item_Serial;
      Server::ServerShort Item_ID;
      Server::ServerShort X	;
      Server::ServerShort Y	;
      Server::ServerByte Z	;
      Server::ServerByte Direction	;
      Server::ServerShort Skin_Color;
      Server::ServerByte Status	;
      Server::ServerByte Notoriety	;
    };

    struct Item
    {
      Server::ServerInt	Serial	;
      Server::ServerShort	Item_ID;
      Server::ServerByte	Item_Layer;
      Server::ServerShort	Item_Color;
    };
    struct ItemSmall
    {
      Server::ServerInt	Serial	;
      Server::ServerShort	Item_ID;
      Server::ServerByte	Item_Layer;
    };

    std::vector<Item> m_items;
  };
  struct scsMoveItem
  {
    scsMoveItem(){p.id=scMoveItem;Packet_Size=sizeof(scsMoveItem);}
    scsMoveItem(Server::ServerByte* p)
    {
      memcpy(this,p,sizeof(scsMoveItem));
    }
    void operator = (Server::ServerByte* p)
    {
      memcpy(this,p,sizeof(scsMoveItem));
    }

    scsPack p;
    unsigned short Packet_Size;
    Server::ServerInt Item_Serial;
    Server::ServerShort Item_ID;
    Server::ServerShort Item_Amount;
    Server::ServerByte stack;
    Server::ServerShort worldx;
    Server::ServerShort worldy;
    Server::ServerByte direction;
    Server::ServerByte worldz;
    Server::ServerShort hue;
    Server::ServerByte status;
  };
  struct scsMoveItemSmall
  {
    scsMoveItemSmall(){p.id=scMoveItem;Packet_Size=sizeof(scsMoveItemSmall);}

    scsPack p;
    Server::ServerShort Packet_Size;
    Server::ServerInt Item_Serial;
    Server::ServerShort Item_ID;
    Server::ServerShort worldx;
    Server::ServerShort worldy;
    Server::ServerByte worldz;
  };
  struct scsUseItem
  {
    scsPack p;
    Server::ServerInt itemserial;

    scsUseItem(){p.id=scUseItem;}
  };
  struct scsCitiesAndChars
  {
    scsPack p;
    Server::ServerShort size;
    struct Chars_tag
    {
      Server::ServerByte numchars;
      struct Char_tag
      {
        Server::ServerChar CharName[60];
      };
      Char_tag Char[1];
    };
    Chars_tag Chars;
    struct Cities_tag
    {
      Server::ServerByte numsites;
      struct Citie_tag
      {
        Server::ServerByte index;
        Server::ServerChar name[30];
        Server::ServerChar area[30];
        Server::ServerShort unknown;
      };
      Citie_tag Cite[1];
    };

    Chars_tag& GetChars();
    Cities_tag& GetCities();
    int GetCharCount();
    Chars_tag::Char_tag& GetCharInfo(int index);
    int GetCitieCount();
    Cities_tag::Citie_tag& GetCitieInfo(int index);
  };
  struct scsPreLogin
  {
    scsPreLogin():res(0xEDEDEDED),res2(0x5661),res3(0x69)
    {
      p.id=scPreLogin;
    };

    scsPack p;
    Server::ServerInt res;
    Server::ServerChar Character_Name[60];
    Server::ServerShort res2; 
    Server::ServerByte res3; 
    Server::ServerByte Character_Index;
    Server::ServerInt Client_IP;
  };
  struct scsLogin
  {
    scsLogin() {p.id=scLogin;unk=-1;};
    scsLogin(const char* _name,const char* _password):unk(-1)
    {
      p.id=scLogin;
      strcpy(name,_name);
      strcpy(password,_password);
    }

    operator Packet()
    {
      // + for hope...
      return Packet((Server::ServerByte*)this,sizeof(scsLogin));
    }

    scsPack p;
    Server::ServerChar name[30];
    Server::ServerChar password[30];
    Server::ServerByte unk;
  };
  class scsOpenGump
  {
  public:
    struct gump_tag
    {
      Server::ServerInt gumpser;
      Server::ServerShort gumpid;
    };
    struct item_tag
    {
      Server::ServerInt itemserial;
      Server::ServerShort itemid;
      Server::ServerByte b1;
      Server::ServerShort itemamount;
      Server::ServerShort itemx;
      Server::ServerShort itemy;
      Server::ServerInt containerserial;
      Server::ServerShort itemhue;
    };
    struct scsOpenGump_tag
    {
      Server::ServerShort numberofitems;
      item_tag item[1];
    };
    typedef std::vector<item_tag> items_t;

    scsOpenGump();
    void operator = (const void* p);
    Server::ServerInt GetGumpSer();
    Server::ServerShort GetGumpId();
    int GetItemsCount();
    item_tag* GetItem(int);

  private:
    items_t m_list;
    gump_tag m_gump;
    struct scsOpenGumpExt_tag
    {
      scsPack p;
      gump_tag gump;
      Server::ServerByte unk;
      Server::ServerByte ext;// 1=расширенная структура
    };
  };
  struct scsReqObjGet
  {
    scsReqObjGet() {p.id=scReqObjGet;}
    scsPack p;
    Server::ServerInt itemserial;
    Server::ServerShort amount;
  };
  struct scsReqObjDrop
  {
    scsReqObjDrop() {p.id=scReqObjDrop;}
    scsPack p;
    Server::ServerInt itemserial;
    Server::ServerShort x;
    Server::ServerShort y;
    Server::ServerByte z;
    Server::ServerInt containerserial;
  };
  struct scsAddObject
  {
    scsPack p;
    Server::ServerInt itemserial;
    Server::ServerShort itemid;
    Server::ServerByte unk1;
    Server::ServerShort amount;
    Server::ServerShort x;
    Server::ServerShort y;
    Server::ServerInt contser;
    Server::ServerShort itemcolor;

    scsAddObject(){}
    scsAddObject(Server::ServerByte*p)
    {
      memcpy(this,p,sizeof(scsAddObject));
    }
    void operator =(Server::ServerByte*p)
    {
      memcpy(this,p,sizeof(scsAddObject));
    }
  };
  class scsDisplayGump
  {
  public:
    struct line_tag
    {
      Server::ServerShort itemid;
      Server::ServerShort checked;
      std::string name;

      line_tag(Server::ServerShort id,Server::ServerShort ch,const Server::ServerChar* p,int plen):itemid(id),checked(ch),name(p,plen) {}
    };

    scsDisplayGump();
    scsDisplayGump(const void*);
    Server::ServerInt GetGumpSer();
    Server::ServerShort GetGumpId();
    int GetLineCount();
    line_tag* GetLine(int index);
    int FindIndex(Server::ServerShort itemid);

  private:
    struct scsDisplayGump_tag
    {
      scsPack p;
      Server::ServerShort size;
      struct gumpinfo_tag
      {
        Server::ServerInt gumpserial;
        Server::ServerShort gumpid;
      };
      gumpinfo_tag gumpinfo;
      Server::ServerByte txtlen;
      Server::ServerChar txt[1];
      Server::ServerByte numoflines;
      struct line_tag
      {
        Server::ServerShort id;
        Server::ServerShort checked;
        Server::ServerByte txtlen;
        Server::ServerChar txt[1];
      };
      line_tag line[1];
    };
    typedef std::vector<line_tag> lines_t;

    scsDisplayGump_tag::gumpinfo_tag m_gumpinfo;
    std::string m_name;
    lines_t m_lines;
  };
  struct scsChoiseOption
  {
    scsChoiseOption(){p.id=scChoiseOption;unk1=0;}
    scsPack p;
    Server::ServerInt gumpser;
    Server::ServerShort gumpid;
    Server::ServerShort itemindex;
    Server::ServerShort itemid;
    Server::ServerShort unk1;
  };
#pragma pack(pop)

  class EthCommand
  {
    Server::ServerInt m_cmd;

  public:
    EthCommand():m_cmd(-1) {}
    EthCommand(Server::ServerByte cmd):m_cmd(cmd) {}
    EthCommand(const void* cmdbuf):m_cmd(*(Server::ServerByte*)cmdbuf) {}
    EthCommand(EthCommandEnumerate e):m_cmd(e) {}

    operator Server::ServerInt()
    {
      return m_cmd;
    }
    bool isnull()
    {
      return m_cmd==-1;
    }
    EthCommand& operator = (const void* cmdbuf)
    {
      m_cmd=*(Server::ServerByte*)cmdbuf;
      return *this;
    }
    bool operator < (const EthCommand&s1) const
    {
      return m_cmd<s1.m_cmd;
    }
    operator int ()
    {
      return m_cmd;
    }
  };
};

// } Server

#endif
