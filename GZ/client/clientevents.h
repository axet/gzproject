#ifndef CLIENTEVENTS_H
#define CLIENTEVENTS_H

/*
реализация класса, те этот на этот класс приходят все события уже разжованные

перехватывает CGZUser

*/

#include "clientgameevents.h"

class CClientEvents:public CClientGameEvents
{
public:
  virtual ~CClientEvents() {}
  // когда клиент.exe запущен
  virtual void Connected() {};
  virtual void LocalConnectionLost() {};
  // клиент вызвает эту фукнцию когда с сервера приходит сообщение
  // по сообщению лога нельзя понять какого оно типа (в окне журнала
  // они разных цветов\типов)
  virtual void LogMessage(const char* msg) {}

  // когда логин на сервер выполенн
  virtual void ConnectionEstablished(const char* name,const char* pass, const char* charname) {};
  // когда связь разорвана
  virtual void ConnectionLost() {};

  // приложение клиента завершило работу
  // коды ошибок:
  //   0 нормальное заврершение работы
  //   -1 фатальнафя ошибка, клиент закрыт
  virtual void Exit(unsigned int exitcode) {}
};

#endif
