#pragma once

#include <vector>

typedef std::vector<unsigned> UnsignedList;
typedef std::vector< std::pair<unsigned,unsigned> > NumPairList;
typedef std::vector<short> ShortList;

struct WorldCord
{
  int x,y,z;

  WorldCord():x(-1),y(-1),z(-1)
  {
  }
  WorldCord(int xx,int yy,int zz):x(xx),y(yy),z(zz)
  {
  }
};
