#include <winsock2.h>

#include "clientinterface.h"
#include "packets.h"

void CClientInterface::handle_receive_message(const unsigned char* buf, int size)
{
  const MessageType & type = m_messages[buf[0]];
  for(cnelist_t::iterator i=m_cnelist.begin();i!=m_cnelist.end();i++)
  {
    if(type.shandler!=0)
      ((*i)->*(type.shandler))(buf,size);
    if(type.rhandler!=0)
      ((*i)->*(type.rhandler))(buf,size);
  }
}

CClientNetEvents* CClientInterface::AddAdvise(CClientNetEvents*p)
{
  m_cnelist.insert(p);
  return p;
}

CClientNetEvents* CClientInterface::RemoveAdvise(CClientNetEvents*p)
{
  m_cnelist.erase(p);
  return p;
}

void CClientInterface::PostData(const unsigned char* buf,int bufsize)
{
  Command command;

  if(!m_fragment.empty())
  {
    m_fragment.insert(m_fragment.end(),buf,buf+bufsize);
    command=GetCommand(m_fragment);
  }else
  {
    command=GetCommand(buf,bufsize);
    m_fragment=MakeFragment(buf,bufsize,command);
  }

  // защита от переполнения
  if(m_fragment.size()>0x1000)
  {
    m_fragment.clear();
    return;
  }

  if(command.Empty())
    return;

  // получаем код команды
  Server::ServerByte id=buf[command.begin];

  // команда нам неизвестна? выкидываем ее
  if(id>=m_messagescount)
    return;
  const MessageType &mt=m_messages[id];
  if(mt.name==0)
    return;

  // разбераем команду

  handle_receive_message(&buf[command.begin],command.end);
}
