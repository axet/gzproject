package client;

import java.util.*;

public class Types
{
    public static class UnsignedList extends ArrayList
    {
    };

    public static class Pair
    {
            public long l1;
            public long l2;
    };

    public static class NumPairList extends ArrayList
    {
    };

    public static class ShortList extends ArrayList
    {
        public ShortList()
        {
            super();
        }
    };

    public static class WorldCord
    {
    	public int x;
    	public int y;
    	public int z;

    	public WorldCord()
    	{
    	}
    	
    	public WorldCord(int xx,int yy,int zz)
    	{
    		x=xx;
    		y=yy;
    		z=zz;
    	}
    }
}
