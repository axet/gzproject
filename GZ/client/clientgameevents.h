#ifndef __CLIENTGAMEEVENTS__H__
#define __CLIENTGAMEEVENTS__H__

#include "types.h"

namespace Client
{
  class ItemType;
}

/// игровые события которые могут придти от персонажа игры

class CClientGameEvents
{
public:
  /// игрок хочет выбрать для ГЗ приложения предмет
  virtual void SelectedItem(unsigned itemserial) {}
  /// игрок выберает клеточку на земле
  virtual void SelectedGround(WorldCord w,const Client::ItemType &) {}
  /// игрок выберает  путь персонажа
  virtual void SelectedPath(WorldCord w) {};
  /// выбор из окошка создания предмета
  virtual void SelectedOption(const Client::ItemType &) {};
};


#endif
