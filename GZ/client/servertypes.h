namespace Server
{
  class ServerShort;
  class ServerInt;
  typedef unsigned char ServerByte;
  typedef char ServerChar;

  class ServerShort
  {
    unsigned short m_value;

  public:
    ServerShort();
    // x86
    ServerShort(unsigned short);
    operator unsigned short();
    ServerShort& operator +=(int);
  };

  class ServerInt
  {
    unsigned m_value;

  public:
    ServerInt();
    ServerInt(unsigned int);
    operator unsigned int();
    bool operator <(const ServerInt &)const;
  };
}
