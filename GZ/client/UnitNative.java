package client;

public class UnitNative implements Unit
{
    
    public native void AddAdvise(ClientGameEvents cge) ;
    
    public native void CharMoveTo(Types.WorldCord w) ;
    
    public native void CharSpeech(String buf) ;
    
    public native Types.WorldCord GetUnitPos() ;
    
    public native int MoveItem(int itemwhat, int containerserialto, int count) ;
    
    public native void MoveItemsG2G(int containerserialfrom, ItemType it, int containerserialto) ;
    
    public native void MoveItemsG2G(int containerserialfrom, ItemType it, int count, int containerserialto) ;
    
    public native void RemoveAdvise(ClientGameEvents cge) ;
    
    public native int SelectItem(int containerserialwhere) ;
    
    public native int SelectItem(int containerserialwhere, ItemType it) ;
    
    public native int SelectItem(int containerserialfrom, ItemType it, int count) ;
    
    public native int StatsGetHits() ;
    
    public native int StatsGetMana() ;
    
    public native int StatsGetStam() ;
    
    public native int StatsGetWeight();
    
    public native String UseItem(int itemserial) ;
    
    public native String UseItem(int itemserialtouse, int itemserialonuse) ;
    
    public native String UseItem(int itemserialtouse, Types.WorldCord w, ItemType it) ;
    
    public native int UseItemGump(int itemserialtouse, client.Types.UnsignedList itemserialtoselect, client.Types.ShortList itemslist) ;
    
};
