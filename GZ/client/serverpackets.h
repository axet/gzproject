namespace ServerPackets
{
  // пакеты в этом классе не требуют идетификации,
  // разбором занимаеться CClientInterface

  // необходимо предусмотреть интерфейс который позволит создавать
  // пакеты для отправки и позволит разберать принятые пакеты

  // Бросается в случае не валидного пакета со стороны клиента
  class CPacketValid:public exception
  {
  public:
    CPacketValid() {}
    virtual const char *what() const
    {
      return "Packet from client not valid";
    }
  };

  struct scsUnknown
  {
    // init from buffer

    // инициализация с проверкой (указываеться длинна)
    scsUnknown(const void* buf,int bufsize)
    {
      ;
    }

    // иннициализация для создания
    scsUnknown(void* buf,int bufsize)
    {
      ;
    }

    // инициализация без проверки (не указываеться длинна)
    scsUnknown& operator = (const void* buf)
    {
      ;
    }

    // get property
  };

  struct scsFirstLogin
  {
    scsFirstLogin(const void* buf,int bufsize)
    {
      ;
    }
  };
}
