#include "itemtype.h"

Client::ItemType::ItemType()
{
  ;
}

Client::ItemType::ItemType(unsigned short u)
{
  push_back(u);
}


Client::ItemType::ItemType(BuildInType bi)
{
  switch(bi)
  {
  case itOre_s:
    {
      unsigned short items[]={0x019b7,0x019b8,0x019b9,0x019ba};
      insert(end(),&items[0],&items[sizeof(items)/sizeof(unsigned short)]);
    }
    break;
  case itLog_s:
    {
      unsigned short items[]={0x01bde,0x01bdd ,0x01bdf,0x01be0,0x01be1,0x01be2};
      insert(end(),&items[0],&items[sizeof(items)/sizeof(unsigned short)]);
    }
    break;
  case itDoorOpen_s:
    {
      unsigned short items[]={0x06a8,0x06aa,0x06ac,0x06ae,0x06b0,0x06b2,0x06b4};
      insert(end(),&items[0],&items[sizeof(items)/sizeof(unsigned short)]);
    }
    break;
  case itDoorClose_s:
    {
      unsigned short items[]={0x06a7,0x06a9,0x06ab,0x06ad,0x06af,0x06b1,0x06b3};
      insert(end(),&items[0],&items[sizeof(items)/sizeof(unsigned short)]);
    }
    break;
  }
}

bool Client::ItemType::oneof(unsigned short s) const
{
  for(ShortList::const_iterator i=begin();i!=end();i++)
  {
    if(*i==s)
      return true;
  }
  return false;
}

bool Client::ItemType::oneof(const Client::ItemType& s) const
{
  for(ShortList::const_iterator i=s.begin();i!=s.end();i++)
  {
    if(oneof(*i))
      return true;
  }
  return false;
}

Client::ItemType& Client::ItemType::operator = (unsigned short s)
{
  clear();
  push_back(s);
  return *this;
}

bool Client::ItemType::operator == (unsigned short s) const
{
  return oneof(s);
}

bool Client::ItemType::operator == (const Client::ItemType& s) const
{
  if(size()!=s.size())
    return false;
  for(int i=0;i<size();i++)
  {
    if(!oneof(s[i]))
      return false;
  }
  return true;
}

Client::ItemType::operator unsigned short() const
{
  if (size()==1)
    return front();
  else
    throw std::exception();
}
