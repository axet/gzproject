#ifndef __CLIENTINTERFACE_H__
#define __CLIENTINTERFACE_H__


#pragma warning(disable:4786)
#include <set>
#include <vector>

#include "clientnetevents.h"
#include "ethinterface.h"

/// только сетевое общение

/// как для клиента так и для сервера.
/// делает разбор пакета по типам (определяет какой пакет в каую функцию отдать).
/// передает пакеты в подписчиков CClientNetEvents.

class CClientInterface:public EthInterface
{
public:
  class CReplacePacket;

  void PostData(const unsigned char* buf,int bufsize) throw (CReplacePacket);
  CClientNetEvents* AddAdvise(CClientNetEvents*);
  CClientNetEvents* RemoveAdvise(CClientNetEvents*);

private:
  typedef std::set<CClientNetEvents*> cnelist_t;

  cnelist_t m_cnelist;
  EthInterface::Fragment m_fragment;

  void handle_receive_message(const unsigned char* buf, int size);
  int GetMessageSize(const unsigned char* buf,int bufsize);
};

/// Замена пакета новым.

class CClientInterface::CReplacePacket:public std::exception
{
public:
  typedef std::vector<const unsigned char> bytearray_t;
  bytearray_t m_bytearray;

  CClientInterface::CReplacePacket()
  {
    unsigned char a[]={0x73,0x00};
    m_bytearray.assign(a,a+sizeof(a));
  }
  CReplacePacket(const unsigned char* buf,int bufsize)
  {
    m_bytearray.assign(buf,buf+bufsize);
  }
  const char* what()
  {
    return "Replace client packet";
  }
};

#endif
