#ifndef __UNIT_H_
#define __UNIT_H_

#include "../client/clientgamecommands.h"

class CClientGameEvents;

/// Класс для управления персонажем.

class CUnit:public ClientGameCommands::Control
{
public:
  /// подписаться на события
  virtual void AddAdvise(CClientGameEvents*) = 0;
  /// отписаться
  virtual void RemoveAdvise(CClientGameEvents*) = 0;
  /// Прерывание выполнения текущей команды
  virtual void Abort() = 0;
  /// получить координаты игрока
  virtual WorldCord GetUnitPos()  = 0;
  /// уникальный идентификатор аккаунта
  virtual const char* GetUnitId() = 0;
};

#endif
