#include <winsock2.h>

#include "ethinterface.h"
#include "packets.h"

const int SIZE_VARIABLE = 0;
const int USE_DISTANCE  = 3;

// An unknown/unverified/obsolete message type
#define UMSG(size) { 0, size, DIR_BOTH, 0, 0 }
// A message type sent to the server
#define SMSG(name, size) { name, size, DIR_SEND, 0, 0 }
// A message type received from the server
#define RMSG(name, size) { name, size, DIR_RECV, 0, 0 }
// A message type transmitted in both directions
#define BMSG(name, size) { name, size, DIR_BOTH, 0, 0 }
// Message types that have handler methods
#define SMSGH(name, size, smethod) \
    { name, size, DIR_SEND, &CClientNetEvents::smethod, 0 }
#define RMSGH(name, size, rmethod) \
    { name, size, DIR_RECV, 0, &CClientNetEvents::rmethod }
#define BMSGH(name, size, smethod, rmethod) \
    { name, size, DIR_BOTH, &CClientNetEvents::smethod, &CClientNetEvents::rmethod }

const EthInterface::MessageType EthInterface::m_messages[]=
{
  SMSG("Create Character", 0x68), // 0x00
  SMSG("Disconnect", 0x05), // 0x01
  SMSGH("Walk Request", 0x07, handle_walk_request), // 0x02
  SMSGH("Client Talk", SIZE_VARIABLE, handle_client_talk), // 0x03
  UMSG(0x02), // 0x04
  SMSG("Attack", 0x05), // 0x05
  SMSGH("Double Click", 0x05,handle_double_click), // 0x06
  SMSGH("Pick Up Item", 0x07,handle_get_item), // 0x07
  SMSG("Drop Item", 0x0e), // 0x08
  SMSG("Single Click", 0x05), // 0x09
  UMSG(0x0b), // 0x0a
  UMSG(0x10a), // 0x0b
  UMSG(SIZE_VARIABLE), // 0x0c
  UMSG(0x03), // 0x0d
  UMSG(SIZE_VARIABLE), // 0x0e
  UMSG(0x3d), // 0x0f
  UMSG(0xd7), // 0x10
  RMSGH("Character Status", SIZE_VARIABLE, handle_character_status), // 0x11
  SMSG("Perform Action", SIZE_VARIABLE), // 0x12
  SMSGH("Client Equip Item", 0x0a,handle_client_equip_item), // 0x13
  UMSG(0x06), // 0x14
  UMSG(0x09), // 0x15
  UMSG(0x01), // 0x16
  UMSG(SIZE_VARIABLE), // 0x17
  UMSG(SIZE_VARIABLE), // 0x18
  UMSG(SIZE_VARIABLE), // 0x19
  RMSGH("Update Item", SIZE_VARIABLE, handle_update_item), // 0x1a
  RMSGH("Enter World", 0x25, handle_enter_world), // 0x1b
  RMSGH("Server Talk", SIZE_VARIABLE, handle_server_talk), // 0x1c
  RMSGH("Delete Object", 0x05, handle_delete_object), // 0x1d
  UMSG(0x04), // 0x1e
  UMSG(0x08), // 0x1f
  RMSGH("Update Player", 0x13, handle_update_player), // 0x20
  RMSG("Deny Walk", 0x08), // 0x21
  BMSG("Confirm Walk", 0x03), // 0x22
  RMSG("Drag Animation", 0x1a), // 0x23
  RMSGH("Open Container", 0x07, handle_open_container),  // 0x24
  RMSGH("Update Contained Item", 0x14, handle_update_contained_item), //0x25
  UMSG(0x05), // 0x26
  RMSG("Deny Move Item", 0x02), // 0x27
  UMSG(0x05), // 0x28
  UMSG(0x01), // 0x29
  UMSG(0x05), // 0x2a
  UMSG(0x02), // 0x2b
  BMSG("Death Dialog", 0x02), // 0x2c
  UMSG(0x11), // 0x2d
  RMSGH("Server Equip Item", 0x0f, handle_server_equip_item), // 0x2e
  RMSG("Combat Notification", 0x0a), // 0x2f
  UMSG(0x05), // 0x30
  UMSG(0x01), // 0x31
  UMSG(0x02), // 0x32
  RMSGH("Pause Control", 0x02, handle_pause_control), // 0x33
  SMSGH("Status Request", 0x0a, handle_status_request), // 0x34
  UMSG(0x28d), // 0x35
  UMSG(SIZE_VARIABLE), // 0x36
  UMSG(0x08), // 0x37
  UMSG(0x07), // 0x38
  UMSG(0x09), // 0x39
  BMSG("Update Skills", SIZE_VARIABLE), // 0x3a
  BMSGH("Vendor Buy Reply", SIZE_VARIABLE, handle_vendor_buy_reply_s,handle_vendor_buy_reply_r), // 0x3b
  RMSGH("Update Contained Items", SIZE_VARIABLE,handle_update_contained_items),    //ox3c
  UMSG(0x02), // 0x3d
  UMSG(0x25), // 0x3e
  UMSG(SIZE_VARIABLE), // 0x3f
  UMSG(0xc9), // 0x40
  UMSG(SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  UMSG(0x229),
  UMSG(0x2c9),
  UMSG(0x05),
  UMSG(SIZE_VARIABLE),
  UMSG(0x0b),
  UMSG(0x49), // 0x48
  UMSG(0x5d),
  UMSG(0x05),
  UMSG(0x09),
  UMSG(SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  RMSG("Object Light Level", 0x06),
  RMSGH("Global Light Level", 0x02, handle_global_light_level),
  UMSG(SIZE_VARIABLE), // 0x50
  UMSG(SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  RMSGH("Error Code", 0x02, handle_error_code),   // Idle message
  RMSG("Sound Effect", 0x0c),
  RMSG("Login Complete", 0x01),
  BMSG("Map Data", 0x0b),
  UMSG(0x6e),
  UMSG(0x6a), // 0x58
  UMSG(SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  RMSG("Set Time?", 0x04),
  UMSG(0x02),
  SMSGH("Select Character", 0x49, handle_select_character),
  UMSG(SIZE_VARIABLE),
  UMSG(0x31),
  UMSG(0x05), // 0x60
  UMSG(0x09),
  UMSG(0x0f),
  UMSG(0x0d),
  UMSG(0x01),
  RMSGH("Set Weather", 0x04, handle_weather_change),
  BMSG("Book Page Data", SIZE_VARIABLE),
  UMSG(0x15),
  UMSG(SIZE_VARIABLE), // 0x68
  UMSG(SIZE_VARIABLE),
  UMSG(0x03),
  UMSG(0x09),
  BMSGH("Target Data", 0x13, handle_target_s, handle_target_r),
  RMSG("Play Music", 0x03),
  RMSG("Character Animation", 0x0e),
  BMSG("Secure Trading", SIZE_VARIABLE),
  RMSG("Graphic Effect", 0x1c), // 0x70
  BMSG("Message Board Data", SIZE_VARIABLE),
  BMSG("War Mode", 0x05),
  BMSG("Ping", 0x02),
  RMSGH("Vendor Buy List", SIZE_VARIABLE, handle_vendor_buy_list),
  SMSG("Rename Character", 0x23),
  UMSG(0x10),
  RMSG("Update Character", 0x11),
  RMSGH("Update Object", SIZE_VARIABLE, handle_update_object), // 0x78
  UMSG(0x09),
  UMSG(SIZE_VARIABLE),
  UMSG(0x02),
  RMSGH("Open Menu Gump", SIZE_VARIABLE, handle_open_menu_gump),
  SMSG("Menu Choice", 0x0d),
  UMSG(0x02),
  UMSG(SIZE_VARIABLE),
  SMSGH("First Login", 0x3e, handle_first_login), // 0x80
  UMSG(SIZE_VARIABLE),
  RMSG("Login Error", 0x02),
  SMSG("Delete Character", 0x27),
  UMSG(0x45),
  UMSG(0x02),
  RMSGH("Character List 2", SIZE_VARIABLE, handle_character_list2),
  UMSG(SIZE_VARIABLE),
  RMSG("Open Paperdoll", 0x42), // 0x88
  RMSG("Corpse Equipment", SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  RMSGH("Relay Server", 0x0b, handle_relay_server),
  UMSG(SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  RMSG("Display Map", 0x13), // 0x90
  SMSGH("Second Login", 0x41, handle_second_login),
  UMSG(SIZE_VARIABLE),
  RMSG("Open Book", 0x63),
  UMSG(SIZE_VARIABLE),
  BMSGH("Dye Data", 0x09, handle_dye_s, handle_dye_r),
  UMSG(SIZE_VARIABLE),
  UMSG(0x02),
  UMSG(SIZE_VARIABLE), // 0x98
  BMSG("Multi Placement", 0x1a),
  UMSG(SIZE_VARIABLE),
  SMSG("Help Request", 0x102),
  UMSG(0x135),
  UMSG(0x33),
  RMSGH("Vendor Sell List", SIZE_VARIABLE, handle_vendor_sell_list),
  SMSGH("Vendor Sell Reply", SIZE_VARIABLE, handle_vendor_sell_reply),
  SMSGH("Select Server", 0x03, handle_select_server), // 0xa0
  RMSGH("Update Hitpoints", 0x09, handle_update_hitpoints),
  RMSGH("Update Mana", 0x09, handle_update_mana),
  RMSGH("Update Stamina", 0x09, handle_update_stamina),
  SMSG("System Information", 0x95),
  RMSG("Open URL", SIZE_VARIABLE),
  RMSG("Tip Window", SIZE_VARIABLE),
  SMSG("Request Tip", 0x04),
  RMSGH("Server List", SIZE_VARIABLE, handle_server_list), // 0xa8
  RMSGH("Character List", SIZE_VARIABLE, handle_character_list), // 0xa9
  RMSG("Attack Reply", 0x05), // 0xaa
  RMSG("Text Input Dialog", SIZE_VARIABLE), // 0xab
  SMSG("Text Input Reply", SIZE_VARIABLE), // 0xac
  SMSGH("Unicode Client Talk", SIZE_VARIABLE, handle_unicode_client_talk), // 0xad
  RMSG("Unicode Server Talk", SIZE_VARIABLE),
  UMSG(0x0d),
  RMSGH("Open Dialog Gump", SIZE_VARIABLE, handle_open_gump), // 0xb0
  SMSG("Dialog Choice", SIZE_VARIABLE),
  BMSG("Chat Data", SIZE_VARIABLE),
  RMSG("Chat Text ?", SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  RMSG("Open Chat Window", 0x40),
  SMSG("Popup Help Request", 0x09),
  RMSG("Popup Help Data", SIZE_VARIABLE),
  BMSG("Character Profile", SIZE_VARIABLE), // 0xb8
  RMSG("Chat Enable", 0x03),
  RMSG("Display Guidance Arrow", 0x06),
  SMSG("Account ID ?", 0x09),
  RMSG("Season ?", 0x03),
  SMSG("Client Version", SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  BMSGH("New Commands", SIZE_VARIABLE, handle_new_command_filter_s, handle_new_command_filter_r),
  UMSG(0x24), // 0xc0
  RMSG("Display cliloc String", SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  UMSG(SIZE_VARIABLE),
  UMSG(0x06),
  UMSG(0xcb),
  UMSG(0x01),
  UMSG(0x31),
  UMSG(0x02), // 0xc8
  UMSG(0x06),
  UMSG(0x06),
  UMSG(0x07),
  UMSG(SIZE_VARIABLE)
};

const int EthInterface::m_messagescount=sizeof(EthInterface::m_messages)/sizeof(EthInterface::MessageType);

EthInterface::Command EthInterface::GetCommand(const unsigned char* buf,int bufsize)
{
  if(buf[0]>=m_messagescount)
  {
    // если команда не в указанносм списке
    // нужно проглотить весь буфер
    // и надеяться что следующая команда будет нормальной
    return Command(0,bufsize);
  }

  const MessageType &mt=m_messages[buf[0]];
  if(mt.msgsize==0)
  {
    struct SimpleVairableSize
    {
      Server::ServerChar id;
      Server::ServerShort size;
    };
    if(bufsize>=sizeof(SimpleVairableSize))
    {
      SimpleVairableSize *p=(SimpleVairableSize*)buf;
      if(p->size>bufsize)
        return Command();
      else
        return Command(0,p->size);
    }else
    {
      return Command();
    }
  }else
  {
    if(mt.msgsize<=bufsize)
    {
      return Command(0,mt.msgsize);
    }else
    {
      return Command();
    }
  }
}

EthInterface::Command EthInterface::GetCommand(Fragment &f)
{
  Command &cmd=GetCommand(&f.front(),std::distance(f.begin(),f.end()));
  if(!cmd.Empty())
    f.erase(f.begin()+cmd.begin,f.begin()+cmd.end);
  return cmd;
}

EthInterface::Fragment EthInterface::MakeFragment(const unsigned char* buf,int bufsize,const Command& cmd)
{
  return Fragment(buf+cmd.end,buf+(bufsize-cmd.end));
}
