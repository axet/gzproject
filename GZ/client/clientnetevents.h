#ifndef __CLIENTNETEVENTS_H__
#define __CLIENTNETEVENTS_H__

/// низкий уровень, сетевые события пришедшие на клиент
/// выскоий уровень это уже сложные события CClientEvents

class CClientNetEvents
{
public:
  virtual ~CClientNetEvents() {};
  virtual void handle_walk_request(const void * buf, int bufsize) {}
  virtual void handle_client_talk(const void * buf, int bufsize) {}
  virtual void handle_character_status(const void * buf, int bufsize) {}
  virtual void handle_get_item(const void * buf, int bufsize) {}
  virtual void handle_update_hitpoints(const void * buf, int bufsize) {}
  virtual void handle_client_equip_item(const void * buf, int bufsize) {}
  virtual void handle_double_click(const void * buf, int bufsize) {}
  virtual void handle_update_mana(const void * buf, int bufsize) {}
  virtual void handle_update_stamina(const void * buf, int bufsize) {}
  virtual void handle_enter_world(const void * buf, int bufsize) {}
  virtual void handle_update_item(const void * buf, int bufsize) {}
  virtual void handle_delete_object(const void * buf, int bufsize) {}
  virtual void handle_update_player(const void * buf, int bufsize) {}
  virtual void handle_open_container(const void * buf, int bufsize) {}
  virtual void handle_update_contained_item(const void * buf, int bufsize) {}
  virtual void handle_server_equip_item(const void * buf, int bufsize) {}
  virtual void handle_pause_control(const void * buf, int bufsize) {}
  virtual void handle_status_request(const void * buf, int bufsize) {}
  virtual void handle_global_light_level(const void * buf, int bufsize) {}
  virtual void handle_error_code(const void * buf, int bufsize) {}
  virtual void handle_select_character(const void * buf, int bufsize) {}
  virtual void handle_weather_change(const void * buf, int bufsize) {}
  virtual void handle_target_s(const void * buf, int bufsize) {}
  virtual void handle_target_r(const void * buf, int bufsize) {}
  virtual void handle_vendor_buy_list(const void * buf, int bufsize) {}
  virtual void handle_vendor_buy_reply_s(const void * buf, int bufsize) {}
  virtual void handle_vendor_buy_reply_r(const void * buf, int bufsize) {}
  virtual void handle_update_contained_items(const void * buf, int bufsize) {}
  virtual void handle_update_object(const void * buf, int bufsize) {}
  virtual void handle_open_menu_gump(const void * buf, int bufsize) {}
  virtual void handle_first_login(const void * buf, int bufsize) {}
  virtual void handle_character_list2(const void * buf, int bufsize) {}
  virtual void handle_relay_server(const void * buf, int bufsize) {}
  virtual void handle_second_login(const void * buf, int bufsize) {}
  virtual void handle_dye_s(const void * buf, int bufsize) {}
  virtual void handle_dye_r(const void * buf, int bufsize) {}
  virtual void handle_vendor_sell_list(const void * buf, int bufsize) {}
  virtual void handle_vendor_sell_reply(const void * buf, int bufsize) {}
  virtual void handle_select_server(const void * buf, int bufsize) {}
  virtual void handle_server_list(const void * buf, int bufsize) {}
  virtual void handle_character_list(const void * buf, int bufsize) {}
  virtual void handle_unicode_client_talk(const void * buf, int bufsize) {}
  virtual void handle_server_talk(const void * buf, int bufsize) {}
  virtual void handle_open_gump(const void * buf, int bufsize) {}
  virtual void handle_new_command_filter_s(const void * buf, int bufsize) {}
  virtual void handle_new_command_filter_r(const void * buf, int bufsize) {}
};

#endif
