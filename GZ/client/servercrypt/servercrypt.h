#include "uo_huffman.h"

class CServerCrypt:private DecompressingCopier,private CompressingCopier
{
public:
  CServerCrypt();
  void Decrypt(const unsigned char* buf,int bufsize,unsigned char* bufout,int &bufoutsize);
  void Encrypt(const unsigned char* buf,int bufsize,unsigned char* bufout,int &bufoutsize);
};
