#ifndef CLIENTCOMMANDS_H
#define CLIENTCOMMANDS_H

#include "clientgamecommands.h"

/// Команды от клиента

class CClientCommands:public ClientGameCommands::Control
{
public:
  virtual ~CClientCommands() {}
  /// послыка буфера на сервер
  virtual void SendToServer(const void* buf,int bufsize) = 0;
  virtual void Login(const char*name = 0,const char*pass = 0, const char* charname = 0) = 0;
  virtual void CloseClient() = 0;
  virtual void ShowWindow(bool) = 0;
  virtual bool IsWindowVisible() = 0;
};

#endif
