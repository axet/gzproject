#ifndef __ETHINTERFACE_H__
#define __ETHINTERFACE_H__


#include <set>
#include <vector>

#include "clientnetevents.h"

/// только сетевое общение
/// как для клиента так и для сервера

class EthInterface
{
public:
  /// массив байт
  typedef std::vector<unsigned char> Array;
  /// Fragment - фрагмент пакета
  typedef Array Fragment;
  /// команда
  struct Command
  {
    int begin;
    int end;

    Command():begin(0),end(0) {}
    Command(int _begin,int _end):
      begin(_begin),end(_end) {}
    int Size()
    {
      return end-begin;
    }
    bool Empty()
    {
      return Size()==0;
    }
  };
  /// указатель на обработчика
  typedef void (CClientNetEvents::*MsgHandler)(const void * buf, int bufsize);
  /// тип направления сообщения
  enum MsgDir
  {
    DIR_SEND,   /// client to server
    DIR_RECV,   /// server to client
    DIR_BOTH
  };
  /// описание сообщения
  struct MessageType
  {
    const char * name;
    int msgsize;
    MsgDir direction;
    MsgHandler shandler, rhandler;
  };

  static const MessageType m_messages[];
  static const int m_messagescount;

  /// не меняет фходного буфера
  static Command GetCommand(const unsigned char* buf,int bufsize);
  /// меняет входнй буфер
  static Command GetCommand(Fragment &f);
  /// получает фрагмент (остаток в входном буфере)
  static Fragment MakeFragment(const unsigned char* buf,int bufsize,const Command&);
};

#endif
