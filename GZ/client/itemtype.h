#ifndef __ITEMTYPE_H__
#define __ITEMTYPE_H__

#include "types.h"

namespace Client
{
  class ItemType;
};

/// Обьект предмета

/// необходимо для идентифицации предмета. Если необходимо реботать с каким либо типом предмета нужно создать
/// этот обьект. К примеру я хочу работать с "логами" (logs, дровами).  Я хочу преложить их из одого ящика
/// в дрйгой, для этого я создаю обьект ItemType logs(ItemType::itLog_s), и даю команду на перекладывание обьекта
/// logs из одного ящика в другой.
class Client::ItemType:public ShortList
{
public:
  enum BuildInType {itOre_s,itLog_s,itDoorOpen_s,itDoorClose_s};

  ItemType();
  /// Конструктор от одного из встроенных типов
  ItemType(BuildInType);
  /// Конструтор по идентификатору типа, тип береться из мира сферы.
  ItemType(unsigned short);

  ItemType &operator = (unsigned short s);
  bool operator == (unsigned short s) const;
  bool oneof(unsigned short s) const ;
  bool oneof(const ItemType& s) const ;
  operator unsigned short() const;
  bool operator == (const ItemType& s) const;
};

#endif
