#include "stdafx.h"
#include "interprocslave.h"

#include <string>
#include <io.h>
#include <objbase.h>

#include "fluke/interprocexception.h"
#include "fluke/sync.h"
#include "debugger/debug.h"
#include <ErrorReport/errorreport.h>
#include <misc/SentrySc.h>

/// время на ожидание события из сокета
const static int g_mailslotreadwait=500;

CInterProcSlave::CInterProcSlave():
  m_file(INVALID_HANDLE_VALUE),
  m_slot(INVALID_HANDLE_VALUE)
{
}

void CInterProcSlave::Connect(unsigned postfix)
{
  SentrySc sc(m_cs);

  m_postfix=postfix;

  std::string &ms=GetClientPostfix(GetSlaveSlotName(),postfix);
  m_slot = CreateMailslot(ms.c_str(),0,g_mailslotreadwait,(LPSECURITY_ATTRIBUTES) NULL);

  if (m_slot == INVALID_HANDLE_VALUE) 
    throw interprocexception(ErrorReport("can't create mail slot")+ErrorReport(ms.c_str(),GetLastErrorString().c_str())); // local error handler 

  CSyncServer master(GetMasterMutex(),GetCurrentProcessId());
  CSyncClient slave(GetSlaveMutex(),GetCurrentProcessId());

  HANDLE h=CreateMutex(0,TRUE,GetClientPostfix(GetMasterInterproc(),postfix).c_str());
  if(GetLastError()!=ERROR_ALREADY_EXISTS)
  {
    master.WaitClient();
  }else
  {
    slave.RunClient();
  }
  CloseHandle(h);

  std::string &fname=GetClientPostfix(GetMasterSlotName(),postfix);
  m_file= CreateFile(fname.c_str(), 
    GENERIC_WRITE, 
    FILE_SHARE_READ,  // required to write to a mailslot 
    (LPSECURITY_ATTRIBUTES) NULL, 
    OPEN_EXISTING, 
    FILE_ATTRIBUTE_NORMAL, 
    (HANDLE) NULL); 

  if (m_file== INVALID_HANDLE_VALUE) 
  { 
    throw interprocexception(ErrorReport("can't create file",fname.c_str()));
  } 
}

int CInterProcSlave::Read(unsigned char* buf,int bufsize)
{
  SentrySc sc(m_cs);

  DWORD cbRead;

  if(m_slot==INVALID_HANDLE_VALUE)
    Connect(m_postfix);

  BOOL fResult = ReadFile(m_slot, buf, bufsize, &cbRead, 0);
  if(fResult==FALSE)
  {
    int err=GetLastError();
    if(err==ERROR_SEM_TIMEOUT)
    {
      //DBGTRACE("error read from master TIMEOUT\n");
      if(!Ping())
      {
        throw std::exception("NO PING AFTER TIMEOUT DISCONNECTIONG\n");
      }
    }else
    {
      throw std::exception(ErrorReport("ERROR READ FROM MASTER",GetLastErrorString().c_str()));
    }
  }
  sc.Unlock();

  return cbRead;
}

CInterProcSlave::~CInterProcSlave()
{
  Disconnect();
  //if(m_receivethread!=0)
  //{
  //  Disconnect();
  //  CloseHandle(m_receivethread);
  //  m_receivethread=0;
  //}
}

void CInterProcSlave::Disconnect()
{
  SentrySc sc(m_cs);

  if(m_file!=INVALID_HANDLE_VALUE)
  {
    CloseHandle(m_file);
    m_file=INVALID_HANDLE_VALUE;
  }
  if(m_slot!=INVALID_HANDLE_VALUE)
  {
    CloseHandle(m_slot);
    m_slot=INVALID_HANDLE_VALUE;
  }
}

void CInterProcSlave::Write(const void* buf,int size)
{
  DWORD cbWritten;
  if(!WriteFile(m_file, buf, size,&cbWritten,(LPOVERLAPPED) NULL)||cbWritten!=size)
    throw BadWrite();
}

bool CInterProcSlave::Ping()
{
  try
  {
    Write("",0);
    return true;
  }catch(BadWrite &)
  {
  }
  return false;
}
