#ifndef __INTERPROCSLAVE_H__
#define __INTERPROCSLAVE_H__

#include <atlbase.h>

#include "interproc.h"

class CInterProcSlave:public CInterProc
{
  void* m_slot, *m_file;
  unsigned m_postfix;
  CComAutoCriticalSection m_cs;

  bool Ping();

public:
  CInterProcSlave();
  virtual ~CInterProcSlave();
  void Connect(unsigned postfix);
  void Disconnect();
  int Read(unsigned char* buf,int bufsize);
  void Write(const void* buf,int size);
};

#endif
