#include "stdafx.h"
#include "interproc.h"
#include <sstream>

std::string CInterProc::GetClientPostfix(const char* p,unsigned dwClientId)
{
  std::string str(p);
  std::stringstream sstr;
  sstr<<(unsigned int)dwClientId;
  str+=sstr.str();
  return str;
}

const char* CInterProc::GetMasterSlotName()
{
  static char slotname[]="\\\\.\\mailslot\\mmast";
  return slotname;
}

const char* CInterProc::GetSlaveSlotName()
{
  static char slotname[]="\\\\.\\mailslot\\mslave";
  return slotname;
}

const char* CInterProc::GetSlaveMutex()
{
  return "slavemutex";
}

const char* CInterProc::GetMasterMutex()
{
  return "mastermutex";
}

const char* CInterProc::GetMasterInterproc()
{
  return "interproc_master";
}
