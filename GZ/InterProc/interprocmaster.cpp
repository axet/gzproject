#include "stdafx.h"
#include "interprocmaster.h"

#include <string>
#include <process.h>
#include <objbase.h>

#include "fluke/interprocexception.h"
#include <ErrorReport/ErrorReport.h>
#include <ErrorReport/GetLastError.h>
#include "fluke/sync.h"
#include "debugger/debug.h"
#include <misc/SentrySc.h>

/// время данное клиенту чтобы завершить работу после получения команды выхода.
/// на практике должно быть мгновенным, но могут возникать непредвиденные обстоятельства
/// при которых клиент зависает
const static int g_waitforclientterminate=30000;
/// время на ожидание события из сокета
const static int g_mailslotreadwait=500;

CInterProcMaster::CInterProcMaster():
m_file(INVALID_HANDLE_VALUE),
m_slot(INVALID_HANDLE_VALUE)
{
}

void CInterProcMaster::WaitForClientTerminate()
{
  SentrySc sc(m_cs);

  HANDLE h=OpenProcess(SYNCHRONIZE,FALSE,m_clientprocessid);
  if(WaitForSingleObject(h,g_waitforclientterminate)==WAIT_TIMEOUT)
  {
    CloseHandle(h);
    throw interprocexception("Wait Process TimeOut");
  }
  CloseHandle(h);
}

bool CInterProcMaster::Connect(unsigned postfix)
{
  SentrySc sc(m_cs);

  m_clientprocessid=postfix;

  std::string ms=GetClientPostfix(GetMasterSlotName(),postfix);
  m_slot = CreateMailslot(ms.c_str(),0,g_mailslotreadwait,(LPSECURITY_ATTRIBUTES) NULL);

  if (m_slot == INVALID_HANDLE_VALUE) 
    throw interprocexception(ErrorReport("can't create mail slot",GetLastErrorString().c_str())+ErrorReport(ms.c_str())); // local error handler 

  CSyncServer slave(GetSlaveMutex(),postfix);
  CSyncClient master(GetMasterMutex(),postfix);

  HANDLE h=CreateMutex(0,TRUE,GetClientPostfix(GetMasterInterproc(),postfix).c_str());
  if(::GetLastError()!=ERROR_ALREADY_EXISTS)
  {
    slave.WaitClient();
  }else
  {
    master.RunClient();
  }
  CloseHandle(h);

  std::string fname=GetClientPostfix(GetSlaveSlotName(),postfix);
  m_file= CreateFile(fname.c_str(), 
    GENERIC_WRITE, 
    FILE_SHARE_READ,  // required to write to a mailslot 
    (LPSECURITY_ATTRIBUTES) NULL, 
    OPEN_EXISTING, 
    FILE_ATTRIBUTE_NORMAL,
    (HANDLE) NULL); 
  if (m_file== INVALID_HANDLE_VALUE) 
  { 
    throw interprocexception(ErrorReport("error create file",GetLastErrorString().c_str())+
      ErrorReport(fname.c_str()));
  }

  return true;
}

CInterProcMaster::~CInterProcMaster()
{
  Disconnect();
}

int CInterProcMaster::Read(unsigned char *buf,int bufsize)
{
  SentrySc sc(m_cs);
  DWORD bb;
  BOOL fResult = ReadFile(m_slot, 
    buf, 
    bufsize, 
    &bb, 
    0);

  if(!fResult)
  {
    int err=GetLastError();
    if(err==ERROR_SEM_TIMEOUT)
    {
      if(!Ping())
        throw IPMLocalConnectionLost();
    }else
      throw IPMLocalConnectionLost(GetLastErrorString(err).c_str());
  }
  return bb;
}

bool CInterProcMaster::IsConnected()
{
  return m_file!=INVALID_HANDLE_VALUE || m_slot!=INVALID_HANDLE_VALUE;
}

void CInterProcMaster::Disconnect()
{
  SentrySc sc(m_cs);
  if(m_slot!=INVALID_HANDLE_VALUE)
    CloseHandle(m_slot);

  if(m_file!=INVALID_HANDLE_VALUE)
    CloseHandle(m_file);

  m_file=INVALID_HANDLE_VALUE;
  m_slot=INVALID_HANDLE_VALUE;
}

void CInterProcMaster::Write(const unsigned char* buf,int bufsize)
{
  SentrySc sc(m_cs);
  DWORD cbWritten;
  if(!WriteFile(m_file,buf, bufsize,&cbWritten, (LPOVERLAPPED) NULL)||cbWritten!=bufsize)
    throw BadWrite();
}

bool CInterProcMaster::Ping()
{
  try
  {
    unsigned char buf[1];
    Write(buf,0);
    return true;
  }catch(BadWrite&)
  {
    return false;
  }
}
