#ifndef __INTERPROCMASTER_H
#define __INTERPROCMASTER_H

#include "interproc.h"

class CInterProcMaster:public CInterProc
{
private:
  CComAutoCriticalSection m_cs;
  void* m_slot,*m_file;
  DWORD m_clientprocessid;

  bool Ping();

public:
  CInterProcMaster();
  ~CInterProcMaster();
  // postfix - значение аналогичное порту на машине tcp\ip содедения
  //	возможно потребуеться расширить функцию соеденения чтобы она позволяла
  //	работать по сети
  bool Connect(unsigned postfix);
  void Disconnect();
  void WaitForClientTerminate();
  bool IsConnected();
  int Read(unsigned char* buf,int bufsize);
  void Write(const unsigned char* buf,int bufsize);
};

#endif
