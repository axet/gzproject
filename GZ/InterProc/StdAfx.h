// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__5350BC3F_EAF2_4058_80EF_906FE26D76C0__INCLUDED_)
#define AFX_STDAFX_H__5350BC3F_EAF2_4058_80EF_906FE26D76C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers


#include "../include/mfc.h"

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__5350BC3F_EAF2_4058_80EF_906FE26D76C0__INCLUDED_)
