#ifndef __INTERPROC_H
#define __INTERPROC_H

#include <string>

class CInterProc
{
protected:
  static std::string GetClientPostfix(const char* p,unsigned dwClientId);

  // для определения жив ли клиент
  const char* GetSlaveMutexALive();
  const char* GetMasterMutexALive();

  // для синхронизации мастера и слейва
  const char* GetSlaveMutex();
  // для синхронизации мастера и слейва
  const char* GetMasterMutex();
  // На случай если мастер первым приходит на точку
  const char* GetMasterInterproc();
  // интерфейс на мастера
  const char * GetMasterSlotName();
  // интерфейс на слейва
  const char * GetSlaveSlotName();
public:

  class BadWrite:public std::exception
  {
  public:
    BadWrite():exception("bad write") {}
  };

  class IPMLocalConnectionLost:public std::exception
  {
    std::string str;
  public:
    IPMLocalConnectionLost(const char*p = 0)
    {
      if(p==0)
        p="IPMLocalConnectionLost";
      str=p;
    }
    const char* what() const throw()
    {
      return str.c_str();
    }
  };
};

#endif
