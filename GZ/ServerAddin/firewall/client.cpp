#include "stdafx.h"

#pragma warning(disable:4786)

#include <vector>
#include <atlbase.h>

#include "client.h"
#include "../../client/packets.h"
#include "../../client/clientcrypt/clientcrypt.h"

CClient::CClient(SOCKET s):
  m_socket(s),
  m_newclient(true),
  m_login(false),m_crypt(cryptNone),m_badclient(false),
  m_cfgcrypt(true),m_cfgsecure(true)
{
  AddAdvise(this);

  SOCKADDR_IN in;
  int insize=sizeof(in);
  getpeername(s,(sockaddr*)&in,&insize);
  m_ipaddress=in.sin_addr;
  HOSTENT *p=gethostbyaddr((const char*)&in.sin_addr,sizeof(in.sin_addr),AF_INET);
  //m_ipaddress=(*p).h_name;
}

CClient::~CClient()
{
  RemoveAdvise(this);
}

bool CClient::IsLoggedIn() const
{
  return m_login;
}

bool CClient::IsNewClient() const
{
  return m_newclient;
}

void CClient::SetNewClient(unsigned key)
{
  SetKey(key);
  m_newclient=false;

  switch(key)
  {
  case 0:
    m_crypt=cryptNone;
    break;
  default:
    m_crypt=cryptDetect;
    break;
  }
}

bool CClient::BadClient() const
{
  return m_badclient;
}

/*

  [input buffer]
    пришедкший пакет

  [decrypt, parse]
    его нужно расшифровать и обработать

  [change]
    в момент обработки может оказаться
    что пакет необходимо заменить другим
    чаще это простое игнорирование пакета
    те пакет от клиента заменяеться пакетом "ответ на Пинг"

  [encrypt]
    отдаем на сторону сервера
    шифрование требуеться для того чтобы мы могли менять трафик
    от клиента иначе это былобы не возможно так как пакеты
    шифруються своим кодом.

*/

void CClient::ReceiveData(unsigned char* buf, int *bufsize,int bufmaxsize)
{
  if(*bufsize<=0)
    return;

  //unsigned char tempbuf[16384];

  unsigned char* input=buf;
  int inputlen=*bufsize;
  int inputmaxlen=bufmaxsize;

  if(IsNewClient())
  {
    SetNewClient(*reinterpret_cast<const unsigned*>(input));
    input+=sizeof(unsigned);
    inputlen-=sizeof(unsigned);
    inputmaxlen-=sizeof(unsigned);
  }

  if(inputlen>0)
  {
    handle_receive_data((const void*)buf,*bufsize);

    if(m_crypt==cryptDetect)
    {
      crypt_tag crypt=GetEncryption(input,inputlen);
      if(crypt==cryptUnknown)
        throw std::exception("encryption unknown");
      SetEncryption(crypt);
      m_crypt=cryptSet;

      m_clientcrypt.SetKey(GetKey());
      m_clientcrypt.SetEncryption(GetEncryption());
    }

    CClientCrypt::Decrypt(input,inputlen);

    try
    {
      PostData(input,inputlen);
    }catch(CClientInterface::CReplacePacket &pack)
    {
      // packetdelta - Это длинна пакета который идет перед данными которые шифруються
      // всегда значение равно 0, за исключением первого пакета
      int packetdelta=*bufsize-inputlen;
      memcpy(input,&pack.m_bytearray.front(),pack.m_bytearray.size());
      inputlen=pack.m_bytearray.size();
      *bufsize=packetdelta+pack.m_bytearray.size();
    }
  }

  m_clientcrypt.Encrypt(input,inputlen);
}

int CClient::Send(Packets::Packet &packet)
{
  unsigned char buf[16384];
  int bufsize=sizeof(buf);
  m_servercrypt.Encrypt(&packet.front(),packet.size(),buf,bufsize);

  return send(m_socket,(const char*)buf,bufsize,0);
}

const char* CClient::GetLogin() const
{
  if(m_loginname.empty())
    return 0;
  return m_loginname.c_str();
}

unsigned CClient::GetIPAddress() const
{
  return m_ipaddress.S_un.S_addr;
}

void CClient::Kick()
{
  m_badclient=true;
}

void CClient::handle_first_login(const void* buf, int bufsize)
{
  Packets::scsLogin *scsLogin=(Packets::scsLogin *)buf;
  m_loginname=scsLogin->name;
}

void CClient::handle_second_login(const void* buf, int bufsize)
{
  Packets::scsPostLogin *scsLogin=(Packets::scsPostLogin *)buf;
  m_loginname=scsLogin->name;

  m_login=true;
}

void CClient::handle_select_character(const void* buf, int bufsize)
{
  Packets::scsPreLogin *scsLogin=(Packets::scsPreLogin *)buf;
  m_charname=scsLogin->Character_Name;

  return CClientNetEvents::handle_select_character(buf,bufsize);
}

const char* CClient::GetCharName() const
{
  return m_charname.c_str();
}
