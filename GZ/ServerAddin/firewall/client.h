#ifndef __CLIENT_H__
#define __CLIENT_H__

#pragma warning(disable:4786)

#include <winsock2.h>
#include "../../client/clientcrypt/clientcrypt.h"
#include "../../client/clientinterface.h"
#include "../../clientsecure/clientsecure.h"
#include "../../client/packets.h"
#include "../../client/servercrypt/servercrypt.h"

namespace Firewall{

  class CClient:
    public CClientInterface,
    private CClientCrypt,// decrupt
    public CClientNetEvents,
    public ClientSecure::CClientSecure
  {
    SOCKET m_socket;
    CClientCrypt m_clientcrypt; // crypt
    CServerCrypt m_servercrypt;
    // если не получено ни одного пакета с клиента то это = true
    // это особенность трафика, первый пакет  = 4 байта, IP адресс клиента
    bool m_newclient;
    bool m_badclient;
    std::string m_loginname,m_charname;
    //std::string m_ipaddress;
    in_addr m_ipaddress;
    bool m_login;

  public:
    // включен ли ClientCrypt
    bool m_cfgcrypt;
    // включен ли ClientSecure
    bool m_cfgsecure;
    enum crypt{cryptNone,cryptDetect,cryptSet} m_crypt;

    CClient(SOCKET s);
    ~CClient();
    // еще не одного пакеты с килента не пришло true
    bool IsNewClient() const;
    void SetNewClient(unsigned key);
    // клиент уже зашел в игру? true
    // second login
    bool IsLoggedIn() const;
    // если с клиентом нужно разорвать соеденение true
    bool BadClient() const;

    const char* GetLogin() const;
    const char* GetCharName() const;
    unsigned GetIPAddress() const;
    void Kick();
    // возвращает число отправленных байт, это будет число
    // скорее всего меньше длинны пакета, поскольку он сжимаеться
    int Send(Packets::Packet &packet);

    void ReceiveData(unsigned char* buf,int *bufsize,int bufmaxsize);

    //virtual void handle_receive_data(const void* buf, int bufsize);
    virtual void handle_first_login(const void* buf, int bufsize);
    virtual void handle_second_login(const void* buf, int bufsize);
    virtual void handle_select_character(const void* buf,int bufsize);
  };

}
using namespace Firewall;

#endif
