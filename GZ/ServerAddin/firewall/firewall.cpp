#include "stdafx.h"
#include "firewall.h"

#include <debugger/debug.h>

#pragma comment(lib,"wsock32.lib")

// CFirewall {

void CFirewall::hook_accept_leave(CMemMngr* mngr,int *retval)
{
  CFirewall* current=dynamic_cast<CFirewall*>(mngr);

  SOCKET s=*(SOCKET*)retval;
  //struct sockaddr *addr=va_arg(args,sockaddr*);
  //int *addrlen=va_arg(args,int*);

  DBGTRACE("accpet game connection: %08x\n",s);

  current->m_sockets[s]=new CClientKernel(current,s);
}

void CFirewall::hook_closesocket(CMemMngr* mngr,va_list args)
{
  CFirewall* current=dynamic_cast<CFirewall*>(mngr);

  SOCKET s=va_arg(args,SOCKET);
  
  DBGTRACE("close game connection: %08x\n",s);

  socketmap_t::iterator i=current->m_sockets.find(s);
  if(i!=current->m_sockets.end())
  {
    delete i->second;
    current->m_sockets.erase(i);
  }
}

void CFirewall::hook_recv(CMemMngr* mngr,va_list args)
{
  CFirewall* current=dynamic_cast<CFirewall*>(mngr);

  current->m_recv_socket=va_arg(args,SOCKET);
  current->m_recv_buf=va_arg(args,unsigned char*);
  current->m_recv_bufmaxsize=va_arg(args,unsigned);
}

void CFirewall::hook_recv_leave(CMemMngr* mngr,int *retval)
{
  CFirewall* current=dynamic_cast<CFirewall*>(mngr);

  try
  {
    socketmap_t::iterator i=current->m_sockets.find(current->m_recv_socket);
    if(i!=current->m_sockets.end())
    {
      current->m_currentuser=i->second;
      current->m_currentuser->ReceiveData(current->m_recv_buf,retval,current->m_recv_bufmaxsize);
      if(current->m_currentuser->BadClient())
      {
        DBGTRACE("bad client: %08x\n",current->m_recv_socket);
        *retval=-1;
      }
    }
    current->m_currentuser=0;
  }catch(std::exception &e)
  {
    *retval=-1;

    if(current->m_currentuser!=0)
    {
      DBGTRACE("exception: login '%s', '%08x'",
        current->m_currentuser->GetLogin()?current->m_currentuser->GetLogin():"",
        current->m_currentuser->GetIPAddress());
    }else
      DBGTRACE("exception: null user\n");
  }
}

void CFirewall::hook_send(CMemMngr* mngr,va_list args)
{
  CFirewall* current=dynamic_cast<CFirewall*>(mngr);

  SOCKET s=va_arg(args,SOCKET);
  unsigned char *buf=va_arg(args,unsigned char*);
  int bufsize=va_arg(args,int);
  int flags=va_arg(args,int);

  socketmap_t::iterator i=current->m_sockets.find(current->m_recv_socket);
  CClientKernel* currentuser=0;
  if(i!=current->m_sockets.end())
    currentuser=i->second;

  if(currentuser&&currentuser->IsLoggedIn())
  {
    unsigned char bufout[16384];
    int bufoutsize=sizeof(bufout);
    CServerCrypt servercrypt;
    servercrypt.Decrypt(buf,bufsize,bufout,bufoutsize);
    current->m_send_ret=currentuser->Send(Packets::Packet(bufout,bufoutsize));
  }else
  {
    current->m_send_ret=send(s,(const char*)buf,bufsize,flags);
  }
}

void CFirewall::hook_send_leave(CMemMngr* mngr,int *retval)
{
  CFirewall* current=dynamic_cast<CFirewall*>(mngr);

  *retval=current->m_send_ret;
}

int CFirewall::hook_send_stub(SOCKET s,unsigned char*,int,int)
{
  return 0;
}

CFirewall::CFirewall()
{
  char dll[][64]=
  {
    "wsock32.dll",
    "ws2_32.dll"
  };
  for(int i=0;i<sizeof(dll)/64;i++)
  {
    CatchImportFunction("accept",dll[i],0,hook_accept_leave);
    CatchImportFunction("recv",dll[i],hook_recv,hook_recv_leave);
    //CatchImportFunction("send",dll[i],hook_send,hook_send_leave);
    RedirectImportFunction("send",dll[i],hook_send,hook_send_leave,(Proc)hook_send_stub);
    CatchImportFunction("closesocket",dll[i],hook_closesocket);
  }
}

void CFirewall::AddModule(CFirewallModule *m)
{
  m->m_fm=this;
  m_modules.push_back(m);
}

// } CFirewall

// { CCClientKernel

CFirewall::CClientKernel::CClientKernel(CFirewall*pp,SOCKET s):CClient(s)
{
  for(CFirewall::modules_t::iterator i=pp->m_modules.begin();i!=pp->m_modules.end();i++)
    AddController(*i);
}

void CFirewall::CClientKernel::AddController(CClientNetEvents*p)
{
  m_netevents.push(AddAdvise(p));
}

CFirewall::CClientKernel::~CClientKernel()
{
  while(!m_netevents.empty())
  {
    RemoveAdvise(m_netevents.top());
    m_netevents.pop();
  }
}

// } CClientKernel
