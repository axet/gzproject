#ifndef __FIREWALLMODULE_H__
#define __FIREWALLMODULE_H__

#include "../../client/ClientNetEvents.h"

namespace Firewall{

  class CFirewall;
  class CClient;

  class CFirewallModule:public CClientNetEvents
  {
    CFirewall* m_fm;
    friend CFirewall;

  protected:
    inline CFirewall* GetFirewall()
    {
      return m_fm;
    }
    // возвращет текущую сесиию, команду можно исползьовать когда идет обработка
    // пакета.
    CClient* GetCurrentSession();

  public:
    virtual ~CFirewallModule(){};
  };
}

using namespace Firewall;

#endif
