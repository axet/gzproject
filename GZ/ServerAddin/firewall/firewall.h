#ifndef __FIREWALL_H__
#define __FIREWALL_H__

class CInterNetSASlave;

#include <winsock2.h>
#include <map>
#include <stack>

#include <iofluke/memmngr.h>
#include "client.h"
#include "../../serveraddinio/internetsaslave.h"
#include "FirewallModule.h"
#include "../serveraddinmodule.h"

namespace Firewall{

  class CClient;
  class CFirewall;
  class CFirewallModule;

  class CFirewall: public IOFluke::CMemMngr,public CServerAddinModule
  {
    class CClientKernel;
    friend CClientKernel;
    friend CFirewallModule;

    typedef std::vector<CFirewallModule*> modules_t;
    modules_t m_modules;
    typedef std::map<SOCKET,CClientKernel*> socketmap_t;
    socketmap_t m_sockets;

    // такая схема с текущем пользователем нужна только для Fluke на чтение
    // из сокета, другие функции кроме hook_recv_leave не должны использовать
    // эту группу переменных, Надо запихнуть их в отделный класс
    SOCKET m_recv_socket;
    unsigned char* m_recv_buf;
    int m_recv_bufmaxsize;
    CClientKernel* m_currentuser;

    // для отправки похоже..
    int m_send_ret;

    static void hook_closesocket(CMemMngr* mngr,va_list args);
    static void hook_accept(CMemMngr* mngr,va_list args);
    static void hook_accept_leave(CMemMngr* mngr,int *retval);
    static void hook_recv(CMemMngr* mngr,va_list args);
    static void hook_recv_leave(CMemMngr* mngr,int *retval);
    static void hook_send(CMemMngr* mngr,va_list args);
    static void hook_send_leave(CMemMngr* mngr,int *retval);
    static int __stdcall hook_send_stub(SOCKET s,unsigned char*,int,int);

    const char* GetName()
    {
      return "Firewall";
    }

  public:
    void AddModule(CFirewallModule*);

    CFirewall();
  };

  class CFirewall::CClientKernel:public CClient
  {
    std::stack<CClientNetEvents*> m_netevents;

    void AddController(CClientNetEvents*);

  public:
    CClientKernel(CFirewall*,SOCKET s);
    ~CClientKernel();
  };
}

using namespace Firewall;

#endif
