// serveraddin.cpp : Defines the entry point for the DLL application.
//

class CServerAddin;

#define _AFXDLL
#include "../include/mfc.h"

#include <Altova/AltovaLib.h>

#undef _USRDLL
#include "xsd/SchemaServerAddinConfig/SchemaServerAddinConfig.h"
#define _USRDLL

#include <process.h>
#include <map>
#include <string>
#include <atlbase.h>

#include <ControlWindow/controlwindow.h>
#include <debugger/debug.h>
#include <iniext/iniext.h>
#include "serveraddin.h"
#include "speedhack/speedhack.h"
#include "../clientsecure/clientsecure.h"
#include "accountmanager/accountmanager.h"
#include "accountmanager/ipprotection.h"

using namespace ssa;

CServerAddin *g_serveraddin;
ControlWindow::CControlWindow g_mywnd;

struct exitprogramm
{
  HMODULE h;
  CServerAddin * p;

  exitprogramm(HMODULE hh,CServerAddin* pp):h(hh),p(pp) {}
};

void ExitProgramm(exitprogramm* e);

//

CServerAddin::CServerAddin():m_module(0),m_runthread(0)
{
  m_firewall=new CFirewall();
  AddModule(m_firewall);
  CSpeedHack *sp=new CSpeedHack();
  AddModule(sp);
  m_firewall->AddModule(sp);
  ClientSecure::CClientSecure *sc=new ClientSecure::CClientSecure();
  AddModule(sc);
  m_firewall->AddModule(sc);
  CAccountManager* accountmanager=new CAccountManager;
  AddModule(accountmanager);
  CIpProtection *ipprotection=new CIpProtection(accountmanager);
  m_firewall->AddModule(ipprotection);
  AddModule(ipprotection);
  CPrivileges *privileges=new CPrivileges(accountmanager);
  m_firewall->AddModule(privileges);
  AddModule(privileges);
}

CServerAddin::~CServerAddin()
{
  if(m_runthread!=0)
  {
    WaitForSingleObject((HANDLE)m_runthread,-1);
    CloseHandle((HANDLE)m_runthread);
    m_runthread=0;
  }

  if(m_module)
    SaveConfigs();

  for(ModuleList::iterator i=m_modulelistall.begin();i!=m_modulelistall.end();i++)
    delete *i;
}

const char* CServerAddin::GetConfigName()
{
  return "ServerAddin.config";
}

void CServerAddin::LoadConfigs()
{
  try
  {
    CSchemaServerAddinConfigDoc doc;
    CSchemaServerAddinConfigType type1,type2;
    CSchemaServerAddinConfigType type=doc.Load((IniExt::GetModuleRunDir(m_module)+GetConfigName()).c_str());
    for(int i=0;i<type.GetModuleCount();i++)
    {
      CModuleType module=type.GetModuleAt(i);
      CModuleList::iterator mod=m_modulelistname.find(((std::string)module.GetName()).c_str());
      if(mod!=m_modulelistname.end())
        mod->second->LoadConfig(((std::string)module.GetData()).c_str());
    }
  }catch(CXmlException &)
  {
    ;
  }
}

void CServerAddin::SaveConfigs()
{
  try
  {
    CSchemaServerAddinConfigDoc doc;
    CSchemaServerAddinConfigType type;
    for(CModuleList::iterator i=m_modulelistname.begin();i!=m_modulelistname.end();i++)
    {
      CModuleType module;
      module.AddName(i->first.c_str());
      module.AddData(i->second->SaveConfig().c_str());
      type.AddModule(module);
    }
    doc.SetRootElementName("","SchemaServerAddinConfig");

    doc.Save((IniExt::GetModuleRunDir(m_module)+GetConfigName()).c_str(),type);
  }catch(CXmlException &)
  {
    ;
  }
}

void CServerAddin::AddModule(CServerAddinModule* e)
{
  e->m_sa=this;
  if(const char*name=e->GetName())
    m_modulelistname[e->GetName()]=e;
  m_modulelistall.insert(e);
}

void CServerAddin::Unload()
{
  //CInterNetSASlave::Disconnect();
  _beginthread((void(*)(void*))ExitProgramm,0,new exitprogramm(m_module,this));
}

void CServerAddin::ModuleCommand(std::string &name,std::string &xmlinput,std::string &xmloutput)
{
  // необходимо пройдтись по списку модулей (map<std::string,..>) выбрать нужный
  // и запульнуть в него командой;

  CModuleList::iterator i;
  if((i=m_modulelistname.find(name))==m_modulelistname.end())
    return;

  i->second->Command(xmlinput,xmloutput);
}

void CServerAddin::Run(CServerAddin* p)
{
  try
  {
    CoInitialize(0);

    p->LoadConfigs();

    p->Listen(48989);

    CoUninitialize();
  }catch(std::exception &e)
  {
    DBGTRACE("%s\n",e.what());
    MessageBox(0,e.what(),"Exception Slave",MB_ICONHAND);
  }
  _endthreadex(0);
}

void CServerAddin::InitInstance(HMODULE hmod)
{
  m_module = hmod;

  //g_mywnd.Control();

  typedef void (ddd)(void*);
  m_runthread=_beginthread( (ddd*) Run,0,this);
}

void ExitProgramm(exitprogramm* e)
{
  e->p->Disconnect();
  HMODULE h=e->h;
  delete e;
  __asm
  {
    push 0x12345678; // exit code from this thread, ExitThread(0x12345678)
    push 0x0;// return address for ExitThread
    push h; // FreeLibrary(h)
    push ExitThread;
    jmp dword ptr [FreeLibrary];
  }
}

///

class CMyApp:public CWinApp,public CFlukeSlave
{
public:
  CMyApp()
  {
    DBGTRACE("CMyApp\n");
  };
  void FlukeInit(HMODULE h);
  BOOL InitInstance();
  int ExitInstance();
};

CMyApp myApp;

void CMyApp::FlukeInit(HMODULE h)
{
  CoInitialize(0);
  g_serveraddin=new CServerAddin;
  g_serveraddin->InitInstance((HMODULE)m_hInstance);
  CoUninitialize();
}

BOOL CMyApp::InitInstance()
{
  Create((HMODULE)m_hInstance);
	return CWinApp::InitInstance();
}

int CMyApp::ExitInstance()
{
  CoInitialize(0);
  if(g_serveraddin)
    delete g_serveraddin;
  g_serveraddin=0;
  Close();
  CoUninitialize();
  return 0;
}
