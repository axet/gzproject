////////////////////////////////////////////////////////////////////////
//
// SchemaSpeedHack_CGetPropertyType.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef SchemaSpeedHack_CGetPropertyType_H_INCLUDED
#define SchemaSpeedHack_CGetPropertyType_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000




namespace SpeedHack // URI: www.sourceforge.net/projects/gzproject
{


class SchemaSpeedHack_DECLSPECIFIER CGetPropertyType : public CNode
{
public:
	CGetPropertyType() : CNode() {}
	CGetPropertyType(CNode& rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
	CGetPropertyType(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
	static EGroupType GetGroupType();

	//
	// Request Request (1...1)
	//
	static int GetRequestMinCount();
	static int GetRequestMaxCount();
	int GetRequestCount();
	bool HasRequest();
	void AddRequest(CRequestType& Request);
	void InsertRequestAt(CRequestType& Request, int nIndex);
	void ReplaceRequestAt(CRequestType& Request, int nIndex);
	CRequestType GetRequestAt(int nIndex);
	CRequestType GetRequest();
	MSXML2::IXMLDOMNodePtr GetStartingRequestCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedRequestCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CRequestType GetRequestValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveRequestAt(int nIndex);
	void RemoveRequest();

	//
	// Respond Respond (1...1)
	//
	static int GetRespondMinCount();
	static int GetRespondMaxCount();
	int GetRespondCount();
	bool HasRespond();
	void AddRespond(CRespondType& Respond);
	void InsertRespondAt(CRespondType& Respond, int nIndex);
	void ReplaceRespondAt(CRespondType& Respond, int nIndex);
	CRespondType GetRespondAt(int nIndex);
	CRespondType GetRespond();
	MSXML2::IXMLDOMNodePtr GetStartingRespondCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedRespondCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CRespondType GetRespondValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveRespondAt(int nIndex);
	void RemoveRespond();
};


} // end of namespace SpeedHack

#endif // SchemaSpeedHack_CGetPropertyType_H_INCLUDED
