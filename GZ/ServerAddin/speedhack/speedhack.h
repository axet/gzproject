class CServerAddin;

#include "../../client/clientnetevents.h"
#include "../firewall/firewallmodule.h"
#include "../serveraddinmodule.h"

class CSpeedHack:public CFirewallModule,public CServerAddinModule
{
  int m_walk_lastwalkcount,m_walk_lastwalktotaltime,m_walk_lastwalkrequest,m_walk_lastdirection;
  // настройки, скорость на количество шагов
  int m_walk_speed,m_walk_count;

  // количество пердметов
  unsigned m_item_manipulate_count_items;
  // в единицу времени
  unsigned m_item_manipulate_time;
  // сколько было исползовано предметов
  unsigned m_item_manipulate_count;
  // время затраченное на использование
  unsigned m_item_manipulate_times;
  // время полсденего использования
  unsigned m_item_manipulate_last;

  // возвращает false если частота использования предмета превышена
  bool BadItemManipulate();

  const char* GetName()
  {
    return "SpeedHack";
  }
  virtual void Command(std::string& xmlinput,std::string& xmloutput);
  virtual std::string SaveConfig();
  virtual void LoadConfig(const char* xml);

  virtual void handle_get_item(const void * buf, int bufsize);
  virtual void handle_walk_request(const void * buf, int bufsize);
  virtual void handle_double_click(const void * buf, int bufsize);
  virtual void handle_client_equip_item(const void* buf,int bufsize);
public:
  CSpeedHack();
};
