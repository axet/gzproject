#include "stdafx.h"

#include "../speedhack/xsd/SchemaSpeedhack/SchemaSpeedhack.h"
#include "../speedhack/xsd/SchemaSpeedhackConfigFile/SchemaSpeedhackConfigFile.h"
#include "speedhack.h"
#include "../../client/packets.h"
#include "../firewall/firewall.h"
#include "../serveraddin.h"
#include <debugger/debug.h>

#include <atlbase.h>
#include <exception>

using namespace SpeedHack;

CSpeedHack::CSpeedHack():
  m_walk_lastwalkrequest(0),m_walk_lastwalkcount(0),m_walk_lastwalktotaltime(0),m_walk_lastdirection(-1),m_walk_speed(90),m_walk_count(20),
  m_item_manipulate_count_items(2),m_item_manipulate_time(1000),m_item_manipulate_count(0),m_item_manipulate_times(0),m_item_manipulate_last(0)
{
}

void CSpeedHack::handle_walk_request(const void * buf, int bufsize)
{
  Packets::scsWalkReq * walk=(Packets::scsWalkReq *)buf;

  int lastdirection=m_walk_lastdirection;
  m_walk_lastdirection=walk->direction;

  // нужно учитывать что поворот персонажа это не шаг.
  if(lastdirection!=walk->direction)
    return;

  unsigned current=GetTickCount();
  unsigned step=current-m_walk_lastwalkrequest;
  if(m_walk_lastwalkrequest!=0)
  {
    m_walk_lastwalkcount++;
    m_walk_lastwalktotaltime+=step;

    if(m_walk_lastwalkcount>=m_walk_count)
    {
      int speed=m_walk_lastwalktotaltime/m_walk_lastwalkcount;

      bool valid=speed>=m_walk_speed;

      DBGTRACE("speedhack, %s, walk %d, count %d, total %d, speed %d - '%s'\n",
        GetCurrentSession()->GetLogin(),
        step,m_walk_lastwalkcount,m_walk_lastwalktotaltime,speed,
        valid?"valid":"invalid");

      if(!valid)
      {
        GetCurrentSession()->Kick();
      }

      m_walk_lastwalkcount=0;
      m_walk_lastwalkrequest=0;
      m_walk_lastwalktotaltime=0;
    }
  }

  m_walk_lastwalkrequest=current;
}

void CSpeedHack::Command(std::string& xmlinput,std::string& xmloutput)
{
  CDocAddin<CSchemaSpeedHackDoc> doc;
  CPacketType packet=doc.LoadXML(xmlinput.c_str());
  if(packet.HasGetProperty())
  {
    CPacketType packet;
    CGetPropertyType property;
    CRespondType respond;

    respond.AddWalkCount(m_walk_count);
    respond.AddWalkSpeed(m_walk_speed);
    respond.AddCountItems(m_item_manipulate_count_items);

    property.AddRespond(respond);
    packet.AddGetProperty(property);
    doc.SetRootElementName("","Packet");
    xmloutput=doc.SaveXML(packet);
  }
  if(packet.HasSetProperty())
  {
    CSetPropertyType property=packet.GetSetProperty();

    m_walk_count=(int)property.GetWalkCount();
    m_walk_speed=(int)property.GetWalkSpeed();
    m_item_manipulate_count_items=(int)property.GetCountItems();
  }
}

std::string CSpeedHack::SaveConfig()
{
  CConfigType config;

  config.AddWalkCount(m_walk_count);
  config.AddWalkSpeed(m_walk_speed);
  config.AddCountItems(m_item_manipulate_count_items);

  CDocAddin<CSchemaSpeedHackConfigFileDoc> doc;
  doc.SetRootElementName("","Config");
  return doc.SaveXML(config).c_str();
}

void CSpeedHack::LoadConfig(const char* str)
{
  CDocAddin<CSchemaSpeedHackConfigFileDoc> doc;
  CConfigType config=doc.LoadXML(str);

  m_walk_count=(int)config.GetWalkCount();
  m_walk_speed=(int)config.GetWalkSpeed();
  m_item_manipulate_count_items=(int)config.GetCountItems();
}

void CSpeedHack::handle_double_click(const void * buf, int bufsize)
{
  if(BadItemManipulate())
  {
    Packets::scsSpeaking speak("Hi rate use item, reject");
    GetCurrentSession()->Send(speak);
    Packets::scsDenyMoveItem move;
    GetCurrentSession()->Send(move);
    throw CClientInterface::CReplacePacket();
  }
}

void CSpeedHack::handle_get_item(const void * buf, int bufsize)
{
  if(BadItemManipulate())
  {
    Packets::scsSpeaking speak("Hi rate get item, reject");
    GetCurrentSession()->Send(speak);
    Packets::scsDenyMoveItem move;
    GetCurrentSession()->Send(move);
    throw CClientInterface::CReplacePacket();
  }
}

void CSpeedHack::handle_client_equip_item(const void* buf,int bufsize)
{
  /*
  if(BadItemManipulate())
  {
    CServer::scsSpeaking speak("Hi rate equip item, reject");
    GetCurrentSession()->Send(speak);
    throw CClientInterface::CReplacePacket();
  }
  */
}

bool CSpeedHack::BadItemManipulate()
{
  unsigned dw=GetTickCount();

  unsigned delta=dw-m_item_manipulate_last;
  m_item_manipulate_times+=delta;
  m_item_manipulate_count++;

  m_item_manipulate_last=dw;

  double gamevalue=(double)m_item_manipulate_times/(double)m_item_manipulate_count;
  double configvalue=(double)m_item_manipulate_time/(double)m_item_manipulate_count_items;

  if(m_item_manipulate_count>m_item_manipulate_count_items)
  {
    // нельзя обнулять значения поскольку
    // при лимите в 2 предмета в секунду
    // возможно после использовнаия двух третий надеть моментально.
    m_item_manipulate_count=1;
    m_item_manipulate_times=delta;
  }

  if(gamevalue<configvalue)
    return true;

  return false;
}

/*
2E - Equip Item
06 - Req Obj Use
07 - Req Get Obj
08 - Req Drop Obj
13 - Req Obj Equip
1A - Move
25 - Obj to Obj Add Object to an Object
27 - Get Obj Failed
28 - Drop Obj Failed
2E - Equip Item
37 - Move Object
*/
