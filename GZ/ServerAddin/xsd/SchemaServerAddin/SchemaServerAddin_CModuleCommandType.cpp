////////////////////////////////////////////////////////////////////////
//
// SchemaServerAddin_CModuleCommandType.cpp
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "SchemaServerAddinBase.h"
#include "SchemaServerAddin_CModuleCommandType.h"
#include "SchemaServerAddin_CRequestType.h"
#include "SchemaServerAddin_CRespondType.h"



namespace ssa // URI: http://gzproject.sf.net/ssa
{
////////////////////////////////////////////////////////////////////////
//
// class CModuleCommandType
//
////////////////////////////////////////////////////////////////////////


CNode::EGroupType CModuleCommandType::GetGroupType()
{
	return eChoice;
}
int CModuleCommandType::GetRequestMinCount()
{
	return 1;
}


int CModuleCommandType::GetRequestMaxCount()
{
	return 1;
}


int CModuleCommandType::GetRequestCount()
{
	return ChildCountInternal(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Request"));
}


bool CModuleCommandType::HasRequest()
{
	return InternalHasChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Request"));
}


void CModuleCommandType::AddRequest(CRequestType& Request)
{
	InternalAppendNode(_T("http://gzproject.sf.net/ssa"), _T("ssa:Request"), Request);
}


void CModuleCommandType::InsertRequestAt(CRequestType& Request, int nIndex)
{
	InternalInsertNodeAt(_T("http://gzproject.sf.net/ssa"), _T("ssa:Request"), nIndex, Request);
}


void CModuleCommandType::ReplaceRequestAt(CRequestType& Request, int nIndex)
{
	InternalReplaceNodeAt(_T("http://gzproject.sf.net/ssa"), _T("ssa:Request"), nIndex, Request);
}



CRequestType CModuleCommandType::GetRequestAt(int nIndex)
{
	return CRequestType(*this, InternalGetAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Request"), nIndex));
}

MSXML2::IXMLDOMNodePtr CModuleCommandType::GetStartingRequestCursor()
{
	return InternalGetFirstChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Request"));
}

MSXML2::IXMLDOMNodePtr CModuleCommandType::GetAdvancedRequestCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Request"), pCurNode);
}

CRequestType CModuleCommandType::GetRequestValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return CRequestType( *this, pCurNode );
}



CRequestType CModuleCommandType::GetRequest()
{
	return GetRequestAt(0);
}


void CModuleCommandType::RemoveRequestAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Request"), nIndex);
}


void CModuleCommandType::RemoveRequest()
{
	while (HasRequest())
		RemoveRequestAt(0);
}

int CModuleCommandType::GetRespondMinCount()
{
	return 1;
}


int CModuleCommandType::GetRespondMaxCount()
{
	return 1;
}


int CModuleCommandType::GetRespondCount()
{
	return ChildCountInternal(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Respond"));
}


bool CModuleCommandType::HasRespond()
{
	return InternalHasChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Respond"));
}


void CModuleCommandType::AddRespond(CRespondType& Respond)
{
	InternalAppendNode(_T("http://gzproject.sf.net/ssa"), _T("ssa:Respond"), Respond);
}


void CModuleCommandType::InsertRespondAt(CRespondType& Respond, int nIndex)
{
	InternalInsertNodeAt(_T("http://gzproject.sf.net/ssa"), _T("ssa:Respond"), nIndex, Respond);
}


void CModuleCommandType::ReplaceRespondAt(CRespondType& Respond, int nIndex)
{
	InternalReplaceNodeAt(_T("http://gzproject.sf.net/ssa"), _T("ssa:Respond"), nIndex, Respond);
}



CRespondType CModuleCommandType::GetRespondAt(int nIndex)
{
	return CRespondType(*this, InternalGetAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Respond"), nIndex));
}

MSXML2::IXMLDOMNodePtr CModuleCommandType::GetStartingRespondCursor()
{
	return InternalGetFirstChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Respond"));
}

MSXML2::IXMLDOMNodePtr CModuleCommandType::GetAdvancedRespondCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Respond"), pCurNode);
}

CRespondType CModuleCommandType::GetRespondValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return CRespondType( *this, pCurNode );
}



CRespondType CModuleCommandType::GetRespond()
{
	return GetRespondAt(0);
}


void CModuleCommandType::RemoveRespondAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Respond"), nIndex);
}


void CModuleCommandType::RemoveRespond()
{
	while (HasRespond())
		RemoveRespondAt(0);
}

} // end of namespace ssa
