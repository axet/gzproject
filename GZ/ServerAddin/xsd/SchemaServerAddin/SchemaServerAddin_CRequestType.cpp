////////////////////////////////////////////////////////////////////////
//
// SchemaServerAddin_CRequestType.cpp
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "SchemaServerAddinBase.h"
#include "SchemaServerAddin_CRequestType.h"



namespace ssa // URI: http://gzproject.sf.net/ssa
{
////////////////////////////////////////////////////////////////////////
//
// class CRequestType
//
////////////////////////////////////////////////////////////////////////


CNode::EGroupType CRequestType::GetGroupType()
{
	return eSequence;
}
int CRequestType::GetNameMinCount()
{
	return 1;
}


int CRequestType::GetNameMaxCount()
{
	return 1;
}


int CRequestType::GetNameCount()
{
	return ChildCountInternal(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Name"));
}


bool CRequestType::HasName()
{
	return InternalHasChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Name"));
}


void CRequestType::AddName(CSchemaString Name)
{
	if( !Name.IsNull() )
		InternalAppend(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Name"), Name);
}


void CRequestType::InsertNameAt(CSchemaString Name, int nIndex)
{
	if( !Name.IsNull() )
		InternalInsertAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Name"), nIndex, Name);
}


void CRequestType::ReplaceNameAt(CSchemaString Name, int nIndex)
{
	InternalReplaceAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Name"), nIndex, Name);
}



CSchemaString CRequestType::GetNameAt(int nIndex)
{
	return (LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Name"), nIndex)->text;
	//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Name"), nIndex)->text);
}

MSXML2::IXMLDOMNodePtr CRequestType::GetStartingNameCursor()
{
	return InternalGetFirstChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Name"));
}

MSXML2::IXMLDOMNodePtr CRequestType::GetAdvancedNameCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Name"), pCurNode);
}

CSchemaString CRequestType::GetNameValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return (LPCTSTR)pCurNode->text;
}



CSchemaString CRequestType::GetName()
{
	return GetNameAt(0);
}


void CRequestType::RemoveNameAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Name"), nIndex);
}


void CRequestType::RemoveName()
{
	while (HasName())
		RemoveNameAt(0);
}

int CRequestType::GetDataMinCount()
{
	return 1;
}


int CRequestType::GetDataMaxCount()
{
	return 1;
}


int CRequestType::GetDataCount()
{
	return ChildCountInternal(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"));
}


bool CRequestType::HasData()
{
	return InternalHasChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"));
}


void CRequestType::AddData(CSchemaString Data)
{
	if( !Data.IsNull() )
		InternalAppend(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), Data);
}


void CRequestType::InsertDataAt(CSchemaString Data, int nIndex)
{
	if( !Data.IsNull() )
		InternalInsertAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), nIndex, Data);
}


void CRequestType::ReplaceDataAt(CSchemaString Data, int nIndex)
{
	InternalReplaceAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), nIndex, Data);
}



CSchemaString CRequestType::GetDataAt(int nIndex)
{
	return (LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), nIndex)->text;
	//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), nIndex)->text);
}

MSXML2::IXMLDOMNodePtr CRequestType::GetStartingDataCursor()
{
	return InternalGetFirstChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"));
}

MSXML2::IXMLDOMNodePtr CRequestType::GetAdvancedDataCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), pCurNode);
}

CSchemaString CRequestType::GetDataValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return (LPCTSTR)pCurNode->text;
}



CSchemaString CRequestType::GetData()
{
	return GetDataAt(0);
}


void CRequestType::RemoveDataAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), nIndex);
}


void CRequestType::RemoveData()
{
	while (HasData())
		RemoveDataAt(0);
}

} // end of namespace ssa
