////////////////////////////////////////////////////////////////////////
//
// SchemaServerAddin_CRespondType.cpp
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "SchemaServerAddinBase.h"
#include "SchemaServerAddin_CRespondType.h"



namespace ssa // URI: http://gzproject.sf.net/ssa
{
////////////////////////////////////////////////////////////////////////
//
// class CRespondType
//
////////////////////////////////////////////////////////////////////////


CNode::EGroupType CRespondType::GetGroupType()
{
	return eSequence;
}
int CRespondType::GetDataMinCount()
{
	return 1;
}


int CRespondType::GetDataMaxCount()
{
	return 1;
}


int CRespondType::GetDataCount()
{
	return ChildCountInternal(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"));
}


bool CRespondType::HasData()
{
	return InternalHasChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"));
}


void CRespondType::AddData(CSchemaString Data)
{
	if( !Data.IsNull() )
		InternalAppend(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), Data);
}


void CRespondType::InsertDataAt(CSchemaString Data, int nIndex)
{
	if( !Data.IsNull() )
		InternalInsertAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), nIndex, Data);
}


void CRespondType::ReplaceDataAt(CSchemaString Data, int nIndex)
{
	InternalReplaceAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), nIndex, Data);
}



CSchemaString CRespondType::GetDataAt(int nIndex)
{
	return (LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), nIndex)->text;
	//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), nIndex)->text);
}

MSXML2::IXMLDOMNodePtr CRespondType::GetStartingDataCursor()
{
	return InternalGetFirstChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"));
}

MSXML2::IXMLDOMNodePtr CRespondType::GetAdvancedDataCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), pCurNode);
}

CSchemaString CRespondType::GetDataValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return (LPCTSTR)pCurNode->text;
}



CSchemaString CRespondType::GetData()
{
	return GetDataAt(0);
}


void CRespondType::RemoveDataAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Data"), nIndex);
}


void CRespondType::RemoveData()
{
	while (HasData())
		RemoveDataAt(0);
}

} // end of namespace ssa
