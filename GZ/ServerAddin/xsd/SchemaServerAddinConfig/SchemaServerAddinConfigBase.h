////////////////////////////////////////////////////////////////////////
//
// SchemaServerAddinConfigBase.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef SchemaServerAddinConfigBase_H_INCLUDED
#define SchemaServerAddinConfigBase_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000


#ifndef _USRDLL
	#define SchemaServerAddinConfig_DECLSPECIFIER
#else
	#ifdef SchemaServerAddinConfig_EXPORTS
		#define SchemaServerAddinConfig_DECLSPECIFIER __declspec(dllexport)
	#else
		#define SchemaServerAddinConfig_DECLSPECIFIER __declspec(dllimport)
	#endif
#endif

// there's a #define small char somewhere in Microsoft's headers
#undef small

class CSchemaServerAddinConfigDoc;
/*
class CModuleType;
class CSchemaServerAddinConfigType;

*/
namespace ssa // URI: http://gzproject.sf.net/ssa
{
class CModuleType;
}
namespace ssa // URI: http://gzproject.sf.net/ssa
{
class CSchemaServerAddinConfigType;
}


////////////////////////////////////////////////////////////////////////

class SchemaServerAddinConfig_DECLSPECIFIER CSchemaServerAddinConfigDoc : public CDoc
{
protected:
	virtual void DeclareNamespaces(CNode& rNode);
};

#endif // SchemaServerAddinConfigBase_H_INCLUDED
