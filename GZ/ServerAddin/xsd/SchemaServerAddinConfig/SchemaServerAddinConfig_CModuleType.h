////////////////////////////////////////////////////////////////////////
//
// SchemaServerAddinConfig_CModuleType.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef SchemaServerAddinConfig_CModuleType_H_INCLUDED
#define SchemaServerAddinConfig_CModuleType_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000




namespace ssa // URI: http://gzproject.sf.net/ssa
{


class SchemaServerAddinConfig_DECLSPECIFIER CModuleType : public CNode
{
public:
	CModuleType() : CNode() {}
	CModuleType(CNode& rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
	CModuleType(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
	static EGroupType GetGroupType();

	//
	// string Name (1...1)
	//
	static int GetNameMinCount();
	static int GetNameMaxCount();
	int GetNameCount();
	bool HasName();
	void AddName(CSchemaString Name);
	void InsertNameAt(CSchemaString Name, int nIndex);
	void ReplaceNameAt(CSchemaString Name, int nIndex);
	CSchemaString GetNameAt(int nIndex);
	CSchemaString GetName();
	MSXML2::IXMLDOMNodePtr GetStartingNameCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedNameCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaString GetNameValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveNameAt(int nIndex);
	void RemoveName();

	//
	// string Data (1...1)
	//
	static int GetDataMinCount();
	static int GetDataMaxCount();
	int GetDataCount();
	bool HasData();
	void AddData(CSchemaString Data);
	void InsertDataAt(CSchemaString Data, int nIndex);
	void ReplaceDataAt(CSchemaString Data, int nIndex);
	CSchemaString GetDataAt(int nIndex);
	CSchemaString GetData();
	MSXML2::IXMLDOMNodePtr GetStartingDataCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedDataCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaString GetDataValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveDataAt(int nIndex);
	void RemoveData();
};


} // end of namespace ssa

#endif // SchemaServerAddinConfig_CModuleType_H_INCLUDED
