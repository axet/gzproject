////////////////////////////////////////////////////////////////////////
//
// SchemaServerConfig_CConfigType.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef SchemaServerConfig_CConfigType_H_INCLUDED
#define SchemaServerConfig_CConfigType_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000




namespace ssa // URI: http://gzproject.sf.net/ssa
{


class SchemaServerConfig_DECLSPECIFIER CConfigType : public CNode
{
public:
	CConfigType() : CNode() {}
	CConfigType(CNode& rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
	CConfigType(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
	static EGroupType GetGroupType();

	//
	// int Port (1...1)
	//
	static int GetPortMinCount();
	static int GetPortMaxCount();
	int GetPortCount();
	bool HasPort();
	void AddPort(CSchemaInt Port);
	void InsertPortAt(CSchemaInt Port, int nIndex);
	void ReplacePortAt(CSchemaInt Port, int nIndex);
	CSchemaInt GetPortAt(int nIndex);
	CSchemaInt GetPort();
	MSXML2::IXMLDOMNodePtr GetStartingPortCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedPortCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSchemaInt GetPortValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemovePortAt(int nIndex);
	void RemovePort();
};


} // end of namespace ssa

#endif // SchemaServerConfig_CConfigType_H_INCLUDED
