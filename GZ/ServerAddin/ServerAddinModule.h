#ifndef __SERVERADDINMODULE_H__
#define __SERVERADDINMODULE_H__

class CServerAddin;

#include <string>

class CServerAddinModule
{
  friend CServerAddin;

  CServerAddin* m_sa;

protected:
  inline CServerAddin* GetServerAddin()
  {
    return m_sa;
  }

public:
  virtual ~CServerAddinModule(){};
  virtual const char* GetName() {return 0;}
  virtual void Command(std::string& xmlinput,std::string& xmloutput) {}
  virtual std::string SaveConfig() {return std::string();}
  virtual void LoadConfig(const char*) {}
};

#endif
