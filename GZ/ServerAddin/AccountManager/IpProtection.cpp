#include "ipprotection.h"

#include <Altova/AltovaLib.h>
#include "xsd/SchemaAccountManager/SchemaAccountManager.h"
#include "accountmanager.h"
#include "../../client/packets.h"
#include "../firewall/client.h"
#include <debugger/debug.h>

#include <exception>
#include <fstream>

CIpProtection::CIpProtection(CAccountManager* accountmanager):
  m_accountmanager(*accountmanager)
{
}

void CIpProtection::CheckLogin(const char* name)
{
  bool valid=m_accountmanager.CheckValidIp(name,GetCurrentSession()->GetIPAddress());
  if(!valid)
    GetCurrentSession()->Kick();

  DBGTRACE("login check %s, %08x - %s\n",name,
    GetCurrentSession()->GetIPAddress(),valid?"valid":"invalid");
}

void CIpProtection::handle_first_login(const void * buf, int bufsize)
{
  Packets::scsLogin * login=(Packets::scsLogin*)buf;
  CheckLogin(login->name);
}

void CIpProtection::handle_second_login(const void * buf, int bufsize)
{
  Packets::scsPostLogin * login=(Packets::scsPostLogin*)buf;
  CheckLogin(login->name);
}

/*
53 - Login Reject
*/