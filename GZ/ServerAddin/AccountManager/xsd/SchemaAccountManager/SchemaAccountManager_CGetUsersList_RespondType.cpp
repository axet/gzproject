////////////////////////////////////////////////////////////////////////
//
// SchemaAccountManager_CGetUsersList_RespondType.cpp
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "SchemaAccountManagerBase.h"
#include "SchemaAccountManager_CGetUsersList_RespondType.h"
#include "SchemaAccountManager_CItemsType.h"



namespace AccountManager // URI: http://www.sourceforge.net/projects/gzproject
{
////////////////////////////////////////////////////////////////////////
//
// class CGetUsersList_RespondType
//
////////////////////////////////////////////////////////////////////////


CNode::EGroupType CGetUsersList_RespondType::GetGroupType()
{
	return eSequence;
}
int CGetUsersList_RespondType::GetItemsMinCount()
{
	return 0;
}


int CGetUsersList_RespondType::GetItemsMaxCount()
{
	return 1;
}


int CGetUsersList_RespondType::GetItemsCount()
{
	return ChildCountInternal(Element, _T("http://www.sourceforge.net/projects/gzproject"), _T("AccountManager:Items"));
}


bool CGetUsersList_RespondType::HasItems()
{
	return InternalHasChild(Element, _T("http://www.sourceforge.net/projects/gzproject"), _T("AccountManager:Items"));
}


void CGetUsersList_RespondType::AddItems(CItemsType& Items)
{
	InternalAppendNode(_T("http://www.sourceforge.net/projects/gzproject"), _T("AccountManager:Items"), Items);
}


void CGetUsersList_RespondType::InsertItemsAt(CItemsType& Items, int nIndex)
{
	InternalInsertNodeAt(_T("http://www.sourceforge.net/projects/gzproject"), _T("AccountManager:Items"), nIndex, Items);
}


void CGetUsersList_RespondType::ReplaceItemsAt(CItemsType& Items, int nIndex)
{
	InternalReplaceNodeAt(_T("http://www.sourceforge.net/projects/gzproject"), _T("AccountManager:Items"), nIndex, Items);
}



CItemsType CGetUsersList_RespondType::GetItemsAt(int nIndex)
{
	return CItemsType(*this, InternalGetAt(Element, _T("http://www.sourceforge.net/projects/gzproject"), _T("AccountManager:Items"), nIndex));
}

MSXML2::IXMLDOMNodePtr CGetUsersList_RespondType::GetStartingItemsCursor()
{
	return InternalGetFirstChild(Element, _T("http://www.sourceforge.net/projects/gzproject"), _T("AccountManager:Items"));
}

MSXML2::IXMLDOMNodePtr CGetUsersList_RespondType::GetAdvancedItemsCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	return InternalGetNextChild(Element, _T("http://www.sourceforge.net/projects/gzproject"), _T("AccountManager:Items"), pCurNode);
}

CItemsType CGetUsersList_RespondType::GetItemsValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode)
{
	if( pCurNode == NULL )
		throw CXmlException(CXmlException::eError1, _T("Index out of range"));
	else

		return CItemsType( *this, pCurNode );
}



CItemsType CGetUsersList_RespondType::GetItems()
{
	return GetItemsAt(0);
}


void CGetUsersList_RespondType::RemoveItemsAt(int nIndex)
{
	InternalRemoveAt(Element, _T("http://www.sourceforge.net/projects/gzproject"), _T("AccountManager:Items"), nIndex);
}


void CGetUsersList_RespondType::RemoveItems()
{
	while (HasItems())
		RemoveItemsAt(0);
}

} // end of namespace AccountManager
