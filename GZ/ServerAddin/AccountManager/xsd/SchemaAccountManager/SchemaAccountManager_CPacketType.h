////////////////////////////////////////////////////////////////////////
//
// SchemaAccountManager_CPacketType.h
//
// This file was generated by XMLSpy 2006 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////


#ifndef SchemaAccountManager_CPacketType_H_INCLUDED
#define SchemaAccountManager_CPacketType_H_INCLUDED

#if _MSC_VER > 1000
	#pragma once
#endif // _MSC_VER > 1000




namespace AccountManager // URI: http://www.sourceforge.net/projects/gzproject
{


class SchemaAccountManager_DECLSPECIFIER CPacketType : public CNode
{
public:
	CPacketType() : CNode() {}
	CPacketType(CNode& rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
	CPacketType(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
	static EGroupType GetGroupType();

	//
	// GetUsersList_Request GetUsersList_Request reference (1...1)
	//
	static int GetGetUsersList_RequestMinCount();
	static int GetGetUsersList_RequestMaxCount();
	int GetGetUsersList_RequestCount();
	bool HasGetUsersList_Request();
	void AddGetUsersList_Request(CGetUsersList_RequestType& GetUsersList_Request);
	void InsertGetUsersList_RequestAt(CGetUsersList_RequestType& GetUsersList_Request, int nIndex);
	void ReplaceGetUsersList_RequestAt(CGetUsersList_RequestType& GetUsersList_Request, int nIndex);
	CGetUsersList_RequestType GetGetUsersList_RequestAt(int nIndex);
	CGetUsersList_RequestType GetGetUsersList_Request();
	MSXML2::IXMLDOMNodePtr GetStartingGetUsersList_RequestCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedGetUsersList_RequestCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CGetUsersList_RequestType GetGetUsersList_RequestValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveGetUsersList_RequestAt(int nIndex);
	void RemoveGetUsersList_Request();

	//
	// GetUsersList_Respond GetUsersList_Respond reference (1...1)
	//
	static int GetGetUsersList_RespondMinCount();
	static int GetGetUsersList_RespondMaxCount();
	int GetGetUsersList_RespondCount();
	bool HasGetUsersList_Respond();
	void AddGetUsersList_Respond(CGetUsersList_RespondType& GetUsersList_Respond);
	void InsertGetUsersList_RespondAt(CGetUsersList_RespondType& GetUsersList_Respond, int nIndex);
	void ReplaceGetUsersList_RespondAt(CGetUsersList_RespondType& GetUsersList_Respond, int nIndex);
	CGetUsersList_RespondType GetGetUsersList_RespondAt(int nIndex);
	CGetUsersList_RespondType GetGetUsersList_Respond();
	MSXML2::IXMLDOMNodePtr GetStartingGetUsersList_RespondCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedGetUsersList_RespondCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CGetUsersList_RespondType GetGetUsersList_RespondValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveGetUsersList_RespondAt(int nIndex);
	void RemoveGetUsersList_Respond();

	//
	// SetUsersList SetUsersList reference (1...1)
	//
	static int GetSetUsersListMinCount();
	static int GetSetUsersListMaxCount();
	int GetSetUsersListCount();
	bool HasSetUsersList();
	void AddSetUsersList(CSetUsersListType& SetUsersList);
	void InsertSetUsersListAt(CSetUsersListType& SetUsersList, int nIndex);
	void ReplaceSetUsersListAt(CSetUsersListType& SetUsersList, int nIndex);
	CSetUsersListType GetSetUsersListAt(int nIndex);
	CSetUsersListType GetSetUsersList();
	MSXML2::IXMLDOMNodePtr GetStartingSetUsersListCursor();
	MSXML2::IXMLDOMNodePtr GetAdvancedSetUsersListCursor(MSXML2::IXMLDOMNodePtr pCurNode);
	CSetUsersListType GetSetUsersListValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);

	void RemoveSetUsersListAt(int nIndex);
	void RemoveSetUsersList();
};


} // end of namespace AccountManager

#endif // SchemaAccountManager_CPacketType_H_INCLUDED
