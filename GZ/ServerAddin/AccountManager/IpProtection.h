#ifndef __IPPROTECTION_H__
#define __IPPROTECTION_H__

class CAccountManager;

#include <string>
#include <set>
#include <map>

#include "../../client/clientnetevents.h"
#include "../firewall/firewallmodule.h"
#include "../serveraddinmodule.h"

class CIpProtection:public CFirewallModule,public CServerAddinModule
{
  friend CAccountManager;

  CAccountManager& m_accountmanager;

  void CheckLogin(const char*name);

  virtual void handle_first_login(const void * buf, int bufsize);
  virtual void handle_second_login(const void * buf, int bufsize);
public:
  CIpProtection(CAccountManager* accountmanager);
};

#endif
