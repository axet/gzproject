class CServerAddin;

#include <string>
#include <set>
#include <map>

#include <Altova/AltovaLib.h>
#include "xsd/SchemaAccountManager/SchemaAccountManager.h"

#include "../../client/clientnetevents.h"
#include "../firewall/firewallmodule.h"
#include "../serveraddinmodule.h"
#include "privileges.h"
#include "ipprotection.h"

class CAccountManager:public CServerAddinModule
{
  friend CIpProtection;
  friend CPrivileges;

  typedef struct account_tag
  {
    // то что аккаунт существует в файле аккаунтов
    bool available;
    std::string groupname;
    account_tag *group;
    bool isgroup;

    typedef struct ipprotection_tag
    {
      typedef std::set<unsigned> iplist_t;

      bool enable;
      iplist_t iplist;

      ipprotection_tag(bool b=false):enable(b) {}
    }ipprotection_t;
    ipprotection_t ipprotection;

    typedef struct privileges_tag
    {
      typedef std::set<std::string> strlist_t;

      bool disableprotection;
      strlist_t commandslist;

      privileges_tag():disableprotection(false) {}
    }privileges_t;
    
    privileges_t privileges;

    account_tag(bool _available=false):
      available(_available),isgroup(false),group(0) {}
  }account_t;
  typedef std::map<std::string,account_t> accountlist_t;
  accountlist_t m_accountlist;
  std::string m_spherepath;

  const char* GetName()
  {
    return "AccountManager";
  }
  virtual void Command(std::string& xmlinput,std::string& xmloutput);
  virtual std::string SaveConfig();
  virtual void LoadConfig(const char* xml);

  AccountManager::CItemsType SaveItems();
  void LoadItems(AccountManager::CItemsType&);

  bool CheckValidIpForList(unsigned ip,account_t &i);
  bool CheckValidIp(const char* account,unsigned ip);
  bool IsValidCommandForList(const char* command,account_t &i);
  bool IsValidCommand(const char* account,const char* command);

public:
  CAccountManager();
  void AddModule();
};

class CAccountManagerModule
{
  friend CAccountManager;

  CAccountManager* m_am;
public:
};
