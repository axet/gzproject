#include "privileges.h"

#include "accountmanager.h"
#include "../../client/packets.h"
#include "../firewall/client.h"
#include <debugger/debug.h>

CPrivileges::CPrivileges(CAccountManager* accountmanager):m_accountmanager(*accountmanager)
{
  ;
}

void CPrivileges::CheckCommand(const char* text)
{
  if(text[0]=='.')
  {
    const char *front=&text[1];
    const char* end=strpbrk(front," .");
    if(end==0)
      end=strchr(text,0);
    std::string str(front,end);

    bool valid=m_accountmanager.IsValidCommand(GetCurrentSession()->GetLogin(),str.c_str());

    DBGTRACE("check command %s, '%s' = %s\n",GetCurrentSession()->GetLogin(),
      str.c_str(),valid?"valid":"invalid");

    if(!valid)
    {
      Packets::scsSpeaking speak("Command not valid for this account");
      GetCurrentSession()->Send(speak);
      throw CClientInterface::CReplacePacket();
    }
  }
}

void CPrivileges::handle_unicode_client_talk(const void * buf, int bufsize)
{
  Packets::scsSpeakingUnicode su((unsigned char*)buf,bufsize);
  std::string str=(const char*)su.text;
  CheckCommand(str.c_str());
}

void CPrivileges::handle_client_talk(const void * buf, int bufsize)
{
  Packets::scsClientTalk talk((unsigned char*)buf,bufsize);
  CheckCommand(talk.text.c_str());
}
