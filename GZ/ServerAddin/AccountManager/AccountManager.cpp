#include "accountmanager.h"

#include <atlbase.h>
#include <exception>
#include <fstream>
#include <locale>
#include <algorithm>

#include "../../client/packets.h"
#include "../firewall/client.h"
#include <iniext/path.h>
#include <debugger/debug.h>

using namespace AccountManager;

CAccountManager::CAccountManager()
{
  m_spherepath=IniExt::GetModuleRunDir(0);

  std::string accounts;
  {
    char buf[1024];
    GetPrivateProfileString("SPHERE","ACCTFILES",0,buf,sizeof(buf),(m_spherepath+"\\sphere.ini").c_str());
    accounts=buf;
  }

  // читаю список аккаунтов на память.
  {
    m_accountlist.clear();

    account_tag & account=m_accountlist[""];
    account.isgroup=true;

    std::ifstream input((accounts+"\\sphereaccu.scp").c_str());
    if(input.is_open())
    {
      char buf[1024];
      while(input.getline(buf,sizeof(buf)).gcount())
      {
        if(buf[0]=='[')
        {
          std::string name(strchr(buf,'[')+1,strchr(buf,']'));
          std::transform(name.begin(),name.end(),name.begin(), tolower);
          account_tag & account=m_accountlist[name.c_str()];
          account.available=true;
        }
      }
    }
  }
}

void CAccountManager::Command(std::string& xmlinput,std::string& xmloutput)
{
  CDocAddin<CSchemaAccountManagerDoc> doc;
  CPacketType packet=doc.LoadXML(xmlinput.c_str());
  if(packet.HasGetUsersList_Request())
  {
    CPacketType packet;
    CGetUsersList_RespondType respond;
    CItemsType items=SaveItems();
    respond.AddItems(items);
    packet.AddGetUsersList_Respond(respond);
    doc.SetRootElementName("","Packet");
    xmloutput=doc.SaveXML(packet);
  }
  if(packet.HasSetUsersList())
  {
    CSetUsersListType request=packet.GetSetUsersList();
    m_accountlist.clear();
    LoadItems(request.GetItems());
  }
}

CItemsType CAccountManager::SaveItems()
{
  CItemsType items;
  for(accountlist_t::iterator i=m_accountlist.begin();i!=m_accountlist.end();i++)
  {
    //ATLTRACE("%s\n",i->first.c_str());

    CItemType item;
    item.AddName(i->first.c_str());
    if(i->second.isgroup)
      item.AddIsGroup(tstring());
    item.AddGroup(i->second.groupname.c_str());
    {
      CIpProtectionType ipprotection;
      ipprotection.AddEnableIpProtection(i->second.ipprotection.enable);
      for(account_t::ipprotection_t::iplist_t::iterator ii=i->second.ipprotection.iplist.begin();
        ii!=i->second.ipprotection.iplist.end();ii++)
      {
        ipprotection.AddIpAddress(*ii);
      }
      item.AddIpProtection(ipprotection);
    }
    {
      CPrivilegesType privileges;
      if(i->second.privileges.disableprotection)
        privileges.AddDisableProtection(tstring());
      for(account_t::privileges_t::strlist_t::iterator ii=i->second.privileges.commandslist.begin();
        ii!=i->second.privileges.commandslist.end();
        ii++)
      {
        privileges.AddValidCommands(ii->c_str());
      }
      item.AddPrivileges(privileges);
    }
    items.AddItem(item);
  }
  return items;
}

std::string CAccountManager::SaveConfig()
{
  CDocAddin<CSchemaAccountManagerDoc> doc;
  CConfigType config;
  CItemsType items=SaveItems();
  config.AddItems(items);
  doc.SetRootElementName("","Config");
  return (tstring)doc.SaveXML(config);
}

void CAccountManager::LoadConfig(const char* str)
{
  CDocAddin<CSchemaAccountManagerDoc> doc;
  CConfigType config=doc.LoadXML(str);

  CItemsType items=config.GetItems();
  LoadItems(items);
}

void CAccountManager::LoadItems(CItemsType& items)
{
  for(int i=items.GetItemCount()-1;i>=0;i--)
  {
    CItemType &item=items.GetItemAt(i);
    tstring accountname=item.GetName();

    if(!item.HasIsGroup())
      ;//accountname.MakeLower();
    account_tag & account=m_accountlist[accountname.c_str()];
    account.groupname=(const char*)((tstring)item.GetGroup()).c_str();
    CIpProtectionType &ipprotection=item.GetIpProtection();
    account.ipprotection.enable=ipprotection.GetEnableIpProtection();
    for(int i=ipprotection.GetIpAddressCount()-1;i>=0;i--)
    {
      account.ipprotection.iplist.insert(account.ipprotection.iplist.end(),(unsigned)ipprotection.GetIpAddressAt(i));
    }
    account.isgroup=item.HasIsGroup();
    CPrivilegesType &privileges=item.GetPrivileges();
    account.privileges.disableprotection=privileges.HasDisableProtection();
    for(int i=privileges.GetValidCommandsCount()-1;i>=0;i--)
    {
      account.privileges.commandslist.insert(account.privileges.commandslist.end(),(const char*)((tstring)privileges.GetValidCommandsAt(i)).c_str());
    }
  }
}

bool CAccountManager::CheckValidIpForList(unsigned ip,account_t& i)
{
  if(i.ipprotection.enable)
  {// требуеться следить с валидного ли адрса происходит подключение
    if(i.ipprotection.iplist.find(ip)==i.ipprotection.iplist.end())
    {
      return false;
    }
  }
  return true;
}

bool  CAccountManager::CheckValidIp(const char* name,unsigned ip)
{
  CAccountManager::accountlist_t::iterator i=m_accountlist.find(name);
  if(i!=m_accountlist.end())
  {
    if(!CheckValidIpForList(ip,i->second))
      return false;
    if(i->second.group==0)
      i->second.group=&m_accountlist.find(i->second.groupname.c_str())->second;
    return CheckValidIpForList(ip,*i->second.group);
  }
  return true;
}

bool CAccountManager::IsValidCommandForList(const char* command,account_t &i)
{
  if(i.privileges.disableprotection)
    return true;

  if(i.privileges.commandslist.find(command)==i.privileges.commandslist.end())
  {
    return false;
  }
  return true;
}

bool CAccountManager::IsValidCommand(const char* account,const char* command)
{
  CAccountManager::accountlist_t::iterator i=m_accountlist.find(account);
  if(i!=m_accountlist.end())
  {
    if(IsValidCommandForList(command,i->second))
      return true;
    if(i->second.group==0)
      i->second.group=&m_accountlist.find(i->second.groupname.c_str())->second;
    if(IsValidCommandForList(command,*i->second.group))
      return true;
  }
  return false;
}
