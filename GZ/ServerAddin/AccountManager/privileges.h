#ifndef __PRIVILEGES_H__
#define __PRIVILEGES_H__

class CAccountManager;

#include <string>
#include <set>
#include <map>

#include "../../client/clientnetevents.h"
#include "../firewall/firewallmodule.h"
#include "../serveraddinmodule.h"

class CPrivileges:public CFirewallModule,public CServerAddinModule
{
  friend CAccountManager;

  CAccountManager& m_accountmanager;

  void CheckCommand(const char*);

  virtual void handle_unicode_client_talk(const void * buf, int bufsize);
  virtual void handle_client_talk(const void * buf, int bufsize);
public:
  CPrivileges(CAccountManager* accountmanager);
};

#endif
