#include "../serveraddinio/internetsaslave.h"
#include <fluke/flukeslave.h>
#include "firewall/firewall.h"
#include "serveraddinmodule.h"

class CServerAddin:public virtual CInterNetSASlave
{
  class CModuleList:public std::map<std::string,CServerAddinModule*> {};
  typedef std::set<CServerAddinModule*> ModuleList;

  // если m_module!=0 значит инициализация была, и можно в деструкторе
  // сохранить конфиг
  HMODULE m_module;
  CFirewall* m_firewall;
  CModuleList m_modulelistname;
  ModuleList m_modulelistall;
  uintptr_t m_runthread;

  const char* GetConfigName();

  void AddModule(CServerAddinModule*);
  void LoadConfigs();
  void SaveConfigs();

  // CFlukeSlave
private:
  static void Run(CServerAddin*);

  // CInterNetSASlave
private:
  void Unload();
  void ModuleCommand(std::string &name,std::string &xmlinput,std::string &xmloutput);

public:
  CServerAddin();
  ~CServerAddin();
  void InitInstance(HMODULE);
};

extern CServerAddin *g_serveraddin;