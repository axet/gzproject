#include "../../client/clientnetevents.h"
#include "../firewall/firewallmodule.h"
#include "../serveraddinmodule.h"

class CHearAll:public CFirewallModule,CServerAddinModule
{
  void talk(const char*);

  virtual void handle_client_talk(const void * buf, int bufsize);
  virtual void handle_unicode_client_talk(const void * buf, int bufsize);
public:
  CHearAll();
};
