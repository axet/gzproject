//#include <comdef.h>
//#include <xutility>

#include "hearall.h"

#include <winsock2.h>

#include "../../client/packets.h"
#include "../firewall/firewallmodule.h"
#include "../firewall/firewall.h"
#include <comdef.h>


CHearAll::CHearAll()
{
}

void CHearAll::talk(const char* t)
{
  char buf[4096];
  //strncpy(buf,GetFirewall()->GetLogin(),sizeof(buf));
  strncat(buf,"/",sizeof(buf));
  //strncat(buf,GetFirewall()->GetCharName(),sizeof(buf));
  strncat(buf," ",sizeof(buf));
  strncat(buf,t,sizeof(buf));
  //m_p->Message("hearall",CFirewallModule::msgInformation,buf);
}

void CHearAll::handle_client_talk(const void * buf, int bufsize)
{
  talk((char*)buf+8);
}

void CHearAll::handle_unicode_client_talk(const void * buf, int bufsize)
{
  /*
  char tempbuf[4096];
  memcpy(tempbuf,buf,min(bufsize,sizeof(tempbuf)));

  CServer::scsSpeakingUnicode* p=(CServer::scsSpeakingUnicode*)tempbuf;
  wchar_t* ptr=(wchar_t*)p->Text;
  while(*ptr!=0)
  {
    std::swap(*(char*)ptr,*((char*)ptr+1));
    ptr++;
  }
  talk(_bstr_t((wchar_t*)p->Text));
  */
}
