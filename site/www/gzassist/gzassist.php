<?php 
define('GZ_ROOT_PATH', './..'); 

include(GZ_ROOT_PATH.'/common.php');
include(GZ_ROOT_PATH.'/header.php');
?>
<div id="content">
<?php include(GZ_ROOT_PATH.'/nav.php');?>
<div class="page">
  <h1> GZAssist </h1>
  <h2>
    <a name="12">Для чего предназначена программа?</a>
  </h2>
  <p>
    <ul>
      <li>Программа позволяет прятать игровое окно по горячим клавишам.</li>
      <li>Писать лог.</li>
      <li>Восстанавливать соединение при падении клиента.</li>
    </ul>
  </p>

  <h1>
    <a name="13">Скачать</a>
  </h1>

  <p>
    Описание пользовательского интерфейса воспроизведено для GZone 1.0.0, GZAssist 1.0.0.
  </p>

  <h2>Release 2.0.1 (Версия от 14.07.2006)</h2>
  <ul>
    <li>Добавлена библиотека MFC80 в инсталятор.</li>
  </ul>
  <p>
    Доступен для скачивания, стабильная версия! <a href="http://prdownloads.sourceforge.net/gzproject/GZAssist-2.0.1.msi?download">GZAssist 2.0.1 Скачать.rar 4,23 MB</a>
  </p>

  <h1>Архив релизов</h1>

  <h2>Release 2.0.0-final (Версия от 28.06.2006)</h2>
  <p>
    Доступен для скачивания, стабильная версия! <a href="http://prdownloads.sourceforge.net/gzproject/GZAssist-2.0.0.msi?download">GZAssist 2.0.0 Final Скачать.rar 355 КБ (364 032 байт)</a>
  </p>

  <h2>Release 2.0.0-rc1 (Версия от 25.06.2006)</h2>
  <p>
    Доступен для скачивания Релиз Кандидат №1, тестовая стабильная версия! <a href="http://prdownloads.sourceforge.net/gzproject/GZAssist-2.0.0-rc1.msi?download">GZAssist 2.0.0 Release Candidate1 Скачать.rar 357 Кб (365 568 байт)</a>
  </p>

  <h2>Release 1.0.0 (Версия от 08.12.2002)</h2>
  <p>
    Старая версия релиза, доступна для работы со старыми модулями и старыми версиями макросов. <a href="http://prdownloads.sourceforge.net/gzproject/GZ-1.0.0.rar?download">GZone 1.0.0 &amp; GZAssist 1.0.0 Скачать.rar 173кб</a>
  </p>

</div>
</div>
<?php
include(GZ_ROOT_PATH.'/footer.php');
?>
