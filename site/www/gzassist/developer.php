<?php 
define('GZ_ROOT_PATH', './..'); 

include(GZ_ROOT_PATH.'/common.php');
include(GZ_ROOT_PATH.'/header.php');
?>
<div id="content">
<?php include(GZ_ROOT_PATH.'/nav.php');?>
<div class='page'>
  <h1>
    <a name="9">GZAssist</a>
  </h1>

  <h2>Документация для разработчиков</h2>

  <p>

    В документации описаны основные принципы используемые при реализации программного продукта.
    Среди которых:
    <ul>
      <li>основные правила ведения проекта</li>
      <li>по управления подчиненным процессом</li>
      <li>связ с внешних процессом посредством Pipe</li>
      <li>объеденение двух сред C++ и виртуальной машины Java</li>
      <li>способы подключения расшерений проекта и новых виртуальных сред</li>
      <li>мульти программность</li>
      <li>
        <?php wiki_link('HOWTO-Start-Develop'); ?> необходимое программное обеспечение
      </li>
    </ul>

  </p>

  <h2>Объеденение C++ и виртуальной машины Java</h2>

  <p>
    Объеденение предпологает полный контроль подчиненного процесса, через запуск
    виртуальной машины и ява кода на стороне внешнего приложения. Это
    осуществляеться через JNI переходники и перехват вызова внутренних фукнций
    приложения. В статьях рассмотрены способы реализации универсальных переходников
    для приложения, а так же приведен работающий пример, который успешно реализует
    данную фукнцию.
  </p>

  <p>
    Статья которая описывает основные принцыпы данного подхода <?php wiki_link('GZone-Goals'); ?>
  </p>

  <p>
    Более детальное описание перехвата фукнций с примерами <?php wiki_link('HOWTO-Fluke'); ?>
  </p>

  <p>
    Описание встраивание виртуальной машины и переадресацию вызова события из одной среды в другую
    <?php wiki_link('HOWTO-JavaNativeRuntime'); ?>
  </p>

  <h2>SVN: SOURCE</h2>
  <h3>Development - Последняя версия исходных кодов</h3>
  <p>
    Код этого проекта являеться открытым и доступен для скачивания.
    Для того чтобы просмотреть и скачать исходный код достаточно
    зайти по адресу
    <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/trunk/GZ/GZAssist">GZAssist</a>.
    Для успешной компиляции проекта необходимо получить на локальный диск содержимое двух папок с рабочими
    версиями исходных кодов:
    <ul>
      <li>
        <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/trunk/GZ">http://svn.sourceforge.net/viewvc/gzproject/trunk/GZ</a>
      </li>
      <li>
        <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/trunk/library">http://svn.sourceforge.net/viewvc/gzproject/trunk/library</a>
      </li>
    </ul>
  </p>
  <h3>2.0.1 - версия исходных кодов</h3>
  <p>
    Код этого проекта так же являеться открытым и доступен для скачивания.
    Для того чтобы просмотреть и скачать исходный код достаточно
    зайти по адресу <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0/">http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0</a>.
    Для успешной компиляции проекта необходимо получить на локальный диск содержимое двух папок с рабочими
    версиями исходных кодов:
  </p>
  <ul>
    <li>
      <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0/GZ">http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0/GZ</a>
    </li>
    <li>
      <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0/library">http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0/library</a>
    </li>
  </ul>
  <p>
    После чего наложить пач <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/branches/GZone-2.0.0-2.0.1">http://svn.sourceforge.net/viewvc/gzproject/branches/GZone-2.0.0-2.0.1</a>.
    Он необходим для сборки без msxml4, и обновления MSI инсталятора, но так же может быть использован для сборки проекта
    релиза 2.0.1.
  </p>
  <p>
    Исходный код по адресу: <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.1/">GZAssist-2.0.1</a>.
    Являеться кодом релиза 2.0.1, без правки MSI инсталятора.
    <ul>
      <li>
        <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.1/GZ">http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.1/GZ</a>
      </li>
      <li>
        <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.1/library">http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.1/library</a>
      </li>
    </ul>
  </p>
  <h3>2.0.0 - версия исходных кодов</h3>
  <p>
    Код этого проекта так же являеться открытым и доступен для скачивания.
    Для того чтобы просмотреть и скачать исходный код достаточно
    зайти по адресу <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0/">http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0</a>.
    Для успешной компиляции проекта необходимо получить на локальный диск содержимое двух папок с рабочими
    версиями исходных кодов:
    <ul>
      <li>
        <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0/GZ">http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0/GZ</a>
      </li>
      <li>
        <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0/library">http://svn.sourceforge.net/viewvc/gzproject/tags/GZAssist-2.0.0/library</a>
      </li>
    </ul>
  </p>
  <h3>Предыдущие версии исходных кодов</h3>
  <p>
    Все предыдущие версии проекта так же являються доступными для скачивания и находяться в SVN папке
    <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags">http://svn.sourceforge.net/viewvc/gzproject/tags</a>
  </p>

</div>
</div>
<?php
include(GZ_ROOT_PATH.'/footer.php');
?>