<?php 
define('GZ_ROOT_PATH', './..'); 

include(GZ_ROOT_PATH.'/common.php');
include(GZ_ROOT_PATH.'/header.php');
?>
<div id="content">
<?php include(GZ_ROOT_PATH.'/nav.php');?>
<div class='page'>
  <h1>
    <a name="9">GZone API</a>
  </h1>

  <h2>Документация для разработчиков</h2>

  <p>

    В документации описаны основные принципы используемые при реализации программного продукта.
    Среди которых:

  </p>

  <ul>
    <li>основные правила ведения проекта</li>
    <li>по управления подчиненным процессом</li>
    <li>связ с внешних процессом посредством Pipe</li>
    <li>объеденение двух сред C++ и виртуальной машины Java</li>
    <li>способы подключения расшерений проекта и новых виртуальных сред</li>
    <li>мульти программность</li>
    <li>
      <?php wiki_link('HOWTO-Start-Develop'); ?> необходимое программное обеспечение</a>
    </li>
  </ul>

  <h2>Объеденение C++ и виртуальной машины Java</h2>

  <p>
    Объеденение предпологает полный контроль подчиненного процесса, через запуск
    виртуальной машины и ява кода на стороне внешнего приложения. Это
    осуществляеться через JNI переходники и перехват вызова внутренних фукнций
    приложения. В статьях рассмотрены способы реализации универсальных переходников
    для приложения, а так же приведен работающий пример, который успешно реализует
    данную фукнцию.
  </p>

  <p>
    Статья которая описывает основные принцыпы данного подхода <?php wiki_link('GZone-Goals'); ?>
  </p>

  <p>
    Более детальное описание перехвата фукнций с примерами <?php wiki_link('HOWTO-Fluke'); ?>
  </p>

  <p>
    Описание встраивание виртуальной машины и переадресацию вызова события из одной среды в другую
    <?php wiki_link('HOWTO-JavaNativeRuntime'); ?>
  </p>

  <p>
    Подключение Eclipse 3.1 к проекту: <?php wiki_link('GZone-JavaModules-Debug'); ?>
  </p>

  <h2>SVN: SOURCE</h2>
  <h3>Development - Последняя версия исходных кодов</h3>

  <p>
    Код этого проекта являеться открытым и доступен для скачивания.
    Для того чтобы просмотреть и скачать исходный код достаточно
    зайти по адресу <a target='_blank' href='http://gzproject.git.sourceforge.net/git/gitweb.cgi?p=gzproject/gzproject;a=tree;f=GZ/GZone'>GZ/GZone</a>.
    Для успешной компиляции проекта необходимо получить на локальный диск содержимое двух папок с рабочими
    версиями исходных кодов:
  </p>

  <ul>
    <li>
      <a target='_blank' href='http://gzproject.git.sourceforge.net/git/gitweb.cgi?p=gzproject/gzproject;a=tree;f=GZ'>GZ</a>
    </li>
    <li>
      <a target='_blank' href='http://gzproject.git.sourceforge.net/git/gitweb.cgi?p=gzproject/gzproject;a=tree;f=library'>library</a>
    </li>
  </ul>

  <h2>Написание расширений</h2>

  <p>
    Класс управления командиром должен наследоваться от CCommanderIQ. Он будет
    получать команды, а так же иметь возможность отсылать события в модуль
    программы. Для управления одним персонажем рекомендую наследовать командира еще
    и от CClientEvents.
  </p>

  <p>
    Описания классов с.м. в хедерах. <a href="../doxygen/html/index.php">Doxygen</a>.
    Пример командира на C++,
    <a href='http://prdownloads.sourceforge.net/gzproject/CIQSample-1.0.0.rar?download'>CIQSample-1.0.0.rar</a>
  </p>

</div>
</div>
<?php
include(GZ_ROOT_PATH.'/footer.php');
?>