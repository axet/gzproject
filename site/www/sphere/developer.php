<?php 
define('GZ_ROOT_PATH', './..'); 

include(GZ_ROOT_PATH.'/common.php');
include(GZ_ROOT_PATH.'/header.php');
?>
<div id="content">
<?php include(GZ_ROOT_PATH.'/nav.php');?>
<div class="page">
  <h1>
    <a name="9">Sphere Balancer</a>
  </h1>

  <h2>Документация для разработчиков</h2>

  <p>
    Полная докуменатция находиться на страницах сайта, а последняя версия в SVN папке <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/trunk/doc">Developer Documentation</a>
  </p>

  <h2>SVN: SOURCE</h2>
  <p>
    Код этого проекта являеться открытым и доступен по адресу:
    <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/sphere-20020624">sphere-20020624</a>
  </p>

</div>
</div>
<?php
include(GZ_ROOT_PATH.'/footer.php');
?>