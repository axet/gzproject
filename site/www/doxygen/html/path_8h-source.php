<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_9aa33d2cd8dd981e9a7ee8bcdb7921b2.php">library</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_a6fc829ba20568df93f8f069fe73ef99.php">iniext</a></div>
<h1>path.h</h1><a href="path_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// GZProject - library, Ultima Online utils.</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="comment">// This program is free software; you can redistribute it and/or</span>
<a name="l00005"></a>00005 <span class="comment">// modify it under the terms of the GNU General Public License</span>
<a name="l00006"></a>00006 <span class="comment">// as published by the Free Software Foundation; either version 2</span>
<a name="l00007"></a>00007 <span class="comment">// of the License, or (at your option) any later version.</span>
<a name="l00008"></a>00008
<a name="l00009"></a>00009 <span class="comment">// This program is distributed in the hope that it will be useful,</span>
<a name="l00010"></a>00010 <span class="comment">// but WITHOUT ANY WARRANTY; without even the implied warranty of</span>
<a name="l00011"></a>00011 <span class="comment">// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the</span>
<a name="l00012"></a>00012 <span class="comment">// GNU General Public License for more details.</span>
<a name="l00013"></a>00013
<a name="l00014"></a>00014 <span class="comment">// You should have received a copy of the GNU General Public License</span>
<a name="l00015"></a>00015 <span class="comment">// along with this program; if not, write to the Free Software</span>
<a name="l00016"></a>00016 <span class="comment">// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</span>
<a name="l00017"></a>00017
<a name="l00018"></a>00018 <span class="preprocessor">#pragma once</span>
<a name="l00019"></a>00019 <span class="preprocessor"></span>
<a name="l00020"></a>00020 <span class="preprocessor">#include &lt;string&gt;</span>
<a name="l00021"></a>00021
<a name="l00022"></a>00022 <span class="keyword">namespace </span>IniExt
<a name="l00023"></a>00023 {
<a name="l00026"></a>00026   std::string <a class="code" href="namespace_ini_ext.php#d57a751f4b0835a7f50f46e09eb4e582">GetModuleRunDir</a>(<span class="keywordtype">void</span>* h = 0);
<a name="l00027"></a>00027   std::string <a class="code" href="namespace_ini_ext.php#e8ec57b84b079b6b4bcac40184ab24dc">GetModuleFileName</a>(<span class="keywordtype">void</span>* h = 0);
<a name="l00029"></a>00029   std::string <a class="code" href="namespace_ini_ext.php#bb454be0d063b277b6b64868fbea522e">GetModuleFullName</a>(<span class="keywordtype">void</span>* h = 0);
<a name="l00031"></a>00031   std::string <a class="code" href="namespace_ini_ext.php#a95b514880abb2c5fc111d58372d2635">GetModuleName</a>(<span class="keywordtype">void</span>* h = 0);
<a name="l00033"></a>00033   std::string <a class="code" href="namespace_ini_ext.php#cb7001002fff0e81ee96f9da42701da8">GetSystemDirectory</a>();
<a name="l00035"></a>00035   std::string <a class="code" href="namespace_ini_ext.php#28417a9bc7b70f721d7d46aab86b98bf">GetTempFilePath</a>();
<a name="l00037"></a>00037   <span class="keywordtype">void</span> <a class="code" href="namespace_ini_ext.php#28f6778cd9bcc901b164717746c1b991">RemoveDirectoryTree</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* path);
<a name="l00039"></a>00039   <span class="keywordtype">void</span> <a class="code" href="namespace_ini_ext.php#aa61164b23bab3f45eec1a9ef624b620">CreateDirectoryTree</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* path);
<a name="l00040"></a>00040 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
