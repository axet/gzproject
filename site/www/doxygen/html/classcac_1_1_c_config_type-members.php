<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>cac::CConfigType Member List</h1>This is the complete list of members for <a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#82474039265f99e23d770e80d6a45d60">AddClientPath</a>(CSchemaString ClientPath)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#82474039265f99e23d770e80d6a45d60">AddClientPath</a>(CSchemaString ClientPath)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#268d4c6c4d0263f3211a3da8bf9ac183">AddEmulation</a>(CEmulationType Emulation)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#268d4c6c4d0263f3211a3da8bf9ac183">AddEmulation</a>(CEmulationType Emulation)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#6146d516877e33e3febd44896bd72379">AddSecure</a>(CSecureType Secure)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#6146d516877e33e3febd44896bd72379">AddSecure</a>(CSecureType Secure)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#80a97a6f8b0d0445b807dae89c5f6519">CConfigType</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#63d90ca18e62133b171f67829ad70aad">CConfigType</a>(CNode &amp;rParentNode, MSXML2::IXMLDOMNodePtr spThisNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#736a9a6a1d24a7dd88ebfef670ba63a2">CConfigType</a>(MSXML2::IXMLDOMDocument2Ptr spDoc)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#80a97a6f8b0d0445b807dae89c5f6519">CConfigType</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#63d90ca18e62133b171f67829ad70aad">CConfigType</a>(CNode &amp;rParentNode, MSXML2::IXMLDOMNodePtr spThisNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#736a9a6a1d24a7dd88ebfef670ba63a2">CConfigType</a>(MSXML2::IXMLDOMDocument2Ptr spDoc)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#34d52c75a18f0cd2f1d2dd2dca666e39">GetAdvancedClientPathCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#34d52c75a18f0cd2f1d2dd2dca666e39">GetAdvancedClientPathCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#aea5a5b2fe417ae99d4b05a470679a5a">GetAdvancedEmulationCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#aea5a5b2fe417ae99d4b05a470679a5a">GetAdvancedEmulationCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#aa74bfc4b692dce847047248d39e7470">GetAdvancedSecureCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#aa74bfc4b692dce847047248d39e7470">GetAdvancedSecureCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#e2158eb9d5ef59ca840e5420692e4122">GetClientPath</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#e2158eb9d5ef59ca840e5420692e4122">GetClientPath</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#cd1e23fd1277c572ef67ec41cfea21e0">GetClientPathAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#cd1e23fd1277c572ef67ec41cfea21e0">GetClientPathAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#2f9fc76e1b6d4ad432278ede4169b7db">GetClientPathCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#2f9fc76e1b6d4ad432278ede4169b7db">GetClientPathCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#3949243d6571dc5e4db9393103bc511f">GetClientPathMaxCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#ba444b3b85af10caf745a1e8936d805e">GetClientPathMaxCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#7c5b1a2547c8e3a109205e589fb49328">GetClientPathMinCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#d023752d5eb001f6a8d793586db66481">GetClientPathMinCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#70cf8a735802f73f630b4bd03822bb8b">GetClientPathValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#70cf8a735802f73f630b4bd03822bb8b">GetClientPathValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#92f450ad8f6bbbe8bc7480bcfceed8f4">GetEmulation</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#92f450ad8f6bbbe8bc7480bcfceed8f4">GetEmulation</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#aff72c341112d58d40d16e6de6208d81">GetEmulationAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#aff72c341112d58d40d16e6de6208d81">GetEmulationAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#f0a4c5fe33015f7018b8ddc8fadd8f46">GetEmulationCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#f0a4c5fe33015f7018b8ddc8fadd8f46">GetEmulationCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#d6802a8b3aceebe077b6f9ae4f05fcac">GetEmulationMaxCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#87b226b3947c87a2def7458d2b3d388e">GetEmulationMaxCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#6e2071e204c1cd2ac823cd12c2978a69">GetEmulationMinCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#d36528b032289ab7bd6143d7bc3448f9">GetEmulationMinCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#6bb24caec2a579a5454e279830669645">GetEmulationValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#6bb24caec2a579a5454e279830669645">GetEmulationValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#995efb3bd42026fdb197a6d1185ac754">GetGroupType</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#09a1944b54773b1a5d47b318d43a8708">GetGroupType</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#dd24f46b5b8cc37f93304d231529707d">GetSecure</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#dd24f46b5b8cc37f93304d231529707d">GetSecure</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#f2cbbc493a4a47ce312ec1b859781316">GetSecureAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#f2cbbc493a4a47ce312ec1b859781316">GetSecureAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#4ccd89bb9f6f871fbeaed750be68aaf5">GetSecureCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#4ccd89bb9f6f871fbeaed750be68aaf5">GetSecureCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#414c7b332dbc9d9d92e5dee3b347c576">GetSecureMaxCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#d845358ff7b044fd31147f96bb4f9d56">GetSecureMaxCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#2dd6d24fc293b64a900778b667aa5185">GetSecureMinCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#cdc0dd62410db29c5669dbf7daaf27de">GetSecureMinCount</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#967db944cfd98f0d569c1829ce78bedf">GetSecureValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#967db944cfd98f0d569c1829ce78bedf">GetSecureValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#413793afcbe34bd1e0c46d633a1912f2">GetStartingClientPathCursor</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#413793afcbe34bd1e0c46d633a1912f2">GetStartingClientPathCursor</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#302b67b73d08e54fe2194752603035a8">GetStartingEmulationCursor</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#302b67b73d08e54fe2194752603035a8">GetStartingEmulationCursor</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#41226f83a5454aa276ff1d85a9fffda5">GetStartingSecureCursor</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#41226f83a5454aa276ff1d85a9fffda5">GetStartingSecureCursor</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#ca406205c13c82b58c3711c296a2d57d">HasClientPath</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#ca406205c13c82b58c3711c296a2d57d">HasClientPath</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#351cd79b3b80bb0e90b80ed1888a387a">HasEmulation</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#351cd79b3b80bb0e90b80ed1888a387a">HasEmulation</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#8e3989f75debc41579ee061bcdbf5651">HasSecure</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#8e3989f75debc41579ee061bcdbf5651">HasSecure</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#fa6f9698024d2201c0e6f8a1b4f07bd5">InsertClientPathAt</a>(CSchemaString ClientPath, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#fa6f9698024d2201c0e6f8a1b4f07bd5">InsertClientPathAt</a>(CSchemaString ClientPath, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#aa88eae2bc6e07597b3e27b96b2ae2ee">InsertEmulationAt</a>(CEmulationType Emulation, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#aa88eae2bc6e07597b3e27b96b2ae2ee">InsertEmulationAt</a>(CEmulationType Emulation, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#61aaa5005de2ae4590d354f6d9918196">InsertSecureAt</a>(CSecureType Secure, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#61aaa5005de2ae4590d354f6d9918196">InsertSecureAt</a>(CSecureType Secure, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#4f64cb7489014e43216c0c3f59e3901c">RemoveClientPath</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#4f64cb7489014e43216c0c3f59e3901c">RemoveClientPath</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#73db29949941fbcba49e7f99be88582b">RemoveClientPathAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#73db29949941fbcba49e7f99be88582b">RemoveClientPathAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#bc547de15b5f9cc82d9e069ef27604f1">RemoveEmulation</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#bc547de15b5f9cc82d9e069ef27604f1">RemoveEmulation</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#1bd79fcb60b020828c212301c20f13a0">RemoveEmulationAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#1bd79fcb60b020828c212301c20f13a0">RemoveEmulationAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#4d04d7651d173f6960f5e9ea3afc28c3">RemoveSecure</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#4d04d7651d173f6960f5e9ea3afc28c3">RemoveSecure</a>()</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#934931b95d6ed4e8dfee818755740b95">RemoveSecureAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#934931b95d6ed4e8dfee818755740b95">RemoveSecureAt</a>(int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#9b293a0a4cf5584e8113d845137f0bf6">ReplaceClientPathAt</a>(CSchemaString ClientPath, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#9b293a0a4cf5584e8113d845137f0bf6">ReplaceClientPathAt</a>(CSchemaString ClientPath, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#5f8d89bbbe6cee8a46861848fe073b75">ReplaceEmulationAt</a>(CEmulationType Emulation, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#5f8d89bbbe6cee8a46861848fe073b75">ReplaceEmulationAt</a>(CEmulationType Emulation, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#bcd5da344ce913253263052f19f22d56">ReplaceSecureAt</a>(CSecureType Secure, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classcac_1_1_c_config_type.php#bcd5da344ce913253263052f19f22d56">ReplaceSecureAt</a>(CSecureType Secure, int nIndex)</td><td><a class="el" href="classcac_1_1_c_config_type.php">cac::CConfigType</a></td><td></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
