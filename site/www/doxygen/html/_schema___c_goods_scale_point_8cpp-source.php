<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ea6c3855b402b97c9537b801f064bee.php">GZone</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_347b1ab9c8f40ff6d2ccb1af4edca24f.php">xsd</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ec6636ee402724ca794c7dbb85eb5f3.php">Schema</a></div>
<h1>Schema_CGoodsScalePoint.cpp</h1><a href="_schema___c_goods_scale_point_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// Schema_CGoodsScalePoint.cpp</span>
<a name="l00004"></a>00004 <span class="comment">//</span>
<a name="l00005"></a>00005 <span class="comment">// This file was generated by XMLSpy 2006 Enterprise Edition.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00008"></a>00008 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00009"></a>00009 <span class="comment">//</span>
<a name="l00010"></a>00010 <span class="comment">// Refer to the XMLSpy Documentation for further details.</span>
<a name="l00011"></a>00011 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00012"></a>00012 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment"></span>
<a name="l00015"></a>00015
<a name="l00016"></a>00016 <span class="preprocessor">#include "<a class="code" href="_g_z_2_g_zone_2xsd_2_schema_2_std_afx_8h.php">StdAfx.h</a>"</span>
<a name="l00017"></a>00017 <span class="preprocessor">#include "<a class="code" href="_schema_base_8h.php">SchemaBase.h</a>"</span>
<a name="l00018"></a>00018 <span class="preprocessor">#include "<a class="code" href="_schema___c_goods_scale_point_8h.php">Schema_CGoodsScalePoint.h</a>"</span>
<a name="l00019"></a>00019
<a name="l00020"></a>00020
<a name="l00021"></a>00021
<a name="l00022"></a>00022 <span class="keyword">namespace </span>goodsconf <span class="comment">// URI: http://gzproject.sourceforge.net/GoodsConfig</span>
<a name="l00023"></a>00023 {
<a name="l00025"></a>00025 <span class="comment">//</span>
<a name="l00026"></a>00026 <span class="comment">// class CGoodsScalePoint</span>
<a name="l00027"></a>00027 <span class="comment">//</span>
<a name="l00029"></a>00029 <span class="comment"></span>
<a name="l00030"></a>00030
<a name="l00031"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#b06db5cda1e1a1ed6f25f22ac6fff29e">00031</a> <a class="code" href="classaltova_1_1_c_node.php#23a91b5b6ef1abca12745ecd7a2fe7a4">CNode::EGroupType</a> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#b06db5cda1e1a1ed6f25f22ac6fff29e">CGoodsScalePoint::GetGroupType</a>()
<a name="l00032"></a>00032 {
<a name="l00033"></a>00033         <span class="keywordflow">return</span> eSequence;
<a name="l00034"></a>00034 }
<a name="l00035"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#ec273dfe1a273899aac07a3a9d7c9a46">00035</a> <span class="keywordtype">int</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#ec273dfe1a273899aac07a3a9d7c9a46">CGoodsScalePoint::GetXMinCount</a>()
<a name="l00036"></a>00036 {
<a name="l00037"></a>00037         <span class="keywordflow">return</span> 1;
<a name="l00038"></a>00038 }
<a name="l00039"></a>00039
<a name="l00040"></a>00040
<a name="l00041"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#96344680f085172e0b5e900cf4a4157f">00041</a> <span class="keywordtype">int</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#96344680f085172e0b5e900cf4a4157f">CGoodsScalePoint::GetXMaxCount</a>()
<a name="l00042"></a>00042 {
<a name="l00043"></a>00043         <span class="keywordflow">return</span> 1;
<a name="l00044"></a>00044 }
<a name="l00045"></a>00045
<a name="l00046"></a>00046
<a name="l00047"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#030d4b953571834aa80888c71ebc4566">00047</a> <span class="keywordtype">int</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#030d4b953571834aa80888c71ebc4566">CGoodsScalePoint::GetXCount</a>()
<a name="l00048"></a>00048 {
<a name="l00049"></a>00049         <span class="keywordflow">return</span> ChildCountInternal(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:X"</span>));
<a name="l00050"></a>00050 }
<a name="l00051"></a>00051
<a name="l00052"></a>00052
<a name="l00053"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#36207f6faaf34f3bc94cb17effc47f61">00053</a> <span class="keywordtype">bool</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#36207f6faaf34f3bc94cb17effc47f61">CGoodsScalePoint::HasX</a>()
<a name="l00054"></a>00054 {
<a name="l00055"></a>00055         <span class="keywordflow">return</span> InternalHasChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:X"</span>));
<a name="l00056"></a>00056 }
<a name="l00057"></a>00057
<a name="l00058"></a>00058
<a name="l00059"></a>00059 <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#f740dc2cf1c139f298875b5abe0fb2ff">CGoodsScalePoint::AddX</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> X)
<a name="l00060"></a>00060 {
<a name="l00061"></a>00061         <span class="keywordflow">if</span>( !X.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00062"></a>00062                 InternalAppend(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:X"</span>), X);
<a name="l00063"></a>00063 }
<a name="l00064"></a>00064
<a name="l00065"></a>00065
<a name="l00066"></a>00066 <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#12324c378ca8cdd679ea9fe237ff304b">CGoodsScalePoint::InsertXAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> X, <span class="keywordtype">int</span> nIndex)
<a name="l00067"></a>00067 {
<a name="l00068"></a>00068         <span class="keywordflow">if</span>( !X.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00069"></a>00069                 InternalInsertAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:X"</span>), nIndex, X);
<a name="l00070"></a>00070 }
<a name="l00071"></a>00071
<a name="l00072"></a>00072
<a name="l00073"></a>00073 <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#e067dfeba002602044fca4339d3947f8">CGoodsScalePoint::ReplaceXAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> X, <span class="keywordtype">int</span> nIndex)
<a name="l00074"></a>00074 {
<a name="l00075"></a>00075         InternalReplaceAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:X"</span>), nIndex, X);
<a name="l00076"></a>00076 }
<a name="l00077"></a>00077
<a name="l00078"></a>00078
<a name="l00079"></a>00079
<a name="l00080"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#2676a253eadcaa03f8777f1559dd901c">00080</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#2676a253eadcaa03f8777f1559dd901c">CGoodsScalePoint::GetXAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00081"></a>00081 {
<a name="l00082"></a>00082         <span class="keywordflow">return</span> (LPCTSTR)InternalGetAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:X"</span>), nIndex)-&gt;text;
<a name="l00083"></a>00083         <span class="comment">//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/GoodsConfig"), _T("goodsconf:X"), nIndex)-&gt;text);</span>
<a name="l00084"></a>00084 }
<a name="l00085"></a>00085
<a name="l00086"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#ede3ad2dd74d605565ae58bff4c83b64">00086</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#ede3ad2dd74d605565ae58bff4c83b64">CGoodsScalePoint::GetStartingXCursor</a>()
<a name="l00087"></a>00087 {
<a name="l00088"></a>00088         <span class="keywordflow">return</span> InternalGetFirstChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:X"</span>));
<a name="l00089"></a>00089 }
<a name="l00090"></a>00090
<a name="l00091"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#b5baf5f676ceb7a6369cec7937bb0b73">00091</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#b5baf5f676ceb7a6369cec7937bb0b73">CGoodsScalePoint::GetAdvancedXCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00092"></a>00092 {
<a name="l00093"></a>00093         <span class="keywordflow">return</span> InternalGetNextChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:X"</span>), pCurNode);
<a name="l00094"></a>00094 }
<a name="l00095"></a>00095
<a name="l00096"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#66ea98e101fcc5bd840531709e04b4b2">00096</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#66ea98e101fcc5bd840531709e04b4b2">CGoodsScalePoint::GetXValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00097"></a>00097 {
<a name="l00098"></a>00098         <span class="keywordflow">if</span>( pCurNode == NULL )
<a name="l00099"></a>00099                 <span class="keywordflow">throw</span> <a class="code" href="classaltova_1_1_c_xml_exception.php">CXmlException</a>(CXmlException::eError1, _T(<span class="stringliteral">"Index out of range"</span>));
<a name="l00100"></a>00100         <span class="keywordflow">else</span>
<a name="l00101"></a>00101
<a name="l00102"></a>00102                 <span class="keywordflow">return</span> (LPCTSTR)pCurNode-&gt;text;
<a name="l00103"></a>00103 }
<a name="l00104"></a>00104
<a name="l00105"></a>00105
<a name="l00106"></a>00106
<a name="l00107"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#33862f132094cdb54da1b2149dba0a0d">00107</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#33862f132094cdb54da1b2149dba0a0d">CGoodsScalePoint::GetX</a>()
<a name="l00108"></a>00108 {
<a name="l00109"></a>00109         <span class="keywordflow">return</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#2676a253eadcaa03f8777f1559dd901c">GetXAt</a>(0);
<a name="l00110"></a>00110 }
<a name="l00111"></a>00111
<a name="l00112"></a>00112
<a name="l00113"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#a953e47966f37c97c4b1b93a4731d549">00113</a> <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#a953e47966f37c97c4b1b93a4731d549">CGoodsScalePoint::RemoveXAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00114"></a>00114 {
<a name="l00115"></a>00115         InternalRemoveAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:X"</span>), nIndex);
<a name="l00116"></a>00116 }
<a name="l00117"></a>00117
<a name="l00118"></a>00118
<a name="l00119"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#6c2bf4d26fe42d9a8411ef4ded50442b">00119</a> <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#6c2bf4d26fe42d9a8411ef4ded50442b">CGoodsScalePoint::RemoveX</a>()
<a name="l00120"></a>00120 {
<a name="l00121"></a>00121         <span class="keywordflow">while</span> (<a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#36207f6faaf34f3bc94cb17effc47f61">HasX</a>())
<a name="l00122"></a>00122                 <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#a953e47966f37c97c4b1b93a4731d549">RemoveXAt</a>(0);
<a name="l00123"></a>00123 }
<a name="l00124"></a>00124
<a name="l00125"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#0bde3f2106e520a2987d522f8a4ebe61">00125</a> <span class="keywordtype">int</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#0bde3f2106e520a2987d522f8a4ebe61">CGoodsScalePoint::GetYMinCount</a>()
<a name="l00126"></a>00126 {
<a name="l00127"></a>00127         <span class="keywordflow">return</span> 1;
<a name="l00128"></a>00128 }
<a name="l00129"></a>00129
<a name="l00130"></a>00130
<a name="l00131"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#aa2a92a4317553af9d858cb43218c68c">00131</a> <span class="keywordtype">int</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#aa2a92a4317553af9d858cb43218c68c">CGoodsScalePoint::GetYMaxCount</a>()
<a name="l00132"></a>00132 {
<a name="l00133"></a>00133         <span class="keywordflow">return</span> 1;
<a name="l00134"></a>00134 }
<a name="l00135"></a>00135
<a name="l00136"></a>00136
<a name="l00137"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#96c98e834335e36641f6bada213e2cce">00137</a> <span class="keywordtype">int</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#96c98e834335e36641f6bada213e2cce">CGoodsScalePoint::GetYCount</a>()
<a name="l00138"></a>00138 {
<a name="l00139"></a>00139         <span class="keywordflow">return</span> ChildCountInternal(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Y"</span>));
<a name="l00140"></a>00140 }
<a name="l00141"></a>00141
<a name="l00142"></a>00142
<a name="l00143"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#4e4aa4022b27c50fe7cf32796b460146">00143</a> <span class="keywordtype">bool</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#4e4aa4022b27c50fe7cf32796b460146">CGoodsScalePoint::HasY</a>()
<a name="l00144"></a>00144 {
<a name="l00145"></a>00145         <span class="keywordflow">return</span> InternalHasChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Y"</span>));
<a name="l00146"></a>00146 }
<a name="l00147"></a>00147
<a name="l00148"></a>00148
<a name="l00149"></a>00149 <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#ed5fab99142dae55c9bf4ae90602e9f4">CGoodsScalePoint::AddY</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> Y)
<a name="l00150"></a>00150 {
<a name="l00151"></a>00151         <span class="keywordflow">if</span>( !Y.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00152"></a>00152                 InternalAppend(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Y"</span>), Y);
<a name="l00153"></a>00153 }
<a name="l00154"></a>00154
<a name="l00155"></a>00155
<a name="l00156"></a>00156 <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#4ddccdb69df97cf8b71489de18b41184">CGoodsScalePoint::InsertYAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> Y, <span class="keywordtype">int</span> nIndex)
<a name="l00157"></a>00157 {
<a name="l00158"></a>00158         <span class="keywordflow">if</span>( !Y.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00159"></a>00159                 InternalInsertAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Y"</span>), nIndex, Y);
<a name="l00160"></a>00160 }
<a name="l00161"></a>00161
<a name="l00162"></a>00162
<a name="l00163"></a>00163 <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#7f4b29859cb06d53d80885b750217d7f">CGoodsScalePoint::ReplaceYAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> Y, <span class="keywordtype">int</span> nIndex)
<a name="l00164"></a>00164 {
<a name="l00165"></a>00165         InternalReplaceAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Y"</span>), nIndex, Y);
<a name="l00166"></a>00166 }
<a name="l00167"></a>00167
<a name="l00168"></a>00168
<a name="l00169"></a>00169
<a name="l00170"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#61112aec7b1c44186f9267c519272e5a">00170</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#61112aec7b1c44186f9267c519272e5a">CGoodsScalePoint::GetYAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00171"></a>00171 {
<a name="l00172"></a>00172         <span class="keywordflow">return</span> (LPCTSTR)InternalGetAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Y"</span>), nIndex)-&gt;text;
<a name="l00173"></a>00173         <span class="comment">//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/GoodsConfig"), _T("goodsconf:Y"), nIndex)-&gt;text);</span>
<a name="l00174"></a>00174 }
<a name="l00175"></a>00175
<a name="l00176"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#998b57fcdb81705718782f150651002b">00176</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#998b57fcdb81705718782f150651002b">CGoodsScalePoint::GetStartingYCursor</a>()
<a name="l00177"></a>00177 {
<a name="l00178"></a>00178         <span class="keywordflow">return</span> InternalGetFirstChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Y"</span>));
<a name="l00179"></a>00179 }
<a name="l00180"></a>00180
<a name="l00181"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#8b04e51cd53f5520c9f62498075a4661">00181</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#8b04e51cd53f5520c9f62498075a4661">CGoodsScalePoint::GetAdvancedYCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00182"></a>00182 {
<a name="l00183"></a>00183         <span class="keywordflow">return</span> InternalGetNextChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Y"</span>), pCurNode);
<a name="l00184"></a>00184 }
<a name="l00185"></a>00185
<a name="l00186"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#918e88e31c324a58314508eb63ffaaec">00186</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#918e88e31c324a58314508eb63ffaaec">CGoodsScalePoint::GetYValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00187"></a>00187 {
<a name="l00188"></a>00188         <span class="keywordflow">if</span>( pCurNode == NULL )
<a name="l00189"></a>00189                 <span class="keywordflow">throw</span> <a class="code" href="classaltova_1_1_c_xml_exception.php">CXmlException</a>(CXmlException::eError1, _T(<span class="stringliteral">"Index out of range"</span>));
<a name="l00190"></a>00190         <span class="keywordflow">else</span>
<a name="l00191"></a>00191
<a name="l00192"></a>00192                 <span class="keywordflow">return</span> (LPCTSTR)pCurNode-&gt;text;
<a name="l00193"></a>00193 }
<a name="l00194"></a>00194
<a name="l00195"></a>00195
<a name="l00196"></a>00196
<a name="l00197"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#6da72db1fdd5a2dd9eeb25f77d8022ef">00197</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaDecimal</a> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#6da72db1fdd5a2dd9eeb25f77d8022ef">CGoodsScalePoint::GetY</a>()
<a name="l00198"></a>00198 {
<a name="l00199"></a>00199         <span class="keywordflow">return</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#61112aec7b1c44186f9267c519272e5a">GetYAt</a>(0);
<a name="l00200"></a>00200 }
<a name="l00201"></a>00201
<a name="l00202"></a>00202
<a name="l00203"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#29a7bf04198ed0a9522313ecf310c373">00203</a> <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#29a7bf04198ed0a9522313ecf310c373">CGoodsScalePoint::RemoveYAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00204"></a>00204 {
<a name="l00205"></a>00205         InternalRemoveAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Y"</span>), nIndex);
<a name="l00206"></a>00206 }
<a name="l00207"></a>00207
<a name="l00208"></a>00208
<a name="l00209"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#8fa3ce4e6d594ee37cad5c70ffd76964">00209</a> <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#8fa3ce4e6d594ee37cad5c70ffd76964">CGoodsScalePoint::RemoveY</a>()
<a name="l00210"></a>00210 {
<a name="l00211"></a>00211         <span class="keywordflow">while</span> (<a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#4e4aa4022b27c50fe7cf32796b460146">HasY</a>())
<a name="l00212"></a>00212                 <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#29a7bf04198ed0a9522313ecf310c373">RemoveYAt</a>(0);
<a name="l00213"></a>00213 }
<a name="l00214"></a>00214
<a name="l00215"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#2d21a9519d9e1f55d5c0f137c1997ede">00215</a> <span class="keywordtype">int</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#2d21a9519d9e1f55d5c0f137c1997ede">CGoodsScalePoint::GetZMinCount</a>()
<a name="l00216"></a>00216 {
<a name="l00217"></a>00217         <span class="keywordflow">return</span> 1;
<a name="l00218"></a>00218 }
<a name="l00219"></a>00219
<a name="l00220"></a>00220
<a name="l00221"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#db80cd27bba15b1c0307cea83ddc3d3f">00221</a> <span class="keywordtype">int</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#db80cd27bba15b1c0307cea83ddc3d3f">CGoodsScalePoint::GetZMaxCount</a>()
<a name="l00222"></a>00222 {
<a name="l00223"></a>00223         <span class="keywordflow">return</span> 1;
<a name="l00224"></a>00224 }
<a name="l00225"></a>00225
<a name="l00226"></a>00226
<a name="l00227"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#fb56ac9a4fce6f3717c994127ed8f076">00227</a> <span class="keywordtype">int</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#fb56ac9a4fce6f3717c994127ed8f076">CGoodsScalePoint::GetZCount</a>()
<a name="l00228"></a>00228 {
<a name="l00229"></a>00229         <span class="keywordflow">return</span> ChildCountInternal(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Z"</span>));
<a name="l00230"></a>00230 }
<a name="l00231"></a>00231
<a name="l00232"></a>00232
<a name="l00233"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#b8ef212259a10a040bf1015a1443a293">00233</a> <span class="keywordtype">bool</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#b8ef212259a10a040bf1015a1443a293">CGoodsScalePoint::HasZ</a>()
<a name="l00234"></a>00234 {
<a name="l00235"></a>00235         <span class="keywordflow">return</span> InternalHasChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Z"</span>));
<a name="l00236"></a>00236 }
<a name="l00237"></a>00237
<a name="l00238"></a>00238
<a name="l00239"></a>00239 <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#788bf632b995edd1d42973283fe944b9">CGoodsScalePoint::AddZ</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> Z)
<a name="l00240"></a>00240 {
<a name="l00241"></a>00241         <span class="keywordflow">if</span>( !Z.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00242"></a>00242                 InternalAppend(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Z"</span>), Z);
<a name="l00243"></a>00243 }
<a name="l00244"></a>00244
<a name="l00245"></a>00245
<a name="l00246"></a>00246 <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#d0ece7ee95e77db55b93834741d932ff">CGoodsScalePoint::InsertZAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> Z, <span class="keywordtype">int</span> nIndex)
<a name="l00247"></a>00247 {
<a name="l00248"></a>00248         <span class="keywordflow">if</span>( !Z.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00249"></a>00249                 InternalInsertAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Z"</span>), nIndex, Z);
<a name="l00250"></a>00250 }
<a name="l00251"></a>00251
<a name="l00252"></a>00252
<a name="l00253"></a>00253 <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#0c442333f7078ab11606ac94d3400961">CGoodsScalePoint::ReplaceZAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> Z, <span class="keywordtype">int</span> nIndex)
<a name="l00254"></a>00254 {
<a name="l00255"></a>00255         InternalReplaceAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Z"</span>), nIndex, Z);
<a name="l00256"></a>00256 }
<a name="l00257"></a>00257
<a name="l00258"></a>00258
<a name="l00259"></a>00259
<a name="l00260"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#517e32b212ee867419cd16750e6fb739">00260</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#517e32b212ee867419cd16750e6fb739">CGoodsScalePoint::GetZAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00261"></a>00261 {
<a name="l00262"></a>00262         <span class="keywordflow">return</span> (LPCTSTR)InternalGetAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Z"</span>), nIndex)-&gt;text;
<a name="l00263"></a>00263         <span class="comment">//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/GoodsConfig"), _T("goodsconf:Z"), nIndex)-&gt;text);</span>
<a name="l00264"></a>00264 }
<a name="l00265"></a>00265
<a name="l00266"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#21426cc7025cea177479eb715c78d43d">00266</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#21426cc7025cea177479eb715c78d43d">CGoodsScalePoint::GetStartingZCursor</a>()
<a name="l00267"></a>00267 {
<a name="l00268"></a>00268         <span class="keywordflow">return</span> InternalGetFirstChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Z"</span>));
<a name="l00269"></a>00269 }
<a name="l00270"></a>00270
<a name="l00271"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#0d7cb7e6dbae6e9958bb1ee25563cc69">00271</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#0d7cb7e6dbae6e9958bb1ee25563cc69">CGoodsScalePoint::GetAdvancedZCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00272"></a>00272 {
<a name="l00273"></a>00273         <span class="keywordflow">return</span> InternalGetNextChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Z"</span>), pCurNode);
<a name="l00274"></a>00274 }
<a name="l00275"></a>00275
<a name="l00276"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#06bdfc07ad344e9cbb45d1aa06dbe13b">00276</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#06bdfc07ad344e9cbb45d1aa06dbe13b">CGoodsScalePoint::GetZValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00277"></a>00277 {
<a name="l00278"></a>00278         <span class="keywordflow">if</span>( pCurNode == NULL )
<a name="l00279"></a>00279                 <span class="keywordflow">throw</span> <a class="code" href="classaltova_1_1_c_xml_exception.php">CXmlException</a>(CXmlException::eError1, _T(<span class="stringliteral">"Index out of range"</span>));
<a name="l00280"></a>00280         <span class="keywordflow">else</span>
<a name="l00281"></a>00281
<a name="l00282"></a>00282                 <span class="keywordflow">return</span> (LPCTSTR)pCurNode-&gt;text;
<a name="l00283"></a>00283 }
<a name="l00284"></a>00284
<a name="l00285"></a>00285
<a name="l00286"></a>00286
<a name="l00287"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#8834e20faecb79053ce5ed926e7bf219">00287</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#8834e20faecb79053ce5ed926e7bf219">CGoodsScalePoint::GetZ</a>()
<a name="l00288"></a>00288 {
<a name="l00289"></a>00289         <span class="keywordflow">return</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#517e32b212ee867419cd16750e6fb739">GetZAt</a>(0);
<a name="l00290"></a>00290 }
<a name="l00291"></a>00291
<a name="l00292"></a>00292
<a name="l00293"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#65bf29318fef84cdd476387561ef8efa">00293</a> <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#65bf29318fef84cdd476387561ef8efa">CGoodsScalePoint::RemoveZAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00294"></a>00294 {
<a name="l00295"></a>00295         InternalRemoveAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/GoodsConfig"</span>), _T(<span class="stringliteral">"goodsconf:Z"</span>), nIndex);
<a name="l00296"></a>00296 }
<a name="l00297"></a>00297
<a name="l00298"></a>00298
<a name="l00299"></a><a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#07fa48a79ab1069a5e7e645597834296">00299</a> <span class="keywordtype">void</span> <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#07fa48a79ab1069a5e7e645597834296">CGoodsScalePoint::RemoveZ</a>()
<a name="l00300"></a>00300 {
<a name="l00301"></a>00301         <span class="keywordflow">while</span> (<a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#b8ef212259a10a040bf1015a1443a293">HasZ</a>())
<a name="l00302"></a>00302                 <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php#65bf29318fef84cdd476387561ef8efa">RemoveZAt</a>(0);
<a name="l00303"></a>00303 }
<a name="l00304"></a>00304
<a name="l00305"></a>00305 } <span class="comment">// end of namespace goodsconf</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
