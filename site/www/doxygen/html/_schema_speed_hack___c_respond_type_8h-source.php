<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_8ba19508940e77109a2da1f76a26e336.php">ServerAddin</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_2efa85a7c8835a7e29a8c925d0732445.php">speedhack</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_e0cd0a396b8837484e274ac14ceb0c18.php">xsd</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_bcb512f6fcd7c2f3d5b8e8efb73572f8.php">SchemaSpeedHack</a></div>
<h1>SchemaSpeedHack_CRespondType.h</h1><a href="_schema_speed_hack___c_respond_type_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// SchemaSpeedHack_CRespondType.h</span>
<a name="l00004"></a>00004 <span class="comment">//</span>
<a name="l00005"></a>00005 <span class="comment">// This file was generated by XMLSpy 2006 Enterprise Edition.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00008"></a>00008 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00009"></a>00009 <span class="comment">//</span>
<a name="l00010"></a>00010 <span class="comment">// Refer to the XMLSpy Documentation for further details.</span>
<a name="l00011"></a>00011 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00012"></a>00012 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment"></span>
<a name="l00015"></a>00015
<a name="l00016"></a>00016 <span class="preprocessor">#ifndef SchemaSpeedHack_CRespondType_H_INCLUDED</span>
<a name="l00017"></a>00017 <span class="preprocessor"></span><span class="preprocessor">#define SchemaSpeedHack_CRespondType_H_INCLUDED</span>
<a name="l00018"></a>00018 <span class="preprocessor"></span>
<a name="l00019"></a>00019 <span class="preprocessor">#if _MSC_VER &gt; 1000</span>
<a name="l00020"></a>00020 <span class="preprocessor"></span><span class="preprocessor">        #pragma once</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span><span class="preprocessor">#endif // _MSC_VER &gt; 1000</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023
<a name="l00024"></a>00024
<a name="l00025"></a>00025
<a name="l00026"></a>00026 <span class="keyword">namespace </span>SpeedHack <span class="comment">// URI: www.sourceforge.net/projects/gzproject</span>
<a name="l00027"></a>00027 {
<a name="l00028"></a>00028
<a name="l00029"></a>00029
<a name="l00030"></a><a class="code" href="class_speed_hack_1_1_c_respond_type.php">00030</a> <span class="keyword">class </span><a class="code" href="_schema_speed_hack_base_8h.php#9e4a31c5d528962d51c020aa52ae1696">SchemaSpeedHack_DECLSPECIFIER</a> CRespondType : <span class="keyword">public</span> CNode
<a name="l00031"></a>00031 {
<a name="l00032"></a>00032 <span class="keyword">public</span>:
<a name="l00033"></a><a class="code" href="class_speed_hack_1_1_c_respond_type.php#ee6ce7b98b065e9e3dce46d8b0d7d67b">00033</a>         CRespondType() : CNode() {}
<a name="l00034"></a><a class="code" href="class_speed_hack_1_1_c_respond_type.php#88e71ee9ceb2e29d080617bf06a0f40c">00034</a>         CRespondType(CNode&amp; rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
<a name="l00035"></a><a class="code" href="class_speed_hack_1_1_c_respond_type.php#2e4e077df90e6e08d4b13d6eb135c100">00035</a>         CRespondType(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
<a name="l00036"></a>00036         <span class="keyword">static</span> EGroupType GetGroupType();
<a name="l00037"></a>00037
<a name="l00038"></a>00038         <span class="comment">//</span>
<a name="l00039"></a>00039         <span class="comment">// decimal WalkCount (1...1)</span>
<a name="l00040"></a>00040         <span class="comment">//</span>
<a name="l00041"></a>00041         <span class="keyword">static</span> <span class="keywordtype">int</span> GetWalkCountMinCount();
<a name="l00042"></a>00042         <span class="keyword">static</span> <span class="keywordtype">int</span> GetWalkCountMaxCount();
<a name="l00043"></a>00043         <span class="keywordtype">int</span> GetWalkCountCount();
<a name="l00044"></a>00044         <span class="keywordtype">bool</span> HasWalkCount();
<a name="l00045"></a>00045         <span class="keywordtype">void</span> AddWalkCount(<a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> WalkCount);
<a name="l00046"></a>00046         <span class="keywordtype">void</span> InsertWalkCountAt(<a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> WalkCount, <span class="keywordtype">int</span> nIndex);
<a name="l00047"></a>00047         <span class="keywordtype">void</span> ReplaceWalkCountAt(<a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> WalkCount, <span class="keywordtype">int</span> nIndex);
<a name="l00048"></a>00048         <a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> GetWalkCountAt(<span class="keywordtype">int</span> nIndex);
<a name="l00049"></a>00049         <a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> GetWalkCount();
<a name="l00050"></a>00050         MSXML2::IXMLDOMNodePtr GetStartingWalkCountCursor();
<a name="l00051"></a>00051         MSXML2::IXMLDOMNodePtr GetAdvancedWalkCountCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00052"></a>00052         <a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> GetWalkCountValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00053"></a>00053
<a name="l00054"></a>00054         <span class="keywordtype">void</span> RemoveWalkCountAt(<span class="keywordtype">int</span> nIndex);
<a name="l00055"></a>00055         <span class="keywordtype">void</span> RemoveWalkCount();
<a name="l00056"></a>00056
<a name="l00057"></a>00057         <span class="comment">//</span>
<a name="l00058"></a>00058         <span class="comment">// decimal WalkSpeed (1...1)</span>
<a name="l00059"></a>00059         <span class="comment">//</span>
<a name="l00060"></a>00060         <span class="keyword">static</span> <span class="keywordtype">int</span> GetWalkSpeedMinCount();
<a name="l00061"></a>00061         <span class="keyword">static</span> <span class="keywordtype">int</span> GetWalkSpeedMaxCount();
<a name="l00062"></a>00062         <span class="keywordtype">int</span> GetWalkSpeedCount();
<a name="l00063"></a>00063         <span class="keywordtype">bool</span> HasWalkSpeed();
<a name="l00064"></a>00064         <span class="keywordtype">void</span> AddWalkSpeed(<a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> WalkSpeed);
<a name="l00065"></a>00065         <span class="keywordtype">void</span> InsertWalkSpeedAt(<a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> WalkSpeed, <span class="keywordtype">int</span> nIndex);
<a name="l00066"></a>00066         <span class="keywordtype">void</span> ReplaceWalkSpeedAt(<a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> WalkSpeed, <span class="keywordtype">int</span> nIndex);
<a name="l00067"></a>00067         <a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> GetWalkSpeedAt(<span class="keywordtype">int</span> nIndex);
<a name="l00068"></a>00068         <a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> GetWalkSpeed();
<a name="l00069"></a>00069         MSXML2::IXMLDOMNodePtr GetStartingWalkSpeedCursor();
<a name="l00070"></a>00070         MSXML2::IXMLDOMNodePtr GetAdvancedWalkSpeedCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00071"></a>00071         <a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> GetWalkSpeedValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00072"></a>00072
<a name="l00073"></a>00073         <span class="keywordtype">void</span> RemoveWalkSpeedAt(<span class="keywordtype">int</span> nIndex);
<a name="l00074"></a>00074         <span class="keywordtype">void</span> RemoveWalkSpeed();
<a name="l00075"></a>00075
<a name="l00076"></a>00076         <span class="comment">//</span>
<a name="l00077"></a>00077         <span class="comment">// decimal CountItems (1...1)</span>
<a name="l00078"></a>00078         <span class="comment">//</span>
<a name="l00079"></a>00079         <span class="keyword">static</span> <span class="keywordtype">int</span> GetCountItemsMinCount();
<a name="l00080"></a>00080         <span class="keyword">static</span> <span class="keywordtype">int</span> GetCountItemsMaxCount();
<a name="l00081"></a>00081         <span class="keywordtype">int</span> GetCountItemsCount();
<a name="l00082"></a>00082         <span class="keywordtype">bool</span> HasCountItems();
<a name="l00083"></a>00083         <span class="keywordtype">void</span> AddCountItems(<a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> CountItems);
<a name="l00084"></a>00084         <span class="keywordtype">void</span> InsertCountItemsAt(<a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> CountItems, <span class="keywordtype">int</span> nIndex);
<a name="l00085"></a>00085         <span class="keywordtype">void</span> ReplaceCountItemsAt(<a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> CountItems, <span class="keywordtype">int</span> nIndex);
<a name="l00086"></a>00086         <a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> GetCountItemsAt(<span class="keywordtype">int</span> nIndex);
<a name="l00087"></a>00087         <a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> GetCountItems();
<a name="l00088"></a>00088         MSXML2::IXMLDOMNodePtr GetStartingCountItemsCursor();
<a name="l00089"></a>00089         MSXML2::IXMLDOMNodePtr GetAdvancedCountItemsCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00090"></a>00090         <a class="code" href="namespacealtova.php#09bfe25f704fe798bbcecce3b1a058d8">CSchemaDecimal</a> GetCountItemsValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00091"></a>00091
<a name="l00092"></a>00092         <span class="keywordtype">void</span> RemoveCountItemsAt(<span class="keywordtype">int</span> nIndex);
<a name="l00093"></a>00093         <span class="keywordtype">void</span> RemoveCountItems();
<a name="l00094"></a>00094 };
<a name="l00095"></a>00095
<a name="l00096"></a>00096
<a name="l00097"></a>00097 } <span class="comment">// end of namespace SpeedHack</span>
<a name="l00098"></a>00098
<a name="l00099"></a>00099 <span class="preprocessor">#endif // SchemaSpeedHack_CRespondType_H_INCLUDED</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
