<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ea6c3855b402b97c9537b801f064bee.php">GZone</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_261848f3f32d278f15a1eb75313f8c6c.php">modules</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_7a2a1bb64dd94cb341d79ebcc8f15747.php">CIQSamplev6</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_a6643cbeefee4f239b55fdd88ac5fbd7.php">common</a></div>
<h1>GZone/modules/CIQSamplev6/common/unit.h</h1><a href="_g_zone_2modules_2_c_i_q_samplev6_2common_2unit_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#ifndef __UNIT_H_</span>
<a name="l00002"></a>00002 <span class="preprocessor"></span><span class="preprocessor">#define __UNIT_H_</span>
<a name="l00003"></a>00003 <span class="preprocessor"></span>
<a name="l00004"></a>00004 <span class="preprocessor">#include "<a class="code" href="_g_zone_2modules_2_c_i_q_samplev6_2common_2clientgamecommands_8h.php">clientgamecommands.h</a>"</span>
<a name="l00005"></a>00005
<a name="l00006"></a>00006 <span class="keyword">class </span><a class="code" href="class_c_client_game_events.php">CClientGameEvents</a>;
<a name="l00007"></a>00007
<a name="l00008"></a>00008 <span class="keyword">class </span><a class="code" href="class_c_unit.php">CUnit</a>:<span class="keyword">public</span> ClientGameCommands::Control
<a name="l00009"></a>00009 {
<a name="l00010"></a>00010 <span class="keyword">public</span>:
<a name="l00011"></a>00011   <span class="comment">// подписаться на события</span>
<a name="l00012"></a><a class="code" href="class_c_unit.php#b4e2af9ac9de45fbc558da03afe842bd">00012</a>   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_unit.php#ad45b901e623fde9d4b5699ab82094e2">AddAdvise</a>(<a class="code" href="class_c_client_game_events.php">CClientGameEvents</a>*) {}
<a name="l00013"></a>00013   <span class="comment">// отписаться</span>
<a name="l00014"></a><a class="code" href="class_c_unit.php#79f55445d251fed7e97758f18442031c">00014</a>   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_unit.php#1583aa9c78794778b322c3c5f84bc1b4">RemoveAdvise</a>(<a class="code" href="class_c_client_game_events.php">CClientGameEvents</a>*) {};
<a name="l00015"></a>00015   <span class="comment">// получить координаты игрока</span>
<a name="l00016"></a><a class="code" href="class_c_unit.php#f9fa31c240ef8ac6fc627557ea3f9e10">00016</a>   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_unit.php#330ab859debc46ffbd2f720d69073a8c">GetUnitPos</a>(<span class="keywordtype">int</span> *x,<span class="keywordtype">int</span> *<a class="code" href="pathfinding_8cpp.php#ab9e199ab34637bb8176d43909336d34">y</a>) {};
<a name="l00017"></a>00017 };
<a name="l00018"></a>00018
<a name="l00019"></a>00019 <span class="preprocessor">#endif</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
