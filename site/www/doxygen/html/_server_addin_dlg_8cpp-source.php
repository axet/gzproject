<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_d2d383d4738f8a137bd616c8820954cc.php">GZServerLoader</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_6831ac409d0ad28d9ec47f35495a264d.php">GZServerConfig</a></div>
<h1>ServerAddinDlg.cpp</h1><a href="_server_addin_dlg_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// ServerAddinDlg.cpp : implementation file</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="preprocessor">#include "<a class="code" href="_g_z_2_g_z_server_loader_2_g_z_server_config_2_std_afx_8h.php">stdafx.h</a>"</span>
<a name="l00005"></a>00005 <span class="preprocessor">#include "<a class="code" href="_g_z_server_addin_8h.php">gzServeraddin.h</a>"</span>
<a name="l00006"></a>00006 <span class="preprocessor">#include "<a class="code" href="_g_z_administrator_app_8h.php">gzadministratorapp.h</a>"</span>
<a name="l00007"></a>00007 <span class="preprocessor">#include "<a class="code" href="_server_addin_dlg_8h.php">ServerAddinDlg.h</a>"</span>
<a name="l00008"></a>00008 <span class="preprocessor">#include "<a class="code" href="_server_addin_dlg_8h.php">.\serveraddindlg.h</a>"</span>
<a name="l00009"></a>00009
<a name="l00010"></a>00010 <span class="preprocessor">#include &lt;io.h&gt;</span>
<a name="l00011"></a>00011 <span class="preprocessor">#include &lt;direct.h&gt;</span>
<a name="l00012"></a>00012
<a name="l00013"></a>00013 <span class="keyword">extern</span> <a class="code" href="class_c_g_z_administrator_app.php">CGZAdministratorApp</a> <a class="code" href="_g_z_administrator_app_8cpp.php#0e1be92c3adfe51b005fbd619630f593">g_app</a>;
<a name="l00014"></a>00014
<a name="l00015"></a>00015 <span class="comment">// ServerAddinDlg dialog</span>
<a name="l00016"></a>00016
<a name="l00017"></a>00017 <a class="code" href="scbarcf_8cpp.php#b3489d3192e2a1a0ad6f8d8f3674b433">IMPLEMENT_DYNAMIC</a>(<a class="code" href="class_server_addin_dlg.php">ServerAddinDlg</a>, CDialog)
<a name="l00018"></a><a class="code" href="class_server_addin_dlg.php#a8838b4128be63233725da1d3eda4509">00018</a> <a class="code" href="class_server_addin_dlg.php">ServerAddinDlg</a>::<a class="code" href="class_server_addin_dlg.php">ServerAddinDlg</a>(CWnd* pParent <span class="comment">/*=NULL*/</span>)
<a name="l00019"></a>00019         : CDialog(<a class="code" href="class_server_addin_dlg.php">ServerAddinDlg</a>::IDD, pParent)
<a name="l00020"></a>00020   , m_port(0)
<a name="l00021"></a>00021 {
<a name="l00022"></a>00022 }
<a name="l00023"></a>00023
<a name="l00024"></a><a class="code" href="class_server_addin_dlg.php#908877ea9a7bc4e2353a1084975fa0f7">00024</a> <a class="code" href="class_server_addin_dlg.php#908877ea9a7bc4e2353a1084975fa0f7">ServerAddinDlg::~ServerAddinDlg</a>()
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 }
<a name="l00027"></a>00027
<a name="l00028"></a><a class="code" href="class_server_addin_dlg.php#e399a836b6390d95623594164db08e01">00028</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#e399a836b6390d95623594164db08e01">ServerAddinDlg::DoDataExchange</a>(CDataExchange* pDX)
<a name="l00029"></a>00029 {
<a name="l00030"></a>00030   CDialog::DoDataExchange(pDX);
<a name="l00031"></a>00031   DDX_Control(pDX, <a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#cbe20d5aa5875e8f927fad66b7610c09">IDC_COMBO1</a>, <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>);
<a name="l00032"></a>00032   DDX_Text(pDX, <a class="code" href="_g_z_2_g_z_server_loader_2_g_z_server_config_2resource_8h.php#4986e0f1fb11d85d51319b296a8423e1">IDC_EDIT_PORT</a>, <a class="code" href="class_server_addin_dlg.php#d8327ee2c48abc9b19493f8a609923d4">m_port</a>);
<a name="l00033"></a>00033 }
<a name="l00034"></a>00034
<a name="l00035"></a>00035
<a name="l00036"></a>00036 BEGIN_MESSAGE_MAP(<a class="code" href="class_server_addin_dlg.php">ServerAddinDlg</a>, CDialog)
<a name="l00037"></a>00037   ON_BN_CLICKED(IDOK, OnBnClickedOk)
<a name="l00038"></a>00038   ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
<a name="l00039"></a>00039   ON_BN_CLICKED(<a class="code" href="_g_z_2_g_z_server_loader_2_g_z_server_config_2resource_8h.php#d6984b0376b26b4662366f6de187cf6f">IDC_BUTTON_UNLOAD</a>, OnBnClickedButtonUnload)
<a name="l00040"></a>00040   ON_CBN_DROPDOWN(<a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#cbe20d5aa5875e8f927fad66b7610c09">IDC_COMBO1</a>, OnCbnDropdownCombo1)
<a name="l00041"></a>00041   ON_CBN_SELENDOK(IDC_COMBO1, OnCbnSelendokCombo1)
<a name="l00042"></a>00042   ON_BN_CLICKED(<a class="code" href="_g_z_2_g_z_server_loader_2_g_z_server_config_2resource_8h.php#6dfd412983ff5617e5ae37d7356d25a8">IDC_BUTTON_SAVE</a>, OnBnClickedButtonSave)
<a name="l00043"></a>00043   ON_BN_CLICKED(<a class="code" href="_g_z_2_g_zone_2resource_8h.php#a292b478b0d4035010ef8d22f895e207">IDC_BUTTON_DELETE</a>, OnBnClickedButtonDelete)
<a name="l00044"></a>00044   ON_BN_CLICKED(<a class="code" href="_g_z_2_g_z_server_loader_2_g_z_server_config_2resource_8h.php#e30ea03b329cfab4ca5d38fb3faeb13f">IDC_BUTTON_LOAD</a>, OnBnClickedButtonLoad)
<a name="l00045"></a>00045   ON_WM_CLOSE()
<a name="l00046"></a>00046 END_MESSAGE_MAP()
<a name="l00047"></a>00047
<a name="l00048"></a>00048
<a name="l00049"></a>00049 <span class="comment">// ServerAddinDlg message handlers</span>
<a name="l00050"></a>00050
<a name="l00051"></a><a class="code" href="class_server_addin_dlg.php#710f3930fb3fab55f5283a34367013bb">00051</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php">ServerAddinDlg</a>::OnBnClickedOk()
<a name="l00052"></a>00052 {
<a name="l00053"></a>00053   <span class="keywordflow">try</span>
<a name="l00054"></a>00054   {
<a name="l00055"></a>00055     Save();
<a name="l00056"></a>00056     CWaitCursor wc;
<a name="l00057"></a>00057     <span class="keywordflow">if</span>(<a class="code" href="_g_z_administrator_app_8cpp.php#0e1be92c3adfe51b005fbd619630f593">g_app</a>.<a class="code" href="class_c_g_z_administrator_app.php#958687c8aade73252b1cd40f6dadcdac">ServerAttach</a>()==0)
<a name="l00058"></a>00058       <span class="keywordflow">return</span>;
<a name="l00059"></a>00059   }<span class="keywordflow">catch</span>(std::exception &amp;e)
<a name="l00060"></a>00060   {
<a name="l00061"></a>00061     AfxMessageBox(e.what());
<a name="l00062"></a>00062     <span class="keywordflow">return</span>;
<a name="l00063"></a>00063   }
<a name="l00064"></a>00064
<a name="l00065"></a>00065   OnOK();
<a name="l00066"></a>00066 }
<a name="l00067"></a>00067
<a name="l00068"></a><a class="code" href="class_server_addin_dlg.php#4e26a84ad1353b3829f4038d2dbdcb8f">00068</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#4e26a84ad1353b3829f4038d2dbdcb8f">ServerAddinDlg::OnBnClickedCancel</a>()
<a name="l00069"></a>00069 {
<a name="l00070"></a>00070   <a class="code" href="class_server_addin_dlg.php#96b89ada398dd65626a2dded94ffcbad">Save</a>();
<a name="l00071"></a>00071   OnCancel();
<a name="l00072"></a>00072 }
<a name="l00073"></a>00073
<a name="l00074"></a><a class="code" href="class_server_addin_dlg.php#5e9fc443646de1f1856763b134145916">00074</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#5e9fc443646de1f1856763b134145916">ServerAddinDlg::OnBnClickedButtonUnload</a>()
<a name="l00075"></a>00075 {
<a name="l00076"></a>00076   <span class="keywordflow">try</span>
<a name="l00077"></a>00077   {
<a name="l00078"></a>00078     CWaitCursor wc;
<a name="l00079"></a>00079     <a class="code" href="_g_z_administrator_app_8cpp.php#0e1be92c3adfe51b005fbd619630f593">g_app</a>.<a class="code" href="class_c_g_z_administrator_app.php#31983149c4e8673eaddbd9701aff5c69">ServerDetach</a>();
<a name="l00080"></a>00080   }<span class="keywordflow">catch</span>(std::exception &amp;e)
<a name="l00081"></a>00081   {
<a name="l00082"></a>00082     AfxMessageBox(e.what());
<a name="l00083"></a>00083   }
<a name="l00084"></a>00084 }
<a name="l00085"></a>00085
<a name="l00086"></a><a class="code" href="class_server_addin_dlg.php#f543551d993fc35c9ba99ad8d11026ac">00086</a> BOOL <a class="code" href="class_server_addin_dlg.php#f543551d993fc35c9ba99ad8d11026ac">ServerAddinDlg::OnInitDialog</a>()
<a name="l00087"></a>00087 {
<a name="l00088"></a>00088   CDialog::OnInitDialog();
<a name="l00089"></a>00089
<a name="l00090"></a>00090   <a class="code" href="class_server_addin_dlg.php#7213ea86ce3a5f5dc99c80235d727122">m_hIcon</a> = AfxGetApp()-&gt;LoadIcon(<a class="code" href="_g_z_2client_2test_server_crypt_2resource_8h.php#9772c84d39896ad00b9aeb34b15d324d">IDR_MAINFRAME</a>);
<a name="l00091"></a>00091         SetIcon(<a class="code" href="class_server_addin_dlg.php#7213ea86ce3a5f5dc99c80235d727122">m_hIcon</a>, TRUE);                 <span class="comment">// Set big icon</span>
<a name="l00092"></a>00092         SetIcon(<a class="code" href="class_server_addin_dlg.php#7213ea86ce3a5f5dc99c80235d727122">m_hIcon</a>, FALSE);                <span class="comment">// Set small icon</span>
<a name="l00093"></a>00093
<a name="l00094"></a>00094   <span class="keywordflow">try</span>
<a name="l00095"></a>00095   {
<a name="l00096"></a>00096     <a class="code" href="class_server_addin_dlg.php#4b7863a1608b873de3edf49c63f40f6a">Load</a>();
<a name="l00097"></a>00097
<a name="l00098"></a>00098     CString str;
<a name="l00099"></a>00099     <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.GetWindowText(str);
<a name="l00100"></a>00100     <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.SelectString(-1,str);
<a name="l00101"></a>00101     <a class="code" href="class_server_addin_dlg.php#50815ebd94815b9b46dd0e3652bb41ec">OnCbnSelendokCombo1</a>();
<a name="l00102"></a>00102   }<span class="keywordflow">catch</span>(std::exception &amp;e)
<a name="l00103"></a>00103   {
<a name="l00104"></a>00104     AfxMessageBox(e.what());
<a name="l00105"></a>00105   }
<a name="l00106"></a>00106
<a name="l00107"></a>00107   <span class="keywordflow">return</span> TRUE;  <span class="comment">// return TRUE unless you set the focus to a control</span>
<a name="l00108"></a>00108   <span class="comment">// EXCEPTION: OCX Property Pages should return FALSE</span>
<a name="l00109"></a>00109 }
<a name="l00110"></a>00110
<a name="l00111"></a><a class="code" href="class_server_addin_dlg.php#4b7863a1608b873de3edf49c63f40f6a">00111</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#4b7863a1608b873de3edf49c63f40f6a">ServerAddinDlg::Load</a>()
<a name="l00112"></a>00112 {
<a name="l00113"></a>00113   <a class="code" href="class_server_addin_dlg.php#90d1bda3445d5fbad21a8fa2824075f4">m_configpath</a>=<span class="stringliteral">"GZServerConfig.config"</span>;
<a name="l00114"></a>00114   <span class="keywordflow">if</span>(access(<a class="code" href="class_server_addin_dlg.php#90d1bda3445d5fbad21a8fa2824075f4">m_configpath</a>.c_str(),0)!=-1)
<a name="l00115"></a>00115   {
<a name="l00116"></a>00116     <a class="code" href="class_server_addin_dlg.php#d8bd6daf0a8cce1a0ab1590f2e78c029">m_config</a>=<a class="code" href="class_server_addin_dlg.php#d396d84e93935e0fe180610712a1c22a">m_doc</a>.Load(<a class="code" href="class_server_addin_dlg.php#90d1bda3445d5fbad21a8fa2824075f4">m_configpath</a>);
<a name="l00117"></a>00117     <span class="keywordflow">for</span>(<span class="keywordtype">int</span> i=0;i&lt;<a class="code" href="class_server_addin_dlg.php#d8bd6daf0a8cce1a0ab1590f2e78c029">m_config</a>.<a class="code" href="classsc_1_1_c_config_type.php#a5aa0d75be03ca96edae1a1468f4e783">GetServerPathCount</a>();i++)
<a name="l00118"></a>00118     {
<a name="l00119"></a>00119       <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.AddString(((<a class="code" href="_altova_8h.php#e5d59c80151fb6bca8c486086a1ed05b">tstring</a>)<a class="code" href="class_server_addin_dlg.php#d8bd6daf0a8cce1a0ab1590f2e78c029">m_config</a>.<a class="code" href="classsc_1_1_c_config_type.php#bd057012a9a6d4194909292c1323bf02">GetServerPathAt</a>(i)).c_str());
<a name="l00120"></a>00120     }
<a name="l00121"></a>00121     <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.SetWindowText(((<a class="code" href="_altova_8h.php#e5d59c80151fb6bca8c486086a1ed05b">tstring</a>)<a class="code" href="class_server_addin_dlg.php#d8bd6daf0a8cce1a0ab1590f2e78c029">m_config</a>.<a class="code" href="classsc_1_1_c_config_type.php#9ead39abe504e604b4e993a698fd3824">GetActiveServer</a>()).c_str());
<a name="l00122"></a>00122   }<span class="keywordflow">else</span>
<a name="l00123"></a>00123     <a class="code" href="class_server_addin_dlg.php#d396d84e93935e0fe180610712a1c22a">m_doc</a>.SetRootElementName(<span class="stringliteral">""</span>,<span class="stringliteral">"Config"</span>);
<a name="l00124"></a>00124
<a name="l00125"></a>00125   CString str;
<a name="l00126"></a>00126   <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.GetWindowText(str);
<a name="l00127"></a>00127   <span class="comment">//m_serverlist.ResetContent();</span>
<a name="l00128"></a>00128   <span class="keywordtype">int</span> i=<a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.FindString(-1,<span class="stringliteral">"Browse.."</span>);
<a name="l00129"></a>00129   <span class="keywordflow">if</span>(i!=-1)
<a name="l00130"></a>00130     <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.DeleteString(i);
<a name="l00131"></a>00131   <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.InsertString(-1,<span class="stringliteral">"Browse.."</span>);
<a name="l00132"></a>00132   <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.SetWindowText(str);
<a name="l00133"></a>00133 }
<a name="l00134"></a>00134
<a name="l00135"></a><a class="code" href="class_server_addin_dlg.php#102f876fce7e5b56a558d58b8b323cd4">00135</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#102f876fce7e5b56a558d58b8b323cd4">ServerAddinDlg::OnCbnDropdownCombo1</a>()
<a name="l00136"></a>00136 {
<a name="l00137"></a>00137 }
<a name="l00138"></a>00138
<a name="l00139"></a><a class="code" href="class_server_addin_dlg.php#50815ebd94815b9b46dd0e3652bb41ec">00139</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#50815ebd94815b9b46dd0e3652bb41ec">ServerAddinDlg::OnCbnSelendokCombo1</a>()
<a name="l00140"></a>00140 {
<a name="l00141"></a>00141   <a class="code" href="class_server_addin_dlg.php#d5fb9f1a6d75ccd90de7e32a81502501">SaveConfig</a>();
<a name="l00142"></a>00142
<a name="l00143"></a>00143   <span class="keywordtype">int</span> sel=<a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.GetCurSel();
<a name="l00144"></a>00144   <span class="keywordflow">if</span>(sel==-1)
<a name="l00145"></a>00145   {
<a name="l00146"></a>00146     <a class="code" href="class_server_addin_dlg.php#863ef9816af0d06fd3296a6d64860e7a">m_addinconfigpath</a>.clear();
<a name="l00147"></a>00147     <span class="keywordflow">return</span>;
<a name="l00148"></a>00148   }
<a name="l00149"></a>00149
<a name="l00150"></a>00150   CString str;
<a name="l00151"></a>00151   <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.GetLBText(sel,str);
<a name="l00152"></a>00152   <span class="keywordflow">if</span>(str==<span class="stringliteral">"Browse.."</span>)
<a name="l00153"></a>00153   {
<a name="l00154"></a>00154     CFileDialog dlg(1);
<a name="l00155"></a>00155     <span class="keywordtype">int</span> i=-1;
<a name="l00156"></a>00156     <span class="keywordflow">if</span>(dlg.DoModal()==IDOK)
<a name="l00157"></a>00157     {
<a name="l00158"></a>00158       i=<a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.FindString(-1,dlg.GetPathName());
<a name="l00159"></a>00159       <span class="keywordflow">if</span>(i==-1)
<a name="l00160"></a>00160         i=<a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.InsertString(0,dlg.GetPathName());
<a name="l00161"></a>00161     }
<a name="l00162"></a>00162
<a name="l00163"></a>00163     <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.SetCurSel(i);
<a name="l00164"></a>00164     <a class="code" href="class_server_addin_dlg.php#50815ebd94815b9b46dd0e3652bb41ec">OnCbnSelendokCombo1</a>();
<a name="l00165"></a>00165     <span class="keywordflow">return</span>;
<a name="l00166"></a>00166   }
<a name="l00167"></a>00167
<a name="l00168"></a>00168   <span class="keywordflow">try</span>
<a name="l00169"></a>00169   {
<a name="l00170"></a>00170     <a class="code" href="class_server_addin_dlg.php#7b2840316cde9ae7259730daae964c25">LoadConfig</a>(str+<span class="stringliteral">".serveraddin.config"</span>);
<a name="l00171"></a>00171   }<span class="keywordflow">catch</span>(std::exception &amp;e)
<a name="l00172"></a>00172   {
<a name="l00173"></a>00173     AfxMessageBox(e.what());
<a name="l00174"></a>00174   }
<a name="l00175"></a>00175 }
<a name="l00176"></a>00176
<a name="l00177"></a><a class="code" href="class_server_addin_dlg.php#7b2840316cde9ae7259730daae964c25">00177</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#7b2840316cde9ae7259730daae964c25">ServerAddinDlg::LoadConfig</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>*p)
<a name="l00178"></a>00178 {
<a name="l00179"></a>00179   <a class="code" href="class_server_addin_dlg.php#863ef9816af0d06fd3296a6d64860e7a">m_addinconfigpath</a>=p;
<a name="l00180"></a>00180
<a name="l00181"></a>00181   <a class="code" href="class_server_addin_dlg.php#d8327ee2c48abc9b19493f8a609923d4">m_port</a>=49049;
<a name="l00182"></a>00182
<a name="l00183"></a>00183   <span class="keywordflow">if</span>(access(p,0)!=-1)
<a name="l00184"></a>00184   {
<a name="l00185"></a>00185     <a class="code" href="class_server_addin_dlg.php#5932f683c5b99e4b3bd0cec49f18ddde">m_addinconfig</a>=<a class="code" href="class_server_addin_dlg.php#8011d44a29ce9da68503d584e55fa9cc">m_addindoc</a>.Load(p);
<a name="l00186"></a>00186     <a class="code" href="class_server_addin_dlg.php#d8327ee2c48abc9b19493f8a609923d4">m_port</a>=<a class="code" href="class_server_addin_dlg.php#5932f683c5b99e4b3bd0cec49f18ddde">m_addinconfig</a>.<a class="code" href="classssa_1_1_c_config_type.php#271d914aa602f6094511b69076050bdd">GetPort</a>();
<a name="l00187"></a>00187   }<span class="keywordflow">else</span>
<a name="l00188"></a>00188     <a class="code" href="class_server_addin_dlg.php#8011d44a29ce9da68503d584e55fa9cc">m_addindoc</a>.SetRootElementName(<span class="stringliteral">""</span>,<span class="stringliteral">"Config"</span>);
<a name="l00189"></a>00189
<a name="l00190"></a>00190   UpdateData(FALSE);
<a name="l00191"></a>00191 }
<a name="l00192"></a>00192
<a name="l00193"></a><a class="code" href="class_server_addin_dlg.php#eb35513dfed35b320a130835fb147485">00193</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#eb35513dfed35b320a130835fb147485">ServerAddinDlg::OnBnClickedButtonSave</a>()
<a name="l00194"></a>00194 {
<a name="l00195"></a>00195   <a class="code" href="class_server_addin_dlg.php#d5fb9f1a6d75ccd90de7e32a81502501">SaveConfig</a>();
<a name="l00196"></a>00196 }
<a name="l00197"></a>00197
<a name="l00198"></a><a class="code" href="class_server_addin_dlg.php#d5fb9f1a6d75ccd90de7e32a81502501">00198</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#d5fb9f1a6d75ccd90de7e32a81502501">ServerAddinDlg::SaveConfig</a>()
<a name="l00199"></a>00199 {
<a name="l00200"></a>00200   <span class="keywordflow">if</span>(<a class="code" href="class_server_addin_dlg.php#863ef9816af0d06fd3296a6d64860e7a">m_addinconfigpath</a>.empty())
<a name="l00201"></a>00201     <span class="keywordflow">return</span>;
<a name="l00202"></a>00202
<a name="l00203"></a>00203   UpdateData();
<a name="l00204"></a>00204
<a name="l00205"></a>00205   <a class="code" href="class_server_addin_dlg.php#5932f683c5b99e4b3bd0cec49f18ddde">m_addinconfig</a>.<a class="code" href="classssa_1_1_c_config_type.php#bfaa0c7b4fd0243e86b28dd0a98e3982">RemovePort</a>();
<a name="l00206"></a>00206   <a class="code" href="class_server_addin_dlg.php#5932f683c5b99e4b3bd0cec49f18ddde">m_addinconfig</a>.<a class="code" href="classssa_1_1_c_config_type.php#5cdd8396e8de1c69a4bf27c902b19e98">AddPort</a>(<a class="code" href="class_server_addin_dlg.php#d8327ee2c48abc9b19493f8a609923d4">m_port</a>);
<a name="l00207"></a>00207   <a class="code" href="class_server_addin_dlg.php#8011d44a29ce9da68503d584e55fa9cc">m_addindoc</a>.Save(<a class="code" href="class_server_addin_dlg.php#863ef9816af0d06fd3296a6d64860e7a">m_addinconfigpath</a>,<a class="code" href="class_server_addin_dlg.php#5932f683c5b99e4b3bd0cec49f18ddde">m_addinconfig</a>);
<a name="l00208"></a>00208 }
<a name="l00209"></a>00209
<a name="l00210"></a><a class="code" href="class_server_addin_dlg.php#a851b8cfd569ab455441d335dfe67d7a">00210</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#a851b8cfd569ab455441d335dfe67d7a">ServerAddinDlg::OnBnClickedButtonDelete</a>()
<a name="l00211"></a>00211 {
<a name="l00212"></a>00212   <span class="keywordtype">int</span> sel=<a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.GetCurSel();
<a name="l00213"></a>00213   <span class="keywordflow">if</span>(sel==-1)
<a name="l00214"></a>00214     <span class="keywordflow">return</span>;
<a name="l00215"></a>00215   <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.DeleteString(sel);
<a name="l00216"></a>00216
<a name="l00217"></a>00217   remove(((<a class="code" href="_altova_8h.php#e5d59c80151fb6bca8c486086a1ed05b">tstring</a>)<a class="code" href="class_server_addin_dlg.php#863ef9816af0d06fd3296a6d64860e7a">m_addinconfigpath</a>).c_str());
<a name="l00218"></a>00218 }
<a name="l00219"></a>00219
<a name="l00220"></a><a class="code" href="class_server_addin_dlg.php#b12418834f01766b268246f65263af4e">00220</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#b12418834f01766b268246f65263af4e">ServerAddinDlg::OnBnClickedButtonLoad</a>()
<a name="l00221"></a>00221 {
<a name="l00222"></a>00222   <a class="code" href="class_server_addin_dlg.php#d5fb9f1a6d75ccd90de7e32a81502501">SaveConfig</a>();
<a name="l00223"></a>00223   WinExec(<span class="stringliteral">"GZServerLoader.exe"</span>,SW_SHOW);
<a name="l00224"></a>00224
<a name="l00225"></a>00225   <a class="code" href="class_server_addin_dlg.php#4e26a84ad1353b3829f4038d2dbdcb8f">OnBnClickedCancel</a>();
<a name="l00226"></a>00226 }
<a name="l00227"></a>00227
<a name="l00228"></a><a class="code" href="class_server_addin_dlg.php#4761513d6bad6e93bdef876c13b95125">00228</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#4761513d6bad6e93bdef876c13b95125">ServerAddinDlg::OnClose</a>()
<a name="l00229"></a>00229 {
<a name="l00230"></a>00230   <a class="code" href="class_server_addin_dlg.php#d5fb9f1a6d75ccd90de7e32a81502501">SaveConfig</a>();
<a name="l00231"></a>00231   <a class="code" href="class_server_addin_dlg.php#96b89ada398dd65626a2dded94ffcbad">Save</a>();
<a name="l00232"></a>00232
<a name="l00233"></a>00233   CDialog::OnClose();
<a name="l00234"></a>00234 }
<a name="l00235"></a>00235
<a name="l00236"></a><a class="code" href="class_server_addin_dlg.php#96b89ada398dd65626a2dded94ffcbad">00236</a> <span class="keywordtype">void</span> <a class="code" href="class_server_addin_dlg.php#96b89ada398dd65626a2dded94ffcbad">ServerAddinDlg::Save</a>()
<a name="l00237"></a>00237 {
<a name="l00238"></a>00238   <span class="keywordflow">if</span>(<a class="code" href="class_server_addin_dlg.php#90d1bda3445d5fbad21a8fa2824075f4">m_configpath</a>.empty())
<a name="l00239"></a>00239     <span class="keywordflow">return</span>;
<a name="l00240"></a>00240
<a name="l00241"></a>00241   <a class="code" href="class_server_addin_dlg.php#d8bd6daf0a8cce1a0ab1590f2e78c029">m_config</a>.<a class="code" href="classsc_1_1_c_config_type.php#0d2e430df5ee1bba36c0e43c9e3b45c6">RemoveServerPath</a>();
<a name="l00242"></a>00242   <span class="keywordflow">for</span>(<span class="keywordtype">int</span> i=0;i&lt;<a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.GetCount();i++)
<a name="l00243"></a>00243   {
<a name="l00244"></a>00244     CString str;
<a name="l00245"></a>00245     <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.GetLBText(i,str);
<a name="l00246"></a>00246     <a class="code" href="class_server_addin_dlg.php#d8bd6daf0a8cce1a0ab1590f2e78c029">m_config</a>.<a class="code" href="classsc_1_1_c_config_type.php#4107a8ef8065062b517500f9d4696316">AddServerPath</a>((<span class="keyword">const</span> <span class="keywordtype">char</span>*)str);
<a name="l00247"></a>00247   }
<a name="l00248"></a>00248   CString str;
<a name="l00249"></a>00249   <a class="code" href="class_server_addin_dlg.php#871a0b02a52e2579e954787c79551180">m_serverlist</a>.GetWindowText(str);
<a name="l00250"></a>00250   <a class="code" href="class_server_addin_dlg.php#d8bd6daf0a8cce1a0ab1590f2e78c029">m_config</a>.<a class="code" href="classsc_1_1_c_config_type.php#9484213585f4c0a75dfea83b34fb4b10">RemoveActiveServer</a>();
<a name="l00251"></a>00251   <a class="code" href="class_server_addin_dlg.php#d8bd6daf0a8cce1a0ab1590f2e78c029">m_config</a>.<a class="code" href="classsc_1_1_c_config_type.php#28bff05912923fb0dae981b2d3e8b96a">AddActiveServer</a>((<span class="keyword">const</span> <span class="keywordtype">char</span>*)str);
<a name="l00252"></a>00252   <a class="code" href="class_server_addin_dlg.php#d396d84e93935e0fe180610712a1c22a">m_doc</a>.Save(<a class="code" href="class_server_addin_dlg.php#90d1bda3445d5fbad21a8fa2824075f4">m_configpath</a>,<a class="code" href="class_server_addin_dlg.php#d8bd6daf0a8cce1a0ab1590f2e78c029">m_config</a>);
<a name="l00253"></a>00253 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
