<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>CGZServerAddinLoaderDlg Member List</h1>This is the complete list of members for <a class="el" href="class_c_g_z_server_addin_loader_dlg.php">CGZServerAddinLoaderDlg</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#c4780f0a8246ff1328564d06510b4879">CGZServerAddinLoaderDlg</a>(CWnd *pParent=NULL)</td><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php">CGZServerAddinLoaderDlg</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#b3ebc4f291130e87bc6a5e28d6a27fba">DoDataExchange</a>(CDataExchange *pDX)</td><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php">CGZServerAddinLoaderDlg</a></td><td><code> [protected, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#4e93fefc72398f8641d5e6e631be7cc5c7dc00f5fe2c9243dcc110327e400fee">IDD</a> enum value</td><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php">CGZServerAddinLoaderDlg</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#1386705ed7b4a8028a8e0660f60e4bdc">m_hIcon</a></td><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php">CGZServerAddinLoaderDlg</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#bb54977c2cd57539093fd9bb24a16c0b">OnInitDialog</a>()</td><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php">CGZServerAddinLoaderDlg</a></td><td><code> [protected, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#9de7e8c6d154aabbc92b299b898c11f4">OnPaint</a>()</td><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php">CGZServerAddinLoaderDlg</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#392fee5d6482e5cda9b29732690c170f">OnQueryDragIcon</a>()</td><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php">CGZServerAddinLoaderDlg</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#4b81a1535dba58c322adfdc5cb165dd5">OnSysCommand</a>(UINT nID, LPARAM lParam)</td><td><a class="el" href="class_c_g_z_server_addin_loader_dlg.php">CGZServerAddinLoaderDlg</a></td><td><code> [protected]</code></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
