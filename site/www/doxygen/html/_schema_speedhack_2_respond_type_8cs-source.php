<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_285c2ba7298296be1db6222076863471.php">GZAdministrator</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_8d48eff7f7385c6ae602f7565068492f.php">SchemaSpeedhack</a></div>
<h1>SchemaSpeedhack/RespondType.cs</h1><a href="_schema_speedhack_2_respond_type_8cs.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">//</span>
<a name="l00002"></a>00002 <span class="comment">// RespondType.cs.cs</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file was generated by XMLSPY 5 Enterprise Edition.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00007"></a>00007 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00008"></a>00008 <span class="comment">//</span>
<a name="l00009"></a>00009 <span class="comment">// Refer to the XMLSPY Documentation for further details.</span>
<a name="l00010"></a>00010 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012
<a name="l00013"></a>00013
<a name="l00014"></a>00014 <span class="keyword">using</span> System;
<a name="l00015"></a>00015 <span class="keyword">using</span> System.Xml;
<a name="l00016"></a>00016 <span class="keyword">using</span> Altova.Types;
<a name="l00017"></a>00017
<a name="l00018"></a>00018 <span class="keyword">namespace </span>SchemaSpeedHack
<a name="l00019"></a>00019 {
<a name="l00020"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php">00020</a>         <span class="keyword">public</span> <span class="keyword">class </span><a class="code" href="class_schema_speed_hack_1_1_respond_type.php">RespondType</a> : Altova.Node
<a name="l00021"></a>00021         {
<a name="l00022"></a>00022 <span class="preprocessor">                #region Forward constructors</span>
<a name="l00023"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#e09be201aedd516236643ef29cacb6c5">00023</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#e09be201aedd516236643ef29cacb6c5">RespondType</a>() : base() {}
<a name="l00024"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#da2b13f73cab8b5168a283c2be051119">00024</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#e09be201aedd516236643ef29cacb6c5">RespondType</a>(XmlDocument doc) : base(doc) {}
<a name="l00025"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#af2d477124084ae623c80a1e010494cf">00025</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#e09be201aedd516236643ef29cacb6c5">RespondType</a>(XmlNode node) : base(node) {}
<a name="l00026"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#f832ce0212ce872758ea5ab3db592366">00026</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#e09be201aedd516236643ef29cacb6c5">RespondType</a>(Altova.Node node) : base(node) {}
<a name="l00027"></a>00027 <span class="preprocessor">                #endregion // Forward constructors</span>
<a name="l00028"></a>00028 <span class="preprocessor"></span>
<a name="l00029"></a>00029 <span class="preprocessor">                #region WalkCount accessor methods</span>
<a name="l00030"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#7d079153e90e79bff04ae7957e88b564">00030</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#7d079153e90e79bff04ae7957e88b564">GetWalkCountMinCount</a>()
<a name="l00031"></a>00031                 {
<a name="l00032"></a>00032                         <span class="keywordflow">return</span> 1;
<a name="l00033"></a>00033                 }
<a name="l00034"></a>00034
<a name="l00035"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#13e89bb8695e66cf326d7b08ece61bd7">00035</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#13e89bb8695e66cf326d7b08ece61bd7">GetWalkCountMaxCount</a>()
<a name="l00036"></a>00036                 {
<a name="l00037"></a>00037                         <span class="keywordflow">return</span> 1;
<a name="l00038"></a>00038                 }
<a name="l00039"></a>00039
<a name="l00040"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#76dd3d2608c4fe1a8d5ca2e6426c8486">00040</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#76dd3d2608c4fe1a8d5ca2e6426c8486">GetWalkCountCount</a>()
<a name="l00041"></a>00041                 {
<a name="l00042"></a>00042                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#5a78f4a7e638948db52a50bf27f97277">DomChildCount</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>);
<a name="l00043"></a>00043                 }
<a name="l00044"></a>00044
<a name="l00045"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#87673ecfede14b5db47507c625924b32">00045</a>                 <span class="keyword">public</span> <span class="keywordtype">bool</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#87673ecfede14b5db47507c625924b32">HasWalkCount</a>()
<a name="l00046"></a>00046                 {
<a name="l00047"></a>00047                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#22caf29f497e86a36ca976c5b9344d11">HasDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>);
<a name="l00048"></a>00048                 }
<a name="l00049"></a>00049
<a name="l00050"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#1f9a4e63572514698eeab7aa3e5deb03">00050</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#1f9a4e63572514698eeab7aa3e5deb03">GetWalkCountAt</a>(<span class="keywordtype">int</span> index)
<a name="l00051"></a>00051                 {
<a name="l00052"></a>00052                         <span class="keywordflow">return</span> <span class="keyword">new</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a>(<a class="code" href="class_altova_1_1_node.php#3e9877a06a6e9021aecc6cf026c34f7a">GetDomNodeValue</a>(<a class="code" href="class_altova_1_1_node.php#30b0b16f6517d59c5415e7e7d8f83959">GetDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>, index)));
<a name="l00053"></a>00053                 }
<a name="l00054"></a>00054
<a name="l00055"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#039ce377c5c4f0d3de4ad0b3daf8971e">00055</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#039ce377c5c4f0d3de4ad0b3daf8971e">GetWalkCount</a>()
<a name="l00056"></a>00056                 {
<a name="l00057"></a>00057                         <span class="keywordflow">return</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#1f9a4e63572514698eeab7aa3e5deb03">GetWalkCountAt</a>(0);
<a name="l00058"></a>00058                 }
<a name="l00059"></a>00059
<a name="l00060"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#da6b27d619ce72a4760357dc9c1ed615">00060</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#da6b27d619ce72a4760357dc9c1ed615">RemoveWalkCountAt</a>(<span class="keywordtype">int</span> index)
<a name="l00061"></a>00061                 {
<a name="l00062"></a>00062                         <a class="code" href="class_altova_1_1_node.php#7938fa304858dfc0e9f8f492c9a65ecb">RemoveDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>, index);
<a name="l00063"></a>00063                 }
<a name="l00064"></a>00064
<a name="l00065"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#59975e88f2b425ec2960a0c52b1bc437">00065</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#59975e88f2b425ec2960a0c52b1bc437">RemoveWalkCount</a>()
<a name="l00066"></a>00066                 {
<a name="l00067"></a>00067                         <span class="keywordflow">while</span> (<a class="code" href="class_schema_speed_hack_1_1_respond_type.php#87673ecfede14b5db47507c625924b32">HasWalkCount</a>())
<a name="l00068"></a>00068                                 <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#da6b27d619ce72a4760357dc9c1ed615">RemoveWalkCountAt</a>(0);
<a name="l00069"></a>00069                 }
<a name="l00070"></a>00070
<a name="l00071"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#b8afc7267cbfe273d3e5c3b39179197c">00071</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#b8afc7267cbfe273d3e5c3b39179197c">AddWalkCount</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue)
<a name="l00072"></a>00072                 {
<a name="l00073"></a>00073                         <a class="code" href="class_altova_1_1_node.php#c75ea55d1cdcb9119c14c8e7598d27fd">AppendDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00074"></a>00074                 }
<a name="l00075"></a>00075
<a name="l00076"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#5797d869f5d0a633f5eedbae2ad98b2e">00076</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#5797d869f5d0a633f5eedbae2ad98b2e">InsertWalkCountAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00077"></a>00077                 {
<a name="l00078"></a>00078                         <a class="code" href="class_altova_1_1_node.php#04fbd644a61272767ac268a6837e07b8">InsertDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00079"></a>00079                 }
<a name="l00080"></a>00080
<a name="l00081"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#11a8ea2ca931095852cd9fd9d79b5bd5">00081</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#11a8ea2ca931095852cd9fd9d79b5bd5">ReplaceWalkCountAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00082"></a>00082                 {
<a name="l00083"></a>00083                         <a class="code" href="class_altova_1_1_node.php#a99e2051ebcba9f999ee5cfa19b6072f">ReplaceDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00084"></a>00084                 }
<a name="l00085"></a>00085 <span class="preprocessor">                #endregion // WalkCount accessor methods</span>
<a name="l00086"></a>00086 <span class="preprocessor"></span>
<a name="l00087"></a>00087 <span class="preprocessor">                #region WalkSpeed accessor methods</span>
<a name="l00088"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#0a9b725a85c1aa51278e8f3665cc12b6">00088</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#0a9b725a85c1aa51278e8f3665cc12b6">GetWalkSpeedMinCount</a>()
<a name="l00089"></a>00089                 {
<a name="l00090"></a>00090                         <span class="keywordflow">return</span> 1;
<a name="l00091"></a>00091                 }
<a name="l00092"></a>00092
<a name="l00093"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#3adb2e867b57349133ded2850e465548">00093</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#3adb2e867b57349133ded2850e465548">GetWalkSpeedMaxCount</a>()
<a name="l00094"></a>00094                 {
<a name="l00095"></a>00095                         <span class="keywordflow">return</span> 1;
<a name="l00096"></a>00096                 }
<a name="l00097"></a>00097
<a name="l00098"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#36d73e05a4a95f8b56abc8bc771f1fd7">00098</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#36d73e05a4a95f8b56abc8bc771f1fd7">GetWalkSpeedCount</a>()
<a name="l00099"></a>00099                 {
<a name="l00100"></a>00100                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#5a78f4a7e638948db52a50bf27f97277">DomChildCount</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>);
<a name="l00101"></a>00101                 }
<a name="l00102"></a>00102
<a name="l00103"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#9866c1498c9b62694d9530587b28f622">00103</a>                 <span class="keyword">public</span> <span class="keywordtype">bool</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#9866c1498c9b62694d9530587b28f622">HasWalkSpeed</a>()
<a name="l00104"></a>00104                 {
<a name="l00105"></a>00105                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#22caf29f497e86a36ca976c5b9344d11">HasDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>);
<a name="l00106"></a>00106                 }
<a name="l00107"></a>00107
<a name="l00108"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#97b0de003d5a5b3cba25735098fc090a">00108</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#97b0de003d5a5b3cba25735098fc090a">GetWalkSpeedAt</a>(<span class="keywordtype">int</span> index)
<a name="l00109"></a>00109                 {
<a name="l00110"></a>00110                         <span class="keywordflow">return</span> <span class="keyword">new</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a>(<a class="code" href="class_altova_1_1_node.php#3e9877a06a6e9021aecc6cf026c34f7a">GetDomNodeValue</a>(<a class="code" href="class_altova_1_1_node.php#30b0b16f6517d59c5415e7e7d8f83959">GetDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>, index)));
<a name="l00111"></a>00111                 }
<a name="l00112"></a>00112
<a name="l00113"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#4be5caa637255bb1784e7cd0fa3a2ba7">00113</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#4be5caa637255bb1784e7cd0fa3a2ba7">GetWalkSpeed</a>()
<a name="l00114"></a>00114                 {
<a name="l00115"></a>00115                         <span class="keywordflow">return</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#97b0de003d5a5b3cba25735098fc090a">GetWalkSpeedAt</a>(0);
<a name="l00116"></a>00116                 }
<a name="l00117"></a>00117
<a name="l00118"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#e53a6962e6b92373ee620d951233a48e">00118</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#e53a6962e6b92373ee620d951233a48e">RemoveWalkSpeedAt</a>(<span class="keywordtype">int</span> index)
<a name="l00119"></a>00119                 {
<a name="l00120"></a>00120                         <a class="code" href="class_altova_1_1_node.php#7938fa304858dfc0e9f8f492c9a65ecb">RemoveDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>, index);
<a name="l00121"></a>00121                 }
<a name="l00122"></a>00122
<a name="l00123"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#323bae8ae841761626ffa65d2086f304">00123</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#323bae8ae841761626ffa65d2086f304">RemoveWalkSpeed</a>()
<a name="l00124"></a>00124                 {
<a name="l00125"></a>00125                         <span class="keywordflow">while</span> (<a class="code" href="class_schema_speed_hack_1_1_respond_type.php#9866c1498c9b62694d9530587b28f622">HasWalkSpeed</a>())
<a name="l00126"></a>00126                                 <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#e53a6962e6b92373ee620d951233a48e">RemoveWalkSpeedAt</a>(0);
<a name="l00127"></a>00127                 }
<a name="l00128"></a>00128
<a name="l00129"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#c09764403f7bef54337f7bf6865e9684">00129</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#c09764403f7bef54337f7bf6865e9684">AddWalkSpeed</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue)
<a name="l00130"></a>00130                 {
<a name="l00131"></a>00131                         <a class="code" href="class_altova_1_1_node.php#c75ea55d1cdcb9119c14c8e7598d27fd">AppendDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00132"></a>00132                 }
<a name="l00133"></a>00133
<a name="l00134"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#80556768f71f2cf374a8b609ceb5c9c2">00134</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#80556768f71f2cf374a8b609ceb5c9c2">InsertWalkSpeedAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00135"></a>00135                 {
<a name="l00136"></a>00136                         <a class="code" href="class_altova_1_1_node.php#04fbd644a61272767ac268a6837e07b8">InsertDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00137"></a>00137                 }
<a name="l00138"></a>00138
<a name="l00139"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#c7a96da25302781b3a820752073261be">00139</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#c7a96da25302781b3a820752073261be">ReplaceWalkSpeedAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00140"></a>00140                 {
<a name="l00141"></a>00141                         <a class="code" href="class_altova_1_1_node.php#a99e2051ebcba9f999ee5cfa19b6072f">ReplaceDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00142"></a>00142                 }
<a name="l00143"></a>00143 <span class="preprocessor">                #endregion // WalkSpeed accessor methods</span>
<a name="l00144"></a>00144 <span class="preprocessor"></span>
<a name="l00145"></a>00145 <span class="preprocessor">                #region CountItems accessor methods</span>
<a name="l00146"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#f98fd0e3b5b0205246b36cf9c6443cea">00146</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#f98fd0e3b5b0205246b36cf9c6443cea">GetCountItemsMinCount</a>()
<a name="l00147"></a>00147                 {
<a name="l00148"></a>00148                         <span class="keywordflow">return</span> 1;
<a name="l00149"></a>00149                 }
<a name="l00150"></a>00150
<a name="l00151"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#c7a9085938a11218e171577475f0fea4">00151</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#c7a9085938a11218e171577475f0fea4">GetCountItemsMaxCount</a>()
<a name="l00152"></a>00152                 {
<a name="l00153"></a>00153                         <span class="keywordflow">return</span> 1;
<a name="l00154"></a>00154                 }
<a name="l00155"></a>00155
<a name="l00156"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#bdad39f257a530a047219ca29b9bd6b0">00156</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#bdad39f257a530a047219ca29b9bd6b0">GetCountItemsCount</a>()
<a name="l00157"></a>00157                 {
<a name="l00158"></a>00158                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#5a78f4a7e638948db52a50bf27f97277">DomChildCount</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>);
<a name="l00159"></a>00159                 }
<a name="l00160"></a>00160
<a name="l00161"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#f46d661977f61d8b92bfc3ec9e051055">00161</a>                 <span class="keyword">public</span> <span class="keywordtype">bool</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#f46d661977f61d8b92bfc3ec9e051055">HasCountItems</a>()
<a name="l00162"></a>00162                 {
<a name="l00163"></a>00163                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#22caf29f497e86a36ca976c5b9344d11">HasDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>);
<a name="l00164"></a>00164                 }
<a name="l00165"></a>00165
<a name="l00166"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#7a91f1937998b30720181a73e7a1b206">00166</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#7a91f1937998b30720181a73e7a1b206">GetCountItemsAt</a>(<span class="keywordtype">int</span> index)
<a name="l00167"></a>00167                 {
<a name="l00168"></a>00168                         <span class="keywordflow">return</span> <span class="keyword">new</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a>(<a class="code" href="class_altova_1_1_node.php#3e9877a06a6e9021aecc6cf026c34f7a">GetDomNodeValue</a>(<a class="code" href="class_altova_1_1_node.php#30b0b16f6517d59c5415e7e7d8f83959">GetDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>, index)));
<a name="l00169"></a>00169                 }
<a name="l00170"></a>00170
<a name="l00171"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#65dd9f49b7039ed1af760aa387719e35">00171</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#65dd9f49b7039ed1af760aa387719e35">GetCountItems</a>()
<a name="l00172"></a>00172                 {
<a name="l00173"></a>00173                         <span class="keywordflow">return</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#7a91f1937998b30720181a73e7a1b206">GetCountItemsAt</a>(0);
<a name="l00174"></a>00174                 }
<a name="l00175"></a>00175
<a name="l00176"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#ac6d2f686110f43e10e3b226a601d961">00176</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#ac6d2f686110f43e10e3b226a601d961">RemoveCountItemsAt</a>(<span class="keywordtype">int</span> index)
<a name="l00177"></a>00177                 {
<a name="l00178"></a>00178                         <a class="code" href="class_altova_1_1_node.php#7938fa304858dfc0e9f8f492c9a65ecb">RemoveDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>, index);
<a name="l00179"></a>00179                 }
<a name="l00180"></a>00180
<a name="l00181"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#d6550b1db3ccb1e1e976afa412d84d01">00181</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#d6550b1db3ccb1e1e976afa412d84d01">RemoveCountItems</a>()
<a name="l00182"></a>00182                 {
<a name="l00183"></a>00183                         <span class="keywordflow">while</span> (<a class="code" href="class_schema_speed_hack_1_1_respond_type.php#f46d661977f61d8b92bfc3ec9e051055">HasCountItems</a>())
<a name="l00184"></a>00184                                 <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#ac6d2f686110f43e10e3b226a601d961">RemoveCountItemsAt</a>(0);
<a name="l00185"></a>00185                 }
<a name="l00186"></a>00186
<a name="l00187"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#72660007552d03001ed9300b3dc9ac45">00187</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#72660007552d03001ed9300b3dc9ac45">AddCountItems</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue)
<a name="l00188"></a>00188                 {
<a name="l00189"></a>00189                         <a class="code" href="class_altova_1_1_node.php#c75ea55d1cdcb9119c14c8e7598d27fd">AppendDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00190"></a>00190                 }
<a name="l00191"></a>00191
<a name="l00192"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#f02f101e3da286d86f84086cfb3a2d01">00192</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#f02f101e3da286d86f84086cfb3a2d01">InsertCountItemsAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00193"></a>00193                 {
<a name="l00194"></a>00194                         <a class="code" href="class_altova_1_1_node.php#04fbd644a61272767ac268a6837e07b8">InsertDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00195"></a>00195                 }
<a name="l00196"></a>00196
<a name="l00197"></a><a class="code" href="class_schema_speed_hack_1_1_respond_type.php#1c73fe0a76872ba6b8c53813e687fa85">00197</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_respond_type.php#1c73fe0a76872ba6b8c53813e687fa85">ReplaceCountItemsAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00198"></a>00198                 {
<a name="l00199"></a>00199                         <a class="code" href="class_altova_1_1_node.php#a99e2051ebcba9f999ee5cfa19b6072f">ReplaceDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00200"></a>00200                 }
<a name="l00201"></a>00201 <span class="preprocessor">                #endregion // CountItems accessor methods</span>
<a name="l00202"></a>00202 <span class="preprocessor"></span>        }
<a name="l00203"></a>00203 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
