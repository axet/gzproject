<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>Debugger Class Reference</h1><!-- doxytag: class="Debugger" --><!-- doxytag: inherits="Debug" --><code>#include &lt;<a class="el" href="_debugger_router_8h-source.php">DebuggerRouter.h</a>&gt;</code>
<p>
<p>Inheritance diagram for Debugger:
<p><center><img src="class_debugger.png" usemap="#Debugger_map" border="0" alt=""></center>
<map name="Debugger_map">
<area href="class_debug.php" alt="Debug" shape="rect" coords="0,0,68,24">
</map>
<a href="class_debugger-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_debugger.php#2b76be38cdcef612b0f307758c5ea17a">Debugger</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_debugger.php#df9970eb4cb515dc87f8206c58eb9b4d">OnFinalMessage</a> (HWND hWnd)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">при получении сообщения о завершении работы, прячим иконку из трея.  <a href="#df9970eb4cb515dc87f8206c58eb9b4d"></a><br></td></tr>
<tr><td colspan="2"><br><h2>Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_debugger_router.php">DebuggerRouter</a> *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_debugger.php#cef3c21801163b7dccb4d84b5d93cec8">m_dr</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_debugger_router_8h-source.php#l00027">27</a> of file <a class="el" href="_debugger_router_8h-source.php">DebuggerRouter.h</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="2b76be38cdcef612b0f307758c5ea17a"></a><!-- doxytag: member="Debugger::Debugger" ref="2b76be38cdcef612b0f307758c5ea17a" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">Debugger::Debugger           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="df9970eb4cb515dc87f8206c58eb9b4d"></a><!-- doxytag: member="Debugger::OnFinalMessage" ref="df9970eb4cb515dc87f8206c58eb9b4d" args="(HWND hWnd)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual void Debugger::OnFinalMessage           </td>
          <td>(</td>
          <td class="paramtype">HWND&nbsp;</td>
          <td class="paramname"> <em>hWnd</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
при получении сообщения о завершении работы, прячим иконку из трея.
<p>

<p>
Reimplemented from <a class="el" href="class_debug.php#93e88932b69876de978ba2a4e21a90ca">Debug</a>.
</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="cef3c21801163b7dccb4d84b5d93cec8"></a><!-- doxytag: member="Debugger::m_dr" ref="cef3c21801163b7dccb4d84b5d93cec8" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_debugger_router.php">DebuggerRouter</a>* <a class="el" href="class_debugger.php#cef3c21801163b7dccb4d84b5d93cec8">Debugger::m_dr</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_router_8h-source.php#l00030">30</a> of file <a class="el" href="_debugger_router_8h-source.php">DebuggerRouter.h</a>.
<p>
Referenced by <a class="el" href="_debugger_router_8cpp-source.php#l00028">OnFinalMessage()</a>.
</div>
</div><p>
<hr>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="_debugger_router_8h-source.php">DebuggerRouter.h</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
