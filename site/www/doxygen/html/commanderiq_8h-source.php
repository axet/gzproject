<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ea6c3855b402b97c9537b801f064bee.php">GZone</a></div>
<h1>commanderiq.h</h1><a href="commanderiq_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// GZProject, Ultima Online utils.</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="comment">// This program is free software; you can redistribute it and/or</span>
<a name="l00005"></a>00005 <span class="comment">// modify it under the terms of the GNU General Public License</span>
<a name="l00006"></a>00006 <span class="comment">// as published by the Free Software Foundation; either version 2</span>
<a name="l00007"></a>00007 <span class="comment">// of the License, or (at your option) any later version.</span>
<a name="l00008"></a>00008
<a name="l00009"></a>00009 <span class="comment">// This program is distributed in the hope that it will be useful,</span>
<a name="l00010"></a>00010 <span class="comment">// but WITHOUT ANY WARRANTY; without even the implied warranty of</span>
<a name="l00011"></a>00011 <span class="comment">// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the</span>
<a name="l00012"></a>00012 <span class="comment">// GNU General Public License for more details.</span>
<a name="l00013"></a>00013
<a name="l00014"></a>00014 <span class="comment">// You should have received a copy of the GNU General Public License</span>
<a name="l00015"></a>00015 <span class="comment">// along with this program; if not, write to the Free Software</span>
<a name="l00016"></a>00016 <span class="comment">// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</span>
<a name="l00017"></a>00017
<a name="l00018"></a>00018 <span class="preprocessor">#ifndef COMMANDERIQ_H</span>
<a name="l00019"></a>00019 <span class="preprocessor"></span><span class="preprocessor">#define COMMANDERIQ_H</span>
<a name="l00020"></a>00020 <span class="preprocessor"></span>
<a name="l00021"></a>00021 <span class="preprocessor">#include "<a class="code" href="gamemap_8h.php">gamemap.h</a>"</span>
<a name="l00022"></a>00022
<a name="l00023"></a>00023 <span class="preprocessor">#include &lt;wtypes.h&gt;</span>
<a name="l00024"></a>00024 <span class="preprocessor">#include &lt;string&gt;</span>
<a name="l00025"></a>00025
<a name="l00026"></a>00026 <span class="keyword">class </span><a class="code" href="class_c_unit.php">CUnit</a>;
<a name="l00027"></a>00027
<a name="l00028"></a>00028
<a name="l00029"></a>00029 <span class="keyword">class </span><a class="code" href="class_c_commander_i_q_events.php">CCommanderIQEvents</a>;
<a name="l00030"></a>00030
<a name="l00032"></a>00032
<a name="l00038"></a>00038
<a name="l00039"></a><a class="code" href="class_c_commander_i_q.php">00039</a> <span class="keyword">class </span><a class="code" href="class_c_commander_i_q.php">CCommanderIQ</a>: <span class="keyword">public</span> <a class="code" href="class_c_game_map_events.php">CGameMapEvents</a>
<a name="l00040"></a>00040 {
<a name="l00041"></a>00041 <span class="keyword">public</span>:
<a name="l00042"></a><a class="code" href="class_c_commander_i_q.php#734e903064642bc183c2db68c5456ee1">00042</a>   <span class="keyword">virtual</span> <a class="code" href="class_c_commander_i_q.php#734e903064642bc183c2db68c5456ee1">~CCommanderIQ</a>() {};
<a name="l00043"></a>00043
<a name="l00044"></a>00044   <span class="comment">// работа с интерфейсом пользователя (человека)</span>
<a name="l00045"></a>00045
<a name="l00047"></a>00047
<a name="l00051"></a><a class="code" href="class_c_commander_i_q.php#757d350730cb8529c5c913cf6336e1ad">00051</a>   <span class="keyword">virtual</span> HWND <a class="code" href="class_c_commander_i_q.php#757d350730cb8529c5c913cf6336e1ad">FormCreate</a>(HWND parent,<a class="code" href="class_c_game_map.php">CGameMap</a>*,<a class="code" href="class_c_commander_i_q_events.php">CCommanderIQEvents</a>*) {<span class="keywordflow">return</span> 0;}
<a name="l00053"></a><a class="code" href="class_c_commander_i_q.php#57cd73128e5c836ff85ee57eca16f0f1">00053</a>   <span class="keyword">virtual</span> HWND <a class="code" href="class_c_commander_i_q.php#57cd73128e5c836ff85ee57eca16f0f1">FormGet</a>() {<span class="keywordflow">return</span> 0;}
<a name="l00055"></a><a class="code" href="class_c_commander_i_q.php#b5a0a80ec53393be9b58845e55c2a10d">00055</a>   <span class="keyword">virtual</span> <span class="keyword">const</span> <span class="keywordtype">char</span>* <a class="code" href="class_c_commander_i_q.php#b5a0a80ec53393be9b58845e55c2a10d">FormGetName</a>() {<span class="keywordflow">return</span> 0;}
<a name="l00056"></a>00056
<a name="l00057"></a>00057   <span class="comment">// работа с персонажами</span>
<a name="l00058"></a>00058
<a name="l00060"></a><a class="code" href="class_c_commander_i_q.php#61dc89e112f3d9790974840e02e8a5c2">00060</a>   <span class="keyword">virtual</span> <span class="keywordtype">bool</span> <a class="code" href="class_c_commander_i_q.php#61dc89e112f3d9790974840e02e8a5c2">UnitAdd</a>(<a class="code" href="class_c_unit.php">CUnit</a>* ) {<span class="keywordflow">return</span> <span class="keyword">false</span>;}
<a name="l00062"></a><a class="code" href="class_c_commander_i_q.php#90c048c81a71c748686ae8f22aa97a8d">00062</a>   <span class="keyword">virtual</span> <span class="keywordtype">bool</span> <a class="code" href="class_c_commander_i_q.php#90c048c81a71c748686ae8f22aa97a8d">UnitRemove</a>(<a class="code" href="class_c_unit.php">CUnit</a>* ) {<span class="keywordflow">return</span> <span class="keyword">false</span>;}
<a name="l00064"></a><a class="code" href="class_c_commander_i_q.php#ea5ec0c8b73c38bb188c9013824abac8">00064</a>   <span class="keyword">virtual</span> <span class="keywordtype">int</span> <a class="code" href="class_c_commander_i_q.php#ea5ec0c8b73c38bb188c9013824abac8">UnitGetCount</a>() {<span class="keywordflow">return</span> 0;}
<a name="l00066"></a><a class="code" href="class_c_commander_i_q.php#8a7fb5cc62b2aa54abd56bd285fc5b74">00066</a>   <span class="keyword">virtual</span> <a class="code" href="class_c_unit.php">CUnit</a>* <a class="code" href="class_c_commander_i_q.php#8a7fb5cc62b2aa54abd56bd285fc5b74">UnitGet</a>(<span class="keywordtype">int</span> index) {<span class="keywordflow">return</span> 0;}
<a name="l00068"></a><a class="code" href="class_c_commander_i_q.php#b1064a59c8f0e8689747e32b58c123da">00068</a>   <span class="keyword">virtual</span> <span class="keyword">const</span> <span class="keywordtype">char</span>* <a class="code" href="class_c_commander_i_q.php#b1064a59c8f0e8689747e32b58c123da">UnitMissing</a>() {<span class="keywordflow">return</span> 0;};
<a name="l00069"></a>00069
<a name="l00070"></a>00070   <span class="comment">// обмен данными</span>
<a name="l00071"></a>00071
<a name="l00073"></a><a class="code" href="class_c_commander_i_q.php#116d4cf73374db3f01ccbde445904339">00073</a>   <span class="keyword">virtual</span> std::string <a class="code" href="class_c_commander_i_q.php#116d4cf73374db3f01ccbde445904339">Save</a>() {<span class="keywordflow">return</span> std::string();}
<a name="l00075"></a><a class="code" href="class_c_commander_i_q.php#bd88af56f5e79bf67cd8dbb9d0599778">00075</a>   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_commander_i_q.php#bd88af56f5e79bf67cd8dbb9d0599778">Load</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>*) {}
<a name="l00076"></a>00076 };
<a name="l00077"></a>00077
<a name="l00079"></a>00079
<a name="l00080"></a><a class="code" href="class_c_commander_i_q_events.php">00080</a> <span class="keyword">class </span><a class="code" href="class_c_commander_i_q_events.php">CCommanderIQEvents</a>
<a name="l00081"></a>00081 {
<a name="l00082"></a>00082 <span class="keyword">public</span>:
<a name="l00085"></a>00085   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_commander_i_q_events.php#ffafeafca32dd53b6f1586c16087742b">SetModified</a>(<span class="keywordtype">bool</span> b = <span class="keyword">true</span>) = 0;
<a name="l00086"></a>00086 };
<a name="l00087"></a>00087
<a name="l00088"></a>00088 <span class="preprocessor">#endif</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
