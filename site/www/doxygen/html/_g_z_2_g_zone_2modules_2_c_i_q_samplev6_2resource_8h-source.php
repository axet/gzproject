<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ea6c3855b402b97c9537b801f064bee.php">GZone</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_261848f3f32d278f15a1eb75313f8c6c.php">modules</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_7a2a1bb64dd94cb341d79ebcc8f15747.php">CIQSamplev6</a></div>
<h1>GZ/GZone/modules/CIQSamplev6/resource.h</h1><a href="_g_z_2_g_zone_2modules_2_c_i_q_samplev6_2resource_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">//{{NO_DEPENDENCIES}}</span>
<a name="l00002"></a>00002 <span class="comment">// Microsoft Developer Studio generated include file.</span>
<a name="l00003"></a>00003 <span class="comment">// Used by CIQSample.rc</span>
<a name="l00004"></a>00004 <span class="comment">//</span>
<a name="l00005"></a><a class="code" href="_g_z_2_g_zone_2modules_2_c_i_q_samplev6_2resource_8h.php#0812789b9f24aae63de58c385da2ffde">00005</a> <span class="preprocessor">#define IDS_STRING_START                1</span>
<a name="l00006"></a><a class="code" href="_g_z_2_g_zone_2modules_2_c_i_q_samplev6_2resource_8h.php#e3ea61dc86d3e99200347af362ec98c4">00006</a> <span class="preprocessor"></span><span class="preprocessor">#define IDS_STRING_STOP                 2</span>
<a name="l00007"></a><a class="code" href="_g_z_2_g_zone_2modules_2_c_i_q_samplev6_2resource_8h.php#41697b4ed1385d083ba06b794c84e01c">00007</a> <span class="preprocessor"></span><span class="preprocessor">#define IDD_DIALOG_MAINDLG              2000</span>
<a name="l00008"></a><a class="code" href="_g_z_2_g_zone_2modules_2_c_i_q_samplev6_2resource_8h.php#fc8b055c4321e6e1869cfb337cc65712">00008</a> <span class="preprocessor"></span><span class="preprocessor">#define IDC_BUTTON_START                2000</span>
<a name="l00009"></a><a class="code" href="_g_z_2_g_zone_2modules_2_c_i_q_samplev6_2resource_8h.php#6161f96827dba07cc98ef87015d91ed4">00009</a> <span class="preprocessor"></span><span class="preprocessor">#define IDC_RICHEDIT1                   2001</span>
<a name="l00010"></a><a class="code" href="_g_z_2_g_zone_2modules_2_c_i_q_samplev6_2resource_8h.php#e38a0fbc0a040a6eded616c476c844d4">00010</a> <span class="preprocessor"></span><span class="preprocessor">#define IDC_RADIO_THREE                 2003</span>
<a name="l00011"></a><a class="code" href="_g_z_2_g_zone_2modules_2_c_i_q_samplev6_2resource_8h.php#32d4f6be27bec9afc9cfc24addc31e79">00011</a> <span class="preprocessor"></span><span class="preprocessor">#define IDC_RADIO_SIX                   2004</span>
<a name="l00012"></a><a class="code" href="_g_z_2_g_zone_2modules_2_c_i_q_samplev6_2resource_8h.php#781dc0155031990eb7eaf08e2b86ac86">00012</a> <span class="preprocessor"></span><span class="preprocessor">#define IDC_BUTTON_GENERATE             2005</span>
<a name="l00013"></a>00013 <span class="preprocessor"></span>
<a name="l00014"></a>00014 <span class="comment">// Next default values for new objects</span>
<a name="l00015"></a>00015 <span class="comment">// </span>
<a name="l00016"></a>00016 <span class="preprocessor">#ifdef APSTUDIO_INVOKED</span>
<a name="l00017"></a>00017 <span class="preprocessor"></span><span class="preprocessor">#ifndef APSTUDIO_READONLY_SYMBOLS</span>
<a name="l00018"></a>00018 <span class="preprocessor"></span><span class="preprocessor">#define _APS_NEXT_RESOURCE_VALUE        2001</span>
<a name="l00019"></a>00019 <span class="preprocessor"></span><span class="preprocessor">#define _APS_NEXT_COMMAND_VALUE         32771</span>
<a name="l00020"></a>00020 <span class="preprocessor"></span><span class="preprocessor">#define _APS_NEXT_CONTROL_VALUE         2006</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span><span class="preprocessor">#define _APS_NEXT_SYMED_VALUE           2000</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00023"></a>00023 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
