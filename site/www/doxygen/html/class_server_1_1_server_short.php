<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespace_server.php">Server</a>::<a class="el" href="class_server_1_1_server_short.php">ServerShort</a></div>
<h1>Server::ServerShort Class Reference</h1><!-- doxytag: class="Server::ServerShort" --><code>#include &lt;<a class="el" href="servertypes_8h-source.php">servertypes.h</a>&gt;</code>
<p>
<a href="class_server_1_1_server_short-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_server_1_1_server_short.php#91ed68807a5c4b6606a3699128147450">ServerShort</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_server_1_1_server_short.php#f6e190abed713b8721da211d7e88a609">ServerShort</a> (unsigned short)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_server_1_1_server_short.php#2a3d49cefb5b49fc7e42566c4bd04241">operator unsigned short</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">ServerShort</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_server_1_1_server_short.php#439fa9bea406f6952aeb3bf5a602a4d9">operator+=</a> (int)</td></tr>

<tr><td colspan="2"><br><h2>Private Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">unsigned short&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_server_1_1_server_short.php#e4fbca2520275509191e4f53216c616b">m_value</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="servertypes_8h-source.php#l00008">8</a> of file <a class="el" href="servertypes_8h-source.php">servertypes.h</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="91ed68807a5c4b6606a3699128147450"></a><!-- doxytag: member="Server::ServerShort::ServerShort" ref="91ed68807a5c4b6606a3699128147450" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">Server::ServerShort::ServerShort           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8cpp-source.php#l00177">177</a> of file <a class="el" href="packets_8cpp-source.php">packets.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00178"></a>00178 {
<a name="l00179"></a>00179   ;
<a name="l00180"></a>00180 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="f6e190abed713b8721da211d7e88a609"></a><!-- doxytag: member="Server::ServerShort::ServerShort" ref="f6e190abed713b8721da211d7e88a609" args="(unsigned short)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">Server::ServerShort::ServerShort           </td>
          <td>(</td>
          <td class="paramtype">unsigned&nbsp;</td>
          <td class="paramname"> <em>short</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8cpp-source.php#l00182">182</a> of file <a class="el" href="packets_8cpp-source.php">packets.cpp</a>.
<p>
References <a class="el" href="servertypes_8h-source.php#l00010">m_value</a>.<div class="fragment"><pre class="fragment"><a name="l00183"></a>00183 {
<a name="l00184"></a>00184   <a class="code" href="class_server_1_1_server_short.php#e4fbca2520275509191e4f53216c616b">m_value</a>=htons(s);
<a name="l00185"></a>00185 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="2a3d49cefb5b49fc7e42566c4bd04241"></a><!-- doxytag: member="Server::ServerShort::operator unsigned short" ref="2a3d49cefb5b49fc7e42566c4bd04241" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">Server::ServerShort::operator unsigned short           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8cpp-source.php#l00187">187</a> of file <a class="el" href="packets_8cpp-source.php">packets.cpp</a>.
<p>
References <a class="el" href="servertypes_8h-source.php#l00010">m_value</a>.<div class="fragment"><pre class="fragment"><a name="l00188"></a>00188 {
<a name="l00189"></a>00189   <span class="keywordflow">return</span> htons(<a class="code" href="class_server_1_1_server_short.php#e4fbca2520275509191e4f53216c616b">m_value</a>);
<a name="l00190"></a>00190 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="439fa9bea406f6952aeb3bf5a602a4d9"></a><!-- doxytag: member="Server::ServerShort::operator+=" ref="439fa9bea406f6952aeb3bf5a602a4d9" args="(int)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> &amp; Server::ServerShort::operator+=           </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8cpp-source.php#l00192">192</a> of file <a class="el" href="packets_8cpp-source.php">packets.cpp</a>.
<p>
References <a class="el" href="servertypes_8h-source.php#l00010">m_value</a>.<div class="fragment"><pre class="fragment"><a name="l00193"></a>00193 {
<a name="l00194"></a>00194   <a class="code" href="class_server_1_1_server_short.php#e4fbca2520275509191e4f53216c616b">m_value</a>=htons(htons(<a class="code" href="class_server_1_1_server_short.php#e4fbca2520275509191e4f53216c616b">m_value</a>)+i);
<a name="l00195"></a>00195   <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00196"></a>00196 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="e4fbca2520275509191e4f53216c616b"></a><!-- doxytag: member="Server::ServerShort::m_value" ref="e4fbca2520275509191e4f53216c616b" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">unsigned short <a class="el" href="class_server_1_1_server_short.php#e4fbca2520275509191e4f53216c616b">Server::ServerShort::m_value</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="servertypes_8h-source.php#l00010">10</a> of file <a class="el" href="servertypes_8h-source.php">servertypes.h</a>.
<p>
Referenced by <a class="el" href="packets_8cpp-source.php#l00187">operator unsigned short()</a>, <a class="el" href="packets_8cpp-source.php#l00192">operator+=()</a>, and <a class="el" href="packets_8cpp-source.php#l00182">ServerShort()</a>.
</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="servertypes_8h-source.php">servertypes.h</a><li><a class="el" href="packets_8cpp-source.php">packets.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
