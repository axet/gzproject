<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>ShellNofityIcon Member List</h1>This is the complete list of members for <a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#d5b31a5b77ee44a5cdca5c8d2f53e4d5">END_MSG_MAP</a>()</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#efd41806965c605d5fe4362ea1ce244a">HideIcon</a>(const Icons &amp;)</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#71c8716e3a1d78db46455017f949a5b6">Icons</a> typedef</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#274cb7fc12e3ed278cfc499f49a03815">OnCreate</a>(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &amp;bHandled)</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#76ffd6571454262d1906346fe3b41bed">OnDestory</a>(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &amp;bHandled)</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#5a66a746830e3d8b004aab23e6ffe653">OnIconMessage</a>(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &amp;bHandled)</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#b3e687b02292aedec25958d7a8ab81de">OnShellNofityIconCreate</a>()</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#d191e9e48103d87ae872bd05b72041c4">OnShellNofityIconDestroy</a>()</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#7f2b9deed5332c9c53f06285791fdd14">OnShellNofityIconProcessMessage</a>(int iconnumber, UINT msg)</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#287e86d919ef5fd42192a867c0fdc76c">OnShellNofityIconShowIcons</a>()</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#29f0ed8fffef5feca027e825746e9ea0">OnTaskBarCreated</a>(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &amp;bHandled)</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#a3d555a3be64bbc6a070f340b99efc49">ShellNofityIcon</a>()</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#5d7fa86de2b0f9b9ddb24bf2aa56fd00">ShellNofityIconCreate</a>()</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#6fe1a0f354ac509a24d91ec05aa7ee50">ShellNofityIconDestroy</a>()</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#abae668e6d96288d1729d7799413a99b">ShowIcon</a>(const Icons &amp;)</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#e4f0df816dfc3a9dc6005f05935d6aa6">UpdateIcon</a>(const Icons &amp;)</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#8c6754c9a5a0c7d83ac96d246f2d4f89">WM_TASKBARCREATED</a></td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_shell_nofity_icon.php#734d4645336723be75d66b72fc9ebc37">~ShellNofityIcon</a>()</td><td><a class="el" href="class_shell_nofity_icon.php">ShellNofityIcon</a></td><td></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
