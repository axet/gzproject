<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li id="current"><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="namespaces.php"><span>Namespace List</span></a></li>
    <li><a href="namespacemembers.php"><span>Namespace&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespacealtova.php">altova</a>::<a class="el" href="namespacealtova_1_1_i_c_mapforce_machine_util.php">ICMapforceMachineUtil</a></div>
<h1>altova::ICMapforceMachineUtil Namespace Reference</h1>
<p>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Functions</h2></td></tr>
<tr><td class="memTemplParams" nowrap colspan="2">template&lt;typename TValue&gt; </td></tr>
<tr><td class="memTemplItemLeft" nowrap align="right" valign="top">TValue&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="namespacealtova_1_1_i_c_mapforce_machine_util.php#776d6c5a5bae25e68711d4866594db02">InRangeInt</a> (TValue nValue, __int64 nMin, __int64 nMax)</td></tr>

<tr><td class="memTemplParams" nowrap colspan="2">template&lt;typename TValue&gt; </td></tr>
<tr><td class="memTemplItemLeft" nowrap align="right" valign="top">TValue&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="namespacealtova_1_1_i_c_mapforce_machine_util.php#9f846af1d1507b891463377cf57eef96">InRangeDbl</a> (TValue nValue, const double nPosMin, const double nPosMax)</td></tr>

</table>
<hr><h2>Function Documentation</h2>
<a class="anchor" name="9f846af1d1507b891463377cf57eef96"></a><!-- doxytag: member="altova::ICMapforceMachineUtil::InRangeDbl" ref="9f846af1d1507b891463377cf57eef96" args="(TValue nValue, const double nPosMin, const double nPosMax)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;typename TValue&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">TValue altova::ICMapforceMachineUtil::InRangeDbl           </td>
          <td>(</td>
          <td class="paramtype">TValue&nbsp;</td>
          <td class="paramname"> <em>nValue</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const double&nbsp;</td>
          <td class="paramname"> <em>nPosMin</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const double&nbsp;</td>
          <td class="paramname"> <em>nPosMax</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_number_8h-source.php#l00050">50</a> of file <a class="el" href="_schema_type_number_8h-source.php">SchemaTypeNumber.h</a>.
<p>
References <a class="el" href="_altova_8h-source.php#l00038">ThrowOutOfRangeError</a>.<div class="fragment"><pre class="fragment"><a name="l00051"></a>00051 {
<a name="l00052"></a>00052         TValue nTmpValue = nValue;
<a name="l00053"></a>00053         <span class="keywordflow">if</span>( nTmpValue == 0 )
<a name="l00054"></a>00054                 <span class="keywordflow">return</span> nValue;
<a name="l00055"></a>00055         <span class="keywordflow">if</span>( nTmpValue &lt; 0 )
<a name="l00056"></a>00056                 nTmpValue = -nTmpValue;
<a name="l00057"></a>00057         <span class="keywordflow">if</span>( nTmpValue &gt;= nPosMin  &amp;&amp;  nTmpValue &lt;= nPosMax )
<a name="l00058"></a>00058                 <span class="keywordflow">return</span> nValue;
<a name="l00059"></a>00059         <a class="code" href="_altova_8h.php#7636d37a55eaf66f7b50d957c53058bf">ThrowOutOfRangeError</a>();
<a name="l00060"></a>00060 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="776d6c5a5bae25e68711d4866594db02"></a><!-- doxytag: member="altova::ICMapforceMachineUtil::InRangeInt" ref="776d6c5a5bae25e68711d4866594db02" args="(TValue nValue, __int64 nMin, __int64 nMax)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;typename TValue&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">TValue altova::ICMapforceMachineUtil::InRangeInt           </td>
          <td>(</td>
          <td class="paramtype">TValue&nbsp;</td>
          <td class="paramname"> <em>nValue</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">__int64&nbsp;</td>
          <td class="paramname"> <em>nMin</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">__int64&nbsp;</td>
          <td class="paramname"> <em>nMax</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_number_8h-source.php#l00041">41</a> of file <a class="el" href="_schema_type_number_8h-source.php">SchemaTypeNumber.h</a>.
<p>
References <a class="el" href="_altova_8h-source.php#l00038">ThrowOutOfRangeError</a>.<div class="fragment"><pre class="fragment"><a name="l00042"></a>00042 {
<a name="l00043"></a>00043         <span class="keywordflow">if</span>( nValue &lt; nMin || nValue &gt; nMax )
<a name="l00044"></a>00044                 <a class="code" href="_altova_8h.php#7636d37a55eaf66f7b50d957c53058bf">ThrowOutOfRangeError</a>()
<a name="l00045"></a>00045         <span class="keywordflow">return</span> nValue;
<a name="l00046"></a>00046 }
</pre></div>
<p>

</div>
</div><p>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
