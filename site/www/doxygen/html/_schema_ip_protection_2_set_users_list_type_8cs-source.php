<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_285c2ba7298296be1db6222076863471.php">GZAdministrator</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_4b6972d9d8cfe06aa798f38257009ef5.php">SchemaIpProtection</a></div>
<h1>SchemaIpProtection/SetUsersListType.cs</h1><a href="_schema_ip_protection_2_set_users_list_type_8cs.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">//</span>
<a name="l00002"></a>00002 <span class="comment">// SetUsersListType.cs.cs</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file was generated by XMLSPY 5 Enterprise Edition.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00007"></a>00007 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00008"></a>00008 <span class="comment">//</span>
<a name="l00009"></a>00009 <span class="comment">// Refer to the XMLSPY Documentation for further details.</span>
<a name="l00010"></a>00010 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012
<a name="l00013"></a>00013
<a name="l00014"></a>00014 <span class="keyword">using</span> System;
<a name="l00015"></a>00015 <span class="keyword">using</span> System.Xml;
<a name="l00016"></a>00016 <span class="keyword">using</span> Altova.Types;
<a name="l00017"></a>00017
<a name="l00018"></a>00018 <span class="keyword">namespace </span>SchemaIpProtection
<a name="l00019"></a>00019 {
<a name="l00020"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php">00020</a>         <span class="keyword">public</span> <span class="keyword">class </span><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php">SetUsersListType</a> : Altova.Node
<a name="l00021"></a>00021         {
<a name="l00022"></a>00022 <span class="preprocessor">                #region Forward constructors</span>
<a name="l00023"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#20b89572a74cb9ae2ad84b32ebb18789">00023</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#20b89572a74cb9ae2ad84b32ebb18789">SetUsersListType</a>() : base() {}
<a name="l00024"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#bc05c383f5c32504e16ed7e0b67f889e">00024</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#20b89572a74cb9ae2ad84b32ebb18789">SetUsersListType</a>(XmlDocument doc) : base(doc) {}
<a name="l00025"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#f6109d3f265e7a72a1669c8d4e37a72e">00025</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#20b89572a74cb9ae2ad84b32ebb18789">SetUsersListType</a>(XmlNode node) : base(node) {}
<a name="l00026"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#69efad52848f7881ba98769cad7b7a35">00026</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#20b89572a74cb9ae2ad84b32ebb18789">SetUsersListType</a>(Altova.Node node) : base(node) {}
<a name="l00027"></a>00027 <span class="preprocessor">                #endregion // Forward constructors</span>
<a name="l00028"></a>00028 <span class="preprocessor"></span>
<a name="l00029"></a>00029 <span class="preprocessor">                #region Users accessor methods</span>
<a name="l00030"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#4744d04c84b9569d5e2b5bf67474124b">00030</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#4744d04c84b9569d5e2b5bf67474124b">GetUsersMinCount</a>()
<a name="l00031"></a>00031                 {
<a name="l00032"></a>00032                         <span class="keywordflow">return</span> 0;
<a name="l00033"></a>00033                 }
<a name="l00034"></a>00034
<a name="l00035"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#27dfa4f155ba40dd86052a5520e5c833">00035</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#27dfa4f155ba40dd86052a5520e5c833">GetUsersMaxCount</a>()
<a name="l00036"></a>00036                 {
<a name="l00037"></a>00037                         <span class="keywordflow">return</span> 1;
<a name="l00038"></a>00038                 }
<a name="l00039"></a>00039
<a name="l00040"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#a13c7cc4bc2e93e9860c69f77f5b4311">00040</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#a13c7cc4bc2e93e9860c69f77f5b4311">GetUsersCount</a>()
<a name="l00041"></a>00041                 {
<a name="l00042"></a>00042                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#5a78f4a7e638948db52a50bf27f97277">DomChildCount</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Users"</span>);
<a name="l00043"></a>00043                 }
<a name="l00044"></a>00044
<a name="l00045"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#83331f6e6b410e455491f2d702029164">00045</a>                 <span class="keyword">public</span> <span class="keywordtype">bool</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#83331f6e6b410e455491f2d702029164">HasUsers</a>()
<a name="l00046"></a>00046                 {
<a name="l00047"></a>00047                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#22caf29f497e86a36ca976c5b9344d11">HasDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Users"</span>);
<a name="l00048"></a>00048                 }
<a name="l00049"></a>00049
<a name="l00050"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#acb6f6f9add4a39369f32a49a394095c">00050</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_ip_protection_1_1_users_type.php">UsersType</a> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#acb6f6f9add4a39369f32a49a394095c">GetUsersAt</a>(<span class="keywordtype">int</span> index)
<a name="l00051"></a>00051                 {
<a name="l00052"></a>00052                         <span class="keywordflow">return</span> <span class="keyword">new</span> <a class="code" href="class_schema_ip_protection_1_1_users_type.php">UsersType</a>(<a class="code" href="class_altova_1_1_node.php#30b0b16f6517d59c5415e7e7d8f83959">GetDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Users"</span>, index));
<a name="l00053"></a>00053                 }
<a name="l00054"></a>00054
<a name="l00055"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#3fb878cd062d1826d94953ce73b04a54">00055</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_ip_protection_1_1_users_type.php">UsersType</a> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#3fb878cd062d1826d94953ce73b04a54">GetUsers</a>()
<a name="l00056"></a>00056                 {
<a name="l00057"></a>00057                         <span class="keywordflow">return</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#acb6f6f9add4a39369f32a49a394095c">GetUsersAt</a>(0);
<a name="l00058"></a>00058                 }
<a name="l00059"></a>00059
<a name="l00060"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#cf517f5bf5000790bde05915ce677aae">00060</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#cf517f5bf5000790bde05915ce677aae">RemoveUsersAt</a>(<span class="keywordtype">int</span> index)
<a name="l00061"></a>00061                 {
<a name="l00062"></a>00062                         <a class="code" href="class_altova_1_1_node.php#7938fa304858dfc0e9f8f492c9a65ecb">RemoveDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Users"</span>, index);
<a name="l00063"></a>00063                 }
<a name="l00064"></a>00064
<a name="l00065"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#1f4596ddfaebff405d9b446db74aec49">00065</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#1f4596ddfaebff405d9b446db74aec49">RemoveUsers</a>()
<a name="l00066"></a>00066                 {
<a name="l00067"></a>00067                         <span class="keywordflow">while</span> (<a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#83331f6e6b410e455491f2d702029164">HasUsers</a>())
<a name="l00068"></a>00068                                 <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#cf517f5bf5000790bde05915ce677aae">RemoveUsersAt</a>(0);
<a name="l00069"></a>00069                 }
<a name="l00070"></a>00070
<a name="l00071"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#e57042924ba4d1dc9dbd92a33b35d228">00071</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#e57042924ba4d1dc9dbd92a33b35d228">AddUsers</a>(<a class="code" href="class_schema_ip_protection_1_1_users_type.php">UsersType</a> newValue)
<a name="l00072"></a>00072                 {
<a name="l00073"></a>00073                         <a class="code" href="class_altova_1_1_node.php#b9500bc54742bc18fb283eb45b43b082">AppendDomElement</a>(<span class="stringliteral">""</span>, <span class="stringliteral">"Users"</span>, newValue);
<a name="l00074"></a>00074                 }
<a name="l00075"></a>00075
<a name="l00076"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#ebf933b1199882936a6702bc362e15dd">00076</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#ebf933b1199882936a6702bc362e15dd">InsertUsersAt</a>(<a class="code" href="class_schema_ip_protection_1_1_users_type.php">UsersType</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00077"></a>00077                 {
<a name="l00078"></a>00078                         <a class="code" href="class_altova_1_1_node.php#291c76a765b719a0f540e7ee40d0168b">InsertDomElementAt</a>(<span class="stringliteral">""</span>, <span class="stringliteral">"Users"</span>, index, newValue);
<a name="l00079"></a>00079                 }
<a name="l00080"></a>00080
<a name="l00081"></a><a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#fe59929017ba1d608f27fe94eb999fe0">00081</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_ip_protection_1_1_set_users_list_type.php#fe59929017ba1d608f27fe94eb999fe0">ReplaceUsersAt</a>(<a class="code" href="class_schema_ip_protection_1_1_users_type.php">UsersType</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00082"></a>00082                 {
<a name="l00083"></a>00083                         <a class="code" href="class_altova_1_1_node.php#b225018014944548e7123b3d934da30d">ReplaceDomElementAt</a>(<span class="stringliteral">""</span>, <span class="stringliteral">"Users"</span>, index, newValue);
<a name="l00084"></a>00084                 }
<a name="l00085"></a>00085 <span class="preprocessor">                #endregion // Users accessor methods</span>
<a name="l00086"></a>00086 <span class="preprocessor"></span>        }
<a name="l00087"></a>00087 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
