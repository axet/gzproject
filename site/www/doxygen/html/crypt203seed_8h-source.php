<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_58c9cd060c988d7c9a1f81a2de283682.php">client</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_8747469bf24f7219e24c4264ea805d07.php">clientcrypt</a></div>
<h1>crypt203seed.h</h1><a href="crypt203seed_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a><a class="code" href="structcrypt203__tag.php">00001</a> <span class="keyword">typedef</span> <span class="keyword">struct </span><a class="code" href="structcrypt203__tag.php">crypt203_tag</a>
<a name="l00002"></a>00002 {
<a name="l00003"></a><a class="code" href="structcrypt203__tag_1_1crypt203first__tag.php">00003</a>   <span class="keyword">struct </span><a class="code" href="structcrypt203__tag_1_1crypt203first__tag.php">crypt203first_tag</a>
<a name="l00004"></a>00004   {
<a name="l00005"></a><a class="code" href="structcrypt203__tag_1_1crypt203first__tag.php#ac6a61233bd43abaa6f72f01823061cd">00005</a>     <span class="keywordtype">unsigned</span> <span class="keywordtype">int</span> <a class="code" href="structcrypt203__tag_1_1crypt203first__tag.php#d6866bf47300cc93141c554934eeaf21">key1</a>[4],<a class="code" href="structcrypt203__tag_1_1crypt203first__tag.php#ac6a61233bd43abaa6f72f01823061cd">key2</a>[4];
<a name="l00006"></a><a class="code" href="structcrypt203__tag_1_1crypt203first__tag.php#943867366829c79ceaa49e725e2da3d4">00006</a>     <span class="keywordtype">unsigned</span> <span class="keywordtype">char</span> <a class="code" href="structcrypt203__tag_1_1crypt203first__tag.php#943867366829c79ceaa49e725e2da3d4">unk1</a>;
<a name="l00007"></a><a class="code" href="structcrypt203__tag_1_1crypt203first__tag.php#653e47014fc2933d2634d5e49f11e8fd">00007</a>     <span class="keywordtype">unsigned</span> <span class="keywordtype">char</span> <a class="code" href="structcrypt203__tag_1_1crypt203first__tag.php#653e47014fc2933d2634d5e49f11e8fd">unk2</a>;
<a name="l00008"></a>00008   };
<a name="l00009"></a><a class="code" href="structcrypt203__tag.php#0c9290b935f62aa7e291c93da655ecb7">00009</a>   <a class="code" href="structcrypt203__tag_1_1crypt203first__tag.php">crypt203first_tag</a> <a class="code" href="structcrypt203__tag.php#0c9290b935f62aa7e291c93da655ecb7">crypt203first</a>;
<a name="l00010"></a><a class="code" href="structcrypt203__tag_1_1crypt203second__tag.php">00010</a>   <span class="keyword">struct </span><a class="code" href="structcrypt203__tag_1_1crypt203second__tag.php">crypt203second_tag</a>
<a name="l00011"></a>00011   {
<a name="l00012"></a><a class="code" href="structcrypt203__tag_1_1crypt203second__tag.php#af970692c246bcba4b1180deef447929">00012</a>     <span class="keywordtype">unsigned</span> <span class="keywordtype">char</span> <a class="code" href="structcrypt203__tag_1_1crypt203second__tag.php#af970692c246bcba4b1180deef447929">unk</a>[0x124e];
<a name="l00013"></a>00013   };
<a name="l00014"></a><a class="code" href="structcrypt203__tag.php#17eaddeb12ab009190f1a828851cb3a3">00014</a>   <a class="code" href="structcrypt203__tag_1_1crypt203second__tag.php">crypt203second_tag</a> <a class="code" href="structcrypt203__tag.php#17eaddeb12ab009190f1a828851cb3a3">crypt203second</a>;
<a name="l00015"></a>00015 }<a class="code" href="structcrypt203__tag.php">crypt203_t</a>;
<a name="l00016"></a>00016
<a name="l00017"></a>00017 <span class="keyword">extern</span> <span class="stringliteral">"C"</span> <span class="keywordtype">void</span> __stdcall <a class="code" href="crypt203seed_8h.php#3584237451a680013e5b2e7f8281c4ea">Crypt203SeedInit</a>(<a class="code" href="structcrypt203__tag.php">crypt203_t</a>* key,<span class="keywordtype">unsigned</span> ip=0);
<a name="l00018"></a>00018 <span class="keyword">extern</span> <span class="stringliteral">"C"</span> <span class="keywordtype">void</span> __stdcall <a class="code" href="crypt203seed_8h.php#3e8321a5d91c47553023a5d634d738f2">Crypt203SeedEncrypt</a>(<a class="code" href="structcrypt203__tag.php">crypt203_t</a>* crypt,<span class="keyword">const</span> <span class="keywordtype">unsigned</span> <span class="keywordtype">char</span>* input,<span class="keywordtype">unsigned</span> <span class="keywordtype">char</span>* output,<span class="keywordtype">int</span> size);
<a name="l00019"></a>00019 <span class="keyword">extern</span> <span class="stringliteral">"C"</span> <span class="keywordtype">void</span> __stdcall <a class="code" href="crypt203seed_8h.php#c0087657ad4de81f0947629f7961937b">Crypt203SeedDecrypt</a>(<a class="code" href="structcrypt203__tag.php">crypt203_t</a>* crypt,<span class="keyword">const</span> <span class="keywordtype">unsigned</span> <span class="keywordtype">char</span>* input,<span class="keywordtype">unsigned</span> <span class="keywordtype">char</span>* output,<span class="keywordtype">int</span> size);
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
