<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>AccountManager::CItemsType Member List</h1>This is the complete list of members for <a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#2fa25a683001e2c63781db18aafd5c63">AddItem</a>(CItemType &amp;Item)</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#4d92c57ae91128349c616ea1ac609e5b">CItemsType</a>()</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#58191a4bf674f3e67878efb7bc588212">CItemsType</a>(CNode &amp;rParentNode, MSXML2::IXMLDOMNodePtr spThisNode)</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#05a40ec7067a40bfa4ccff1dabf2a1cc">CItemsType</a>(MSXML2::IXMLDOMDocument2Ptr spDoc)</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#21c1881a852817b9551eff6286f09bcd">GetAdvancedItemCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#8711e1145e216ddc34503020d12f4f9a">GetGroupType</a>()</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#6ecab1e56492743042a187b9ed40e632">GetItem</a>()</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#4880c5af1783be0318fe94b05cea7786">GetItemAt</a>(int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#adb4d65f8de7b2088e7c1b62c34464c6">GetItemCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#011563931a93c998b3f62c29d67466b8">GetItemMaxCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#7b7698a199a9d79e3c80d21c3c00ecf1">GetItemMinCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#c7348662197e80410301bc8a453fa7b3">GetItemValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#50680d8d13eead12af6d79b777c49384">GetStartingItemCursor</a>()</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#72436ad5a77c3ffa0ebe443792cd43b2">HasItem</a>()</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#fcc0590e1520866e21b0fbeb2c70d75e">InsertItemAt</a>(CItemType &amp;Item, int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#7a8f6cce51d3a9b7cf1ccdc6a4e8fa5a">RemoveItem</a>()</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#2ff01bb51aa1a8d9c303e12602b81ea6">RemoveItemAt</a>(int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_items_type.php#5585aa0312a47e5bfb119170e3b1611c">ReplaceItemAt</a>(CItemType &amp;Item, int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_items_type.php">AccountManager::CItemsType</a></td><td></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
