<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ea6c3855b402b97c9537b801f064bee.php">GZone</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_261848f3f32d278f15a1eb75313f8c6c.php">modules</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_06f702c6ed4c4dbe9f8676ec3a219996.php">CIQSamplev7</a></div>
<h1>CIQSamplev7/CIQSample.cpp File Reference</h1><code>#include &quot;<a class="el" href="_g_z_2_g_zone_2modules_2_c_i_q_samplev7_2_std_afx_8h-source.php">stdafx.h</a>&quot;</code><br>
<code>#include &lt;afxdllx.h&gt;</code><br>

<p>
<a href="_c_i_q_samplev7_2_c_i_q_sample_8cpp-source.php">Go to the source code of this file.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">int APIENTRY&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="_c_i_q_samplev7_2_c_i_q_sample_8cpp.php#4cad954840511629307ac5ea095ebb67">DllMain</a> (HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)</td></tr>

<tr><td colspan="2"><br><h2>Variables</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">static AFX_EXTENSION_MODULE&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="_c_i_q_samplev7_2_c_i_q_sample_8cpp.php#6a20a3e2762b5b7e5d1e0b7942b36cd2">CIQSampleDLL</a> = { NULL, NULL }</td></tr>

</table>
<hr><h2>Function Documentation</h2>
<a class="anchor" name="4cad954840511629307ac5ea095ebb67"></a><!-- doxytag: member="CIQSamplev7/CIQSample.cpp::DllMain" ref="4cad954840511629307ac5ea095ebb67" args="(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int APIENTRY DllMain           </td>
          <td>(</td>
          <td class="paramtype">HINSTANCE&nbsp;</td>
          <td class="paramname"> <em>hInstance</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">DWORD&nbsp;</td>
          <td class="paramname"> <em>dwReason</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">LPVOID&nbsp;</td>
          <td class="paramname"> <em>lpReserved</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_c_i_q_samplev7_2_c_i_q_sample_8cpp-source.php#l00014">14</a> of file <a class="el" href="_c_i_q_samplev7_2_c_i_q_sample_8cpp-source.php">CIQSamplev7/CIQSample.cpp</a>.
<p>
References <a class="el" href="_c_i_q_samplev7_2_c_i_q_sample_8cpp-source.php#l00011">CIQSampleDLL</a>.<div class="fragment"><pre class="fragment"><a name="l00015"></a>00015 {
<a name="l00016"></a>00016         <span class="comment">// Remove this if you use lpReserved</span>
<a name="l00017"></a>00017         UNREFERENCED_PARAMETER(lpReserved);
<a name="l00018"></a>00018
<a name="l00019"></a>00019         <span class="keywordflow">if</span> (dwReason == DLL_PROCESS_ATTACH)
<a name="l00020"></a>00020         {
<a name="l00021"></a>00021                 TRACE0(<span class="stringliteral">"CIQSample.DLL Initializing!\n"</span>);
<a name="l00022"></a>00022
<a name="l00023"></a>00023                 <span class="comment">// Extension DLL one-time initialization</span>
<a name="l00024"></a>00024                 <span class="keywordflow">if</span> (!AfxInitExtensionModule(<a class="code" href="_c_i_q_samplev7_2_c_i_q_sample_8cpp.php#6a20a3e2762b5b7e5d1e0b7942b36cd2">CIQSampleDLL</a>, hInstance))
<a name="l00025"></a>00025                         <span class="keywordflow">return</span> 0;
<a name="l00026"></a>00026
<a name="l00027"></a>00027                 <span class="comment">// Insert this DLL into the resource chain</span>
<a name="l00028"></a>00028                 <span class="comment">// NOTE: If this Extension DLL is being implicitly linked to by</span>
<a name="l00029"></a>00029                 <span class="comment">//  an MFC Regular DLL (such as an ActiveX Control)</span>
<a name="l00030"></a>00030                 <span class="comment">//  instead of an MFC application, then you will want to</span>
<a name="l00031"></a>00031                 <span class="comment">//  remove this line from DllMain and put it in a separate</span>
<a name="l00032"></a>00032                 <span class="comment">//  function exported from this Extension DLL.  The Regular DLL</span>
<a name="l00033"></a>00033                 <span class="comment">//  that uses this Extension DLL should then explicitly call that</span>
<a name="l00034"></a>00034                 <span class="comment">//  function to initialize this Extension DLL.  Otherwise,</span>
<a name="l00035"></a>00035                 <span class="comment">//  the CDynLinkLibrary object will not be attached to the</span>
<a name="l00036"></a>00036                 <span class="comment">//  Regular DLL's resource chain, and serious problems will</span>
<a name="l00037"></a>00037                 <span class="comment">//  result.</span>
<a name="l00038"></a>00038
<a name="l00039"></a>00039                 <span class="keyword">new</span> CDynLinkLibrary(<a class="code" href="_c_i_q_samplev7_2_c_i_q_sample_8cpp.php#6a20a3e2762b5b7e5d1e0b7942b36cd2">CIQSampleDLL</a>);
<a name="l00040"></a>00040
<a name="l00041"></a>00041         }
<a name="l00042"></a>00042         <span class="keywordflow">else</span> <span class="keywordflow">if</span> (dwReason == DLL_PROCESS_DETACH)
<a name="l00043"></a>00043         {
<a name="l00044"></a>00044                 TRACE0(<span class="stringliteral">"CIQSample.DLL Terminating!\n"</span>);
<a name="l00045"></a>00045
<a name="l00046"></a>00046                 <span class="comment">// Terminate the library before destructors are called</span>
<a name="l00047"></a>00047                 AfxTermExtensionModule(<a class="code" href="_c_i_q_samplev7_2_c_i_q_sample_8cpp.php#6a20a3e2762b5b7e5d1e0b7942b36cd2">CIQSampleDLL</a>);
<a name="l00048"></a>00048         }
<a name="l00049"></a>00049         <span class="keywordflow">return</span> 1;   <span class="comment">// ok</span>
<a name="l00050"></a>00050 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Variable Documentation</h2>
<a class="anchor" name="6a20a3e2762b5b7e5d1e0b7942b36cd2"></a><!-- doxytag: member="CIQSamplev7/CIQSample.cpp::CIQSampleDLL" ref="6a20a3e2762b5b7e5d1e0b7942b36cd2" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">AFX_EXTENSION_MODULE <a class="el" href="_c_i_q_samplev8_2_c_i_q_sample_8cpp.php#6a20a3e2762b5b7e5d1e0b7942b36cd2">CIQSampleDLL</a> = { NULL, NULL }<code> [static]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_c_i_q_samplev7_2_c_i_q_sample_8cpp-source.php#l00011">11</a> of file <a class="el" href="_c_i_q_samplev7_2_c_i_q_sample_8cpp-source.php">CIQSamplev7/CIQSample.cpp</a>.
<p>
Referenced by <a class="el" href="_c_i_q_samplev7_2_c_i_q_sample_8cpp-source.php#l00014">DllMain()</a>.
</div>
</div><p>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
