<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>CGZAssistApp Member List</h1>This is the complete list of members for <a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#7d79e88c890a789d20b43dceb75e478e">AssistAddinList</a> typedef</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#210dacccdd599acd196a6b321a3a5b1f">CGZAssistApp</a>()</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#93155cb475a6b22d1a8eab962d65806e">ExitInstance</a>()</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#15fda1f3f14aba75110fce935144eed8">GetIni</a>()</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#e97093d659c82504fadd56ba3938e4bd">GetIniPath</a>()</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#930152f7bc2cfe5c8a45ca50091ce06b">InitInstance</a>()</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#3c976f3922706271ee5e184f9a2d4529">LoadModules</a>()</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#5517348fdd36bd3e99d8342b5a9c56df">m_addinlist</a></td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#b472e74a015ca799b8feb5c39e79e778">m_gzunit</a></td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#d6b612da302d6037db98586114ce8fde">m_optionsmain</a></td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#ab89590cecbb1eeaa832f4402353bfc6">m_unitlog</a></td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#0fbb140bde409020bd0609cb08b3761e">MainWnd</a>()</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#c14463dff8d0f0f28cd43374b5f30773">OnAppAbout</a>()</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#b065e86834ff64b951e5e207541960d5">OnFileRestart</a>()</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#e69d77263068227767f4b856fba3af36">PreTranslateMessage</a>(MSG *pMsg)</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#4e7fbe7ac28ca62a38e71e89dc5667a0">ProcessWndProcException</a>(CException *e, const MSG *pMsg)</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_g_z_assist_app.php#03459fc5998252d84a7bb92971c6c4fc">Run</a>()</td><td><a class="el" href="class_c_g_z_assist_app.php">CGZAssistApp</a></td><td><code> [virtual]</code></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
