<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_8ba19508940e77109a2da1f76a26e336.php">ServerAddin</a></div>
<h1>serveraddin.cpp</h1><a href="serveraddin_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// serveraddin.cpp : Defines the entry point for the DLL application.</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="keyword">class </span><a class="code" href="class_c_server_addin.php">CServerAddin</a>;
<a name="l00005"></a>00005
<a name="l00006"></a><a class="code" href="serveraddin_8cpp.php#72cc69751765d28544d94e2a93da4412">00006</a> <span class="preprocessor">#define _AFXDLL</span>
<a name="l00007"></a>00007 <span class="preprocessor"></span><span class="preprocessor">#include "../include/mfc.h"</span>
<a name="l00008"></a>00008
<a name="l00009"></a>00009 <span class="preprocessor">#include &lt;<a class="code" href="_altova_lib_8h.php">Altova/AltovaLib.h</a>&gt;</span>
<a name="l00010"></a>00010
<a name="l00011"></a>00011 <span class="preprocessor">#undef _USRDLL</span>
<a name="l00012"></a>00012 <span class="preprocessor"></span><span class="preprocessor">#include "<a class="code" href="_schema_server_addin_config_8h.php">xsd/SchemaServerAddinConfig/SchemaServerAddinConfig.h</a>"</span>
<a name="l00013"></a><a class="code" href="serveraddin_8cpp.php#d1f034e6f1c34b76a59d68ed33f7fc01">00013</a> <span class="preprocessor">#define _USRDLL</span>
<a name="l00014"></a>00014 <span class="preprocessor"></span>
<a name="l00015"></a>00015 <span class="preprocessor">#include &lt;process.h&gt;</span>
<a name="l00016"></a>00016 <span class="preprocessor">#include &lt;map&gt;</span>
<a name="l00017"></a>00017 <span class="preprocessor">#include &lt;string&gt;</span>
<a name="l00018"></a>00018 <span class="preprocessor">#include &lt;atlbase.h&gt;</span>
<a name="l00019"></a>00019
<a name="l00020"></a>00020 <span class="preprocessor">#include &lt;<a class="code" href="controlwindow_8h.php">ControlWindow/controlwindow.h</a>&gt;</span>
<a name="l00021"></a>00021 <span class="preprocessor">#include &lt;<a class="code" href="debug_8h.php">debugger/debug.h</a>&gt;</span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &lt;<a class="code" href="iniext_8h.php">iniext/iniext.h</a>&gt;</span>
<a name="l00023"></a>00023 <span class="preprocessor">#include "<a class="code" href="serveraddin_8h.php">serveraddin.h</a>"</span>
<a name="l00024"></a>00024 <span class="preprocessor">#include "<a class="code" href="speedhack_8h.php">speedhack/speedhack.h</a>"</span>
<a name="l00025"></a>00025 <span class="preprocessor">#include "../clientsecure/clientsecure.h"</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include "accountmanager/accountmanager.h"</span>
<a name="l00027"></a>00027 <span class="preprocessor">#include "accountmanager/ipprotection.h"</span>
<a name="l00028"></a>00028
<a name="l00029"></a>00029 <span class="keyword">using namespace </span>ssa;
<a name="l00030"></a>00030
<a name="l00031"></a><a class="code" href="serveraddin_8h.php#ff18bf134515de23dedbf0c63c86421c">00031</a> <a class="code" href="class_c_server_addin.php">CServerAddin</a> *<a class="code" href="serveraddin_8cpp.php#ff18bf134515de23dedbf0c63c86421c">g_serveraddin</a>;
<a name="l00032"></a><a class="code" href="serveraddin_8cpp.php#54a8257fc73142bc59be2d8f0bc667c5">00032</a> <a class="code" href="class_control_window_1_1_c_control_window.php">ControlWindow::CControlWindow</a> <a class="code" href="serveraddin_8cpp.php#54a8257fc73142bc59be2d8f0bc667c5">g_mywnd</a>;
<a name="l00033"></a>00033
<a name="l00034"></a><a class="code" href="structexitprogramm.php">00034</a> <span class="keyword">struct </span><a class="code" href="structexitprogramm.php">exitprogramm</a>
<a name="l00035"></a>00035 {
<a name="l00036"></a><a class="code" href="structexitprogramm.php#db0ee93de904921fad5cad49e57dc510">00036</a>   HMODULE <a class="code" href="structexitprogramm.php#db0ee93de904921fad5cad49e57dc510">h</a>;
<a name="l00037"></a><a class="code" href="structexitprogramm.php#47a9e935ad5477f1922b617a6e75be9f">00037</a>   <a class="code" href="class_c_server_addin.php">CServerAddin</a> * <a class="code" href="structexitprogramm.php#47a9e935ad5477f1922b617a6e75be9f">p</a>;
<a name="l00038"></a>00038
<a name="l00039"></a><a class="code" href="structexitprogramm.php#01baf147d0bbad761ef790a6f035e5b6">00039</a>   <a class="code" href="structexitprogramm.php#01baf147d0bbad761ef790a6f035e5b6">exitprogramm</a>(HMODULE hh,<a class="code" href="class_c_server_addin.php">CServerAddin</a>* pp):<a class="code" href="structexitprogramm.php#db0ee93de904921fad5cad49e57dc510">h</a>(hh),<a class="code" href="structexitprogramm.php#47a9e935ad5477f1922b617a6e75be9f">p</a>(pp) {}
<a name="l00040"></a>00040 };
<a name="l00041"></a>00041
<a name="l00042"></a>00042 <span class="keywordtype">void</span> <a class="code" href="serveraddin_8cpp.php#0614389ed1c10e378415009fe8c7529c">ExitProgramm</a>(<a class="code" href="structexitprogramm.php">exitprogramm</a>* e);
<a name="l00043"></a>00043
<a name="l00044"></a>00044 <span class="comment">//</span>
<a name="l00045"></a>00045
<a name="l00046"></a><a class="code" href="class_c_server_addin.php#7df90a5d15f7877b3da1d03729437e09">00046</a> <a class="code" href="class_c_server_addin.php#7df90a5d15f7877b3da1d03729437e09">CServerAddin::CServerAddin</a>():m_module(0),m_runthread(0)
<a name="l00047"></a>00047 {
<a name="l00048"></a>00048   <a class="code" href="class_c_server_addin.php#c7890d2e3a53c52c168fe9f84be90e1e">m_firewall</a>=<span class="keyword">new</span> <a class="code" href="class_firewall_1_1_c_firewall.php">CFirewall</a>();
<a name="l00049"></a>00049   <a class="code" href="class_c_server_addin.php#02b55192a79867b228679d08c1304053">AddModule</a>(<a class="code" href="class_c_server_addin.php#c7890d2e3a53c52c168fe9f84be90e1e">m_firewall</a>);
<a name="l00050"></a>00050   <a class="code" href="class_c_speed_hack.php">CSpeedHack</a> *sp=<span class="keyword">new</span> <a class="code" href="class_c_speed_hack.php">CSpeedHack</a>();
<a name="l00051"></a>00051   <a class="code" href="class_c_server_addin.php#02b55192a79867b228679d08c1304053">AddModule</a>(sp);
<a name="l00052"></a>00052   <a class="code" href="class_c_server_addin.php#c7890d2e3a53c52c168fe9f84be90e1e">m_firewall</a>-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#cd7ea01859dc128350909a30a0d34ccc">AddModule</a>(sp);
<a name="l00053"></a>00053   <a class="code" href="class_client_secure_1_1_c_client_secure.php">ClientSecure::CClientSecure</a> *sc=<span class="keyword">new</span> <a class="code" href="class_client_secure_1_1_c_client_secure.php">ClientSecure::CClientSecure</a>();
<a name="l00054"></a>00054   <a class="code" href="class_c_server_addin.php#02b55192a79867b228679d08c1304053">AddModule</a>(sc);
<a name="l00055"></a>00055   <a class="code" href="class_c_server_addin.php#c7890d2e3a53c52c168fe9f84be90e1e">m_firewall</a>-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#cd7ea01859dc128350909a30a0d34ccc">AddModule</a>(sc);
<a name="l00056"></a>00056   <a class="code" href="class_c_account_manager.php">CAccountManager</a>* accountmanager=<span class="keyword">new</span> <a class="code" href="class_c_account_manager.php">CAccountManager</a>;
<a name="l00057"></a>00057   <a class="code" href="class_c_server_addin.php#02b55192a79867b228679d08c1304053">AddModule</a>(accountmanager);
<a name="l00058"></a>00058   <a class="code" href="class_c_ip_protection.php">CIpProtection</a> *ipprotection=<span class="keyword">new</span> <a class="code" href="class_c_ip_protection.php">CIpProtection</a>(accountmanager);
<a name="l00059"></a>00059   <a class="code" href="class_c_server_addin.php#c7890d2e3a53c52c168fe9f84be90e1e">m_firewall</a>-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#cd7ea01859dc128350909a30a0d34ccc">AddModule</a>(ipprotection);
<a name="l00060"></a>00060   <a class="code" href="class_c_server_addin.php#02b55192a79867b228679d08c1304053">AddModule</a>(ipprotection);
<a name="l00061"></a>00061   <a class="code" href="class_c_privileges.php">CPrivileges</a> *privileges=<span class="keyword">new</span> <a class="code" href="class_c_privileges.php">CPrivileges</a>(accountmanager);
<a name="l00062"></a>00062   <a class="code" href="class_c_server_addin.php#c7890d2e3a53c52c168fe9f84be90e1e">m_firewall</a>-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#cd7ea01859dc128350909a30a0d34ccc">AddModule</a>(privileges);
<a name="l00063"></a>00063   <a class="code" href="class_c_server_addin.php#02b55192a79867b228679d08c1304053">AddModule</a>(privileges);
<a name="l00064"></a>00064 }
<a name="l00065"></a>00065
<a name="l00066"></a><a class="code" href="class_c_server_addin.php#02fca8e01ba93777e049bd78bd364b8a">00066</a> <a class="code" href="class_c_server_addin.php#02fca8e01ba93777e049bd78bd364b8a">CServerAddin::~CServerAddin</a>()
<a name="l00067"></a>00067 {
<a name="l00068"></a>00068   <span class="keywordflow">if</span>(<a class="code" href="class_c_server_addin.php#762f23cb9f4d939a7d5cb83f64b64940">m_runthread</a>!=0)
<a name="l00069"></a>00069   {
<a name="l00070"></a>00070     WaitForSingleObject((HANDLE)<a class="code" href="class_c_server_addin.php#762f23cb9f4d939a7d5cb83f64b64940">m_runthread</a>,-1);
<a name="l00071"></a>00071     CloseHandle((HANDLE)m_runthread);
<a name="l00072"></a>00072     m_runthread=0;
<a name="l00073"></a>00073   }
<a name="l00074"></a>00074
<a name="l00075"></a>00075   <span class="keywordflow">if</span>(<a class="code" href="class_c_server_addin.php#fc7cda6e817c29e6e8711b096b915b59">m_module</a>)
<a name="l00076"></a>00076     <a class="code" href="class_c_server_addin.php#c4c9e299ace11d2efd760b7c602067cb">SaveConfigs</a>();
<a name="l00077"></a>00077
<a name="l00078"></a>00078   <span class="keywordflow">for</span>(ModuleList::iterator i=<a class="code" href="class_c_server_addin.php#18fac97ce961abdbac3f42b56dd355aa">m_modulelistall</a>.begin();i!=<a class="code" href="class_c_server_addin.php#18fac97ce961abdbac3f42b56dd355aa">m_modulelistall</a>.end();i++)
<a name="l00079"></a>00079     <span class="keyword">delete</span> *i;
<a name="l00080"></a>00080 }
<a name="l00081"></a>00081
<a name="l00082"></a><a class="code" href="class_c_server_addin.php#db680fc8a176ce06c27fe3df1a18e0f0">00082</a> <span class="keyword">const</span> <span class="keywordtype">char</span>* <a class="code" href="class_c_server_addin.php#db680fc8a176ce06c27fe3df1a18e0f0">CServerAddin::GetConfigName</a>()
<a name="l00083"></a>00083 {
<a name="l00084"></a>00084   <span class="keywordflow">return</span> <span class="stringliteral">"ServerAddin.config"</span>;
<a name="l00085"></a>00085 }
<a name="l00086"></a>00086
<a name="l00087"></a><a class="code" href="class_c_server_addin.php#0aac67a1f4a9b12eba4f34a6d6b6d4e8">00087</a> <span class="keywordtype">void</span> <a class="code" href="class_c_server_addin.php#0aac67a1f4a9b12eba4f34a6d6b6d4e8">CServerAddin::LoadConfigs</a>()
<a name="l00088"></a>00088 {
<a name="l00089"></a>00089   <span class="keywordflow">try</span>
<a name="l00090"></a>00090   {
<a name="l00091"></a>00091     <a class="code" href="class_c_schema_server_addin_config_doc.php">CSchemaServerAddinConfigDoc</a> doc;
<a name="l00092"></a>00092     <a class="code" href="classssa_1_1_c_schema_server_addin_config_type.php">CSchemaServerAddinConfigType</a> type1,type2;
<a name="l00093"></a>00093     <a class="code" href="classssa_1_1_c_schema_server_addin_config_type.php">CSchemaServerAddinConfigType</a> type=doc.Load((<a class="code" href="namespace_ini_ext.php#d57a751f4b0835a7f50f46e09eb4e582">IniExt::GetModuleRunDir</a>(<a class="code" href="class_c_server_addin.php#fc7cda6e817c29e6e8711b096b915b59">m_module</a>)+<a class="code" href="class_c_server_addin.php#db680fc8a176ce06c27fe3df1a18e0f0">GetConfigName</a>()).c_str());
<a name="l00094"></a>00094     <span class="keywordflow">for</span>(<span class="keywordtype">int</span> i=0;i&lt;type.<a class="code" href="classssa_1_1_c_schema_server_addin_config_type.php#0391b66d225d018dd8d027e1d388cd1a">GetModuleCount</a>();i++)
<a name="l00095"></a>00095     {
<a name="l00096"></a>00096       <a class="code" href="classssa_1_1_c_module_type.php">CModuleType</a> module=type.<a class="code" href="classssa_1_1_c_schema_server_addin_config_type.php#c4839f898d4b13e496e18f1f2718c068">GetModuleAt</a>(i);
<a name="l00097"></a>00097       CModuleList::iterator mod=<a class="code" href="class_c_server_addin.php#4d849e11d7eb36e20fdf0d0291d44bb0">m_modulelistname</a>.find(((std::string)module.GetName()).c_str());
<a name="l00098"></a>00098       <span class="keywordflow">if</span>(mod!=<a class="code" href="class_c_server_addin.php#4d849e11d7eb36e20fdf0d0291d44bb0">m_modulelistname</a>.end())
<a name="l00099"></a>00099         mod-&gt;second-&gt;LoadConfig(((std::string)module.GetData()).c_str());
<a name="l00100"></a>00100     }
<a name="l00101"></a>00101   }<span class="keywordflow">catch</span>(<a class="code" href="classaltova_1_1_c_xml_exception.php">CXmlException</a> &amp;)
<a name="l00102"></a>00102   {
<a name="l00103"></a>00103     ;
<a name="l00104"></a>00104   }
<a name="l00105"></a>00105 }
<a name="l00106"></a>00106
<a name="l00107"></a><a class="code" href="class_c_server_addin.php#c4c9e299ace11d2efd760b7c602067cb">00107</a> <span class="keywordtype">void</span> <a class="code" href="class_c_server_addin.php#c4c9e299ace11d2efd760b7c602067cb">CServerAddin::SaveConfigs</a>()
<a name="l00108"></a>00108 {
<a name="l00109"></a>00109   <span class="keywordflow">try</span>
<a name="l00110"></a>00110   {
<a name="l00111"></a>00111     <a class="code" href="class_c_schema_server_addin_config_doc.php">CSchemaServerAddinConfigDoc</a> doc;
<a name="l00112"></a>00112     <a class="code" href="classssa_1_1_c_schema_server_addin_config_type.php">CSchemaServerAddinConfigType</a> type;
<a name="l00113"></a>00113     <span class="keywordflow">for</span>(CModuleList::iterator i=<a class="code" href="class_c_server_addin.php#4d849e11d7eb36e20fdf0d0291d44bb0">m_modulelistname</a>.begin();i!=<a class="code" href="class_c_server_addin.php#4d849e11d7eb36e20fdf0d0291d44bb0">m_modulelistname</a>.end();i++)
<a name="l00114"></a>00114     {
<a name="l00115"></a>00115       <a class="code" href="classssa_1_1_c_module_type.php">CModuleType</a> module;
<a name="l00116"></a>00116       module.AddName(i-&gt;first.c_str());
<a name="l00117"></a>00117       module.<a class="code" href="classssa_1_1_c_module_type.php#5ba8ffc624c4c98a5860a8b073ae6985">AddData</a>(i-&gt;second-&gt;SaveConfig().c_str());
<a name="l00118"></a>00118       type.<a class="code" href="classssa_1_1_c_schema_server_addin_config_type.php#2ec115ae72a8dcb5512c3f0496655234">AddModule</a>(module);
<a name="l00119"></a>00119     }
<a name="l00120"></a>00120     doc.SetRootElementName(<span class="stringliteral">""</span>,<span class="stringliteral">"SchemaServerAddinConfig"</span>);
<a name="l00121"></a>00121
<a name="l00122"></a>00122     doc.Save((<a class="code" href="namespace_ini_ext.php#d57a751f4b0835a7f50f46e09eb4e582">IniExt::GetModuleRunDir</a>(<a class="code" href="class_c_server_addin.php#fc7cda6e817c29e6e8711b096b915b59">m_module</a>)+<a class="code" href="class_c_server_addin.php#db680fc8a176ce06c27fe3df1a18e0f0">GetConfigName</a>()).c_str(),type);
<a name="l00123"></a>00123   }<span class="keywordflow">catch</span>(<a class="code" href="classaltova_1_1_c_xml_exception.php">CXmlException</a> &amp;)
<a name="l00124"></a>00124   {
<a name="l00125"></a>00125     ;
<a name="l00126"></a>00126   }
<a name="l00127"></a>00127 }
<a name="l00128"></a>00128
<a name="l00129"></a><a class="code" href="class_c_server_addin.php#02b55192a79867b228679d08c1304053">00129</a> <span class="keywordtype">void</span> <a class="code" href="class_c_server_addin.php#02b55192a79867b228679d08c1304053">CServerAddin::AddModule</a>(<a class="code" href="class_c_server_addin_module.php">CServerAddinModule</a>* e)
<a name="l00130"></a>00130 {
<a name="l00131"></a>00131   e-&gt;<a class="code" href="class_c_server_addin_module.php#0c16f7a7f8a8904c67f1d4075041bdb2">m_sa</a>=<span class="keyword">this</span>;
<a name="l00132"></a>00132   <span class="keywordflow">if</span>(<span class="keyword">const</span> <span class="keywordtype">char</span>*name=e-&gt;<a class="code" href="class_c_server_addin_module.php#2ba9cc29f212ee912f93e568ff5a352e">GetName</a>())
<a name="l00133"></a>00133     <a class="code" href="class_c_server_addin.php#4d849e11d7eb36e20fdf0d0291d44bb0">m_modulelistname</a>[e-&gt;<a class="code" href="class_c_server_addin_module.php#2ba9cc29f212ee912f93e568ff5a352e">GetName</a>()]=e;
<a name="l00134"></a>00134   <a class="code" href="class_c_server_addin.php#18fac97ce961abdbac3f42b56dd355aa">m_modulelistall</a>.insert(e);
<a name="l00135"></a>00135 }
<a name="l00136"></a>00136
<a name="l00137"></a><a class="code" href="class_c_server_addin.php#f75b2dda3ef41e1f68bcaafa8bc5b803">00137</a> <span class="keywordtype">void</span> <a class="code" href="class_c_server_addin.php#f75b2dda3ef41e1f68bcaafa8bc5b803">CServerAddin::Unload</a>()
<a name="l00138"></a>00138 {
<a name="l00139"></a>00139   <span class="comment">//CInterNetSASlave::Disconnect();</span>
<a name="l00140"></a>00140   _beginthread((<span class="keywordtype">void</span>(*)(<span class="keywordtype">void</span>*))<a class="code" href="serveraddin_8cpp.php#0614389ed1c10e378415009fe8c7529c">ExitProgramm</a>,0,<span class="keyword">new</span> <a class="code" href="structexitprogramm.php">exitprogramm</a>(<a class="code" href="class_c_server_addin.php#fc7cda6e817c29e6e8711b096b915b59">m_module</a>,<span class="keyword">this</span>));
<a name="l00141"></a>00141 }
<a name="l00142"></a>00142
<a name="l00143"></a><a class="code" href="class_c_server_addin.php#d269beef1c5f54efac8905aff9e4e91e">00143</a> <span class="keywordtype">void</span> <a class="code" href="class_c_server_addin.php#d269beef1c5f54efac8905aff9e4e91e">CServerAddin::ModuleCommand</a>(std::string &amp;name,std::string &amp;xmlinput,std::string &amp;xmloutput)
<a name="l00144"></a>00144 {
<a name="l00145"></a>00145   <span class="comment">// необходимо пройдтись по списку модулей (map&lt;std::string,..&gt;) выбрать нужный</span>
<a name="l00146"></a>00146   <span class="comment">// и запульнуть в него командой;</span>
<a name="l00147"></a>00147
<a name="l00148"></a>00148   CModuleList::iterator i;
<a name="l00149"></a>00149   <span class="keywordflow">if</span>((i=<a class="code" href="class_c_server_addin.php#4d849e11d7eb36e20fdf0d0291d44bb0">m_modulelistname</a>.find(name))==<a class="code" href="class_c_server_addin.php#4d849e11d7eb36e20fdf0d0291d44bb0">m_modulelistname</a>.end())
<a name="l00150"></a>00150     <span class="keywordflow">return</span>;
<a name="l00151"></a>00151
<a name="l00152"></a>00152   i-&gt;second-&gt;Command(xmlinput,xmloutput);
<a name="l00153"></a>00153 }
<a name="l00154"></a>00154
<a name="l00155"></a><a class="code" href="class_c_server_addin.php#c6d8162d8be9b68335e86eb15c2b2aa8">00155</a> <span class="keywordtype">void</span> <a class="code" href="class_c_server_addin.php#c6d8162d8be9b68335e86eb15c2b2aa8">CServerAddin::Run</a>(<a class="code" href="class_c_server_addin.php">CServerAddin</a>* p)
<a name="l00156"></a>00156 {
<a name="l00157"></a>00157   <span class="keywordflow">try</span>
<a name="l00158"></a>00158   {
<a name="l00159"></a>00159     CoInitialize(0);
<a name="l00160"></a>00160
<a name="l00161"></a>00161     p-&gt;<a class="code" href="class_c_server_addin.php#0aac67a1f4a9b12eba4f34a6d6b6d4e8">LoadConfigs</a>();
<a name="l00162"></a>00162
<a name="l00163"></a>00163     p-&gt;<a class="code" href="class_c_inter_net_slave.php#9845a08b3803e3fe2e00639074399f7e">Listen</a>(48989);
<a name="l00164"></a>00164
<a name="l00165"></a>00165     CoUninitialize();
<a name="l00166"></a>00166   }<span class="keywordflow">catch</span>(std::exception &amp;e)
<a name="l00167"></a>00167   {
<a name="l00168"></a>00168     <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"%s\n"</span>,e.what());
<a name="l00169"></a>00169     MessageBox(0,e.what(),<span class="stringliteral">"Exception Slave"</span>,MB_ICONHAND);
<a name="l00170"></a>00170   }
<a name="l00171"></a>00171   _endthreadex(0);
<a name="l00172"></a>00172 }
<a name="l00173"></a>00173
<a name="l00174"></a><a class="code" href="class_c_server_addin.php#b00878f0dc3a0330f0b5d3c0035860a7">00174</a> <span class="keywordtype">void</span> <a class="code" href="class_c_server_addin.php#b00878f0dc3a0330f0b5d3c0035860a7">CServerAddin::InitInstance</a>(HMODULE hmod)
<a name="l00175"></a>00175 {
<a name="l00176"></a>00176   <a class="code" href="class_c_server_addin.php#fc7cda6e817c29e6e8711b096b915b59">m_module</a> = hmod;
<a name="l00177"></a>00177
<a name="l00178"></a>00178   <span class="comment">//g_mywnd.Control();</span>
<a name="l00179"></a>00179
<a name="l00180"></a>00180   <span class="keyword">typedef</span> <a class="code" href="pathfinding_8cpp.php#52daf63af51346479bea3bd2e2af7f01">void</a> (ddd)(<span class="keywordtype">void</span>*);
<a name="l00181"></a>00181   <a class="code" href="class_c_server_addin.php#762f23cb9f4d939a7d5cb83f64b64940">m_runthread</a>=_beginthread( (ddd*) <a class="code" href="class_c_server_addin.php#c6d8162d8be9b68335e86eb15c2b2aa8">Run</a>,0,<span class="keyword">this</span>);
<a name="l00182"></a>00182 }
<a name="l00183"></a>00183
<a name="l00184"></a><a class="code" href="serveraddin_8cpp.php#0614389ed1c10e378415009fe8c7529c">00184</a> <span class="keywordtype">void</span> <a class="code" href="serveraddin_8cpp.php#0614389ed1c10e378415009fe8c7529c">ExitProgramm</a>(<a class="code" href="structexitprogramm.php">exitprogramm</a>* e)
<a name="l00185"></a>00185 {
<a name="l00186"></a>00186   e-&gt;<a class="code" href="structexitprogramm.php#47a9e935ad5477f1922b617a6e75be9f">p</a>-&gt;<a class="code" href="class_c_inter_net_slave.php#f624d129e3e713e41c80054641da1b61">Disconnect</a>();
<a name="l00187"></a>00187   HMODULE h=e-&gt;<a class="code" href="structexitprogramm.php#db0ee93de904921fad5cad49e57dc510">h</a>;
<a name="l00188"></a>00188   <span class="keyword">delete</span> e;
<a name="l00189"></a>00189   __asm
<a name="l00190"></a>00190   {
<a name="l00191"></a>00191     push 0x12345678; <span class="comment">// exit code from this thread, ExitThread(0x12345678)</span>
<a name="l00192"></a>00192     push 0x0;<span class="comment">// return address for ExitThread</span>
<a name="l00193"></a>00193     push h; <span class="comment">// FreeLibrary(h)</span>
<a name="l00194"></a>00194     push ExitThread;
<a name="l00195"></a>00195     jmp dword ptr [FreeLibrary];
<a name="l00196"></a>00196   }
<a name="l00197"></a>00197 }
<a name="l00198"></a>00198
<a name="l00200"></a>00200
<a name="l00201"></a>00201 <span class="keyword">class </span><a class="code" href="class_c_my_app.php">CMyApp</a>:<span class="keyword">public</span> CWinApp,<span class="keyword">public</span> CFlukeSlave
<a name="l00202"></a>00202 {
<a name="l00203"></a>00203 <span class="keyword">public</span>:
<a name="l00204"></a><a class="code" href="class_c_my_app.php#56a0bb903a880e79ce33123086e1a089">00204</a>   <a class="code" href="class_c_my_app.php#56a0bb903a880e79ce33123086e1a089">CMyApp</a>()
<a name="l00205"></a>00205   {
<a name="l00206"></a>00206     <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"CMyApp\n"</span>);
<a name="l00207"></a>00207   };
<a name="l00208"></a>00208   <span class="keywordtype">void</span> <a class="code" href="class_c_my_app.php#a095ce4a16b98a61cfd3dafac62c0f99">FlukeInit</a>(HMODULE h);
<a name="l00209"></a>00209   BOOL <a class="code" href="class_c_my_app.php#614d527d5901dfdce1301ad73b2edbe6">InitInstance</a>();
<a name="l00210"></a>00210   <span class="keywordtype">int</span> <a class="code" href="class_c_my_app.php#e1e59e51e3e9be2aa6e9ff3c705a6d01">ExitInstance</a>();
<a name="l00211"></a>00211 };
<a name="l00212"></a>00212
<a name="l00213"></a><a class="code" href="serveraddin_8cpp.php#000611c562fa48e805cfcbbdbae75a44">00213</a> <a class="code" href="class_c_my_app.php">CMyApp</a> <a class="code" href="serveraddin_8cpp.php#000611c562fa48e805cfcbbdbae75a44">myApp</a>;
<a name="l00214"></a>00214
<a name="l00215"></a>00215 <span class="keywordtype">void</span> <a class="code" href="class_c_my_app.php#a095ce4a16b98a61cfd3dafac62c0f99">CMyApp::FlukeInit</a>(HMODULE h)
<a name="l00216"></a>00216 {
<a name="l00217"></a>00217   CoInitialize(0);
<a name="l00218"></a>00218   <a class="code" href="serveraddin_8cpp.php#ff18bf134515de23dedbf0c63c86421c">g_serveraddin</a>=<span class="keyword">new</span> <a class="code" href="class_c_server_addin.php">CServerAddin</a>;
<a name="l00219"></a>00219   <a class="code" href="serveraddin_8cpp.php#ff18bf134515de23dedbf0c63c86421c">g_serveraddin</a>-&gt;<a class="code" href="class_c_server_addin.php#b00878f0dc3a0330f0b5d3c0035860a7">InitInstance</a>((HMODULE)m_hInstance);
<a name="l00220"></a>00220   CoUninitialize();
<a name="l00221"></a>00221 }
<a name="l00222"></a>00222
<a name="l00223"></a><a class="code" href="class_c_my_app.php#614d527d5901dfdce1301ad73b2edbe6">00223</a> BOOL <a class="code" href="class_c_my_app.php#614d527d5901dfdce1301ad73b2edbe6">CMyApp::InitInstance</a>()
<a name="l00224"></a>00224 {
<a name="l00225"></a>00225   Create((HMODULE)m_hInstance);
<a name="l00226"></a>00226         <span class="keywordflow">return</span> CWinApp::InitInstance();
<a name="l00227"></a>00227 }
<a name="l00228"></a>00228
<a name="l00229"></a><a class="code" href="class_c_my_app.php#e1e59e51e3e9be2aa6e9ff3c705a6d01">00229</a> <span class="keywordtype">int</span> <a class="code" href="class_c_my_app.php#e1e59e51e3e9be2aa6e9ff3c705a6d01">CMyApp::ExitInstance</a>()
<a name="l00230"></a>00230 {
<a name="l00231"></a>00231   CoInitialize(0);
<a name="l00232"></a>00232   <span class="keywordflow">if</span>(<a class="code" href="serveraddin_8cpp.php#ff18bf134515de23dedbf0c63c86421c">g_serveraddin</a>)
<a name="l00233"></a>00233     <span class="keyword">delete</span> <a class="code" href="serveraddin_8cpp.php#ff18bf134515de23dedbf0c63c86421c">g_serveraddin</a>;
<a name="l00234"></a>00234   <a class="code" href="serveraddin_8cpp.php#ff18bf134515de23dedbf0c63c86421c">g_serveraddin</a>=0;
<a name="l00235"></a>00235   Close();
<a name="l00236"></a>00236   CoUninitialize();
<a name="l00237"></a>00237   <span class="keywordflow">return</span> 0;
<a name="l00238"></a>00238 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
