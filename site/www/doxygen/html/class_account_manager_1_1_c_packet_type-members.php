<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>AccountManager::CPacketType Member List</h1>This is the complete list of members for <a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#ea466c3dcd0afad54d6f9b3af6660fc3">AddGetUsersList_Request</a>(CGetUsersList_RequestType &amp;GetUsersList_Request)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#cbdebc186f215a44bf6ecc7223246bb7">AddGetUsersList_Respond</a>(CGetUsersList_RespondType &amp;GetUsersList_Respond)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#12e5611da1ae81efac76a5c2e9ca12f2">AddSetUsersList</a>(CSetUsersListType &amp;SetUsersList)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#28dde8d41a41cbf84403a949ae52ba9a">CPacketType</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#5172727950957831870c00650fb5f6ea">CPacketType</a>(CNode &amp;rParentNode, MSXML2::IXMLDOMNodePtr spThisNode)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#c83532023236858b2bf11be8e8c47118">CPacketType</a>(MSXML2::IXMLDOMDocument2Ptr spDoc)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#7169efb20e3fc0a533e4318a880d36af">GetAdvancedGetUsersList_RequestCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#75be002bb67f6c35a942ddf799794328">GetAdvancedGetUsersList_RespondCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#f5c1e14a7bf925a9e1c383a6a09f12c0">GetAdvancedSetUsersListCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#0903552736e9f74766a37b5399740d45">GetGetUsersList_Request</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#24193e0e2e8aac94704f3af9555909cf">GetGetUsersList_RequestAt</a>(int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#66d9f1ba90b07eed3e2d222c17e8499b">GetGetUsersList_RequestCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#485a598f631a2939afd6886ee55136eb">GetGetUsersList_RequestMaxCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#783492eca7c3a0b088f7c64d78e6a87d">GetGetUsersList_RequestMinCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#5ef75f7309b67fc02080a891f3bc1161">GetGetUsersList_RequestValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#7691034d217b17bf4e3466bf937f5e3a">GetGetUsersList_Respond</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#be29c2fea28e55152dd50dbe9493934a">GetGetUsersList_RespondAt</a>(int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#5f3e924f6ea9db133a8c51e048b2f4b1">GetGetUsersList_RespondCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#dcfd4bcb873d878fd58ea5f8f67df61e">GetGetUsersList_RespondMaxCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#a2bf5638e628bccae12cdb92939c317f">GetGetUsersList_RespondMinCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#3139006113caddf9ff68e9815e5dd541">GetGetUsersList_RespondValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#cc0b59b37cb406aecff12029afc0ada0">GetGroupType</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#bb0f7e37102bc93da17a1da90b81e93b">GetSetUsersList</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#37a1a452e52864b81b0961817585e7b0">GetSetUsersListAt</a>(int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#63bcf767d8882ac6f8541e2dc271119a">GetSetUsersListCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#c32bbdabedd7e4f6ad7cdeb4942670eb">GetSetUsersListMaxCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#50b9d47ddc9ea5dc1ec742844fb3fb7d">GetSetUsersListMinCount</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#96e67f10a7b710a989d603cc1c2a2489">GetSetUsersListValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#8e5acc72348faed0329a2d46aa93e3d4">GetStartingGetUsersList_RequestCursor</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#b9d28d69aafd058139837e8f5086ccfc">GetStartingGetUsersList_RespondCursor</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#12ff3d87729b1af0f0bba34c64b8e472">GetStartingSetUsersListCursor</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#d9ee6bf6e4a66defafa2b9c3d21458a3">HasGetUsersList_Request</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#25e20797b79720da02dcdd1b3c33064f">HasGetUsersList_Respond</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#6cb743d99653d55a2cd9f5550e50f5be">HasSetUsersList</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#d0b0746256acef8e9c8296073dbbc914">InsertGetUsersList_RequestAt</a>(CGetUsersList_RequestType &amp;GetUsersList_Request, int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#d3eb1086668db4c22d8c80be08ffbb15">InsertGetUsersList_RespondAt</a>(CGetUsersList_RespondType &amp;GetUsersList_Respond, int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#01fd8d495d4137d60f43020b23500c20">InsertSetUsersListAt</a>(CSetUsersListType &amp;SetUsersList, int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#cee6bd1ed754490d16d306ba7bbd755e">RemoveGetUsersList_Request</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#c4abda080e2694c42a34d3051397f425">RemoveGetUsersList_RequestAt</a>(int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#509c50f85817d60230216e20a190eb2a">RemoveGetUsersList_Respond</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#1ee8d6b261ddb10556b320554da6199d">RemoveGetUsersList_RespondAt</a>(int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#dbb42286e07638aa0b5f318d266df4bc">RemoveSetUsersList</a>()</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#2bc6740fa0967838e69fd622198e6f81">RemoveSetUsersListAt</a>(int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#139888bed9275512c14b40f14a0ebef0">ReplaceGetUsersList_RequestAt</a>(CGetUsersList_RequestType &amp;GetUsersList_Request, int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#447487b799fad4143eedd176c92adde8">ReplaceGetUsersList_RespondAt</a>(CGetUsersList_RespondType &amp;GetUsersList_Respond, int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_account_manager_1_1_c_packet_type.php#0f9cea8c6f5670575bb1bb3d6694ad92">ReplaceSetUsersListAt</a>(CSetUsersListType &amp;SetUsersList, int nIndex)</td><td><a class="el" href="class_account_manager_1_1_c_packet_type.php">AccountManager::CPacketType</a></td><td></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
