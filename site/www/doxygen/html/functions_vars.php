<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li id="current"><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="functions.php"><span>All</span></a></li>
    <li><a href="functions_func.php"><span>Functions</span></a></li>
    <li id="current"><a href="functions_vars.php"><span>Variables</span></a></li>
    <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    <li><a href="functions_enum.php"><span>Enumerations</span></a></li>
    <li><a href="functions_eval.php"><span>Enumerator</span></a></li>
    <li><a href="functions_prop.php"><span>Properties</span></a></li>
    <li><a href="functions_rela.php"><span>Related&nbsp;Functions</span></a></li>
  </ul>
</div>
<div class="tabs">
  <ul>
    <li id="current"><a href="functions_vars.php#index__"><span>_</span></a></li>
    <li><a href="functions_vars_0x61.php#index_a"><span>a</span></a></li>
    <li><a href="functions_vars_0x62.php#index_b"><span>b</span></a></li>
    <li><a href="functions_vars_0x63.php#index_c"><span>c</span></a></li>
    <li><a href="functions_vars_0x64.php#index_d"><span>d</span></a></li>
    <li><a href="functions_vars_0x65.php#index_e"><span>e</span></a></li>
    <li><a href="functions_vars_0x66.php#index_f"><span>f</span></a></li>
    <li><a href="functions_vars_0x67.php#index_g"><span>g</span></a></li>
    <li><a href="functions_vars_0x68.php#index_h"><span>h</span></a></li>
    <li><a href="functions_vars_0x69.php#index_i"><span>i</span></a></li>
    <li><a href="functions_vars_0x6a.php#index_j"><span>j</span></a></li>
    <li><a href="functions_vars_0x6b.php#index_k"><span>k</span></a></li>
    <li><a href="functions_vars_0x6c.php#index_l"><span>l</span></a></li>
    <li><a href="functions_vars_0x6d.php#index_m"><span>m</span></a></li>
    <li><a href="functions_vars_0x6e.php#index_n"><span>n</span></a></li>
    <li><a href="functions_vars_0x6f.php#index_o"><span>o</span></a></li>
    <li><a href="functions_vars_0x70.php#index_p"><span>p</span></a></li>
    <li><a href="functions_vars_0x72.php#index_r"><span>r</span></a></li>
    <li><a href="functions_vars_0x73.php#index_s"><span>s</span></a></li>
    <li><a href="functions_vars_0x74.php#index_t"><span>t</span></a></li>
    <li><a href="functions_vars_0x75.php#index_u"><span>u</span></a></li>
    <li><a href="functions_vars_0x76.php#index_v"><span>v</span></a></li>
    <li><a href="functions_vars_0x77.php#index_w"><span>w</span></a></li>
    <li><a href="functions_vars_0x78.php#index_x"><span>x</span></a></li>
    <li><a href="functions_vars_0x79.php#index_y"><span>y</span></a></li>
    <li><a href="functions_vars_0x7a.php#index_z"><span>z</span></a></li>
  </ul>
</div>

<p>
&nbsp;
<p>
<h3><a class="anchor" name="index__">- _ -</a></h3><ul>
<li>__JAWT_GetAWT
: <a class="el" href="class_java_loader.php#2dc5e7a85ebb51207b81203b60c80e70">JavaLoader</a>
<li>__JNI_CreateJavaVM
: <a class="el" href="class_java_loader.php#ba5f09c701f1bd5989a03d98a3882f87">JavaLoader</a>
</ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
