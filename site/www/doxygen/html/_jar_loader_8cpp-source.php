<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ea6c3855b402b97c9537b801f064bee.php">GZone</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_626480bc8ffd78e75dafe6dd0252235e.php">JavaExt</a></div>
<h1>JarLoader.cpp</h1><a href="_jar_loader_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#include "<a class="code" href="_jar_loader_8h.php">JarLoader.h</a>"</span>
<a name="l00002"></a>00002 <span class="preprocessor">#include "<a class="code" href="_java_exception_8h.php">JavaException.h</a>"</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="preprocessor">#include &lt;comdef.h&gt;</span>
<a name="l00005"></a>00005
<a name="l00006"></a><a class="code" href="class_jar_loader.php#b217589b8c3c47c312ec8a0fb5e88067">00006</a> <span class="keywordtype">void</span> <a class="code" href="class_jar_loader.php#b217589b8c3c47c312ec8a0fb5e88067">JarLoader::Create</a>(JNIEnv*e,<span class="keyword">const</span> <span class="keywordtype">char</span>*envr,<a class="code" href="class_jar_loader.php">JarLoader</a>* parent)
<a name="l00007"></a>00007 {
<a name="l00008"></a>00008   <a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>=e;
<a name="l00009"></a>00009
<a name="l00010"></a>00010   _bstr_t environmentfilebstr=<span class="stringliteral">"file:/"</span>+_bstr_t(envr);
<a name="l00011"></a>00011   jstring environmentfilestr=<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>-&gt;NewString((jchar*)environmentfilebstr.GetBSTR(),environmentfilebstr.length());
<a name="l00012"></a>00012   <span class="keywordflow">if</span>(<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>-&gt;ExceptionOccurred()!=0)
<a name="l00013"></a>00013     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>);
<a name="l00014"></a>00014
<a name="l00015"></a>00015   jclass urlclass=env-&gt;FindClass(<span class="stringliteral">"java/net/URL"</span>);
<a name="l00016"></a>00016   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00017"></a>00017     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00018"></a>00018   jmethodID urlinit=env-&gt;GetMethodID(urlclass,<span class="stringliteral">"&lt;init&gt;"</span>,<span class="stringliteral">"(Ljava/lang/String;)V"</span>);
<a name="l00019"></a>00019   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00020"></a>00020     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00021"></a>00021
<a name="l00022"></a>00022   jobject envobj=env-&gt;NewObject(urlclass,urlinit,environmentfilestr);
<a name="l00023"></a>00023   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00024"></a>00024     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00025"></a>00025
<a name="l00026"></a>00026   <span class="comment">//URL urls[]=new URL[]{url}</span>
<a name="l00027"></a>00027   jobjectArray urlsobjar=env-&gt;NewObjectArray(1,urlclass,0);
<a name="l00028"></a>00028   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00029"></a>00029     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00030"></a>00030   env-&gt;SetObjectArrayElement(urlsobjar,0,envobj);
<a name="l00031"></a>00031   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00032"></a>00032     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00033"></a>00033
<a name="l00034"></a>00034   <span class="keywordflow">if</span>(parent==0)
<a name="l00035"></a>00035   {
<a name="l00036"></a>00036     <span class="comment">//URLClassLoader uc=new URLClassLoader(urls);</span>
<a name="l00037"></a>00037     <a class="code" href="class_jar_loader.php#f53441880dee8461f8ca8081d3bd0b40">classloaderclass</a>=env-&gt;FindClass(<span class="stringliteral">"java/net/URLClassLoader"</span>);
<a name="l00038"></a>00038     <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00039"></a>00039       <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00040"></a>00040     jmethodID ucinit=env-&gt;GetMethodID(<a class="code" href="class_jar_loader.php#f53441880dee8461f8ca8081d3bd0b40">classloaderclass</a>,<span class="stringliteral">"&lt;init&gt;"</span>,<span class="stringliteral">"([Ljava/net/URL;)V"</span>);
<a name="l00041"></a>00041     <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00042"></a>00042       <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00043"></a>00043     <a class="code" href="class_jar_loader.php#2a8726063909eb63bd76a73c4c044124">classloaderobj</a>=env-&gt;NewObject(<a class="code" href="class_jar_loader.php#f53441880dee8461f8ca8081d3bd0b40">classloaderclass</a>,ucinit,urlsobjar);
<a name="l00044"></a>00044     <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00045"></a>00045       <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00046"></a>00046   }<span class="keywordflow">else</span>
<a name="l00047"></a>00047   {
<a name="l00048"></a>00048     <span class="comment">//URLClassLoader uc=new URLClassLoader(urls,parent);</span>
<a name="l00049"></a>00049     <a class="code" href="class_jar_loader.php#f53441880dee8461f8ca8081d3bd0b40">classloaderclass</a>=env-&gt;FindClass(<span class="stringliteral">"java/net/URLClassLoader"</span>);
<a name="l00050"></a>00050     <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00051"></a>00051       <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00052"></a>00052     jmethodID ucinit=env-&gt;GetMethodID(<a class="code" href="class_jar_loader.php#f53441880dee8461f8ca8081d3bd0b40">classloaderclass</a>,<span class="stringliteral">"&lt;init&gt;"</span>,<span class="stringliteral">"([Ljava/net/URL;Ljava/lang/ClassLoader;)V"</span>);
<a name="l00053"></a>00053     <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00054"></a>00054       <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00055"></a>00055     <a class="code" href="class_jar_loader.php#2a8726063909eb63bd76a73c4c044124">classloaderobj</a>=env-&gt;NewObject(<a class="code" href="class_jar_loader.php#f53441880dee8461f8ca8081d3bd0b40">classloaderclass</a>,ucinit,urlsobjar,parent-&gt;<a class="code" href="class_jar_loader.php#2a8726063909eb63bd76a73c4c044124">classloaderobj</a>);
<a name="l00056"></a>00056     <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00057"></a>00057       <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00058"></a>00058   }
<a name="l00059"></a>00059
<a name="l00060"></a>00060   env-&gt;DeleteLocalRef(environmentfilestr);
<a name="l00061"></a>00061   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00062"></a>00062     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00063"></a>00063
<a name="l00064"></a>00064   env-&gt;DeleteLocalRef(urlclass);
<a name="l00065"></a>00065   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00066"></a>00066     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00067"></a>00067
<a name="l00068"></a>00068   env-&gt;DeleteLocalRef(envobj);
<a name="l00069"></a>00069   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00070"></a>00070     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00071"></a>00071
<a name="l00072"></a>00072   env-&gt;DeleteLocalRef(urlsobjar);
<a name="l00073"></a>00073   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00074"></a>00074     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00075"></a>00075 }
<a name="l00076"></a>00076
<a name="l00077"></a><a class="code" href="class_jar_loader.php#65815fc321e7d0df9b50080b6def6a85">00077</a> <a class="code" href="class_jar_loader.php#65815fc321e7d0df9b50080b6def6a85">JarLoader::~JarLoader</a>()
<a name="l00078"></a>00078 {
<a name="l00079"></a>00079   <span class="keywordflow">if</span>(<a class="code" href="class_jar_loader.php#2a8726063909eb63bd76a73c4c044124">classloaderobj</a>!=0)
<a name="l00080"></a>00080     <a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>-&gt;DeleteLocalRef(<a class="code" href="class_jar_loader.php#2a8726063909eb63bd76a73c4c044124">classloaderobj</a>);
<a name="l00081"></a>00081   <span class="keywordflow">if</span>(<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>!=0)
<a name="l00082"></a>00082   {
<a name="l00083"></a>00083     <span class="keywordflow">if</span>(<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>-&gt;ExceptionOccurred()!=0)
<a name="l00084"></a>00084       <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>);
<a name="l00085"></a>00085     env-&gt;DeleteLocalRef(<a class="code" href="class_jar_loader.php#f53441880dee8461f8ca8081d3bd0b40">classloaderclass</a>);
<a name="l00086"></a>00086     <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00087"></a>00087       <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00088"></a>00088   }
<a name="l00089"></a>00089 }
<a name="l00090"></a>00090
<a name="l00091"></a><a class="code" href="class_jar_loader.php#262674702153a2d3e7efa527ae61c8c7">00091</a> jclass <a class="code" href="class_jar_loader.php#262674702153a2d3e7efa527ae61c8c7">JarLoader::LoadClass</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>*p)
<a name="l00092"></a>00092 {
<a name="l00093"></a>00093   _bstr_t mmbstr=_bstr_t(p);
<a name="l00094"></a>00094   jstring mmstr=<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>-&gt;NewString((jchar*)mmbstr.GetBSTR(),mmbstr.length());
<a name="l00095"></a>00095   <span class="keywordflow">if</span>(<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>-&gt;ExceptionOccurred()!=0)
<a name="l00096"></a>00096     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>);
<a name="l00097"></a>00097
<a name="l00098"></a>00098   <span class="comment">//Class c=uc.loadClass(mm);</span>
<a name="l00099"></a>00099   jmethodID ucloadClass=env-&gt;GetMethodID(<a class="code" href="class_jar_loader.php#f53441880dee8461f8ca8081d3bd0b40">classloaderclass</a>,<span class="stringliteral">"loadClass"</span>,<span class="stringliteral">"(Ljava/lang/String;)Ljava/lang/Class;"</span>);
<a name="l00100"></a>00100   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00101"></a>00101     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00102"></a>00102   jclass cclass=(jclass)env-&gt;CallObjectMethod(<a class="code" href="class_jar_loader.php#2a8726063909eb63bd76a73c4c044124">classloaderobj</a>,ucloadClass,mmstr);
<a name="l00103"></a>00103   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00104"></a>00104     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00105"></a>00105   env-&gt;DeleteLocalRef(mmstr);
<a name="l00106"></a>00106   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00107"></a>00107     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00108"></a>00108   <span class="keywordflow">return</span> cclass;
<a name="l00109"></a>00109 }
<a name="l00110"></a>00110
<a name="l00111"></a><a class="code" href="class_jar_loader.php#d96fd89fded3f197c13132706a1390a6">00111</a> jobject <a class="code" href="class_jar_loader.php#d96fd89fded3f197c13132706a1390a6">JarLoader::NewInstance</a>(jclass cls)
<a name="l00112"></a>00112 {
<a name="l00113"></a>00113   <span class="comment">//Object o=c.newInstance();</span>
<a name="l00114"></a>00114   jclass cclass=<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>-&gt;FindClass(<span class="stringliteral">"java/lang/Class"</span>);
<a name="l00115"></a>00115   <span class="keywordflow">if</span>(<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>-&gt;ExceptionOccurred()!=0)
<a name="l00116"></a>00116     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(<a class="code" href="class_jar_loader.php#d00abdb5a5868b91da6defff2951c07d">env</a>);
<a name="l00117"></a>00117   jmethodID cnewInstance=env-&gt;GetMethodID(cclass,<span class="stringliteral">"newInstance"</span>,<span class="stringliteral">"()Ljava/lang/Object;"</span>);
<a name="l00118"></a>00118   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00119"></a>00119     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00120"></a>00120   jobject oobj=env-&gt;CallObjectMethod(cls,cnewInstance);
<a name="l00121"></a>00121   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00122"></a>00122     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00123"></a>00123   env-&gt;DeleteLocalRef(cclass);
<a name="l00124"></a>00124   <span class="keywordflow">if</span>(env-&gt;ExceptionOccurred()!=0)
<a name="l00125"></a>00125     <span class="keywordflow">throw</span> <a class="code" href="class_java_exception.php">JavaException</a>(env);
<a name="l00126"></a>00126   <span class="keywordflow">return</span> oobj;
<a name="l00127"></a>00127 };
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
