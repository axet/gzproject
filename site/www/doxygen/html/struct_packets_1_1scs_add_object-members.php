<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>Packets::scsAddObject Member List</h1>This is the complete list of members for <a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#da23c19c1c275aaae35955e11665d930">amount</a></td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#59de2d2df106477ac2d573ccbd07b132">contser</a></td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#3b5d2b34cc638588a25b8de79d569814">itemcolor</a></td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#267caad5b3ba6f56125b3b6345ed5b6f">itemid</a></td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#841945d90ee3038fad56014111046a47">itemserial</a></td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#3d81bfafc163d4ac9785baf533cd6994">operator=</a>(Server::ServerByte *p)</td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#48f052ed44a01383eac7c8afb41ac30d">p</a></td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#a52433dc459bfe21aacc1fdd634eaf98">scsAddObject</a>()</td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#5ef6d750805090226aef45b3e6eebdf5">scsAddObject</a>(Server::ServerByte *p)</td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#5d9cd462569d661951eff6cfc6336d99">unk1</a></td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#517bb56951844eec56226b6fcd2b5aab">x</a></td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_packets_1_1scs_add_object.php#514777d75eefe243dd7a6510b9944dd4">y</a></td><td><a class="el" href="struct_packets_1_1scs_add_object.php">Packets::scsAddObject</a></td><td></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
