<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_71fa59a8877cc78b8ebbdf09160824b9.php">GZAssist</a></div>
<h1>GZAssistDoc.h</h1><a href="_g_z_assist_doc_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// GZAssistDoc.h : interface of the CGZAssistDoc class</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment"></span>
<a name="l00005"></a>00005 <span class="preprocessor">#if !defined(AFX_GZASSISTDOC_H__D058AC55_D0F0_442E_839A_6819CA1A9A1D__INCLUDED_)</span>
<a name="l00006"></a><a class="code" href="_g_z_assist_doc_8h.php#02d0b839590364c1325433be1ff24950">00006</a> <span class="preprocessor"></span><span class="preprocessor">#define AFX_GZASSISTDOC_H__D058AC55_D0F0_442E_839A_6819CA1A9A1D__INCLUDED_</span>
<a name="l00007"></a>00007 <span class="preprocessor"></span>
<a name="l00008"></a>00008 <span class="preprocessor">#if _MSC_VER &gt; 1000</span>
<a name="l00009"></a>00009 <span class="preprocessor"></span><span class="preprocessor">#pragma once</span>
<a name="l00010"></a>00010 <span class="preprocessor"></span><span class="preprocessor">#endif // _MSC_VER &gt; 1000</span>
<a name="l00011"></a>00011 <span class="preprocessor"></span>
<a name="l00012"></a>00012
<a name="l00013"></a><a class="code" href="class_c_g_z_assist_doc.php">00013</a> <span class="keyword">class </span><a class="code" href="class_c_g_z_assist_doc.php">CGZAssistDoc</a> : <span class="keyword">public</span> CDocument
<a name="l00014"></a>00014 {
<a name="l00015"></a>00015 <span class="keyword">protected</span>: <span class="comment">// create from serialization only</span>
<a name="l00016"></a>00016         <a class="code" href="class_c_g_z_assist_doc.php#ddaab27e64270e2a44dceae21036aed6">CGZAssistDoc</a>();
<a name="l00017"></a>00017         DECLARE_DYNCREATE(<a class="code" href="class_c_g_z_assist_doc.php">CGZAssistDoc</a>)
<a name="l00018"></a>00018
<a name="l00019"></a>00019 <span class="comment">// Attributes</span>
<a name="l00020"></a>00020 <span class="keyword">public</span>:
<a name="l00021"></a>00021
<a name="l00022"></a>00022 <span class="comment">// Operations</span>
<a name="l00023"></a>00023 <span class="keyword">public</span>:
<a name="l00024"></a>00024
<a name="l00025"></a>00025 <span class="comment">// Overrides</span>
<a name="l00026"></a>00026         <span class="comment">// ClassWizard generated virtual function overrides</span>
<a name="l00027"></a>00027         <span class="comment">//{{AFX_VIRTUAL(CGZAssistDoc)</span>
<a name="l00028"></a>00028         <span class="keyword">public</span>:
<a name="l00029"></a>00029         <span class="keyword">virtual</span> BOOL <a class="code" href="class_c_g_z_assist_doc.php#f53617d80a9f02e3b04e02374b867b28">OnNewDocument</a>();
<a name="l00030"></a>00030         <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_g_z_assist_doc.php#4f8af59c50038fe82406280361993688">Serialize</a>(CArchive&amp; ar);
<a name="l00031"></a>00031         <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_g_z_assist_doc.php#9a208af370d5c9bedf05fe4aa8b02c4d">OnCloseDocument</a>();
<a name="l00032"></a>00032         <span class="keyword">protected</span>:
<a name="l00033"></a>00033         <span class="keyword">virtual</span> BOOL <a class="code" href="class_c_g_z_assist_doc.php#5616cb4b75b67139b43684e329d17c0a">SaveModified</a>();
<a name="l00034"></a>00034         <span class="comment">//}}AFX_VIRTUAL</span>
<a name="l00035"></a>00035
<a name="l00036"></a>00036 <span class="comment">// Implementation</span>
<a name="l00037"></a>00037 <span class="keyword">public</span>:
<a name="l00038"></a>00038         <span class="keyword">virtual</span> <a class="code" href="class_c_g_z_assist_doc.php#e518ed470258a979baa20f36b83c13f2">~CGZAssistDoc</a>();
<a name="l00039"></a>00039 <span class="preprocessor">#ifdef _DEBUG</span>
<a name="l00040"></a>00040 <span class="preprocessor"></span>        <span class="keyword">virtual</span> <span class="keywordtype">void</span> AssertValid() <span class="keyword">const</span>;
<a name="l00041"></a>00041         <span class="keyword">virtual</span> <span class="keywordtype">void</span> Dump(CDumpContext&amp; dc) <span class="keyword">const</span>;
<a name="l00042"></a>00042 <span class="preprocessor">#endif</span>
<a name="l00043"></a>00043 <span class="preprocessor"></span>
<a name="l00044"></a>00044 <span class="keyword">protected</span>:
<a name="l00045"></a>00045
<a name="l00046"></a>00046 <span class="comment">// Generated message map functions</span>
<a name="l00047"></a>00047 <span class="keyword">protected</span>:
<a name="l00048"></a>00048         <span class="comment">//{{AFX_MSG(CGZAssistDoc)</span>
<a name="l00049"></a>00049                 <span class="comment">// NOTE - the ClassWizard will add and remove member functions here.</span>
<a name="l00050"></a>00050                 <span class="comment">//    DO NOT EDIT what you see in these blocks of generated code !</span>
<a name="l00051"></a>00051         <span class="comment">//}}AFX_MSG</span>
<a name="l00052"></a>00052         DECLARE_MESSAGE_MAP()
<a name="l00053"></a>00053 };
<a name="l00054"></a>00054
<a name="l00056"></a>00056
<a name="l00057"></a>00057 <span class="comment">//{{AFX_INSERT_LOCATION}}</span>
<a name="l00058"></a>00058 <span class="comment">// Microsoft Visual C++ will insert additional declarations immediately before the previous line.</span>
<a name="l00059"></a>00059
<a name="l00060"></a>00060 <span class="preprocessor">#endif // !defined(AFX_GZASSISTDOC_H__D058AC55_D0F0_442E_839A_6819CA1A9A1D__INCLUDED_)</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
