<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_71fa59a8877cc78b8ebbdf09160824b9.php">GZAssist</a></div>
<h1>logctrl.cpp</h1><a href="logctrl_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#include "<a class="code" href="_g_z_2_g_z_assist_2_std_afx_8h.php">stdafx.h</a>"</span>
<a name="l00002"></a>00002 <span class="preprocessor">#include "<a class="code" href="logctrl_8h.php">logctrl.h</a>"</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="preprocessor">#include "<a class="code" href="_g_z_assist_8h.php">gzassist.h</a>"</span>
<a name="l00005"></a>00005 <span class="preprocessor">#include "<a class="code" href="unitlog_8h.php">iniext/unitlog.h</a>"</span>
<a name="l00006"></a>00006
<a name="l00007"></a><a class="code" href="class_c_log_ctrl.php#0e9a26bdc8d53798ca6279520ff553aa">00007</a> <a class="code" href="class_c_log_ctrl.php#0e9a26bdc8d53798ca6279520ff553aa">CLogCtrl::CLogCtrl</a>()
<a name="l00008"></a>00008 {
<a name="l00009"></a>00009 }
<a name="l00010"></a>00010
<a name="l00011"></a><a class="code" href="class_c_log_ctrl.php#9b77ddcb9c776a29af1a2cc961ea9317">00011</a> <a class="code" href="class_c_log_ctrl.php#9b77ddcb9c776a29af1a2cc961ea9317">CLogCtrl::~CLogCtrl</a>()
<a name="l00012"></a>00012 {
<a name="l00013"></a>00013 }
<a name="l00014"></a>00014
<a name="l00015"></a><a class="code" href="class_c_log_ctrl.php#5856f730aec16e9158c958a9df315d3a">00015</a> LRESULT <a class="code" href="class_c_log_ctrl.php#5856f730aec16e9158c958a9df315d3a">CLogCtrl::WindowProc</a>( UINT message, WPARAM <a class="code" href="flukeslave_8cpp.php#2c28a497a9b541541d5b8713a639a31b">wParam</a>, LPARAM <a class="code" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a> )
<a name="l00016"></a>00016 {
<a name="l00017"></a>00017   <span class="keywordflow">switch</span>(message)
<a name="l00018"></a>00018   {
<a name="l00019"></a>00019   <span class="keywordflow">case</span> WM_CREATE:
<a name="l00020"></a>00020     {
<a name="l00021"></a>00021       m_dwSCBStyle|=<a class="code" href="sizecbar_8h.php#e8249fde090aa7903ab6435f07bd27a2">SCBS_SIZECHILD</a>;
<a name="l00022"></a>00022       CSizingControlBarG::WindowProc(message,wParam,lParam);
<a name="l00023"></a>00023       <a class="code" href="class_c_log_ctrl.php#d50c0531dbaa5f1aaf7d227f99bacce5">m_rich</a>.Create(WS_VSCROLL|ES_READONLY|ES_AUTOVSCROLL|ES_MULTILINE|WS_VISIBLE|WS_BORDER|WS_CHILD,CRect(0,0,0,0),<span class="keyword">this</span>,0);
<a name="l00024"></a>00024       <a class="code" href="class_c_log_ctrl.php#df7a6c8e900a74aec70391cc0d234d45">m_font</a>.CreateFont(-10,0,0,0,0,0,0,0,0,0,0,0,0,0);
<a name="l00025"></a>00025       <a class="code" href="class_c_log_ctrl.php#d50c0531dbaa5f1aaf7d227f99bacce5">m_rich</a>.SetFont(&amp;<a class="code" href="class_c_log_ctrl.php#df7a6c8e900a74aec70391cc0d234d45">m_font</a>);
<a name="l00026"></a>00026
<a name="l00027"></a>00027       <span class="keywordflow">return</span> TRUE;
<a name="l00028"></a>00028     }
<a name="l00029"></a>00029     <span class="keywordflow">break</span>;
<a name="l00030"></a>00030   <span class="keywordflow">case</span> WM_DESTROY:
<a name="l00031"></a>00031     <span class="keywordflow">if</span>(<a class="code" href="_g_z_assist_8h.php#c7ceca7e0e421e274303c8392504f68d">App</a>().m_unitlog.GetLogState())
<a name="l00032"></a>00032       <a class="code" href="_g_z_assist_8h.php#c7ceca7e0e421e274303c8392504f68d">App</a>().<a class="code" href="class_c_g_z_assist_app.php#ab89590cecbb1eeaa832f4402353bfc6">m_unitlog</a>.<a class="code" href="class_c_unit_log.php#9a3d9051591b27d05853cd632f7ec3b2">SetLogState</a>(<span class="keyword">false</span>);
<a name="l00033"></a>00033     <span class="keywordflow">break</span>;
<a name="l00034"></a>00034   }
<a name="l00035"></a>00035   <span class="keywordflow">return</span> CSizingControlBarG::WindowProc(message,wParam,lParam);
<a name="l00036"></a>00036 }
<a name="l00037"></a>00037
<a name="l00038"></a><a class="code" href="class_c_log_ctrl.php#7369250c996279fde641b2d8f0986049">00038</a> <span class="keywordtype">void</span> <a class="code" href="class_c_log_ctrl.php#7369250c996279fde641b2d8f0986049">CLogCtrl::AddText</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* p)
<a name="l00039"></a>00039 {
<a name="l00040"></a>00040   <a class="code" href="class_c_log_ctrl.php#d50c0531dbaa5f1aaf7d227f99bacce5">m_rich</a>.HideSelection(TRUE,FALSE);
<a name="l00041"></a>00041   CHARRANGE cr,cr2;
<a name="l00042"></a>00042   <a class="code" href="class_c_log_ctrl.php#d50c0531dbaa5f1aaf7d227f99bacce5">m_rich</a>.GetSel(cr);
<a name="l00043"></a>00043   <a class="code" href="class_c_log_ctrl.php#d50c0531dbaa5f1aaf7d227f99bacce5">m_rich</a>.SetSel(-1,-1);
<a name="l00044"></a>00044   <a class="code" href="class_c_log_ctrl.php#d50c0531dbaa5f1aaf7d227f99bacce5">m_rich</a>.GetSel(cr2);
<a name="l00045"></a>00045   <a class="code" href="class_c_log_ctrl.php#d50c0531dbaa5f1aaf7d227f99bacce5">m_rich</a>.ReplaceSel(p);
<a name="l00046"></a>00046   <a class="code" href="class_c_log_ctrl.php#d50c0531dbaa5f1aaf7d227f99bacce5">m_rich</a>.ReplaceSel(<span class="stringliteral">"\n"</span>);
<a name="l00047"></a>00047   <a class="code" href="class_c_log_ctrl.php#d50c0531dbaa5f1aaf7d227f99bacce5">m_rich</a>.SetSel(cr);
<a name="l00048"></a>00048   <a class="code" href="class_c_log_ctrl.php#d50c0531dbaa5f1aaf7d227f99bacce5">m_rich</a>.HideSelection(FALSE,FALSE);
<a name="l00049"></a>00049   <span class="keywordflow">if</span>(cr.cpMin==cr2.cpMin&amp;&amp;cr.cpMax==cr2.cpMax)
<a name="l00050"></a>00050     <a class="code" href="class_c_log_ctrl.php#d50c0531dbaa5f1aaf7d227f99bacce5">m_rich</a>.SetSel(-1,-1);
<a name="l00051"></a>00051   <span class="keywordflow">if</span>(<a class="code" href="_g_z_assist_8h.php#c7ceca7e0e421e274303c8392504f68d">App</a>().m_unitlog.GetLogState())
<a name="l00052"></a>00052     <a class="code" href="_g_z_assist_8h.php#c7ceca7e0e421e274303c8392504f68d">App</a>().<a class="code" href="class_c_g_z_assist_app.php#ab89590cecbb1eeaa832f4402353bfc6">m_unitlog</a>.<a class="code" href="class_c_unit_log.php#40a7048281c5acf07e975fea886df338">Message</a>(p);
<a name="l00053"></a>00053 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
