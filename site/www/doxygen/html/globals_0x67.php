<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li id="current"><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="tabs">
  <ul>
    <li id="current"><a href="globals.php"><span>All</span></a></li>
    <li><a href="globals_func.php"><span>Functions</span></a></li>
    <li><a href="globals_vars.php"><span>Variables</span></a></li>
    <li><a href="globals_type.php"><span>Typedefs</span></a></li>
    <li><a href="globals_enum.php"><span>Enumerations</span></a></li>
    <li><a href="globals_eval.php"><span>Enumerator</span></a></li>
    <li><a href="globals_defs.php"><span>Defines</span></a></li>
  </ul>
</div>
<div class="tabs">
  <ul>
    <li><a href="globals.php#index__"><span>_</span></a></li>
    <li><a href="globals_0x61.php#index_a"><span>a</span></a></li>
    <li><a href="globals_0x62.php#index_b"><span>b</span></a></li>
    <li><a href="globals_0x63.php#index_c"><span>c</span></a></li>
    <li><a href="globals_0x64.php#index_d"><span>d</span></a></li>
    <li><a href="globals_0x65.php#index_e"><span>e</span></a></li>
    <li><a href="globals_0x66.php#index_f"><span>f</span></a></li>
    <li id="current"><a href="globals_0x67.php#index_g"><span>g</span></a></li>
    <li><a href="globals_0x68.php#index_h"><span>h</span></a></li>
    <li><a href="globals_0x69.php#index_i"><span>i</span></a></li>
    <li><a href="globals_0x6a.php#index_j"><span>j</span></a></li>
    <li><a href="globals_0x6c.php#index_l"><span>l</span></a></li>
    <li><a href="globals_0x6d.php#index_m"><span>m</span></a></li>
    <li><a href="globals_0x6e.php#index_n"><span>n</span></a></li>
    <li><a href="globals_0x6f.php#index_o"><span>o</span></a></li>
    <li><a href="globals_0x70.php#index_p"><span>p</span></a></li>
    <li><a href="globals_0x72.php#index_r"><span>r</span></a></li>
    <li><a href="globals_0x73.php#index_s"><span>s</span></a></li>
    <li><a href="globals_0x74.php#index_t"><span>t</span></a></li>
    <li><a href="globals_0x75.php#index_u"><span>u</span></a></li>
    <li><a href="globals_0x76.php#index_v"><span>v</span></a></li>
    <li><a href="globals_0x77.php#index_w"><span>w</span></a></li>
    <li><a href="globals_0x79.php#index_y"><span>y</span></a></li>
    <li><a href="globals_0x7a.php#index_z"><span>z</span></a></li>
  </ul>
</div>

<p>
Here is a list of all file members with links to the files they belong to:
<p>
<h3><a class="anchor" name="index_g">- g -</a></h3><ul>
<li>g_app
: <a class="el" href="_g_z_administrator_app_8cpp.php#0e1be92c3adfe51b005fbd619630f593">GZAdministratorApp.cpp</a>
, <a class="el" href="_server_addin_dlg_8cpp.php#0e1be92c3adfe51b005fbd619630f593">ServerAddinDlg.cpp</a>
<li>g_bufsize
: <a class="el" href="clientaddin_8cpp.php#9d262eade97b06e3fe977134740e204c">clientaddin.cpp</a>
, <a class="el" href="outofprocmasterevents_8h.php#9d262eade97b06e3fe977134740e204c">outofprocmasterevents.h</a>
, <a class="el" href="outofprocslave_8cpp.php#9d262eade97b06e3fe977134740e204c">outofprocslave.cpp</a>
<li>g_catchcommandwait
: <a class="el" href="_client_u_o_commands_8cpp.php#8c7d8bf0c778ddac06ad7b40ad104213">ClientUOCommands.cpp</a>
<li>g_CatchLog
: <a class="el" href="clientff_8cpp.php#71b82e2fc6d9e0330173a2ee590470c6">clientff.cpp</a>
<li>g_CatchLogUni
: <a class="el" href="clientff_8cpp.php#023acf94f3647e51d4f2af7f22becda8">clientff.cpp</a>
<li>g_CatchWindowCreate
: <a class="el" href="clientff_8cpp.php#73c5558ea364e4802e4fd58df79ba766">clientff.cpp</a>
<li>g_clientaddin
: <a class="el" href="clientaddin_8cpp.php#1567609793cfb758d1857e6106b394ab">clientaddin.cpp</a>
<li>g_ClientDisconnected
: <a class="el" href="clientff_8cpp.php#4d7edd545f5bc73da870cf9e606c24a2">clientff.cpp</a>
<li>g_createthread
: <a class="el" href="clientaddin_8cpp.php#0717ceb131f3ebecca5bb1ea2c5f7ad1">clientaddin.cpp</a>
<li>g_debuggerrourter
: <a class="el" href="_debugger_windows__i_8cpp.php#69370465fc407b941ec0f71e7596eeab">DebuggerWindows_i.cpp</a>
<li>g_DocumentIQ
: <a class="el" href="_c_i_q_goods_8cpp.php#cd7e0cd043fdba29598cf1f483d2ec9e">CIQGoods.cpp</a>
, <a class="el" href="_c_i_q_harvester_8cpp.php#cd7e0cd043fdba29598cf1f483d2ec9e">CIQHarvester.cpp</a>
, <a class="el" href="_c_i_q_trainer_8cpp.php#cd7e0cd043fdba29598cf1f483d2ec9e">CIQTrainer.cpp</a>
, <a class="el" href="_g_zone_doc_8cpp.php#cd7e0cd043fdba29598cf1f483d2ec9e">GZoneDoc.cpp</a>
<li>g_dwThreadId
: <a class="el" href="clientaddin_8cpp.php#8219bd78cc711483b2d22288d9efe815">clientaddin.cpp</a>
<li>g_flukeslaveready
: <a class="el" href="fluke_8cpp.php#d26934cda4104c73f05dfd193bdd1878">fluke.cpp</a>
, <a class="el" href="namespace_fluke.php#4f37bf76477f52e1e19b8c24d3e2647b">fluke.h</a>
<li>g_FromClient
: <a class="el" href="clientff_8cpp.php#107063d399f2334bacb46d33a04fee48">clientff.cpp</a>
<li>g_FromServer
: <a class="el" href="clientff_8cpp.php#b441812e9fdfb83ec3d68d61d661eadb">clientff.cpp</a>
<li>g_gamemapmanager
: <a class="el" href="_environment_8cpp.php#d4e2fb9c4dd20ca644fdaa5fe494b479">Environment.cpp</a>
, <a class="el" href="_environment_8h.php#d4e2fb9c4dd20ca644fdaa5fe494b479">Environment.h</a>
<li>g_hModule
: <a class="el" href="clientaddin_8cpp.php#abfa166491243a801c226df70fbc4ad8">clientaddin.cpp</a>
<li>g_LoggedInPass
: <a class="el" href="clientff_8cpp.php#fffab2d31df3d41ab2494e721b09b34a">clientff.cpp</a>
<li>g_mailslotreadwait
: <a class="el" href="interprocmaster_8cpp.php#7da8d53844eb2d4ae1efb8c7d55003a5">interprocmaster.cpp</a>
, <a class="el" href="interprocslave_8cpp.php#7da8d53844eb2d4ae1efb8c7d55003a5">interprocslave.cpp</a>
<li>g_multiuo1
: <a class="el" href="clientff_8cpp.php#e6966dfed1386562663c1529065f1862">clientff.cpp</a>
<li>g_multiuo2
: <a class="el" href="clientff_8cpp.php#a95ce0dc9ce1e70963f278bc4b908260">clientff.cpp</a>
<li>g_myapp
: <a class="el" href="clientaddin_8cpp.php#a281341b601b563a21079c6c47f5b932">clientaddin.cpp</a>
<li>g_mywnd
: <a class="el" href="serveraddin_8cpp.php#54a8257fc73142bc59be2d8f0bc667c5">serveraddin.cpp</a>
<li>g_optionsdlg
: <a class="el" href="_g_zone_8cpp.php#c393aeedfc67ca74e911f47f3a9b5c15">GZone.cpp</a>
, <a class="el" href="_g_zone_2_main_frm_8h.php#c393aeedfc67ca74e911f47f3a9b5c15">GZone/MainFrm.h</a>
, <a class="el" href="_soldier_list_view_8cpp.php#c393aeedfc67ca74e911f47f3a9b5c15">SoldierListView.cpp</a>
<li>g_PathFindingNetCall
: <a class="el" href="clientff_8cpp.php#d628401f74992c2276ef973a9d3aed41">clientff.cpp</a>
<li>g_PathFindingNetCallRetCode
: <a class="el" href="clientff_8cpp.php#39c078a6733aa3a56f9ae6b965baa90d">clientff.cpp</a>
<li>g_PathFindingNetCallTerminate
: <a class="el" href="clientff_8cpp.php#a2b7bbd8844277fefaf548959119b70e">clientff.cpp</a>
<li>g_PathFindingTerminate
: <a class="el" href="clientff_8cpp.php#ecc90d3591150a8c23222655a2bcfeed">clientff.cpp</a>
<li>g_pathfindingtimeout
: <a class="el" href="pathfinding_8cpp.php#cb39c2415b5ffaa8c6c8177d744b03f7">pathfinding.cpp</a>
<li>g_send
: <a class="el" href="clientff_8cpp.php#7a0a17facf322228ce45678f93e599ca">clientff.cpp</a>
<li>g_sendproc
: <a class="el" href="clientff_8cpp.php#a81d0b6b17fa26c321b1348ca48d5dbb">clientff.cpp</a>
<li>g_serveraddin
: <a class="el" href="serveraddin_8cpp.php#ff18bf134515de23dedbf0c63c86421c">serveraddin.cpp</a>
, <a class="el" href="serveraddin_8h.php#ff18bf134515de23dedbf0c63c86421c">serveraddin.h</a>
<li>g_SetNewPlayersCoord
: <a class="el" href="clientff_8cpp.php#b4fc9a05181b8415f2c4a98596d1ba94">clientff.cpp</a>
<li>g_slowexchange
: <a class="el" href="clientuo_8cpp.php#0b12628039137680f29ce41dc392b5e0">clientuo.cpp</a>
<li>g_unitnativemanager
: <a class="el" href="_environment_8cpp.php#5c10d68e8b2bdea42710b0f68d66b607">Environment.cpp</a>
, <a class="el" href="_environment_8h.php#5c10d68e8b2bdea42710b0f68d66b607">Environment.h</a>
<li>g_uosend0
: <a class="el" href="clientff_8cpp.php#83d9504fe3f1599d0187350ba6d689f4">clientff.cpp</a>
<li>g_uosend1
: <a class="el" href="clientff_8cpp.php#1a28cc8b0a06f3c38c99f7b3921fd2dd">clientff.cpp</a>
<li>g_uosend2
: <a class="el" href="clientff_8cpp.php#aba7ba0396f2d4cdb804a584a5f757b5">clientff.cpp</a>
<li>g_waitforclientterminate
: <a class="el" href="interprocmaster_8cpp.php#d964b892f23469ff7e9e06ea68417e87">interprocmaster.cpp</a>
<li>g_window
: <a class="el" href="clientff_8cpp.php#7e81a453bcc57bc182e66f1b3d9eb61c">clientff.cpp</a>
<li>g_WindowProc
: <a class="el" href="clientff_8cpp.php#868161ae853ec256a51f6cd0b2d89955">clientff.cpp</a>
<li>GetClientVersion()
: <a class="el" href="namespace_client_identify.php#fcaa59a2937c9ae050362dde9d2615dc">clientidentify.cpp</a>
<li>GetFileName()
: <a class="el" href="emulator_8cpp.php#8e42a5262d1a9451d2479008b92573a8">emulator.cpp</a>
<li>GetLastErrorString()
: <a class="el" href="_get_last_error_8cpp.php#08a27e7e8262a89266902d7e8bd002ae">GetLastError.cpp</a>
, <a class="el" href="_get_last_error_8h.php#c4f875854ed858d52d160e42a9b62cc9">GetLastError.h</a>
, <a class="el" href="_get_last_error_8cpp.php#c159f66e5b0dbb1a8c978cb969db05f0">GetLastError.cpp</a>
<li>GetLibraryBody()
: <a class="el" href="body_8cpp.php#e7e526f89d8d0365ca2c446f7341cbe5">body.cpp</a>
<li>GetModuleFileName()
: <a class="el" href="namespace_ini_ext.php#e8ec57b84b079b6b4bcac40184ab24dc">path.cpp</a>
<li>GetModuleFullName()
: <a class="el" href="namespace_ini_ext.php#bb454be0d063b277b6b64868fbea522e">path.cpp</a>
<li>GetModuleName()
: <a class="el" href="namespace_ini_ext.php#a95b514880abb2c5fc111d58372d2635">path.cpp</a>
, <a class="el" href="emulator_8cpp.php#cda7731403cbaea2e632feb47cb2a8de">emulator.cpp</a>
<li>GetModuleRunDir()
: <a class="el" href="namespace_ini_ext.php#d57a751f4b0835a7f50f46e09eb4e582">path.cpp</a>
, <a class="el" href="log_8cpp.php#5237486e38d18105bd744c3bc60140bc">log.cpp</a>
<li>GetSystemDirectory()
: <a class="el" href="namespace_ini_ext.php#cb7001002fff0e81ee96f9da42701da8">path.cpp</a>
<li>GetTempFilePath()
: <a class="el" href="namespace_ini_ext.php#28417a9bc7b70f721d7d46aab86b98bf">path.cpp</a>
</ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
