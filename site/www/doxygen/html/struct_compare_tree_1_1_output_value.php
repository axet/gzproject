<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="class_compare_tree.php">CompareTree</a>::<a class="el" href="struct_compare_tree_1_1_output_value.php">OutputValue</a></div>
<h1>CompareTree&lt; LinkDataType &gt;::OutputValue Struct Reference</h1><!-- doxytag: class="CompareTree::OutputValue" --><code>#include &lt;<a class="el" href="_compare_tree_8h-source.php">CompareTree.h</a>&gt;</code>
<p>
<a href="struct_compare_tree_1_1_output_value-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Types</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">enum &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffc">Action</a> { <a class="el" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffca687aac0f1b9b33002dc072d213d738f">Increment</a>,
<a class="el" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffca445cd17414dce061116b26537586c53">Decrement</a>
 }</td></tr>

<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_compare_tree_1_1_output_value.php#8e40b5ccbed7079fb508bd8a02e61cc0">OutputValue</a> (<a class="el" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffc">Action</a> a, InputValueList::iterator i)</td></tr>

<tr><td colspan="2"><br><h2>Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">enum <a class="el" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffc">CompareTree::OutputValue::Action</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_compare_tree_1_1_output_value.php#825e0e4d3fef6615a13e2fce9705822c">action</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">LinkDataType&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_compare_tree_1_1_output_value.php#ffed88cfd0a7fa7189997669f408a97f">data</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class LinkDataType&gt;<br>
 struct CompareTree&lt; LinkDataType &gt;::OutputValue</h3>


<p>

<p>
Definition at line <a class="el" href="_compare_tree_8h-source.php#l00063">63</a> of file <a class="el" href="_compare_tree_8h-source.php">CompareTree.h</a>.<hr><h2>Member Enumeration Documentation</h2>
<a class="anchor" name="02e5aed19698253ad19dbee1d1224ffc"></a><!-- doxytag: member="CompareTree::OutputValue::Action" ref="02e5aed19698253ad19dbee1d1224ffc" args="" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class LinkDataType&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">enum <a class="el" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffc">CompareTree::OutputValue::Action</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
<dl compact><dt><b>Enumerator: </b></dt><dd>
<table border="0" cellspacing="2" cellpadding="0">
<tr><td valign="top"><em><a class="anchor" name="02e5aed19698253ad19dbee1d1224ffca687aac0f1b9b33002dc072d213d738f"></a><!-- doxytag: member="Increment" ref="02e5aed19698253ad19dbee1d1224ffca687aac0f1b9b33002dc072d213d738f" args="" -->Increment</em>&nbsp;</td><td>
</td></tr>
<tr><td valign="top"><em><a class="anchor" name="02e5aed19698253ad19dbee1d1224ffca445cd17414dce061116b26537586c53"></a><!-- doxytag: member="Decrement" ref="02e5aed19698253ad19dbee1d1224ffca445cd17414dce061116b26537586c53" args="" -->Decrement</em>&nbsp;</td><td>
</td></tr>
</table>
</dl>

<p>
Definition at line <a class="el" href="_compare_tree_8h-source.php#l00065">65</a> of file <a class="el" href="_compare_tree_8h-source.php">CompareTree.h</a>.<div class="fragment"><pre class="fragment"><a name="l00065"></a>00065 {<a class="code" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffca687aac0f1b9b33002dc072d213d738f">Increment</a>,<a class="code" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffca445cd17414dce061116b26537586c53">Decrement</a>} <a class="code" href="struct_compare_tree_1_1_output_value.php#825e0e4d3fef6615a13e2fce9705822c">action</a>;
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="8e40b5ccbed7079fb508bd8a02e61cc0"></a><!-- doxytag: member="CompareTree::OutputValue::OutputValue" ref="8e40b5ccbed7079fb508bd8a02e61cc0" args="(Action a, InputValueList::iterator i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class LinkDataType&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_compare_tree.php">CompareTree</a>&lt; LinkDataType &gt;::OutputValue::OutputValue           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffc">Action</a>&nbsp;</td>
          <td class="paramname"> <em>a</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">InputValueList::iterator&nbsp;</td>
          <td class="paramname"> <em>i</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_compare_tree_8h-source.php#l00068">68</a> of file <a class="el" href="_compare_tree_8h-source.php">CompareTree.h</a>.<div class="fragment"><pre class="fragment"><a name="l00068"></a>00068                                                   :<a class="code" href="struct_compare_tree_1_1_output_value.php#ffed88cfd0a7fa7189997669f408a97f">data</a>(i-&gt;data),<a class="code" href="struct_compare_tree_1_1_output_value.php#825e0e4d3fef6615a13e2fce9705822c">action</a>(a)
<a name="l00069"></a>00069     {
<a name="l00070"></a>00070     }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="825e0e4d3fef6615a13e2fce9705822c"></a><!-- doxytag: member="CompareTree::OutputValue::action" ref="825e0e4d3fef6615a13e2fce9705822c" args="" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class LinkDataType&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">enum <a class="el" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffc">CompareTree::OutputValue::Action</a>  <a class="el" href="class_compare_tree.php">CompareTree</a>&lt; LinkDataType &gt;::<a class="el" href="struct_compare_tree_1_1_output_value.php#825e0e4d3fef6615a13e2fce9705822c">OutputValue::action</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<a class="anchor" name="ffed88cfd0a7fa7189997669f408a97f"></a><!-- doxytag: member="CompareTree::OutputValue::data" ref="ffed88cfd0a7fa7189997669f408a97f" args="" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class LinkDataType&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">LinkDataType <a class="el" href="class_compare_tree.php">CompareTree</a>&lt; LinkDataType &gt;::<a class="el" href="struct_compare_tree_1_1_output_value.php#ffed88cfd0a7fa7189997669f408a97f">OutputValue::data</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_compare_tree_8h-source.php#l00066">66</a> of file <a class="el" href="_compare_tree_8h-source.php">CompareTree.h</a>.
</div>
</div><p>
<hr>The documentation for this struct was generated from the following file:<ul>
<li><a class="el" href="_compare_tree_8h-source.php">CompareTree.h</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
