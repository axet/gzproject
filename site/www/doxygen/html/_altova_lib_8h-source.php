<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_9aa33d2cd8dd981e9a7ee8bcdb7921b2.php">library</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_6082230fd4939409a029fb3498f63f15.php">Altova</a></div>
<h1>AltovaLib.h</h1><a href="_altova_lib_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#include &lt;WinSock2.h&gt;</span>
<a name="l00002"></a>00002 <span class="preprocessor">#include &lt;tchar.h&gt;</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="preprocessor">#pragma comment(lib,"altova.lib")</span>
<a name="l00005"></a>00005 <span class="preprocessor"></span><span class="preprocessor">#pragma comment(lib,"altovaxml.lib")</span>
<a name="l00006"></a>00006 <span class="preprocessor"></span>
<a name="l00007"></a>00007 <span class="preprocessor">#import "msxml3.dll"</span> named_guids no_implementation
<a name="l00008"></a>00008
<a name="l00009"></a>00009 <span class="preprocessor">#include "<a class="code" href="_altova_8h.php">Altova.h</a>"</span>
<a name="l00010"></a>00010 <span class="preprocessor">#include "<a class="code" href="_schema_types_8h.php">SchemaTypes.h</a>"</span>
<a name="l00011"></a>00011 <span class="preprocessor">#include "<a class="code" href="_altova_exception_8h.php">AltovaException.h</a>"</span>
<a name="l00012"></a>00012 <span class="preprocessor">#include "<a class="code" href="_schema_type_string_8h.php">SchemaTypeString.h</a>"</span>
<a name="l00013"></a>00013 <span class="preprocessor">#include "<a class="code" href="_schema_type_number_8h.php">SchemaTypeNumber.h</a>"</span>
<a name="l00014"></a>00014 <span class="preprocessor">#include "<a class="code" href="_schema_type_calendar_8h.php">SchemaTypeCalendar.h</a>"</span>
<a name="l00015"></a>00015 <span class="preprocessor">#include "<a class="code" href="_schema_type_binary_8h.php">SchemaTypeBinary.h</a>"</span>
<a name="l00016"></a>00016 <span class="preprocessor">#include "../AltovaXML/XmlException.h"</span>
<a name="l00017"></a>00017 <span class="preprocessor">#include "../AltovaXML/Doc.h"</span>
<a name="l00018"></a>00018 <span class="preprocessor">#include "../AltovaXML/Node.h"</span>
<a name="l00019"></a>00019 <span class="preprocessor">#include "<a class="code" href="_doc_addin_8h.php">DocAddin.h</a>"</span>
<a name="l00020"></a>00020 <span class="keyword">using namespace </span>altova;
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
