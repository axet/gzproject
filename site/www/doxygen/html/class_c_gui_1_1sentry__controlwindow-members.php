<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>CGui::sentry_controlwindow Member List</h1>This is the complete list of members for <a class="el" href="class_c_gui_1_1sentry__controlwindow.php">CGui::sentry_controlwindow</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_c_gui_1_1sentry__controlwindow.php#4a4c9a8e553e34e2d96b20525609b42f">m_h</a></td><td><a class="el" href="class_c_gui_1_1sentry__controlwindow.php">CGui::sentry_controlwindow</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_gui_1_1sentry__controlwindow.php#e97aee714443258e4db1479d2468d064">sentry_controlwindow</a>(HWND h)</td><td><a class="el" href="class_c_gui_1_1sentry__controlwindow.php">CGui::sentry_controlwindow</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_gui_1_1sentry__controlwindow.php#9297bf08d04fc49d4761f6ce8a665adf">~sentry_controlwindow</a>()</td><td><a class="el" href="class_c_gui_1_1sentry__controlwindow.php">CGui::sentry_controlwindow</a></td><td><code> [inline]</code></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
