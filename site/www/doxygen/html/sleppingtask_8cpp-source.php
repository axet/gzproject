<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0e80f8b385e544f89c616410da1961e3.php">ClientAddin</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_62517a2ed17a94014abd0a2cfd485e7b.php">clientsuck</a></div>
<h1>sleppingtask.cpp</h1><a href="sleppingtask_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#include "<a class="code" href="_g_z_2_client_addin_2clientsuck_2_std_afx_8h.php">stdafx.h</a>"</span>
<a name="l00002"></a>00002 <span class="preprocessor">#include "<a class="code" href="sleppingtask_8h.php">sleppingtask.h</a>"</span>
<a name="l00003"></a>00003
<a name="l00004"></a><a class="code" href="class_c_slepping_task.php#314afb3cae79ff0ec8facfca2fda43ba">00004</a> <a class="code" href="class_c_slepping_task.php#314afb3cae79ff0ec8facfca2fda43ba">CSleppingTask::CSleppingTask</a>():m_fp(0)
<a name="l00005"></a>00005 {
<a name="l00006"></a>00006 }
<a name="l00007"></a>00007
<a name="l00008"></a><a class="code" href="class_c_slepping_task.php#b7b40b0a75cb47216f48c297ac042660">00008</a> <span class="keywordtype">void</span> <a class="code" href="class_c_slepping_task.php#b7b40b0a75cb47216f48c297ac042660">CSleppingTask::SleppingTask</a>(MYCALL fp,DWORD d)
<a name="l00009"></a>00009 {
<a name="l00010"></a>00010   <a class="code" href="class_c_slepping_task.php#92c4980c8e8deb7ac7a3d49da7faa765">m_fp</a>=fp;
<a name="l00011"></a>00011   <a class="code" href="class_c_slepping_task.php#e548bf789cdf22f56dcd8a8ef79ed29f">m_fpprm</a>=d;
<a name="l00012"></a>00012   ::SendMessage(<a class="code" href="class_c_client_u_o_commands.php#ab6ce74de21d0f330aa3e65dbaaed763">GetWindow</a>(),WM_NULL,0,0);
<a name="l00013"></a>00013   <a class="code" href="class_c_slepping_task.php#92c4980c8e8deb7ac7a3d49da7faa765">m_fp</a>=0;
<a name="l00014"></a>00014 }
<a name="l00015"></a>00015
<a name="l00016"></a><a class="code" href="class_c_slepping_task.php#7c73b8f79d0930143136c1db3d3a25c6">00016</a> <span class="keywordtype">void</span> <a class="code" href="class_c_slepping_task.php#7c73b8f79d0930143136c1db3d3a25c6">CSleppingTask::WindowProc</a>(CMemMngr* memmngr,va_list args)
<a name="l00017"></a>00017 {
<a name="l00018"></a>00018   <a class="code" href="class_c_slepping_task.php">CSleppingTask</a>* thisclass=dynamic_cast&lt;CSleppingTask*&gt;(memmngr);
<a name="l00019"></a>00019   <span class="keywordflow">if</span>(thisclass-&gt;<a class="code" href="class_c_slepping_task.php#92c4980c8e8deb7ac7a3d49da7faa765">m_fp</a>!=0)
<a name="l00020"></a>00020     thisclass-&gt;<a class="code" href="class_c_slepping_task.php#92c4980c8e8deb7ac7a3d49da7faa765">m_fp</a>(thisclass-&gt;<a class="code" href="class_c_slepping_task.php#e548bf789cdf22f56dcd8a8ef79ed29f">m_fpprm</a>);
<a name="l00021"></a>00021
<a name="l00022"></a>00022   <a class="code" href="class_c_gui.php#1a969b8a13dddb0d93208f8900293b61">CGui::WindowProc</a>(memmngr,args);
<a name="l00023"></a>00023 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
