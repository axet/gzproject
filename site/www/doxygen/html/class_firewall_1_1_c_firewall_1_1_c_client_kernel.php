<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespace_firewall.php">Firewall</a>::<a class="el" href="class_firewall_1_1_c_firewall.php">CFirewall</a>::<a class="el" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php">CClientKernel</a></div>
<h1>Firewall::CFirewall::CClientKernel Class Reference</h1><!-- doxytag: class="Firewall::CFirewall::CClientKernel" --><!-- doxytag: inherits="Firewall::CClient" --><code>#include &lt;<a class="el" href="firewall_8h-source.php">firewall.h</a>&gt;</code>
<p>
<p>Inheritance diagram for Firewall::CFirewall::CClientKernel:
<p><center><img src="class_firewall_1_1_c_firewall_1_1_c_client_kernel.png" usemap="#Firewall::CFirewall::CClientKernel_map" border="0" alt=""></center>
<map name="Firewall::CFirewall::CClientKernel_map">
<area href="class_firewall_1_1_c_client.php" alt="Firewall::CClient" shape="rect" coords="316,112,517,136">
<area href="class_c_client_interface.php" alt="CClientInterface" shape="rect" coords="0,56,201,80">
<area href="class_c_client_crypt.php" alt="CClientCrypt" shape="rect" coords="211,56,412,80">
<area href="class_c_client_net_events.php" alt="CClientNetEvents" shape="rect" coords="422,56,623,80">
<area href="class_client_secure_1_1_c_client_secure.php" alt="ClientSecure::CClientSecure" shape="rect" coords="633,56,834,80">
<area href="class_eth_interface.php" alt="EthInterface" shape="rect" coords="0,0,201,24">
<area href="class_c_server_addin_module.php" alt="CServerAddinModule" shape="rect" coords="527,0,728,24">
<area href="class_client_secure_1_1_secure_connection.php" alt="ClientSecure::SecureConnection" shape="rect" coords="738,0,939,24">
</map>
<a href="class_firewall_1_1_c_firewall_1_1_c_client_kernel-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#afc2e365f4980451c81607faa3e45c93">CClientKernel</a> (<a class="el" href="class_firewall_1_1_c_firewall.php">CFirewall</a> *, SOCKET s)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#910f35539dfd93b2a1c3a2701b048689">~CClientKernel</a> ()</td></tr>

<tr><td colspan="2"><br><h2>Private Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#8249c6f5fa668f50674aa505e31bcc68">AddController</a> (<a class="el" href="class_c_client_net_events.php">CClientNetEvents</a> *)</td></tr>

<tr><td colspan="2"><br><h2>Private Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">std::stack&lt; <a class="el" href="class_c_client_net_events.php">CClientNetEvents</a> * &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#96a11d68a9173fe5548ad3a3b4faceb2">m_netevents</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="firewall_8h-source.php#l00064">64</a> of file <a class="el" href="firewall_8h-source.php">firewall.h</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="afc2e365f4980451c81607faa3e45c93"></a><!-- doxytag: member="Firewall::CFirewall::CClientKernel::CClientKernel" ref="afc2e365f4980451c81607faa3e45c93" args="(CFirewall *, SOCKET s)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">CFirewall::CClientKernel::CClientKernel           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_firewall_1_1_c_firewall.php">CFirewall</a> *&nbsp;</td>
          <td class="paramname">, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">SOCKET&nbsp;</td>
          <td class="paramname"> <em>s</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="firewall_8cpp-source.php#l00146">146</a> of file <a class="el" href="firewall_8cpp-source.php">firewall.cpp</a>.
<p>
References <a class="el" href="firewall_8cpp-source.php#l00152">AddController()</a>, and <a class="el" href="firewall_8h-source.php#l00029">Firewall::CFirewall::m_modules</a>.<div class="fragment"><pre class="fragment"><a name="l00146"></a>00146                                                           :<a class="code" href="class_firewall_1_1_c_client.php#e91a79de299b1d3681310aaa8fba90ca">CClient</a>(s)
<a name="l00147"></a>00147 {
<a name="l00148"></a>00148   <span class="keywordflow">for</span>(CFirewall::modules_t::iterator i=pp-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#093c71a976a3ad2a0f074468411a37fb">m_modules</a>.begin();i!=pp-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#093c71a976a3ad2a0f074468411a37fb">m_modules</a>.end();i++)
<a name="l00149"></a>00149     <a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#8249c6f5fa668f50674aa505e31bcc68">AddController</a>(*i);
<a name="l00150"></a>00150 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="910f35539dfd93b2a1c3a2701b048689"></a><!-- doxytag: member="Firewall::CFirewall::CClientKernel::~CClientKernel" ref="910f35539dfd93b2a1c3a2701b048689" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">CFirewall::CClientKernel::~CClientKernel           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="firewall_8cpp-source.php#l00157">157</a> of file <a class="el" href="firewall_8cpp-source.php">firewall.cpp</a>.
<p>
References <a class="el" href="firewall_8h-source.php#l00066">m_netevents</a>, and <a class="el" href="clientinterface_8cpp-source.php#l00024">CClientInterface::RemoveAdvise()</a>.<div class="fragment"><pre class="fragment"><a name="l00158"></a>00158 {
<a name="l00159"></a>00159   <span class="keywordflow">while</span>(!<a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#96a11d68a9173fe5548ad3a3b4faceb2">m_netevents</a>.empty())
<a name="l00160"></a>00160   {
<a name="l00161"></a>00161     <a class="code" href="class_c_client_interface.php#c8d8f8ef14a2a792ebe23d2ddbc856f9">RemoveAdvise</a>(<a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#96a11d68a9173fe5548ad3a3b4faceb2">m_netevents</a>.top());
<a name="l00162"></a>00162     <a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#96a11d68a9173fe5548ad3a3b4faceb2">m_netevents</a>.pop();
<a name="l00163"></a>00163   }
<a name="l00164"></a>00164 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="8249c6f5fa668f50674aa505e31bcc68"></a><!-- doxytag: member="Firewall::CFirewall::CClientKernel::AddController" ref="8249c6f5fa668f50674aa505e31bcc68" args="(CClientNetEvents *)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CFirewall::CClientKernel::AddController           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a> *&nbsp;</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [private]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="firewall_8cpp-source.php#l00152">152</a> of file <a class="el" href="firewall_8cpp-source.php">firewall.cpp</a>.
<p>
References <a class="el" href="clientinterface_8cpp-source.php#l00018">CClientInterface::AddAdvise()</a>, and <a class="el" href="firewall_8h-source.php#l00066">m_netevents</a>.
<p>
Referenced by <a class="el" href="firewall_8cpp-source.php#l00146">CClientKernel()</a>.<div class="fragment"><pre class="fragment"><a name="l00153"></a>00153 {
<a name="l00154"></a>00154   <a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#96a11d68a9173fe5548ad3a3b4faceb2">m_netevents</a>.push(<a class="code" href="class_c_client_interface.php#445ef60309443464cb4da7e710f9fa1d">AddAdvise</a>(p));
<a name="l00155"></a>00155 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="96a11d68a9173fe5548ad3a3b4faceb2"></a><!-- doxytag: member="Firewall::CFirewall::CClientKernel::m_netevents" ref="96a11d68a9173fe5548ad3a3b4faceb2" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">std::stack&lt;<a class="el" href="class_c_client_net_events.php">CClientNetEvents</a>*&gt; <a class="el" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#96a11d68a9173fe5548ad3a3b4faceb2">Firewall::CFirewall::CClientKernel::m_netevents</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="firewall_8h-source.php#l00066">66</a> of file <a class="el" href="firewall_8h-source.php">firewall.h</a>.
<p>
Referenced by <a class="el" href="firewall_8cpp-source.php#l00152">AddController()</a>, and <a class="el" href="firewall_8cpp-source.php#l00157">~CClientKernel()</a>.
</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="firewall_8h-source.php">firewall.h</a><li><a class="el" href="firewall_8cpp-source.php">firewall.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
