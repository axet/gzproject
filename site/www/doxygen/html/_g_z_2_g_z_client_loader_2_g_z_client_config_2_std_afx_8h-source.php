<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_52f2866252e58876aa460cdbcef5f577.php">GZClientLoader</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_047fd9d3f985bb564ea29a1bfd6196d7.php">GZClientConfig</a></div>
<h1>GZ/GZClientLoader/GZClientConfig/StdAfx.h</h1><a href="_g_z_2_g_z_client_loader_2_g_z_client_config_2_std_afx_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// stdafx.h : јР·ЗЁtІО Include АЙЄє Include АЙЎA</span>
<a name="l00002"></a>00002 <span class="comment">// ©О¬Oёg±`ЁПҐО«o«Ь¤ЦЕЬ§уЄє±M®Ч±MҐО Include АЙ®Ч</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="preprocessor">#pragma once</span>
<a name="l00005"></a>00005 <span class="preprocessor"></span>
<a name="l00006"></a>00006 <span class="preprocessor">#ifndef VC_EXTRALEAN</span>
<a name="l00007"></a><a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2_std_afx_8h.php#0172fbace36625330d5f0f163a1ddc1a">00007</a> <span class="preprocessor"></span><span class="preprocessor">#define VC_EXTRALEAN            // ±q Windows јРАY±Ж°Ј¤Ј±`ЁПҐОЄє¦Ё­ы</span>
<a name="l00008"></a>00008 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00009"></a>00009 <span class="preprocessor"></span>
<a name="l00010"></a>00010 <span class="comment">// ¦pЄG±z¦іҐІ¶·АuҐэїпЁъЄєҐ­ҐxЎAЅР­Ч§п¤U¦C©wёqЎC</span>
<a name="l00011"></a>00011 <span class="comment">// °С¦Т MSDN Ёъ±o¤Ј¦PҐ­Ґx№пАі­ИЄєіМ·sёк°TЎC</span>
<a name="l00012"></a>00012 <span class="preprocessor">#ifndef WINVER                          // ¤№і\ЁПҐО Windows 95 »P Windows NT 4 (§t) ҐH«бЄ©Ґ»ЄєЇS©wҐ\ЇаЎC</span>
<a name="l00013"></a><a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2_std_afx_8h.php#966cd377b9f3fdeb1432460c33352af1">00013</a> <span class="preprocessor"></span><span class="preprocessor">#define WINVER 0x0400           // ±NҐ¦ЕЬ§у¬°°w№п Windows 98 ©M Windows 2000 (§t) ҐH«бЄ©Ґ»ѕA·нЄє­ИЎC</span>
<a name="l00014"></a>00014 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00015"></a>00015 <span class="preprocessor"></span>
<a name="l00016"></a>00016 <span class="preprocessor">#ifndef _WIN32_WINNT            // ¤№і\ЁПҐО Windows NT 4 (§t) ҐH«бЄ©Ґ»ЄєЇS©wҐ\ЇаЎC</span>
<a name="l00017"></a><a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2_std_afx_8h.php#c50762666aa00bd3a4308158510f1748">00017</a> <span class="preprocessor"></span><span class="preprocessor">#define _WIN32_WINNT 0x0400             // ±NҐ¦ЕЬ§у¬°°w№п Windows 98 ©M Windows 2000 (§t) ҐH«бЄ©Ґ»ѕA·нЄє­ИЎC</span>
<a name="l00018"></a>00018 <span class="preprocessor"></span><span class="preprocessor">#endif                                          </span>
<a name="l00019"></a>00019 <span class="preprocessor"></span>
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef _WIN32_WINDOWS          // ¤№і\ЁПҐО Windows 98 (§t) ҐH«бЄ©Ґ»ЄєЇS©wҐ\ЇаЎC</span>
<a name="l00021"></a><a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2_std_afx_8h.php#074ca98c073d899c62fc6629918186c8">00021</a> <span class="preprocessor"></span><span class="preprocessor">#define _WIN32_WINDOWS 0x0410 // ±NҐ¦ЕЬ§у¬°°w№п Windows Me (§t) ҐH«бЄ©Ґ»ѕA·нЄє­ИЎC</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00023"></a>00023 <span class="preprocessor"></span>
<a name="l00024"></a>00024 <span class="preprocessor">#ifndef _WIN32_IE                       // ¤№і\ЁПҐО IE 4.0 (§t) ҐH«бЄ©Ґ»ЄєЇS©wҐ\ЇаЎC</span>
<a name="l00025"></a><a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2_std_afx_8h.php#d4562ce705fe4682e63dc8f1ea9dd344">00025</a> <span class="preprocessor"></span><span class="preprocessor">#define _WIN32_IE 0x0400        // ±NҐ¦ЕЬ§у¬°°w№п IE 5.0 (§t) ҐH«бЄ©Ґ»ѕA·нЄє­ИЎC</span>
<a name="l00026"></a>00026 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00027"></a>00027 <span class="preprocessor"></span>
<a name="l00028"></a><a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2_std_afx_8h.php#137e19a46f145129447af81af06def9f">00028</a> <span class="preprocessor">#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // ©ъЅT©wёqіЎ¤АЄє CString «ШєcЁз¦Ў</span>
<a name="l00029"></a>00029 <span class="preprocessor"></span>
<a name="l00030"></a>00030 <span class="comment">// Гці¬ MFC БфВГ¤@ЁЗ±`ЁЈ©ОҐi©їІ¤Дµ§i°T®§ЄєҐ\Їа</span>
<a name="l00031"></a><a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2_std_afx_8h.php#b5fd5e01d3484ef3116a878a65b82980">00031</a> <span class="preprocessor">#define _AFX_ALL_WARNINGS</span>
<a name="l00032"></a>00032 <span class="preprocessor"></span>
<a name="l00033"></a>00033 <span class="preprocessor">#include &lt;afxwin.h&gt;</span>         <span class="comment">// MFC ®Ц¤Я»PјР·З¤ёҐу</span>
<a name="l00034"></a>00034 <span class="preprocessor">#include &lt;afxext.h&gt;</span>         <span class="comment">// MFC ВXҐRҐ\Їа</span>
<a name="l00035"></a>00035 <span class="preprocessor">#include &lt;afxdisp.h&gt;</span>        <span class="comment">// MFC Automation Гю§O</span>
<a name="l00036"></a>00036
<a name="l00037"></a>00037 <span class="preprocessor">#include &lt;afxdtctl.h&gt;</span>           <span class="comment">// MFC ¤дґ©Єє Internet Explorer 4 іqҐО±±Ёо¶µ</span>
<a name="l00038"></a>00038 <span class="preprocessor">#ifndef _AFX_NO_AFXCMN_SUPPORT</span>
<a name="l00039"></a>00039 <span class="preprocessor"></span><span class="preprocessor">#include &lt;afxcmn.h&gt;</span>                     <span class="comment">// MFC ¤дґ©Єє Windows іqҐО±±Ёо¶µ</span>
<a name="l00040"></a>00040 <span class="preprocessor">#endif // _AFX_NO_AFXCMN_SUPPORT</span>
<a name="l00041"></a>00041 <span class="preprocessor"></span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
