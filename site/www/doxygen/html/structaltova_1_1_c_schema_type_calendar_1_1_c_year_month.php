<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespacealtova.php">altova</a>::<a class="el" href="classaltova_1_1_c_schema_type_calendar.php">CSchemaTypeCalendar</a>::<a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php">CYearMonth</a></div>
<h1>altova::CSchemaTypeCalendar::CYearMonth Struct Reference</h1><!-- doxytag: class="altova::CSchemaTypeCalendar::CYearMonth" --><!-- doxytag: inherits="altova::CSchemaTypeCalendar::CDateTimeBase" --><code>#include &lt;<a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>&gt;</code>
<p>
<p>Inheritance diagram for altova::CSchemaTypeCalendar::CYearMonth:
<p><center><img src="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.png" usemap="#altova::CSchemaTypeCalendar::CYearMonth_map" border="0" alt=""></center>
<map name="altova::CSchemaTypeCalendar::CYearMonth_map">
<area href="structaltova_1_1_c_schema_type_calendar_1_1_c_date_time_base.php" alt="altova::CSchemaTypeCalendar::CDateTimeBase" shape="rect" coords="0,0,281,24">
</map>
<a href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#f07d541b3f8d4f55da0bbde027ca6d18">CYearMonth</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#abf6dd014af161ded33d13bfcc136108">CYearMonth</a> (int _nYear, int _nMonth)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">long&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#edfe245d308962c8d280861d25632cc0">NormalizedMonths</a> () const</td></tr>

<tr><td colspan="2"><br><h2>Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#01803cb779c4e8ff2393ca7613f25c62">nYear</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#9e08dbc896150e165e9864fa84320cc9">nMonth</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00242">242</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="f07d541b3f8d4f55da0bbde027ca6d18"></a><!-- doxytag: member="altova::CSchemaTypeCalendar::CYearMonth::CYearMonth" ref="f07d541b3f8d4f55da0bbde027ca6d18" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">altova::CSchemaTypeCalendar::CYearMonth::CYearMonth           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00244">244</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.<div class="fragment"><pre class="fragment"><a name="l00245"></a>00245                 {
<a name="l00246"></a>00246                         time_t long_time;
<a name="l00247"></a>00247                         time( &amp;long_time );
<a name="l00248"></a>00248                         <span class="keyword">struct </span>tm *newtime = localtime( &amp;long_time );
<a name="l00249"></a>00249                         <a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#01803cb779c4e8ff2393ca7613f25c62">nYear</a> = newtime-&gt;tm_year + 1900;
<a name="l00250"></a>00250                         <a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#9e08dbc896150e165e9864fa84320cc9">nMonth</a> = newtime-&gt;tm_mon + 1;
<a name="l00251"></a>00251                 };
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="abf6dd014af161ded33d13bfcc136108"></a><!-- doxytag: member="altova::CSchemaTypeCalendar::CYearMonth::CYearMonth" ref="abf6dd014af161ded33d13bfcc136108" args="(int _nYear, int _nMonth)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">altova::CSchemaTypeCalendar::CYearMonth::CYearMonth           </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>_nYear</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>_nMonth</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00253">253</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.<div class="fragment"><pre class="fragment"><a name="l00257"></a>00257                         : <a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#01803cb779c4e8ff2393ca7613f25c62">nYear</a>( _nYear )
<a name="l00258"></a>00258                         , <a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#9e08dbc896150e165e9864fa84320cc9">nMonth</a>( _nMonth )
<a name="l00259"></a>00259                 {};
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="edfe245d308962c8d280861d25632cc0"></a><!-- doxytag: member="altova::CSchemaTypeCalendar::CYearMonth::NormalizedMonths" ref="edfe245d308962c8d280861d25632cc0" args="() const" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">long altova::CSchemaTypeCalendar::CYearMonth::NormalizedMonths           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00264">264</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.<div class="fragment"><pre class="fragment"><a name="l00264"></a>00264 { <span class="keywordflow">return</span> <a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#01803cb779c4e8ff2393ca7613f25c62">nYear</a>*12+<a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#9e08dbc896150e165e9864fa84320cc9">nMonth</a>; }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="01803cb779c4e8ff2393ca7613f25c62"></a><!-- doxytag: member="altova::CSchemaTypeCalendar::CYearMonth::nYear" ref="01803cb779c4e8ff2393ca7613f25c62" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#01803cb779c4e8ff2393ca7613f25c62">altova::CSchemaTypeCalendar::CYearMonth::nYear</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00259">259</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.
</div>
</div><p>
<a class="anchor" name="9e08dbc896150e165e9864fa84320cc9"></a><!-- doxytag: member="altova::CSchemaTypeCalendar::CYearMonth::nMonth" ref="9e08dbc896150e165e9864fa84320cc9" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_year_month.php#9e08dbc896150e165e9864fa84320cc9">altova::CSchemaTypeCalendar::CYearMonth::nMonth</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00262">262</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.
</div>
</div><p>
<hr>The documentation for this struct was generated from the following file:<ul>
<li><a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
