<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>CCIQGoodsUnit Member List</h1>This is the complete list of members for <a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#60b5c3206812ce5ef851296673869816">CCIQGoodsUnit</a>()</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#98ba926de44b43f5e8d3c125c7b27b9c">Create</a>(HWND parent, CUnit *p)</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#dd92bbeb29642e25ae38b6deb392f2c1">ExitInstance</a>()</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#67af8b00c32dd37250c16e831591b440">Goods</a>()</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#244bac08d9df6c612d4570ea16ce9ec0">Harvest</a>()</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#0a611cdff2c408e4d7c3a8055760bd4d">InitInstance</a>()</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#40c6a565a9f77724c9e4d96c785ba0d2">IsGhost</a>()</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#5d0d753b700a0bee5c2d3dbf87ab798d">IsOverloaded</a>()</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#a3268bac372a46487ec612d8a79a0f95">m_backpackser</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#4f573ba0d29645cac87da2c02f72093d">m_catchplayercords</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#7d7692404e5ef1ada947a896fc023d59">m_charname</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#9b3be41c3250aadbaef3838645ee9399">m_commands</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#bdfb615bb88885265d07d386df5fd2af">m_count</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#7738c49a8d054cb7ca5d53b07f8f0bc1">m_goodsitemtype</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#4e3f92b4c533ef4a61e811496ff85108">m_harvestuseitemtype</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#a1928067c2f426acb087349926a1aec8">m_makeitemtype</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#91ba72363105346fb58ec5824840b353">m_makeuseitemtype</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#7beb8bfddb6f5bd0d74c588a1c9747b4">m_parent</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#56df92e7f33d32b8217097ef0f04a0ef">m_pt</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#c6a0274cb0c4cbb594beeffc7043c24f">m_vendorname</a></td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#88bcbf341c3c347599f7061669ef7adc">msgs</a> enum name</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#3f2fae72529811ac0f23cd6f54d3f9963f3fca5abeddf62a7d24c4ce7775a8b4">msgStart</a> enum value</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#6718ea53cd54656adf8bb8f829f6801a">OnStart</a>(unsigned, long)</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#f86022b9c41ac00effda9499bd16af19">operator CUnit *</a>()</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#7ac7821baa336e9a24e13a53d87cdd74">operator-&gt;</a>()</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#782dd9c0edd983305ca64c10c64b48c0">Sell</a>()</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_c_i_q_goods_unit.php#6d042d5d8b4588cfcdb959e288635c54">~CCIQGoodsUnit</a>()</td><td><a class="el" href="class_c_c_i_q_goods_unit.php">CCIQGoodsUnit</a></td><td><code> [protected, virtual]</code></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
