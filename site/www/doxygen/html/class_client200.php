<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>Client200 Class Reference</h1><!-- doxytag: class="Client200" --><!-- doxytag: inherits="CClientUO" -->Класс управления клиентом.
<a href="#_details">More...</a>
<p>
<code>#include &lt;<a class="el" href="client200_8h-source.php">client200.h</a>&gt;</code>
<p>
<p>Inheritance diagram for Client200:
<p><center><img src="class_client200.png" usemap="#Client200_map" border="0" alt=""></center>
<map name="Client200_map">
<area href="class_c_client_u_o.php" alt="CClientUO" shape="rect" coords="1314,336,1523,360">
<area href="class_c_path_finding.php" alt="CPathFinding" shape="rect" coords="0,280,209,304">
<area href="class_c_client_u_o_commands.php" alt="CClientUOCommands" shape="rect" coords="438,280,647,304">
<area href="class_c_gui.php" alt="CGui" shape="rect" coords="876,280,1085,304">
<area href="class_c_slepping_task.php" alt="CSleppingTask" shape="rect" coords="1314,280,1523,304">
<area href="class_client_u_o_1_1_c_login.php" alt="ClientUO::CLogin" shape="rect" coords="1752,280,1961,304">
<area href="class_c_out_of_proc_slave.php" alt="COutOfProcSlave" shape="rect" coords="2190,280,2399,304">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="2628,280,2837,304">
<area href="class_client_secure_1_1_secure_connection.php" alt="ClientSecure::SecureConnection" shape="rect" coords="2847,224,3056,248">
<area href="class_c_client_crypt.php" alt="CClientCrypt" shape="rect" coords="2847,168,3056,192">
<area href="class_i_o_fluke_1_1_c_mem_mngr.php" alt="IOFluke::CMemMngr" shape="rect" coords="2847,112,3056,136">
<area href="class_c_client_commands.php" alt="CClientCommands" shape="rect" coords="2409,224,2618,248">
<area href="class_c_client_events.php" alt="CClientEvents" shape="rect" coords="2409,168,2618,192">
<area href="class_c_inter_proc_client_addin.php" alt="CInterProcClientAddin" shape="rect" coords="2409,112,2618,136">
<area href="class_c_inter_proc_slave.php" alt="CInterProcSlave" shape="rect" coords="2409,56,2618,80">
<area href="class_c_gui.php" alt="CGui" shape="rect" coords="1971,224,2180,248">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="1971,168,2180,192">
<area href="class_c_gui.php" alt="CGui" shape="rect" coords="1533,224,1742,248">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="1533,168,1742,192">
<area href="class_c_client_u_o_commands.php" alt="CClientUOCommands" shape="rect" coords="1095,224,1304,248">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="1095,168,1304,192">
<area href="class_client_objects.php" alt="ClientObjects" shape="rect" coords="657,224,866,248">
<area href="class_c_out_of_proc_slave.php" alt="COutOfProcSlave" shape="rect" coords="657,168,866,192">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="657,112,866,136">
<area href="class_client_objects.php" alt="ClientObjects" shape="rect" coords="219,224,428,248">
<area href="class_c_slepping_task.php" alt="CSleppingTask" shape="rect" coords="219,168,428,192">
<area href="class_c_gui.php" alt="CGui" shape="rect" coords="219,112,428,136">
<area href="class_c_client_u_o_commands.php" alt="CClientUOCommands" shape="rect" coords="219,56,428,80">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="219,0,428,24">
</map>
<a href="class_client200-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client200.php#f888213af004eb3f5172940fd00d5e36">DisableClientCrypt</a> ()</td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>
Класс управления клиентом.
<p>

<p>
Definition at line <a class="el" href="client200_8h-source.php#l00005">5</a> of file <a class="el" href="client200_8h-source.php">client200.h</a>.<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="f888213af004eb3f5172940fd00d5e36"></a><!-- doxytag: member="Client200::DisableClientCrypt" ref="f888213af004eb3f5172940fd00d5e36" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Client200::DisableClientCrypt           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implements <a class="el" href="class_client_u_o_secure_1_1_c_client_u_o_secure.php#289ab9c72c80498dd216c3b377f9de68">ClientUOSecure::CClientUOSecure</a>.
<p>
Definition at line <a class="el" href="client200_8cpp-source.php#l00004">4</a> of file <a class="el" href="client200_8cpp-source.php">client200.cpp</a>.
<p>
References <a class="el" href="class_client_u_o_secure_1_1_c_client_u_o_secure.php#289ab9c72c80498dd216c3b377f9de68">ClientUOSecure::CClientUOSecure::DisableClientCrypt()</a>, and <a class="el" href="class_i_o_fluke_1_1_c_mem_mngr.php#67a8d2aab08524b7454835cd64fe70f1">IOFluke::CMemMngr::PatchProgram()</a>.<div class="fragment"><pre class="fragment"><a name="l00005"></a>00005 {
<a name="l00006"></a>00006   <span class="keywordtype">unsigned</span> <span class="keywordtype">char</span> path[]={0x90,0x90};
<a name="l00007"></a>00007
<a name="l00008"></a>00008   <a class="code" href="class_i_o_fluke_1_1_c_mem_mngr.php#67a8d2aab08524b7454835cd64fe70f1">PatchProgram</a>(0x0040F79C,path,<span class="keyword">sizeof</span>(path));
<a name="l00009"></a>00009   <a class="code" href="class_i_o_fluke_1_1_c_mem_mngr.php#67a8d2aab08524b7454835cd64fe70f1">PatchProgram</a>(0x004BAF22,path,<span class="keyword">sizeof</span>(path));
<a name="l00010"></a>00010
<a name="l00011"></a>00011   <a class="code" href="class_client_u_o_secure_1_1_c_client_u_o_secure.php#289ab9c72c80498dd216c3b377f9de68">CClientUO::DisableClientCrypt</a>();
<a name="l00012"></a>00012 }
</pre></div>
<p>

</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="client200_8h-source.php">client200.h</a><li><a class="el" href="client200_8cpp-source.php">client200.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
