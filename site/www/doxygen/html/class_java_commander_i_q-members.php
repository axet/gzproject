<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>JavaCommanderIQ Member List</h1>This is the complete list of members for <a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#93b51b186f28ba0a77233aff7f709ad7">appletobj</a></td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#4667f387ab480dd78196c7935bd64a82">clientgameeventsclass</a></td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#76904ac792b755d1b8681af43fe36256">commanderiqclass</a></td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_jar_loader.php#b217589b8c3c47c312ec8a0fb5e88067">Create</a>(JNIEnv *env, const char *jarfile, JarLoader *parent=0)</td><td><a class="el" href="class_jar_loader.php">JarLoader</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#643ac9ea84f44a0d8411a50f1f7135b1">FormCreate</a>(HWND parent, CGameMap *, CCommanderIQEvents *)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#1572485f61d633eb8644e87604cbb1cc">FormGet</a>()</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#9b23a7db8c7fc6d0dee7a72ad0cbe97d">FormGetName</a>()</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#3109a95661d1b004901e1f1a0acacb93">gamemapeventsclass</a></td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#f10cff7130f9c2af7de56d8fbaf8adc2">JavaCommanderIQ</a>(JavaBase *b, const char *jar, const char *classname, UnitNativeManager *p, GameMapManager *gm)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#eccd24bdc9cd7063c468f4196f758e8c">Load</a>(const char *)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_jar_loader.php#262674702153a2d3e7efa527ae61c8c7">LoadClass</a>(const char *p)</td><td><a class="el" href="class_jar_loader.php">JarLoader</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#5cd86fc36d18c77212b688126d6c5337">m_base</a></td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#03c35128a89caf89dbec197d1bc666b3">m_cev</a></td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#9a51f66d48e0d0e09d6cf051b222a670">m_formname</a></td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#150ab419b4bca6cd59fca8d02b988491">m_gm</a></td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#86f7e00f62b741aa6dd44dda716535f5">m_unitnativemanager</a></td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#e2e3edc421ca72425ebe564e4fd2dba3">m_wnd</a></td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_jar_loader.php#d96fd89fded3f197c13132706a1390a6">NewInstance</a>(jclass)</td><td><a class="el" href="class_jar_loader.php">JarLoader</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#1454c7e47ccb134ef1b13a61865c7917">PointHiLight</a>(CGameMap::point *)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#063bc04531553ec9f210d18cf1e55cf3">PointMoveAfter</a>(CGameMap::point *)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#4f54016d4136011b90aacc32fb3e50ca">PointMoveBefore</a>(CGameMap::point *)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#03ce55549a39dfc286ab6f26bb7dd223">PointSelect</a>(CGameMap::point *)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#12f05ae50dc453a7384475310e46ac5f">Save</a>()</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#7c5cad6ed48d8c107e958a409cd65e0a">SelectedGround</a>(WorldCord w, const Client::ItemType &amp;)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_game_events.php#d0a758baa7b0140292d4c9caa25e7d92">CClientGameEvents::SelectedGround</a>(unsigned x, unsigned y, const ItemType &amp;)</td><td><a class="el" href="class_c_client_game_events.php">CClientGameEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#d95822c7e76b69d2749ee011788b3856">SelectedItem</a>(unsigned itemserial)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#06e72d1360f7d4ca051322f592a3d09c">SelectedOption</a>(const Client::ItemType &amp;)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_game_events.php#530a5790f49e0fff5df1cc2ea128dba8">CClientGameEvents::SelectedOption</a>(const ItemType &amp;)</td><td><a class="el" href="class_c_client_game_events.php">CClientGameEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#fee78ec5de4f3e06f259060c3d92a86d">SelectedPath</a>(WorldCord w)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_game_events.php#d79b2fe924b9a620dc4fea56b2603067">CClientGameEvents::SelectedPath</a>(unsigned x, unsigned y)</td><td><a class="el" href="class_c_client_game_events.php">CClientGameEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#0c701e394d68d23464b0b6a0007d6735">UnitAdd</a>(CUnit *)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#295949bd0541095769d100435baa9eb0">UnitGet</a>(int index)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#f619e2bfef55b7b9caab0ebc97f54d36">UnitGetCount</a>()</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_commander_i_q.php#b1064a59c8f0e8689747e32b58c123da">UnitMissing</a>()</td><td><a class="el" href="class_c_commander_i_q.php">CCommanderIQ</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#0434b93b20f142edf913c3f8df450b5f">UnitRemove</a>(CUnit *)</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_commander_i_q.php#734e903064642bc183c2db68c5456ee1">~CCommanderIQ</a>()</td><td><a class="el" href="class_c_commander_i_q.php">CCommanderIQ</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_commander_i_q.php#734e903064642bc183c2db68c5456ee1">~CCommanderIQ</a>()</td><td><a class="el" href="class_c_commander_i_q.php">CCommanderIQ</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_commander_i_q.php#734e903064642bc183c2db68c5456ee1">~CCommanderIQ</a>()</td><td><a class="el" href="class_c_commander_i_q.php">CCommanderIQ</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_commander_i_q.php#734e903064642bc183c2db68c5456ee1">~CCommanderIQ</a>()</td><td><a class="el" href="class_c_commander_i_q.php">CCommanderIQ</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_jar_loader.php#65815fc321e7d0df9b50080b6def6a85">~JarLoader</a>()</td><td><a class="el" href="class_jar_loader.php">JarLoader</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_java_commander_i_q.php#a4d1ea1936f52a0ee02680a60191f6bf">~JavaCommanderIQ</a>()</td><td><a class="el" href="class_java_commander_i_q.php">JavaCommanderIQ</a></td><td><code> [virtual]</code></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
