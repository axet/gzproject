<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_285c2ba7298296be1db6222076863471.php">GZAdministrator</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_82dab37c07783b34922e64ff31c5b2a4.php">SchemaAccountManager</a></div>
<h1>ConfigType.cs</h1><a href="_config_type_8cs.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">//</span>
<a name="l00002"></a>00002 <span class="comment">// ConfigType.cs.cs</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file was generated by XMLSPY 5 Enterprise Edition.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00007"></a>00007 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00008"></a>00008 <span class="comment">//</span>
<a name="l00009"></a>00009 <span class="comment">// Refer to the XMLSPY Documentation for further details.</span>
<a name="l00010"></a>00010 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012
<a name="l00013"></a>00013
<a name="l00014"></a>00014 <span class="keyword">using</span> System;
<a name="l00015"></a>00015 <span class="keyword">using</span> System.Xml;
<a name="l00016"></a>00016 <span class="keyword">using</span> Altova.Types;
<a name="l00017"></a>00017
<a name="l00018"></a><a class="code" href="namespace_schema_account_manager.php">00018</a> <span class="keyword">namespace </span>SchemaAccountManager
<a name="l00019"></a>00019 {
<a name="l00020"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php">00020</a>         <span class="keyword">public</span> <span class="keyword">class </span><a class="code" href="class_schema_account_manager_1_1_config_type.php">ConfigType</a> : Altova.Node
<a name="l00021"></a>00021         {
<a name="l00022"></a>00022 <span class="preprocessor">                #region Forward constructors</span>
<a name="l00023"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#f7ef95a769eb30139b5080086ba0301e">00023</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#f7ef95a769eb30139b5080086ba0301e">ConfigType</a>() : base() {}
<a name="l00024"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#69c079f9d172cc58e5f641328f74f28b">00024</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#f7ef95a769eb30139b5080086ba0301e">ConfigType</a>(XmlDocument doc) : base(doc) {}
<a name="l00025"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#adf1400f68336027b85c8f36c71f3763">00025</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#f7ef95a769eb30139b5080086ba0301e">ConfigType</a>(XmlNode node) : base(node) {}
<a name="l00026"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#8d224d1148b84c5a333238c7324ea8e7">00026</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#f7ef95a769eb30139b5080086ba0301e">ConfigType</a>(Altova.Node node) : base(node) {}
<a name="l00027"></a>00027 <span class="preprocessor">                #endregion // Forward constructors</span>
<a name="l00028"></a>00028 <span class="preprocessor"></span>
<a name="l00029"></a>00029 <span class="preprocessor">                #region SpherePath accessor methods</span>
<a name="l00030"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#bf1cc804b14325b165c21a4cdef85780">00030</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#bf1cc804b14325b165c21a4cdef85780">GetSpherePathMinCount</a>()
<a name="l00031"></a>00031                 {
<a name="l00032"></a>00032                         <span class="keywordflow">return</span> 1;
<a name="l00033"></a>00033                 }
<a name="l00034"></a>00034
<a name="l00035"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#4f7af8efb2cf4d38e6e30ffa9177e1d9">00035</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#4f7af8efb2cf4d38e6e30ffa9177e1d9">GetSpherePathMaxCount</a>()
<a name="l00036"></a>00036                 {
<a name="l00037"></a>00037                         <span class="keywordflow">return</span> 1;
<a name="l00038"></a>00038                 }
<a name="l00039"></a>00039
<a name="l00040"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#b2145d23a89e912ee85d13cfc9578b11">00040</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#b2145d23a89e912ee85d13cfc9578b11">GetSpherePathCount</a>()
<a name="l00041"></a>00041                 {
<a name="l00042"></a>00042                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#5a78f4a7e638948db52a50bf27f97277">DomChildCount</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"SpherePath"</span>);
<a name="l00043"></a>00043                 }
<a name="l00044"></a>00044
<a name="l00045"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#85537b6ad620638523d30a3a6c8d2cd4">00045</a>                 <span class="keyword">public</span> <span class="keywordtype">bool</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#85537b6ad620638523d30a3a6c8d2cd4">HasSpherePath</a>()
<a name="l00046"></a>00046                 {
<a name="l00047"></a>00047                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#22caf29f497e86a36ca976c5b9344d11">HasDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"SpherePath"</span>);
<a name="l00048"></a>00048                 }
<a name="l00049"></a>00049
<a name="l00050"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#c5a57ebd38e39bcace29c13939f61dad">00050</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a> <a class="code" href="class_schema_account_manager_1_1_config_type.php#c5a57ebd38e39bcace29c13939f61dad">GetSpherePathAt</a>(<span class="keywordtype">int</span> index)
<a name="l00051"></a>00051                 {
<a name="l00052"></a>00052                         <span class="keywordflow">return</span> <span class="keyword">new</span> <a class="code" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a>(<a class="code" href="class_altova_1_1_node.php#3e9877a06a6e9021aecc6cf026c34f7a">GetDomNodeValue</a>(<a class="code" href="class_altova_1_1_node.php#30b0b16f6517d59c5415e7e7d8f83959">GetDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"SpherePath"</span>, index)));
<a name="l00053"></a>00053                 }
<a name="l00054"></a>00054
<a name="l00055"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#34b5127a53987b32bfd05c386f600153">00055</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a> <a class="code" href="class_schema_account_manager_1_1_config_type.php#34b5127a53987b32bfd05c386f600153">GetSpherePath</a>()
<a name="l00056"></a>00056                 {
<a name="l00057"></a>00057                         <span class="keywordflow">return</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#c5a57ebd38e39bcace29c13939f61dad">GetSpherePathAt</a>(0);
<a name="l00058"></a>00058                 }
<a name="l00059"></a>00059
<a name="l00060"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#dece9adf409277ac5cc692c69047ec78">00060</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#dece9adf409277ac5cc692c69047ec78">RemoveSpherePathAt</a>(<span class="keywordtype">int</span> index)
<a name="l00061"></a>00061                 {
<a name="l00062"></a>00062                         <a class="code" href="class_altova_1_1_node.php#7938fa304858dfc0e9f8f492c9a65ecb">RemoveDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"SpherePath"</span>, index);
<a name="l00063"></a>00063                 }
<a name="l00064"></a>00064
<a name="l00065"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#25419026fd48cbf6244e0e7f1a51a38e">00065</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#25419026fd48cbf6244e0e7f1a51a38e">RemoveSpherePath</a>()
<a name="l00066"></a>00066                 {
<a name="l00067"></a>00067                         <span class="keywordflow">while</span> (<a class="code" href="class_schema_account_manager_1_1_config_type.php#85537b6ad620638523d30a3a6c8d2cd4">HasSpherePath</a>())
<a name="l00068"></a>00068                                 <a class="code" href="class_schema_account_manager_1_1_config_type.php#dece9adf409277ac5cc692c69047ec78">RemoveSpherePathAt</a>(0);
<a name="l00069"></a>00069                 }
<a name="l00070"></a>00070
<a name="l00071"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#5f45cc946dfe2b23eddcb56ece20865a">00071</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#5f45cc946dfe2b23eddcb56ece20865a">AddSpherePath</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a> newValue)
<a name="l00072"></a>00072                 {
<a name="l00073"></a>00073                         <a class="code" href="class_altova_1_1_node.php#c75ea55d1cdcb9119c14c8e7598d27fd">AppendDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"SpherePath"</span>, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_string.php#c3c38a1ea21f766474a649fee88b76bc">ToString</a>());
<a name="l00074"></a>00074                 }
<a name="l00075"></a>00075
<a name="l00076"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#7c07de0a181fc78cd2c327f39d16b6dd">00076</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#7c07de0a181fc78cd2c327f39d16b6dd">InsertSpherePathAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00077"></a>00077                 {
<a name="l00078"></a>00078                         <a class="code" href="class_altova_1_1_node.php#04fbd644a61272767ac268a6837e07b8">InsertDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"SpherePath"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_string.php#c3c38a1ea21f766474a649fee88b76bc">ToString</a>());
<a name="l00079"></a>00079                 }
<a name="l00080"></a>00080
<a name="l00081"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#3c6e0b2b110014d610ec5a6e60d4eb5c">00081</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#3c6e0b2b110014d610ec5a6e60d4eb5c">ReplaceSpherePathAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00082"></a>00082                 {
<a name="l00083"></a>00083                         <a class="code" href="class_altova_1_1_node.php#a99e2051ebcba9f999ee5cfa19b6072f">ReplaceDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"SpherePath"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_string.php#c3c38a1ea21f766474a649fee88b76bc">ToString</a>());
<a name="l00084"></a>00084                 }
<a name="l00085"></a>00085 <span class="preprocessor">                #endregion // SpherePath accessor methods</span>
<a name="l00086"></a>00086 <span class="preprocessor"></span>
<a name="l00087"></a>00087 <span class="preprocessor">                #region Items accessor methods</span>
<a name="l00088"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#2f9d3448e8b6e3f64dc4a47f68e31ae2">00088</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#2f9d3448e8b6e3f64dc4a47f68e31ae2">GetItemsMinCount</a>()
<a name="l00089"></a>00089                 {
<a name="l00090"></a>00090                         <span class="keywordflow">return</span> 1;
<a name="l00091"></a>00091                 }
<a name="l00092"></a>00092
<a name="l00093"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#f7915b7a27302e8b53d15afb001b3154">00093</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#f7915b7a27302e8b53d15afb001b3154">GetItemsMaxCount</a>()
<a name="l00094"></a>00094                 {
<a name="l00095"></a>00095                         <span class="keywordflow">return</span> 1;
<a name="l00096"></a>00096                 }
<a name="l00097"></a>00097
<a name="l00098"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#7a3dd0fad941aac121475c62b8a27641">00098</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#7a3dd0fad941aac121475c62b8a27641">GetItemsCount</a>()
<a name="l00099"></a>00099                 {
<a name="l00100"></a>00100                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#5a78f4a7e638948db52a50bf27f97277">DomChildCount</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Items"</span>);
<a name="l00101"></a>00101                 }
<a name="l00102"></a>00102
<a name="l00103"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#b869d5c02d3c6f30e95c2bea49b2b448">00103</a>                 <span class="keyword">public</span> <span class="keywordtype">bool</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#b869d5c02d3c6f30e95c2bea49b2b448">HasItems</a>()
<a name="l00104"></a>00104                 {
<a name="l00105"></a>00105                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#22caf29f497e86a36ca976c5b9344d11">HasDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Items"</span>);
<a name="l00106"></a>00106                 }
<a name="l00107"></a>00107
<a name="l00108"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#0b95d50c81a17e374186eb705c4303df">00108</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_account_manager_1_1_items_type.php">ItemsType</a> <a class="code" href="class_schema_account_manager_1_1_config_type.php#0b95d50c81a17e374186eb705c4303df">GetItemsAt</a>(<span class="keywordtype">int</span> index)
<a name="l00109"></a>00109                 {
<a name="l00110"></a>00110                         <span class="keywordflow">return</span> <span class="keyword">new</span> <a class="code" href="class_schema_account_manager_1_1_items_type.php">ItemsType</a>(<a class="code" href="class_altova_1_1_node.php#30b0b16f6517d59c5415e7e7d8f83959">GetDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Items"</span>, index));
<a name="l00111"></a>00111                 }
<a name="l00112"></a>00112
<a name="l00113"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#e24e5ad5db9505b3654cae62548205ad">00113</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_account_manager_1_1_items_type.php">ItemsType</a> <a class="code" href="class_schema_account_manager_1_1_config_type.php#e24e5ad5db9505b3654cae62548205ad">GetItems</a>()
<a name="l00114"></a>00114                 {
<a name="l00115"></a>00115                         <span class="keywordflow">return</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#0b95d50c81a17e374186eb705c4303df">GetItemsAt</a>(0);
<a name="l00116"></a>00116                 }
<a name="l00117"></a>00117
<a name="l00118"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#3e8ae6946168413aacf74e923289daf3">00118</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#3e8ae6946168413aacf74e923289daf3">RemoveItemsAt</a>(<span class="keywordtype">int</span> index)
<a name="l00119"></a>00119                 {
<a name="l00120"></a>00120                         <a class="code" href="class_altova_1_1_node.php#7938fa304858dfc0e9f8f492c9a65ecb">RemoveDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Items"</span>, index);
<a name="l00121"></a>00121                 }
<a name="l00122"></a>00122
<a name="l00123"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#f7531ef921479ab38bf1e6e58c71acd1">00123</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#f7531ef921479ab38bf1e6e58c71acd1">RemoveItems</a>()
<a name="l00124"></a>00124                 {
<a name="l00125"></a>00125                         <span class="keywordflow">while</span> (<a class="code" href="class_schema_account_manager_1_1_config_type.php#b869d5c02d3c6f30e95c2bea49b2b448">HasItems</a>())
<a name="l00126"></a>00126                                 <a class="code" href="class_schema_account_manager_1_1_config_type.php#3e8ae6946168413aacf74e923289daf3">RemoveItemsAt</a>(0);
<a name="l00127"></a>00127                 }
<a name="l00128"></a>00128
<a name="l00129"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#3f3fb60fe385ee6936860bed14400e61">00129</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#3f3fb60fe385ee6936860bed14400e61">AddItems</a>(<a class="code" href="class_schema_account_manager_1_1_items_type.php">ItemsType</a> newValue)
<a name="l00130"></a>00130                 {
<a name="l00131"></a>00131                         <a class="code" href="class_altova_1_1_node.php#b9500bc54742bc18fb283eb45b43b082">AppendDomElement</a>(<span class="stringliteral">""</span>, <span class="stringliteral">"Items"</span>, newValue);
<a name="l00132"></a>00132                 }
<a name="l00133"></a>00133
<a name="l00134"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#5546f24e2adb9e53aacf749eeae8d7a2">00134</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#5546f24e2adb9e53aacf749eeae8d7a2">InsertItemsAt</a>(<a class="code" href="class_schema_account_manager_1_1_items_type.php">ItemsType</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00135"></a>00135                 {
<a name="l00136"></a>00136                         <a class="code" href="class_altova_1_1_node.php#291c76a765b719a0f540e7ee40d0168b">InsertDomElementAt</a>(<span class="stringliteral">""</span>, <span class="stringliteral">"Items"</span>, index, newValue);
<a name="l00137"></a>00137                 }
<a name="l00138"></a>00138
<a name="l00139"></a><a class="code" href="class_schema_account_manager_1_1_config_type.php#987d50c0441a9856902a29d1b07e48ad">00139</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_account_manager_1_1_config_type.php#987d50c0441a9856902a29d1b07e48ad">ReplaceItemsAt</a>(<a class="code" href="class_schema_account_manager_1_1_items_type.php">ItemsType</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00140"></a>00140                 {
<a name="l00141"></a>00141                         <a class="code" href="class_altova_1_1_node.php#b225018014944548e7123b3d934da30d">ReplaceDomElementAt</a>(<span class="stringliteral">""</span>, <span class="stringliteral">"Items"</span>, index, newValue);
<a name="l00142"></a>00142                 }
<a name="l00143"></a>00143 <span class="preprocessor">                #endregion // Items accessor methods</span>
<a name="l00144"></a>00144 <span class="preprocessor"></span>        }
<a name="l00145"></a>00145 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
