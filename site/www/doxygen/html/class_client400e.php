<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>Client400e Class Reference</h1><!-- doxytag: class="Client400e" --><!-- doxytag: inherits="CClientUO" -->Класс управления клиентом.
<a href="#_details">More...</a>
<p>
<code>#include &lt;<a class="el" href="client400e_8h-source.php">client400e.h</a>&gt;</code>
<p>
<p>Inheritance diagram for Client400e:
<p><center><img src="class_client400e.png" usemap="#Client400e_map" border="0" alt=""></center>
<map name="Client400e_map">
<area href="class_c_client_u_o.php" alt="CClientUO" shape="rect" coords="1314,336,1523,360">
<area href="class_c_path_finding.php" alt="CPathFinding" shape="rect" coords="0,280,209,304">
<area href="class_c_client_u_o_commands.php" alt="CClientUOCommands" shape="rect" coords="438,280,647,304">
<area href="class_c_gui.php" alt="CGui" shape="rect" coords="876,280,1085,304">
<area href="class_c_slepping_task.php" alt="CSleppingTask" shape="rect" coords="1314,280,1523,304">
<area href="class_client_u_o_1_1_c_login.php" alt="ClientUO::CLogin" shape="rect" coords="1752,280,1961,304">
<area href="class_c_out_of_proc_slave.php" alt="COutOfProcSlave" shape="rect" coords="2190,280,2399,304">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="2628,280,2837,304">
<area href="class_client_secure_1_1_secure_connection.php" alt="ClientSecure::SecureConnection" shape="rect" coords="2847,224,3056,248">
<area href="class_c_client_crypt.php" alt="CClientCrypt" shape="rect" coords="2847,168,3056,192">
<area href="class_i_o_fluke_1_1_c_mem_mngr.php" alt="IOFluke::CMemMngr" shape="rect" coords="2847,112,3056,136">
<area href="class_c_client_commands.php" alt="CClientCommands" shape="rect" coords="2409,224,2618,248">
<area href="class_c_client_events.php" alt="CClientEvents" shape="rect" coords="2409,168,2618,192">
<area href="class_c_inter_proc_client_addin.php" alt="CInterProcClientAddin" shape="rect" coords="2409,112,2618,136">
<area href="class_c_inter_proc_slave.php" alt="CInterProcSlave" shape="rect" coords="2409,56,2618,80">
<area href="class_c_gui.php" alt="CGui" shape="rect" coords="1971,224,2180,248">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="1971,168,2180,192">
<area href="class_c_gui.php" alt="CGui" shape="rect" coords="1533,224,1742,248">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="1533,168,1742,192">
<area href="class_c_client_u_o_commands.php" alt="CClientUOCommands" shape="rect" coords="1095,224,1304,248">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="1095,168,1304,192">
<area href="class_client_objects.php" alt="ClientObjects" shape="rect" coords="657,224,866,248">
<area href="class_c_out_of_proc_slave.php" alt="COutOfProcSlave" shape="rect" coords="657,168,866,192">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="657,112,866,136">
<area href="class_client_objects.php" alt="ClientObjects" shape="rect" coords="219,224,428,248">
<area href="class_c_slepping_task.php" alt="CSleppingTask" shape="rect" coords="219,168,428,192">
<area href="class_c_gui.php" alt="CGui" shape="rect" coords="219,112,428,136">
<area href="class_c_client_u_o_commands.php" alt="CClientUOCommands" shape="rect" coords="219,56,428,80">
<area href="class_client_u_o_secure_1_1_c_client_u_o_secure.php" alt="ClientUOSecure::CClientUOSecure" shape="rect" coords="219,0,428,24">
</map>
<a href="class_client400e-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client400e.php#1f8178cafe359853d3e3cc5f94db7828">PathClient</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client400e.php#a2e4bbe2566e86a838abf16dc790cdfd">ZeroFirstPacket</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client400e.php#06a8680547572e347485998737fcba32">DisableClientCrypt</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual HWND&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client400e.php#9c5c93b3011f060510f9d3ba7ece3598">GetWindow</a> ()</td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>
Класс управления клиентом.
<p>

<p>
Definition at line <a class="el" href="client400e_8h-source.php#l00005">5</a> of file <a class="el" href="client400e_8h-source.php">client400e.h</a>.<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="1f8178cafe359853d3e3cc5f94db7828"></a><!-- doxytag: member="Client400e::PathClient" ref="1f8178cafe359853d3e3cc5f94db7828" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Client400e::PathClient           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="client400e_8cpp-source.php#l00041">41</a> of file <a class="el" href="client400e_8cpp-source.php">client400e.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00042"></a>00042 {
<a name="l00043"></a>00043 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="a2e4bbe2566e86a838abf16dc790cdfd"></a><!-- doxytag: member="Client400e::ZeroFirstPacket" ref="a2e4bbe2566e86a838abf16dc790cdfd" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Client400e::ZeroFirstPacket           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="client400e_8cpp-source.php#l00015">15</a> of file <a class="el" href="client400e_8cpp-source.php">client400e.cpp</a>.
<p>
References <a class="el" href="client308s_8cpp-source.php#l00004">PATCH_B</a>, and <a class="el" href="client308s_8cpp-source.php#l00009">PATCH_E</a>.<div class="fragment"><pre class="fragment"><a name="l00016"></a>00016 {
<a name="l00017"></a>00017   <span class="comment">// first packet</span>
<a name="l00018"></a>00018   <span class="comment">//.text:00417FB2 8B 0D A4 23 DC 00     mov    ecx, dword_DC23A4</span>
<a name="l00019"></a>00019   <span class="comment">//.text:00417FB8 51                    push   ecx</span>
<a name="l00020"></a>00020   <span class="comment">//.text:00417FB9 E8 8C 0A 10 00        call   WSOCK32_8</span>
<a name="l00021"></a>00021   <a class="code" href="client308s_8cpp.php#be097f0db03ffc37830df72560b7be01">PATCH_B</a>(0x417fb2) {0x31,0xc0,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90,0x90} <a class="code" href="client308s_8cpp.php#ee0924413f502cc0cf2dd4916b83665e">PATCH_E</a>;
<a name="l00022"></a>00022 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="06a8680547572e347485998737fcba32"></a><!-- doxytag: member="Client400e::DisableClientCrypt" ref="06a8680547572e347485998737fcba32" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void Client400e::DisableClientCrypt           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implements <a class="el" href="class_client_u_o_secure_1_1_c_client_u_o_secure.php#289ab9c72c80498dd216c3b377f9de68">ClientUOSecure::CClientUOSecure</a>.
<p>
Definition at line <a class="el" href="client400e_8cpp-source.php#l00024">24</a> of file <a class="el" href="client400e_8cpp-source.php">client400e.cpp</a>.
<p>
References <a class="el" href="class_client_u_o_secure_1_1_c_client_u_o_secure.php#289ab9c72c80498dd216c3b377f9de68">ClientUOSecure::CClientUOSecure::DisableClientCrypt()</a>, <a class="el" href="client308s_8cpp-source.php#l00004">PATCH_B</a>, and <a class="el" href="client308s_8cpp-source.php#l00009">PATCH_E</a>.<div class="fragment"><pre class="fragment"><a name="l00025"></a>00025 {
<a name="l00026"></a>00026   <span class="comment">// login crypt</span>
<a name="l00027"></a>00027   <span class="comment">//.text:0041A2DF 32 D0                 xor    dl, al</span>
<a name="l00028"></a>00028   <a class="code" href="client308s_8cpp.php#be097f0db03ffc37830df72560b7be01">PATCH_B</a>(0x0041A2DF) {0x88,0xc2} <a class="code" href="client308s_8cpp.php#ee0924413f502cc0cf2dd4916b83665e">PATCH_E</a>;
<a name="l00029"></a>00029
<a name="l00030"></a>00030   <span class="comment">// game crypt</span>
<a name="l00031"></a>00031   <span class="comment">//004313E8 32 D0            xor         dl,al </span>
<a name="l00032"></a>00032   <a class="code" href="client308s_8cpp.php#be097f0db03ffc37830df72560b7be01">PATCH_B</a>(0x004313E8) {0x90,0x90} <a class="code" href="client308s_8cpp.php#ee0924413f502cc0cf2dd4916b83665e">PATCH_E</a>;
<a name="l00033"></a>00033
<a name="l00034"></a>00034   <span class="comment">// from server - decrypt</span>
<a name="l00035"></a>00035   <span class="comment">//004197A5 32 DA            xor         bl,dl </span>
<a name="l00036"></a>00036   <a class="code" href="client308s_8cpp.php#be097f0db03ffc37830df72560b7be01">PATCH_B</a>(0x004197A5) {0x90,0x90} <a class="code" href="client308s_8cpp.php#ee0924413f502cc0cf2dd4916b83665e">PATCH_E</a>;
<a name="l00037"></a>00037
<a name="l00038"></a>00038   <a class="code" href="class_client_u_o_secure_1_1_c_client_u_o_secure.php#289ab9c72c80498dd216c3b377f9de68">CClientUO::DisableClientCrypt</a>();
<a name="l00039"></a>00039 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="9c5c93b3011f060510f9d3ba7ece3598"></a><!-- doxytag: member="Client400e::GetWindow" ref="9c5c93b3011f060510f9d3ba7ece3598" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">HWND Client400e::GetWindow           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implements <a class="el" href="class_c_client_u_o_commands.php#ab6ce74de21d0f330aa3e65dbaaed763">CClientUOCommands</a>.
<p>
Definition at line <a class="el" href="client400e_8cpp-source.php#l00045">45</a> of file <a class="el" href="client400e_8cpp-source.php">client400e.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00046"></a>00046 {
<a name="l00047"></a>00047   <span class="keywordflow">return</span> *(HWND*)0x00E57380;
<a name="l00048"></a>00048 }
</pre></div>
<p>

</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="client400e_8h-source.php">client400e.h</a><li><a class="el" href="client400e_8cpp-source.php">client400e.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
