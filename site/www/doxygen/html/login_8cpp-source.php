<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0e80f8b385e544f89c616410da1961e3.php">ClientAddin</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_62517a2ed17a94014abd0a2cfd485e7b.php">clientsuck</a></div>
<h1>login.cpp</h1><a href="login_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#include "<a class="code" href="_g_z_2_client_addin_2clientsuck_2_std_afx_8h.php">stdafx.h</a>"</span>
<a name="l00002"></a>00002 <span class="preprocessor">#include "<a class="code" href="login_8h.php">login.h</a>"</span>
<a name="l00003"></a>00003 <span class="preprocessor">#include "<a class="code" href="setup_8h.php">setup.h</a>"</span>
<a name="l00004"></a>00004
<a name="l00005"></a><a class="code" href="class_client_u_o_1_1_c_login.php#0e120a6282998545702ac8ea1b5474cc">00005</a> <a class="code" href="class_client_u_o_1_1_c_login.php#0e120a6282998545702ac8ea1b5474cc">ClientUO::CLogin::CLogin</a>():m_loggedin(false)
<a name="l00006"></a>00006 {
<a name="l00007"></a>00007 }
<a name="l00008"></a>00008
<a name="l00009"></a><a class="code" href="class_client_u_o_1_1_c_login.php#8f00dcd4ca02cfae0bbefdde11a7c1ea">00009</a> <span class="keywordtype">void</span> <a class="code" href="class_client_u_o_1_1_c_login.php#8f00dcd4ca02cfae0bbefdde11a7c1ea">ClientUO::CLogin::Login</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>*name,<span class="keyword">const</span> <span class="keywordtype">char</span>*pass,<span class="keyword">const</span> <span class="keywordtype">char</span>* charname)
<a name="l00010"></a>00010 {
<a name="l00011"></a>00011   <a class="code" href="class_c_gui.php#5aadc2e34c76c2e2a26df74270cd29e7">WaitForClientInit</a>();
<a name="l00012"></a>00012
<a name="l00013"></a>00013   <span class="keywordflow">if</span>(name==0)
<a name="l00014"></a>00014     name=<a class="code" href="class_c_client_u_o_commands.php#87ac84ac2d7c752f5ce1f6137272c87c">m_login</a>.c_str();
<a name="l00015"></a>00015   <span class="keywordflow">if</span>(pass==0)
<a name="l00016"></a>00016     pass=<a class="code" href="class_c_client_u_o_commands.php#808d6e8c9e3f82541cfacbc1ce11d66e">m_password</a>.c_str();
<a name="l00017"></a>00017   <span class="keywordflow">if</span>(charname==0)
<a name="l00018"></a>00018     charname=<a class="code" href="class_c_client_u_o_commands.php#35ec09cde1b4d8c98451d2a2c9b0e8d9">m_charname</a>.c_str();
<a name="l00019"></a>00019
<a name="l00020"></a>00020   sentry_controlwindow scw(<a class="code" href="class_c_client_u_o_commands.php#ab6ce74de21d0f330aa3e65dbaaed763">GetWindow</a>());
<a name="l00021"></a>00021
<a name="l00022"></a>00022   <span class="keywordflow">if</span>(strlen(name)==0)
<a name="l00023"></a>00023     <span class="keywordflow">throw</span> std::exception(<span class="stringliteral">"account name is empty"</span>);
<a name="l00024"></a>00024   <span class="keywordflow">if</span>(strlen(pass)==0)
<a name="l00025"></a>00025     <span class="keywordflow">throw</span> std::exception(<span class="stringliteral">"password is empty"</span>);
<a name="l00026"></a>00026   <span class="keywordflow">if</span>(strlen(charname)==0)
<a name="l00027"></a>00027     <span class="keywordflow">throw</span> std::exception(<span class="stringliteral">"char name is empty"</span>);
<a name="l00028"></a>00028
<a name="l00029"></a>00029   <a class="code" href="class_c_gui.php#97227b8be483db9c322d2fb671830d88">SendKey</a>(VK_TAB);
<a name="l00030"></a>00030   <a class="code" href="class_c_gui.php#97227b8be483db9c322d2fb671830d88">SendKey</a>(VK_TAB);
<a name="l00031"></a>00031
<a name="l00032"></a>00032   <span class="keywordtype">int</span> charindex=-1;
<a name="l00033"></a>00033   {
<a name="l00034"></a>00034     <a class="code" href="class_c_client_u_o_commands.php#b0e5ae19135767820d7f2bda2d281095">CmdSet</a> cs;
<a name="l00035"></a>00035     cs.insert(<a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae8bf6c3ddb59936cfe8cff79b3b893a248">Packets::scPostLogin</a>);
<a name="l00036"></a>00036     CommandControl secondlogin(<a class="code" href="class_c_client_u_o_commands.php#0ae29a98fc0621c58e7e7936170c9340">CreateInterceptor</a>(cs));
<a name="l00037"></a>00037     {
<a name="l00038"></a>00038       cs.insert(<a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae8702ab317f0cde4681da74fd490318483">Packets::scBritaniaList</a>);
<a name="l00039"></a>00039       CommandControl britanialist(<a class="code" href="class_c_client_u_o_commands.php#0ae29a98fc0621c58e7e7936170c9340">CreateInterceptor</a>(cs));
<a name="l00040"></a>00040       {
<a name="l00041"></a>00041         cs.insert(<a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae8101a6333a808989923e86d81f080a5aa">Packets::scLogin</a>);
<a name="l00042"></a>00042         CommandControl firstlogin(<a class="code" href="class_c_client_u_o_commands.php#0ae29a98fc0621c58e7e7936170c9340">CreateInterceptor</a>(cs));
<a name="l00043"></a>00043         <a class="code" href="class_c_gui.php#20c5367ab8e4271c044c072b1f23fb4a">SendClick</a>(615,455); <span class="comment">// next</span>
<a name="l00044"></a>00044         firstlogin-&gt;Catch(<a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae8101a6333a808989923e86d81f080a5aa">Packets::scLogin</a>);
<a name="l00045"></a>00045         firstlogin-&gt;SetReplace((<a class="code" href="class_packets_1_1_packet.php">Packets::Packet</a>)<a class="code" href="struct_packets_1_1scs_login.php">Packets::scsLogin</a>(name,pass));
<a name="l00046"></a>00046       }
<a name="l00047"></a>00047       britanialist-&gt;Catch(<a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae8702ab317f0cde4681da74fd490318483">Packets::scBritaniaList</a>);
<a name="l00048"></a>00048       <a class="code" href="class_c_gui.php#20c5367ab8e4271c044c072b1f23fb4a">SendClick</a>(615,455); <span class="comment">// next</span>
<a name="l00049"></a>00049     }
<a name="l00050"></a>00050     secondlogin-&gt;Catch(<a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae8bf6c3ddb59936cfe8cff79b3b893a248">Packets::scPostLogin</a>);
<a name="l00051"></a>00051     <a class="code" href="struct_packets_1_1scs_post_login.php">Packets::scsPostLogin</a> *p=(<a class="code" href="struct_packets_1_1scs_post_login.php">Packets::scsPostLogin</a> *)secondlogin-&gt;Buffer();
<a name="l00052"></a>00052     secondlogin-&gt;SetReplace((<a class="code" href="class_packets_1_1_packet.php">Packets::Packet</a>)<a class="code" href="struct_packets_1_1scs_post_login.php">Packets::scsPostLogin</a>(p-&gt;<a class="code" href="struct_packets_1_1scs_post_login.php#89fa11ddcd97ac75bd916bd2a6b20375">authid</a>,name,pass));
<a name="l00053"></a>00053   }
<a name="l00054"></a>00054
<a name="l00055"></a>00055   {
<a name="l00056"></a>00056     <a class="code" href="class_c_client_u_o_commands.php#b0e5ae19135767820d7f2bda2d281095">CClientUOCommands::CmdSet</a> cs;
<a name="l00057"></a>00057     cs.insert(<a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae8ad57508ac208f79843246a14eb752964">Packets::scCitiesAndChars</a>);
<a name="l00058"></a>00058     cs.insert(<a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae80d654c335b03103cf7b716bcecdf76d4">Packets::scLoginComplite</a>);
<a name="l00059"></a>00059     cs.insert(<a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae8363b0cf2eb8cf89d96dd8c027285eebd">Packets::scAcctLoginFail</a>);
<a name="l00060"></a>00060     CommandControl cc(<a class="code" href="class_c_client_u_o_commands.php#0ae29a98fc0621c58e7e7936170c9340">CreateInterceptor</a>(cs));
<a name="l00061"></a>00061     <a class="code" href="class_c_gui.php#20c5367ab8e4271c044c072b1f23fb4a">SendClick</a>(615,455); <span class="comment">// next</span>
<a name="l00062"></a>00062     <span class="keywordflow">switch</span>(cc-&gt;Catch())
<a name="l00063"></a>00063     {
<a name="l00064"></a>00064     <span class="keywordflow">case</span> <a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae8363b0cf2eb8cf89d96dd8c027285eebd">Packets::scAcctLoginFail</a>:
<a name="l00065"></a>00065       {
<a name="l00066"></a>00066         <span class="keywordflow">throw</span> <a class="code" href="class_client_u_o_1_1_c_login_1_1_login_error.php">LoginError</a>(<a class="code" href="struct_packets_1_1scs_acct_login_fail.php#b118da9b937ff3ff83326813ceec6af4">Packets::scsAcctLoginFail::GetErrorText</a>(cc-&gt;Buffer()));
<a name="l00067"></a>00067       }
<a name="l00068"></a>00068       <span class="keywordflow">break</span>;
<a name="l00069"></a>00069     <span class="keywordflow">case</span> <a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae8ad57508ac208f79843246a14eb752964">Packets::scCitiesAndChars</a>:
<a name="l00070"></a>00070       {
<a name="l00071"></a>00071         <a class="code" href="struct_packets_1_1scs_cities_and_chars.php">Packets::scsCitiesAndChars</a> *p=(<a class="code" href="struct_packets_1_1scs_cities_and_chars.php">Packets::scsCitiesAndChars</a> *)cc-&gt;Buffer();
<a name="l00072"></a>00072         <a class="code" href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag.php">Packets::scsCitiesAndChars::Chars_tag</a> &amp;chars=p-&gt;<a class="code" href="struct_packets_1_1scs_cities_and_chars.php#de2974fcf731c23e04cdbd8bd3672281">GetChars</a>();
<a name="l00073"></a>00073         <a class="code" href="struct_packets_1_1scs_cities_and_chars_1_1_cities__tag.php">Packets::scsCitiesAndChars::Cities_tag</a> &amp;cities=p-&gt;<a class="code" href="struct_packets_1_1scs_cities_and_chars.php#f14dec8dda6570410a7e1beab18c2242">GetCities</a>();
<a name="l00074"></a>00074         <span class="keywordflow">for</span>(<span class="keywordtype">int</span> i=0;i&lt;chars.<a class="code" href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag.php#62e0b5e3b98a99308d02012939f8a33f">numchars</a>;i++)
<a name="l00075"></a>00075         {
<a name="l00076"></a>00076           <span class="keywordflow">if</span>(stricmp(chars.<a class="code" href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag.php#d9f6c31e88a2ebbda665e7a2e8151ddc">Char</a>[i].CharName,charname)==0&amp;&amp;charindex==-1)
<a name="l00077"></a>00077             charindex=i;
<a name="l00078"></a>00078         }
<a name="l00079"></a>00079       }
<a name="l00080"></a>00080       <span class="keywordflow">break</span>;
<a name="l00081"></a>00081     <span class="keywordflow">case</span> <a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae80d654c335b03103cf7b716bcecdf76d4">Packets::scLoginComplite</a>: <span class="comment">//login correct</span>
<a name="l00082"></a>00082       <span class="keywordflow">return</span>;
<a name="l00083"></a>00083     }
<a name="l00084"></a>00084   }
<a name="l00085"></a>00085   <span class="keywordflow">if</span>(charindex==-1)
<a name="l00086"></a>00086   {
<a name="l00087"></a>00087     <a class="code" href="class_c_gui.php#6134fee4150a2a0762d0d2611010da97">ShowWindow</a>(<span class="keyword">true</span>);
<a name="l00088"></a>00088     <span class="keywordflow">throw</span> std::exception((std::string(<span class="stringliteral">"char name not found '"</span>)+charname+<span class="stringliteral">"'"</span>).c_str());
<a name="l00089"></a>00089   }
<a name="l00090"></a>00090
<a name="l00091"></a>00091   <a class="code" href="class_c_gui.php#20c5367ab8e4271c044c072b1f23fb4a">SendClick</a>(365,165+charindex*40); <span class="comment">// first char</span>
<a name="l00092"></a>00092
<a name="l00093"></a>00093   {
<a name="l00094"></a>00094     CommandControl cc(<a class="code" href="class_c_client_u_o_commands.php#0ae29a98fc0621c58e7e7936170c9340">CreateInterceptor</a>(<a class="code" href="namespace_packets.php#f904fbdbeb0801a8facc3fa52f299ae828bad95c28a5c86ab8bcba55f38a6cc1">Packets::scLoginConfirm</a>));
<a name="l00095"></a>00095     <a class="code" href="class_c_gui.php#20c5367ab8e4271c044c072b1f23fb4a">SendClick</a>(615,455); <span class="comment">// next</span>
<a name="l00096"></a>00096     cc-&gt;Catch();
<a name="l00097"></a>00097   }
<a name="l00098"></a>00098 }
<a name="l00099"></a>00099
<a name="l00100"></a><a class="code" href="class_client_u_o_1_1_c_login.php#5148158c47670fe80073fc0952f72118">00100</a> <span class="keywordtype">void</span> <a class="code" href="class_client_u_o_1_1_c_login.php#5148158c47670fe80073fc0952f72118">ClientUO::CLogin::LoggedInPass</a>(CMemMngr* client,va_list args)
<a name="l00101"></a>00101 {
<a name="l00102"></a>00102   <a class="code" href="class_client_u_o_1_1_c_login.php">ClientUO::CLogin</a>* thisclass=dynamic_cast&lt;ClientUO::CLogin*&gt;(client);
<a name="l00103"></a>00103   thisclass-&gt;<a class="code" href="class_client_u_o_1_1_c_login.php#f350180504dbbe5ea806ee0c87d7a21d">m_loggedin</a>=<span class="keyword">true</span>;
<a name="l00104"></a>00104   <span class="comment">//thisclass-&gt;ConnectionEstablished();</span>
<a name="l00105"></a>00105 }
<a name="l00106"></a>00106
<a name="l00107"></a><a class="code" href="class_client_u_o_1_1_c_login.php#c55c46050f32b01a36101efa8cd21b59">00107</a> <span class="keywordtype">void</span> <a class="code" href="class_client_u_o_1_1_c_login.php#c55c46050f32b01a36101efa8cd21b59">ClientUO::CLogin::ClientDisconnected</a>(CMemMngr* memmngr,va_list args)
<a name="l00108"></a>00108 {
<a name="l00109"></a>00109   <a class="code" href="class_client_u_o_1_1_c_login.php">ClientUO::CLogin</a>* thisclass=dynamic_cast&lt;ClientUO::CLogin*&gt;(memmngr);
<a name="l00110"></a>00110   thisclass-&gt;<a class="code" href="class_c_out_of_proc_slave.php#371988fd5d983debe691ff572be5b0d7">ConnectionLost</a>();
<a name="l00111"></a>00111 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
