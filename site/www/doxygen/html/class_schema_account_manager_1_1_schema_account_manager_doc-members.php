<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>SchemaAccountManager::SchemaAccountManagerDoc Member List</h1>This is the complete list of members for <a class="el" href="class_schema_account_manager_1_1_schema_account_manager_doc.php">SchemaAccountManager::SchemaAccountManagerDoc</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#ab6f64b0d7e735b2c95b74b4bd9ae9ce">CreateTemporaryDomNode</a>()</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#3c2f5e2847b84d174036d0bbcea63952">DeclareNamespace</a>(Node node, string prefix, string URI)</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_account_manager_1_1_schema_account_manager_doc.php#ab4d73ceff80ff08ec206c4145310b20">DeclareNamespaces</a>(Altova.Node node)</td><td><a class="el" href="class_schema_account_manager_1_1_schema_account_manager_doc.php">SchemaAccountManager::SchemaAccountManagerDoc</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#34421e9b740b71a356134bea4eaf1ce3">Altova::Document::DeclareNamespaces</a>(Node node)</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [protected, pure virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#1a2be59fa2700c9b1d8feac69b1e62cb">Document</a>()</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#8766cea1f71bd669a49170d51900a519">FinalizeRootElement</a>(Node node)</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#ade9bff089a2327b3303a650a0ce3eb2">GetTemporaryDocument</a>()</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline, protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#0e30654fcb3b57ab52f540b6d3b61ea6">Load</a>(XmlReader reader)</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#3eeed0a2c0e1575422601ab7ff00c9f6">Load</a>(string filename)</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#beedbe6a4c75e21e121cc8f44f12201e">LoadXML</a>(string xml)</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#514d77c61ed77b981179f7f6d9a5ef30">namespaceURI</a></td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#01704ddb7bb192764e117aac4c96d14f">rootElementName</a></td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#cddb0505631975964f8b439bde2dde93">Save</a>(string filename, Node node)</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#389fef0117da9b2bdcf1910d04248893">SaveXML</a>(Node node)</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#ae9847a71b91c4035913dee0ad58ee15">schemaLocation</a></td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#050fae2d4a132670cd3fee9e30dd865b">SetRootElementName</a>(string namespaceURI, string rootElementName)</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#bc0535524c965777c8d9027e7c37f140">SetSchemaLocation</a>(string schemaLocation)</td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#e3daf79d942331c7a10db2b6d5727a42">tmpDocument</a></td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#6e6444d62c415f61b116b56109f32d79">tmpFragment</a></td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_document.php#c016bdb01b044fefd51cb3262ca6ad87">tmpNameCounter</a></td><td><a class="el" href="class_altova_1_1_document.php">Altova::Document</a></td><td><code> [protected, static]</code></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
