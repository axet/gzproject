<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespacealtova.php">altova</a>::<a class="el" href="classaltova_1_1_c_schema_type_calendar.php">CSchemaTypeCalendar</a>::<a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php">CMonthDay</a></div>
<h1>altova::CSchemaTypeCalendar::CMonthDay Struct Reference</h1><!-- doxytag: class="altova::CSchemaTypeCalendar::CMonthDay" --><!-- doxytag: inherits="altova::CSchemaTypeCalendar::CDateTimeBase" --><code>#include &lt;<a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>&gt;</code>
<p>
<p>Inheritance diagram for altova::CSchemaTypeCalendar::CMonthDay:
<p><center><img src="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.png" usemap="#altova::CSchemaTypeCalendar::CMonthDay_map" border="0" alt=""></center>
<map name="altova::CSchemaTypeCalendar::CMonthDay_map">
<area href="structaltova_1_1_c_schema_type_calendar_1_1_c_date_time_base.php" alt="altova::CSchemaTypeCalendar::CDateTimeBase" shape="rect" coords="0,0,281,24">
</map>
<a href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#5336f1c29d9d8ae66a06885019f74bfd">CMonthDay</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#fe1d6a9baf1f25b42a5acfcc2a1f95be">CMonthDay</a> (int _nMonth, int _nDay)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">long&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#88f43bac6d4ad15b608005d513961054">ApproximatedNormalizedDays</a> () const</td></tr>

<tr><td colspan="2"><br><h2>Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#f8734173c6e20d0db60a1dda2f189948">nMonth</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#00fc10708904605f10e05e15b5fcbf79">nDay</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00267">267</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="5336f1c29d9d8ae66a06885019f74bfd"></a><!-- doxytag: member="altova::CSchemaTypeCalendar::CMonthDay::CMonthDay" ref="5336f1c29d9d8ae66a06885019f74bfd" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">altova::CSchemaTypeCalendar::CMonthDay::CMonthDay           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00269">269</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.<div class="fragment"><pre class="fragment"><a name="l00270"></a>00270                 {
<a name="l00271"></a>00271                         time_t long_time;
<a name="l00272"></a>00272                         time( &amp;long_time );
<a name="l00273"></a>00273                         <span class="keyword">struct </span>tm *newtime = localtime( &amp;long_time );
<a name="l00274"></a>00274                         <a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#f8734173c6e20d0db60a1dda2f189948">nMonth</a> = newtime-&gt;tm_mon + 1;
<a name="l00275"></a>00275                         <a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#00fc10708904605f10e05e15b5fcbf79">nDay</a> = newtime-&gt;tm_mday;
<a name="l00276"></a>00276                 };
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="fe1d6a9baf1f25b42a5acfcc2a1f95be"></a><!-- doxytag: member="altova::CSchemaTypeCalendar::CMonthDay::CMonthDay" ref="fe1d6a9baf1f25b42a5acfcc2a1f95be" args="(int _nMonth, int _nDay)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">altova::CSchemaTypeCalendar::CMonthDay::CMonthDay           </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>_nMonth</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>_nDay</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00278">278</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.<div class="fragment"><pre class="fragment"><a name="l00282"></a>00282                         : <a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#f8734173c6e20d0db60a1dda2f189948">nMonth</a>( _nMonth )
<a name="l00283"></a>00283                         , <a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#00fc10708904605f10e05e15b5fcbf79">nDay</a>( _nDay )
<a name="l00284"></a>00284                 {};
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="88f43bac6d4ad15b608005d513961054"></a><!-- doxytag: member="altova::CSchemaTypeCalendar::CMonthDay::ApproximatedNormalizedDays" ref="88f43bac6d4ad15b608005d513961054" args="() const" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">long altova::CSchemaTypeCalendar::CMonthDay::ApproximatedNormalizedDays           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00289">289</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.<div class="fragment"><pre class="fragment"><a name="l00289"></a>00289 { <span class="keywordflow">return</span> <a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#f8734173c6e20d0db60a1dda2f189948">nMonth</a>*31+<a class="code" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#00fc10708904605f10e05e15b5fcbf79">nDay</a>; }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="f8734173c6e20d0db60a1dda2f189948"></a><!-- doxytag: member="altova::CSchemaTypeCalendar::CMonthDay::nMonth" ref="f8734173c6e20d0db60a1dda2f189948" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#f8734173c6e20d0db60a1dda2f189948">altova::CSchemaTypeCalendar::CMonthDay::nMonth</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00284">284</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.
</div>
</div><p>
<a class="anchor" name="00fc10708904605f10e05e15b5fcbf79"></a><!-- doxytag: member="altova::CSchemaTypeCalendar::CMonthDay::nDay" ref="00fc10708904605f10e05e15b5fcbf79" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="structaltova_1_1_c_schema_type_calendar_1_1_c_month_day.php#00fc10708904605f10e05e15b5fcbf79">altova::CSchemaTypeCalendar::CMonthDay::nDay</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_type_calendar_8h-source.php#l00287">287</a> of file <a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a>.
</div>
</div><p>
<hr>The documentation for this struct was generated from the following file:<ul>
<li><a class="el" href="_schema_type_calendar_8h-source.php">SchemaTypeCalendar.h</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
