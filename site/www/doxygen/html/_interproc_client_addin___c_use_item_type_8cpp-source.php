<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_9dcfd5a61dba1352584094bd6fa46c11.php">interproc_clientaddin</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_30b943b43c01f5b1d4921d3b9410bd88.php">xsd</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_872562f892b2bed05f65639a1cdbe636.php">InterprocClientAddin</a></div>
<h1>InterprocClientAddin_CUseItemType.cpp</h1><a href="_interproc_client_addin___c_use_item_type_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// InterprocClientAddin_CUseItemType.cpp</span>
<a name="l00004"></a>00004 <span class="comment">//</span>
<a name="l00005"></a>00005 <span class="comment">// This file was generated by XMLSpy 2006 Enterprise Edition.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00008"></a>00008 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00009"></a>00009 <span class="comment">//</span>
<a name="l00010"></a>00010 <span class="comment">// Refer to the XMLSpy Documentation for further details.</span>
<a name="l00011"></a>00011 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00012"></a>00012 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment"></span>
<a name="l00015"></a>00015
<a name="l00016"></a>00016 <span class="preprocessor">#include "<a class="code" href="_g_z_2interproc__clientaddin_2xsd_2_interproc_client_addin_2_std_afx_8h.php">StdAfx.h</a>"</span>
<a name="l00017"></a>00017 <span class="preprocessor">#include "<a class="code" href="_interproc_client_addin_base_8h.php">InterprocClientAddinBase.h</a>"</span>
<a name="l00018"></a>00018 <span class="preprocessor">#include "<a class="code" href="_interproc_client_addin___c_use_item_type_8h.php">InterprocClientAddin_CUseItemType.h</a>"</span>
<a name="l00019"></a>00019
<a name="l00020"></a>00020
<a name="l00021"></a>00021
<a name="l00022"></a>00022 <span class="keyword">namespace </span>ica <span class="comment">// URI: http://gzproject.sourceforge.net/InterprocClientAddin</span>
<a name="l00023"></a>00023 {
<a name="l00025"></a>00025 <span class="comment">//</span>
<a name="l00026"></a>00026 <span class="comment">// class CUseItemType</span>
<a name="l00027"></a>00027 <span class="comment">//</span>
<a name="l00029"></a>00029 <span class="comment"></span>
<a name="l00030"></a>00030
<a name="l00031"></a><a class="code" href="classica_1_1_c_use_item_type.php#03c4427c9a3e9c6a7f45b2edc42d4a33">00031</a> <a class="code" href="classaltova_1_1_c_node.php#23a91b5b6ef1abca12745ecd7a2fe7a4">CNode::EGroupType</a> <a class="code" href="classica_1_1_c_use_item_type.php#03c4427c9a3e9c6a7f45b2edc42d4a33">CUseItemType::GetGroupType</a>()
<a name="l00032"></a>00032 {
<a name="l00033"></a>00033         <span class="keywordflow">return</span> eSequence;
<a name="l00034"></a>00034 }
<a name="l00035"></a><a class="code" href="classica_1_1_c_use_item_type.php#f0fd8f2bccbf56293fe4a88ff9cf4de6">00035</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_use_item_type.php#f0fd8f2bccbf56293fe4a88ff9cf4de6">CUseItemType::GetItemSerialToUseMinCount</a>()
<a name="l00036"></a>00036 {
<a name="l00037"></a>00037         <span class="keywordflow">return</span> 1;
<a name="l00038"></a>00038 }
<a name="l00039"></a>00039
<a name="l00040"></a>00040
<a name="l00041"></a><a class="code" href="classica_1_1_c_use_item_type.php#6a7af5433ac2375c57e4b3751c8bdcfb">00041</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_use_item_type.php#6a7af5433ac2375c57e4b3751c8bdcfb">CUseItemType::GetItemSerialToUseMaxCount</a>()
<a name="l00042"></a>00042 {
<a name="l00043"></a>00043         <span class="keywordflow">return</span> 1;
<a name="l00044"></a>00044 }
<a name="l00045"></a>00045
<a name="l00046"></a>00046
<a name="l00047"></a><a class="code" href="classica_1_1_c_use_item_type.php#1585cbd3dca337a5035bd6b8b9b15191">00047</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_use_item_type.php#1585cbd3dca337a5035bd6b8b9b15191">CUseItemType::GetItemSerialToUseCount</a>()
<a name="l00048"></a>00048 {
<a name="l00049"></a>00049         <span class="keywordflow">return</span> ChildCountInternal(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemSerialToUse"</span>));
<a name="l00050"></a>00050 }
<a name="l00051"></a>00051
<a name="l00052"></a>00052
<a name="l00053"></a><a class="code" href="classica_1_1_c_use_item_type.php#0d5c2caf7d4183ba4556a9e9e8375dc4">00053</a> <span class="keywordtype">bool</span> <a class="code" href="classica_1_1_c_use_item_type.php#0d5c2caf7d4183ba4556a9e9e8375dc4">CUseItemType::HasItemSerialToUse</a>()
<a name="l00054"></a>00054 {
<a name="l00055"></a>00055         <span class="keywordflow">return</span> InternalHasChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemSerialToUse"</span>));
<a name="l00056"></a>00056 }
<a name="l00057"></a>00057
<a name="l00058"></a>00058
<a name="l00059"></a>00059 <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_use_item_type.php#a10dfb0256827699109d7f5bc91a2d94">CUseItemType::AddItemSerialToUse</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> ItemSerialToUse)
<a name="l00060"></a>00060 {
<a name="l00061"></a>00061         <span class="keywordflow">if</span>( !ItemSerialToUse.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00062"></a>00062                 InternalAppend(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemSerialToUse"</span>), ItemSerialToUse);
<a name="l00063"></a>00063 }
<a name="l00064"></a>00064
<a name="l00065"></a>00065
<a name="l00066"></a>00066 <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_use_item_type.php#84948e63d4b658e9b41b48ea1a0806df">CUseItemType::InsertItemSerialToUseAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> ItemSerialToUse, <span class="keywordtype">int</span> nIndex)
<a name="l00067"></a>00067 {
<a name="l00068"></a>00068         <span class="keywordflow">if</span>( !ItemSerialToUse.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00069"></a>00069                 InternalInsertAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemSerialToUse"</span>), nIndex, ItemSerialToUse);
<a name="l00070"></a>00070 }
<a name="l00071"></a>00071
<a name="l00072"></a>00072
<a name="l00073"></a>00073 <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_use_item_type.php#41cac2f7282f83cf6af2e736900b38d8">CUseItemType::ReplaceItemSerialToUseAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> ItemSerialToUse, <span class="keywordtype">int</span> nIndex)
<a name="l00074"></a>00074 {
<a name="l00075"></a>00075         InternalReplaceAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemSerialToUse"</span>), nIndex, ItemSerialToUse);
<a name="l00076"></a>00076 }
<a name="l00077"></a>00077
<a name="l00078"></a>00078
<a name="l00079"></a>00079
<a name="l00080"></a><a class="code" href="classica_1_1_c_use_item_type.php#c9cffa56204e19e32325430441660b7d">00080</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classica_1_1_c_use_item_type.php#c9cffa56204e19e32325430441660b7d">CUseItemType::GetItemSerialToUseAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00081"></a>00081 {
<a name="l00082"></a>00082         <span class="keywordflow">return</span> (LPCTSTR)InternalGetAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemSerialToUse"</span>), nIndex)-&gt;text;
<a name="l00083"></a>00083         <span class="comment">//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:ItemSerialToUse"), nIndex)-&gt;text);</span>
<a name="l00084"></a>00084 }
<a name="l00085"></a>00085
<a name="l00086"></a><a class="code" href="classica_1_1_c_use_item_type.php#5f11fa75d3f7a24395eb408f4573ed7d">00086</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classica_1_1_c_use_item_type.php#5f11fa75d3f7a24395eb408f4573ed7d">CUseItemType::GetStartingItemSerialToUseCursor</a>()
<a name="l00087"></a>00087 {
<a name="l00088"></a>00088         <span class="keywordflow">return</span> InternalGetFirstChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemSerialToUse"</span>));
<a name="l00089"></a>00089 }
<a name="l00090"></a>00090
<a name="l00091"></a><a class="code" href="classica_1_1_c_use_item_type.php#e20694eb952138f08429837fe730c6b7">00091</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classica_1_1_c_use_item_type.php#e20694eb952138f08429837fe730c6b7">CUseItemType::GetAdvancedItemSerialToUseCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00092"></a>00092 {
<a name="l00093"></a>00093         <span class="keywordflow">return</span> InternalGetNextChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemSerialToUse"</span>), pCurNode);
<a name="l00094"></a>00094 }
<a name="l00095"></a>00095
<a name="l00096"></a><a class="code" href="classica_1_1_c_use_item_type.php#5c562b9920ee7b10e3f0ff2920d88e23">00096</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classica_1_1_c_use_item_type.php#5c562b9920ee7b10e3f0ff2920d88e23">CUseItemType::GetItemSerialToUseValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00097"></a>00097 {
<a name="l00098"></a>00098         <span class="keywordflow">if</span>( pCurNode == NULL )
<a name="l00099"></a>00099                 <span class="keywordflow">throw</span> <a class="code" href="classaltova_1_1_c_xml_exception.php">CXmlException</a>(CXmlException::eError1, _T(<span class="stringliteral">"Index out of range"</span>));
<a name="l00100"></a>00100         <span class="keywordflow">else</span>
<a name="l00101"></a>00101
<a name="l00102"></a>00102                 <span class="keywordflow">return</span> (LPCTSTR)pCurNode-&gt;text;
<a name="l00103"></a>00103 }
<a name="l00104"></a>00104
<a name="l00105"></a>00105
<a name="l00106"></a>00106
<a name="l00107"></a><a class="code" href="classica_1_1_c_use_item_type.php#ec5c05e7e1550bc5462b9468f62c7dd8">00107</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classica_1_1_c_use_item_type.php#ec5c05e7e1550bc5462b9468f62c7dd8">CUseItemType::GetItemSerialToUse</a>()
<a name="l00108"></a>00108 {
<a name="l00109"></a>00109         <span class="keywordflow">return</span> <a class="code" href="classica_1_1_c_use_item_type.php#c9cffa56204e19e32325430441660b7d">GetItemSerialToUseAt</a>(0);
<a name="l00110"></a>00110 }
<a name="l00111"></a>00111
<a name="l00112"></a>00112
<a name="l00113"></a><a class="code" href="classica_1_1_c_use_item_type.php#3723c0392dd888a92c2b293056ce4523">00113</a> <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_use_item_type.php#3723c0392dd888a92c2b293056ce4523">CUseItemType::RemoveItemSerialToUseAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00114"></a>00114 {
<a name="l00115"></a>00115         InternalRemoveAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemSerialToUse"</span>), nIndex);
<a name="l00116"></a>00116 }
<a name="l00117"></a>00117
<a name="l00118"></a>00118
<a name="l00119"></a><a class="code" href="classica_1_1_c_use_item_type.php#1414505e056c187fa72d1592bc1f0f43">00119</a> <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_use_item_type.php#1414505e056c187fa72d1592bc1f0f43">CUseItemType::RemoveItemSerialToUse</a>()
<a name="l00120"></a>00120 {
<a name="l00121"></a>00121         <span class="keywordflow">while</span> (<a class="code" href="classica_1_1_c_use_item_type.php#0d5c2caf7d4183ba4556a9e9e8375dc4">HasItemSerialToUse</a>())
<a name="l00122"></a>00122                 <a class="code" href="classica_1_1_c_use_item_type.php#3723c0392dd888a92c2b293056ce4523">RemoveItemSerialToUseAt</a>(0);
<a name="l00123"></a>00123 }
<a name="l00124"></a>00124
<a name="l00125"></a>00125 } <span class="comment">// end of namespace ica</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
