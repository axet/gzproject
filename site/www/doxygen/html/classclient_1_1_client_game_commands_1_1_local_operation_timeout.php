<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespaceclient.php">client</a>::<a class="el" href="classclient_1_1_client_game_commands.php">ClientGameCommands</a>::<a class="el" href="classclient_1_1_client_game_commands_1_1_local_operation_timeout.php">LocalOperationTimeout</a></div>
<h1>client::ClientGameCommands::LocalOperationTimeout Class Reference</h1><!-- doxytag: class="client::ClientGameCommands::LocalOperationTimeout" --><a href="classclient_1_1_client_game_commands_1_1_local_operation_timeout-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classclient_1_1_client_game_commands_1_1_local_operation_timeout.php#9e0122f351bd2a3e5671f634e470fa75">LocalOperationTimeout</a> (String p)</td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_client_game_commands_8java-source.php#l00006">6</a> of file <a class="el" href="_client_game_commands_8java-source.php">ClientGameCommands.java</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="9e0122f351bd2a3e5671f634e470fa75"></a><!-- doxytag: member="client::ClientGameCommands::LocalOperationTimeout::LocalOperationTimeout" ref="9e0122f351bd2a3e5671f634e470fa75" args="(String p)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">client::ClientGameCommands::LocalOperationTimeout::LocalOperationTimeout           </td>
          <td>(</td>
          <td class="paramtype">String&nbsp;</td>
          <td class="paramname"> <em>p</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_client_game_commands_8java-source.php#l00008">8</a> of file <a class="el" href="_client_game_commands_8java-source.php">ClientGameCommands.java</a>.<div class="fragment"><pre class="fragment"><a name="l00009"></a>00009         {
<a name="l00010"></a>00010             super(p);
<a name="l00011"></a>00011         }
</pre></div>
<p>

</div>
</div><p>
<hr>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="_client_game_commands_8java-source.php">ClientGameCommands.java</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
