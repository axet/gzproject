<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_9aa33d2cd8dd981e9a7ee8bcdb7921b2.php">library</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_913688eee9de0f0dbbd44e12ef912995.php">debugger</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_3a9a0bbfbb983552bc14e630455851c8.php">DebuggerWindows</a></div>
<h1>DebuggerCommon.cpp</h1><a href="_debugger_common_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net</span>
<a name="l00002"></a>00002
<a name="l00003"></a>00003 <span class="comment">// This program is free software; you can redistribute it and/or</span>
<a name="l00004"></a>00004 <span class="comment">// modify it under the terms of the GNU General Public License</span>
<a name="l00005"></a>00005 <span class="comment">// as published by the Free Software Foundation; either version 2</span>
<a name="l00006"></a>00006 <span class="comment">// of the License, or (at your option) any later version.</span>
<a name="l00007"></a>00007
<a name="l00008"></a>00008 <span class="comment">// This program is distributed in the hope that it will be useful,</span>
<a name="l00009"></a>00009 <span class="comment">// but WITHOUT ANY WARRANTY; without even the implied warranty of</span>
<a name="l00010"></a>00010 <span class="comment">// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the</span>
<a name="l00011"></a>00011 <span class="comment">// GNU General Public License for more details.</span>
<a name="l00012"></a>00012
<a name="l00013"></a>00013 <span class="comment">// You should have received a copy of the GNU General Public License</span>
<a name="l00014"></a>00014 <span class="comment">// along with this program; if not, write to the Free Software</span>
<a name="l00015"></a>00015 <span class="comment">// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</span>
<a name="l00016"></a>00016
<a name="l00017"></a>00017 <span class="preprocessor">#include "<a class="code" href="library_2debugger_2_debugger_windows_2_std_afx_8h.php">stdafx.h</a>"</span>
<a name="l00018"></a>00018 <span class="preprocessor">#include "<a class="code" href="_debugger_common_8h.php">debuggercommon.h</a>"</span>
<a name="l00019"></a>00019
<a name="l00020"></a><a class="code" href="class_debug.php#5fd1d25c9df649e8d2fc12d54594ba70">00020</a> LRESULT <a class="code" href="class_debug.php#5fd1d25c9df649e8d2fc12d54594ba70">Debug::OnIconMsg</a>(UINT uMsg, WPARAM <a class="code" href="flukeslave_8cpp.php#2c28a497a9b541541d5b8713a639a31b">wParam</a>, LPARAM <a class="code" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a>, BOOL &amp;bHandled)
<a name="l00021"></a>00021 {
<a name="l00022"></a>00022   <span class="keywordflow">switch</span>(lParam)
<a name="l00023"></a>00023   {
<a name="l00024"></a>00024   <span class="keywordflow">case</span> WM_LBUTTONUP:
<a name="l00025"></a>00025     ShowWindow((GetWindowLong(GWL_STYLE)&amp;WS_VISIBLE)==WS_VISIBLE?SW_HIDE:SW_SHOW);
<a name="l00026"></a>00026     <span class="keywordflow">break</span>;
<a name="l00027"></a>00027   <span class="keywordflow">case</span> WM_RBUTTONUP:
<a name="l00028"></a>00028     PostMessage(WM_CLOSE);
<a name="l00029"></a>00029     <span class="keywordflow">break</span>;
<a name="l00030"></a>00030   }
<a name="l00031"></a>00031   <span class="keywordflow">return</span> 0;
<a name="l00032"></a>00032 }
<a name="l00033"></a>00033
<a name="l00034"></a><a class="code" href="class_debug.php#e98d49a2b51464e8529919176d33e9e6">00034</a> LRESULT <a class="code" href="class_debug.php#e98d49a2b51464e8529919176d33e9e6">Debug::OnClose</a>(UINT uMsg, WPARAM <a class="code" href="flukeslave_8cpp.php#2c28a497a9b541541d5b8713a639a31b">wParam</a>, LPARAM <a class="code" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a>, BOOL &amp;bHandled)
<a name="l00035"></a>00035 {
<a name="l00036"></a>00036   <span class="comment">//ShowWindow(SW_HIDE);</span>
<a name="l00037"></a>00037   <span class="comment">//bHandled=TRUE;</span>
<a name="l00038"></a>00038   <span class="keywordflow">return</span> 0;
<a name="l00039"></a>00039 }
<a name="l00040"></a>00040
<a name="l00041"></a><a class="code" href="class_debug.php#5b453c195c4cfffed2702c3330f53a64">00041</a> <a class="code" href="class_debug.php#5b453c195c4cfffed2702c3330f53a64">Debug::Debug</a>():
<a name="l00042"></a>00042   <span class="comment">//m_debugger(IsDebuggerPresent()==TRUE)</span>
<a name="l00043"></a>00043   m_debugger(FALSE)
<a name="l00044"></a>00044 {
<a name="l00045"></a>00045   <span class="keywordflow">if</span>(!<a class="code" href="class_debug.php#91b0b93a1c5541e7d5367e3472df7884">m_debugger</a>)
<a name="l00046"></a>00046   {
<a name="l00047"></a>00047     <span class="keywordtype">char</span> buf[65];
<a name="l00048"></a>00048     itoa(GetCurrentThreadId(),buf,10);
<a name="l00049"></a>00049
<a name="l00050"></a>00050     std::string processname;
<a name="l00051"></a>00051     processname=<a class="code" href="namespace_ini_ext.php#e8ec57b84b079b6b4bcac40184ab24dc">GetModuleFileName</a>();
<a name="l00052"></a>00052
<a name="l00053"></a>00053     std::string log(processname+<span class="stringliteral">"_Debug_"</span>+buf+<span class="stringliteral">".log"</span>);
<a name="l00054"></a>00054
<a name="l00055"></a>00055     RECT rect;
<a name="l00056"></a>00056     CWindowImpl&lt;Debug&gt;::Create(GetDesktopWindow(),rcDefault,log.c_str(),WS_OVERLAPPEDWINDOW);
<a name="l00057"></a>00057     GetClientRect(&amp;rect);
<a name="l00058"></a>00058     <a class="code" href="class_debug.php#3df916671565b5c8d47f6b064cc067b1">m_edit</a>.Create(m_hWnd,rect,0,WS_VISIBLE|WS_CHILDWINDOW|
<a name="l00059"></a>00059       ES_MULTILINE|ES_AUTOVSCROLL|ES_READONLY|
<a name="l00060"></a>00060       ES_AUTOHSCROLL|ES_NOHIDESEL|WS_VSCROLL|WS_HSCROLL,0,1);
<a name="l00061"></a>00061     DlgResize_Init();
<a name="l00062"></a>00062
<a name="l00063"></a>00063     <a class="code" href="class_debug.php#3d80cfc90d4169b0775f631219efeec5">m_icons</a>.push_back(<a class="code" href="struct_shell_nofity_icon_1_1_icon.php">ShellNofityIcon::Icon</a>(0,IDI_INFORMATION,
<a name="l00064"></a>00064       log.c_str(),m_hWnd,WM_USER+1));
<a name="l00065"></a>00065
<a name="l00066"></a>00066     <a class="code" href="class_shell_nofity_icon.php#abae668e6d96288d1729d7799413a99b">ShellNofityIcon::ShowIcon</a>(<a class="code" href="class_debug.php#3d80cfc90d4169b0775f631219efeec5">m_icons</a>);
<a name="l00067"></a>00067   }
<a name="l00068"></a>00068 }
<a name="l00069"></a>00069
<a name="l00070"></a><a class="code" href="class_debug.php#911a84a0f56b770724e09a049399dc30">00070</a> <a class="code" href="class_debug.php#911a84a0f56b770724e09a049399dc30">Debug::~Debug</a>()
<a name="l00071"></a>00071 {
<a name="l00072"></a>00072 }
<a name="l00073"></a>00073
<a name="l00074"></a><a class="code" href="class_debug.php#93e88932b69876de978ba2a4e21a90ca">00074</a> <span class="keywordtype">void</span> <a class="code" href="class_debug.php#93e88932b69876de978ba2a4e21a90ca">Debug::OnFinalMessage</a>(HWND hWnd)
<a name="l00075"></a>00075 {
<a name="l00076"></a>00076   <a class="code" href="class_shell_nofity_icon.php#efd41806965c605d5fe4362ea1ce244a">ShellNofityIcon::HideIcon</a>(<a class="code" href="class_debug.php#3d80cfc90d4169b0775f631219efeec5">m_icons</a>);
<a name="l00077"></a>00077
<a name="l00078"></a>00078   <span class="comment">//delete this;</span>
<a name="l00079"></a>00079 }
<a name="l00080"></a>00080
<a name="l00081"></a><a class="code" href="class_debug.php#e94245586ffce3b8b74a66e144740a8f">00081</a> <a class="code" href="class_debug.php">Debug</a>&amp; <a class="code" href="class_debug.php#e94245586ffce3b8b74a66e144740a8f">Debug::operator &lt;&lt; </a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* p)
<a name="l00082"></a>00082 {
<a name="l00083"></a>00083   <span class="keywordflow">if</span>(!<a class="code" href="class_debug.php#91b0b93a1c5541e7d5367e3472df7884">m_debugger</a>)
<a name="l00084"></a>00084   {
<a name="l00085"></a>00085     WTL::CString str(p);
<a name="l00086"></a>00086     str.Replace(<span class="stringliteral">"\n"</span>,<span class="stringliteral">"\r\n"</span>);
<a name="l00087"></a>00087     <a class="code" href="class_debug.php#3df916671565b5c8d47f6b064cc067b1">m_edit</a>.AppendText(str);
<a name="l00088"></a>00088   }<span class="keywordflow">else</span>
<a name="l00089"></a>00089   {
<a name="l00090"></a>00090     OutputDebugString(p);
<a name="l00091"></a>00091   }
<a name="l00092"></a>00092
<a name="l00093"></a>00093   <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00094"></a>00094 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
