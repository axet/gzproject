<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="class_c_client_u_o_commands.php">CClientUOCommands</a>::<a class="el" href="class_c_client_u_o_commands_1_1_sentry_command.php">SentryCommand</a>::<a class="el" href="class_c_client_u_o_commands_1_1_sentry_command_1_1_sentry_command_timeout.php">SentryCommandTimeout</a></div>
<h1>CClientUOCommands::SentryCommand::SentryCommandTimeout Class Reference</h1><!-- doxytag: class="CClientUOCommands::SentryCommand::SentryCommandTimeout" --><code>#include &lt;<a class="el" href="_client_u_o_commands_8h-source.php">ClientUOCommands.h</a>&gt;</code>
<p>
<a href="class_c_client_u_o_commands_1_1_sentry_command_1_1_sentry_command_timeout-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_client_u_o_commands_1_1_sentry_command_1_1_sentry_command_timeout.php#e24b1b7b68bfad709a838af489e9c8eb">SentryCommandTimeout</a> (<a class="el" href="class_c_client_u_o_commands.php#b0e5ae19135767820d7f2bda2d281095">CmdSet</a> &amp;)</td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_client_u_o_commands_8h-source.php#l00110">110</a> of file <a class="el" href="_client_u_o_commands_8h-source.php">ClientUOCommands.h</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="e24b1b7b68bfad709a838af489e9c8eb"></a><!-- doxytag: member="CClientUOCommands::SentryCommand::SentryCommandTimeout::SentryCommandTimeout" ref="e24b1b7b68bfad709a838af489e9c8eb" args="(CmdSet &amp;)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">CClientUOCommands::SentryCommand::SentryCommandTimeout::SentryCommandTimeout           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_c_client_u_o_commands.php#b0e5ae19135767820d7f2bda2d281095">CmdSet</a> &amp;&nbsp;</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_client_u_o_commands_8cpp-source.php#l00429">429</a> of file <a class="el" href="_client_u_o_commands_8cpp-source.php">ClientUOCommands.cpp</a>.
<p>
References <a class="el" href="ethinterface_8h-source.php#l00056">EthInterface::m_messages</a>.<div class="fragment"><pre class="fragment"><a name="l00430"></a>00430 {
<a name="l00431"></a>00431   std::string str;
<a name="l00432"></a>00432   <span class="keywordflow">for</span>(CmdSet::iterator i=s.begin();i!=s.end();i++)
<a name="l00433"></a>00433   {
<a name="l00434"></a>00434     str+=<a class="code" href="class_eth_interface.php#8c1f1885f30b1d980bce4c2a26498533">EthInterface::m_messages</a>[(<a class="code" href="class_server_1_1_server_int.php">Server::ServerInt</a>)*i].name;
<a name="l00435"></a>00435     str+=<span class="stringliteral">"\n"</span>;
<a name="l00436"></a>00436   }
<a name="l00437"></a>00437   exception::operator =(exception(str.c_str()));
<a name="l00438"></a>00438 }
</pre></div>
<p>

</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="_client_u_o_commands_8h-source.php">ClientUOCommands.h</a><li><a class="el" href="_client_u_o_commands_8cpp-source.php">ClientUOCommands.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
