<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ea6c3855b402b97c9537b801f064bee.php">GZone</a></div>
<h1>SoldierListView.cpp File Reference</h1><code>#include &quot;<a class="el" href="_g_z_2_g_zone_2_std_afx_8h-source.php">stdafx.h</a>&quot;</code><br>
<code>#include &quot;<a class="el" href="_g_zone_8h-source.php">GZone.h</a>&quot;</code><br>
<code>#include &quot;<a class="el" href="_soldier_list_view_8h-source.php">SoldierListView.h</a>&quot;</code><br>
<code>#include &quot;<a class="el" href="_edit_unit_dlg_8h-source.php">editunitdlg.h</a>&quot;</code><br>
<code>#include &quot;<a class="el" href="_g_zone_doc_8h-source.php">gzonedoc.h</a>&quot;</code><br>
<code>#include &quot;<a class="el" href="_g_zone_2_main_frm_8h-source.php">MainFrm.h</a>&quot;</code><br>
<code>#include &quot;<a class="el" href="_options_dlg_8h-source.php">OptionsDlg.h</a>&quot;</code><br>
<code>#include &quot;<a class="el" href="_interruptable_unit_8h-source.php">gzunit/InterruptableUnit.h</a>&quot;</code><br>
<code>#include &lt;exception&gt;</code><br>

<p>
<a href="_soldier_list_view_8cpp-source.php">Go to the source code of this file.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Variables</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="struct_options_dlg.php">OptionsDlg</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="_soldier_list_view_8cpp.php#c393aeedfc67ca74e911f47f3a9b5c15">g_optionsdlg</a></td></tr>

</table>
<hr><h2>Variable Documentation</h2>
<a class="anchor" name="c393aeedfc67ca74e911f47f3a9b5c15"></a><!-- doxytag: member="SoldierListView.cpp::g_optionsdlg" ref="c393aeedfc67ca74e911f47f3a9b5c15" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="struct_options_dlg.php">OptionsDlg</a> <a class="el" href="_soldier_list_view_8cpp.php#c393aeedfc67ca74e911f47f3a9b5c15">g_optionsdlg</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_zone_8cpp-source.php#l00055">55</a> of file <a class="el" href="_g_zone_8cpp-source.php">GZone.cpp</a>.
<p>
Referenced by <a class="el" href="_g_zone_2_main_frm_8cpp-source.php#l00213">CMainFrame::LoadBGImage()</a>, <a class="el" href="_g_zone_8cpp-source.php#l00289">CGZoneApp::LoadUI()</a>, <a class="el" href="_g_zone_2_main_frm_8cpp-source.php#l00176">CMainFrame::OnFileOptions()</a>, <a class="el" href="_soldier_list_view_8cpp-source.php#l00135">CSoldierListView::OnUnitRunclient()</a>, and <a class="el" href="_g_zone_8cpp-source.php#l00333">CGZoneApp::SaveUI()</a>.
</div>
</div><p>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
