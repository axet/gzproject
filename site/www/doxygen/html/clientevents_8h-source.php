<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_58c9cd060c988d7c9a1f81a2de283682.php">client</a></div>
<h1>clientevents.h</h1><a href="clientevents_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#ifndef CLIENTEVENTS_H</span>
<a name="l00002"></a>00002 <span class="preprocessor"></span><span class="preprocessor">#define CLIENTEVENTS_H</span>
<a name="l00003"></a>00003 <span class="preprocessor"></span>
<a name="l00004"></a>00004 <span class="comment">/*</span>
<a name="l00005"></a>00005 <span class="comment">реализация класса, те этот на этот класс приходят все события уже разжованные</span>
<a name="l00006"></a>00006 <span class="comment"></span>
<a name="l00007"></a>00007 <span class="comment">перехватывает CGZUser</span>
<a name="l00008"></a>00008 <span class="comment"></span>
<a name="l00009"></a>00009 <span class="comment">*/</span>
<a name="l00010"></a>00010
<a name="l00011"></a>00011 <span class="preprocessor">#include "<a class="code" href="client_2clientgameevents_8h.php">clientgameevents.h</a>"</span>
<a name="l00012"></a>00012
<a name="l00013"></a><a class="code" href="class_c_client_events.php">00013</a> <span class="keyword">class </span><a class="code" href="class_c_client_events.php">CClientEvents</a>:<span class="keyword">public</span> <a class="code" href="class_c_client_game_events.php">CClientGameEvents</a>
<a name="l00014"></a>00014 {
<a name="l00015"></a>00015 <span class="keyword">public</span>:
<a name="l00016"></a><a class="code" href="class_c_client_events.php#98d2417fcb633d19d5d97ee5a4f46aae">00016</a>   <span class="keyword">virtual</span> <a class="code" href="class_c_client_events.php#98d2417fcb633d19d5d97ee5a4f46aae">~CClientEvents</a>() {}
<a name="l00017"></a>00017   <span class="comment">// когда клиент.exe запущен</span>
<a name="l00018"></a><a class="code" href="class_c_client_events.php#cdabefe17ca4a53b545987acea68cc88">00018</a>   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_client_events.php#cdabefe17ca4a53b545987acea68cc88">Connected</a>() {};
<a name="l00019"></a><a class="code" href="class_c_client_events.php#e0e26acef775bc123d8bf4aa09375d45">00019</a>   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_client_events.php#e0e26acef775bc123d8bf4aa09375d45">LocalConnectionLost</a>() {};
<a name="l00020"></a>00020   <span class="comment">// клиент вызвает эту фукнцию когда с сервера приходит сообщение</span>
<a name="l00021"></a>00021   <span class="comment">// по сообщению лога нельзя понять какого оно типа (в окне журнала</span>
<a name="l00022"></a>00022   <span class="comment">// они разных цветов\типов)</span>
<a name="l00023"></a><a class="code" href="class_c_client_events.php#dd879835553b3df975c1ffb208331933">00023</a>   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_client_events.php#dd879835553b3df975c1ffb208331933">LogMessage</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* msg) {}
<a name="l00024"></a>00024
<a name="l00025"></a>00025   <span class="comment">// когда логин на сервер выполенн</span>
<a name="l00026"></a><a class="code" href="class_c_client_events.php#4ee3b03cc4a432f30b7bf496ec8f0afc">00026</a>   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_client_events.php#4ee3b03cc4a432f30b7bf496ec8f0afc">ConnectionEstablished</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* name,<span class="keyword">const</span> <span class="keywordtype">char</span>* pass, <span class="keyword">const</span> <span class="keywordtype">char</span>* charname) {};
<a name="l00027"></a>00027   <span class="comment">// когда связь разорвана</span>
<a name="l00028"></a><a class="code" href="class_c_client_events.php#cd7ed4c89fb8b6b4538b2e18c848522d">00028</a>   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_client_events.php#cd7ed4c89fb8b6b4538b2e18c848522d">ConnectionLost</a>() {};
<a name="l00029"></a>00029
<a name="l00030"></a>00030   <span class="comment">// приложение клиента завершило работу</span>
<a name="l00031"></a>00031   <span class="comment">// коды ошибок:</span>
<a name="l00032"></a>00032   <span class="comment">//   0 нормальное заврершение работы</span>
<a name="l00033"></a>00033   <span class="comment">//   -1 фатальнафя ошибка, клиент закрыт</span>
<a name="l00034"></a><a class="code" href="class_c_client_events.php#246466f10cb4160cf53e297890a6673e">00034</a>   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_client_events.php#246466f10cb4160cf53e297890a6673e">Exit</a>(<span class="keywordtype">unsigned</span> <span class="keywordtype">int</span> exitcode) {}
<a name="l00035"></a>00035 };
<a name="l00036"></a>00036
<a name="l00037"></a>00037 <span class="preprocessor">#endif</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
