<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespace_packets.php">Packets</a>::<a class="el" href="struct_packets_1_1scs_cities_and_chars.php">scsCitiesAndChars</a>::<a class="el" href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag.php">Chars_tag</a></div>
<h1>Packets::scsCitiesAndChars::Chars_tag Struct Reference</h1><!-- doxytag: class="Packets::scsCitiesAndChars::Chars_tag" --><code>#include &lt;<a class="el" href="packets_8h-source.php">packets.h</a>&gt;</code>
<p>
<a href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="namespace_server.php#c2b5b0c54059a0371f64b64dae190caf">Server::ServerByte</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag.php#62e0b5e3b98a99308d02012939f8a33f">numchars</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag_1_1_char__tag.php">Char_tag</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag.php#d9f6c31e88a2ebbda665e7a2e8151ddc">Char</a> [1]</td></tr>

<tr><td colspan="2"><br><h2>Classes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">struct &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag_1_1_char__tag.php">Char_tag</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00511">511</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="62e0b5e3b98a99308d02012939f8a33f"></a><!-- doxytag: member="Packets::scsCitiesAndChars::Chars_tag::numchars" ref="62e0b5e3b98a99308d02012939f8a33f" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="namespace_server.php#c2b5b0c54059a0371f64b64dae190caf">Server::ServerByte</a> <a class="el" href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag.php#62e0b5e3b98a99308d02012939f8a33f">Packets::scsCitiesAndChars::Chars_tag::numchars</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00513">513</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="packets_8cpp-source.php#l00228">Packets::scsCitiesAndChars::GetCharCount()</a>, <a class="el" href="packets_8cpp-source.php#l00223">Packets::scsCitiesAndChars::GetCities()</a>, and <a class="el" href="login_8cpp-source.php#l00009">ClientUO::CLogin::Login()</a>.
</div>
</div><p>
<a class="anchor" name="d9f6c31e88a2ebbda665e7a2e8151ddc"></a><!-- doxytag: member="Packets::scsCitiesAndChars::Chars_tag::Char" ref="d9f6c31e88a2ebbda665e7a2e8151ddc" args="[1]" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag_1_1_char__tag.php">Char_tag</a> <a class="el" href="struct_packets_1_1scs_cities_and_chars_1_1_chars__tag.php#d9f6c31e88a2ebbda665e7a2e8151ddc">Packets::scsCitiesAndChars::Chars_tag::Char</a>[1]          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00518">518</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="packets_8cpp-source.php#l00233">Packets::scsCitiesAndChars::GetCharInfo()</a>, <a class="el" href="packets_8cpp-source.php#l00223">Packets::scsCitiesAndChars::GetCities()</a>, and <a class="el" href="login_8cpp-source.php#l00009">ClientUO::CLogin::Login()</a>.
</div>
</div><p>
<hr>The documentation for this struct was generated from the following file:<ul>
<li><a class="el" href="packets_8h-source.php">packets.h</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
