<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li id="current"><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="namespaces.php"><span>Namespace List</span></a></li>
    <li><a href="namespacemembers.php"><span>Namespace&nbsp;Members</span></a></li>
  </ul></div>
<h1>Fluke Namespace Reference</h1>модуль встраивание во внешнее НЕ загружонное приложение.
<a href="#_details">More...</a>
<p>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Classes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_body.php">CBody</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Тело експлоита которое передаеться в процесс.  <a href="class_fluke_1_1_c_body.php#_details">More...</a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_fluke_exception.php">FlukeException</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_fluke_master.php">CFlukeMaster</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">класс контролер инфицирого приложения.  <a href="class_fluke_1_1_c_fluke_master.php#_details">More...</a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1interprocexception.php">interprocexception</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_sync_server.php">CSyncServer</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">класс для сонхронизации мастера со слейвом.  <a href="class_fluke_1_1_c_sync_server.php#_details">More...</a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_fluke_1_1_c_sync_client.php">CSyncClient</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">класс для синзронизации слейва с мастером.  <a href="class_fluke_1_1_c_sync_client.php#_details">More...</a><br></td></tr>
<tr><td colspan="2"><br><h2>Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">class&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="namespace_fluke.php#5f1d27a5c3e65e4b4d0e98f8ce09bc68">__declspec</a> (novtable) CFlukeSlave</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">должен жить пока загружена Длл (глобальный класс).  <a href="#5f1d27a5c3e65e4b4d0e98f8ce09bc68"></a><br></td></tr>
<tr><td colspan="2"><br><h2>Variables</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">const char&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="namespace_fluke.php#4f37bf76477f52e1e19b8c24d3e2647b">g_flukeslaveready</a> []</td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>
модуль встраивание во внешнее НЕ загружонное приложение.
<p>
Спомощью этого модуля можно легко подключиться к любому процессу приложения, перехватывать обращения к люым библиотечным фукнциям, контролировать передачупараметров, заменять их, отписывать события на сторону контролирующей стороны. Так же имеються фукнции для перехвата внутренних фукнций приложения и управления ими , тех фукнций которые не являються билиотекчными, не экпортируемых, предварительно найденых спомощью дизассемблеров или отладчика. метод инфицирования основан на подгрузке бибилиотеки, собранной на высокоуровневом языке, таком как с++, VB, java. сам процесс инфцицированя реализован на подставлении тела вируса написанного на асемблере, в код инфицируемого процесса.
<p>
<hr><h2>Function Documentation</h2>
<a class="anchor" name="5f1d27a5c3e65e4b4d0e98f8ce09bc68"></a><!-- doxytag: member="Fluke::__declspec" ref="5f1d27a5c3e65e4b4d0e98f8ce09bc68" args="(novtable) CFlukeSlave" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">class Fluke::__declspec           </td>
          <td>(</td>
          <td class="paramtype">novtable&nbsp;</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
должен жить пока загружена Длл (глобальный класс).
<p>
в нем есть мутекс котороый говорит что этот процесс успешно подсоеденен. объект создаеться когда инфицированная длл уже загружна, она должна создать один экземпляр CFlukeSlave и он информирует основной процесс (масера) о том что длл подгружена и можно вернуть основному процессу нормльную фукнциональност, те можно убить вирус из кода процесса и продолжить испольнения с той точки где он был прерван.
<p>
mutex который говорит что длл успешно загружена FLukeMaster сначало проверит этот фалг а только оптом будет пробовать ожидать клиента<p>
Хендл на бибилотеку которую мы держим (защищаем сами себя от выгрузки)<p>
возврващет текущий хендл, чтобы можно было найдти имя длл которая создала экземлпяр этого классса<p>
если инициализация флукеслейва проходит успешно данный метод будет вызнан что означает - главному приложению можно инициализироваться<p>
лучше конечно делать конструктор, это нагладнее, но в этом случае не будет работать виртульный вызов FlukeInit для приложения<p>
когда длл выгнужаеться нужно закрыть хендлы.
<p>
Definition at line <a class="el" href="flukeslave_8h-source.php#l00037">37</a> of file <a class="el" href="flukeslave_8h-source.php">flukeslave.h</a>.<div class="fragment"><pre class="fragment"><a name="l00038"></a>00038   {
<a name="l00042"></a>00042     HANDLE m_globalpass;
<a name="l00044"></a>00044     HMODULE m_library;
<a name="l00045"></a>00045
<a name="l00046"></a>00046     <span class="keywordtype">void</span> flukeslave(HMODULE);
<a name="l00049"></a>00049     <span class="keyword">static</span> HMODULE GetCurrentModuleHandle();
<a name="l00050"></a>00050
<a name="l00053"></a>00053     <span class="keyword">virtual</span> <span class="keywordtype">void</span> FlukeInit(HMODULE) {};
<a name="l00054"></a>00054   <span class="keyword">public</span>:
<a name="l00055"></a>00055     CFlukeSlave();
<a name="l00056"></a>00056     ~CFlukeSlave();
<a name="l00057"></a>00057
<a name="l00060"></a>00060     <span class="keywordtype">void</span> Create(HMODULE h = 0);
<a name="l00062"></a>00062     <span class="keywordtype">void</span> Close();
<a name="l00063"></a>00063   };
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Variable Documentation</h2>
<a class="anchor" name="4f37bf76477f52e1e19b8c24d3e2647b"></a><!-- doxytag: member="Fluke::g_flukeslaveready" ref="4f37bf76477f52e1e19b8c24d3e2647b" args="[]" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">const char <a class="el" href="namespace_fluke.php#4f37bf76477f52e1e19b8c24d3e2647b">Fluke::g_flukeslaveready</a>[]          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="fluke_8cpp-source.php#l00001">1</a> of file <a class="el" href="fluke_8cpp-source.php">fluke.cpp</a>.
<p>
Referenced by <a class="el" href="flukemaster_8cpp-source.php#l00174">Fluke::CFlukeMaster::flukemaster()</a>.
</div>
</div><p>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
