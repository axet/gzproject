<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespacessa.php">ssa</a>::<a class="el" href="classssa_1_1_c_config_type.php">CConfigType</a></div>
<h1>ssa::CConfigType Class Reference</h1><!-- doxytag: class="ssa::CConfigType" --><code>#include &lt;<a class="el" href="_schema_server_config___c_config_type_8h-source.php">SchemaServerConfig_CConfigType.h</a>&gt;</code>
<p>
<a href="classssa_1_1_c_config_type-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#c2f3f16701581ebdea30efed37a4e4b2">CConfigType</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#394acab65bb8405a55434712671ebb44">CConfigType</a> (CNode &amp;rParentNode, MSXML2::IXMLDOMNodePtr spThisNode)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#364149a98ef6ebd2bc752e2676c66361">CConfigType</a> (MSXML2::IXMLDOMDocument2Ptr spDoc)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#dd8574a4b0fcdd4d60f12d39bb2b7e74">GetPortCount</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#3e3c64f0852fde3b5ac061a7679b5254">HasPort</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#5cdd8396e8de1c69a4bf27c902b19e98">AddPort</a> (CSchemaInt Port)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#201629f019c6df6a07ac08928c9b941d">InsertPortAt</a> (CSchemaInt Port, int nIndex)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#202bfe162b8e7c562e2a8dc7a4f5fc70">ReplacePortAt</a> (CSchemaInt Port, int nIndex)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#4c695ebdea28443dc227934dba9529f1">GetPortAt</a> (int nIndex)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#271d914aa602f6094511b69076050bdd">GetPort</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">MSXML2::IXMLDOMNodePtr&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#90b76d93f7685df1e980654d84921b38">GetStartingPortCursor</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">MSXML2::IXMLDOMNodePtr&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#9b7b248273cf2a3ba7473e8ca6373698">GetAdvancedPortCursor</a> (MSXML2::IXMLDOMNodePtr pCurNode)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#106cf2c69c1f12bfc443500227f04224">GetPortValueAtCursor</a> (MSXML2::IXMLDOMNodePtr pCurNode)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#0ff41a050508e96dcc2611345f8b4d55">RemovePortAt</a> (int nIndex)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#bfaa0c7b4fd0243e86b28dd0a98e3982">RemovePort</a> ()</td></tr>

<tr><td colspan="2"><br><h2>Static Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">static EGroupType&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#607e79c2f7e549f6ec39b7eb1c62a409">GetGroupType</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">static int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#d54ac683af744d80a8c2d1b897816cd3">GetPortMinCount</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">static int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classssa_1_1_c_config_type.php#532231cf14510620abeed7db764dc928">GetPortMaxCount</a> ()</td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8h-source.php#l00030">30</a> of file <a class="el" href="_schema_server_config___c_config_type_8h-source.php">SchemaServerConfig_CConfigType.h</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="c2f3f16701581ebdea30efed37a4e4b2"></a><!-- doxytag: member="ssa::CConfigType::CConfigType" ref="c2f3f16701581ebdea30efed37a4e4b2" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">ssa::CConfigType::CConfigType           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8h-source.php#l00033">33</a> of file <a class="el" href="_schema_server_config___c_config_type_8h-source.php">SchemaServerConfig_CConfigType.h</a>.<div class="fragment"><pre class="fragment"><a name="l00033"></a>00033 : CNode() {}
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="394acab65bb8405a55434712671ebb44"></a><!-- doxytag: member="ssa::CConfigType::CConfigType" ref="394acab65bb8405a55434712671ebb44" args="(CNode &amp;rParentNode, MSXML2::IXMLDOMNodePtr spThisNode)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">ssa::CConfigType::CConfigType           </td>
          <td>(</td>
          <td class="paramtype">CNode &amp;&nbsp;</td>
          <td class="paramname"> <em>rParentNode</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">MSXML2::IXMLDOMNodePtr&nbsp;</td>
          <td class="paramname"> <em>spThisNode</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8h-source.php#l00034">34</a> of file <a class="el" href="_schema_server_config___c_config_type_8h-source.php">SchemaServerConfig_CConfigType.h</a>.<div class="fragment"><pre class="fragment"><a name="l00034"></a>00034 : CNode(rParentNode, spThisNode) {}
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="364149a98ef6ebd2bc752e2676c66361"></a><!-- doxytag: member="ssa::CConfigType::CConfigType" ref="364149a98ef6ebd2bc752e2676c66361" args="(MSXML2::IXMLDOMDocument2Ptr spDoc)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">ssa::CConfigType::CConfigType           </td>
          <td>(</td>
          <td class="paramtype">MSXML2::IXMLDOMDocument2Ptr&nbsp;</td>
          <td class="paramname"> <em>spDoc</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8h-source.php#l00035">35</a> of file <a class="el" href="_schema_server_config___c_config_type_8h-source.php">SchemaServerConfig_CConfigType.h</a>.<div class="fragment"><pre class="fragment"><a name="l00035"></a>00035 : CNode(spDoc) {}
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="607e79c2f7e549f6ec39b7eb1c62a409"></a><!-- doxytag: member="ssa::CConfigType::GetGroupType" ref="607e79c2f7e549f6ec39b7eb1c62a409" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="classaltova_1_1_c_node.php#23a91b5b6ef1abca12745ecd7a2fe7a4">CNode::EGroupType</a> ssa::CConfigType::GetGroupType           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00031">31</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00032"></a>00032 {
<a name="l00033"></a>00033         <span class="keywordflow">return</span> eSequence;
<a name="l00034"></a>00034 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="d54ac683af744d80a8c2d1b897816cd3"></a><!-- doxytag: member="ssa::CConfigType::GetPortMinCount" ref="d54ac683af744d80a8c2d1b897816cd3" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int ssa::CConfigType::GetPortMinCount           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00035">35</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00036"></a>00036 {
<a name="l00037"></a>00037         <span class="keywordflow">return</span> 1;
<a name="l00038"></a>00038 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="532231cf14510620abeed7db764dc928"></a><!-- doxytag: member="ssa::CConfigType::GetPortMaxCount" ref="532231cf14510620abeed7db764dc928" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int ssa::CConfigType::GetPortMaxCount           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00041">41</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00042"></a>00042 {
<a name="l00043"></a>00043         <span class="keywordflow">return</span> 1;
<a name="l00044"></a>00044 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="dd8574a4b0fcdd4d60f12d39bb2b7e74"></a><!-- doxytag: member="ssa::CConfigType::GetPortCount" ref="dd8574a4b0fcdd4d60f12d39bb2b7e74" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int ssa::CConfigType::GetPortCount           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00047">47</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00048"></a>00048 {
<a name="l00049"></a>00049         <span class="keywordflow">return</span> ChildCountInternal(Element, _T(<span class="stringliteral">"http://gzproject.sf.net/ssa"</span>), _T(<span class="stringliteral">"ssa:Port"</span>));
<a name="l00050"></a>00050 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="3e3c64f0852fde3b5ac061a7679b5254"></a><!-- doxytag: member="ssa::CConfigType::HasPort" ref="3e3c64f0852fde3b5ac061a7679b5254" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">bool ssa::CConfigType::HasPort           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00053">53</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.
<p>
Referenced by <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00119">RemovePort()</a>.<div class="fragment"><pre class="fragment"><a name="l00054"></a>00054 {
<a name="l00055"></a>00055         <span class="keywordflow">return</span> InternalHasChild(Element, _T(<span class="stringliteral">"http://gzproject.sf.net/ssa"</span>), _T(<span class="stringliteral">"ssa:Port"</span>));
<a name="l00056"></a>00056 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="5cdd8396e8de1c69a4bf27c902b19e98"></a><!-- doxytag: member="ssa::CConfigType::AddPort" ref="5cdd8396e8de1c69a4bf27c902b19e98" args="(CSchemaInt Port)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ssa::CConfigType::AddPort           </td>
          <td>(</td>
          <td class="paramtype">CSchemaInt&nbsp;</td>
          <td class="paramname"> <em>Port</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Referenced by <a class="el" href="_server_addin_dlg_8cpp-source.php#l00198">ServerAddinDlg::SaveConfig()</a>.
</div>
</div><p>
<a class="anchor" name="201629f019c6df6a07ac08928c9b941d"></a><!-- doxytag: member="ssa::CConfigType::InsertPortAt" ref="201629f019c6df6a07ac08928c9b941d" args="(CSchemaInt Port, int nIndex)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ssa::CConfigType::InsertPortAt           </td>
          <td>(</td>
          <td class="paramtype">CSchemaInt&nbsp;</td>
          <td class="paramname"> <em>Port</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nIndex</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<a class="anchor" name="202bfe162b8e7c562e2a8dc7a4f5fc70"></a><!-- doxytag: member="ssa::CConfigType::ReplacePortAt" ref="202bfe162b8e7c562e2a8dc7a4f5fc70" args="(CSchemaInt Port, int nIndex)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ssa::CConfigType::ReplacePortAt           </td>
          <td>(</td>
          <td class="paramtype">CSchemaInt&nbsp;</td>
          <td class="paramname"> <em>Port</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nIndex</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<a class="anchor" name="4c695ebdea28443dc227934dba9529f1"></a><!-- doxytag: member="ssa::CConfigType::GetPortAt" ref="4c695ebdea28443dc227934dba9529f1" args="(int nIndex)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> ssa::CConfigType::GetPortAt           </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nIndex</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00080">80</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.
<p>
Referenced by <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00107">GetPort()</a>.<div class="fragment"><pre class="fragment"><a name="l00081"></a>00081 {
<a name="l00082"></a>00082         <span class="keywordflow">return</span> (LPCTSTR)InternalGetAt(Element, _T(<span class="stringliteral">"http://gzproject.sf.net/ssa"</span>), _T(<span class="stringliteral">"ssa:Port"</span>), nIndex)-&gt;text;
<a name="l00083"></a>00083         <span class="comment">//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sf.net/ssa"), _T("ssa:Port"), nIndex)-&gt;text);</span>
<a name="l00084"></a>00084 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="271d914aa602f6094511b69076050bdd"></a><!-- doxytag: member="ssa::CConfigType::GetPort" ref="271d914aa602f6094511b69076050bdd" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> ssa::CConfigType::GetPort           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00107">107</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.
<p>
References <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00080">GetPortAt()</a>.
<p>
Referenced by <a class="el" href="_server_addin_dlg_8cpp-source.php#l00177">ServerAddinDlg::LoadConfig()</a>.<div class="fragment"><pre class="fragment"><a name="l00108"></a>00108 {
<a name="l00109"></a>00109         <span class="keywordflow">return</span> <a class="code" href="classssa_1_1_c_config_type.php#4c695ebdea28443dc227934dba9529f1">GetPortAt</a>(0);
<a name="l00110"></a>00110 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="90b76d93f7685df1e980654d84921b38"></a><!-- doxytag: member="ssa::CConfigType::GetStartingPortCursor" ref="90b76d93f7685df1e980654d84921b38" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">MSXML2::IXMLDOMNodePtr ssa::CConfigType::GetStartingPortCursor           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00086">86</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00087"></a>00087 {
<a name="l00088"></a>00088         <span class="keywordflow">return</span> InternalGetFirstChild(Element, _T(<span class="stringliteral">"http://gzproject.sf.net/ssa"</span>), _T(<span class="stringliteral">"ssa:Port"</span>));
<a name="l00089"></a>00089 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="9b7b248273cf2a3ba7473e8ca6373698"></a><!-- doxytag: member="ssa::CConfigType::GetAdvancedPortCursor" ref="9b7b248273cf2a3ba7473e8ca6373698" args="(MSXML2::IXMLDOMNodePtr pCurNode)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">MSXML2::IXMLDOMNodePtr ssa::CConfigType::GetAdvancedPortCursor           </td>
          <td>(</td>
          <td class="paramtype">MSXML2::IXMLDOMNodePtr&nbsp;</td>
          <td class="paramname"> <em>pCurNode</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00091">91</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00092"></a>00092 {
<a name="l00093"></a>00093         <span class="keywordflow">return</span> InternalGetNextChild(Element, _T(<span class="stringliteral">"http://gzproject.sf.net/ssa"</span>), _T(<span class="stringliteral">"ssa:Port"</span>), pCurNode);
<a name="l00094"></a>00094 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="106cf2c69c1f12bfc443500227f04224"></a><!-- doxytag: member="ssa::CConfigType::GetPortValueAtCursor" ref="106cf2c69c1f12bfc443500227f04224" args="(MSXML2::IXMLDOMNodePtr pCurNode)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> ssa::CConfigType::GetPortValueAtCursor           </td>
          <td>(</td>
          <td class="paramtype">MSXML2::IXMLDOMNodePtr&nbsp;</td>
          <td class="paramname"> <em>pCurNode</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00096">96</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00097"></a>00097 {
<a name="l00098"></a>00098         <span class="keywordflow">if</span>( pCurNode == NULL )
<a name="l00099"></a>00099                 <span class="keywordflow">throw</span> <a class="code" href="classaltova_1_1_c_xml_exception.php">CXmlException</a>(CXmlException::eError1, _T(<span class="stringliteral">"Index out of range"</span>));
<a name="l00100"></a>00100         <span class="keywordflow">else</span>
<a name="l00101"></a>00101
<a name="l00102"></a>00102                 <span class="keywordflow">return</span> (LPCTSTR)pCurNode-&gt;text;
<a name="l00103"></a>00103 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="0ff41a050508e96dcc2611345f8b4d55"></a><!-- doxytag: member="ssa::CConfigType::RemovePortAt" ref="0ff41a050508e96dcc2611345f8b4d55" args="(int nIndex)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ssa::CConfigType::RemovePortAt           </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nIndex</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00113">113</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.
<p>
Referenced by <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00119">RemovePort()</a>.<div class="fragment"><pre class="fragment"><a name="l00114"></a>00114 {
<a name="l00115"></a>00115         InternalRemoveAt(Element, _T(<span class="stringliteral">"http://gzproject.sf.net/ssa"</span>), _T(<span class="stringliteral">"ssa:Port"</span>), nIndex);
<a name="l00116"></a>00116 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="bfaa0c7b4fd0243e86b28dd0a98e3982"></a><!-- doxytag: member="ssa::CConfigType::RemovePort" ref="bfaa0c7b4fd0243e86b28dd0a98e3982" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ssa::CConfigType::RemovePort           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00119">119</a> of file <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a>.
<p>
References <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00053">HasPort()</a>, and <a class="el" href="_schema_server_config___c_config_type_8cpp-source.php#l00113">RemovePortAt()</a>.
<p>
Referenced by <a class="el" href="_server_addin_dlg_8cpp-source.php#l00198">ServerAddinDlg::SaveConfig()</a>.<div class="fragment"><pre class="fragment"><a name="l00120"></a>00120 {
<a name="l00121"></a>00121         <span class="keywordflow">while</span> (<a class="code" href="classssa_1_1_c_config_type.php#3e3c64f0852fde3b5ac061a7679b5254">HasPort</a>())
<a name="l00122"></a>00122                 <a class="code" href="classssa_1_1_c_config_type.php#0ff41a050508e96dcc2611345f8b4d55">RemovePortAt</a>(0);
<a name="l00123"></a>00123 }
</pre></div>
<p>

</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="_schema_server_config___c_config_type_8h-source.php">SchemaServerConfig_CConfigType.h</a><li><a class="el" href="_schema_server_config___c_config_type_8cpp-source.php">SchemaServerConfig_CConfigType.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
