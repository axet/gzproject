<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespace_packets.php">Packets</a>::<a class="el" href="struct_packets_1_1scs_shop_offer.php">scsShopOffer</a>::<a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php">tag1</a></div>
<h1>Packets::scsShopOffer::tag1 Struct Reference</h1><!-- doxytag: class="Packets::scsShopOffer::tag1" --><code>#include &lt;<a class="el" href="packets_8h-source.php">packets.h</a>&gt;</code>
<p>
<a href="struct_packets_1_1scs_shop_offer_1_1tag1-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="struct_packets_1_1scs_pack.php">scsPack</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php#194770fe58fc636de29da39d6d0bbcfe">pack</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php#0f6af3a9b5f22fb5f7abec16a140c31b">Packet_Size</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_int.php">Server::ServerInt</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php#8e8ff40692d0d02afd11f53d1abd0192">Vendor_Serial</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="namespace_server.php#c2b5b0c54059a0371f64b64dae190caf">Server::ServerByte</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php#8cd2859e53cc5f06e67158f941d8cf02">Unknown</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="namespace_server.php#c2b5b0c54059a0371f64b64dae190caf">Server::ServerByte</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php#6981bac85e2b175cca5b4e8ed43842a2">Number_of_Items</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00151">151</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="194770fe58fc636de29da39d6d0bbcfe"></a><!-- doxytag: member="Packets::scsShopOffer::tag1::pack" ref="194770fe58fc636de29da39d6d0bbcfe" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="struct_packets_1_1scs_pack.php">scsPack</a> <a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php#194770fe58fc636de29da39d6d0bbcfe">Packets::scsShopOffer::tag1::pack</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00153">153</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="packets_8cpp-source.php#l00321">Packets::scsShopOffer::scsShopOffer()</a>.
</div>
</div><p>
<a class="anchor" name="0f6af3a9b5f22fb5f7abec16a140c31b"></a><!-- doxytag: member="Packets::scsShopOffer::tag1::Packet_Size" ref="0f6af3a9b5f22fb5f7abec16a140c31b" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php#0f6af3a9b5f22fb5f7abec16a140c31b">Packets::scsShopOffer::tag1::Packet_Size</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00154">154</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="packets_8cpp-source.php#l00321">Packets::scsShopOffer::scsShopOffer()</a>.
</div>
</div><p>
<a class="anchor" name="8e8ff40692d0d02afd11f53d1abd0192"></a><!-- doxytag: member="Packets::scsShopOffer::tag1::Vendor_Serial" ref="8e8ff40692d0d02afd11f53d1abd0192" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_int.php">Server::ServerInt</a> <a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php#8e8ff40692d0d02afd11f53d1abd0192">Packets::scsShopOffer::tag1::Vendor_Serial</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00155">155</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="packets_8cpp-source.php#l00321">Packets::scsShopOffer::scsShopOffer()</a>.
</div>
</div><p>
<a class="anchor" name="8cd2859e53cc5f06e67158f941d8cf02"></a><!-- doxytag: member="Packets::scsShopOffer::tag1::Unknown" ref="8cd2859e53cc5f06e67158f941d8cf02" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="namespace_server.php#c2b5b0c54059a0371f64b64dae190caf">Server::ServerByte</a> <a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php#8cd2859e53cc5f06e67158f941d8cf02">Packets::scsShopOffer::tag1::Unknown</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00156">156</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="packets_8cpp-source.php#l00321">Packets::scsShopOffer::scsShopOffer()</a>.
</div>
</div><p>
<a class="anchor" name="6981bac85e2b175cca5b4e8ed43842a2"></a><!-- doxytag: member="Packets::scsShopOffer::tag1::Number_of_Items" ref="6981bac85e2b175cca5b4e8ed43842a2" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="namespace_server.php#c2b5b0c54059a0371f64b64dae190caf">Server::ServerByte</a> <a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php#6981bac85e2b175cca5b4e8ed43842a2">Packets::scsShopOffer::tag1::Number_of_Items</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00157">157</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="packets_8cpp-source.php#l00321">Packets::scsShopOffer::scsShopOffer()</a>.
</div>
</div><p>
<hr>The documentation for this struct was generated from the following file:<ul>
<li><a class="el" href="packets_8h-source.php">packets.h</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
