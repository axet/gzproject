<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_8ba19508940e77109a2da1f76a26e336.php">ServerAddin</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_5e7f10b6405615a06dd3d049e6d29113.php">firewall</a></div>
<h1>firewall.cpp</h1><a href="firewall_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#include "<a class="code" href="_g_z_2_server_addin_2firewall_2_std_afx_8h.php">stdafx.h</a>"</span>
<a name="l00002"></a>00002 <span class="preprocessor">#include "<a class="code" href="firewall_8h.php">firewall.h</a>"</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="preprocessor">#include &lt;<a class="code" href="debug_8h.php">debugger/debug.h</a>&gt;</span>
<a name="l00005"></a>00005
<a name="l00006"></a>00006 <span class="preprocessor">#pragma comment(lib,"wsock32.lib")</span>
<a name="l00007"></a>00007 <span class="preprocessor"></span>
<a name="l00008"></a>00008 <span class="comment">// CFirewall {</span>
<a name="l00009"></a>00009
<a name="l00010"></a><a class="code" href="class_firewall_1_1_c_firewall.php#1e946497b3e6400755ca8c68e1cb9417">00010</a> <span class="keywordtype">void</span> CFirewall::hook_accept_leave(CMemMngr* mngr,<span class="keywordtype">int</span> *retval)
<a name="l00011"></a>00011 {
<a name="l00012"></a>00012   <a class="code" href="class_firewall_1_1_c_firewall.php">CFirewall</a>* current=dynamic_cast&lt;CFirewall*&gt;(mngr);
<a name="l00013"></a>00013
<a name="l00014"></a>00014   SOCKET s=*(SOCKET*)retval;
<a name="l00015"></a>00015   <span class="comment">//struct sockaddr *addr=va_arg(args,sockaddr*);</span>
<a name="l00016"></a>00016   <span class="comment">//int *addrlen=va_arg(args,int*);</span>
<a name="l00017"></a>00017
<a name="l00018"></a>00018   <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"accpet game connection: %08x\n"</span>,s);
<a name="l00019"></a>00019
<a name="l00020"></a>00020   current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#2124667108305aef7b861e730e2569bb">m_sockets</a>[s]=<span class="keyword">new</span> <a class="code" href="class_firewall_1_1_c_firewall.php#f9554ef6b04a84dab725286511b94266">CClientKernel</a>(current,s);
<a name="l00021"></a>00021 }
<a name="l00022"></a>00022
<a name="l00023"></a><a class="code" href="class_firewall_1_1_c_firewall.php#cc400a3bcd5f54ba37048ab468d72653">00023</a> <span class="keywordtype">void</span> <a class="code" href="class_firewall_1_1_c_firewall.php#cc400a3bcd5f54ba37048ab468d72653">CFirewall::hook_closesocket</a>(CMemMngr* mngr,va_list args)
<a name="l00024"></a>00024 {
<a name="l00025"></a>00025   <a class="code" href="class_firewall_1_1_c_firewall.php">CFirewall</a>* current=dynamic_cast&lt;CFirewall*&gt;(mngr);
<a name="l00026"></a>00026
<a name="l00027"></a>00027   SOCKET s=va_arg(args,SOCKET);
<a name="l00028"></a>00028
<a name="l00029"></a>00029   <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"close game connection: %08x\n"</span>,s);
<a name="l00030"></a>00030
<a name="l00031"></a>00031   socketmap_t::iterator i=current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#2124667108305aef7b861e730e2569bb">m_sockets</a>.find(s);
<a name="l00032"></a>00032   <span class="keywordflow">if</span>(i!=current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#2124667108305aef7b861e730e2569bb">m_sockets</a>.end())
<a name="l00033"></a>00033   {
<a name="l00034"></a>00034     <span class="keyword">delete</span> i-&gt;second;
<a name="l00035"></a>00035     current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#2124667108305aef7b861e730e2569bb">m_sockets</a>.erase(i);
<a name="l00036"></a>00036   }
<a name="l00037"></a>00037 }
<a name="l00038"></a>00038
<a name="l00039"></a><a class="code" href="class_firewall_1_1_c_firewall.php#6ed03c5d84d66cf1da6fd06851349be8">00039</a> <span class="keywordtype">void</span> <a class="code" href="class_firewall_1_1_c_firewall.php#6ed03c5d84d66cf1da6fd06851349be8">CFirewall::hook_recv</a>(CMemMngr* mngr,va_list args)
<a name="l00040"></a>00040 {
<a name="l00041"></a>00041   <a class="code" href="class_firewall_1_1_c_firewall.php">CFirewall</a>* current=dynamic_cast&lt;CFirewall*&gt;(mngr);
<a name="l00042"></a>00042
<a name="l00043"></a>00043   current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#04f5dd1385cf2be2f7545164bb82db47">m_recv_socket</a>=va_arg(args,SOCKET);
<a name="l00044"></a>00044   current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#60eed438fd998689f9a4adb9c8f74509">m_recv_buf</a>=va_arg(args,<span class="keywordtype">unsigned</span> <span class="keywordtype">char</span>*);
<a name="l00045"></a>00045   current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#c3350847c5f2064ca2112a4deed8a8a3">m_recv_bufmaxsize</a>=va_arg(args,<span class="keywordtype">unsigned</span>);
<a name="l00046"></a>00046 }
<a name="l00047"></a>00047
<a name="l00048"></a><a class="code" href="class_firewall_1_1_c_firewall.php#b293cec8eed94ad77b3dddc69fc0069d">00048</a> <span class="keywordtype">void</span> <a class="code" href="class_firewall_1_1_c_firewall.php#b293cec8eed94ad77b3dddc69fc0069d">CFirewall::hook_recv_leave</a>(CMemMngr* mngr,<span class="keywordtype">int</span> *retval)
<a name="l00049"></a>00049 {
<a name="l00050"></a>00050   <a class="code" href="class_firewall_1_1_c_firewall.php">CFirewall</a>* current=dynamic_cast&lt;CFirewall*&gt;(mngr);
<a name="l00051"></a>00051
<a name="l00052"></a>00052   <span class="keywordflow">try</span>
<a name="l00053"></a>00053   {
<a name="l00054"></a>00054     socketmap_t::iterator i=current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#2124667108305aef7b861e730e2569bb">m_sockets</a>.find(current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#04f5dd1385cf2be2f7545164bb82db47">m_recv_socket</a>);
<a name="l00055"></a>00055     <span class="keywordflow">if</span>(i!=current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#2124667108305aef7b861e730e2569bb">m_sockets</a>.end())
<a name="l00056"></a>00056     {
<a name="l00057"></a>00057       current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#4ff8a0c48196be2a11175867c6146985">m_currentuser</a>=i-&gt;second;
<a name="l00058"></a>00058       current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#4ff8a0c48196be2a11175867c6146985">m_currentuser</a>-&gt;<a class="code" href="class_firewall_1_1_c_client.php#7616b44d1805e65cad400b6ff72c4f73">ReceiveData</a>(current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#60eed438fd998689f9a4adb9c8f74509">m_recv_buf</a>,retval,current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#c3350847c5f2064ca2112a4deed8a8a3">m_recv_bufmaxsize</a>);
<a name="l00059"></a>00059       <span class="keywordflow">if</span>(current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#4ff8a0c48196be2a11175867c6146985">m_currentuser</a>-&gt;<a class="code" href="class_firewall_1_1_c_client.php#ce23c5c7e50f706421d68ba7d1031681">BadClient</a>())
<a name="l00060"></a>00060       {
<a name="l00061"></a>00061         <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"bad client: %08x\n"</span>,current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#04f5dd1385cf2be2f7545164bb82db47">m_recv_socket</a>);
<a name="l00062"></a>00062         *retval=-1;
<a name="l00063"></a>00063       }
<a name="l00064"></a>00064     }
<a name="l00065"></a>00065     current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#4ff8a0c48196be2a11175867c6146985">m_currentuser</a>=0;
<a name="l00066"></a>00066   }<span class="keywordflow">catch</span>(std::exception &amp;e)
<a name="l00067"></a>00067   {
<a name="l00068"></a>00068     *retval=-1;
<a name="l00069"></a>00069
<a name="l00070"></a>00070     <span class="keywordflow">if</span>(current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#4ff8a0c48196be2a11175867c6146985">m_currentuser</a>!=0)
<a name="l00071"></a>00071     {
<a name="l00072"></a>00072       <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"exception: login '%s', '%08x'"</span>,
<a name="l00073"></a>00073         current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#4ff8a0c48196be2a11175867c6146985">m_currentuser</a>-&gt;<a class="code" href="class_firewall_1_1_c_client.php#9e0a5fd0e2c0a2cfffa0152f474f65fa">GetLogin</a>()?current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#4ff8a0c48196be2a11175867c6146985">m_currentuser</a>-&gt;<a class="code" href="class_firewall_1_1_c_client.php#9e0a5fd0e2c0a2cfffa0152f474f65fa">GetLogin</a>():<span class="stringliteral">""</span>,
<a name="l00074"></a>00074         current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#4ff8a0c48196be2a11175867c6146985">m_currentuser</a>-&gt;<a class="code" href="class_firewall_1_1_c_client.php#88c0332fa22c3c18dddfde705f8a8fa9">GetIPAddress</a>());
<a name="l00075"></a>00075     }<span class="keywordflow">else</span>
<a name="l00076"></a>00076       <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"exception: null user\n"</span>);
<a name="l00077"></a>00077   }
<a name="l00078"></a>00078 }
<a name="l00079"></a>00079
<a name="l00080"></a><a class="code" href="class_firewall_1_1_c_firewall.php#09b9e696a547299dd0ef54005f154dbe">00080</a> <span class="keywordtype">void</span> <a class="code" href="class_firewall_1_1_c_firewall.php#09b9e696a547299dd0ef54005f154dbe">CFirewall::hook_send</a>(CMemMngr* mngr,va_list args)
<a name="l00081"></a>00081 {
<a name="l00082"></a>00082   <a class="code" href="class_firewall_1_1_c_firewall.php">CFirewall</a>* current=dynamic_cast&lt;CFirewall*&gt;(mngr);
<a name="l00083"></a>00083
<a name="l00084"></a>00084   SOCKET s=va_arg(args,SOCKET);
<a name="l00085"></a>00085   <span class="keywordtype">unsigned</span> <span class="keywordtype">char</span> *buf=va_arg(args,<span class="keywordtype">unsigned</span> <span class="keywordtype">char</span>*);
<a name="l00086"></a>00086   <span class="keywordtype">int</span> bufsize=va_arg(args,<span class="keywordtype">int</span>);
<a name="l00087"></a>00087   <span class="keywordtype">int</span> flags=va_arg(args,<span class="keywordtype">int</span>);
<a name="l00088"></a>00088
<a name="l00089"></a>00089   socketmap_t::iterator i=current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#2124667108305aef7b861e730e2569bb">m_sockets</a>.find(current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#04f5dd1385cf2be2f7545164bb82db47">m_recv_socket</a>);
<a name="l00090"></a>00090   <a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php">CClientKernel</a>* currentuser=0;
<a name="l00091"></a>00091   <span class="keywordflow">if</span>(i!=current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#2124667108305aef7b861e730e2569bb">m_sockets</a>.end())
<a name="l00092"></a>00092     currentuser=i-&gt;second;
<a name="l00093"></a>00093
<a name="l00094"></a>00094   <span class="keywordflow">if</span>(currentuser&amp;&amp;currentuser-&gt;<a class="code" href="class_firewall_1_1_c_client.php#f22582446658da2415c5c9f1c00846bb">IsLoggedIn</a>())
<a name="l00095"></a>00095   {
<a name="l00096"></a>00096     <span class="keywordtype">unsigned</span> <span class="keywordtype">char</span> bufout[16384];
<a name="l00097"></a>00097     <span class="keywordtype">int</span> bufoutsize=<span class="keyword">sizeof</span>(bufout);
<a name="l00098"></a>00098     <a class="code" href="class_c_server_crypt.php">CServerCrypt</a> servercrypt;
<a name="l00099"></a>00099     servercrypt.<a class="code" href="class_c_server_crypt.php#0fd201e8eca90575b903bc8069a3c522">Decrypt</a>(buf,bufsize,bufout,bufoutsize);
<a name="l00100"></a>00100     current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#86e063863c8e4b5ddaa1aa5a8e86bda1">m_send_ret</a>=currentuser-&gt;<a class="code" href="class_firewall_1_1_c_client.php#03a67964e457698a6dead4517a2dfb61">Send</a>(<a class="code" href="class_packets_1_1_packet.php">Packets::Packet</a>(bufout,bufoutsize));
<a name="l00101"></a>00101   }<span class="keywordflow">else</span>
<a name="l00102"></a>00102   {
<a name="l00103"></a>00103     current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#86e063863c8e4b5ddaa1aa5a8e86bda1">m_send_ret</a>=send(s,(<span class="keyword">const</span> <span class="keywordtype">char</span>*)buf,bufsize,flags);
<a name="l00104"></a>00104   }
<a name="l00105"></a>00105 }
<a name="l00106"></a>00106
<a name="l00107"></a><a class="code" href="class_firewall_1_1_c_firewall.php#6a1e681e761f1f44e66c59f0c9194741">00107</a> <span class="keywordtype">void</span> <a class="code" href="class_firewall_1_1_c_firewall.php#6a1e681e761f1f44e66c59f0c9194741">CFirewall::hook_send_leave</a>(CMemMngr* mngr,<span class="keywordtype">int</span> *retval)
<a name="l00108"></a>00108 {
<a name="l00109"></a>00109   <a class="code" href="class_firewall_1_1_c_firewall.php">CFirewall</a>* current=dynamic_cast&lt;CFirewall*&gt;(mngr);
<a name="l00110"></a>00110
<a name="l00111"></a>00111   *retval=current-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#86e063863c8e4b5ddaa1aa5a8e86bda1">m_send_ret</a>;
<a name="l00112"></a>00112 }
<a name="l00113"></a>00113
<a name="l00114"></a><a class="code" href="class_firewall_1_1_c_firewall.php#1007733324dc36b9fa9d75c3e43e2972">00114</a> <span class="keywordtype">int</span> <a class="code" href="class_firewall_1_1_c_firewall.php#1007733324dc36b9fa9d75c3e43e2972">CFirewall::hook_send_stub</a>(SOCKET s,<span class="keywordtype">unsigned</span> <span class="keywordtype">char</span>*,<span class="keywordtype">int</span>,<span class="keywordtype">int</span>)
<a name="l00115"></a>00115 {
<a name="l00116"></a>00116   <span class="keywordflow">return</span> 0;
<a name="l00117"></a>00117 }
<a name="l00118"></a>00118
<a name="l00119"></a><a class="code" href="class_firewall_1_1_c_firewall.php#5962d8e995ee092d9b0d78b3dd93cb5e">00119</a> <a class="code" href="class_firewall_1_1_c_firewall.php#5962d8e995ee092d9b0d78b3dd93cb5e">CFirewall::CFirewall</a>()
<a name="l00120"></a>00120 {
<a name="l00121"></a>00121   <span class="keywordtype">char</span> dll[][64]=
<a name="l00122"></a>00122   {
<a name="l00123"></a>00123     <span class="stringliteral">"wsock32.dll"</span>,
<a name="l00124"></a>00124     <span class="stringliteral">"ws2_32.dll"</span>
<a name="l00125"></a>00125   };
<a name="l00126"></a>00126   <span class="keywordflow">for</span>(<span class="keywordtype">int</span> i=0;i&lt;<span class="keyword">sizeof</span>(dll)/64;i++)
<a name="l00127"></a>00127   {
<a name="l00128"></a>00128     <a class="code" href="class_i_o_fluke_1_1_c_mem_mngr.php#3d6775abe75749e579c4ad3dd0922566">CatchImportFunction</a>(<span class="stringliteral">"accept"</span>,dll[i],0,<a class="code" href="class_firewall_1_1_c_firewall.php#1e946497b3e6400755ca8c68e1cb9417">hook_accept_leave</a>);
<a name="l00129"></a>00129     <a class="code" href="class_i_o_fluke_1_1_c_mem_mngr.php#3d6775abe75749e579c4ad3dd0922566">CatchImportFunction</a>(<span class="stringliteral">"recv"</span>,dll[i],<a class="code" href="class_firewall_1_1_c_firewall.php#6ed03c5d84d66cf1da6fd06851349be8">hook_recv</a>,<a class="code" href="class_firewall_1_1_c_firewall.php#b293cec8eed94ad77b3dddc69fc0069d">hook_recv_leave</a>);
<a name="l00130"></a>00130     <span class="comment">//CatchImportFunction("send",dll[i],hook_send,hook_send_leave);</span>
<a name="l00131"></a>00131     <a class="code" href="class_i_o_fluke_1_1_c_mem_mngr.php#e7848d8696ded55bded3caeb2b5f8afe">RedirectImportFunction</a>(<span class="stringliteral">"send"</span>,dll[i],<a class="code" href="class_firewall_1_1_c_firewall.php#09b9e696a547299dd0ef54005f154dbe">hook_send</a>,<a class="code" href="class_firewall_1_1_c_firewall.php#6a1e681e761f1f44e66c59f0c9194741">hook_send_leave</a>,(<a class="code" href="class_i_o_fluke_1_1_c_mem_mngr.php#abaec71f3876157ea0deaed1a6b4a918">Proc</a>)<a class="code" href="class_firewall_1_1_c_firewall.php#1007733324dc36b9fa9d75c3e43e2972">hook_send_stub</a>);
<a name="l00132"></a>00132     <a class="code" href="class_i_o_fluke_1_1_c_mem_mngr.php#3d6775abe75749e579c4ad3dd0922566">CatchImportFunction</a>(<span class="stringliteral">"closesocket"</span>,dll[i],<a class="code" href="class_firewall_1_1_c_firewall.php#cc400a3bcd5f54ba37048ab468d72653">hook_closesocket</a>);
<a name="l00133"></a>00133   }
<a name="l00134"></a>00134 }
<a name="l00135"></a>00135
<a name="l00136"></a><a class="code" href="class_firewall_1_1_c_firewall.php#cd7ea01859dc128350909a30a0d34ccc">00136</a> <span class="keywordtype">void</span> <a class="code" href="class_firewall_1_1_c_firewall.php#cd7ea01859dc128350909a30a0d34ccc">CFirewall::AddModule</a>(<a class="code" href="class_firewall_1_1_c_firewall_module.php">CFirewallModule</a> *m)
<a name="l00137"></a>00137 {
<a name="l00138"></a>00138   m-&gt;<a class="code" href="class_firewall_1_1_c_firewall_module.php#3a56f08f7f555049cf1510fc3baefd59">m_fm</a>=<span class="keyword">this</span>;
<a name="l00139"></a>00139   <a class="code" href="class_firewall_1_1_c_firewall.php#093c71a976a3ad2a0f074468411a37fb">m_modules</a>.push_back(m);
<a name="l00140"></a>00140 }
<a name="l00141"></a>00141
<a name="l00142"></a>00142 <span class="comment">// } CFirewall</span>
<a name="l00143"></a>00143
<a name="l00144"></a>00144 <span class="comment">// { CCClientKernel</span>
<a name="l00145"></a>00145
<a name="l00146"></a><a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#afc2e365f4980451c81607faa3e45c93">00146</a> <a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#afc2e365f4980451c81607faa3e45c93">CFirewall::CClientKernel::CClientKernel</a>(<a class="code" href="class_firewall_1_1_c_firewall.php">CFirewall</a>*pp,SOCKET s):<a class="code" href="class_firewall_1_1_c_client.php">CClient</a>(s)
<a name="l00147"></a>00147 {
<a name="l00148"></a>00148   <span class="keywordflow">for</span>(CFirewall::modules_t::iterator i=pp-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#093c71a976a3ad2a0f074468411a37fb">m_modules</a>.begin();i!=pp-&gt;<a class="code" href="class_firewall_1_1_c_firewall.php#093c71a976a3ad2a0f074468411a37fb">m_modules</a>.end();i++)
<a name="l00149"></a>00149     <a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#8249c6f5fa668f50674aa505e31bcc68">AddController</a>(*i);
<a name="l00150"></a>00150 }
<a name="l00151"></a>00151
<a name="l00152"></a><a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#8249c6f5fa668f50674aa505e31bcc68">00152</a> <span class="keywordtype">void</span> <a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#8249c6f5fa668f50674aa505e31bcc68">CFirewall::CClientKernel::AddController</a>(<a class="code" href="class_c_client_net_events.php">CClientNetEvents</a>*p)
<a name="l00153"></a>00153 {
<a name="l00154"></a>00154   <a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#96a11d68a9173fe5548ad3a3b4faceb2">m_netevents</a>.push(<a class="code" href="class_c_client_interface.php#445ef60309443464cb4da7e710f9fa1d">AddAdvise</a>(p));
<a name="l00155"></a>00155 }
<a name="l00156"></a>00156
<a name="l00157"></a><a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#910f35539dfd93b2a1c3a2701b048689">00157</a> <a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#910f35539dfd93b2a1c3a2701b048689">CFirewall::CClientKernel::~CClientKernel</a>()
<a name="l00158"></a>00158 {
<a name="l00159"></a>00159   <span class="keywordflow">while</span>(!<a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#96a11d68a9173fe5548ad3a3b4faceb2">m_netevents</a>.empty())
<a name="l00160"></a>00160   {
<a name="l00161"></a>00161     <a class="code" href="class_c_client_interface.php#c8d8f8ef14a2a792ebe23d2ddbc856f9">RemoveAdvise</a>(<a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#96a11d68a9173fe5548ad3a3b4faceb2">m_netevents</a>.top());
<a name="l00162"></a>00162     <a class="code" href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php#96a11d68a9173fe5548ad3a3b4faceb2">m_netevents</a>.pop();
<a name="l00163"></a>00163   }
<a name="l00164"></a>00164 }
<a name="l00165"></a>00165
<a name="l00166"></a>00166 <span class="comment">// } CClientKernel</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
