<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>CGZClientLoaderDlg Class Reference</h1><!-- doxytag: class="CGZClientLoaderDlg" --><code>#include &lt;<a class="el" href="_g_z_client_loader_dlg_8h-source.php">GZClientLoaderDlg.h</a>&gt;</code>
<p>
<a href="class_c_g_z_client_loader_dlg-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Types</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">enum &nbsp;</td><td class="memItemRight" valign="bottom">{ <a class="el" href="class_c_g_z_client_loader_dlg.php#97edea966f5950c5515d6427a38b82c4777f83bc04ab34dd0ff4dc0cc9979caa">IDD</a> =  IDD_GZCLIENTLOADER_DIALOG
 }</td></tr>

<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_client_loader_dlg.php#de6828d6657e909bf4147a5330ebd799">CGZClientLoaderDlg</a> (CWnd *pParent=NULL)</td></tr>

<tr><td colspan="2"><br><h2>Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">CRichEditCtrl&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_client_loader_dlg.php#38fc78000585b6d6d75e72842a413780">m_log</a></td></tr>

<tr><td colspan="2"><br><h2>Protected Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_client_loader_dlg.php#36490aa66bccec23474d7224facdeff6">DoDataExchange</a> (CDataExchange *pDX)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual BOOL&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_client_loader_dlg.php#55870124b51d0d6c557ff4c2bd580e89">OnInitDialog</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">afx_msg void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_client_loader_dlg.php#c95d8d1816785f86d9d65fa2ef37135b">OnSysCommand</a> (UINT nID, LPARAM <a class="el" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a>)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">afx_msg void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_client_loader_dlg.php#4ecbad75c6bca5d45b02414720630a7a">OnPaint</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">afx_msg HCURSOR&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_client_loader_dlg.php#395655bcb1b51d2636db732a285df9f3">OnQueryDragIcon</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_client_loader_dlg.php#b0ab2de6b55998e41501b12f9cbf4ac2">OnOK</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_client_loader_dlg.php#c78a4942b70f35e5bec758174a2f13b4">OnCancel</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">afx_msg void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_client_loader_dlg.php#02f2fc75af029d56e975b84af4b4d4ed">OnClose</a> ()</td></tr>

<tr><td colspan="2"><br><h2>Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">HICON&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_client_loader_dlg.php#0673b082a30dadd015ec9c42bb36f41a">m_hIcon</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8h-source.php#l00014">14</a> of file <a class="el" href="_g_z_client_loader_dlg_8h-source.php">GZClientLoaderDlg.h</a>.<hr><h2>Member Enumeration Documentation</h2>
<a class="anchor" name="97edea966f5950c5515d6427a38b82c4"></a><!-- doxytag: member="CGZClientLoaderDlg::@19" ref="97edea966f5950c5515d6427a38b82c4" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">anonymous enum          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
<dl compact><dt><b>Enumerator: </b></dt><dd>
<table border="0" cellspacing="2" cellpadding="0">
<tr><td valign="top"><em><a class="anchor" name="97edea966f5950c5515d6427a38b82c4777f83bc04ab34dd0ff4dc0cc9979caa"></a><!-- doxytag: member="IDD" ref="97edea966f5950c5515d6427a38b82c4777f83bc04ab34dd0ff4dc0cc9979caa" args="" -->IDD</em>&nbsp;</td><td>
</td></tr>
</table>
</dl>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8h-source.php#l00022">22</a> of file <a class="el" href="_g_z_client_loader_dlg_8h-source.php">GZClientLoaderDlg.h</a>.<div class="fragment"><pre class="fragment"><a name="l00022"></a>00022 { <a class="code" href="class_c_g_z_client_loader_dlg.php#97edea966f5950c5515d6427a38b82c4777f83bc04ab34dd0ff4dc0cc9979caa">IDD</a> = <a class="code" href="_g_z_2_g_z_client_loader_2resource_8h.php#388461f97914e9e65adb1eb6ca7121f2">IDD_GZCLIENTLOADER_DIALOG</a> };
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="de6828d6657e909bf4147a5330ebd799"></a><!-- doxytag: member="CGZClientLoaderDlg::CGZClientLoaderDlg" ref="de6828d6657e909bf4147a5330ebd799" args="(CWnd *pParent=NULL)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">CGZClientLoaderDlg::CGZClientLoaderDlg           </td>
          <td>(</td>
          <td class="paramtype">CWnd *&nbsp;</td>
          <td class="paramname"> <em>pParent</em> = <code>NULL</code>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00062">62</a> of file <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php">GZClientLoaderDlg.cpp</a>.
<p>
References <a class="el" href="_g_z_2client_2test_server_crypt_2resource_8h-source.php#l00008">IDR_MAINFRAME</a>.<div class="fragment"><pre class="fragment"><a name="l00063"></a>00063         : CDialog(<a class="code" href="class_c_g_z_client_loader_dlg.php#97edea966f5950c5515d6427a38b82c4777f83bc04ab34dd0ff4dc0cc9979caa">CGZClientLoaderDlg::IDD</a>, pParent)
<a name="l00064"></a>00064 {
<a name="l00065"></a>00065         <span class="comment">//{{AFX_DATA_INIT(CGZClientLoaderDlg)</span>
<a name="l00066"></a>00066                 <span class="comment">// NOTE: the ClassWizard will add member initialization here</span>
<a name="l00067"></a>00067         <span class="comment">//}}AFX_DATA_INIT</span>
<a name="l00068"></a>00068         <span class="comment">// Note that LoadIcon does not require a subsequent DestroyIcon in Win32</span>
<a name="l00069"></a>00069         <a class="code" href="class_c_g_z_client_loader_dlg.php#0673b082a30dadd015ec9c42bb36f41a">m_hIcon</a> = AfxGetApp()-&gt;LoadIcon(<a class="code" href="_g_z_2client_2test_server_crypt_2resource_8h.php#9772c84d39896ad00b9aeb34b15d324d">IDR_MAINFRAME</a>);
<a name="l00070"></a>00070 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="36490aa66bccec23474d7224facdeff6"></a><!-- doxytag: member="CGZClientLoaderDlg::DoDataExchange" ref="36490aa66bccec23474d7224facdeff6" args="(CDataExchange *pDX)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CGZClientLoaderDlg::DoDataExchange           </td>
          <td>(</td>
          <td class="paramtype">CDataExchange *&nbsp;</td>
          <td class="paramname"> <em>pDX</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00072">72</a> of file <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php">GZClientLoaderDlg.cpp</a>.
<p>
References <a class="el" href="_g_z_2_g_z_client_loader_2resource_8h-source.php#l00010">IDC_RICHEDIT1</a>, and <a class="el" href="_g_z_client_loader_dlg_8h-source.php#l00023">m_log</a>.<div class="fragment"><pre class="fragment"><a name="l00073"></a>00073 {
<a name="l00074"></a>00074         CDialog::DoDataExchange(pDX);
<a name="l00075"></a>00075         <span class="comment">//{{AFX_DATA_MAP(CGZClientLoaderDlg)</span>
<a name="l00076"></a>00076         DDX_Control(pDX, <a class="code" href="_g_z_2_g_z_client_loader_2resource_8h.php#6161f96827dba07cc98ef87015d91ed4">IDC_RICHEDIT1</a>, <a class="code" href="class_c_g_z_client_loader_dlg.php#38fc78000585b6d6d75e72842a413780">m_log</a>);
<a name="l00077"></a>00077         <span class="comment">//}}AFX_DATA_MAP</span>
<a name="l00078"></a>00078 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="55870124b51d0d6c557ff4c2bd580e89"></a><!-- doxytag: member="CGZClientLoaderDlg::OnInitDialog" ref="55870124b51d0d6c557ff4c2bd580e89" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">BOOL CGZClientLoaderDlg::OnInitDialog           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00092">92</a> of file <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php">GZClientLoaderDlg.cpp</a>.
<p>
References <a class="el" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h-source.php#l00006">IDM_ABOUTBOX</a>, and <a class="el" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h-source.php#l00008">IDS_ABOUTBOX</a>.<div class="fragment"><pre class="fragment"><a name="l00093"></a>00093 {
<a name="l00094"></a>00094         CDialog::OnInitDialog();
<a name="l00095"></a>00095
<a name="l00096"></a>00096         <span class="comment">// Add "About..." menu item to system menu.</span>
<a name="l00097"></a>00097
<a name="l00098"></a>00098         <span class="comment">// IDM_ABOUTBOX must be in the system command range.</span>
<a name="l00099"></a>00099         ASSERT((<a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#74b41adc6435c09e5393739d1ebeace4">IDM_ABOUTBOX</a> &amp; 0xFFF0) == <a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#74b41adc6435c09e5393739d1ebeace4">IDM_ABOUTBOX</a>);
<a name="l00100"></a>00100         ASSERT(<a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#74b41adc6435c09e5393739d1ebeace4">IDM_ABOUTBOX</a> &lt; 0xF000);
<a name="l00101"></a>00101
<a name="l00102"></a>00102         CMenu* pSysMenu = GetSystemMenu(FALSE);
<a name="l00103"></a>00103         <span class="keywordflow">if</span> (pSysMenu != NULL)
<a name="l00104"></a>00104         {
<a name="l00105"></a>00105                 CString strAboutMenu;
<a name="l00106"></a>00106                 strAboutMenu.LoadString(<a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#912ee15244eed3e0f75c2fc1ef9719eb">IDS_ABOUTBOX</a>);
<a name="l00107"></a>00107                 <span class="keywordflow">if</span> (!strAboutMenu.IsEmpty())
<a name="l00108"></a>00108                 {
<a name="l00109"></a>00109                         pSysMenu-&gt;AppendMenu(MF_SEPARATOR);
<a name="l00110"></a>00110                         pSysMenu-&gt;AppendMenu(MF_STRING, <a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#74b41adc6435c09e5393739d1ebeace4">IDM_ABOUTBOX</a>, strAboutMenu);
<a name="l00111"></a>00111                 }
<a name="l00112"></a>00112         }
<a name="l00113"></a>00113
<a name="l00114"></a>00114         <span class="comment">// Set the icon for this dialog.  The framework does this automatically</span>
<a name="l00115"></a>00115         <span class="comment">//  when the application's main window is not a dialog</span>
<a name="l00116"></a>00116         SetIcon(<a class="code" href="class_c_g_z_client_loader_dlg.php#0673b082a30dadd015ec9c42bb36f41a">m_hIcon</a>, TRUE);                 <span class="comment">// Set big icon</span>
<a name="l00117"></a>00117         SetIcon(<a class="code" href="class_c_g_z_client_loader_dlg.php#0673b082a30dadd015ec9c42bb36f41a">m_hIcon</a>, FALSE);                <span class="comment">// Set small icon</span>
<a name="l00118"></a>00118
<a name="l00119"></a>00119         <span class="comment">// TODO: Add extra initialization here</span>
<a name="l00120"></a>00120
<a name="l00121"></a>00121         <span class="keywordflow">return</span> TRUE;  <span class="comment">// return TRUE  unless you set the focus to a control</span>
<a name="l00122"></a>00122 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="c95d8d1816785f86d9d65fa2ef37135b"></a><!-- doxytag: member="CGZClientLoaderDlg::OnSysCommand" ref="c95d8d1816785f86d9d65fa2ef37135b" args="(UINT nID, LPARAM lParam)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CGZClientLoaderDlg::OnSysCommand           </td>
          <td>(</td>
          <td class="paramtype">UINT&nbsp;</td>
          <td class="paramname"> <em>nID</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">LPARAM&nbsp;</td>
          <td class="paramname"> <em>lParam</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00124">124</a> of file <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php">GZClientLoaderDlg.cpp</a>.
<p>
References <a class="el" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h-source.php#l00006">IDM_ABOUTBOX</a>.<div class="fragment"><pre class="fragment"><a name="l00125"></a>00125 {
<a name="l00126"></a>00126         <span class="keywordflow">if</span> ((nID &amp; 0xFFF0) == <a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#74b41adc6435c09e5393739d1ebeace4">IDM_ABOUTBOX</a>)
<a name="l00127"></a>00127         {
<a name="l00128"></a>00128                 <a class="code" href="class_c_about_dlg.php">CAboutDlg</a> dlgAbout;
<a name="l00129"></a>00129                 dlgAbout.DoModal();
<a name="l00130"></a>00130         }
<a name="l00131"></a>00131         <span class="keywordflow">else</span>
<a name="l00132"></a>00132         {
<a name="l00133"></a>00133                 CDialog::OnSysCommand(nID, <a class="code" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a>);
<a name="l00134"></a>00134         }
<a name="l00135"></a>00135 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="4ecbad75c6bca5d45b02414720630a7a"></a><!-- doxytag: member="CGZClientLoaderDlg::OnPaint" ref="4ecbad75c6bca5d45b02414720630a7a" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CGZClientLoaderDlg::OnPaint           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00141">141</a> of file <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php">GZClientLoaderDlg.cpp</a>.
<p>
References <a class="el" href="_g_z_client_loader_dlg_8h-source.php#l00034">m_hIcon</a>.<div class="fragment"><pre class="fragment"><a name="l00142"></a>00142 {
<a name="l00143"></a>00143         <span class="keywordflow">if</span> (IsIconic())
<a name="l00144"></a>00144         {
<a name="l00145"></a>00145                 CPaintDC dc(<span class="keyword">this</span>); <span class="comment">// device context for painting</span>
<a name="l00146"></a>00146
<a name="l00147"></a>00147                 SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);
<a name="l00148"></a>00148
<a name="l00149"></a>00149                 <span class="comment">// Center icon in client rectangle</span>
<a name="l00150"></a>00150                 <span class="keywordtype">int</span> cxIcon = GetSystemMetrics(SM_CXICON);
<a name="l00151"></a>00151                 <span class="keywordtype">int</span> cyIcon = GetSystemMetrics(SM_CYICON);
<a name="l00152"></a>00152                 CRect rect;
<a name="l00153"></a>00153                 GetClientRect(&amp;rect);
<a name="l00154"></a>00154                 <span class="keywordtype">int</span> x = (rect.Width() - cxIcon + 1) / 2;
<a name="l00155"></a>00155                 <span class="keywordtype">int</span> <a class="code" href="pathfinding_8cpp.php#ab9e199ab34637bb8176d43909336d34">y</a> = (rect.Height() - cyIcon + 1) / 2;
<a name="l00156"></a>00156
<a name="l00157"></a>00157                 <span class="comment">// Draw the icon</span>
<a name="l00158"></a>00158                 dc.DrawIcon(x, y, <a class="code" href="class_c_g_z_client_loader_dlg.php#0673b082a30dadd015ec9c42bb36f41a">m_hIcon</a>);
<a name="l00159"></a>00159         }
<a name="l00160"></a>00160         <span class="keywordflow">else</span>
<a name="l00161"></a>00161         {
<a name="l00162"></a>00162                 CDialog::OnPaint();
<a name="l00163"></a>00163         }
<a name="l00164"></a>00164 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="395655bcb1b51d2636db732a285df9f3"></a><!-- doxytag: member="CGZClientLoaderDlg::OnQueryDragIcon" ref="395655bcb1b51d2636db732a285df9f3" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">HCURSOR CGZClientLoaderDlg::OnQueryDragIcon           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00168">168</a> of file <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php">GZClientLoaderDlg.cpp</a>.
<p>
References <a class="el" href="_g_z_client_loader_dlg_8h-source.php#l00034">m_hIcon</a>.<div class="fragment"><pre class="fragment"><a name="l00169"></a>00169 {
<a name="l00170"></a>00170         <span class="keywordflow">return</span> (HCURSOR) <a class="code" href="class_c_g_z_client_loader_dlg.php#0673b082a30dadd015ec9c42bb36f41a">m_hIcon</a>;
<a name="l00171"></a>00171 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="b0ab2de6b55998e41501b12f9cbf4ac2"></a><!-- doxytag: member="CGZClientLoaderDlg::OnOK" ref="b0ab2de6b55998e41501b12f9cbf4ac2" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CGZClientLoaderDlg::OnOK           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00173">173</a> of file <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php">GZClientLoaderDlg.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00174"></a>00174 {
<a name="l00175"></a>00175         <span class="comment">// TODO: Add extra validation here</span>
<a name="l00176"></a>00176
<a name="l00177"></a>00177         <span class="comment">//CDialog::OnOK();</span>
<a name="l00178"></a>00178 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="c78a4942b70f35e5bec758174a2f13b4"></a><!-- doxytag: member="CGZClientLoaderDlg::OnCancel" ref="c78a4942b70f35e5bec758174a2f13b4" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CGZClientLoaderDlg::OnCancel           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00180">180</a> of file <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php">GZClientLoaderDlg.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00181"></a>00181 {
<a name="l00182"></a>00182         <span class="comment">// TODO: Add extra cleanup here</span>
<a name="l00183"></a>00183
<a name="l00184"></a>00184         <span class="comment">//CDialog::OnCancel();</span>
<a name="l00185"></a>00185 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="02f2fc75af029d56e975b84af4b4d4ed"></a><!-- doxytag: member="CGZClientLoaderDlg::OnClose" ref="02f2fc75af029d56e975b84af4b4d4ed" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CGZClientLoaderDlg::OnClose           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00187">187</a> of file <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php">GZClientLoaderDlg.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00188"></a>00188 {
<a name="l00189"></a>00189   CDialog::OnCancel();
<a name="l00190"></a>00190   DestroyWindow();
<a name="l00191"></a>00191 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="38fc78000585b6d6d75e72842a413780"></a><!-- doxytag: member="CGZClientLoaderDlg::m_log" ref="38fc78000585b6d6d75e72842a413780" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">CRichEditCtrl <a class="el" href="class_c_g_z_client_loader_dlg.php#38fc78000585b6d6d75e72842a413780">CGZClientLoaderDlg::m_log</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8h-source.php#l00023">23</a> of file <a class="el" href="_g_z_client_loader_dlg_8h-source.php">GZClientLoaderDlg.h</a>.
<p>
Referenced by <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00072">DoDataExchange()</a>, and <a class="el" href="_g_z_client_loader_8cpp-source.php#l00159">CGZClientLoaderApp::WriteLog()</a>.
</div>
</div><p>
<a class="anchor" name="0673b082a30dadd015ec9c42bb36f41a"></a><!-- doxytag: member="CGZClientLoaderDlg::m_hIcon" ref="0673b082a30dadd015ec9c42bb36f41a" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">HICON <a class="el" href="class_c_g_z_client_loader_dlg.php#0673b082a30dadd015ec9c42bb36f41a">CGZClientLoaderDlg::m_hIcon</a><code> [protected]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_client_loader_dlg_8h-source.php#l00034">34</a> of file <a class="el" href="_g_z_client_loader_dlg_8h-source.php">GZClientLoaderDlg.h</a>.
<p>
Referenced by <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00141">OnPaint()</a>, and <a class="el" href="_g_z_client_loader_dlg_8cpp-source.php#l00168">OnQueryDragIcon()</a>.
</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="_g_z_client_loader_dlg_8h-source.php">GZClientLoaderDlg.h</a><li><a class="el" href="_g_z_client_loader_dlg_8cpp-source.php">GZClientLoaderDlg.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
