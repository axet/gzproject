<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_285c2ba7298296be1db6222076863471.php">GZAdministrator</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_5e699018e533b5010af65c826095caff.php">Altova</a></div>
<h1>Document.cs</h1><a href="_document_8cs.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="keyword">using</span> System;
<a name="l00002"></a>00002 <span class="keyword">using</span> System.Xml;
<a name="l00003"></a>00003
<a name="l00004"></a><a class="code" href="namespace_altova.php">00004</a> <span class="keyword">namespace </span>Altova
<a name="l00005"></a>00005 {
<a name="l00006"></a><a class="code" href="class_altova_1_1_document.php">00006</a>         <span class="keyword">public</span> <span class="keyword">abstract</span> <span class="keyword">class </span><a class="code" href="class_altova_1_1_document.php">Document</a>
<a name="l00007"></a>00007         {
<a name="l00008"></a><a class="code" href="class_altova_1_1_document.php#e3daf79d942331c7a10db2b6d5727a42">00008</a>                 <span class="keyword">protected</span> <span class="keyword">static</span> XmlDocument                    <a class="code" href="class_altova_1_1_document.php#e3daf79d942331c7a10db2b6d5727a42">tmpDocument</a>             = null;
<a name="l00009"></a><a class="code" href="class_altova_1_1_document.php#6e6444d62c415f61b116b56109f32d79">00009</a>                 <span class="keyword">protected</span> <span class="keyword">static</span> XmlDocumentFragment    <a class="code" href="class_altova_1_1_document.php#6e6444d62c415f61b116b56109f32d79">tmpFragment</a>             = null;
<a name="l00010"></a><a class="code" href="class_altova_1_1_document.php#c016bdb01b044fefd51cb3262ca6ad87">00010</a>                 <span class="keyword">protected</span> <span class="keyword">static</span> <span class="keywordtype">int</span>                                    <a class="code" href="class_altova_1_1_document.php#c016bdb01b044fefd51cb3262ca6ad87">tmpNameCounter</a>  = 0;
<a name="l00011"></a>00011
<a name="l00012"></a><a class="code" href="class_altova_1_1_document.php#ade9bff089a2327b3303a650a0ce3eb2">00012</a>                 <span class="keyword">protected</span> <span class="keyword">static</span> XmlDocument <a class="code" href="class_altova_1_1_document.php#ade9bff089a2327b3303a650a0ce3eb2">GetTemporaryDocument</a>()
<a name="l00013"></a>00013                 {
<a name="l00014"></a>00014                         <span class="keywordflow">if</span> (<a class="code" href="class_altova_1_1_document.php#e3daf79d942331c7a10db2b6d5727a42">tmpDocument</a> == null)
<a name="l00015"></a>00015                                 <a class="code" href="class_altova_1_1_document.php#e3daf79d942331c7a10db2b6d5727a42">tmpDocument</a> = <span class="keyword">new</span> XmlDocument();
<a name="l00016"></a>00016                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_document.php#e3daf79d942331c7a10db2b6d5727a42">tmpDocument</a>;
<a name="l00017"></a>00017                 }
<a name="l00018"></a>00018
<a name="l00019"></a><a class="code" href="class_altova_1_1_document.php#ab6f64b0d7e735b2c95b74b4bd9ae9ce">00019</a>                 <span class="keyword">public</span> <span class="keyword">static</span> XmlNode <a class="code" href="class_altova_1_1_document.php#ab6f64b0d7e735b2c95b74b4bd9ae9ce">CreateTemporaryDomNode</a>()
<a name="l00020"></a>00020                 {
<a name="l00021"></a>00021                         string tmpName = <span class="stringliteral">"_"</span> + <a class="code" href="class_altova_1_1_document.php#c016bdb01b044fefd51cb3262ca6ad87">tmpNameCounter</a>++;
<a name="l00022"></a>00022                         <span class="keywordflow">if</span> (<a class="code" href="class_altova_1_1_document.php#6e6444d62c415f61b116b56109f32d79">tmpFragment</a> == null)
<a name="l00023"></a>00023                         {
<a name="l00024"></a>00024                                 <a class="code" href="class_altova_1_1_document.php#6e6444d62c415f61b116b56109f32d79">tmpFragment</a> = <a class="code" href="class_altova_1_1_document.php#ade9bff089a2327b3303a650a0ce3eb2">GetTemporaryDocument</a>().CreateDocumentFragment();
<a name="l00025"></a>00025                                 <a class="code" href="class_altova_1_1_document.php#e3daf79d942331c7a10db2b6d5727a42">tmpDocument</a>.AppendChild(<a class="code" href="class_altova_1_1_document.php#6e6444d62c415f61b116b56109f32d79">tmpFragment</a>);
<a name="l00026"></a>00026                         }
<a name="l00027"></a>00027
<a name="l00028"></a>00028                         XmlNode node = <a class="code" href="class_altova_1_1_document.php#ade9bff089a2327b3303a650a0ce3eb2">GetTemporaryDocument</a>().CreateElement(tmpName);
<a name="l00029"></a>00029                         <a class="code" href="class_altova_1_1_document.php#6e6444d62c415f61b116b56109f32d79">tmpFragment</a>.AppendChild(node);
<a name="l00030"></a>00030                         <span class="keywordflow">return</span> node;
<a name="l00031"></a>00031                 }
<a name="l00032"></a>00032
<a name="l00033"></a><a class="code" href="class_altova_1_1_document.php#01704ddb7bb192764e117aac4c96d14f">00033</a>                 <span class="keyword">protected</span> string <a class="code" href="class_altova_1_1_document.php#01704ddb7bb192764e117aac4c96d14f">rootElementName</a>        = null;
<a name="l00034"></a><a class="code" href="class_altova_1_1_document.php#514d77c61ed77b981179f7f6d9a5ef30">00034</a>                 <span class="keyword">protected</span> string <a class="code" href="class_altova_1_1_document.php#514d77c61ed77b981179f7f6d9a5ef30">namespaceURI</a>           = null;
<a name="l00035"></a><a class="code" href="class_altova_1_1_document.php#ae9847a71b91c4035913dee0ad58ee15">00035</a>                 <span class="keyword">protected</span> string <a class="code" href="class_altova_1_1_document.php#ae9847a71b91c4035913dee0ad58ee15">schemaLocation</a>         = null;
<a name="l00036"></a>00036
<a name="l00037"></a><a class="code" href="class_altova_1_1_document.php#1a2be59fa2700c9b1d8feac69b1e62cb">00037</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_document.php#1a2be59fa2700c9b1d8feac69b1e62cb">Document</a>()
<a name="l00038"></a>00038                 {
<a name="l00039"></a>00039                 }
<a name="l00040"></a>00040
<a name="l00041"></a><a class="code" href="class_altova_1_1_document.php#050fae2d4a132670cd3fee9e30dd865b">00041</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_altova_1_1_document.php#050fae2d4a132670cd3fee9e30dd865b">SetRootElementName</a>(string <a class="code" href="class_altova_1_1_document.php#514d77c61ed77b981179f7f6d9a5ef30">namespaceURI</a>, string <a class="code" href="class_altova_1_1_document.php#01704ddb7bb192764e117aac4c96d14f">rootElementName</a>)
<a name="l00042"></a>00042                 {
<a name="l00043"></a>00043                         <span class="keyword">this</span>.namespaceURI = namespaceURI;
<a name="l00044"></a>00044                         <span class="keyword">this</span>.rootElementName = rootElementName;
<a name="l00045"></a>00045                 }
<a name="l00046"></a>00046
<a name="l00047"></a><a class="code" href="class_altova_1_1_document.php#bc0535524c965777c8d9027e7c37f140">00047</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_altova_1_1_document.php#bc0535524c965777c8d9027e7c37f140">SetSchemaLocation</a>(string <a class="code" href="class_altova_1_1_document.php#ae9847a71b91c4035913dee0ad58ee15">schemaLocation</a>)
<a name="l00048"></a>00048                 {
<a name="l00049"></a>00049                         <span class="keyword">this</span>.schemaLocation = schemaLocation;
<a name="l00050"></a>00050                 }
<a name="l00051"></a>00051
<a name="l00052"></a><a class="code" href="class_altova_1_1_document.php#0e30654fcb3b57ab52f540b6d3b61ea6">00052</a>                 <span class="keyword">public</span> XmlNode <a class="code" href="class_altova_1_1_document.php#0e30654fcb3b57ab52f540b6d3b61ea6">Load</a>(XmlReader reader)
<a name="l00053"></a>00053                 {
<a name="l00054"></a>00054                         XmlDocument doc = <span class="keyword">new</span> XmlDocument();
<a name="l00055"></a>00055                         doc.Load(reader);
<a name="l00056"></a>00056                         <span class="keywordflow">return</span> doc.DocumentElement;
<a name="l00057"></a>00057                 }
<a name="l00058"></a>00058
<a name="l00059"></a><a class="code" href="class_altova_1_1_document.php#3eeed0a2c0e1575422601ab7ff00c9f6">00059</a>                 <span class="keyword">public</span> XmlNode <a class="code" href="class_altova_1_1_document.php#0e30654fcb3b57ab52f540b6d3b61ea6">Load</a>(string filename)
<a name="l00060"></a>00060                 {
<a name="l00061"></a>00061                         XmlDocument doc = <span class="keyword">new</span> XmlDocument();
<a name="l00062"></a>00062                         doc.Load(filename);
<a name="l00063"></a>00063                         <span class="keywordflow">return</span> doc.DocumentElement;
<a name="l00064"></a>00064                 }
<a name="l00065"></a>00065
<a name="l00066"></a><a class="code" href="class_altova_1_1_document.php#beedbe6a4c75e21e121cc8f44f12201e">00066</a>                 <span class="keyword">public</span> XmlNode <a class="code" href="class_altova_1_1_document.php#beedbe6a4c75e21e121cc8f44f12201e">LoadXML</a>(string xml)
<a name="l00067"></a>00067                 {
<a name="l00068"></a>00068                         XmlDocument doc = <span class="keyword">new</span> XmlDocument();
<a name="l00069"></a>00069                         doc.LoadXml(xml);
<a name="l00070"></a>00070                         <span class="keywordflow">return</span> doc.DocumentElement;
<a name="l00071"></a>00071                 }
<a name="l00072"></a>00072
<a name="l00073"></a><a class="code" href="class_altova_1_1_document.php#389fef0117da9b2bdcf1910d04248893">00073</a>                 <span class="keyword">public</span> string <a class="code" href="class_altova_1_1_document.php#389fef0117da9b2bdcf1910d04248893">SaveXML</a>(<a class="code" href="class_altova_1_1_node.php">Node</a> node)
<a name="l00074"></a>00074                 {
<a name="l00075"></a>00075                         <a class="code" href="class_altova_1_1_document.php#8766cea1f71bd669a49170d51900a519">FinalizeRootElement</a>(node);
<a name="l00076"></a>00076                         <span class="keywordflow">return</span> node.<a class="code" href="class_altova_1_1_node.php#6a3b274665063bc1bef40363d1b69af0">domNode</a>.OwnerDocument.OuterXml;
<a name="l00077"></a>00077                 }
<a name="l00078"></a>00078
<a name="l00079"></a><a class="code" href="class_altova_1_1_document.php#cddb0505631975964f8b439bde2dde93">00079</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_altova_1_1_document.php#cddb0505631975964f8b439bde2dde93">Save</a>(string filename, <a class="code" href="class_altova_1_1_node.php">Node</a> node)
<a name="l00080"></a>00080                 {
<a name="l00081"></a>00081                         <a class="code" href="class_altova_1_1_document.php#8766cea1f71bd669a49170d51900a519">FinalizeRootElement</a>(node);
<a name="l00082"></a>00082                         node.<a class="code" href="class_altova_1_1_node.php#6a3b274665063bc1bef40363d1b69af0">domNode</a>.OwnerDocument.Save(filename);
<a name="l00083"></a>00083                 }
<a name="l00084"></a>00084
<a name="l00085"></a>00085                 <span class="keyword">abstract</span> <span class="keyword">protected</span> <span class="keywordtype">void</span> <a class="code" href="class_altova_1_1_document.php#34421e9b740b71a356134bea4eaf1ce3">DeclareNamespaces</a>(<a class="code" href="class_altova_1_1_node.php">Node</a> node);
<a name="l00086"></a>00086
<a name="l00087"></a><a class="code" href="class_altova_1_1_document.php#3c2f5e2847b84d174036d0bbcea63952">00087</a>                 <span class="keyword">protected</span> <span class="keywordtype">void</span> <a class="code" href="class_altova_1_1_document.php#3c2f5e2847b84d174036d0bbcea63952">DeclareNamespace</a>(<a class="code" href="class_altova_1_1_node.php">Node</a> node, string prefix, string URI)
<a name="l00088"></a>00088                 {
<a name="l00089"></a>00089                         node.<a class="code" href="class_altova_1_1_node.php#05faa22e4d9628b90007a19fca8681f2">DeclareNamespace</a>(prefix, URI);
<a name="l00090"></a>00090                 }
<a name="l00091"></a>00091
<a name="l00092"></a><a class="code" href="class_altova_1_1_document.php#8766cea1f71bd669a49170d51900a519">00092</a>                 <span class="keyword">protected</span> <span class="keywordtype">void</span> <a class="code" href="class_altova_1_1_document.php#8766cea1f71bd669a49170d51900a519">FinalizeRootElement</a>(<a class="code" href="class_altova_1_1_node.php">Node</a> node)
<a name="l00093"></a>00093                 {
<a name="l00094"></a>00094                         <span class="keywordflow">if</span> (node.<a class="code" href="class_altova_1_1_node.php#6a3b274665063bc1bef40363d1b69af0">domNode</a>.ParentNode.NodeType != XmlNodeType.DocumentFragment)
<a name="l00095"></a>00095                                 <span class="keywordflow">return</span>;
<a name="l00096"></a>00096
<a name="l00097"></a>00097                         <span class="keywordflow">if</span> (<a class="code" href="class_altova_1_1_document.php#01704ddb7bb192764e117aac4c96d14f">rootElementName</a> == null || <a class="code" href="class_altova_1_1_document.php#01704ddb7bb192764e117aac4c96d14f">rootElementName</a> == <span class="stringliteral">""</span>)
<a name="l00098"></a>00098                                 <span class="keywordflow">throw</span> <span class="keyword">new</span> Exception(<span class="stringliteral">"Call SetRootElementName first"</span>);
<a name="l00099"></a>00099
<a name="l00100"></a>00100                         node.<a class="code" href="class_altova_1_1_node.php#0fbb28a68cb9ff90f6b8f59fb641ba09">MakeRoot</a>(<a class="code" href="class_altova_1_1_document.php#514d77c61ed77b981179f7f6d9a5ef30">namespaceURI</a>, <a class="code" href="class_altova_1_1_document.php#01704ddb7bb192764e117aac4c96d14f">rootElementName</a>, <a class="code" href="class_altova_1_1_document.php#ae9847a71b91c4035913dee0ad58ee15">schemaLocation</a>);
<a name="l00101"></a>00101                         <a class="code" href="class_altova_1_1_document.php#34421e9b740b71a356134bea4eaf1ce3">DeclareNamespaces</a>(node);
<a name="l00102"></a>00102                 }
<a name="l00103"></a>00103         }
<a name="l00104"></a>00104 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
