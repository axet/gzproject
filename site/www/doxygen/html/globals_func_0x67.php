<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li id="current"><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="globals.php"><span>All</span></a></li>
    <li id="current"><a href="globals_func.php"><span>Functions</span></a></li>
    <li><a href="globals_vars.php"><span>Variables</span></a></li>
    <li><a href="globals_type.php"><span>Typedefs</span></a></li>
    <li><a href="globals_enum.php"><span>Enumerations</span></a></li>
    <li><a href="globals_eval.php"><span>Enumerator</span></a></li>
    <li><a href="globals_defs.php"><span>Defines</span></a></li>
  </ul>
</div>
<div class="tabs">
  <ul>
    <li><a href="globals_func.php#index__"><span>_</span></a></li>
    <li><a href="globals_func_0x61.php#index_a"><span>a</span></a></li>
    <li><a href="globals_func_0x62.php#index_b"><span>b</span></a></li>
    <li><a href="globals_func_0x63.php#index_c"><span>c</span></a></li>
    <li><a href="globals_func_0x64.php#index_d"><span>d</span></a></li>
    <li><a href="globals_func_0x65.php#index_e"><span>e</span></a></li>
    <li><a href="globals_func_0x66.php#index_f"><span>f</span></a></li>
    <li id="current"><a href="globals_func_0x67.php#index_g"><span>g</span></a></li>
    <li><a href="globals_func_0x69.php#index_i"><span>i</span></a></li>
    <li><a href="globals_func_0x6a.php#index_j"><span>j</span></a></li>
    <li><a href="globals_func_0x6c.php#index_l"><span>l</span></a></li>
    <li><a href="globals_func_0x6d.php#index_m"><span>m</span></a></li>
    <li><a href="globals_func_0x6f.php#index_o"><span>o</span></a></li>
    <li><a href="globals_func_0x70.php#index_p"><span>p</span></a></li>
    <li><a href="globals_func_0x72.php#index_r"><span>r</span></a></li>
    <li><a href="globals_func_0x73.php#index_s"><span>s</span></a></li>
    <li><a href="globals_func_0x75.php#index_u"><span>u</span></a></li>
    <li><a href="globals_func_0x76.php#index_v"><span>v</span></a></li>
    <li><a href="globals_func_0x77.php#index_w"><span>w</span></a></li>
  </ul>
</div>

<p>
&nbsp;
<p>
<h3><a class="anchor" name="index_g">- g -</a></h3><ul>
<li>GetClientVersion()
: <a class="el" href="namespace_client_identify.php#fcaa59a2937c9ae050362dde9d2615dc">clientidentify.cpp</a>
<li>GetFileName()
: <a class="el" href="emulator_8cpp.php#8e42a5262d1a9451d2479008b92573a8">emulator.cpp</a>
<li>GetLastErrorString()
: <a class="el" href="_get_last_error_8cpp.php#08a27e7e8262a89266902d7e8bd002ae">GetLastError.cpp</a>
, <a class="el" href="_get_last_error_8h.php#c4f875854ed858d52d160e42a9b62cc9">GetLastError.h</a>
, <a class="el" href="_get_last_error_8cpp.php#c159f66e5b0dbb1a8c978cb969db05f0">GetLastError.cpp</a>
, <a class="el" href="_get_last_error_8h.php#c159f66e5b0dbb1a8c978cb969db05f0">GetLastError.h</a>
<li>GetLibraryBody()
: <a class="el" href="body_8cpp.php#e7e526f89d8d0365ca2c446f7341cbe5">body.cpp</a>
<li>GetModuleFileName()
: <a class="el" href="namespace_ini_ext.php#e8ec57b84b079b6b4bcac40184ab24dc">path.cpp</a>
<li>GetModuleFullName()
: <a class="el" href="namespace_ini_ext.php#bb454be0d063b277b6b64868fbea522e">path.cpp</a>
<li>GetModuleName()
: <a class="el" href="namespace_ini_ext.php#a95b514880abb2c5fc111d58372d2635">path.cpp</a>
, <a class="el" href="emulator_8cpp.php#cda7731403cbaea2e632feb47cb2a8de">emulator.cpp</a>
, <a class="el" href="namespace_ini_ext.php#a95b514880abb2c5fc111d58372d2635">path.cpp</a>
<li>GetModuleRunDir()
: <a class="el" href="namespace_ini_ext.php#d57a751f4b0835a7f50f46e09eb4e582">path.cpp</a>
, <a class="el" href="log_8cpp.php#5237486e38d18105bd744c3bc60140bc">log.cpp</a>
, <a class="el" href="namespace_ini_ext.php#d57a751f4b0835a7f50f46e09eb4e582">path.cpp</a>
<li>GetSystemDirectory()
: <a class="el" href="namespace_ini_ext.php#cb7001002fff0e81ee96f9da42701da8">path.cpp</a>
<li>GetTempFilePath()
: <a class="el" href="namespace_ini_ext.php#28417a9bc7b70f721d7d46aab86b98bf">path.cpp</a>
</ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
