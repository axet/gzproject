<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_52f2866252e58876aa460cdbcef5f577.php">GZClientLoader</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_047fd9d3f985bb564ea29a1bfd6196d7.php">GZClientConfig</a></div>
<h1>GZClientConfig.cpp</h1><a href="_g_z_client_config_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// GZClientConfig.cpp : ©wёqАіҐОµ{¦ЎЄєГю§O¦ж¬°ЎC</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="preprocessor">#include "<a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2_std_afx_8h.php">stdafx.h</a>"</span>
<a name="l00005"></a>00005 <span class="preprocessor">#include "<a class="code" href="_g_z_client_config_8h.php">GZClientConfig.h</a>"</span>
<a name="l00006"></a>00006 <span class="preprocessor">#include "<a class="code" href="_g_z_client_config_dlg_8h.php">GZClientConfigDlg.h</a>"</span>
<a name="l00007"></a>00007
<a name="l00008"></a>00008 <span class="preprocessor">#ifdef _DEBUG</span>
<a name="l00009"></a>00009 <span class="preprocessor"></span><span class="preprocessor">#define new DEBUG_NEW</span>
<a name="l00010"></a>00010 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00011"></a>00011 <span class="preprocessor"></span>
<a name="l00012"></a>00012
<a name="l00013"></a>00013 <span class="comment">// CGZClientConfigApp</span>
<a name="l00014"></a>00014
<a name="l00015"></a>00015 BEGIN_MESSAGE_MAP(<a class="code" href="class_c_g_z_client_config_app.php">CGZClientConfigApp</a>, CWinApp)
<a name="l00016"></a>00016         ON_COMMAND(ID_HELP, CWinApp::OnHelp)
<a name="l00017"></a>00017 END_MESSAGE_MAP()
<a name="l00018"></a>00018
<a name="l00019"></a>00019
<a name="l00020"></a>00020 <span class="comment">// CGZClientConfigApp «Шєc</span>
<a name="l00021"></a>00021
<a name="l00022"></a><a class="code" href="class_c_g_z_client_config_app.php#bfbf01ab355620a8cf6de7a05f3af237">00022</a> <a class="code" href="class_c_g_z_client_config_app.php">CGZClientConfigApp</a>::<a class="code" href="class_c_g_z_client_config_app.php">CGZClientConfigApp</a>()
<a name="l00023"></a>00023 {
<a name="l00024"></a>00024         <span class="comment">// TODO: ¦b¦№Ґ[¤J«Шєcµ{¦ЎЅXЎA</span>
<a name="l00025"></a>00025         <span class="comment">// ±N©Т¦і­«­nЄєЄм©lі]©wҐ[¤J InitInstance ¤¤</span>
<a name="l00026"></a>00026 }
<a name="l00027"></a>00027
<a name="l00028"></a>00028
<a name="l00029"></a>00029 <span class="comment">// ¶И¦іЄє¤@­У CGZClientConfigApp Є«Ґу</span>
<a name="l00030"></a>00030
<a name="l00031"></a><a class="code" href="_g_z_client_config_8cpp.php#3f6eb3e710495dd3d051228d9efb91ef">00031</a> <a class="code" href="class_c_g_z_client_config_app.php">CGZClientConfigApp</a> <a class="code" href="_g_z_assist_8cpp.php#f685e0f163f1f3dce87dc4d55255537a">theApp</a>;
<a name="l00032"></a>00032
<a name="l00033"></a>00033
<a name="l00034"></a>00034 <span class="comment">// CGZClientConfigApp Єм©lі]©w</span>
<a name="l00035"></a>00035
<a name="l00036"></a><a class="code" href="class_c_g_z_client_config_app.php#15830c82fb31158e82a5abd5bf035c1a">00036</a> BOOL <a class="code" href="class_c_g_z_client_config_app.php#15830c82fb31158e82a5abd5bf035c1a">CGZClientConfigApp::InitInstance</a>()
<a name="l00037"></a>00037 {
<a name="l00038"></a>00038         <span class="comment">// °І¦pАіҐОµ{¦Ўёк°TІMіж«ь©wЁПҐО ComCtl32.dll 6.0 (§t) ҐH«бЄ©Ґ»</span>
<a name="l00039"></a>00039         <span class="comment">// ҐH±ТҐОµшД±¤ЖјЛ¦ЎЎA«h Windows XP »Э­n InitCommonControls()ЎC§_«hЄєёЬЎA</span>
<a name="l00040"></a>00040         <span class="comment">// Ґф¦уµшµЎЄє«ШҐЯ±NҐў±СЎC</span>
<a name="l00041"></a>00041         InitCommonControls();
<a name="l00042"></a>00042
<a name="l00043"></a>00043         CWinApp::InitInstance();
<a name="l00044"></a>00044
<a name="l00045"></a>00045         AfxEnableControlContainer();
<a name="l00046"></a>00046
<a name="l00047"></a>00047         <span class="comment">// јР·ЗЄм©lі]©w</span>
<a name="l00048"></a>00048         <span class="comment">// ¦pЄG±z¤ЈЁПҐОіoЁЗҐ\ЇаЁГҐB·Qґо¤ЦіМ«б§№¦ЁЄєҐi°х¦жАЙ¤j¤pЎA±zҐiҐH±q¤U¦C</span>
<a name="l00049"></a>00049         <span class="comment">// µ{¦ЎЅXІѕ°Ј¤Ј»Э­nЄєЄм©l¤Ж±`¦ЎЎAЕЬ§уАx¦sі]©w­ИЄєµnїэѕчЅX</span>
<a name="l00050"></a>00050         <span class="comment">// TODO: ±zАіёУѕA«Ч­Ч§п¦№¦r¦к (ЁТ¦pЎA¤ЅҐq¦WєЩ©ОІХВґ¦WєЩ)</span>
<a name="l00051"></a>00051         SetRegistryKey(_T(<span class="stringliteral">"Guard Zone"</span>));
<a name="l00052"></a>00052
<a name="l00053"></a>00053   CoInitialize(0);
<a name="l00054"></a>00054
<a name="l00055"></a>00055   {
<a name="l00056"></a>00056     <a class="code" href="class_c_g_z_client_config_dlg.php">CGZClientConfigDlg</a> dlg;
<a name="l00057"></a>00057           m_pMainWnd = &amp;dlg;
<a name="l00058"></a>00058           INT_PTR nResponse = dlg.DoModal();
<a name="l00059"></a>00059           <span class="keywordflow">if</span> (nResponse == IDOK)
<a name="l00060"></a>00060           {
<a name="l00061"></a>00061                   <span class="comment">// TODO: ¦b¦№©сёm©уЁПҐО [ЅT©w] ЁУ°±¤оЁПҐО№пёЬ¤и¶ф®Й</span>
<a name="l00062"></a>00062                   <span class="comment">// іBІzЄєµ{¦ЎЅX</span>
<a name="l00063"></a>00063           }
<a name="l00064"></a>00064           <span class="keywordflow">else</span> <span class="keywordflow">if</span> (nResponse == IDCANCEL)
<a name="l00065"></a>00065           {
<a name="l00066"></a>00066                   <span class="comment">// TODO: ¦b¦№©сёm©уЁПҐО [Ёъ®ш] ЁУ°±¤оЁПҐО№пёЬ¤и¶ф®Й</span>
<a name="l00067"></a>00067                   <span class="comment">// іBІzЄєµ{¦ЎЅX</span>
<a name="l00068"></a>00068           }
<a name="l00069"></a>00069   }
<a name="l00070"></a>00070
<a name="l00071"></a>00071   CoUninitialize();
<a name="l00072"></a>00072         <span class="comment">// ¦]¬°¤wёgГці¬№пёЬ¤и¶фЎA¶З¦^ FALSEЎA©ТҐH§Ъ­М·|µІ§фАіҐОµ{¦ЎЎA</span>
<a name="l00073"></a>00073         <span class="comment">// ¦У«DґЈҐЬ¶}©lАіҐОµ{¦ЎЄє°T®§ЎC</span>
<a name="l00074"></a>00074         <span class="keywordflow">return</span> FALSE;
<a name="l00075"></a>00075 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
