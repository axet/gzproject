<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_9aa33d2cd8dd981e9a7ee8bcdb7921b2.php">library</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_001ec9c223af4972373a8871e49e9c04.php">TextParser</a></div>
<h1>CompareTree.h</h1><a href="_compare_tree_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// GZProject - library, Ultima Online utils.</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="comment">// This program is free software; you can redistribute it and/or</span>
<a name="l00005"></a>00005 <span class="comment">// modify it under the terms of the GNU General Public License</span>
<a name="l00006"></a>00006 <span class="comment">// as published by the Free Software Foundation; either version 2</span>
<a name="l00007"></a>00007 <span class="comment">// of the License, or (at your option) any later version.</span>
<a name="l00008"></a>00008
<a name="l00009"></a>00009 <span class="comment">// This program is distributed in the hope that it will be useful,</span>
<a name="l00010"></a>00010 <span class="comment">// but WITHOUT ANY WARRANTY; without even the implied warranty of</span>
<a name="l00011"></a>00011 <span class="comment">// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the</span>
<a name="l00012"></a>00012 <span class="comment">// GNU General Public License for more details.</span>
<a name="l00013"></a>00013
<a name="l00014"></a>00014 <span class="comment">// You should have received a copy of the GNU General Public License</span>
<a name="l00015"></a>00015 <span class="comment">// along with this program; if not, write to the Free Software</span>
<a name="l00016"></a>00016 <span class="comment">// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</span>
<a name="l00017"></a>00017
<a name="l00020"></a>00020
<a name="l00023"></a>00023
<a name="l00027"></a>00027
<a name="l00028"></a>00028 <span class="preprocessor">#include &lt;algorithm&gt;</span>
<a name="l00029"></a>00029
<a name="l00030"></a>00030 <span class="keyword">template</span> &lt;<span class="keyword">class</span> LinkDataType&gt;
<a name="l00031"></a><a class="code" href="class_compare_tree.php">00031</a> <span class="keyword">class </span><a class="code" href="class_compare_tree.php">CompareTree</a>
<a name="l00032"></a>00032 {
<a name="l00033"></a>00033 <span class="keyword">public</span>:
<a name="l00037"></a><a class="code" href="struct_compare_tree_1_1_input_value.php">00037</a>   <span class="keyword">struct </span><a class="code" href="struct_compare_tree_1_1_input_value.php">InputValue</a>
<a name="l00038"></a>00038   {
<a name="l00039"></a><a class="code" href="struct_compare_tree_1_1_input_value.php#bcf9d87223097931a0c611c0d157b739">00039</a>     LinkDataType <a class="code" href="struct_compare_tree_1_1_input_value.php#bcf9d87223097931a0c611c0d157b739">data</a>;
<a name="l00040"></a><a class="code" href="struct_compare_tree_1_1_input_value.php#a7d79766790d52bf073e9920f02af473">00040</a>     std::string <a class="code" href="struct_compare_tree_1_1_input_value.php#a7d79766790d52bf073e9920f02af473">text</a>;
<a name="l00041"></a>00041
<a name="l00042"></a><a class="code" href="struct_compare_tree_1_1_input_value.php#8ac6ae85b3f59f69a3a3e31a73a89f86">00042</a>     <a class="code" href="struct_compare_tree_1_1_input_value.php#8ac6ae85b3f59f69a3a3e31a73a89f86">InputValue</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* t,LinkDataType d):<a class="code" href="struct_compare_tree_1_1_input_value.php#a7d79766790d52bf073e9920f02af473">text</a>(t),<a class="code" href="struct_compare_tree_1_1_input_value.php#bcf9d87223097931a0c611c0d157b739">data</a>(d)
<a name="l00043"></a>00043     {
<a name="l00044"></a>00044     }
<a name="l00045"></a>00045
<a name="l00046"></a><a class="code" href="struct_compare_tree_1_1_input_value.php#76955d7186f371d85b0bdbba74d18561">00046</a>     <span class="keywordtype">bool</span> <a class="code" href="struct_compare_tree_1_1_input_value.php#76955d7186f371d85b0bdbba74d18561">operator &lt; </a>(<span class="keyword">const</span> <a class="code" href="struct_compare_tree_1_1_input_value.php">InputValue</a>&amp;iv)<span class="keyword"> const</span>
<a name="l00047"></a>00047 <span class="keyword">    </span>{
<a name="l00048"></a>00048       <span class="keywordflow">return</span> <a class="code" href="struct_compare_tree_1_1_input_value.php#a7d79766790d52bf073e9920f02af473">text</a>&lt;iv.<a class="code" href="struct_compare_tree_1_1_input_value.php#a7d79766790d52bf073e9920f02af473">text</a>;
<a name="l00049"></a>00049     }
<a name="l00050"></a>00050
<a name="l00051"></a><a class="code" href="struct_compare_tree_1_1_input_value.php#419d2efd8f8810854563f0dde6913cb9">00051</a>     <span class="keywordtype">bool</span> <a class="code" href="struct_compare_tree_1_1_input_value.php#419d2efd8f8810854563f0dde6913cb9">operator == </a>(<span class="keyword">const</span> <a class="code" href="struct_compare_tree_1_1_input_value.php">InputValue</a> &amp;iv)<span class="keyword"> const</span>
<a name="l00052"></a>00052 <span class="keyword">    </span>{
<a name="l00053"></a>00053       <span class="keywordflow">return</span> <a class="code" href="struct_compare_tree_1_1_input_value.php#a7d79766790d52bf073e9920f02af473">text</a>==iv.<a class="code" href="struct_compare_tree_1_1_input_value.php#a7d79766790d52bf073e9920f02af473">text</a>;
<a name="l00054"></a>00054     }
<a name="l00055"></a>00055
<a name="l00056"></a><a class="code" href="struct_compare_tree_1_1_input_value.php#486e755cf8205ddea5e92159ddaa34c3">00056</a>     <span class="keywordtype">bool</span> <a class="code" href="struct_compare_tree_1_1_input_value.php#486e755cf8205ddea5e92159ddaa34c3">operator != </a>(<span class="keyword">const</span> <a class="code" href="struct_compare_tree_1_1_input_value.php">InputValue</a> &amp;iv)<span class="keyword"> const</span>
<a name="l00057"></a>00057 <span class="keyword">    </span>{
<a name="l00058"></a>00058       <span class="keywordflow">return</span> <a class="code" href="struct_compare_tree_1_1_input_value.php#a7d79766790d52bf073e9920f02af473">text</a>!=iv.<a class="code" href="struct_compare_tree_1_1_input_value.php#a7d79766790d52bf073e9920f02af473">text</a>;
<a name="l00059"></a>00059     }
<a name="l00060"></a>00060   };
<a name="l00061"></a><a class="code" href="class_compare_tree.php#aff60cb434bd8e809550a5802649f79a">00061</a>   <span class="keyword">typedef</span> std::vector&lt; InputValue &gt; <a class="code" href="class_compare_tree.php#aff60cb434bd8e809550a5802649f79a">InputValueList</a>;
<a name="l00062"></a>00062
<a name="l00063"></a><a class="code" href="struct_compare_tree_1_1_output_value.php">00063</a>   <span class="keyword">struct </span><a class="code" href="struct_compare_tree_1_1_output_value.php">OutputValue</a>
<a name="l00064"></a>00064   {
<a name="l00065"></a><a class="code" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffc">00065</a>     <span class="keyword">enum</span> <a class="code" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffc">Action</a>{<a class="code" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffca687aac0f1b9b33002dc072d213d738f">Increment</a>,<a class="code" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffca445cd17414dce061116b26537586c53">Decrement</a>} <a class="code" href="struct_compare_tree_1_1_output_value.php#825e0e4d3fef6615a13e2fce9705822c">action</a>;
<a name="l00066"></a><a class="code" href="struct_compare_tree_1_1_output_value.php#ffed88cfd0a7fa7189997669f408a97f">00066</a>     LinkDataType <a class="code" href="struct_compare_tree_1_1_output_value.php#ffed88cfd0a7fa7189997669f408a97f">data</a>;
<a name="l00067"></a>00067
<a name="l00068"></a><a class="code" href="struct_compare_tree_1_1_output_value.php#8e40b5ccbed7079fb508bd8a02e61cc0">00068</a>     <a class="code" href="struct_compare_tree_1_1_output_value.php#8e40b5ccbed7079fb508bd8a02e61cc0">OutputValue</a>(<a class="code" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffc">Action</a> a,InputValueList::iterator i):<a class="code" href="struct_compare_tree_1_1_output_value.php#ffed88cfd0a7fa7189997669f408a97f">data</a>(i-&gt;<a class="code" href="struct_compare_tree_1_1_output_value.php#ffed88cfd0a7fa7189997669f408a97f">data</a>),<a class="code" href="struct_compare_tree_1_1_output_value.php#825e0e4d3fef6615a13e2fce9705822c">action</a>(a)
<a name="l00069"></a>00069     {
<a name="l00070"></a>00070     }
<a name="l00071"></a>00071   };
<a name="l00072"></a><a class="code" href="class_compare_tree.php#957345b69b674cbbaac8d48373d3a7da">00072</a>   <span class="keyword">typedef</span> std::vector&lt; OutputValue &gt; <a class="code" href="class_compare_tree.php#957345b69b674cbbaac8d48373d3a7da">OutputValueList</a>;
<a name="l00073"></a>00073
<a name="l00074"></a><a class="code" href="class_compare_tree.php#08db3a89321fcd496133ad904b15db82">00074</a>   <span class="keyword">static</span> <a class="code" href="class_compare_tree.php#957345b69b674cbbaac8d48373d3a7da">OutputValueList</a> <a class="code" href="class_compare_tree.php#08db3a89321fcd496133ad904b15db82">Compare</a>(<a class="code" href="class_compare_tree.php#aff60cb434bd8e809550a5802649f79a">InputValueList</a>&amp; iv1,<a class="code" href="class_compare_tree.php#aff60cb434bd8e809550a5802649f79a">InputValueList</a> &amp;iv2)
<a name="l00075"></a>00075   {
<a name="l00076"></a>00076     std::sort(iv1.begin(),iv1.end());
<a name="l00077"></a>00077     std::sort(iv2.begin(),iv2.end());
<a name="l00078"></a>00078
<a name="l00079"></a>00079     <a class="code" href="class_compare_tree.php#957345b69b674cbbaac8d48373d3a7da">OutputValueList</a> ovl;
<a name="l00080"></a>00080     {
<a name="l00081"></a>00081     <span class="keywordflow">for</span>(InputValueList::iterator i1=iv1.begin();i1!=iv1.end();i1++)
<a name="l00082"></a>00082     {
<a name="l00083"></a>00083       <span class="keywordtype">bool</span> i1found=<span class="keyword">false</span>;
<a name="l00084"></a>00084       <span class="keywordflow">for</span>(InputValueList::iterator i2=iv2.begin();i2!=iv2.end();i2++)
<a name="l00085"></a>00085       {
<a name="l00086"></a>00086         <span class="keywordflow">if</span>(*i1==*i2)
<a name="l00087"></a>00087         {
<a name="l00088"></a>00088           i1found=<span class="keyword">true</span>;
<a name="l00089"></a>00089           <span class="keywordflow">break</span>;
<a name="l00090"></a>00090         }
<a name="l00091"></a>00091       }
<a name="l00092"></a>00092       <span class="keywordflow">if</span>(!i1found)
<a name="l00093"></a>00093       {
<a name="l00094"></a>00094         ovl.push_back(<a class="code" href="struct_compare_tree_1_1_output_value.php">OutputValue</a>(<a class="code" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffca445cd17414dce061116b26537586c53">OutputValue::Decrement</a>,i1));
<a name="l00095"></a>00095       }
<a name="l00096"></a>00096     }
<a name="l00097"></a>00097     }
<a name="l00098"></a>00098     {
<a name="l00099"></a>00099     <span class="keywordflow">for</span>(InputValueList::iterator i2=iv2.begin();i2!=iv2.end();i2++)
<a name="l00100"></a>00100     {
<a name="l00101"></a>00101       <span class="keywordtype">bool</span> i2found=<span class="keyword">false</span>;
<a name="l00102"></a>00102       <span class="keywordflow">for</span>(InputValueList::iterator i1=iv1.begin();i1!=iv1.end();i1++)
<a name="l00103"></a>00103       {
<a name="l00104"></a>00104         <span class="keywordflow">if</span>(*i1==*i2)
<a name="l00105"></a>00105         {
<a name="l00106"></a>00106           i2found=<span class="keyword">true</span>;
<a name="l00107"></a>00107           <span class="keywordflow">break</span>;
<a name="l00108"></a>00108         }
<a name="l00109"></a>00109       }
<a name="l00110"></a>00110       <span class="keywordflow">if</span>(!i2found)
<a name="l00111"></a>00111       {
<a name="l00112"></a>00112         ovl.push_back(<a class="code" href="struct_compare_tree_1_1_output_value.php">OutputValue</a>(<a class="code" href="struct_compare_tree_1_1_output_value.php#02e5aed19698253ad19dbee1d1224ffca687aac0f1b9b33002dc072d213d738f">OutputValue::Increment</a>,i2));
<a name="l00113"></a>00113       }
<a name="l00114"></a>00114     }
<a name="l00115"></a>00115     }
<a name="l00116"></a>00116     <span class="keywordflow">return</span> ovl;
<a name="l00117"></a>00117   }
<a name="l00118"></a>00118 };
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
