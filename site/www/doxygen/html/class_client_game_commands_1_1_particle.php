<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespace_client_game_commands.php">ClientGameCommands</a>::<a class="el" href="class_client_game_commands_1_1_particle.php">Particle</a></div>
<h1>ClientGameCommands::Particle Class Reference</h1><!-- doxytag: class="ClientGameCommands::Particle" -->работа со статсами.
<a href="#_details">More...</a>
<p>
<code>#include &lt;<a class="el" href="client_2clientgamecommands_8h-source.php">clientgamecommands.h</a>&gt;</code>
<p>
<p>Inheritance diagram for ClientGameCommands::Particle:
<p><center><img src="class_client_game_commands_1_1_particle.png" usemap="#ClientGameCommands::Particle_map" border="0" alt=""></center>
<map name="ClientGameCommands::Particle_map">
<area href="class_client_game_commands_1_1_control.php" alt="ClientGameCommands::Control" shape="rect" coords="0,56,186,80">
<area href="class_client_game_commands_1_1_control.php" alt="ClientGameCommands::Control" shape="rect" coords="392,56,578,80">
<area href="class_client_game_commands_1_1_control.php" alt="ClientGameCommands::Control" shape="rect" coords="784,56,970,80">
<area href="class_client_game_commands_1_1_control.php" alt="ClientGameCommands::Control" shape="rect" coords="1176,56,1362,80">
<area href="class_c_client_commands.php" alt="CClientCommands" shape="rect" coords="196,112,382,136">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="196,168,382,192">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="196,224,382,248">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="196,280,382,304">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="196,336,382,360">
<area href="class_c_client_commands.php" alt="CClientCommands" shape="rect" coords="588,112,774,136">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="588,168,774,192">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="588,224,774,248">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="588,280,774,304">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="588,336,774,360">
<area href="class_c_client_commands.php" alt="CClientCommands" shape="rect" coords="980,112,1166,136">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="980,168,1166,192">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="980,224,1166,248">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="980,280,1166,304">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="980,336,1166,360">
<area href="class_c_client_commands.php" alt="CClientCommands" shape="rect" coords="1372,112,1558,136">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="1372,168,1558,192">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="1372,224,1558,248">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="1372,280,1558,304">
<area href="class_c_unit.php" alt="CUnit" shape="rect" coords="1372,336,1558,360">
</map>
<a href="class_client_game_commands_1_1_particle-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#12cfdd08b737c3b5a8805b8942fe18ac">StatsGetHits</a> ()=0</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">возвращает количество хитов.  <a href="#12cfdd08b737c3b5a8805b8942fe18ac"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#f252082438ddd2ba4ad0190f4b686945">StatsGetMana</a> ()=0</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">возвращает количество маны.  <a href="#f252082438ddd2ba4ad0190f4b686945"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#252e938a07282136c6019d2f9df7643b">StatsGetStam</a> ()=0</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">возвращает количество стамины.  <a href="#252e938a07282136c6019d2f9df7643b"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#101a8f243c7a291a0b605d1c49db8b53">StatsGetWeight</a> ()=0</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">количество нагрузки.  <a href="#101a8f243c7a291a0b605d1c49db8b53"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#12cfdd08b737c3b5a8805b8942fe18ac">StatsGetHits</a> ()=0</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#f252082438ddd2ba4ad0190f4b686945">StatsGetMana</a> ()=0</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#252e938a07282136c6019d2f9df7643b">StatsGetStam</a> ()=0</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#101a8f243c7a291a0b605d1c49db8b53">StatsGetWeight</a> ()=0</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#12cfdd08b737c3b5a8805b8942fe18ac">StatsGetHits</a> ()=0</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#f252082438ddd2ba4ad0190f4b686945">StatsGetMana</a> ()=0</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#252e938a07282136c6019d2f9df7643b">StatsGetStam</a> ()=0</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#101a8f243c7a291a0b605d1c49db8b53">StatsGetWeight</a> ()=0</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#12cfdd08b737c3b5a8805b8942fe18ac">StatsGetHits</a> ()=0</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#f252082438ddd2ba4ad0190f4b686945">StatsGetMana</a> ()=0</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#252e938a07282136c6019d2f9df7643b">StatsGetStam</a> ()=0</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_client_game_commands_1_1_particle.php#101a8f243c7a291a0b605d1c49db8b53">StatsGetWeight</a> ()=0</td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>
работа со статсами.
<p>

<p>
Definition at line <a class="el" href="client_2clientgamecommands_8h-source.php#l00072">72</a> of file <a class="el" href="client_2clientgamecommands_8h-source.php">client/clientgamecommands.h</a>.<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="12cfdd08b737c3b5a8805b8942fe18ac"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetHits" ref="12cfdd08b737c3b5a8805b8942fe18ac" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetHits           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
возвращает количество хитов.
<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#ba225686ac56e1689302c1c964609932">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#7abd446f621ea44b0aa47094907a3364">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#99b7f7e739475ededd6e3ae9d456b185">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#3deae99d707e6d4a1773429f944cae0b">InterruptableUnit&lt; CGZAUnit &gt;</a>.
<p>
Referenced by <a class="el" href="outofprocslave_8cpp-source.php#l00052">COutOfProcSlave::Receive()</a>.
</div>
</div><p>
<a class="anchor" name="f252082438ddd2ba4ad0190f4b686945"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetMana" ref="f252082438ddd2ba4ad0190f4b686945" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetMana           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
возвращает количество маны.
<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#65dc9488f6cb5b1e551b2251c7882e13">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#cb95dab14966cae860c1ccf9fd8bf84d">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#429c063c5047a4934d01d74394123ffa">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#dcdb3fce6618974646a59a570ea2561b">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="252e938a07282136c6019d2f9df7643b"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetStam" ref="252e938a07282136c6019d2f9df7643b" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetStam           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
возвращает количество стамины.
<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#24c3e0522ffc48e0a2b61864f22ec8c1">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#e9015f4aa1438aa0aba180335cb9a56f">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#1cf39323ee1adfe698ee22369201113e">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#d9096f90cb0a18aa708b56d474dc3ff0">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="101a8f243c7a291a0b605d1c49db8b53"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetWeight" ref="101a8f243c7a291a0b605d1c49db8b53" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetWeight           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
количество нагрузки.
<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#c0d2fc009d4a3287dd2f4db789fde548">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#911b2611fe1b346ddd7cedb6c94b0c73">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#f9d2289fcc0b2e87050fc648f05d6737">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#2df60d6bc5510a94c715c9eddfa22044">InterruptableUnit&lt; CGZAUnit &gt;</a>.
<p>
Referenced by <a class="el" href="outofprocslave_8cpp-source.php#l00052">COutOfProcSlave::Receive()</a>.
</div>
</div><p>
<a class="anchor" name="12cfdd08b737c3b5a8805b8942fe18ac"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetHits" ref="12cfdd08b737c3b5a8805b8942fe18ac" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetHits           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#ba225686ac56e1689302c1c964609932">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#7abd446f621ea44b0aa47094907a3364">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#99b7f7e739475ededd6e3ae9d456b185">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#3deae99d707e6d4a1773429f944cae0b">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="f252082438ddd2ba4ad0190f4b686945"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetMana" ref="f252082438ddd2ba4ad0190f4b686945" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetMana           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#65dc9488f6cb5b1e551b2251c7882e13">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#cb95dab14966cae860c1ccf9fd8bf84d">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#429c063c5047a4934d01d74394123ffa">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#dcdb3fce6618974646a59a570ea2561b">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="252e938a07282136c6019d2f9df7643b"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetStam" ref="252e938a07282136c6019d2f9df7643b" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetStam           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#24c3e0522ffc48e0a2b61864f22ec8c1">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#e9015f4aa1438aa0aba180335cb9a56f">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#1cf39323ee1adfe698ee22369201113e">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#d9096f90cb0a18aa708b56d474dc3ff0">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="101a8f243c7a291a0b605d1c49db8b53"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetWeight" ref="101a8f243c7a291a0b605d1c49db8b53" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetWeight           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#c0d2fc009d4a3287dd2f4db789fde548">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#911b2611fe1b346ddd7cedb6c94b0c73">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#f9d2289fcc0b2e87050fc648f05d6737">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#2df60d6bc5510a94c715c9eddfa22044">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="12cfdd08b737c3b5a8805b8942fe18ac"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetHits" ref="12cfdd08b737c3b5a8805b8942fe18ac" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetHits           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#ba225686ac56e1689302c1c964609932">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#7abd446f621ea44b0aa47094907a3364">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#99b7f7e739475ededd6e3ae9d456b185">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#3deae99d707e6d4a1773429f944cae0b">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="f252082438ddd2ba4ad0190f4b686945"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetMana" ref="f252082438ddd2ba4ad0190f4b686945" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetMana           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#65dc9488f6cb5b1e551b2251c7882e13">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#cb95dab14966cae860c1ccf9fd8bf84d">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#429c063c5047a4934d01d74394123ffa">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#dcdb3fce6618974646a59a570ea2561b">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="252e938a07282136c6019d2f9df7643b"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetStam" ref="252e938a07282136c6019d2f9df7643b" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetStam           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#24c3e0522ffc48e0a2b61864f22ec8c1">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#e9015f4aa1438aa0aba180335cb9a56f">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#1cf39323ee1adfe698ee22369201113e">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#d9096f90cb0a18aa708b56d474dc3ff0">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="101a8f243c7a291a0b605d1c49db8b53"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetWeight" ref="101a8f243c7a291a0b605d1c49db8b53" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetWeight           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#c0d2fc009d4a3287dd2f4db789fde548">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#911b2611fe1b346ddd7cedb6c94b0c73">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#f9d2289fcc0b2e87050fc648f05d6737">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#2df60d6bc5510a94c715c9eddfa22044">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="12cfdd08b737c3b5a8805b8942fe18ac"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetHits" ref="12cfdd08b737c3b5a8805b8942fe18ac" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetHits           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#ba225686ac56e1689302c1c964609932">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#7abd446f621ea44b0aa47094907a3364">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#99b7f7e739475ededd6e3ae9d456b185">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#3deae99d707e6d4a1773429f944cae0b">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="f252082438ddd2ba4ad0190f4b686945"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetMana" ref="f252082438ddd2ba4ad0190f4b686945" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetMana           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#65dc9488f6cb5b1e551b2251c7882e13">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#cb95dab14966cae860c1ccf9fd8bf84d">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#429c063c5047a4934d01d74394123ffa">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#dcdb3fce6618974646a59a570ea2561b">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="252e938a07282136c6019d2f9df7643b"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetStam" ref="252e938a07282136c6019d2f9df7643b" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetStam           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#24c3e0522ffc48e0a2b61864f22ec8c1">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#e9015f4aa1438aa0aba180335cb9a56f">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#1cf39323ee1adfe698ee22369201113e">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#d9096f90cb0a18aa708b56d474dc3ff0">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<a class="anchor" name="101a8f243c7a291a0b605d1c49db8b53"></a><!-- doxytag: member="ClientGameCommands::Particle::StatsGetWeight" ref="101a8f243c7a291a0b605d1c49db8b53" args="()=0" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual int ClientGameCommands::Particle::StatsGetWeight           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [pure virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Implemented in <a class="el" href="class_c_client_u_o_commands.php#c0d2fc009d4a3287dd2f4db789fde548">CClientUOCommands</a>, <a class="el" href="class_c_g_z_unit_advise.php#911b2611fe1b346ddd7cedb6c94b0c73">CGZUnitAdvise</a>, <a class="el" href="class_c_out_of_proc_master.php#f9d2289fcc0b2e87050fc648f05d6737">COutOfProcMaster</a>, and <a class="el" href="class_interruptable_unit.php#2df60d6bc5510a94c715c9eddfa22044">InterruptableUnit&lt; CGZAUnit &gt;</a>.
</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="client_2clientgamecommands_8h-source.php">client/clientgamecommands.h</a><li><a class="el" href="_g_zone_2modules_2_c_i_q_samplev6_2common_2clientgamecommands_8h-source.php">GZone/modules/CIQSamplev6/common/clientgamecommands.h</a><li><a class="el" href="_g_zone_2modules_2_c_i_q_samplev7_2common_2clientgamecommands_8h-source.php">GZone/modules/CIQSamplev7/common/clientgamecommands.h</a><li><a class="el" href="_g_zone_2modules_2_c_i_q_samplev8_2common_2clientgamecommands_8h-source.php">GZone/modules/CIQSamplev8/common/clientgamecommands.h</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
