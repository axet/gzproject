<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_8ba19508940e77109a2da1f76a26e336.php">ServerAddin</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_c307239c489953b9a301fb7770146337.php">AccountManager</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_e4481258ccec154c4de1e56fd31a9ea1.php">xsd</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_2ca762023376b09daa055c5a92fae68e.php">SchemaAccountManager</a></div>
<h1>SchemaAccountManager.h</h1><a href="_schema_account_manager_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// SchemaAccountManager.h</span>
<a name="l00004"></a>00004 <span class="comment">//</span>
<a name="l00005"></a>00005 <span class="comment">// This file was generated by XMLSpy 2006 Enterprise Edition.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00008"></a>00008 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00009"></a>00009 <span class="comment">//</span>
<a name="l00010"></a>00010 <span class="comment">// Refer to the XMLSpy Documentation for further details.</span>
<a name="l00011"></a>00011 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00012"></a>00012 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment"></span>
<a name="l00015"></a>00015
<a name="l00016"></a>00016 <span class="preprocessor">#ifndef SchemaAccountManager_H_INCLUDED</span>
<a name="l00017"></a>00017 <span class="preprocessor"></span><span class="preprocessor">#define SchemaAccountManager_H_INCLUDED</span>
<a name="l00018"></a>00018 <span class="preprocessor"></span>
<a name="l00019"></a>00019 <span class="preprocessor">#if _MSC_VER &gt; 1000</span>
<a name="l00020"></a>00020 <span class="preprocessor"></span><span class="preprocessor">        #pragma once</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span><span class="preprocessor">#endif // _MSC_VER &gt; 1000</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include "<a class="code" href="_schema_account_manager_base_8h.php">SchemaAccountManagerBase.h</a>"</span>
<a name="l00024"></a>00024
<a name="l00025"></a>00025 <span class="preprocessor">#include "<a class="code" href="_schema_account_manager___c_config_type_8h.php">SchemaAccountManager_CConfigType.h</a>"</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include "<a class="code" href="_schema_account_manager___c_get_users_list___request_type_8h.php">SchemaAccountManager_CGetUsersList_RequestType.h</a>"</span>
<a name="l00027"></a>00027 <span class="preprocessor">#include "<a class="code" href="_schema_account_manager___c_get_users_list___respond_type_8h.php">SchemaAccountManager_CGetUsersList_RespondType.h</a>"</span>
<a name="l00028"></a>00028 <span class="preprocessor">#include "<a class="code" href="_schema_account_manager___c_ip_protection_type_8h.php">SchemaAccountManager_CIpProtectionType.h</a>"</span>
<a name="l00029"></a>00029 <span class="preprocessor">#include "<a class="code" href="_schema_account_manager___c_item_type_8h.php">SchemaAccountManager_CItemType.h</a>"</span>
<a name="l00030"></a>00030 <span class="preprocessor">#include "<a class="code" href="_schema_account_manager___c_items_type_8h.php">SchemaAccountManager_CItemsType.h</a>"</span>
<a name="l00031"></a>00031 <span class="preprocessor">#include "<a class="code" href="_schema_account_manager___c_packet_type_8h.php">SchemaAccountManager_CPacketType.h</a>"</span>
<a name="l00032"></a>00032 <span class="preprocessor">#include "<a class="code" href="_schema_account_manager___c_privileges_type_8h.php">SchemaAccountManager_CPrivilegesType.h</a>"</span>
<a name="l00033"></a>00033 <span class="preprocessor">#include "<a class="code" href="_schema_account_manager___c_set_sphere_directory_type_8h.php">SchemaAccountManager_CSetSphereDirectoryType.h</a>"</span>
<a name="l00034"></a>00034 <span class="preprocessor">#include "<a class="code" href="_schema_account_manager___c_set_users_list_type_8h.php">SchemaAccountManager_CSetUsersListType.h</a>"</span>
<a name="l00035"></a>00035
<a name="l00036"></a>00036 <span class="preprocessor">#endif // SchemaAccountManager_H_INCLUDED</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
