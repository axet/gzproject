<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_285c2ba7298296be1db6222076863471.php">GZAdministrator</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_4b6972d9d8cfe06aa798f38257009ef5.php">SchemaIpProtection</a></div>
<h1>SchemaIpProtectionDoc.cs</h1><a href="_schema_ip_protection_doc_8cs.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="keyword">using</span> System;
<a name="l00002"></a>00002
<a name="l00003"></a>00003 <span class="keyword">namespace </span>SchemaIpProtection
<a name="l00004"></a>00004 {
<a name="l00005"></a><a class="code" href="class_schema_ip_protection_1_1_schema_ip_protection_doc.php">00005</a>         <span class="keyword">public</span> <span class="keyword">class </span><a class="code" href="class_schema_ip_protection_1_1_schema_ip_protection_doc.php">SchemaIpProtectionDoc</a> : Altova.Document
<a name="l00006"></a>00006         {
<a name="l00007"></a><a class="code" href="class_schema_ip_protection_1_1_schema_ip_protection_doc.php#c4c88d1755dc360a779f4a1a1364d62b">00007</a>                 override <span class="keyword">protected</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_ip_protection_1_1_schema_ip_protection_doc.php#c4c88d1755dc360a779f4a1a1364d62b">DeclareNamespaces</a>(Altova.Node node)
<a name="l00008"></a>00008                 {
<a name="l00009"></a>00009                 }
<a name="l00010"></a>00010         }
<a name="l00011"></a>00011 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
