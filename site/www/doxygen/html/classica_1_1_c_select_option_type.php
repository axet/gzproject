<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespaceica.php">ica</a>::<a class="el" href="classica_1_1_c_select_option_type.php">CSelectOptionType</a></div>
<h1>ica::CSelectOptionType Class Reference</h1><!-- doxytag: class="ica::CSelectOptionType" --><code>#include &lt;<a class="el" href="_interproc_client_addin___c_select_option_type_8h-source.php">InterprocClientAddin_CSelectOptionType.h</a>&gt;</code>
<p>
<a href="classica_1_1_c_select_option_type-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#82c97630fc4de14d2f99bde2711d44f2">CSelectOptionType</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#a7bc4396af54f909408efaa20e767bb1">CSelectOptionType</a> (CNode &amp;rParentNode, MSXML2::IXMLDOMNodePtr spThisNode)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#bf92d6acf83f41e25494e7439a2c0f13">CSelectOptionType</a> (MSXML2::IXMLDOMDocument2Ptr spDoc)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#25f0c23e7e4be9f65dda0d75a12abf1f">GetItemTypeCount</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#532b549d10f367ad8baecb7a748a7367">HasItemType</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#6dfe313a92b9cd3eb312e419b1288309">AddItemType</a> (<a class="el" href="classica_1_1_c_item_type.php">CItemType</a> &amp;<a class="el" href="class_item_type.php">ItemType</a>)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#685f7cdd29fe2f0d6197d5e7e77d4622">InsertItemTypeAt</a> (<a class="el" href="classica_1_1_c_item_type.php">CItemType</a> &amp;<a class="el" href="class_item_type.php">ItemType</a>, int nIndex)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#6461ff537720b34bf3b3f476b208b81d">ReplaceItemTypeAt</a> (<a class="el" href="classica_1_1_c_item_type.php">CItemType</a> &amp;<a class="el" href="class_item_type.php">ItemType</a>, int nIndex)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="classica_1_1_c_item_type.php">CItemType</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#f3213986c9e5e5ca5ecf33f692a2cb64">GetItemTypeAt</a> (int nIndex)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="classica_1_1_c_item_type.php">CItemType</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#f2009df72d57604d5776d16c31a77d1b">GetItemType</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">MSXML2::IXMLDOMNodePtr&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#4bf57332c549df458093fe7c495a22d9">GetStartingItemTypeCursor</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">MSXML2::IXMLDOMNodePtr&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#41e14a83e7b4491ac162e9c1e5dc678e">GetAdvancedItemTypeCursor</a> (MSXML2::IXMLDOMNodePtr pCurNode)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="classica_1_1_c_item_type.php">CItemType</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#4e24d1a35d2a66b4435a241b24284dcb">GetItemTypeValueAtCursor</a> (MSXML2::IXMLDOMNodePtr pCurNode)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#969e5fe18a58f88d22756c18a0f857e2">RemoveItemTypeAt</a> (int nIndex)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#d086fba2b2570b7e5f690c594cfaeadb">RemoveItemType</a> ()</td></tr>

<tr><td colspan="2"><br><h2>Static Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">static EGroupType&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#e1cfc69472a7127ab703af557cb33a19">GetGroupType</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">static int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#0c18f92ad42d01aa5a17f7694a40f98f">GetItemTypeMinCount</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">static int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="classica_1_1_c_select_option_type.php#62481377c1a9b0fa035cd0fffdc97c1d">GetItemTypeMaxCount</a> ()</td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8h-source.php#l00030">30</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8h-source.php">InterprocClientAddin_CSelectOptionType.h</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="82c97630fc4de14d2f99bde2711d44f2"></a><!-- doxytag: member="ica::CSelectOptionType::CSelectOptionType" ref="82c97630fc4de14d2f99bde2711d44f2" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">ica::CSelectOptionType::CSelectOptionType           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8h-source.php#l00033">33</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8h-source.php">InterprocClientAddin_CSelectOptionType.h</a>.<div class="fragment"><pre class="fragment"><a name="l00033"></a>00033 : CNode() {}
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="a7bc4396af54f909408efaa20e767bb1"></a><!-- doxytag: member="ica::CSelectOptionType::CSelectOptionType" ref="a7bc4396af54f909408efaa20e767bb1" args="(CNode &amp;rParentNode, MSXML2::IXMLDOMNodePtr spThisNode)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">ica::CSelectOptionType::CSelectOptionType           </td>
          <td>(</td>
          <td class="paramtype">CNode &amp;&nbsp;</td>
          <td class="paramname"> <em>rParentNode</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">MSXML2::IXMLDOMNodePtr&nbsp;</td>
          <td class="paramname"> <em>spThisNode</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8h-source.php#l00034">34</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8h-source.php">InterprocClientAddin_CSelectOptionType.h</a>.<div class="fragment"><pre class="fragment"><a name="l00034"></a>00034 : CNode(rParentNode, spThisNode) {}
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="bf92d6acf83f41e25494e7439a2c0f13"></a><!-- doxytag: member="ica::CSelectOptionType::CSelectOptionType" ref="bf92d6acf83f41e25494e7439a2c0f13" args="(MSXML2::IXMLDOMDocument2Ptr spDoc)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">ica::CSelectOptionType::CSelectOptionType           </td>
          <td>(</td>
          <td class="paramtype">MSXML2::IXMLDOMDocument2Ptr&nbsp;</td>
          <td class="paramname"> <em>spDoc</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8h-source.php#l00035">35</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8h-source.php">InterprocClientAddin_CSelectOptionType.h</a>.<div class="fragment"><pre class="fragment"><a name="l00035"></a>00035 : CNode(spDoc) {}
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="e1cfc69472a7127ab703af557cb33a19"></a><!-- doxytag: member="ica::CSelectOptionType::GetGroupType" ref="e1cfc69472a7127ab703af557cb33a19" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="classaltova_1_1_c_node.php#23a91b5b6ef1abca12745ecd7a2fe7a4">CNode::EGroupType</a> ica::CSelectOptionType::GetGroupType           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00032">32</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00033"></a>00033 {
<a name="l00034"></a>00034         <span class="keywordflow">return</span> eSequence;
<a name="l00035"></a>00035 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="0c18f92ad42d01aa5a17f7694a40f98f"></a><!-- doxytag: member="ica::CSelectOptionType::GetItemTypeMinCount" ref="0c18f92ad42d01aa5a17f7694a40f98f" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int ica::CSelectOptionType::GetItemTypeMinCount           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00036">36</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00037"></a>00037 {
<a name="l00038"></a>00038         <span class="keywordflow">return</span> 1;
<a name="l00039"></a>00039 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="62481377c1a9b0fa035cd0fffdc97c1d"></a><!-- doxytag: member="ica::CSelectOptionType::GetItemTypeMaxCount" ref="62481377c1a9b0fa035cd0fffdc97c1d" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int ica::CSelectOptionType::GetItemTypeMaxCount           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00042">42</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00043"></a>00043 {
<a name="l00044"></a>00044         <span class="keywordflow">return</span> 1;
<a name="l00045"></a>00045 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="25f0c23e7e4be9f65dda0d75a12abf1f"></a><!-- doxytag: member="ica::CSelectOptionType::GetItemTypeCount" ref="25f0c23e7e4be9f65dda0d75a12abf1f" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int ica::CSelectOptionType::GetItemTypeCount           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00048">48</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00049"></a>00049 {
<a name="l00050"></a>00050         <span class="keywordflow">return</span> ChildCountInternal(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>));
<a name="l00051"></a>00051 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="532b549d10f367ad8baecb7a748a7367"></a><!-- doxytag: member="ica::CSelectOptionType::HasItemType" ref="532b549d10f367ad8baecb7a748a7367" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">bool ica::CSelectOptionType::HasItemType           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00054">54</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.
<p>
Referenced by <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00117">RemoveItemType()</a>.<div class="fragment"><pre class="fragment"><a name="l00055"></a>00055 {
<a name="l00056"></a>00056         <span class="keywordflow">return</span> InternalHasChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>));
<a name="l00057"></a>00057 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="6dfe313a92b9cd3eb312e419b1288309"></a><!-- doxytag: member="ica::CSelectOptionType::AddItemType" ref="6dfe313a92b9cd3eb312e419b1288309" args="(CItemType &amp;ItemType)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ica::CSelectOptionType::AddItemType           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="classica_1_1_c_item_type.php">CItemType</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>ItemType</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00060">60</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.
<p>
Referenced by <a class="el" href="outofprocslave_8cpp-source.php#l00248">COutOfProcSlave::SelectedOption()</a>.<div class="fragment"><pre class="fragment"><a name="l00061"></a>00061 {
<a name="l00062"></a>00062         InternalAppendNode(_T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), ItemType);
<a name="l00063"></a>00063 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="685f7cdd29fe2f0d6197d5e7e77d4622"></a><!-- doxytag: member="ica::CSelectOptionType::InsertItemTypeAt" ref="685f7cdd29fe2f0d6197d5e7e77d4622" args="(CItemType &amp;ItemType, int nIndex)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ica::CSelectOptionType::InsertItemTypeAt           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="classica_1_1_c_item_type.php">CItemType</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>ItemType</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nIndex</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00066">66</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00067"></a>00067 {
<a name="l00068"></a>00068         InternalInsertNodeAt(_T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), nIndex, <a class="code" href="class_item_type.php">ItemType</a>);
<a name="l00069"></a>00069 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="6461ff537720b34bf3b3f476b208b81d"></a><!-- doxytag: member="ica::CSelectOptionType::ReplaceItemTypeAt" ref="6461ff537720b34bf3b3f476b208b81d" args="(CItemType &amp;ItemType, int nIndex)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ica::CSelectOptionType::ReplaceItemTypeAt           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="classica_1_1_c_item_type.php">CItemType</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>ItemType</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nIndex</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00072">72</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00073"></a>00073 {
<a name="l00074"></a>00074         InternalReplaceNodeAt(_T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), nIndex, <a class="code" href="class_item_type.php">ItemType</a>);
<a name="l00075"></a>00075 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="f3213986c9e5e5ca5ecf33f692a2cb64"></a><!-- doxytag: member="ica::CSelectOptionType::GetItemTypeAt" ref="f3213986c9e5e5ca5ecf33f692a2cb64" args="(int nIndex)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="classica_1_1_c_item_type.php">CItemType</a> ica::CSelectOptionType::GetItemTypeAt           </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nIndex</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00079">79</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.
<p>
Referenced by <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00105">GetItemType()</a>.<div class="fragment"><pre class="fragment"><a name="l00080"></a>00080 {
<a name="l00081"></a>00081         <span class="keywordflow">return</span> CItemType(*<span class="keyword">this</span>, InternalGetAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), nIndex));
<a name="l00082"></a>00082 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="f2009df72d57604d5776d16c31a77d1b"></a><!-- doxytag: member="ica::CSelectOptionType::GetItemType" ref="f2009df72d57604d5776d16c31a77d1b" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="classica_1_1_c_item_type.php">CItemType</a> ica::CSelectOptionType::GetItemType           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00105">105</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.
<p>
References <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00079">GetItemTypeAt()</a>.
<p>
Referenced by <a class="el" href="outofprocmasterdata_8cpp-source.php#l00007">COutOfProcMasterData::Receive()</a>.<div class="fragment"><pre class="fragment"><a name="l00106"></a>00106 {
<a name="l00107"></a>00107         <span class="keywordflow">return</span> <a class="code" href="classica_1_1_c_select_option_type.php#f3213986c9e5e5ca5ecf33f692a2cb64">GetItemTypeAt</a>(0);
<a name="l00108"></a>00108 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="4bf57332c549df458093fe7c495a22d9"></a><!-- doxytag: member="ica::CSelectOptionType::GetStartingItemTypeCursor" ref="4bf57332c549df458093fe7c495a22d9" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">MSXML2::IXMLDOMNodePtr ica::CSelectOptionType::GetStartingItemTypeCursor           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00084">84</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00085"></a>00085 {
<a name="l00086"></a>00086         <span class="keywordflow">return</span> InternalGetFirstChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>));
<a name="l00087"></a>00087 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="41e14a83e7b4491ac162e9c1e5dc678e"></a><!-- doxytag: member="ica::CSelectOptionType::GetAdvancedItemTypeCursor" ref="41e14a83e7b4491ac162e9c1e5dc678e" args="(MSXML2::IXMLDOMNodePtr pCurNode)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">MSXML2::IXMLDOMNodePtr ica::CSelectOptionType::GetAdvancedItemTypeCursor           </td>
          <td>(</td>
          <td class="paramtype">MSXML2::IXMLDOMNodePtr&nbsp;</td>
          <td class="paramname"> <em>pCurNode</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00089">89</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00090"></a>00090 {
<a name="l00091"></a>00091         <span class="keywordflow">return</span> InternalGetNextChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), pCurNode);
<a name="l00092"></a>00092 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="4e24d1a35d2a66b4435a241b24284dcb"></a><!-- doxytag: member="ica::CSelectOptionType::GetItemTypeValueAtCursor" ref="4e24d1a35d2a66b4435a241b24284dcb" args="(MSXML2::IXMLDOMNodePtr pCurNode)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="classica_1_1_c_item_type.php">CItemType</a> ica::CSelectOptionType::GetItemTypeValueAtCursor           </td>
          <td>(</td>
          <td class="paramtype">MSXML2::IXMLDOMNodePtr&nbsp;</td>
          <td class="paramname"> <em>pCurNode</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00094">94</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00095"></a>00095 {
<a name="l00096"></a>00096         <span class="keywordflow">if</span>( pCurNode == NULL )
<a name="l00097"></a>00097                 <span class="keywordflow">throw</span> <a class="code" href="classaltova_1_1_c_xml_exception.php">CXmlException</a>(CXmlException::eError1, _T(<span class="stringliteral">"Index out of range"</span>));
<a name="l00098"></a>00098         <span class="keywordflow">else</span>
<a name="l00099"></a>00099
<a name="l00100"></a>00100                 <span class="keywordflow">return</span> CItemType( *<span class="keyword">this</span>, pCurNode );
<a name="l00101"></a>00101 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="969e5fe18a58f88d22756c18a0f857e2"></a><!-- doxytag: member="ica::CSelectOptionType::RemoveItemTypeAt" ref="969e5fe18a58f88d22756c18a0f857e2" args="(int nIndex)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ica::CSelectOptionType::RemoveItemTypeAt           </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>nIndex</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00111">111</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.
<p>
Referenced by <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00117">RemoveItemType()</a>.<div class="fragment"><pre class="fragment"><a name="l00112"></a>00112 {
<a name="l00113"></a>00113         InternalRemoveAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), nIndex);
<a name="l00114"></a>00114 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="d086fba2b2570b7e5f690c594cfaeadb"></a><!-- doxytag: member="ica::CSelectOptionType::RemoveItemType" ref="d086fba2b2570b7e5f690c594cfaeadb" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ica::CSelectOptionType::RemoveItemType           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00117">117</a> of file <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a>.
<p>
References <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00054">HasItemType()</a>, and <a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php#l00111">RemoveItemTypeAt()</a>.<div class="fragment"><pre class="fragment"><a name="l00118"></a>00118 {
<a name="l00119"></a>00119         <span class="keywordflow">while</span> (<a class="code" href="classica_1_1_c_select_option_type.php#532b549d10f367ad8baecb7a748a7367">HasItemType</a>())
<a name="l00120"></a>00120                 <a class="code" href="classica_1_1_c_select_option_type.php#969e5fe18a58f88d22756c18a0f857e2">RemoveItemTypeAt</a>(0);
<a name="l00121"></a>00121 }
</pre></div>
<p>

</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="_interproc_client_addin___c_select_option_type_8h-source.php">InterprocClientAddin_CSelectOptionType.h</a><li><a class="el" href="_interproc_client_addin___c_select_option_type_8cpp-source.php">InterprocClientAddin_CSelectOptionType.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
