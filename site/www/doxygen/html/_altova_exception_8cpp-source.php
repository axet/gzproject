<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_9aa33d2cd8dd981e9a7ee8bcdb7921b2.php">library</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_6082230fd4939409a029fb3498f63f15.php">Altova</a></div>
<h1>AltovaException.cpp</h1><a href="_altova_exception_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// AltovaException.cpp</span>
<a name="l00004"></a>00004 <span class="comment">//</span>
<a name="l00005"></a>00005 <span class="comment">// This file was generated by XMLSpy 2006 Enterprise Edition.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00008"></a>00008 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00009"></a>00009 <span class="comment">//</span>
<a name="l00010"></a>00010 <span class="comment">// Refer to the XMLSpy Documentation for further details.</span>
<a name="l00011"></a>00011 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00012"></a>00012 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment"></span>
<a name="l00015"></a>00015
<a name="l00016"></a>00016 <span class="preprocessor">#include "<a class="code" href="library_2_altova_2_std_afx_8h.php">StdAfx.h</a>"</span>
<a name="l00017"></a>00017 <span class="preprocessor">#include "<a class="code" href="_altova_8h.php">Altova.h</a>"</span>
<a name="l00018"></a>00018 <span class="preprocessor">#include "<a class="code" href="_schema_types_8h.php">SchemaTypes.h</a>"</span>
<a name="l00019"></a>00019 <span class="preprocessor">#include "<a class="code" href="_altova_exception_8h.php">AltovaException.h</a>"</span>
<a name="l00020"></a>00020
<a name="l00021"></a>00021 <span class="keyword">namespace </span>altova {
<a name="l00022"></a>00022
<a name="l00023"></a>00023
<a name="l00024"></a><a class="code" href="classaltova_1_1_c_altova_exception.php#634c1136ede00cd82f56b280dc559a69">00024</a> <a class="code" href="classaltova_1_1_c_altova_exception.php#634c1136ede00cd82f56b280dc559a69">CAltovaException::CAltovaException</a>(<span class="keywordtype">int</span> nCode, <span class="keyword">const</span> <a class="code" href="_altova_8h.php#e5d59c80151fb6bca8c486086a1ed05b">tstring</a>&amp; sMessage, <span class="keywordtype">bool</span> bUserExc)
<a name="l00025"></a>00025         : m_nCode(nCode), m_sMessage(sMessage), m_bUserExc(bUserExc)
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 }
<a name="l00028"></a>00028
<a name="l00029"></a>00029
<a name="l00030"></a><a class="code" href="classaltova_1_1_c_altova_exception.php#46524542bf1373fa341bbdf9089051a4">00030</a> <span class="keywordtype">int</span> <a class="code" href="classaltova_1_1_c_altova_exception.php#46524542bf1373fa341bbdf9089051a4">CAltovaException::GetCode</a>()
<a name="l00031"></a>00031 {
<a name="l00032"></a>00032         <span class="keywordflow">return</span> <a class="code" href="classaltova_1_1_c_altova_exception.php#0ad4934fcac060e018855eb563889ff9">m_nCode</a>;
<a name="l00033"></a>00033 }
<a name="l00034"></a>00034
<a name="l00035"></a>00035
<a name="l00036"></a><a class="code" href="classaltova_1_1_c_altova_exception.php#52368b7bbd89295eb2e353bdf9c24c98">00036</a> <a class="code" href="_altova_8h.php#e5d59c80151fb6bca8c486086a1ed05b">tstring</a> <a class="code" href="classaltova_1_1_c_altova_exception.php#52368b7bbd89295eb2e353bdf9c24c98">CAltovaException::GetInfo</a>()
<a name="l00037"></a>00037 {
<a name="l00038"></a>00038         <span class="keywordflow">return</span> <a class="code" href="classaltova_1_1_c_altova_exception.php#7c4227a13d4dfa1184cc413c62841c70">m_sMessage</a>;
<a name="l00039"></a>00039 }
<a name="l00040"></a>00040
<a name="l00041"></a><a class="code" href="classaltova_1_1_c_altova_exception.php#2c86b566a97e40f865448568f333c073">00041</a> <span class="keywordtype">bool</span> <a class="code" href="classaltova_1_1_c_altova_exception.php#2c86b566a97e40f865448568f333c073">CAltovaException::IsUserException</a>()<span class="keyword"> const</span>
<a name="l00042"></a>00042 <span class="keyword"></span>{
<a name="l00043"></a>00043         <span class="keywordflow">return</span> <a class="code" href="classaltova_1_1_c_altova_exception.php#bdaf5de4004ec086bc60d17209320d7f">m_bUserExc</a>;
<a name="l00044"></a>00044 }
<a name="l00045"></a>00045
<a name="l00046"></a>00046 } <span class="comment">// namespace altova</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
