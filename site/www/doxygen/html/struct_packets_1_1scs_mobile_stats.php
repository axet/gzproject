<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespace_packets.php">Packets</a>::<a class="el" href="struct_packets_1_1scs_mobile_stats.php">scsMobileStats</a></div>
<h1>Packets::scsMobileStats Struct Reference</h1><!-- doxytag: class="Packets::scsMobileStats" --><code>#include &lt;<a class="el" href="packets_8h-source.php">packets.h</a>&gt;</code>
<p>
<a href="struct_packets_1_1scs_mobile_stats-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="struct_packets_1_1scs_pack.php">scsPack</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#f38240a13de748ed08f81abf0d85c987">p</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">unsigned short&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#ace94c700aef36954d10ced195d0464e">size</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_int.php">Server::ServerInt</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#ae752f35a66f65caca7400c2c1038085">serial</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="namespace_server.php#75b563c3fe0028838ba97e321b3af24b">Server::ServerChar</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#a7247edbd7414719f728189528838b9a">charname</a> [30]</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#fb8e73d21e973a0a088c896dfd2ce23c">hits</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#a52d6b5e26cd82937e5b76d2b34c1f21">maxhits</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#3766205631ada0912d1a1fdad34bbf1a">alownamechange</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#ff929b66015ef63522f123f2659bb9a3">validstats</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#2bc80735633e7d94dc3575f86fd4e3c2">grender</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#3e37be651b8eaf79f5ccc1da68248e04">str</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#1d3a462affc7db0b99e6a3e98f38a690">dex</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#b20243a9205ff8ac83d1b66056ae6727">inta</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#b15d2ddf3bcccc1ce956b35e5300d2d9">stam</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#5361b4dd545247e063ba04b8270fc7d2">maxstam</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#0e7638a5d29b952078e659f118395b94">mana</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#eb30a0f0cdf4e47dc2fc269449b9c787">maxmana</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">unsigned&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#09d03a4726aaa75dc49e3711c5f2b511">gold</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#0b8e7721ced24edd3d3d49e2b7b372d4">armor</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_packets_1_1scs_mobile_stats.php#be4dbedb7fba7dd5cc8d4232956703a8">weight</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00265">265</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="f38240a13de748ed08f81abf0d85c987"></a><!-- doxytag: member="Packets::scsMobileStats::p" ref="f38240a13de748ed08f81abf0d85c987" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="struct_packets_1_1scs_pack.php">scsPack</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#f38240a13de748ed08f81abf0d85c987">Packets::scsMobileStats::p</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00267">267</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
</div>
</div><p>
<a class="anchor" name="ace94c700aef36954d10ced195d0464e"></a><!-- doxytag: member="Packets::scsMobileStats::size" ref="ace94c700aef36954d10ced195d0464e" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">unsigned short <a class="el" href="struct_packets_1_1scs_mobile_stats.php#ace94c700aef36954d10ced195d0464e">Packets::scsMobileStats::size</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00268">268</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
</div>
</div><p>
<a class="anchor" name="ae752f35a66f65caca7400c2c1038085"></a><!-- doxytag: member="Packets::scsMobileStats::serial" ref="ae752f35a66f65caca7400c2c1038085" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_int.php">Server::ServerInt</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#ae752f35a66f65caca7400c2c1038085">Packets::scsMobileStats::serial</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00269">269</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="a7247edbd7414719f728189528838b9a"></a><!-- doxytag: member="Packets::scsMobileStats::charname" ref="a7247edbd7414719f728189528838b9a" args="[30]" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="namespace_server.php#75b563c3fe0028838ba97e321b3af24b">Server::ServerChar</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#a7247edbd7414719f728189528838b9a">Packets::scsMobileStats::charname</a>[30]          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00270">270</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="fb8e73d21e973a0a088c896dfd2ce23c"></a><!-- doxytag: member="Packets::scsMobileStats::hits" ref="fb8e73d21e973a0a088c896dfd2ce23c" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#fb8e73d21e973a0a088c896dfd2ce23c">Packets::scsMobileStats::hits</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00271">271</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="a52d6b5e26cd82937e5b76d2b34c1f21"></a><!-- doxytag: member="Packets::scsMobileStats::maxhits" ref="a52d6b5e26cd82937e5b76d2b34c1f21" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#a52d6b5e26cd82937e5b76d2b34c1f21">Packets::scsMobileStats::maxhits</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00272">272</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="3766205631ada0912d1a1fdad34bbf1a"></a><!-- doxytag: member="Packets::scsMobileStats::alownamechange" ref="3766205631ada0912d1a1fdad34bbf1a" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">bool <a class="el" href="struct_packets_1_1scs_mobile_stats.php#3766205631ada0912d1a1fdad34bbf1a">Packets::scsMobileStats::alownamechange</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00273">273</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
</div>
</div><p>
<a class="anchor" name="ff929b66015ef63522f123f2659bb9a3"></a><!-- doxytag: member="Packets::scsMobileStats::validstats" ref="ff929b66015ef63522f123f2659bb9a3" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">bool <a class="el" href="struct_packets_1_1scs_mobile_stats.php#ff929b66015ef63522f123f2659bb9a3">Packets::scsMobileStats::validstats</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00274">274</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
</div>
</div><p>
<a class="anchor" name="2bc80735633e7d94dc3575f86fd4e3c2"></a><!-- doxytag: member="Packets::scsMobileStats::grender" ref="2bc80735633e7d94dc3575f86fd4e3c2" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">bool <a class="el" href="struct_packets_1_1scs_mobile_stats.php#2bc80735633e7d94dc3575f86fd4e3c2">Packets::scsMobileStats::grender</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00275">275</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="3e37be651b8eaf79f5ccc1da68248e04"></a><!-- doxytag: member="Packets::scsMobileStats::str" ref="3e37be651b8eaf79f5ccc1da68248e04" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#3e37be651b8eaf79f5ccc1da68248e04">Packets::scsMobileStats::str</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00276">276</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="1d3a462affc7db0b99e6a3e98f38a690"></a><!-- doxytag: member="Packets::scsMobileStats::dex" ref="1d3a462affc7db0b99e6a3e98f38a690" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#1d3a462affc7db0b99e6a3e98f38a690">Packets::scsMobileStats::dex</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00277">277</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="b20243a9205ff8ac83d1b66056ae6727"></a><!-- doxytag: member="Packets::scsMobileStats::inta" ref="b20243a9205ff8ac83d1b66056ae6727" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#b20243a9205ff8ac83d1b66056ae6727">Packets::scsMobileStats::inta</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00278">278</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="b15d2ddf3bcccc1ce956b35e5300d2d9"></a><!-- doxytag: member="Packets::scsMobileStats::stam" ref="b15d2ddf3bcccc1ce956b35e5300d2d9" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#b15d2ddf3bcccc1ce956b35e5300d2d9">Packets::scsMobileStats::stam</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00279">279</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="5361b4dd545247e063ba04b8270fc7d2"></a><!-- doxytag: member="Packets::scsMobileStats::maxstam" ref="5361b4dd545247e063ba04b8270fc7d2" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#5361b4dd545247e063ba04b8270fc7d2">Packets::scsMobileStats::maxstam</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00280">280</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="0e7638a5d29b952078e659f118395b94"></a><!-- doxytag: member="Packets::scsMobileStats::mana" ref="0e7638a5d29b952078e659f118395b94" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#0e7638a5d29b952078e659f118395b94">Packets::scsMobileStats::mana</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00281">281</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="eb30a0f0cdf4e47dc2fc269449b9c787"></a><!-- doxytag: member="Packets::scsMobileStats::maxmana" ref="eb30a0f0cdf4e47dc2fc269449b9c787" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#eb30a0f0cdf4e47dc2fc269449b9c787">Packets::scsMobileStats::maxmana</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00282">282</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="09d03a4726aaa75dc49e3711c5f2b511"></a><!-- doxytag: member="Packets::scsMobileStats::gold" ref="09d03a4726aaa75dc49e3711c5f2b511" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">unsigned <a class="el" href="struct_packets_1_1scs_mobile_stats.php#09d03a4726aaa75dc49e3711c5f2b511">Packets::scsMobileStats::gold</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00283">283</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="0b8e7721ced24edd3d3d49e2b7b372d4"></a><!-- doxytag: member="Packets::scsMobileStats::armor" ref="0b8e7721ced24edd3d3d49e2b7b372d4" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#0b8e7721ced24edd3d3d49e2b7b372d4">Packets::scsMobileStats::armor</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00284">284</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<a class="anchor" name="be4dbedb7fba7dd5cc8d4232956703a8"></a><!-- doxytag: member="Packets::scsMobileStats::weight" ref="be4dbedb7fba7dd5cc8d4232956703a8" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_server_1_1_server_short.php">Server::ServerShort</a> <a class="el" href="struct_packets_1_1scs_mobile_stats.php#be4dbedb7fba7dd5cc8d4232956703a8">Packets::scsMobileStats::weight</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="packets_8h-source.php#l00285">285</a> of file <a class="el" href="packets_8h-source.php">packets.h</a>.
<p>
Referenced by <a class="el" href="_client_u_o_commands_8cpp-source.php#l00029">CClientUOCommands::FromServer()</a>.
</div>
</div><p>
<hr>The documentation for this struct was generated from the following file:<ul>
<li><a class="el" href="packets_8h-source.php">packets.h</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
