<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_d2d383d4738f8a137bd616c8820954cc.php">GZServerLoader</a></div>
<h1>GZServerAddinLoader.cpp</h1><a href="_g_z_server_addin_loader_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// GZServerAddinLoader.cpp : Defines the class behaviors for the application.</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="preprocessor">#include "<a class="code" href="_g_z_2_g_z_server_loader_2_std_afx_8h.php">stdafx.h</a>"</span>
<a name="l00005"></a>00005 <span class="preprocessor">#include "<a class="code" href="_g_z_server_addin_loader_8h.php">GZServerAddinLoader.h</a>"</span>
<a name="l00006"></a>00006 <span class="preprocessor">#include "<a class="code" href="_g_z_server_addin_loader_dlg_8h.php">GZServerAddinLoaderDlg.h</a>"</span>
<a name="l00007"></a>00007 <span class="preprocessor">#include &lt;altova/altovalib.h&gt;</span>
<a name="l00008"></a>00008 <span class="preprocessor">#include "<a class="code" href="_server_config_8h.php">xsd/ServerConfig/ServerConfig.h</a>"</span>
<a name="l00009"></a>00009 <span class="preprocessor">#include &lt;<a class="code" href="iniext_8h.php">iniext/iniext.h</a>&gt;</span>
<a name="l00010"></a>00010 <span class="preprocessor">#include &lt;<a class="code" href="flukemaster_8h.php">fluke/flukemaster.h</a>&gt;</span>
<a name="l00011"></a>00011
<a name="l00012"></a>00012 <span class="preprocessor">#ifdef _DEBUG</span>
<a name="l00013"></a>00013 <span class="preprocessor"></span><span class="preprocessor">#define new DEBUG_NEW</span>
<a name="l00014"></a>00014 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00015"></a>00015 <span class="preprocessor"></span>
<a name="l00016"></a>00016
<a name="l00017"></a>00017 <span class="comment">// CGZServerAddinLoaderApp</span>
<a name="l00018"></a>00018
<a name="l00019"></a>00019 BEGIN_MESSAGE_MAP(<a class="code" href="class_c_g_z_server_addin_loader_app.php">CGZServerAddinLoaderApp</a>, CWinApp)
<a name="l00020"></a>00020         ON_COMMAND(ID_HELP, CWinApp::OnHelp)
<a name="l00021"></a>00021 END_MESSAGE_MAP()
<a name="l00022"></a>00022
<a name="l00023"></a>00023
<a name="l00024"></a>00024 <span class="comment">// CGZServerAddinLoaderApp construction</span>
<a name="l00025"></a>00025
<a name="l00026"></a><a class="code" href="class_c_g_z_server_addin_loader_app.php#abaa55d9766b6f1dce358e4d29d6527d">00026</a> <a class="code" href="class_c_g_z_server_addin_loader_app.php">CGZServerAddinLoaderApp</a>::<a class="code" href="class_c_g_z_server_addin_loader_app.php">CGZServerAddinLoaderApp</a>()
<a name="l00027"></a>00027 {
<a name="l00028"></a>00028         <span class="comment">// TODO: add construction code here,</span>
<a name="l00029"></a>00029         <span class="comment">// Place all significant initialization in InitInstance</span>
<a name="l00030"></a>00030 }
<a name="l00031"></a>00031
<a name="l00032"></a>00032
<a name="l00033"></a>00033 <span class="comment">// The one and only CGZServerAddinLoaderApp object</span>
<a name="l00034"></a>00034
<a name="l00035"></a><a class="code" href="_g_z_server_addin_loader_8cpp.php#8795895c680daba5ee3c131c10276c8d">00035</a> <a class="code" href="class_c_g_z_server_addin_loader_app.php">CGZServerAddinLoaderApp</a> <a class="code" href="_g_z_assist_8cpp.php#f685e0f163f1f3dce87dc4d55255537a">theApp</a>;
<a name="l00036"></a>00036
<a name="l00037"></a>00037
<a name="l00038"></a>00038 <span class="comment">// CGZServerAddinLoaderApp initialization</span>
<a name="l00039"></a>00039
<a name="l00040"></a><a class="code" href="class_c_g_z_server_addin_loader_app.php#e4722fe4bf6d24aaeac4bd8922b9a257">00040</a> BOOL <a class="code" href="class_c_g_z_server_addin_loader_app.php#e4722fe4bf6d24aaeac4bd8922b9a257">CGZServerAddinLoaderApp::InitInstance</a>()
<a name="l00041"></a>00041 {
<a name="l00042"></a>00042         <span class="comment">// InitCommonControls() is required on Windows XP if an application</span>
<a name="l00043"></a>00043         <span class="comment">// manifest specifies use of ComCtl32.dll version 6 or later to enable</span>
<a name="l00044"></a>00044         <span class="comment">// visual styles.  Otherwise, any window creation will fail.</span>
<a name="l00045"></a>00045         InitCommonControls();
<a name="l00046"></a>00046
<a name="l00047"></a>00047         CWinApp::InitInstance();
<a name="l00048"></a>00048
<a name="l00049"></a>00049         AfxEnableControlContainer();
<a name="l00050"></a>00050
<a name="l00051"></a>00051         <span class="comment">// Standard initialization</span>
<a name="l00052"></a>00052         <span class="comment">// If you are not using these features and wish to reduce the size</span>
<a name="l00053"></a>00053         <span class="comment">// of your final executable, you should remove from the following</span>
<a name="l00054"></a>00054         <span class="comment">// the specific initialization routines you do not need</span>
<a name="l00055"></a>00055         <span class="comment">// Change the registry key under which our settings are stored</span>
<a name="l00056"></a>00056         <span class="comment">// TODO: You should modify this string to be something appropriate</span>
<a name="l00057"></a>00057         <span class="comment">// such as the name of your company or organization</span>
<a name="l00058"></a>00058         SetRegistryKey(_T(<span class="stringliteral">"Guard Zone"</span>));
<a name="l00059"></a>00059
<a name="l00060"></a>00060   CoInitialize(0);
<a name="l00061"></a>00061   <span class="keywordflow">try</span>
<a name="l00062"></a>00062   {
<a name="l00063"></a>00063     <a class="code" href="class_c_server_config_doc.php">CServerConfigDoc</a> doc;
<a name="l00064"></a>00064     <a class="code" href="classsc_1_1_c_config_type.php">sc::CConfigType</a> config=doc.Load(<span class="stringliteral">"GZServerConfig.config"</span>);
<a name="l00065"></a>00065
<a name="l00066"></a>00066     <a class="code" href="_altova_8h.php#e5d59c80151fb6bca8c486086a1ed05b">tstring</a> path=config.<a class="code" href="classsc_1_1_c_config_type.php#9ead39abe504e604b4e993a698fd3824">GetActiveServer</a>();
<a name="l00067"></a>00067     CFlukeMaster::flukemaster_instant(path.c_str(),<span class="stringliteral">""</span>,(<a class="code" href="log_8cpp.php#5237486e38d18105bd744c3bc60140bc">GetModuleRunDir</a>()+<span class="stringliteral">"ServerAddin.dll"</span>).c_str());
<a name="l00068"></a>00068   }<span class="keywordflow">catch</span>(std::exception &amp;e)
<a name="l00069"></a>00069   {
<a name="l00070"></a>00070     AfxMessageBox(e.what());
<a name="l00071"></a>00071   }<span class="keywordflow">catch</span>(CXmlException &amp;e)
<a name="l00072"></a>00072   {
<a name="l00073"></a>00073     AfxMessageBox(e.GetInfo().c_str());
<a name="l00074"></a>00074   }
<a name="l00075"></a>00075
<a name="l00076"></a>00076         <span class="comment">// Since the dialog has been closed, return FALSE so that we exit the</span>
<a name="l00077"></a>00077         <span class="comment">//  application, rather than start the application's message pump.</span>
<a name="l00078"></a>00078         <span class="keywordflow">return</span> FALSE;
<a name="l00079"></a>00079 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
