<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ea6c3855b402b97c9537b801f064bee.php">GZone</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_261848f3f32d278f15a1eb75313f8c6c.php">modules</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_7a2a1bb64dd94cb341d79ebcc8f15747.php">CIQSamplev6</a></div>
<h1>CIQSampleUnit.cpp</h1><a href="_c_i_q_sample_unit_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// CIQSampleUnit.cpp : implementation file</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="preprocessor">#include "<a class="code" href="_g_z_2_g_zone_2modules_2_c_i_q_samplev6_2_std_afx_8h.php">stdafx.h</a>"</span>
<a name="l00005"></a>00005 <span class="preprocessor">#include "<a class="code" href="_c_i_q_sample_8h.php">CIQSample.h</a>"</span>
<a name="l00006"></a>00006 <span class="preprocessor">#include "<a class="code" href="_c_i_q_sample_unit_8h.php">CIQSampleUnit.h</a>"</span>
<a name="l00007"></a>00007 <span class="preprocessor">#include "<a class="code" href="_c_i_q_sample_form_8h.php">ciqsampleform.h</a>"</span>
<a name="l00008"></a>00008
<a name="l00009"></a>00009 <span class="preprocessor">#ifdef _DEBUG</span>
<a name="l00010"></a>00010 <span class="preprocessor"></span><span class="preprocessor">#define new DEBUG_NEW</span>
<a name="l00011"></a>00011 <span class="preprocessor"></span><span class="preprocessor">#undef THIS_FILE</span>
<a name="l00012"></a>00012 <span class="preprocessor"></span><span class="keyword">static</span> <span class="keywordtype">char</span> THIS_FILE[] = __FILE__;
<a name="l00013"></a>00013 <span class="preprocessor">#endif</span>
<a name="l00014"></a>00014 <span class="preprocessor"></span>
<a name="l00016"></a>00016 <span class="comment">// CCIQSampleUnit</span>
<a name="l00017"></a>00017
<a name="l00018"></a>00018 IMPLEMENT_DYNCREATE(<a class="code" href="class_c_c_i_q_sample_unit.php">CCIQSampleUnit</a>, CWinThread)
<a name="l00019"></a>00019
<a name="l00020"></a><a class="code" href="class_c_c_i_q_sample_unit.php#f57f36034ad499b469a72a84b863ad26">00020</a> <a class="code" href="class_c_c_i_q_sample_unit.php">CCIQSampleUnit</a>::<a class="code" href="class_c_c_i_q_sample_unit.php">CCIQSampleUnit</a>()
<a name="l00021"></a>00021 {
<a name="l00022"></a>00022 }
<a name="l00023"></a>00023
<a name="l00024"></a><a class="code" href="class_c_c_i_q_sample_unit.php#dd985567d6623a9e4c8193339455b0d3">00024</a> <a class="code" href="class_c_c_i_q_sample_unit.php#dd985567d6623a9e4c8193339455b0d3">CCIQSampleUnit::~CCIQSampleUnit</a>()
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 }
<a name="l00027"></a>00027
<a name="l00028"></a><a class="code" href="class_c_c_i_q_sample_unit.php#6f62963bb8294abe32674b0570ed714a">00028</a> BOOL <a class="code" href="class_c_c_i_q_sample_unit.php#6f62963bb8294abe32674b0570ed714a">CCIQSampleUnit::InitInstance</a>()
<a name="l00029"></a>00029 {
<a name="l00030"></a>00030         <span class="comment">// TODO:  perform and per-thread initialization here</span>
<a name="l00031"></a>00031         <span class="keywordflow">return</span> TRUE;
<a name="l00032"></a>00032 }
<a name="l00033"></a>00033
<a name="l00034"></a><a class="code" href="class_c_c_i_q_sample_unit.php#a543809b4a00876a135258d5b87e8367">00034</a> <span class="keywordtype">int</span> <a class="code" href="class_c_c_i_q_sample_unit.php#a543809b4a00876a135258d5b87e8367">CCIQSampleUnit::ExitInstance</a>()
<a name="l00035"></a>00035 {
<a name="l00036"></a>00036         <span class="comment">// TODO:  perform any per-thread cleanup here</span>
<a name="l00037"></a>00037         <span class="keywordflow">return</span> CWinThread::ExitInstance();
<a name="l00038"></a>00038 }
<a name="l00039"></a>00039
<a name="l00040"></a>00040 BEGIN_MESSAGE_MAP(<a class="code" href="class_c_c_i_q_sample_unit.php">CCIQSampleUnit</a>, CWinThread)
<a name="l00041"></a>00041   ON_THREAD_MESSAGE(msgStart,OnStart)
<a name="l00042"></a>00042         <span class="comment">//{{AFX_MSG_MAP(CCIQSampleUnit)</span>
<a name="l00043"></a>00043                 <span class="comment">// NOTE - the ClassWizard will add and remove mapping macros here.</span>
<a name="l00044"></a>00044         <span class="comment">//}}AFX_MSG_MAP</span>
<a name="l00045"></a>00045 END_MESSAGE_MAP()
<a name="l00046"></a>00046
<a name="l00048"></a>00048 <span class="comment">// CCIQSampleUnit message handlers</span>
<a name="l00049"></a>00049
<a name="l00050"></a><a class="code" href="class_c_c_i_q_sample_unit.php#34f5123c20c865d4bf3d15e0357a0eca">00050</a> <span class="keywordtype">void</span> <a class="code" href="class_c_c_i_q_sample_unit.php">CCIQSampleUnit</a>::OnStart()
<a name="l00051"></a>00051 {
<a name="l00052"></a>00052   CPoint point;
<a name="l00053"></a>00053   CString speech;
<a name="l00054"></a>00054   <span class="keywordflow">if</span>(!m_main-&gt;GetNextTarget(point,speech))
<a name="l00055"></a>00055     <span class="keywordflow">return</span>;
<a name="l00056"></a>00056
<a name="l00057"></a>00057   <span class="keywordflow">try</span>
<a name="l00058"></a>00058   {
<a name="l00059"></a>00059     m_main-&gt;GetCommands().CharMoveTo(point.x,point.y);
<a name="l00060"></a>00060     m_main-&gt;GetCommands().CharSpeech(speech);
<a name="l00061"></a>00061     Sleep(5000);
<a name="l00062"></a>00062     <span class="keywordflow">goto</span> getnexttarget;
<a name="l00063"></a>00063   }<span class="keywordflow">catch</span>(<a class="code" href="class_client_game_commands_1_1_local_operation_timeout.php">ClientGameCommands::LocalOperationTimeout</a> &amp;)
<a name="l00064"></a>00064   {
<a name="l00065"></a>00065     <span class="keywordflow">goto</span> getnexttarget;
<a name="l00066"></a>00066   }<span class="keywordflow">catch</span>(std::exception &amp;e)
<a name="l00067"></a>00067   {
<a name="l00068"></a>00068     AfxMessageBox(e.what());
<a name="l00069"></a>00069     <span class="keywordflow">goto</span> stop;
<a name="l00070"></a>00070   }
<a name="l00071"></a>00071
<a name="l00072"></a>00072 stop:
<a name="l00073"></a>00073   <span class="keywordflow">return</span>;
<a name="l00074"></a>00074 getnexttarget:
<a name="l00075"></a>00075   m_main-&gt;PostMessage(<a class="code" href="class_c_c_i_q_sample_form.php#0b3b5e5e86dc35ec0b7d9f64f7cdc7577e69f1261915698d092d5cb4b036bc31">CCIQSampleForm::msgEnd</a>);
<a name="l00076"></a>00076 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
