<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>ShellNofityIcon Class Reference</h1><!-- doxytag: class="ShellNofityIcon" --><code>#include &lt;<a class="el" href="_shell_notify_icon_8h-source.php">ShellNotifyIcon.h</a>&gt;</code>
<p>
<a href="class_shell_nofity_icon-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Types</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">typedef std::vector&lt; <a class="el" href="struct_shell_nofity_icon_1_1_icon.php">Icon</a> &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#71c8716e3a1d78db46455017f949a5b6">Icons</a></td></tr>

<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#b3e687b02292aedec25958d7a8ab81de">OnShellNofityIconCreate</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#d191e9e48103d87ae872bd05b72041c4">OnShellNofityIconDestroy</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#287e86d919ef5fd42192a867c0fdc76c">OnShellNofityIconShowIcons</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#7f2b9deed5332c9c53f06285791fdd14">OnShellNofityIconProcessMessage</a> (int iconnumber, UINT msg)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#a3d555a3be64bbc6a070f340b99efc49">ShellNofityIcon</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#734d4645336723be75d66b72fc9ebc37">~ShellNofityIcon</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#5d7fa86de2b0f9b9ddb24bf2aa56fd00">ShellNofityIconCreate</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#6fe1a0f354ac509a24d91ec05aa7ee50">ShellNofityIconDestroy</a> ()</td></tr>

<tr><td colspan="2"><br><h2>Static Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">static void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#abae668e6d96288d1729d7799413a99b">ShowIcon</a> (const <a class="el" href="class_shell_nofity_icon.php#71c8716e3a1d78db46455017f949a5b6">Icons</a> &amp;)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">static void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#efd41806965c605d5fe4362ea1ce244a">HideIcon</a> (const <a class="el" href="class_shell_nofity_icon.php#71c8716e3a1d78db46455017f949a5b6">Icons</a> &amp;)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">static void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#e4f0df816dfc3a9dc6005f05935d6aa6">UpdateIcon</a> (const <a class="el" href="class_shell_nofity_icon.php#71c8716e3a1d78db46455017f949a5b6">Icons</a> &amp;)</td></tr>

<tr><td colspan="2"><br><h2>Protected Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#d5b31a5b77ee44a5cdca5c8d2f53e4d5">END_MSG_MAP</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">LRESULT&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#274cb7fc12e3ed278cfc499f49a03815">OnCreate</a> (UINT uMsg, WPARAM <a class="el" href="flukeslave_8h.php#2c28a497a9b541541d5b8713a639a31b">wParam</a>, LPARAM <a class="el" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a>, BOOL &amp;bHandled)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">LRESULT&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#76ffd6571454262d1906346fe3b41bed">OnDestory</a> (UINT uMsg, WPARAM <a class="el" href="flukeslave_8h.php#2c28a497a9b541541d5b8713a639a31b">wParam</a>, LPARAM <a class="el" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a>, BOOL &amp;bHandled)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">LRESULT&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#29f0ed8fffef5feca027e825746e9ea0">OnTaskBarCreated</a> (UINT uMsg, WPARAM <a class="el" href="flukeslave_8h.php#2c28a497a9b541541d5b8713a639a31b">wParam</a>, LPARAM <a class="el" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a>, BOOL &amp;bHandled)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">LRESULT&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#5a66a746830e3d8b004aab23e6ffe653">OnIconMessage</a> (UINT uMsg, WPARAM <a class="el" href="flukeslave_8h.php#2c28a497a9b541541d5b8713a639a31b">wParam</a>, LPARAM <a class="el" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a>, BOOL &amp;bHandled)</td></tr>

<tr><td colspan="2"><br><h2>Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">static const UINT&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_shell_nofity_icon.php#8c6754c9a5a0c7d83ac96d246f2d4f89">WM_TASKBARCREATED</a></td></tr>

<tr><td colspan="2"><br><h2>Classes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">struct &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_shell_nofity_icon_1_1_icon.php">Icon</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8h-source.php#l00028">28</a> of file <a class="el" href="_shell_notify_icon_8h-source.php">ShellNotifyIcon.h</a>.<hr><h2>Member Typedef Documentation</h2>
<a class="anchor" name="71c8716e3a1d78db46455017f949a5b6"></a><!-- doxytag: member="ShellNofityIcon::Icons" ref="71c8716e3a1d78db46455017f949a5b6" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">typedef std::vector&lt;<a class="el" href="struct_shell_nofity_icon_1_1_icon.php">Icon</a>&gt; <a class="el" href="class_shell_nofity_icon.php#71c8716e3a1d78db46455017f949a5b6">ShellNofityIcon::Icons</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8h-source.php#l00090">90</a> of file <a class="el" href="_shell_notify_icon_8h-source.php">ShellNotifyIcon.h</a>.
</div>
</div><p>
<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="a3d555a3be64bbc6a070f340b99efc49"></a><!-- doxytag: member="ShellNofityIcon::ShellNofityIcon" ref="a3d555a3be64bbc6a070f340b99efc49" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">ShellNofityIcon::ShellNofityIcon           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8cpp-source.php#l00079">79</a> of file <a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00080"></a>00080 {
<a name="l00081"></a>00081 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="734d4645336723be75d66b72fc9ebc37"></a><!-- doxytag: member="ShellNofityIcon::~ShellNofityIcon" ref="734d4645336723be75d66b72fc9ebc37" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">ShellNofityIcon::~ShellNofityIcon           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8cpp-source.php#l00088">88</a> of file <a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00089"></a>00089 {
<a name="l00090"></a>00090 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="abae668e6d96288d1729d7799413a99b"></a><!-- doxytag: member="ShellNofityIcon::ShowIcon" ref="abae668e6d96288d1729d7799413a99b" args="(const Icons &amp;)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ShellNofityIcon::ShowIcon           </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_shell_nofity_icon.php#71c8716e3a1d78db46455017f949a5b6">Icons</a> &amp;&nbsp;</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8cpp-source.php#l00023">23</a> of file <a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a>.
<p>
References <a class="el" href="_shell_notify_icon_8cpp-source.php#l00035">HideIcon()</a>.
<p>
Referenced by <a class="el" href="_debugger_common_8cpp-source.php#l00041">Debug::Debug()</a>.<div class="fragment"><pre class="fragment"><a name="l00024"></a>00024 {
<a name="l00025"></a>00025   <a class="code" href="class_shell_nofity_icon.php#efd41806965c605d5fe4362ea1ce244a">HideIcon</a>(ii);
<a name="l00026"></a>00026
<a name="l00027"></a>00027   <span class="keywordflow">for</span>(Icons::const_iterator i=ii.begin();i!=ii.end();i++)
<a name="l00028"></a>00028   {
<a name="l00029"></a>00029     NOTIFYICONDATA nid=*i;
<a name="l00030"></a>00030     nid.uID=std::distance(ii.begin(),i);
<a name="l00031"></a>00031     Shell_NotifyIcon(NIM_ADD,&amp;nid);
<a name="l00032"></a>00032   }
<a name="l00033"></a>00033 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="efd41806965c605d5fe4362ea1ce244a"></a><!-- doxytag: member="ShellNofityIcon::HideIcon" ref="efd41806965c605d5fe4362ea1ce244a" args="(const Icons &amp;)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ShellNofityIcon::HideIcon           </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_shell_nofity_icon.php#71c8716e3a1d78db46455017f949a5b6">Icons</a> &amp;&nbsp;</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8cpp-source.php#l00035">35</a> of file <a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a>.
<p>
Referenced by <a class="el" href="_debugger_common_8cpp-source.php#l00074">Debug::OnFinalMessage()</a>, and <a class="el" href="_shell_notify_icon_8cpp-source.php#l00023">ShowIcon()</a>.<div class="fragment"><pre class="fragment"><a name="l00036"></a>00036 {
<a name="l00037"></a>00037   <span class="keywordflow">for</span>(Icons::const_iterator i=ii.begin();i!=ii.end();i++)
<a name="l00038"></a>00038   {
<a name="l00039"></a>00039     NOTIFYICONDATA nid={0};
<a name="l00040"></a>00040     nid.hWnd=i-&gt;mainwnd;
<a name="l00041"></a>00041     nid.cbSize=<span class="keyword">sizeof</span>(nid);
<a name="l00042"></a>00042     nid.uID=std::distance(ii.begin(),i);
<a name="l00043"></a>00043     Shell_NotifyIcon(NIM_DELETE,&amp;nid);
<a name="l00044"></a>00044   }
<a name="l00045"></a>00045 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="e4f0df816dfc3a9dc6005f05935d6aa6"></a><!-- doxytag: member="ShellNofityIcon::UpdateIcon" ref="e4f0df816dfc3a9dc6005f05935d6aa6" args="(const Icons &amp;)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ShellNofityIcon::UpdateIcon           </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_shell_nofity_icon.php#71c8716e3a1d78db46455017f949a5b6">Icons</a> &amp;&nbsp;</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8cpp-source.php#l00047">47</a> of file <a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00048"></a>00048 {
<a name="l00049"></a>00049   <span class="keywordflow">for</span>(Icons::const_iterator i=ii.begin();i!=ii.end();i++)
<a name="l00050"></a>00050   {
<a name="l00051"></a>00051     NOTIFYICONDATA nid=*i;
<a name="l00052"></a>00052     nid.uID=std::distance(ii.begin(),i);
<a name="l00053"></a>00053     Shell_NotifyIcon(NIM_MODIFY,&amp;nid);
<a name="l00054"></a>00054   }
<a name="l00055"></a>00055 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="b3e687b02292aedec25958d7a8ab81de"></a><!-- doxytag: member="ShellNofityIcon::OnShellNofityIconCreate" ref="b3e687b02292aedec25958d7a8ab81de" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual void ShellNofityIcon::OnShellNofityIconCreate           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8h-source.php#l00098">98</a> of file <a class="el" href="_shell_notify_icon_8h-source.php">ShellNotifyIcon.h</a>.
<p>
Referenced by <a class="el" href="_shell_notify_icon_8cpp-source.php#l00057">OnCreate()</a>.<div class="fragment"><pre class="fragment"><a name="l00098"></a>00098 {};
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="d191e9e48103d87ae872bd05b72041c4"></a><!-- doxytag: member="ShellNofityIcon::OnShellNofityIconDestroy" ref="d191e9e48103d87ae872bd05b72041c4" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual void ShellNofityIcon::OnShellNofityIconDestroy           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8h-source.php#l00099">99</a> of file <a class="el" href="_shell_notify_icon_8h-source.php">ShellNotifyIcon.h</a>.
<p>
Referenced by <a class="el" href="_shell_notify_icon_8cpp-source.php#l00100">OnDestory()</a>.<div class="fragment"><pre class="fragment"><a name="l00099"></a>00099 {};
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="287e86d919ef5fd42192a867c0fdc76c"></a><!-- doxytag: member="ShellNofityIcon::OnShellNofityIconShowIcons" ref="287e86d919ef5fd42192a867c0fdc76c" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual void ShellNofityIcon::OnShellNofityIconShowIcons           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8h-source.php#l00100">100</a> of file <a class="el" href="_shell_notify_icon_8h-source.php">ShellNotifyIcon.h</a>.
<p>
Referenced by <a class="el" href="_shell_notify_icon_8cpp-source.php#l00057">OnCreate()</a>, and <a class="el" href="_shell_notify_icon_8cpp-source.php#l00065">OnTaskBarCreated()</a>.<div class="fragment"><pre class="fragment"><a name="l00100"></a>00100 {};
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="7f2b9deed5332c9c53f06285791fdd14"></a><!-- doxytag: member="ShellNofityIcon::OnShellNofityIconProcessMessage" ref="7f2b9deed5332c9c53f06285791fdd14" args="(int iconnumber, UINT msg)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">virtual void ShellNofityIcon::OnShellNofityIconProcessMessage           </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>iconnumber</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">UINT&nbsp;</td>
          <td class="paramname"> <em>msg</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [inline, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8h-source.php#l00101">101</a> of file <a class="el" href="_shell_notify_icon_8h-source.php">ShellNotifyIcon.h</a>.
<p>
Referenced by <a class="el" href="_shell_notify_icon_8cpp-source.php#l00072">OnIconMessage()</a>.<div class="fragment"><pre class="fragment"><a name="l00101"></a>00101 {};
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="5d7fa86de2b0f9b9ddb24bf2aa56fd00"></a><!-- doxytag: member="ShellNofityIcon::ShellNofityIconCreate" ref="5d7fa86de2b0f9b9ddb24bf2aa56fd00" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ShellNofityIcon::ShellNofityIconCreate           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8cpp-source.php#l00083">83</a> of file <a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00084"></a>00084 {
<a name="l00085"></a>00085   <a class="code" href="class_game_map_manager.php#71c379c5e9ab576c22e568821ec4673a">Create</a>(GetDesktopWindow(),rcDefault,0,WS_OVERLAPPEDWINDOW);
<a name="l00086"></a>00086 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="6fe1a0f354ac509a24d91ec05aa7ee50"></a><!-- doxytag: member="ShellNofityIcon::ShellNofityIconDestroy" ref="6fe1a0f354ac509a24d91ec05aa7ee50" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void ShellNofityIcon::ShellNofityIconDestroy           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8cpp-source.php#l00092">92</a> of file <a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00093"></a>00093 {
<a name="l00094"></a>00094   <span class="keywordflow">if</span>(IsWindow())
<a name="l00095"></a>00095   {
<a name="l00096"></a>00096     DestroyWindow();
<a name="l00097"></a>00097   }
<a name="l00098"></a>00098 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="d5b31a5b77ee44a5cdca5c8d2f53e4d5"></a><!-- doxytag: member="ShellNofityIcon::END_MSG_MAP" ref="d5b31a5b77ee44a5cdca5c8d2f53e4d5" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">ShellNofityIcon::END_MSG_MAP           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<a class="anchor" name="274cb7fc12e3ed278cfc499f49a03815"></a><!-- doxytag: member="ShellNofityIcon::OnCreate" ref="274cb7fc12e3ed278cfc499f49a03815" args="(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &amp;bHandled)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">LRESULT ShellNofityIcon::OnCreate           </td>
          <td>(</td>
          <td class="paramtype">UINT&nbsp;</td>
          <td class="paramname"> <em>uMsg</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">WPARAM&nbsp;</td>
          <td class="paramname"> <em>wParam</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">LPARAM&nbsp;</td>
          <td class="paramname"> <em>lParam</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">BOOL &amp;&nbsp;</td>
          <td class="paramname"> <em>bHandled</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8cpp-source.php#l00057">57</a> of file <a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a>.
<p>
References <a class="el" href="_shell_notify_icon_8h-source.php#l00098">OnShellNofityIconCreate()</a>, and <a class="el" href="_shell_notify_icon_8h-source.php#l00100">OnShellNofityIconShowIcons()</a>.<div class="fragment"><pre class="fragment"><a name="l00058"></a>00058 {
<a name="l00059"></a>00059   <a class="code" href="class_shell_nofity_icon.php#b3e687b02292aedec25958d7a8ab81de">OnShellNofityIconCreate</a>();
<a name="l00060"></a>00060   <a class="code" href="class_shell_nofity_icon.php#287e86d919ef5fd42192a867c0fdc76c">OnShellNofityIconShowIcons</a>();
<a name="l00061"></a>00061
<a name="l00062"></a>00062   <span class="keywordflow">return</span> 0;
<a name="l00063"></a>00063 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="76ffd6571454262d1906346fe3b41bed"></a><!-- doxytag: member="ShellNofityIcon::OnDestory" ref="76ffd6571454262d1906346fe3b41bed" args="(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &amp;bHandled)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">LRESULT ShellNofityIcon::OnDestory           </td>
          <td>(</td>
          <td class="paramtype">UINT&nbsp;</td>
          <td class="paramname"> <em>uMsg</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">WPARAM&nbsp;</td>
          <td class="paramname"> <em>wParam</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">LPARAM&nbsp;</td>
          <td class="paramname"> <em>lParam</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">BOOL &amp;&nbsp;</td>
          <td class="paramname"> <em>bHandled</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8cpp-source.php#l00100">100</a> of file <a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a>.
<p>
References <a class="el" href="_shell_notify_icon_8h-source.php#l00099">OnShellNofityIconDestroy()</a>.<div class="fragment"><pre class="fragment"><a name="l00101"></a>00101 {
<a name="l00102"></a>00102   <a class="code" href="class_shell_nofity_icon.php#d191e9e48103d87ae872bd05b72041c4">OnShellNofityIconDestroy</a>();
<a name="l00103"></a>00103
<a name="l00104"></a>00104   <span class="keywordflow">return</span> 0;
<a name="l00105"></a>00105 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="29f0ed8fffef5feca027e825746e9ea0"></a><!-- doxytag: member="ShellNofityIcon::OnTaskBarCreated" ref="29f0ed8fffef5feca027e825746e9ea0" args="(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &amp;bHandled)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">LRESULT ShellNofityIcon::OnTaskBarCreated           </td>
          <td>(</td>
          <td class="paramtype">UINT&nbsp;</td>
          <td class="paramname"> <em>uMsg</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">WPARAM&nbsp;</td>
          <td class="paramname"> <em>wParam</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">LPARAM&nbsp;</td>
          <td class="paramname"> <em>lParam</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">BOOL &amp;&nbsp;</td>
          <td class="paramname"> <em>bHandled</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8cpp-source.php#l00065">65</a> of file <a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a>.
<p>
References <a class="el" href="_shell_notify_icon_8h-source.php#l00100">OnShellNofityIconShowIcons()</a>.<div class="fragment"><pre class="fragment"><a name="l00066"></a>00066 {
<a name="l00067"></a>00067   <a class="code" href="class_shell_nofity_icon.php#287e86d919ef5fd42192a867c0fdc76c">OnShellNofityIconShowIcons</a>();
<a name="l00068"></a>00068
<a name="l00069"></a>00069   <span class="keywordflow">return</span> 0;
<a name="l00070"></a>00070 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="5a66a746830e3d8b004aab23e6ffe653"></a><!-- doxytag: member="ShellNofityIcon::OnIconMessage" ref="5a66a746830e3d8b004aab23e6ffe653" args="(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL &amp;bHandled)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">LRESULT ShellNofityIcon::OnIconMessage           </td>
          <td>(</td>
          <td class="paramtype">UINT&nbsp;</td>
          <td class="paramname"> <em>uMsg</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">WPARAM&nbsp;</td>
          <td class="paramname"> <em>wParam</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">LPARAM&nbsp;</td>
          <td class="paramname"> <em>lParam</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">BOOL &amp;&nbsp;</td>
          <td class="paramname"> <em>bHandled</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8cpp-source.php#l00072">72</a> of file <a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a>.
<p>
References <a class="el" href="_shell_notify_icon_8h-source.php#l00101">OnShellNofityIconProcessMessage()</a>.<div class="fragment"><pre class="fragment"><a name="l00073"></a>00073 {
<a name="l00074"></a>00074   <a class="code" href="class_shell_nofity_icon.php#7f2b9deed5332c9c53f06285791fdd14">OnShellNofityIconProcessMessage</a>(<a class="code" href="flukeslave_8cpp.php#2c28a497a9b541541d5b8713a639a31b">wParam</a>,<a class="code" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a>);
<a name="l00075"></a>00075
<a name="l00076"></a>00076   <span class="keywordflow">return</span> 0;
<a name="l00077"></a>00077 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="8c6754c9a5a0c7d83ac96d246f2d4f89"></a><!-- doxytag: member="ShellNofityIcon::WM_TASKBARCREATED" ref="8c6754c9a5a0c7d83ac96d246f2d4f89" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">const UINT <a class="el" href="class_shell_nofity_icon.php#8c6754c9a5a0c7d83ac96d246f2d4f89">ShellNofityIcon::WM_TASKBARCREATED</a><code> [static, protected]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_shell_notify_icon_8h-source.php#l00111">111</a> of file <a class="el" href="_shell_notify_icon_8h-source.php">ShellNotifyIcon.h</a>.
</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="_shell_notify_icon_8h-source.php">ShellNotifyIcon.h</a><li><a class="el" href="_shell_notify_icon_8cpp-source.php">ShellNotifyIcon.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
