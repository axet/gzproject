<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespace_schema_server_addin.php">SchemaServerAddin</a>::<a class="el" href="class_schema_server_addin_1_1_respond_type.php">RespondType</a></div>
<h1>SchemaServerAddin::RespondType Class Reference</h1><!-- doxytag: class="SchemaServerAddin::RespondType" --><!-- doxytag: inherits="Altova::Node" --><p>Inheritance diagram for SchemaServerAddin::RespondType:
<p><center><img src="class_schema_server_addin_1_1_respond_type.png" usemap="#SchemaServerAddin::RespondType_map" border="0" alt=""></center>
<map name="SchemaServerAddin::RespondType_map">
<area href="class_altova_1_1_node.php" alt="Altova::Node" shape="rect" coords="0,0,212,24">
</map>
<a href="class_schema_server_addin_1_1_respond_type-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#975f276dd13a8312a029c6f4a648b20c">RespondType</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#db5fbd3fe6502e9f8694093b74919d47">RespondType</a> (XmlDocument doc)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#477c24f144f81fdd446700880d61f55e">RespondType</a> (XmlNode node)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#8b63728632c3ae0c23273d1b26d39c19">RespondType</a> (<a class="el" href="class_altova_1_1_node.php">Altova.Node</a> node)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#fac3ab64826d1658de8a90b250f897a0">GetDataMinCount</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#486b1ec140f7e62d708c9eacc5c4975b">GetDataMaxCount</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#e66398b027653f2ff0929bd54ac3e186">GetDataCount</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#81e278b90c3c63dc6d2e5a2dab5e0b51">HasData</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#3aee27f0d23d5c89d5bec33fead188ea">GetDataAt</a> (int index)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#cea2ade89bb61ff64a25cc3a1187d11b">GetData</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#f20494fccd0d76326190b2c7dcb21270">RemoveDataAt</a> (int index)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#d228e8e337a99401e184a842e6bf0cf4">RemoveData</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#c15fa65fdfba7348289ac3f33a8aee37">AddData</a> (<a class="el" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a> newValue)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#cc8f78f3a95dc9c4e639e547a8432217">InsertDataAt</a> (<a class="el" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a> newValue, int index)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_schema_server_addin_1_1_respond_type.php#8374c933ae541a4ccebf631439c1748b">ReplaceDataAt</a> (<a class="el" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a> newValue, int index)</td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00020">20</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="975f276dd13a8312a029c6f4a648b20c"></a><!-- doxytag: member="SchemaServerAddin::RespondType::RespondType" ref="975f276dd13a8312a029c6f4a648b20c" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">SchemaServerAddin::RespondType::RespondType           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00023">23</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.<div class="fragment"><pre class="fragment"><a name="l00023"></a>00023 : base() {}
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="db5fbd3fe6502e9f8694093b74919d47"></a><!-- doxytag: member="SchemaServerAddin::RespondType::RespondType" ref="db5fbd3fe6502e9f8694093b74919d47" args="(XmlDocument doc)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">SchemaServerAddin::RespondType::RespondType           </td>
          <td>(</td>
          <td class="paramtype">XmlDocument&nbsp;</td>
          <td class="paramname"> <em>doc</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00024">24</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.<div class="fragment"><pre class="fragment"><a name="l00024"></a>00024 : base(doc) {}
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="477c24f144f81fdd446700880d61f55e"></a><!-- doxytag: member="SchemaServerAddin::RespondType::RespondType" ref="477c24f144f81fdd446700880d61f55e" args="(XmlNode node)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">SchemaServerAddin::RespondType::RespondType           </td>
          <td>(</td>
          <td class="paramtype">XmlNode&nbsp;</td>
          <td class="paramname"> <em>node</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00025">25</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.<div class="fragment"><pre class="fragment"><a name="l00025"></a>00025 : base(node) {}
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="8b63728632c3ae0c23273d1b26d39c19"></a><!-- doxytag: member="SchemaServerAddin::RespondType::RespondType" ref="8b63728632c3ae0c23273d1b26d39c19" args="(Altova.Node node)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">SchemaServerAddin::RespondType::RespondType           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_altova_1_1_node.php">Altova.Node</a>&nbsp;</td>
          <td class="paramname"> <em>node</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00026">26</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.<div class="fragment"><pre class="fragment"><a name="l00026"></a>00026 : base(node) {}
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="fac3ab64826d1658de8a90b250f897a0"></a><!-- doxytag: member="SchemaServerAddin::RespondType::GetDataMinCount" ref="fac3ab64826d1658de8a90b250f897a0" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int SchemaServerAddin::RespondType::GetDataMinCount           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00030">30</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.<div class="fragment"><pre class="fragment"><a name="l00031"></a>00031                 {
<a name="l00032"></a>00032                         <span class="keywordflow">return</span> 1;
<a name="l00033"></a>00033                 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="486b1ec140f7e62d708c9eacc5c4975b"></a><!-- doxytag: member="SchemaServerAddin::RespondType::GetDataMaxCount" ref="486b1ec140f7e62d708c9eacc5c4975b" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int SchemaServerAddin::RespondType::GetDataMaxCount           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00035">35</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.<div class="fragment"><pre class="fragment"><a name="l00036"></a>00036                 {
<a name="l00037"></a>00037                         <span class="keywordflow">return</span> 1;
<a name="l00038"></a>00038                 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="e66398b027653f2ff0929bd54ac3e186"></a><!-- doxytag: member="SchemaServerAddin::RespondType::GetDataCount" ref="e66398b027653f2ff0929bd54ac3e186" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int SchemaServerAddin::RespondType::GetDataCount           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00040">40</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.
<p>
References <a class="el" href="_node_8cs-source.php#l00135">Altova::Node::DomChildCount()</a>.<div class="fragment"><pre class="fragment"><a name="l00041"></a>00041                 {
<a name="l00042"></a>00042                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#5a78f4a7e638948db52a50bf27f97277">DomChildCount</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Data"</span>);
<a name="l00043"></a>00043                 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="81e278b90c3c63dc6d2e5a2dab5e0b51"></a><!-- doxytag: member="SchemaServerAddin::RespondType::HasData" ref="81e278b90c3c63dc6d2e5a2dab5e0b51" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">bool SchemaServerAddin::RespondType::HasData           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00045">45</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.
<p>
References <a class="el" href="_node_8cs-source.php#l00117">Altova::Node::HasDomChild()</a>.
<p>
Referenced by <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00065">RemoveData()</a>.<div class="fragment"><pre class="fragment"><a name="l00046"></a>00046                 {
<a name="l00047"></a>00047                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#22caf29f497e86a36ca976c5b9344d11">HasDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Data"</span>);
<a name="l00048"></a>00048                 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="3aee27f0d23d5c89d5bec33fead188ea"></a><!-- doxytag: member="SchemaServerAddin::RespondType::GetDataAt" ref="3aee27f0d23d5c89d5bec33fead188ea" args="(int index)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a> SchemaServerAddin::RespondType::GetDataAt           </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>index</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00050">50</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.
<p>
References <a class="el" href="_node_8cs-source.php#l00199">Altova::Node::GetDomChildAt()</a>, and <a class="el" href="_node_8cs-source.php#l00049">Altova::Node::GetDomNodeValue()</a>.
<p>
Referenced by <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00055">GetData()</a>.<div class="fragment"><pre class="fragment"><a name="l00051"></a>00051                 {
<a name="l00052"></a>00052                         <span class="keywordflow">return</span> <span class="keyword">new</span> <a class="code" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a>(<a class="code" href="class_altova_1_1_node.php#3e9877a06a6e9021aecc6cf026c34f7a">GetDomNodeValue</a>(<a class="code" href="class_altova_1_1_node.php#30b0b16f6517d59c5415e7e7d8f83959">GetDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Data"</span>, index)));
<a name="l00053"></a>00053                 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="cea2ade89bb61ff64a25cc3a1187d11b"></a><!-- doxytag: member="SchemaServerAddin::RespondType::GetData" ref="cea2ade89bb61ff64a25cc3a1187d11b" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a> SchemaServerAddin::RespondType::GetData           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00055">55</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.
<p>
References <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00050">GetDataAt()</a>.
<p>
Referenced by <a class="el" href="_server_addin_i_o_8cs-source.php#l00038">ServerAddinIO::Socket::ModuleCommand()</a>.<div class="fragment"><pre class="fragment"><a name="l00056"></a>00056                 {
<a name="l00057"></a>00057                         <span class="keywordflow">return</span> <a class="code" href="class_schema_server_addin_1_1_respond_type.php#3aee27f0d23d5c89d5bec33fead188ea">GetDataAt</a>(0);
<a name="l00058"></a>00058                 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="f20494fccd0d76326190b2c7dcb21270"></a><!-- doxytag: member="SchemaServerAddin::RespondType::RemoveDataAt" ref="f20494fccd0d76326190b2c7dcb21270" args="(int index)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void SchemaServerAddin::RespondType::RemoveDataAt           </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>index</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00060">60</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.
<p>
References <a class="el" href="_node_8cs-source.php#l00246">Altova::Node::RemoveDomChildAt()</a>.
<p>
Referenced by <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00065">RemoveData()</a>.<div class="fragment"><pre class="fragment"><a name="l00061"></a>00061                 {
<a name="l00062"></a>00062                         <a class="code" href="class_altova_1_1_node.php#7938fa304858dfc0e9f8f492c9a65ecb">RemoveDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Data"</span>, index);
<a name="l00063"></a>00063                 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="d228e8e337a99401e184a842e6bf0cf4"></a><!-- doxytag: member="SchemaServerAddin::RespondType::RemoveData" ref="d228e8e337a99401e184a842e6bf0cf4" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void SchemaServerAddin::RespondType::RemoveData           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00065">65</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.
<p>
References <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00045">HasData()</a>, and <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00060">RemoveDataAt()</a>.<div class="fragment"><pre class="fragment"><a name="l00066"></a>00066                 {
<a name="l00067"></a>00067                         <span class="keywordflow">while</span> (<a class="code" href="class_schema_server_addin_1_1_respond_type.php#81e278b90c3c63dc6d2e5a2dab5e0b51">HasData</a>())
<a name="l00068"></a>00068                                 <a class="code" href="class_schema_server_addin_1_1_respond_type.php#f20494fccd0d76326190b2c7dcb21270">RemoveDataAt</a>(0);
<a name="l00069"></a>00069                 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="c15fa65fdfba7348289ac3f33a8aee37"></a><!-- doxytag: member="SchemaServerAddin::RespondType::AddData" ref="c15fa65fdfba7348289ac3f33a8aee37" args="(SchemaString newValue)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void SchemaServerAddin::RespondType::AddData           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a>&nbsp;</td>
          <td class="paramname"> <em>newValue</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00071">71</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.
<p>
References <a class="el" href="_node_8cs-source.php#l00154">Altova::Node::AppendDomChild()</a>, and <a class="el" href="_schema_types_8cs-source.php#l00019">Altova::Types::SchemaString::ToString()</a>.<div class="fragment"><pre class="fragment"><a name="l00072"></a>00072                 {
<a name="l00073"></a>00073                         <a class="code" href="class_altova_1_1_node.php#c75ea55d1cdcb9119c14c8e7598d27fd">AppendDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Data"</span>, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_string.php#c3c38a1ea21f766474a649fee88b76bc">ToString</a>());
<a name="l00074"></a>00074                 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="cc8f78f3a95dc9c4e639e547a8432217"></a><!-- doxytag: member="SchemaServerAddin::RespondType::InsertDataAt" ref="cc8f78f3a95dc9c4e639e547a8432217" args="(SchemaString newValue, int index)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void SchemaServerAddin::RespondType::InsertDataAt           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a>&nbsp;</td>
          <td class="paramname"> <em>newValue</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>index</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00076">76</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.
<p>
References <a class="el" href="_node_8cs-source.php#l00178">Altova::Node::InsertDomChildAt()</a>, and <a class="el" href="_schema_types_8cs-source.php#l00019">Altova::Types::SchemaString::ToString()</a>.<div class="fragment"><pre class="fragment"><a name="l00077"></a>00077                 {
<a name="l00078"></a>00078                         <a class="code" href="class_altova_1_1_node.php#04fbd644a61272767ac268a6837e07b8">InsertDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Data"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_string.php#c3c38a1ea21f766474a649fee88b76bc">ToString</a>());
<a name="l00079"></a>00079                 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="8374c933ae541a4ccebf631439c1748b"></a><!-- doxytag: member="SchemaServerAddin::RespondType::ReplaceDataAt" ref="8374c933ae541a4ccebf631439c1748b" args="(SchemaString newValue, int index)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void SchemaServerAddin::RespondType::ReplaceDataAt           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a>&nbsp;</td>
          <td class="paramname"> <em>newValue</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>index</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php#l00081">81</a> of file <a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a>.
<p>
References <a class="el" href="_node_8cs-source.php#l00224">Altova::Node::ReplaceDomChildAt()</a>, and <a class="el" href="_schema_types_8cs-source.php#l00019">Altova::Types::SchemaString::ToString()</a>.<div class="fragment"><pre class="fragment"><a name="l00082"></a>00082                 {
<a name="l00083"></a>00083                         <a class="code" href="class_altova_1_1_node.php#a99e2051ebcba9f999ee5cfa19b6072f">ReplaceDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"Data"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_string.php#c3c38a1ea21f766474a649fee88b76bc">ToString</a>());
<a name="l00084"></a>00084                 }
</pre></div>
<p>

</div>
</div><p>
<hr>The documentation for this class was generated from the following file:<ul>
<li><a class="el" href="_schema_server_addin_2_respond_type_8cs-source.php">SchemaServerAddin/RespondType.cs</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
