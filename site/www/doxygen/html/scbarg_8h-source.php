<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_9aa33d2cd8dd981e9a7ee8bcdb7921b2.php">library</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_c51d32942a5020b834ec79360a5d2930.php">LikeMsdev</a></div>
<h1>scbarg.h</h1><a href="scbarg_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// CSizingControlBarG           Version 2.43</span>
<a name="l00004"></a>00004 <span class="comment">//</span>
<a name="l00005"></a>00005 <span class="comment">// Created: Jan 24, 1998        Last Modified: August 03, 2000</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// See the official site at www.datamekanix.com for documentation and</span>
<a name="l00008"></a>00008 <span class="comment">// the latest news.</span>
<a name="l00009"></a>00009 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment"></span><span class="comment">// Copyright (C) 1998-2000 by Cristi Posea. All rights reserved.</span>
<a name="l00012"></a>00012 <span class="comment">//</span>
<a name="l00013"></a>00013 <span class="comment">// This code is free for personal and commercial use, providing this </span>
<a name="l00014"></a>00014 <span class="comment">// notice remains intact in the source files and all eventual changes are</span>
<a name="l00015"></a>00015 <span class="comment">// clearly marked with comments.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You must obtain the author's consent before you can include this code</span>
<a name="l00018"></a>00018 <span class="comment">// in a software library.</span>
<a name="l00019"></a>00019 <span class="comment">//</span>
<a name="l00020"></a>00020 <span class="comment">// No warrantee of any kind, express or implied, is included with this</span>
<a name="l00021"></a>00021 <span class="comment">// software; use at your own risk, responsibility for damages (if any) to</span>
<a name="l00022"></a>00022 <span class="comment">// anyone resulting from the use of this software rests entirely with the</span>
<a name="l00023"></a>00023 <span class="comment">// user.</span>
<a name="l00024"></a>00024 <span class="comment">//</span>
<a name="l00025"></a>00025 <span class="comment">// Send bug reports, bug fixes, enhancements, requests, flames, etc. to</span>
<a name="l00026"></a>00026 <span class="comment">// cristi@datamekanix.com or post them at the message board at the site.</span>
<a name="l00028"></a>00028 <span class="comment"></span>
<a name="l00029"></a>00029 <span class="preprocessor">#if !defined(__SCBARG_H__)</span>
<a name="l00030"></a><a class="code" href="scbarg_8h.php#e2eca6858980d1d65bea24fdd72c13f5">00030</a> <span class="preprocessor"></span><span class="preprocessor">#define __SCBARG_H__</span>
<a name="l00031"></a>00031 <span class="preprocessor"></span>
<a name="l00032"></a>00032 <span class="preprocessor">#if _MSC_VER &gt;= 1000</span>
<a name="l00033"></a>00033 <span class="preprocessor"></span><span class="preprocessor">#pragma once</span>
<a name="l00034"></a>00034 <span class="preprocessor"></span><span class="preprocessor">#endif // _MSC_VER &gt;= 1000</span>
<a name="l00035"></a>00035 <span class="preprocessor"></span>
<a name="l00037"></a>00037 <span class="comment">// CSCBButton (button info) helper class</span>
<a name="l00038"></a>00038
<a name="l00039"></a>00039 <span class="keyword">namespace </span>LikeMsdev
<a name="l00040"></a>00040 {
<a name="l00041"></a>00041
<a name="l00042"></a><a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php">00042</a>   <span class="keyword">class </span><a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php">CSCBButton</a>
<a name="l00043"></a>00043   {
<a name="l00044"></a>00044   <span class="keyword">public</span>:
<a name="l00045"></a>00045       <a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#5ab7b7555a673f0c56fd890341183d6b">CSCBButton</a>();
<a name="l00046"></a>00046
<a name="l00047"></a><a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#c1943a93d3359d8b997c7f7bfcf7d22d">00047</a>       <span class="keywordtype">void</span> <a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#c1943a93d3359d8b997c7f7bfcf7d22d">Move</a>(CPoint ptTo) {<a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#19e24764f8b577f8a9537b33579c448d">ptOrg</a> = ptTo; };
<a name="l00048"></a><a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#d3d88de0cc100523e9cadbd8a0fcaf73">00048</a>       CRect <a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#d3d88de0cc100523e9cadbd8a0fcaf73">GetRect</a>() { <span class="keywordflow">return</span> CRect(<a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#19e24764f8b577f8a9537b33579c448d">ptOrg</a>, CSize(11, 11)); };
<a name="l00049"></a>00049       <span class="keywordtype">void</span> <a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#1f13a30a52174170494b80f12d22f102">Paint</a>(CDC* pDC);
<a name="l00050"></a>00050
<a name="l00051"></a><a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#91cdf2ae85092247f00ae519aac9eac7">00051</a>       BOOL    <a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#91cdf2ae85092247f00ae519aac9eac7">bPushed</a>;
<a name="l00052"></a><a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#a25ebb11a2821a446a7cfbff5b6304d5">00052</a>       BOOL    <a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#a25ebb11a2821a446a7cfbff5b6304d5">bRaised</a>;
<a name="l00053"></a>00053
<a name="l00054"></a>00054   <span class="keyword">protected</span>:
<a name="l00055"></a><a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#19e24764f8b577f8a9537b33579c448d">00055</a>       CPoint  <a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php#19e24764f8b577f8a9537b33579c448d">ptOrg</a>;
<a name="l00056"></a>00056   };
<a name="l00057"></a>00057
<a name="l00059"></a>00059   <span class="comment">// CSizingControlBar control bar</span>
<a name="l00060"></a>00060
<a name="l00061"></a>00061 <span class="preprocessor">  #ifndef baseCSizingControlBarG</span>
<a name="l00062"></a><a class="code" href="scbarg_8h.php#eeac25004543b642d145a5effbedc7ec">00062</a> <span class="preprocessor"></span><span class="preprocessor">  #define baseCSizingControlBarG CSizingControlBar</span>
<a name="l00063"></a>00063 <span class="preprocessor"></span><span class="preprocessor">  #endif</span>
<a name="l00064"></a>00064 <span class="preprocessor"></span>
<a name="l00065"></a><a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php">00065</a>   <span class="keyword">class </span><a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php">CSizingControlBarG</a> : <span class="keyword">public</span> <a class="code" href="scbarg_8h.php#eeac25004543b642d145a5effbedc7ec">baseCSizingControlBarG</a>
<a name="l00066"></a>00066   {
<a name="l00067"></a>00067       <a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#8c2e8c9b3c3f1584eebd25947c6383cc">DECLARE_DYNAMIC</a>(<a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php">CSizingControlBarG</a>);
<a name="l00068"></a>00068
<a name="l00069"></a>00069   <span class="comment">// Construction</span>
<a name="l00070"></a>00070   <span class="keyword">public</span>:
<a name="l00071"></a>00071       <a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#71d0e137e6a6fe0e8b063b7a4e1a2dba">CSizingControlBarG</a>();
<a name="l00072"></a>00072
<a name="l00073"></a>00073   <span class="comment">// Attributes</span>
<a name="l00074"></a>00074   <span class="keyword">public</span>:
<a name="l00075"></a>00075       <span class="keyword">virtual</span> BOOL <a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#6c1b3cb4d7be803af1b99d00034ec286">HasGripper</a>() <span class="keyword">const</span>;
<a name="l00076"></a>00076
<a name="l00077"></a>00077   <span class="comment">// Operations</span>
<a name="l00078"></a>00078   <span class="keyword">public</span>:
<a name="l00079"></a>00079
<a name="l00080"></a>00080   <span class="comment">// Overridables</span>
<a name="l00081"></a>00081       <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#130eb2314ae2a29d93a4671a04b81c15">OnUpdateCmdUI</a>(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);
<a name="l00082"></a>00082
<a name="l00083"></a>00083   <span class="comment">// Overrides</span>
<a name="l00084"></a>00084   <span class="keyword">public</span>:
<a name="l00085"></a>00085       <span class="comment">// ClassWizard generated virtual function overrides</span>
<a name="l00086"></a>00086       <span class="comment">//{{AFX_VIRTUAL(CSizingControlBarG)</span>
<a name="l00087"></a>00087       <span class="comment">//}}AFX_VIRTUAL</span>
<a name="l00088"></a>00088
<a name="l00089"></a>00089   <span class="comment">// Implementation</span>
<a name="l00090"></a>00090   <span class="keyword">public</span>:
<a name="l00091"></a>00091       <span class="keyword">virtual</span> <a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#236dde3a951738dcd60ca512aff9e711">~CSizingControlBarG</a>();
<a name="l00092"></a>00092
<a name="l00093"></a>00093   <span class="keyword">protected</span>:
<a name="l00094"></a>00094       <span class="comment">// implementation helpers</span>
<a name="l00095"></a>00095       <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#460074b9829c0327a5843f2e50b8c751">NcPaintGripper</a>(CDC* pDC, CRect rcClient);
<a name="l00096"></a>00096       <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#a01b463d1be7c009df7c6fed9e40a677">NcCalcClient</a>(LPRECT pRc, UINT nDockBarID);
<a name="l00097"></a>00097
<a name="l00098"></a>00098   <span class="keyword">protected</span>:
<a name="l00099"></a><a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#406d76890e2ad3051ed0503993adefb3">00099</a>       <span class="keywordtype">int</span>     <a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#406d76890e2ad3051ed0503993adefb3">m_cyGripper</a>;
<a name="l00100"></a>00100
<a name="l00101"></a><a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#8330327164bcc7edeb6b77884289c79c">00101</a>       <a class="code" href="class_like_msdev_1_1_c_s_c_b_button.php">CSCBButton</a> <a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#8330327164bcc7edeb6b77884289c79c">m_biHide</a>;
<a name="l00102"></a>00102
<a name="l00103"></a>00103   <span class="comment">// Generated message map functions</span>
<a name="l00104"></a>00104   <span class="keyword">protected</span>:
<a name="l00105"></a>00105       <span class="comment">//{{AFX_MSG(CSizingControlBarG)</span>
<a name="l00106"></a>00106       afx_msg LRESULT <a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#417aa9a611f27e090a523c258ac63c83">OnNcHitTest</a>(CPoint point);
<a name="l00107"></a>00107       afx_msg <span class="keywordtype">void</span> <a class="code" href="class_like_msdev_1_1_c_sizing_control_bar_g.php#64034068a693e4c98622e55179cbde8a">OnNcLButtonUp</a>(UINT nHitTest, CPoint point);
<a name="l00108"></a>00108       <span class="comment">//}}AFX_MSG</span>
<a name="l00109"></a>00109
<a name="l00110"></a>00110       DECLARE_MESSAGE_MAP()
<a name="l00111"></a>00111   };
<a name="l00112"></a>00112 };
<a name="l00113"></a>00113
<a name="l00114"></a>00114 <span class="preprocessor">#endif // !defined(__SCBARG_H__)</span>
<a name="l00115"></a>00115 <span class="preprocessor"></span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
