<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>GZAdministrator::FormMain Member List</h1>This is the complete list of members for <a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#59c3903151e287424fbceb332d019057">components</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#6d8a6207474b655ba8f63e4836e44667">Dispose</a>(bool disposing)</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#5f7d061b06b58c26dfab1d0d6ba12fcc">Exit</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#7d74e79f27a8d682dcff582916a81bd9">Exit_Click</a>(object sender, System.EventArgs e)</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline, private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#386b067478b11c0e9b8654ee86bbd350">File</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#906c3f2a2f542f4c4fdb9c7d9cf488c1">FormMain</a>()</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#8858618b6afd592a1c51b2261828282c">FormMain_Closing</a>(object sender, System.ComponentModel.CancelEventArgs e)</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline, private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#6a531c167092f0db895d516bc3e180d3">FormMain_Load</a>(object sender, System.EventArgs e)</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline, private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#08acbb4162b6c3beb7fa02f6a26db813">GoPage</a>(string page)</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline, private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#add8110d554ffc83e43570f4b8cc9df2">InitializeComponent</a>()</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline, private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#d791c1af9ae766b1d415bc72211b4431">label1</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#cc6cbbc64dedf583b2a0f1da9e869ea3">label2</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#de5cb9bd4bbc331bb3847a7dec521b91">label3</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#8125bf3570d2f6b562411771260d07bd">label4</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#761fb8572ce5001e1fa96c0585960f12">label5</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#4c79d61113978329cd9a19058820e991">LinkClick</a>(object sender, System.EventArgs e)</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline, private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#30ae8a7b192734c41c26e7224cc5bd66">linkLabel1</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#a172d432f3c92639823f89f509e59551">linkLabel2</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#ab7c1aa0497a15fdc3f0a65faab2b9e3">linkLabel3</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#350a3509d75e0e84a04bef6c25a30b99">LoadDocument</a>(ModuleSheet form)</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline, private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#06a19d570294d8a42a564d11d792844e">m_forms</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#04adf6ad2b117e3550399e97c038cc38">m_serveraddinio</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#66268f93bdc002f62c498f25ffa94b34">Main</a>()</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline, private, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#162eb2478a147a8785e7ee6fd765dacf">mainMenu1</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#74eba1624bea85df806bee6db8d7984e">panel1</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#e716c67f5674386a2c0c24f1c1c86c7b">pictureBox1</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#18a70aa71846560a6c0e6a26f518b0f7">pictureBox2</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#2faf4c88e5a508973c298ecee23ef309">pictureBox3</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#d5bf1948176b036d534de27f1e5e4094">PreCloseDocument</a>(ModuleSheet form)</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#d972797aa16a134a90633bf142c408eb">splitter1</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#3f9332c662d74c034d8c211bfb7473fd">statusBar1</a></td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_g_z_administrator_1_1_form_main.php#9a518a4303d87a3445105e6a22cdebad">toolBar1_ButtonClick</a>(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)</td><td><a class="el" href="class_g_z_administrator_1_1_form_main.php">GZAdministrator::FormMain</a></td><td><code> [inline, private]</code></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
