<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ea6c3855b402b97c9537b801f064bee.php">GZone</a></div>
<h1>GZone.h</h1><a href="_g_zone_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// GZProject, Ultima Online utils.</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="comment">// This program is free software; you can redistribute it and/or</span>
<a name="l00005"></a>00005 <span class="comment">// modify it under the terms of the GNU General Public License</span>
<a name="l00006"></a>00006 <span class="comment">// as published by the Free Software Foundation; either version 2</span>
<a name="l00007"></a>00007 <span class="comment">// of the License, or (at your option) any later version.</span>
<a name="l00008"></a>00008
<a name="l00009"></a>00009 <span class="comment">// This program is distributed in the hope that it will be useful,</span>
<a name="l00010"></a>00010 <span class="comment">// but WITHOUT ANY WARRANTY; without even the implied warranty of</span>
<a name="l00011"></a>00011 <span class="comment">// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the</span>
<a name="l00012"></a>00012 <span class="comment">// GNU General Public License for more details.</span>
<a name="l00013"></a>00013
<a name="l00014"></a>00014 <span class="comment">// You should have received a copy of the GNU General Public License</span>
<a name="l00015"></a>00015 <span class="comment">// along with this program; if not, write to the Free Software</span>
<a name="l00016"></a>00016 <span class="comment">// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</span>
<a name="l00017"></a>00017
<a name="l00018"></a>00018 <span class="comment">// GZone.h : main header file for the GZONE application</span>
<a name="l00019"></a>00019 <span class="comment">//</span>
<a name="l00020"></a>00020
<a name="l00021"></a>00021 <span class="preprocessor">#if !defined(AFX_GZONE_H__CF850E5D_C547_46F3_BBD7_AABF59F95669__INCLUDED_)</span>
<a name="l00022"></a><a class="code" href="_g_zone_8h.php#fdf3cfca196df35db2dccfecc2a47873">00022</a> <span class="preprocessor"></span><span class="preprocessor">#define AFX_GZONE_H__CF850E5D_C547_46F3_BBD7_AABF59F95669__INCLUDED_</span>
<a name="l00023"></a>00023 <span class="preprocessor"></span>
<a name="l00024"></a>00024 <span class="preprocessor">#if _MSC_VER &gt; 1000</span>
<a name="l00025"></a>00025 <span class="preprocessor"></span><span class="preprocessor">#pragma once</span>
<a name="l00026"></a>00026 <span class="preprocessor"></span><span class="preprocessor">#endif // _MSC_VER &gt; 1000</span>
<a name="l00027"></a>00027 <span class="preprocessor"></span>
<a name="l00028"></a>00028 <span class="preprocessor">#ifndef __AFXWIN_H__</span>
<a name="l00029"></a>00029 <span class="preprocessor"></span><span class="preprocessor">        #error include 'stdafx.h' before including this file for PCH</span>
<a name="l00030"></a>00030 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00031"></a>00031 <span class="preprocessor"></span>
<a name="l00032"></a>00032 <span class="preprocessor">#include "<a class="code" href="_g_z_2_g_zone_2resource_8h.php">resource.h</a>"</span>       <span class="comment">// main symbols</span>
<a name="l00033"></a>00033
<a name="l00035"></a>00035 <span class="comment">// CGZoneApp:</span>
<a name="l00036"></a>00036 <span class="comment">// See GZone.cpp for the implementation of this class</span>
<a name="l00037"></a>00037 <span class="comment">//</span>
<a name="l00038"></a>00038
<a name="l00039"></a>00039 CString <a class="code" href="_g_zone_8cpp.php#ade9336c9cc18a559dfd7dc633fd0df0">LoadString</a>(<span class="keywordtype">int</span> i);
<a name="l00040"></a>00040
<a name="l00041"></a>00041 <span class="preprocessor">#include "<a class="code" href="gzunit_8h.php">gzunit/gzunit.h</a>"</span>
<a name="l00042"></a>00042
<a name="l00043"></a><a class="code" href="class_c_g_zone_app.php">00043</a> <span class="keyword">class </span><a class="code" href="class_c_g_zone_app.php">CGZoneApp</a> : <span class="keyword">public</span> CWinApp
<a name="l00044"></a>00044 {
<a name="l00045"></a>00045   <span class="keywordtype">void</span> <a class="code" href="class_c_g_zone_app.php#7b7ece30537e2186f91965755cfbd3ed">SaveUI</a>();
<a name="l00046"></a>00046   <span class="keywordtype">void</span> <a class="code" href="class_c_g_zone_app.php#e311dc0fc67fecd33b194d5ab2ac2698">LoadUI</a>();
<a name="l00047"></a>00047
<a name="l00048"></a>00048 <span class="keyword">public</span>:
<a name="l00049"></a><a class="code" href="class_c_g_zone_app_1_1_unit_list.php">00049</a>   <span class="keyword">class </span><a class="code" href="class_c_g_zone_app_1_1_unit_list.php">UnitList</a>:<span class="keyword">public</span> <a class="code" href="gzunit_8h.php#d126a90ed7ea5c44df003bf09937fe63">UnitList_t</a>
<a name="l00050"></a>00050   {
<a name="l00051"></a>00051   <span class="keyword">public</span>:
<a name="l00052"></a><a class="code" href="class_c_g_zone_app_1_1_unit_list.php#cc7eca178e6f60cbb3238718fb5d0fc6">00052</a>     <a class="code" href="class_c_g_zone_app_1_1_unit_list.php#cc7eca178e6f60cbb3238718fb5d0fc6">~UnitList</a>()
<a name="l00053"></a>00053     {
<a name="l00054"></a>00054       <a class="code" href="class_c_g_zone_app_1_1_unit_list.php#2f54e0f17d54d0a1ac71a78b041b23a2">Close</a>();
<a name="l00055"></a>00055     }
<a name="l00056"></a>00056
<a name="l00057"></a><a class="code" href="class_c_g_zone_app_1_1_unit_list.php#2f54e0f17d54d0a1ac71a78b041b23a2">00057</a>     <span class="keywordtype">void</span> <a class="code" href="class_c_g_zone_app_1_1_unit_list.php#2f54e0f17d54d0a1ac71a78b041b23a2">Close</a>()
<a name="l00058"></a>00058     {
<a name="l00059"></a>00059       <span class="keywordflow">for</span>(UnitList_t::iterator i=begin();i!=end();i++)
<a name="l00060"></a>00060       {
<a name="l00061"></a>00061         <span class="keyword">delete</span> (*i);
<a name="l00062"></a>00062       }
<a name="l00063"></a>00063       clear();
<a name="l00064"></a>00064     }
<a name="l00065"></a>00065   };
<a name="l00066"></a>00066
<a name="l00067"></a>00067   <span class="keyword">virtual</span> BOOL <a class="code" href="class_c_g_zone_app.php#cc19defb799bc761c0f78dcafa89531a">PreTranslateMessage</a>(MSG* pMsg);
<a name="l00068"></a><a class="code" href="class_c_g_zone_app.php#d30eff3c26c021da1327d85e937c5456">00068</a>   <a class="code" href="class_c_g_zone_app_1_1_unit_list.php">UnitList</a> <a class="code" href="class_c_g_zone_app.php#d30eff3c26c021da1327d85e937c5456">m_unitlist</a>;
<a name="l00069"></a>00069
<a name="l00070"></a>00070   <a class="code" href="class_c_g_zone_app.php#70909a87ad2721a94ce2a6e1d27c7adc">CGZoneApp</a>();
<a name="l00071"></a>00071   CString <a class="code" href="class_c_g_zone_app.php#7a2710f44fe703535d23798b69ae97f3">GetIniPath</a>();
<a name="l00072"></a>00072   CString <a class="code" href="class_c_g_zone_app.php#27a925c9d80d42eeb321f438509ab6d2">GetIni</a>();
<a name="l00073"></a>00073
<a name="l00074"></a>00074 <span class="comment">// Overrides</span>
<a name="l00075"></a>00075         <span class="comment">// ClassWizard generated virtual function overrides</span>
<a name="l00076"></a>00076         <span class="comment">//{{AFX_VIRTUAL(CGZoneApp)</span>
<a name="l00077"></a>00077         <span class="keyword">public</span>:
<a name="l00078"></a>00078         <span class="keyword">virtual</span> BOOL <a class="code" href="class_c_g_zone_app.php#658763fa95d546be4849e4ce6f492959">InitInstance</a>();
<a name="l00079"></a>00079         <span class="keyword">virtual</span> <span class="keywordtype">int</span> <a class="code" href="class_c_g_zone_app.php#48ce8f885167fc76217eaf6b2405e381">ExitInstance</a>();
<a name="l00080"></a>00080         <span class="comment">//}}AFX_VIRTUAL</span>
<a name="l00081"></a>00081
<a name="l00082"></a>00082 <span class="comment">// Implementation</span>
<a name="l00083"></a>00083         <span class="comment">//{{AFX_MSG(CGZoneApp)</span>
<a name="l00084"></a>00084         afx_msg <span class="keywordtype">void</span> <a class="code" href="class_c_g_zone_app.php#f455f4f333d962483eaa588c7ba5efe1">OnAppAbout</a>();
<a name="l00085"></a>00085                 <span class="comment">// NOTE - the ClassWizard will add and remove member functions here.</span>
<a name="l00086"></a>00086                 <span class="comment">//    DO NOT EDIT what you see in these blocks of generated code !</span>
<a name="l00087"></a>00087         <span class="comment">//}}AFX_MSG</span>
<a name="l00088"></a>00088         DECLARE_MESSAGE_MAP()
<a name="l00089"></a>00089   virtual <span class="keywordtype">int</span> <a class="code" href="class_c_g_zone_app.php#046a6ddb78598d1aaf047363c4d8a3cc">Run</a>();
<a name="l00090"></a>00090 };
<a name="l00091"></a>00091
<a name="l00092"></a>00092
<a name="l00094"></a>00094
<a name="l00095"></a>00095 <span class="comment">//{{AFX_INSERT_LOCATION}}</span>
<a name="l00096"></a>00096 <span class="comment">// Microsoft Visual C++ will insert additional declarations immediately before the previous line.</span>
<a name="l00097"></a>00097
<a name="l00098"></a>00098 <span class="preprocessor">#endif // !defined(AFX_GZONE_H__CF850E5D_C547_46F3_BBD7_AABF59F95669__INCLUDED_)</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
