<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>CSpeedHack Member List</h1>This is the complete list of members for <a class="el" href="class_c_speed_hack.php">CSpeedHack</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#ce77c24e52c9d0b37c863a33f355c416">BadItemManipulate</a>()</td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#b7971e594669da16978519a78c373328">Command</a>(std::string &amp;xmlinput, std::string &amp;xmloutput)</td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#039f496c7ad5b955352389ac82788899">CSpeedHack</a>()</td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_firewall_1_1_c_firewall_module.php#253cc06b756d79e444955aac3efed742">GetCurrentSession</a>()</td><td><a class="el" href="class_firewall_1_1_c_firewall_module.php">Firewall::CFirewallModule</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_firewall_1_1_c_firewall_module.php#e72d537c0d10b0b72972726cbbcd3d1d">GetFirewall</a>()</td><td><a class="el" href="class_firewall_1_1_c_firewall_module.php">Firewall::CFirewallModule</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#d1d73a5a593859872f29af54ffca8c17">GetName</a>()</td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [inline, private, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_server_addin_module.php#487a52ee344e2e0e3594c2d81fa06720">GetServerAddin</a>()</td><td><a class="el" href="class_c_server_addin_module.php">CServerAddinModule</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#a9506241df51b5bffd464c91e69130ec">handle_character_list</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#5d8015dd78888722703d1a6cbb35958d">handle_character_list2</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#a0266aa1c940b340f4c08214c74422e6">handle_character_status</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#afbd348c78d2622213094e1ca664e13b">handle_client_equip_item</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#bce0ba9659222d3acab1848a894bec28">handle_client_talk</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#9665f062431d7bda9eec52cf38a1afff">handle_delete_object</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#595c9de1e28f36eb683ba8b4f32610c7">handle_double_click</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#f3dfcff718a148685a4e92c032aa7c85">handle_dye_r</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#a3800126ea23d777457a51f08a802137">handle_dye_s</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#8864dc9956c2c019175697c22039dc54">handle_enter_world</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#671def786f2b31092e910ee6f6ada50f">handle_error_code</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#1f5e6cfe895ac5932186005d03843357">handle_first_login</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#fe07d262c49711e50ccb23fae6051302">handle_get_item</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#a085a40dc005af876d8e1f71cd002660">handle_global_light_level</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#69415ea9463606f8f32d8317594ff333">handle_new_command_filter_r</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#d940ca8ac99d3df122a6c2439eabbc3e">handle_new_command_filter_s</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#6f21ca1610f55fef6c8e66e9471b5242">handle_open_container</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#21d146439f49143d74eac842f69c537c">handle_open_gump</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#73e0576071f54385def7688e499be8f5">handle_open_menu_gump</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#bc7d0a9872d4ea02be96cc3a2ccfc9a4">handle_pause_control</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#a97e6192ec96061b62a56183fc2e4967">handle_relay_server</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#2f32a2caa73747f27dfe36d0ddb1ad01">handle_second_login</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#2d9060096eb675da9c2c132fb377e27c">handle_select_character</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#977eb1810a55dd808ad71b19fcf36577">handle_select_server</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#5b339a90b31199dc65736579c3b01d27">handle_server_equip_item</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#90ae6a31d39c38ebe768bb9989e8a809">handle_server_list</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#77311918bf851c81116f2b3a451cff4c">handle_server_talk</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#054aac8b7965faf035576140ab3baa42">handle_status_request</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#fae059f5917548bf702661087b581fa2">handle_target_r</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#aca7b974fa464665d70767d5ae79f153">handle_target_s</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#857f444114a2c84d011ed295fff8d28b">handle_unicode_client_talk</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#689b4088ba65944507d2974ec53c1a97">handle_update_contained_item</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#cfed03e43ee2a44251aab79138b31c49">handle_update_contained_items</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#826be488a3753196839900e3aceb8dd1">handle_update_hitpoints</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#02b345ca20a212410b84a5f031eceeb6">handle_update_item</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#2d3f5411729d0280976d105f6ae6b47a">handle_update_mana</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#c7c9669dc27f70d1b28aba59865dca15">handle_update_object</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#b91857b31fc0c7868d52f4569c4bab63">handle_update_player</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#363863fb863f1057d72f18b129f910d4">handle_update_stamina</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#0a74a7ea9af33555fc18840728431eeb">handle_vendor_buy_list</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#7247cc9430c0b7b117b534245a869fdf">handle_vendor_buy_reply_r</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#a6c3f6e41b64b0c5420ecc2b1bcb2926">handle_vendor_buy_reply_s</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#b464b2d67eca76cbfe5d6001c79bde78">handle_vendor_sell_list</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#97d7d9f4d0144941be2773d3e30e0cde">handle_vendor_sell_reply</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#cd370b117825ee4a2d63301aa42dcbab">handle_walk_request</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#7dcc3b6e4699172998e3ea2e22addf15">handle_weather_change</a>(const void *buf, int bufsize)</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#ecb4e636b39315a40c2530ef61c081ab">LoadConfig</a>(const char *xml)</td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#95961561da3e8188bb99a1bddc45c54d">m_item_manipulate_count</a></td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#7c520cb9d05211a6f00555fc0feadc4b">m_item_manipulate_count_items</a></td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#3f6f09e8529be23d8ff4eab887576650">m_item_manipulate_last</a></td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#21e3e2f97750c99d1d2870d3a9964f6c">m_item_manipulate_time</a></td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#7074a97343f34fd507b95cbfb7788b52">m_item_manipulate_times</a></td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#85585891452ee460f04b1a5889941b21">m_walk_count</a></td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#f8c4824b93cc24c7b8809d29ac4b0917">m_walk_lastdirection</a></td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#20ed4a1844552266ddf73ceb14fd5fb3">m_walk_lastwalkcount</a></td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#9788a0e6681926b3457f51a71cd86e06">m_walk_lastwalkrequest</a></td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#180cc2b9ae8706199e4c1f81d29e676e">m_walk_lastwalktotaltime</a></td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#4b230ff059eca1a00c68b45e776792ff">m_walk_speed</a></td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_speed_hack.php#db63aa86683a8ece2cbf891a09a9ac59">SaveConfig</a>()</td><td><a class="el" href="class_c_speed_hack.php">CSpeedHack</a></td><td><code> [private, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_client_net_events.php#5ef76e7a0e1c46c6115b4648abcd1540">~CClientNetEvents</a>()</td><td><a class="el" href="class_c_client_net_events.php">CClientNetEvents</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_firewall_1_1_c_firewall_module.php#097bd9f48a8355e1143869589b3b3f5e">~CFirewallModule</a>()</td><td><a class="el" href="class_firewall_1_1_c_firewall_module.php">Firewall::CFirewallModule</a></td><td><code> [inline, virtual]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_c_server_addin_module.php#de75520ae128a4a39951525099ffae7b">~CServerAddinModule</a>()</td><td><a class="el" href="class_c_server_addin_module.php">CServerAddinModule</a></td><td><code> [inline, virtual]</code></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
