<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_285c2ba7298296be1db6222076863471.php">GZAdministrator</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_5e699018e533b5010af65c826095caff.php">Altova</a></div>
<h1>SchemaTypes.cs</h1><a href="_schema_types_8cs.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="keyword">using</span> System;
<a name="l00002"></a>00002
<a name="l00003"></a><a class="code" href="namespace_altova_1_1_types.php">00003</a> <span class="keyword">namespace </span>Altova.Types
<a name="l00004"></a>00004 {
<a name="l00005"></a><a class="code" href="class_altova_1_1_types_1_1_schema_type.php">00005</a>         <span class="keyword">public</span> <span class="keyword">abstract</span> <span class="keyword">class </span><a class="code" href="class_altova_1_1_types_1_1_schema_type.php">SchemaType</a>
<a name="l00006"></a>00006         {
<a name="l00007"></a>00007                 <span class="comment">//public abstract string asString();</span>
<a name="l00008"></a>00008         }
<a name="l00009"></a>00009
<a name="l00010"></a><a class="code" href="class_altova_1_1_types_1_1_schema_string.php">00010</a>         <span class="keyword">public</span> <span class="keyword">class </span><a class="code" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a> : <a class="code" href="class_altova_1_1_types_1_1_schema_type.php">SchemaType</a>
<a name="l00011"></a>00011         {
<a name="l00012"></a><a class="code" href="class_altova_1_1_types_1_1_schema_string.php#abf10239b4e56e967beb3b948c924309">00012</a>                 <span class="keyword">public</span> string Value;
<a name="l00013"></a>00013
<a name="l00014"></a><a class="code" href="class_altova_1_1_types_1_1_schema_string.php#1fc689ec601d85b32f03d35628eca8c9">00014</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_string.php">SchemaString</a>(string Value)
<a name="l00015"></a>00015                 {
<a name="l00016"></a>00016                         <span class="keyword">this</span>.Value = Value;
<a name="l00017"></a>00017                 }
<a name="l00018"></a>00018
<a name="l00019"></a><a class="code" href="class_altova_1_1_types_1_1_schema_string.php#c3c38a1ea21f766474a649fee88b76bc">00019</a>                 override <span class="keyword">public</span> string ToString()
<a name="l00020"></a>00020                 {
<a name="l00021"></a>00021                         <span class="keywordflow">return</span> Value;
<a name="l00022"></a>00022                 }
<a name="l00023"></a>00023         }
<a name="l00024"></a>00024
<a name="l00025"></a><a class="code" href="class_altova_1_1_types_1_1_schema_boolean.php">00025</a>         <span class="keyword">public</span> <span class="keyword">class </span><a class="code" href="class_altova_1_1_types_1_1_schema_boolean.php">SchemaBoolean</a> : <a class="code" href="class_altova_1_1_types_1_1_schema_type.php">SchemaType</a>
<a name="l00026"></a>00026         {
<a name="l00027"></a><a class="code" href="class_altova_1_1_types_1_1_schema_boolean.php#b1275c8688359cb56b4e372cfeea3940">00027</a>                 <span class="keyword">public</span> <span class="keywordtype">bool</span> Value;
<a name="l00028"></a>00028
<a name="l00029"></a><a class="code" href="class_altova_1_1_types_1_1_schema_boolean.php#95b823a287cbd6e81123e98ba462dbe3">00029</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_boolean.php">SchemaBoolean</a>(<span class="keywordtype">bool</span> Value)
<a name="l00030"></a>00030                 {
<a name="l00031"></a>00031                         <span class="keyword">this</span>.Value = Value;
<a name="l00032"></a>00032                 }
<a name="l00033"></a>00033
<a name="l00034"></a><a class="code" href="class_altova_1_1_types_1_1_schema_boolean.php#6511960da7d0360a8a4eec9e64051938">00034</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_boolean.php">SchemaBoolean</a>(string Value)
<a name="l00035"></a>00035                 {
<a name="l00036"></a>00036                         <span class="keyword">this</span>.Value = Value == <span class="stringliteral">"true"</span> || Value == <span class="stringliteral">"1"</span>;
<a name="l00037"></a>00037                 }
<a name="l00038"></a>00038
<a name="l00039"></a><a class="code" href="class_altova_1_1_types_1_1_schema_boolean.php#5672f4ce5e29d5d3df303ccbb0b30ab3">00039</a>                 override <span class="keyword">public</span> string ToString()
<a name="l00040"></a>00040                 {
<a name="l00041"></a>00041                         <span class="keywordflow">return</span> Value ? <span class="stringliteral">"true"</span> : <span class="stringliteral">"false"</span>;
<a name="l00042"></a>00042                 }
<a name="l00043"></a>00043         }
<a name="l00044"></a>00044
<a name="l00045"></a><a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">00045</a>         <span class="keyword">public</span> <span class="keyword">class </span><a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> : <a class="code" href="class_altova_1_1_types_1_1_schema_type.php">SchemaType</a>
<a name="l00046"></a>00046         {
<a name="l00047"></a><a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#4a786eaea49e1d2cf87709c7c4ae86f7">00047</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> Value;
<a name="l00048"></a>00048
<a name="l00049"></a><a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#a70bb407e21f036ccee35808404c39ce">00049</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a>(<span class="keywordtype">int</span> Value)
<a name="l00050"></a>00050                 {
<a name="l00051"></a>00051                         <span class="keyword">this</span>.Value = Value;
<a name="l00052"></a>00052                 }
<a name="l00053"></a>00053
<a name="l00054"></a><a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#9c1b9f3c139a3af9fa2a34966fcdfd24">00054</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a>(string Value)
<a name="l00055"></a>00055                 {
<a name="l00056"></a>00056                         <span class="keyword">this</span>.Value = Convert.ToInt32(Value);
<a name="l00057"></a>00057                 }
<a name="l00058"></a>00058
<a name="l00059"></a><a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">00059</a>                 override <span class="keyword">public</span> string ToString()
<a name="l00060"></a>00060                 {
<a name="l00061"></a>00061                         <span class="keywordflow">return</span> Convert.ToString(Value);
<a name="l00062"></a>00062                 }
<a name="l00063"></a>00063         }
<a name="l00064"></a>00064
<a name="l00065"></a><a class="code" href="class_altova_1_1_types_1_1_schema_double.php">00065</a>         <span class="keyword">public</span> <span class="keyword">class </span><a class="code" href="class_altova_1_1_types_1_1_schema_double.php">SchemaDouble</a> : <a class="code" href="class_altova_1_1_types_1_1_schema_type.php">SchemaType</a>
<a name="l00066"></a>00066         {
<a name="l00067"></a><a class="code" href="class_altova_1_1_types_1_1_schema_double.php#97e69d94e923e271f7e1f603cf2fff78">00067</a>                 <span class="keyword">public</span> <span class="keywordtype">double</span> Value;
<a name="l00068"></a>00068
<a name="l00069"></a><a class="code" href="class_altova_1_1_types_1_1_schema_double.php#ea83f2acc7c1a6ebe184226a117a1469">00069</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_double.php">SchemaDouble</a>(<span class="keywordtype">double</span> Value)
<a name="l00070"></a>00070                 {
<a name="l00071"></a>00071                         <span class="keyword">this</span>.Value = Value;
<a name="l00072"></a>00072                 }
<a name="l00073"></a>00073
<a name="l00074"></a><a class="code" href="class_altova_1_1_types_1_1_schema_double.php#06cc821105fc3bf6673f5c0aefd2308a">00074</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_double.php">SchemaDouble</a>(string Value)
<a name="l00075"></a>00075                 {
<a name="l00076"></a>00076                         <span class="keyword">this</span>.Value = Convert.ToDouble(Value);
<a name="l00077"></a>00077                 }
<a name="l00078"></a>00078
<a name="l00079"></a><a class="code" href="class_altova_1_1_types_1_1_schema_double.php#e809bfdc1c0ea330a29cf3b6e69d4d56">00079</a>                 override <span class="keyword">public</span> string ToString()
<a name="l00080"></a>00080                 {
<a name="l00081"></a>00081                         <span class="keywordflow">return</span> Convert.ToString(Value);
<a name="l00082"></a>00082                 }
<a name="l00083"></a>00083         }
<a name="l00084"></a>00084 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
