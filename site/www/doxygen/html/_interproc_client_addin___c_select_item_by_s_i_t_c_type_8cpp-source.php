<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_9dcfd5a61dba1352584094bd6fa46c11.php">interproc_clientaddin</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_30b943b43c01f5b1d4921d3b9410bd88.php">xsd</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_872562f892b2bed05f65639a1cdbe636.php">InterprocClientAddin</a></div>
<h1>InterprocClientAddin_CSelectItemBySITCType.cpp</h1><a href="_interproc_client_addin___c_select_item_by_s_i_t_c_type_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// InterprocClientAddin_CSelectItemBySITCType.cpp</span>
<a name="l00004"></a>00004 <span class="comment">//</span>
<a name="l00005"></a>00005 <span class="comment">// This file was generated by XMLSpy 2006 Enterprise Edition.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00008"></a>00008 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00009"></a>00009 <span class="comment">//</span>
<a name="l00010"></a>00010 <span class="comment">// Refer to the XMLSpy Documentation for further details.</span>
<a name="l00011"></a>00011 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00012"></a>00012 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment"></span>
<a name="l00015"></a>00015
<a name="l00016"></a>00016 <span class="preprocessor">#include "<a class="code" href="_g_z_2interproc__clientaddin_2xsd_2_interproc_client_addin_2_std_afx_8h.php">StdAfx.h</a>"</span>
<a name="l00017"></a>00017 <span class="preprocessor">#include "<a class="code" href="_interproc_client_addin_base_8h.php">InterprocClientAddinBase.h</a>"</span>
<a name="l00018"></a>00018 <span class="preprocessor">#include "<a class="code" href="_interproc_client_addin___c_select_item_by_s_i_t_c_type_8h.php">InterprocClientAddin_CSelectItemBySITCType.h</a>"</span>
<a name="l00019"></a>00019 <span class="preprocessor">#include "<a class="code" href="_interproc_client_addin___c_item_type_8h.php">InterprocClientAddin_CItemType.h</a>"</span>
<a name="l00020"></a>00020
<a name="l00021"></a>00021
<a name="l00022"></a>00022
<a name="l00023"></a>00023 <span class="keyword">namespace </span>ica <span class="comment">// URI: http://gzproject.sourceforge.net/InterprocClientAddin</span>
<a name="l00024"></a>00024 {
<a name="l00026"></a>00026 <span class="comment">//</span>
<a name="l00027"></a>00027 <span class="comment">// class CSelectItemBySITCType</span>
<a name="l00028"></a>00028 <span class="comment">//</span>
<a name="l00030"></a>00030 <span class="comment"></span>
<a name="l00031"></a>00031
<a name="l00032"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#a4c10453c840d95e25b9036a9b8cf540">00032</a> <a class="code" href="classaltova_1_1_c_node.php#23a91b5b6ef1abca12745ecd7a2fe7a4">CNode::EGroupType</a> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#a4c10453c840d95e25b9036a9b8cf540">CSelectItemBySITCType::GetGroupType</a>()
<a name="l00033"></a>00033 {
<a name="l00034"></a>00034         <span class="keywordflow">return</span> eSequence;
<a name="l00035"></a>00035 }
<a name="l00036"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#e4ff2196bbb472495d3a618410d58c4c">00036</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#e4ff2196bbb472495d3a618410d58c4c">CSelectItemBySITCType::GetSerialMinCount</a>()
<a name="l00037"></a>00037 {
<a name="l00038"></a>00038         <span class="keywordflow">return</span> 1;
<a name="l00039"></a>00039 }
<a name="l00040"></a>00040
<a name="l00041"></a>00041
<a name="l00042"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#1e986d0875a64e759521c77a8bc528d9">00042</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#1e986d0875a64e759521c77a8bc528d9">CSelectItemBySITCType::GetSerialMaxCount</a>()
<a name="l00043"></a>00043 {
<a name="l00044"></a>00044         <span class="keywordflow">return</span> 1;
<a name="l00045"></a>00045 }
<a name="l00046"></a>00046
<a name="l00047"></a>00047
<a name="l00048"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#aefc1680374bd052f9eccca821eb3655">00048</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#aefc1680374bd052f9eccca821eb3655">CSelectItemBySITCType::GetSerialCount</a>()
<a name="l00049"></a>00049 {
<a name="l00050"></a>00050         <span class="keywordflow">return</span> ChildCountInternal(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Serial"</span>));
<a name="l00051"></a>00051 }
<a name="l00052"></a>00052
<a name="l00053"></a>00053
<a name="l00054"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#ef393138fc46f8adf1d1fbbc9f1d993d">00054</a> <span class="keywordtype">bool</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#ef393138fc46f8adf1d1fbbc9f1d993d">CSelectItemBySITCType::HasSerial</a>()
<a name="l00055"></a>00055 {
<a name="l00056"></a>00056         <span class="keywordflow">return</span> InternalHasChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Serial"</span>));
<a name="l00057"></a>00057 }
<a name="l00058"></a>00058
<a name="l00059"></a>00059
<a name="l00060"></a>00060 <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#faefd86d94bab7c38685f55feb845961">CSelectItemBySITCType::AddSerial</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> Serial)
<a name="l00061"></a>00061 {
<a name="l00062"></a>00062         <span class="keywordflow">if</span>( !Serial.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00063"></a>00063                 InternalAppend(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Serial"</span>), Serial);
<a name="l00064"></a>00064 }
<a name="l00065"></a>00065
<a name="l00066"></a>00066
<a name="l00067"></a>00067 <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#d99b150289a29d4990a7422954f03da3">CSelectItemBySITCType::InsertSerialAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> Serial, <span class="keywordtype">int</span> nIndex)
<a name="l00068"></a>00068 {
<a name="l00069"></a>00069         <span class="keywordflow">if</span>( !Serial.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00070"></a>00070                 InternalInsertAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Serial"</span>), nIndex, Serial);
<a name="l00071"></a>00071 }
<a name="l00072"></a>00072
<a name="l00073"></a>00073
<a name="l00074"></a>00074 <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#369b0b0f154ede9695972dc5fdca042b">CSelectItemBySITCType::ReplaceSerialAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> Serial, <span class="keywordtype">int</span> nIndex)
<a name="l00075"></a>00075 {
<a name="l00076"></a>00076         InternalReplaceAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Serial"</span>), nIndex, Serial);
<a name="l00077"></a>00077 }
<a name="l00078"></a>00078
<a name="l00079"></a>00079
<a name="l00080"></a>00080
<a name="l00081"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#44bd225b929d98c7dda85cffe82a99b8">00081</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#44bd225b929d98c7dda85cffe82a99b8">CSelectItemBySITCType::GetSerialAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00082"></a>00082 {
<a name="l00083"></a>00083         <span class="keywordflow">return</span> (LPCTSTR)InternalGetAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Serial"</span>), nIndex)-&gt;text;
<a name="l00084"></a>00084         <span class="comment">//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Serial"), nIndex)-&gt;text);</span>
<a name="l00085"></a>00085 }
<a name="l00086"></a>00086
<a name="l00087"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#9c76ff21af05f6cde80557ee86ab7982">00087</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#9c76ff21af05f6cde80557ee86ab7982">CSelectItemBySITCType::GetStartingSerialCursor</a>()
<a name="l00088"></a>00088 {
<a name="l00089"></a>00089         <span class="keywordflow">return</span> InternalGetFirstChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Serial"</span>));
<a name="l00090"></a>00090 }
<a name="l00091"></a>00091
<a name="l00092"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#0f6f7a06e0a872589f577ba687be078c">00092</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#0f6f7a06e0a872589f577ba687be078c">CSelectItemBySITCType::GetAdvancedSerialCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00093"></a>00093 {
<a name="l00094"></a>00094         <span class="keywordflow">return</span> InternalGetNextChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Serial"</span>), pCurNode);
<a name="l00095"></a>00095 }
<a name="l00096"></a>00096
<a name="l00097"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#e2cdde1465357f5f09d0725d773a9be2">00097</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#e2cdde1465357f5f09d0725d773a9be2">CSelectItemBySITCType::GetSerialValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00098"></a>00098 {
<a name="l00099"></a>00099         <span class="keywordflow">if</span>( pCurNode == NULL )
<a name="l00100"></a>00100                 <span class="keywordflow">throw</span> <a class="code" href="classaltova_1_1_c_xml_exception.php">CXmlException</a>(CXmlException::eError1, _T(<span class="stringliteral">"Index out of range"</span>));
<a name="l00101"></a>00101         <span class="keywordflow">else</span>
<a name="l00102"></a>00102
<a name="l00103"></a>00103                 <span class="keywordflow">return</span> (LPCTSTR)pCurNode-&gt;text;
<a name="l00104"></a>00104 }
<a name="l00105"></a>00105
<a name="l00106"></a>00106
<a name="l00107"></a>00107
<a name="l00108"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#2cd1032334db6ac8d2f495202f93ee04">00108</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#2cd1032334db6ac8d2f495202f93ee04">CSelectItemBySITCType::GetSerial</a>()
<a name="l00109"></a>00109 {
<a name="l00110"></a>00110         <span class="keywordflow">return</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#44bd225b929d98c7dda85cffe82a99b8">GetSerialAt</a>(0);
<a name="l00111"></a>00111 }
<a name="l00112"></a>00112
<a name="l00113"></a>00113
<a name="l00114"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#4329c0a17fc372755f61492291d23a36">00114</a> <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#4329c0a17fc372755f61492291d23a36">CSelectItemBySITCType::RemoveSerialAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00115"></a>00115 {
<a name="l00116"></a>00116         InternalRemoveAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Serial"</span>), nIndex);
<a name="l00117"></a>00117 }
<a name="l00118"></a>00118
<a name="l00119"></a>00119
<a name="l00120"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#b6f9218e5a288d66818dd2b2cedda141">00120</a> <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#b6f9218e5a288d66818dd2b2cedda141">CSelectItemBySITCType::RemoveSerial</a>()
<a name="l00121"></a>00121 {
<a name="l00122"></a>00122         <span class="keywordflow">while</span> (<a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#ef393138fc46f8adf1d1fbbc9f1d993d">HasSerial</a>())
<a name="l00123"></a>00123                 <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#4329c0a17fc372755f61492291d23a36">RemoveSerialAt</a>(0);
<a name="l00124"></a>00124 }
<a name="l00125"></a>00125
<a name="l00126"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#227c2211bf1da24a0637a0f0299423fd">00126</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#227c2211bf1da24a0637a0f0299423fd">CSelectItemBySITCType::GetItemTypeMinCount</a>()
<a name="l00127"></a>00127 {
<a name="l00128"></a>00128         <span class="keywordflow">return</span> 1;
<a name="l00129"></a>00129 }
<a name="l00130"></a>00130
<a name="l00131"></a>00131
<a name="l00132"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#fb1a8876fef6f9c0a2e7929428d23af8">00132</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#fb1a8876fef6f9c0a2e7929428d23af8">CSelectItemBySITCType::GetItemTypeMaxCount</a>()
<a name="l00133"></a>00133 {
<a name="l00134"></a>00134         <span class="keywordflow">return</span> 1;
<a name="l00135"></a>00135 }
<a name="l00136"></a>00136
<a name="l00137"></a>00137
<a name="l00138"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#76af43c8bf153051234e4c4e65367d01">00138</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#76af43c8bf153051234e4c4e65367d01">CSelectItemBySITCType::GetItemTypeCount</a>()
<a name="l00139"></a>00139 {
<a name="l00140"></a>00140         <span class="keywordflow">return</span> ChildCountInternal(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>));
<a name="l00141"></a>00141 }
<a name="l00142"></a>00142
<a name="l00143"></a>00143
<a name="l00144"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#a9e238878c975d979e8c26b51b1f231d">00144</a> <span class="keywordtype">bool</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#a9e238878c975d979e8c26b51b1f231d">CSelectItemBySITCType::HasItemType</a>()
<a name="l00145"></a>00145 {
<a name="l00146"></a>00146         <span class="keywordflow">return</span> InternalHasChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>));
<a name="l00147"></a>00147 }
<a name="l00148"></a>00148
<a name="l00149"></a>00149
<a name="l00150"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#5b95a436b7b2a4f8f4ceeaec8588df6e">00150</a> <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#5b95a436b7b2a4f8f4ceeaec8588df6e">CSelectItemBySITCType::AddItemType</a>(<a class="code" href="classica_1_1_c_item_type.php">CItemType</a>&amp; <a class="code" href="class_item_type.php">ItemType</a>)
<a name="l00151"></a>00151 {
<a name="l00152"></a>00152         InternalAppendNode(_T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), ItemType);
<a name="l00153"></a>00153 }
<a name="l00154"></a>00154
<a name="l00155"></a>00155
<a name="l00156"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#e6f842e439b451f0884352bce93e2147">00156</a> <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#e6f842e439b451f0884352bce93e2147">CSelectItemBySITCType::InsertItemTypeAt</a>(<a class="code" href="classica_1_1_c_item_type.php">CItemType</a>&amp; <a class="code" href="class_item_type.php">ItemType</a>, <span class="keywordtype">int</span> nIndex)
<a name="l00157"></a>00157 {
<a name="l00158"></a>00158         InternalInsertNodeAt(_T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), nIndex, ItemType);
<a name="l00159"></a>00159 }
<a name="l00160"></a>00160
<a name="l00161"></a>00161
<a name="l00162"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#501521c7320adf5460af536484971edf">00162</a> <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#501521c7320adf5460af536484971edf">CSelectItemBySITCType::ReplaceItemTypeAt</a>(<a class="code" href="classica_1_1_c_item_type.php">CItemType</a>&amp; <a class="code" href="class_item_type.php">ItemType</a>, <span class="keywordtype">int</span> nIndex)
<a name="l00163"></a>00163 {
<a name="l00164"></a>00164         InternalReplaceNodeAt(_T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), nIndex, ItemType);
<a name="l00165"></a>00165 }
<a name="l00166"></a>00166
<a name="l00167"></a>00167
<a name="l00168"></a>00168
<a name="l00169"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#1c3e0a08171d6018fd7437ff002e8ad6">00169</a> <a class="code" href="classica_1_1_c_item_type.php">CItemType</a> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#1c3e0a08171d6018fd7437ff002e8ad6">CSelectItemBySITCType::GetItemTypeAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00170"></a>00170 {
<a name="l00171"></a>00171         <span class="keywordflow">return</span> <a class="code" href="classica_1_1_c_item_type.php">CItemType</a>(*<span class="keyword">this</span>, InternalGetAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), nIndex));
<a name="l00172"></a>00172 }
<a name="l00173"></a>00173
<a name="l00174"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#1986c78c3f60a8fa79874327a13fad50">00174</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#1986c78c3f60a8fa79874327a13fad50">CSelectItemBySITCType::GetStartingItemTypeCursor</a>()
<a name="l00175"></a>00175 {
<a name="l00176"></a>00176         <span class="keywordflow">return</span> InternalGetFirstChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>));
<a name="l00177"></a>00177 }
<a name="l00178"></a>00178
<a name="l00179"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#26289eb757227f19cfd398e7405dac65">00179</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#26289eb757227f19cfd398e7405dac65">CSelectItemBySITCType::GetAdvancedItemTypeCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00180"></a>00180 {
<a name="l00181"></a>00181         <span class="keywordflow">return</span> InternalGetNextChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), pCurNode);
<a name="l00182"></a>00182 }
<a name="l00183"></a>00183
<a name="l00184"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#72c8ab503d5f56a1c7c10d768dfd0569">00184</a> <a class="code" href="classica_1_1_c_item_type.php">CItemType</a> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#72c8ab503d5f56a1c7c10d768dfd0569">CSelectItemBySITCType::GetItemTypeValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00185"></a>00185 {
<a name="l00186"></a>00186         <span class="keywordflow">if</span>( pCurNode == NULL )
<a name="l00187"></a>00187                 <span class="keywordflow">throw</span> <a class="code" href="classaltova_1_1_c_xml_exception.php">CXmlException</a>(CXmlException::eError1, _T(<span class="stringliteral">"Index out of range"</span>));
<a name="l00188"></a>00188         <span class="keywordflow">else</span>
<a name="l00189"></a>00189
<a name="l00190"></a>00190                 <span class="keywordflow">return</span> <a class="code" href="classica_1_1_c_item_type.php">CItemType</a>( *<span class="keyword">this</span>, pCurNode );
<a name="l00191"></a>00191 }
<a name="l00192"></a>00192
<a name="l00193"></a>00193
<a name="l00194"></a>00194
<a name="l00195"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#57c576b8626cf6d9dc23e60f119d74f8">00195</a> <a class="code" href="classica_1_1_c_item_type.php">CItemType</a> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#57c576b8626cf6d9dc23e60f119d74f8">CSelectItemBySITCType::GetItemType</a>()
<a name="l00196"></a>00196 {
<a name="l00197"></a>00197         <span class="keywordflow">return</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#1c3e0a08171d6018fd7437ff002e8ad6">GetItemTypeAt</a>(0);
<a name="l00198"></a>00198 }
<a name="l00199"></a>00199
<a name="l00200"></a>00200
<a name="l00201"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#4d5e9ee8ebc7371b64e2aa5d52c28bb2">00201</a> <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#4d5e9ee8ebc7371b64e2aa5d52c28bb2">CSelectItemBySITCType::RemoveItemTypeAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00202"></a>00202 {
<a name="l00203"></a>00203         InternalRemoveAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:ItemType"</span>), nIndex);
<a name="l00204"></a>00204 }
<a name="l00205"></a>00205
<a name="l00206"></a>00206
<a name="l00207"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#e83fe9360d7ece8facd030c2f6c96d6f">00207</a> <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#e83fe9360d7ece8facd030c2f6c96d6f">CSelectItemBySITCType::RemoveItemType</a>()
<a name="l00208"></a>00208 {
<a name="l00209"></a>00209         <span class="keywordflow">while</span> (<a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#a9e238878c975d979e8c26b51b1f231d">HasItemType</a>())
<a name="l00210"></a>00210                 <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#4d5e9ee8ebc7371b64e2aa5d52c28bb2">RemoveItemTypeAt</a>(0);
<a name="l00211"></a>00211 }
<a name="l00212"></a>00212
<a name="l00213"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#7a75a4db43df98ce7d927cd15c27390d">00213</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#7a75a4db43df98ce7d927cd15c27390d">CSelectItemBySITCType::GetCountMinCount</a>()
<a name="l00214"></a>00214 {
<a name="l00215"></a>00215         <span class="keywordflow">return</span> 1;
<a name="l00216"></a>00216 }
<a name="l00217"></a>00217
<a name="l00218"></a>00218
<a name="l00219"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#ed41d3598ea006c8212d7313d9806538">00219</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#ed41d3598ea006c8212d7313d9806538">CSelectItemBySITCType::GetCountMaxCount</a>()
<a name="l00220"></a>00220 {
<a name="l00221"></a>00221         <span class="keywordflow">return</span> 1;
<a name="l00222"></a>00222 }
<a name="l00223"></a>00223
<a name="l00224"></a>00224
<a name="l00225"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#9c0b2b77fddefd407796bbd561cd17dd">00225</a> <span class="keywordtype">int</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#9c0b2b77fddefd407796bbd561cd17dd">CSelectItemBySITCType::GetCountCount</a>()
<a name="l00226"></a>00226 {
<a name="l00227"></a>00227         <span class="keywordflow">return</span> ChildCountInternal(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Count"</span>));
<a name="l00228"></a>00228 }
<a name="l00229"></a>00229
<a name="l00230"></a>00230
<a name="l00231"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#14ea91919e8746acb61c5dbaf82b0851">00231</a> <span class="keywordtype">bool</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#14ea91919e8746acb61c5dbaf82b0851">CSelectItemBySITCType::HasCount</a>()
<a name="l00232"></a>00232 {
<a name="l00233"></a>00233         <span class="keywordflow">return</span> InternalHasChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Count"</span>));
<a name="l00234"></a>00234 }
<a name="l00235"></a>00235
<a name="l00236"></a>00236
<a name="l00237"></a>00237 <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#61b7805bba000aab43f2b1a33716418c">CSelectItemBySITCType::AddCount</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> Count)
<a name="l00238"></a>00238 {
<a name="l00239"></a>00239         <span class="keywordflow">if</span>( !Count.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00240"></a>00240                 InternalAppend(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Count"</span>), Count);
<a name="l00241"></a>00241 }
<a name="l00242"></a>00242
<a name="l00243"></a>00243
<a name="l00244"></a>00244 <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#3be4ccb77c1baf1471aeb308332215b1">CSelectItemBySITCType::InsertCountAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> Count, <span class="keywordtype">int</span> nIndex)
<a name="l00245"></a>00245 {
<a name="l00246"></a>00246         <span class="keywordflow">if</span>( !Count.<a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">IsNull</a>() )
<a name="l00247"></a>00247                 InternalInsertAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Count"</span>), nIndex, Count);
<a name="l00248"></a>00248 }
<a name="l00249"></a>00249
<a name="l00250"></a>00250
<a name="l00251"></a>00251 <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#132618dc789dd6ddaa230009454b5e5e">CSelectItemBySITCType::ReplaceCountAt</a>(<a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> Count, <span class="keywordtype">int</span> nIndex)
<a name="l00252"></a>00252 {
<a name="l00253"></a>00253         InternalReplaceAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Count"</span>), nIndex, Count);
<a name="l00254"></a>00254 }
<a name="l00255"></a>00255
<a name="l00256"></a>00256
<a name="l00257"></a>00257
<a name="l00258"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#e6d06f9f4359662bf89510e073e9e934">00258</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#e6d06f9f4359662bf89510e073e9e934">CSelectItemBySITCType::GetCountAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00259"></a>00259 {
<a name="l00260"></a>00260         <span class="keywordflow">return</span> (LPCTSTR)InternalGetAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Count"</span>), nIndex)-&gt;text;
<a name="l00261"></a>00261         <span class="comment">//return tstring((LPCTSTR)InternalGetAt(Element, _T("http://gzproject.sourceforge.net/InterprocClientAddin"), _T("ica:Count"), nIndex)-&gt;text);</span>
<a name="l00262"></a>00262 }
<a name="l00263"></a>00263
<a name="l00264"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#119306d660aeb1e901175a53fb3eac77">00264</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#119306d660aeb1e901175a53fb3eac77">CSelectItemBySITCType::GetStartingCountCursor</a>()
<a name="l00265"></a>00265 {
<a name="l00266"></a>00266         <span class="keywordflow">return</span> InternalGetFirstChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Count"</span>));
<a name="l00267"></a>00267 }
<a name="l00268"></a>00268
<a name="l00269"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#dead02c9cec4a78c164d116418892958">00269</a> MSXML2::IXMLDOMNodePtr <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#dead02c9cec4a78c164d116418892958">CSelectItemBySITCType::GetAdvancedCountCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00270"></a>00270 {
<a name="l00271"></a>00271         <span class="keywordflow">return</span> InternalGetNextChild(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Count"</span>), pCurNode);
<a name="l00272"></a>00272 }
<a name="l00273"></a>00273
<a name="l00274"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#afa0dd5546ea276d7711ee182a2f024a">00274</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#afa0dd5546ea276d7711ee182a2f024a">CSelectItemBySITCType::GetCountValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)
<a name="l00275"></a>00275 {
<a name="l00276"></a>00276         <span class="keywordflow">if</span>( pCurNode == NULL )
<a name="l00277"></a>00277                 <span class="keywordflow">throw</span> <a class="code" href="classaltova_1_1_c_xml_exception.php">CXmlException</a>(CXmlException::eError1, _T(<span class="stringliteral">"Index out of range"</span>));
<a name="l00278"></a>00278         <span class="keywordflow">else</span>
<a name="l00279"></a>00279
<a name="l00280"></a>00280                 <span class="keywordflow">return</span> (LPCTSTR)pCurNode-&gt;text;
<a name="l00281"></a>00281 }
<a name="l00282"></a>00282
<a name="l00283"></a>00283
<a name="l00284"></a>00284
<a name="l00285"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#b0ced3853c947e5103c5745daea17dd6">00285</a> <a class="code" href="classaltova_1_1_c_schema_number.php">CSchemaInt</a> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#b0ced3853c947e5103c5745daea17dd6">CSelectItemBySITCType::GetCount</a>()
<a name="l00286"></a>00286 {
<a name="l00287"></a>00287         <span class="keywordflow">return</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#e6d06f9f4359662bf89510e073e9e934">GetCountAt</a>(0);
<a name="l00288"></a>00288 }
<a name="l00289"></a>00289
<a name="l00290"></a>00290
<a name="l00291"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#3fd933258790fcee0d944425eb6b13ce">00291</a> <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#3fd933258790fcee0d944425eb6b13ce">CSelectItemBySITCType::RemoveCountAt</a>(<span class="keywordtype">int</span> nIndex)
<a name="l00292"></a>00292 {
<a name="l00293"></a>00293         InternalRemoveAt(Element, _T(<span class="stringliteral">"http://gzproject.sourceforge.net/InterprocClientAddin"</span>), _T(<span class="stringliteral">"ica:Count"</span>), nIndex);
<a name="l00294"></a>00294 }
<a name="l00295"></a>00295
<a name="l00296"></a>00296
<a name="l00297"></a><a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#feb34bcd63df06e914d0dfc75851f202">00297</a> <span class="keywordtype">void</span> <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#feb34bcd63df06e914d0dfc75851f202">CSelectItemBySITCType::RemoveCount</a>()
<a name="l00298"></a>00298 {
<a name="l00299"></a>00299         <span class="keywordflow">while</span> (<a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#14ea91919e8746acb61c5dbaf82b0851">HasCount</a>())
<a name="l00300"></a>00300                 <a class="code" href="classica_1_1_c_select_item_by_s_i_t_c_type.php#3fd933258790fcee0d944425eb6b13ce">RemoveCountAt</a>(0);
<a name="l00301"></a>00301 }
<a name="l00302"></a>00302
<a name="l00303"></a>00303 } <span class="comment">// end of namespace ica</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
