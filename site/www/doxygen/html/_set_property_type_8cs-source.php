<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_285c2ba7298296be1db6222076863471.php">GZAdministrator</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_8d48eff7f7385c6ae602f7565068492f.php">SchemaSpeedhack</a></div>
<h1>SetPropertyType.cs</h1><a href="_set_property_type_8cs.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">//</span>
<a name="l00002"></a>00002 <span class="comment">// SetPropertyType.cs.cs</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file was generated by XMLSPY 5 Enterprise Edition.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00007"></a>00007 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00008"></a>00008 <span class="comment">//</span>
<a name="l00009"></a>00009 <span class="comment">// Refer to the XMLSPY Documentation for further details.</span>
<a name="l00010"></a>00010 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012
<a name="l00013"></a>00013
<a name="l00014"></a>00014 <span class="keyword">using</span> System;
<a name="l00015"></a>00015 <span class="keyword">using</span> System.Xml;
<a name="l00016"></a>00016 <span class="keyword">using</span> Altova.Types;
<a name="l00017"></a>00017
<a name="l00018"></a>00018 <span class="keyword">namespace </span>SchemaSpeedHack
<a name="l00019"></a>00019 {
<a name="l00020"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php">00020</a>         <span class="keyword">public</span> <span class="keyword">class </span><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php">SetPropertyType</a> : Altova.Node
<a name="l00021"></a>00021         {
<a name="l00022"></a>00022 <span class="preprocessor">                #region Forward constructors</span>
<a name="l00023"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#da6bc888c8a1b9c8c32e6dab5d486626">00023</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#da6bc888c8a1b9c8c32e6dab5d486626">SetPropertyType</a>() : base() {}
<a name="l00024"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#3445ad96637cfb3b0fc026423ff13068">00024</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#da6bc888c8a1b9c8c32e6dab5d486626">SetPropertyType</a>(XmlDocument doc) : base(doc) {}
<a name="l00025"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#4b165f738fade97f4f18bd0a831a3535">00025</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#da6bc888c8a1b9c8c32e6dab5d486626">SetPropertyType</a>(XmlNode node) : base(node) {}
<a name="l00026"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#3d5f851f00f73f9bdb3a13a5d28acece">00026</a>                 <span class="keyword">public</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#da6bc888c8a1b9c8c32e6dab5d486626">SetPropertyType</a>(Altova.Node node) : base(node) {}
<a name="l00027"></a>00027 <span class="preprocessor">                #endregion // Forward constructors</span>
<a name="l00028"></a>00028 <span class="preprocessor"></span>
<a name="l00029"></a>00029 <span class="preprocessor">                #region WalkCount accessor methods</span>
<a name="l00030"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#4719773b29040e941e8c83ebd41f0945">00030</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#4719773b29040e941e8c83ebd41f0945">GetWalkCountMinCount</a>()
<a name="l00031"></a>00031                 {
<a name="l00032"></a>00032                         <span class="keywordflow">return</span> 1;
<a name="l00033"></a>00033                 }
<a name="l00034"></a>00034
<a name="l00035"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#cb7c436d4c08158fc9509ee0fad3d68d">00035</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#cb7c436d4c08158fc9509ee0fad3d68d">GetWalkCountMaxCount</a>()
<a name="l00036"></a>00036                 {
<a name="l00037"></a>00037                         <span class="keywordflow">return</span> 1;
<a name="l00038"></a>00038                 }
<a name="l00039"></a>00039
<a name="l00040"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#2ae44ddee7b03e38ccf25c32ffabbfe7">00040</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#2ae44ddee7b03e38ccf25c32ffabbfe7">GetWalkCountCount</a>()
<a name="l00041"></a>00041                 {
<a name="l00042"></a>00042                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#5a78f4a7e638948db52a50bf27f97277">DomChildCount</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>);
<a name="l00043"></a>00043                 }
<a name="l00044"></a>00044
<a name="l00045"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#d14e3daad2977b78b5296a218272e99b">00045</a>                 <span class="keyword">public</span> <span class="keywordtype">bool</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#d14e3daad2977b78b5296a218272e99b">HasWalkCount</a>()
<a name="l00046"></a>00046                 {
<a name="l00047"></a>00047                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#22caf29f497e86a36ca976c5b9344d11">HasDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>);
<a name="l00048"></a>00048                 }
<a name="l00049"></a>00049
<a name="l00050"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#6f12c1a8131a347059f4294ab202559b">00050</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#6f12c1a8131a347059f4294ab202559b">GetWalkCountAt</a>(<span class="keywordtype">int</span> index)
<a name="l00051"></a>00051                 {
<a name="l00052"></a>00052                         <span class="keywordflow">return</span> <span class="keyword">new</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a>(<a class="code" href="class_altova_1_1_node.php#3e9877a06a6e9021aecc6cf026c34f7a">GetDomNodeValue</a>(<a class="code" href="class_altova_1_1_node.php#30b0b16f6517d59c5415e7e7d8f83959">GetDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>, index)));
<a name="l00053"></a>00053                 }
<a name="l00054"></a>00054
<a name="l00055"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#6769ffb0d370edee40382bc59c79b1a3">00055</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#6769ffb0d370edee40382bc59c79b1a3">GetWalkCount</a>()
<a name="l00056"></a>00056                 {
<a name="l00057"></a>00057                         <span class="keywordflow">return</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#6f12c1a8131a347059f4294ab202559b">GetWalkCountAt</a>(0);
<a name="l00058"></a>00058                 }
<a name="l00059"></a>00059
<a name="l00060"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#d497b5beb29d93c83becc78e8845da44">00060</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#d497b5beb29d93c83becc78e8845da44">RemoveWalkCountAt</a>(<span class="keywordtype">int</span> index)
<a name="l00061"></a>00061                 {
<a name="l00062"></a>00062                         <a class="code" href="class_altova_1_1_node.php#7938fa304858dfc0e9f8f492c9a65ecb">RemoveDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>, index);
<a name="l00063"></a>00063                 }
<a name="l00064"></a>00064
<a name="l00065"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#d8f8ac600be70461502687cbee0e9a84">00065</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#d8f8ac600be70461502687cbee0e9a84">RemoveWalkCount</a>()
<a name="l00066"></a>00066                 {
<a name="l00067"></a>00067                         <span class="keywordflow">while</span> (<a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#d14e3daad2977b78b5296a218272e99b">HasWalkCount</a>())
<a name="l00068"></a>00068                                 <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#d497b5beb29d93c83becc78e8845da44">RemoveWalkCountAt</a>(0);
<a name="l00069"></a>00069                 }
<a name="l00070"></a>00070
<a name="l00071"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#ba7ff8b6f8c0d6b0520a0c8ef6139a26">00071</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#ba7ff8b6f8c0d6b0520a0c8ef6139a26">AddWalkCount</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue)
<a name="l00072"></a>00072                 {
<a name="l00073"></a>00073                         <a class="code" href="class_altova_1_1_node.php#c75ea55d1cdcb9119c14c8e7598d27fd">AppendDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00074"></a>00074                 }
<a name="l00075"></a>00075
<a name="l00076"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#ba412e8e0159a3744557219b6faeabbb">00076</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#ba412e8e0159a3744557219b6faeabbb">InsertWalkCountAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00077"></a>00077                 {
<a name="l00078"></a>00078                         <a class="code" href="class_altova_1_1_node.php#04fbd644a61272767ac268a6837e07b8">InsertDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00079"></a>00079                 }
<a name="l00080"></a>00080
<a name="l00081"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#517ba9a1690587386f0fee2efe10984c">00081</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#517ba9a1690587386f0fee2efe10984c">ReplaceWalkCountAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00082"></a>00082                 {
<a name="l00083"></a>00083                         <a class="code" href="class_altova_1_1_node.php#a99e2051ebcba9f999ee5cfa19b6072f">ReplaceDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkCount"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00084"></a>00084                 }
<a name="l00085"></a>00085 <span class="preprocessor">                #endregion // WalkCount accessor methods</span>
<a name="l00086"></a>00086 <span class="preprocessor"></span>
<a name="l00087"></a>00087 <span class="preprocessor">                #region WalkSpeed accessor methods</span>
<a name="l00088"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#5e24112584df5183e941e69e1db8671d">00088</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#5e24112584df5183e941e69e1db8671d">GetWalkSpeedMinCount</a>()
<a name="l00089"></a>00089                 {
<a name="l00090"></a>00090                         <span class="keywordflow">return</span> 1;
<a name="l00091"></a>00091                 }
<a name="l00092"></a>00092
<a name="l00093"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#1a1f8070f6ef5594ba4480d02867a302">00093</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#1a1f8070f6ef5594ba4480d02867a302">GetWalkSpeedMaxCount</a>()
<a name="l00094"></a>00094                 {
<a name="l00095"></a>00095                         <span class="keywordflow">return</span> 1;
<a name="l00096"></a>00096                 }
<a name="l00097"></a>00097
<a name="l00098"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#bf66f72b3ab69a4eae8e4d07da90b0b9">00098</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#bf66f72b3ab69a4eae8e4d07da90b0b9">GetWalkSpeedCount</a>()
<a name="l00099"></a>00099                 {
<a name="l00100"></a>00100                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#5a78f4a7e638948db52a50bf27f97277">DomChildCount</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>);
<a name="l00101"></a>00101                 }
<a name="l00102"></a>00102
<a name="l00103"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#d870514a264f7f2c49fea50ce6b19ac1">00103</a>                 <span class="keyword">public</span> <span class="keywordtype">bool</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#d870514a264f7f2c49fea50ce6b19ac1">HasWalkSpeed</a>()
<a name="l00104"></a>00104                 {
<a name="l00105"></a>00105                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#22caf29f497e86a36ca976c5b9344d11">HasDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>);
<a name="l00106"></a>00106                 }
<a name="l00107"></a>00107
<a name="l00108"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#f4ba3c6e9e29aa2412e4fe4a6d6aae4f">00108</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#f4ba3c6e9e29aa2412e4fe4a6d6aae4f">GetWalkSpeedAt</a>(<span class="keywordtype">int</span> index)
<a name="l00109"></a>00109                 {
<a name="l00110"></a>00110                         <span class="keywordflow">return</span> <span class="keyword">new</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a>(<a class="code" href="class_altova_1_1_node.php#3e9877a06a6e9021aecc6cf026c34f7a">GetDomNodeValue</a>(<a class="code" href="class_altova_1_1_node.php#30b0b16f6517d59c5415e7e7d8f83959">GetDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>, index)));
<a name="l00111"></a>00111                 }
<a name="l00112"></a>00112
<a name="l00113"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#cb99cda0307de99ac91b411eec99e50f">00113</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#cb99cda0307de99ac91b411eec99e50f">GetWalkSpeed</a>()
<a name="l00114"></a>00114                 {
<a name="l00115"></a>00115                         <span class="keywordflow">return</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#f4ba3c6e9e29aa2412e4fe4a6d6aae4f">GetWalkSpeedAt</a>(0);
<a name="l00116"></a>00116                 }
<a name="l00117"></a>00117
<a name="l00118"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#63f61894f3c1c21f0f39e06b1936e1da">00118</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#63f61894f3c1c21f0f39e06b1936e1da">RemoveWalkSpeedAt</a>(<span class="keywordtype">int</span> index)
<a name="l00119"></a>00119                 {
<a name="l00120"></a>00120                         <a class="code" href="class_altova_1_1_node.php#7938fa304858dfc0e9f8f492c9a65ecb">RemoveDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>, index);
<a name="l00121"></a>00121                 }
<a name="l00122"></a>00122
<a name="l00123"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#164d36ff6a204f91363dfc3052d6c886">00123</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#164d36ff6a204f91363dfc3052d6c886">RemoveWalkSpeed</a>()
<a name="l00124"></a>00124                 {
<a name="l00125"></a>00125                         <span class="keywordflow">while</span> (<a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#d870514a264f7f2c49fea50ce6b19ac1">HasWalkSpeed</a>())
<a name="l00126"></a>00126                                 <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#63f61894f3c1c21f0f39e06b1936e1da">RemoveWalkSpeedAt</a>(0);
<a name="l00127"></a>00127                 }
<a name="l00128"></a>00128
<a name="l00129"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#a9dca0be773efe0d3439c984f5e90c5f">00129</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#a9dca0be773efe0d3439c984f5e90c5f">AddWalkSpeed</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue)
<a name="l00130"></a>00130                 {
<a name="l00131"></a>00131                         <a class="code" href="class_altova_1_1_node.php#c75ea55d1cdcb9119c14c8e7598d27fd">AppendDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00132"></a>00132                 }
<a name="l00133"></a>00133
<a name="l00134"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#ab75b65b2df2d7452e0b8a560cdc9efb">00134</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#ab75b65b2df2d7452e0b8a560cdc9efb">InsertWalkSpeedAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00135"></a>00135                 {
<a name="l00136"></a>00136                         <a class="code" href="class_altova_1_1_node.php#04fbd644a61272767ac268a6837e07b8">InsertDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00137"></a>00137                 }
<a name="l00138"></a>00138
<a name="l00139"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#aabbf112de6de11d081c3b217bfb584f">00139</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#aabbf112de6de11d081c3b217bfb584f">ReplaceWalkSpeedAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00140"></a>00140                 {
<a name="l00141"></a>00141                         <a class="code" href="class_altova_1_1_node.php#a99e2051ebcba9f999ee5cfa19b6072f">ReplaceDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"WalkSpeed"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00142"></a>00142                 }
<a name="l00143"></a>00143 <span class="preprocessor">                #endregion // WalkSpeed accessor methods</span>
<a name="l00144"></a>00144 <span class="preprocessor"></span>
<a name="l00145"></a>00145 <span class="preprocessor">                #region CountItems accessor methods</span>
<a name="l00146"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#a381475198c9e6123f34f3c220751fa5">00146</a> <span class="preprocessor"></span>                <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#a381475198c9e6123f34f3c220751fa5">GetCountItemsMinCount</a>()
<a name="l00147"></a>00147                 {
<a name="l00148"></a>00148                         <span class="keywordflow">return</span> 1;
<a name="l00149"></a>00149                 }
<a name="l00150"></a>00150
<a name="l00151"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#c08229bc728df24630fe9cb044a2c99d">00151</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#c08229bc728df24630fe9cb044a2c99d">GetCountItemsMaxCount</a>()
<a name="l00152"></a>00152                 {
<a name="l00153"></a>00153                         <span class="keywordflow">return</span> 1;
<a name="l00154"></a>00154                 }
<a name="l00155"></a>00155
<a name="l00156"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#97fae813a757696196b7ac698306f145">00156</a>                 <span class="keyword">public</span> <span class="keywordtype">int</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#97fae813a757696196b7ac698306f145">GetCountItemsCount</a>()
<a name="l00157"></a>00157                 {
<a name="l00158"></a>00158                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#5a78f4a7e638948db52a50bf27f97277">DomChildCount</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>);
<a name="l00159"></a>00159                 }
<a name="l00160"></a>00160
<a name="l00161"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#b7cf85925936557e1d70958b8aaa8a08">00161</a>                 <span class="keyword">public</span> <span class="keywordtype">bool</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#b7cf85925936557e1d70958b8aaa8a08">HasCountItems</a>()
<a name="l00162"></a>00162                 {
<a name="l00163"></a>00163                         <span class="keywordflow">return</span> <a class="code" href="class_altova_1_1_node.php#22caf29f497e86a36ca976c5b9344d11">HasDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>);
<a name="l00164"></a>00164                 }
<a name="l00165"></a>00165
<a name="l00166"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#125e0d334cf8743bb87d57b78056e0e3">00166</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#125e0d334cf8743bb87d57b78056e0e3">GetCountItemsAt</a>(<span class="keywordtype">int</span> index)
<a name="l00167"></a>00167                 {
<a name="l00168"></a>00168                         <span class="keywordflow">return</span> <span class="keyword">new</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a>(<a class="code" href="class_altova_1_1_node.php#3e9877a06a6e9021aecc6cf026c34f7a">GetDomNodeValue</a>(<a class="code" href="class_altova_1_1_node.php#30b0b16f6517d59c5415e7e7d8f83959">GetDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>, index)));
<a name="l00169"></a>00169                 }
<a name="l00170"></a>00170
<a name="l00171"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#dd7fcb34df7ced3456a78012a11ad454">00171</a>                 <span class="keyword">public</span> <a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#dd7fcb34df7ced3456a78012a11ad454">GetCountItems</a>()
<a name="l00172"></a>00172                 {
<a name="l00173"></a>00173                         <span class="keywordflow">return</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#125e0d334cf8743bb87d57b78056e0e3">GetCountItemsAt</a>(0);
<a name="l00174"></a>00174                 }
<a name="l00175"></a>00175
<a name="l00176"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#c32fa456aa4f43633fcabae1d516865d">00176</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#c32fa456aa4f43633fcabae1d516865d">RemoveCountItemsAt</a>(<span class="keywordtype">int</span> index)
<a name="l00177"></a>00177                 {
<a name="l00178"></a>00178                         <a class="code" href="class_altova_1_1_node.php#7938fa304858dfc0e9f8f492c9a65ecb">RemoveDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>, index);
<a name="l00179"></a>00179                 }
<a name="l00180"></a>00180
<a name="l00181"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#22fab43072a77b2dd3280e4eb418945a">00181</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#22fab43072a77b2dd3280e4eb418945a">RemoveCountItems</a>()
<a name="l00182"></a>00182                 {
<a name="l00183"></a>00183                         <span class="keywordflow">while</span> (<a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#b7cf85925936557e1d70958b8aaa8a08">HasCountItems</a>())
<a name="l00184"></a>00184                                 <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#c32fa456aa4f43633fcabae1d516865d">RemoveCountItemsAt</a>(0);
<a name="l00185"></a>00185                 }
<a name="l00186"></a>00186
<a name="l00187"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#4db860a66ec21d97c5ab77bf69a200b1">00187</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#4db860a66ec21d97c5ab77bf69a200b1">AddCountItems</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue)
<a name="l00188"></a>00188                 {
<a name="l00189"></a>00189                         <a class="code" href="class_altova_1_1_node.php#c75ea55d1cdcb9119c14c8e7598d27fd">AppendDomChild</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00190"></a>00190                 }
<a name="l00191"></a>00191
<a name="l00192"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#7e0187df7178ba66f38176c614b1cb68">00192</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#7e0187df7178ba66f38176c614b1cb68">InsertCountItemsAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00193"></a>00193                 {
<a name="l00194"></a>00194                         <a class="code" href="class_altova_1_1_node.php#04fbd644a61272767ac268a6837e07b8">InsertDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00195"></a>00195                 }
<a name="l00196"></a>00196
<a name="l00197"></a><a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#f0105bd179f08df54a2ed7967a92b16f">00197</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_schema_speed_hack_1_1_set_property_type.php#f0105bd179f08df54a2ed7967a92b16f">ReplaceCountItemsAt</a>(<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php">SchemaInteger</a> newValue, <span class="keywordtype">int</span> index)
<a name="l00198"></a>00198                 {
<a name="l00199"></a>00199                         <a class="code" href="class_altova_1_1_node.php#a99e2051ebcba9f999ee5cfa19b6072f">ReplaceDomChildAt</a>(<a class="code" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a>.Element, <span class="stringliteral">""</span>, <span class="stringliteral">"CountItems"</span>, index, newValue.<a class="code" href="class_altova_1_1_types_1_1_schema_integer.php#725a93cefae05d3b824ff17e9e32139e">ToString</a>());
<a name="l00200"></a>00200                 }
<a name="l00201"></a>00201 <span class="preprocessor">                #endregion // CountItems accessor methods</span>
<a name="l00202"></a>00202 <span class="preprocessor"></span>        }
<a name="l00203"></a>00203 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
