<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_9dcfd5a61dba1352584094bd6fa46c11.php">interproc_clientaddin</a></div>
<h1>outofprocmasterevents.h File Reference</h1><code>#include &lt;map&gt;</code><br>
<code>#include &lt;algorithm&gt;</code><br>
<code>#include &lt;<a class="el" href="_sentry_sc_8h-source.php">misc/SentrySc.h</a>&gt;</code><br>
<code>#include &quot;<a class="el" href="outofprocmasterdata_8h-source.php">OutOfProcMasterdata.h</a>&quot;</code><br>
<code>#include &quot;<a class="el" href="interprocmaster_8h-source.php">../InterProc/interprocmaster.h</a>&quot;</code><br>
<code>#include &quot;<a class="el" href="executeabort_8h-source.php">executeabort.h</a>&quot;</code><br>
<code>#include &lt;<a class="el" href="errorreport_8h-source.php">ErrorReport/errorreport.h</a>&gt;</code><br>

<p>
<a href="outofprocmasterevents_8h-source.php">Go to the source code of this file.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Classes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_out_of_proc_master_events.php">COutOfProcMasterEvents</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_out_of_proc_master_events_1_1_event_handle_queue.php">COutOfProcMasterEvents::EventHandleQueue</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Класс очереди событий.  <a href="class_c_out_of_proc_master_events_1_1_event_handle_queue.php#_details">More...</a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_out_of_proc_master_events_1_1_event_handle_queue_1_1_wait_prepare.php">COutOfProcMasterEvents::EventHandleQueue::WaitPrepare</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_out_of_proc_master_events_1_1_event_handle_queue_1_1_do_reset_events.php">COutOfProcMasterEvents::EventHandleQueue::DoResetEvents</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_out_of_proc_master_events_1_1_event_handle_queue_1_1_do_set_events.php">COutOfProcMasterEvents::EventHandleQueue::DoSetEvents</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_out_of_proc_master_events_1_1_event_handle_queue_1_1_close_handles.php">COutOfProcMasterEvents::EventHandleQueue::CloseHandles</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_out_of_proc_master_events_1_1_event_expose.php">COutOfProcMasterEvents::EventExpose</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_out_of_proc_master_events_1_1_w_f_c.php">COutOfProcMasterEvents::WFC</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">class &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_out_of_proc_master_events_1_1_abort_handle.php">COutOfProcMasterEvents::AbortHandle</a></td></tr>

<tr><td colspan="2"><br><h2>Variables</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">const int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="outofprocmasterevents_8h.php#9d262eade97b06e3fe977134740e204c">g_bufsize</a> = 16384</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">размер буфера чтения.  <a href="#9d262eade97b06e3fe977134740e204c"></a><br></td></tr>
</table>
<hr><h2>Variable Documentation</h2>
<a class="anchor" name="9d262eade97b06e3fe977134740e204c"></a><!-- doxytag: member="outofprocmasterevents.h::g_bufsize" ref="9d262eade97b06e3fe977134740e204c" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">const int <a class="el" href="outofprocslave_8cpp.php#9d262eade97b06e3fe977134740e204c">g_bufsize</a> = 16384          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
размер буфера чтения.
<p>

<p>
Definition at line <a class="el" href="outofprocmasterevents_8h-source.php#l00015">15</a> of file <a class="el" href="outofprocmasterevents_8h-source.php">outofprocmasterevents.h</a>.
</div>
</div><p>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
