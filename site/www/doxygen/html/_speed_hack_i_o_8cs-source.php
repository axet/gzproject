<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_285c2ba7298296be1db6222076863471.php">GZAdministrator</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_5ff60b5b3eef1c696ca1940b3f202adc.php">SpeedHackIO</a></div>
<h1>SpeedHackIO.cs</h1><a href="_speed_hack_i_o_8cs.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="keyword">using</span> System;
<a name="l00002"></a>00002 <span class="comment">//using </span>
<a name="l00003"></a>00003 <span class="keyword">using</span> SchemaSpeedHack;
<a name="l00004"></a>00004
<a name="l00005"></a><a class="code" href="namespace_speed_hack_i_o.php">00005</a> <span class="keyword">namespace </span>SpeedHackIO
<a name="l00006"></a>00006 {
<a name="l00010"></a><a class="code" href="class_speed_hack_i_o_1_1_socket.php">00010</a>         <span class="keyword">public</span> <span class="keyword">class </span><a class="code" href="class_speed_hack_i_o_1_1_socket.php">Socket</a>
<a name="l00011"></a>00011         {
<a name="l00012"></a><a class="code" href="class_speed_hack_i_o_1_1_socket.php#8e37e5a2403cd7ccc5748b4cb41b64e1">00012</a>                 <span class="keyword">const</span> string <a class="code" href="class_speed_hack_i_o_1_1_socket.php#8e37e5a2403cd7ccc5748b4cb41b64e1">m_modulename</a>=<span class="stringliteral">"SpeedHack"</span>;
<a name="l00013"></a><a class="code" href="class_speed_hack_i_o_1_1_socket.php#8f6b091b138ae86bb63e74134c40530f">00013</a>                 ServerAddinIO.Socket <a class="code" href="class_speed_hack_i_o_1_1_socket.php#8f6b091b138ae86bb63e74134c40530f">m_socket</a>;
<a name="l00014"></a>00014
<a name="l00015"></a><a class="code" href="class_speed_hack_i_o_1_1_socket.php#ce6bb6e5bb89cd38bc1c8e6e83a61a5a">00015</a>                 <span class="keyword">public</span> <a class="code" href="class_speed_hack_i_o_1_1_socket.php#ce6bb6e5bb89cd38bc1c8e6e83a61a5a">Socket</a>(ref ServerAddinIO.Socket socket)
<a name="l00016"></a>00016                 {
<a name="l00017"></a>00017                         <a class="code" href="class_speed_hack_i_o_1_1_socket.php#8f6b091b138ae86bb63e74134c40530f">m_socket</a>=socket;
<a name="l00018"></a>00018                 }
<a name="l00019"></a>00019
<a name="l00020"></a><a class="code" href="class_speed_hack_i_o_1_1_socket.php#c4f66d3b32e54ea7c00dfaec95761127">00020</a>                 <span class="keyword">public</span> SchemaSpeedHack.RespondType <a class="code" href="class_speed_hack_i_o_1_1_socket.php#c4f66d3b32e54ea7c00dfaec95761127">GetProperty</a>()
<a name="l00021"></a>00021                 {
<a name="l00022"></a>00022                         string xmloutput;
<a name="l00023"></a>00023
<a name="l00024"></a>00024                         SchemaSpeedHack.SchemaSpeedHackDoc doc=<span class="keyword">new</span> SchemaSpeedHack.SchemaSpeedHackDoc();
<a name="l00025"></a>00025                         SchemaSpeedHack.PacketType packet=<span class="keyword">new</span> SchemaSpeedHack.PacketType();
<a name="l00026"></a>00026                         SchemaSpeedHack.GetPropertyType property=<span class="keyword">new</span> SchemaSpeedHack.GetPropertyType();
<a name="l00027"></a>00027                         property.AddRequest(<span class="keyword">new</span> Altova.Types.SchemaString(<span class="stringliteral">""</span>));
<a name="l00028"></a>00028                         packet.AddGetProperty(property);
<a name="l00029"></a>00029                         doc.SetRootElementName(<span class="stringliteral">""</span>,<span class="stringliteral">"Packet"</span>);
<a name="l00030"></a>00030                         string xmlinput=doc.SaveXML(packet);
<a name="l00031"></a>00031                         <a class="code" href="class_speed_hack_i_o_1_1_socket.php#8f6b091b138ae86bb63e74134c40530f">m_socket</a>.ModuleCommand(<a class="code" href="class_speed_hack_i_o_1_1_socket.php#8e37e5a2403cd7ccc5748b4cb41b64e1">m_modulename</a>,xmlinput,out xmloutput);
<a name="l00032"></a>00032
<a name="l00033"></a>00033                         packet=<span class="keyword">new</span> SchemaSpeedHack.PacketType(doc.LoadXML(xmloutput));
<a name="l00034"></a>00034                         <span class="keywordflow">return</span> packet.GetGetProperty().GetRespond();
<a name="l00035"></a>00035                 }
<a name="l00036"></a>00036
<a name="l00037"></a><a class="code" href="class_speed_hack_i_o_1_1_socket.php#080c00fd43d5130a02888b0b8ad93050">00037</a>                 <span class="keyword">public</span> <span class="keywordtype">void</span> <a class="code" href="class_speed_hack_i_o_1_1_socket.php#080c00fd43d5130a02888b0b8ad93050">SetProperty</a>(SchemaSpeedHack.SetPropertyType property)
<a name="l00038"></a>00038                 {
<a name="l00039"></a>00039                         SchemaSpeedHack.SchemaSpeedHackDoc doc=<span class="keyword">new</span> SchemaSpeedHack.SchemaSpeedHackDoc();
<a name="l00040"></a>00040                         SchemaSpeedHack.PacketType packet=<span class="keyword">new</span> SchemaSpeedHack.PacketType();
<a name="l00041"></a>00041
<a name="l00042"></a>00042                         packet.AddSetProperty(property);
<a name="l00043"></a>00043                         doc.SetRootElementName(<span class="stringliteral">""</span>,<span class="stringliteral">"Packet"</span>);
<a name="l00044"></a>00044                         string xmlinput=doc.SaveXML(packet);
<a name="l00045"></a>00045                         string xmloutput;
<a name="l00046"></a>00046                         <a class="code" href="class_speed_hack_i_o_1_1_socket.php#8f6b091b138ae86bb63e74134c40530f">m_socket</a>.ModuleCommand(<a class="code" href="class_speed_hack_i_o_1_1_socket.php#8e37e5a2403cd7ccc5748b4cb41b64e1">m_modulename</a>,xmlinput,out xmloutput);
<a name="l00047"></a>00047                 }
<a name="l00048"></a>00048         }
<a name="l00049"></a>00049 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
