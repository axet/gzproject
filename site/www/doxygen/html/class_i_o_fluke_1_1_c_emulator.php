<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="namespace_i_o_fluke.php">IOFluke</a>::<a class="el" href="class_i_o_fluke_1_1_c_emulator.php">CEmulator</a></div>
<h1>IOFluke::CEmulator Class Reference</h1><!-- doxytag: class="IOFluke::CEmulator" --><a class="el" href="class_i_o_fluke_1_1_c_emulator.php">CEmulator</a> - анализатор памяти, находит вызовы импортируемых функций.
<a href="#_details">More...</a>
<p>
<code>#include &lt;<a class="el" href="emulator_8h-source.php">emulator.h</a>&gt;</code>
<p>
<a href="class_i_o_fluke_1_1_c_emulator-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#89bcca05193ce9c208008f7a515f9c74">CEmulator</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#83d43b6b9470ff313f1402801f47d561">CEmulator</a> (<a class="el" href="class_i_o_fluke_1_1_hook_list.php">HookList</a> *)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">создать эмулятор фукнций которые необходимо перехватывать.  <a href="#83d43b6b9470ff313f1402801f47d561"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#6c6f5b3112917a6f0d71a02d63e22eb5">~CEmulator</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#c795d476aac223325a78d62039d1a9c1">Create</a> (<a class="el" href="class_i_o_fluke_1_1_hook_list.php">HookList</a> *)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">создать эмулятор фукнций которые необходимо перехватывать.  <a href="#c795d476aac223325a78d62039d1a9c1"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#bbc247afe32aeddf289b7e66827143f0">Close</a> ()</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">откатить перехват фукнций.  <a href="#bbc247afe32aeddf289b7e66827143f0"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#20aa1a095de98625cefe76ae76025f71">ParseImport</a> (const char *bodydllname, PIMAGE_THUNK_DATA thunk)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">обработать секцию импорта указанной библиотеки.  <a href="#20aa1a095de98625cefe76ae76025f71"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#3612605935876e4bf08b28dadd7e9c70">ParseModule</a> (HMODULE module)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">обработать указанный модуль на наличие фукнций хока.  <a href="#3612605935876e4bf08b28dadd7e9c70"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#265c7066547b8c985a7c152b53f29612">ParseMemory</a> (HINSTANCE curinstance)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">обработать указаанную область памяти на наличие фукнций хока.  <a href="#265c7066547b8c985a7c152b53f29612"></a><br></td></tr>
<tr><td colspan="2"><br><h2>Private Types</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">typedef <a class="el" href="struct_i_o_fluke_1_1_c_emulator_1_1roolback__tag.php">IOFluke::CEmulator::roolback_tag</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#c1b67d5451c4da787d92b672c032c128">roolback_t</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">информация которую мы сохраняем.  <a href="#c1b67d5451c4da787d92b672c032c128"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">typedef std::list&lt; <a class="el" href="struct_i_o_fluke_1_1_c_emulator_1_1roolback__tag.php">roolback_t</a> &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#979207cc61b92d5582bfaa800f422bea">roolbacklist_t</a></td></tr>

<tr><td colspan="2"><br><h2>Private Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="struct_i_o_fluke_1_1hook.php">hook</a> *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#d6906328d077f6f5fee93484e517fc09">GetHook</a> (DWORD implold)</td></tr>

<tr><td colspan="2"><br><h2>Private Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#979207cc61b92d5582bfaa800f422bea">roolbacklist_t</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#54fea31991498f9b4371f03f019823d9">m_roolbacklist</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">список адреов на память которые были заменены.  <a href="#54fea31991498f9b4371f03f019823d9"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top"><a class="el" href="class_i_o_fluke_1_1_hook_list.php">HookList</a> *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#b98d8d89131d5a8420b7165b5255166b">m_hooklist</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">указатель на массив хоков которые нужно поставить.  <a href="#b98d8d89131d5a8420b7165b5255166b"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">CComAutoCriticalSection&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#8486f71492197279cc77edb8d1ce1a40">m_cs</a></td></tr>

<tr><td colspan="2"><br><h2>Classes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">struct &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_o_fluke_1_1_c_emulator_1_1roolback__tag.php">roolback_tag</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">информация которую мы сохраняем.  <a href="struct_i_o_fluke_1_1_c_emulator_1_1roolback__tag.php#_details">More...</a><br></td></tr>
</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>
<a class="el" href="class_i_o_fluke_1_1_c_emulator.php">CEmulator</a> - анализатор памяти, находит вызовы импортируемых функций.
<p>
класс для перехвата всех внешних вызовов, глючит на MFC из за начличия кривого експорта на переменную. implold - implementation old, та что была implnew - implementation new, та кем стала
<p>

<p>
Definition at line <a class="el" href="emulator_8h-source.php#l00071">71</a> of file <a class="el" href="emulator_8h-source.php">emulator.h</a>.<hr><h2>Member Typedef Documentation</h2>
<a class="anchor" name="c1b67d5451c4da787d92b672c032c128"></a><!-- doxytag: member="IOFluke::CEmulator::roolback_t" ref="c1b67d5451c4da787d92b672c032c128" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">typedef struct <a class="el" href="struct_i_o_fluke_1_1_c_emulator_1_1roolback__tag.php">IOFluke::CEmulator::roolback_tag</a>  <a class="el" href="struct_i_o_fluke_1_1_c_emulator_1_1roolback__tag.php">IOFluke::CEmulator::roolback_t</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
информация которую мы сохраняем.
<p>

</div>
</div><p>
<a class="anchor" name="979207cc61b92d5582bfaa800f422bea"></a><!-- doxytag: member="IOFluke::CEmulator::roolbacklist_t" ref="979207cc61b92d5582bfaa800f422bea" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">typedef std::list&lt;<a class="el" href="struct_i_o_fluke_1_1_c_emulator_1_1roolback__tag.php">roolback_t</a>&gt; <a class="el" href="class_i_o_fluke_1_1_c_emulator.php#979207cc61b92d5582bfaa800f422bea">IOFluke::CEmulator::roolbacklist_t</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="emulator_8h-source.php#l00083">83</a> of file <a class="el" href="emulator_8h-source.php">emulator.h</a>.
</div>
</div><p>
<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="89bcca05193ce9c208008f7a515f9c74"></a><!-- doxytag: member="IOFluke::CEmulator::CEmulator" ref="89bcca05193ce9c208008f7a515f9c74" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">IOFluke::CEmulator::CEmulator           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00087">87</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00088"></a>00088 {
<a name="l00089"></a>00089 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="83d43b6b9470ff313f1402801f47d561"></a><!-- doxytag: member="IOFluke::CEmulator::CEmulator" ref="83d43b6b9470ff313f1402801f47d561" args="(HookList *)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">IOFluke::CEmulator::CEmulator           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_i_o_fluke_1_1_hook_list.php">HookList</a> *&nbsp;</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
создать эмулятор фукнций которые необходимо перехватывать.
<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00097">97</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.
<p>
References <a class="el" href="emulator_8h-source.php#l00089">m_cs</a>, and <a class="el" href="emulator_8h-source.php#l00088">m_hooklist</a>.<div class="fragment"><pre class="fragment"><a name="l00098"></a>00098 {
<a name="l00099"></a>00099   <a class="code" href="class_sentry_sc.php">SentrySc</a> sc(<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#8486f71492197279cc77edb8d1ce1a40">m_cs</a>);
<a name="l00100"></a>00100   <a class="code" href="class_i_o_fluke_1_1_c_emulator.php#b98d8d89131d5a8420b7165b5255166b">m_hooklist</a>=h;
<a name="l00101"></a>00101 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="6c6f5b3112917a6f0d71a02d63e22eb5"></a><!-- doxytag: member="IOFluke::CEmulator::~CEmulator" ref="6c6f5b3112917a6f0d71a02d63e22eb5" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">IOFluke::CEmulator::~CEmulator           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00188">188</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.
<p>
References <a class="el" href="emulator_8cpp-source.php#l00198">Close()</a>.<div class="fragment"><pre class="fragment"><a name="l00189"></a>00189 {
<a name="l00190"></a>00190   <span class="comment">//в деструкторе особождать память поздно :( (для глобальных обьектов)</span>
<a name="l00191"></a>00191   <span class="comment">//дело в том что kernel32 сначало запрашивает адрес ZvTerminateProcess</span>
<a name="l00192"></a>00192   <span class="comment">//потом освобождает эму бибилотеку, а только затем вызвает ^^^</span>
<a name="l00193"></a>00193   <span class="comment">//из за этого указатель идет на unaccessed memory</span>
<a name="l00194"></a>00194
<a name="l00195"></a>00195   <a class="code" href="class_i_o_fluke_1_1_c_emulator.php#bbc247afe32aeddf289b7e66827143f0">Close</a>();
<a name="l00196"></a>00196 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="d6906328d077f6f5fee93484e517fc09"></a><!-- doxytag: member="IOFluke::CEmulator::GetHook" ref="d6906328d077f6f5fee93484e517fc09" args="(DWORD implold)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="struct_i_o_fluke_1_1hook.php">IOFluke::hook</a> * IOFluke::CEmulator::GetHook           </td>
          <td>(</td>
          <td class="paramtype">DWORD&nbsp;</td>
          <td class="paramname"> <em>implold</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [private]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00103">103</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.
<p>
References <a class="el" href="emulator_8h-source.php#l00089">m_cs</a>, and <a class="el" href="emulator_8h-source.php#l00088">m_hooklist</a>.
<p>
Referenced by <a class="el" href="emulator_8cpp-source.php#l00116">ParseImport()</a>.<div class="fragment"><pre class="fragment"><a name="l00104"></a>00104 {
<a name="l00105"></a>00105   <a class="code" href="class_sentry_sc.php">SentrySc</a> sc(<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#8486f71492197279cc77edb8d1ce1a40">m_cs</a>);
<a name="l00106"></a>00106   HookList::iterator i=<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#b98d8d89131d5a8420b7165b5255166b">m_hooklist</a>-&gt;begin();
<a name="l00107"></a>00107   <span class="keywordflow">while</span>(i!=<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#b98d8d89131d5a8420b7165b5255166b">m_hooklist</a>-&gt;end())
<a name="l00108"></a>00108   {
<a name="l00109"></a>00109     <span class="keywordflow">if</span>(i-&gt;implold==implold)
<a name="l00110"></a>00110       <span class="keywordflow">return</span> &amp;*i;
<a name="l00111"></a>00111     i++;
<a name="l00112"></a>00112   }
<a name="l00113"></a>00113   <span class="keywordflow">return</span> 0;
<a name="l00114"></a>00114 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="c795d476aac223325a78d62039d1a9c1"></a><!-- doxytag: member="IOFluke::CEmulator::Create" ref="c795d476aac223325a78d62039d1a9c1" args="(HookList *)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void IOFluke::CEmulator::Create           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_i_o_fluke_1_1_hook_list.php">HookList</a> *&nbsp;</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
создать эмулятор фукнций которые необходимо перехватывать.
<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00091">91</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.
<p>
References <a class="el" href="emulator_8h-source.php#l00089">m_cs</a>, and <a class="el" href="emulator_8h-source.php#l00088">m_hooklist</a>.
<p>
Referenced by <a class="el" href="memmngr_8cpp-source.php#l00049">IOFluke::CMemMngr::CMemMngr()</a>.<div class="fragment"><pre class="fragment"><a name="l00092"></a>00092 {
<a name="l00093"></a>00093   <a class="code" href="class_sentry_sc.php">SentrySc</a> sc(<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#8486f71492197279cc77edb8d1ce1a40">m_cs</a>);
<a name="l00094"></a>00094   <a class="code" href="class_i_o_fluke_1_1_c_emulator.php#b98d8d89131d5a8420b7165b5255166b">m_hooklist</a>=h;
<a name="l00095"></a>00095 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="bbc247afe32aeddf289b7e66827143f0"></a><!-- doxytag: member="IOFluke::CEmulator::Close" ref="bbc247afe32aeddf289b7e66827143f0" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void IOFluke::CEmulator::Close           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
откатить перехват фукнций.
<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00198">198</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.
<p>
References <a class="el" href="emulator_8h-source.php#l00089">m_cs</a>, and <a class="el" href="emulator_8h-source.php#l00086">m_roolbacklist</a>.
<p>
Referenced by <a class="el" href="emulator_8cpp-source.php#l00188">~CEmulator()</a>, and <a class="el" href="memmngr_8cpp-source.php#l00027">IOFluke::CMemMngr::~CMemMngr()</a>.<div class="fragment"><pre class="fragment"><a name="l00199"></a>00199 {
<a name="l00200"></a>00200   <a class="code" href="class_sentry_sc.php">SentrySc</a> sc(<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#8486f71492197279cc77edb8d1ce1a40">m_cs</a>);
<a name="l00201"></a>00201
<a name="l00202"></a>00202   <span class="keywordflow">for</span>(roolbacklist_t::iterator i=<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#54fea31991498f9b4371f03f019823d9">m_roolbacklist</a>.begin();i!=<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#54fea31991498f9b4371f03f019823d9">m_roolbacklist</a>.end();i++)
<a name="l00203"></a>00203   {
<a name="l00204"></a>00204     <span class="keywordtype">unsigned</span> <span class="keywordtype">long</span> old;
<a name="l00205"></a>00205     VirtualProtect(i-&gt;address_mem,4,PAGE_READWRITE,&amp;old);
<a name="l00206"></a>00206     *(i-&gt;address_mem)=i-&gt;address_proc;
<a name="l00207"></a>00207     VirtualProtect(i-&gt;address_mem,4,old,&amp;old);
<a name="l00208"></a>00208   }
<a name="l00209"></a>00209 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="20aa1a095de98625cefe76ae76025f71"></a><!-- doxytag: member="IOFluke::CEmulator::ParseImport" ref="20aa1a095de98625cefe76ae76025f71" args="(const char *bodydllname, PIMAGE_THUNK_DATA thunk)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void IOFluke::CEmulator::ParseImport           </td>
          <td>(</td>
          <td class="paramtype">const char *&nbsp;</td>
          <td class="paramname"> <em>bodydllname</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">PIMAGE_THUNK_DATA&nbsp;</td>
          <td class="paramname"> <em>thunk</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
обработать секцию импорта указанной библиотеки.
<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00116">116</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.
<p>
References <a class="el" href="debug_8h-source.php#l00045">DBGTRACE</a>, <a class="el" href="emulator_8cpp-source.php#l00103">GetHook()</a>, <a class="el" href="emulator_8h-source.php#l00089">m_cs</a>, and <a class="el" href="emulator_8h-source.php#l00086">m_roolbacklist</a>.
<p>
Referenced by <a class="el" href="emulator_8cpp-source.php#l00134">ParseModule()</a>.<div class="fragment"><pre class="fragment"><a name="l00117"></a>00117 {
<a name="l00118"></a>00118   <a class="code" href="class_sentry_sc.php">SentrySc</a> sc(<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#8486f71492197279cc77edb8d1ce1a40">m_cs</a>);
<a name="l00119"></a>00119   <span class="keywordflow">while</span>(thunk-&gt;u1.Function!=0)
<a name="l00120"></a>00120   {
<a name="l00121"></a>00121     <span class="keywordflow">if</span>(hook* h=<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#d6906328d077f6f5fee93484e517fc09">GetHook</a>((DWORD)thunk-&gt;u1.Function))
<a name="l00122"></a>00122     {
<a name="l00123"></a>00123       <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"create hook %s %s\n"</span>,bodydllname,h-&gt;oldfuncname.c_str());
<a name="l00124"></a>00124       DWORD old;
<a name="l00125"></a>00125       VirtualProtect(&amp;thunk-&gt;u1.Function,4,PAGE_READWRITE,&amp;old);
<a name="l00126"></a>00126       <a class="code" href="class_i_o_fluke_1_1_c_emulator.php#54fea31991498f9b4371f03f019823d9">m_roolbacklist</a>.push_back(<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#c1b67d5451c4da787d92b672c032c128">roolback_t</a>(&amp;thunk-&gt;u1.Function,thunk-&gt;u1.Function));
<a name="l00127"></a>00127       thunk-&gt;u1.Function=(DWORD)h-&gt;implnew;
<a name="l00128"></a>00128       VirtualProtect(&amp;thunk-&gt;u1.Function,4,old,&amp;old);
<a name="l00129"></a>00129     }
<a name="l00130"></a>00130     thunk++;
<a name="l00131"></a>00131   }
<a name="l00132"></a>00132 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="3612605935876e4bf08b28dadd7e9c70"></a><!-- doxytag: member="IOFluke::CEmulator::ParseModule" ref="3612605935876e4bf08b28dadd7e9c70" args="(HMODULE module)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void IOFluke::CEmulator::ParseModule           </td>
          <td>(</td>
          <td class="paramtype">HMODULE&nbsp;</td>
          <td class="paramname"> <em>module</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
обработать указанный модуль на наличие фукнций хока.
<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00134">134</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.
<p>
References <a class="el" href="path_8cpp-source.php#l00038">IniExt::GetModuleFileName()</a>, <a class="el" href="emulator_8h-source.php#l00089">m_cs</a>, and <a class="el" href="emulator_8cpp-source.php#l00116">ParseImport()</a>.
<p>
Referenced by <a class="el" href="memmngr_8cpp-source.php#l00071">IOFluke::CMemMngr::CatchImportFunction()</a>, <a class="el" href="emulator_8cpp-source.php#l00162">ParseMemory()</a>, and <a class="el" href="memmngr_8cpp-source.php#l00057">IOFluke::CMemMngr::RedirectImportFunction()</a>.<div class="fragment"><pre class="fragment"><a name="l00135"></a>00135 {
<a name="l00136"></a>00136   <a class="code" href="class_sentry_sc.php">SentrySc</a> sc(<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#8486f71492197279cc77edb8d1ce1a40">m_cs</a>);
<a name="l00137"></a>00137   PIMAGE_DOS_HEADER idh=(PIMAGE_DOS_HEADER)module;
<a name="l00138"></a>00138   PIMAGE_NT_HEADERS inh=(PIMAGE_NT_HEADERS)((DWORD)idh+idh-&gt;e_lfanew);
<a name="l00139"></a>00139   <span class="keywordflow">if</span>((*inh).Signature=='EP')
<a name="l00140"></a>00140   {
<a name="l00141"></a>00141     <span class="keywordtype">char</span> dllname[MAX_PATH];
<a name="l00142"></a>00142     <a class="code" href="namespace_ini_ext.php#e8ec57b84b079b6b4bcac40184ab24dc">GetModuleFileName</a>((HMODULE)idh,dllname,<span class="keyword">sizeof</span>(dllname));
<a name="l00143"></a>00143     <span class="comment">/*if(inh-&gt;OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IAT].Size!=0)</span>
<a name="l00144"></a>00144 <span class="comment">    {</span>
<a name="l00145"></a>00145 <span class="comment">      PIMAGE_THUNK_DATA thunk=(PIMAGE_THUNK_DATA)((DWORD)idh+inh-&gt;OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IAT].VirtualAddress);</span>
<a name="l00146"></a>00146 <span class="comment">      ParseImport(dllname,thunk);</span>
<a name="l00147"></a>00147 <span class="comment">    }*/</span>
<a name="l00148"></a>00148     <span class="keywordflow">if</span>(inh-&gt;OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size!=0)
<a name="l00149"></a>00149     {
<a name="l00150"></a>00150       PIMAGE_IMPORT_DESCRIPTOR <span class="keyword">import</span>=(PIMAGE_IMPORT_DESCRIPTOR)((DWORD)idh+inh-&gt;OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
<a name="l00151"></a>00151       <span class="keywordflow">while</span>((*import).Name!=0)
<a name="l00152"></a>00152       {
<a name="l00153"></a>00153         <span class="keyword">const</span> <span class="keywordtype">char</span>* p=(<span class="keyword">const</span> <span class="keywordtype">char</span>*)((DWORD)import-&gt;Name+(DWORD)idh);
<a name="l00154"></a>00154         PIMAGE_THUNK_DATA thunk=(PIMAGE_THUNK_DATA)((DWORD)idh+import-&gt;FirstThunk);
<a name="l00155"></a>00155         <a class="code" href="class_i_o_fluke_1_1_c_emulator.php#20aa1a095de98625cefe76ae76025f71">ParseImport</a>(dllname,thunk);
<a name="l00156"></a>00156         <span class="keyword">import</span>++;
<a name="l00157"></a>00157       }
<a name="l00158"></a>00158     }
<a name="l00159"></a>00159   }
<a name="l00160"></a>00160 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="265c7066547b8c985a7c152b53f29612"></a><!-- doxytag: member="IOFluke::CEmulator::ParseMemory" ref="265c7066547b8c985a7c152b53f29612" args="(HINSTANCE curinstance)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void IOFluke::CEmulator::ParseMemory           </td>
          <td>(</td>
          <td class="paramtype">HINSTANCE&nbsp;</td>
          <td class="paramname"> <em>curinstance</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
обработать указаанную область памяти на наличие фукнций хока.
<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00162">162</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.
<p>
References <a class="el" href="debug_8h-source.php#l00045">DBGTRACE</a>, <a class="el" href="emulator_8cpp-source.php#l00041">GetModuleName()</a>, <a class="el" href="emulator_8cpp-source.php#l00048">IsBadCodePtr_my()</a>, <a class="el" href="emulator_8h-source.php#l00089">m_cs</a>, and <a class="el" href="emulator_8cpp-source.php#l00134">ParseModule()</a>.<div class="fragment"><pre class="fragment"><a name="l00163"></a>00163 {
<a name="l00164"></a>00164   <a class="code" href="class_sentry_sc.php">SentrySc</a> sc(<a class="code" href="class_i_o_fluke_1_1_c_emulator.php#8486f71492197279cc77edb8d1ce1a40">m_cs</a>);
<a name="l00165"></a>00165
<a name="l00166"></a>00166   <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"ParseMemory\n"</span>);
<a name="l00167"></a>00167
<a name="l00168"></a>00168   std::string &amp;curmodname=<a class="code" href="emulator_8cpp.php#cda7731403cbaea2e632feb47cb2a8de">GetModuleName</a>(curinstance);
<a name="l00169"></a>00169
<a name="l00170"></a>00170   DWORD address=0x00400000;
<a name="l00171"></a>00171   <span class="keywordflow">while</span>(address&lt;0x7fffffff)
<a name="l00172"></a>00172   {
<a name="l00173"></a>00173     PIMAGE_DOS_HEADER idh=(PIMAGE_DOS_HEADER)address;
<a name="l00174"></a>00174     <span class="keywordflow">if</span>(!<a class="code" href="emulator_8cpp.php#b1b4996810117fe2a48b3cb46aab871b">IsBadCodePtr_my</a>((LPDWORD)address)&amp;&amp;
<a name="l00175"></a>00175       idh-&gt;e_magic=='ZM')
<a name="l00176"></a>00176     {
<a name="l00177"></a>00177       std::string &amp;modname=<a class="code" href="emulator_8cpp.php#cda7731403cbaea2e632feb47cb2a8de">GetModuleName</a>((HMODULE)address);
<a name="l00178"></a>00178       <span class="keywordflow">if</span>(modname!=curmodname)
<a name="l00179"></a>00179       {
<a name="l00180"></a>00180         <a class="code" href="debug_8h.php#7c951419644bd217209359b0206a19c9">DBGTRACE</a>(<span class="stringliteral">"parse '%s'\n"</span>,modname.c_str());
<a name="l00181"></a>00181         <a class="code" href="class_i_o_fluke_1_1_c_emulator.php#3612605935876e4bf08b28dadd7e9c70">ParseModule</a>((HMODULE)idh);
<a name="l00182"></a>00182       }
<a name="l00183"></a>00183     }
<a name="l00184"></a>00184     address+=0x1000;
<a name="l00185"></a>00185   }
<a name="l00186"></a>00186 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="54fea31991498f9b4371f03f019823d9"></a><!-- doxytag: member="IOFluke::CEmulator::m_roolbacklist" ref="54fea31991498f9b4371f03f019823d9" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_i_o_fluke_1_1_c_emulator.php#979207cc61b92d5582bfaa800f422bea">roolbacklist_t</a> <a class="el" href="class_i_o_fluke_1_1_c_emulator.php#54fea31991498f9b4371f03f019823d9">IOFluke::CEmulator::m_roolbacklist</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
список адреов на память которые были заменены.
<p>

<p>
Definition at line <a class="el" href="emulator_8h-source.php#l00086">86</a> of file <a class="el" href="emulator_8h-source.php">emulator.h</a>.
<p>
Referenced by <a class="el" href="emulator_8cpp-source.php#l00198">Close()</a>, and <a class="el" href="emulator_8cpp-source.php#l00116">ParseImport()</a>.
</div>
</div><p>
<a class="anchor" name="b98d8d89131d5a8420b7165b5255166b"></a><!-- doxytag: member="IOFluke::CEmulator::m_hooklist" ref="b98d8d89131d5a8420b7165b5255166b" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_i_o_fluke_1_1_hook_list.php">HookList</a>* <a class="el" href="class_i_o_fluke_1_1_c_emulator.php#b98d8d89131d5a8420b7165b5255166b">IOFluke::CEmulator::m_hooklist</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
указатель на массив хоков которые нужно поставить.
<p>

<p>
Definition at line <a class="el" href="emulator_8h-source.php#l00088">88</a> of file <a class="el" href="emulator_8h-source.php">emulator.h</a>.
<p>
Referenced by <a class="el" href="emulator_8cpp-source.php#l00097">CEmulator()</a>, <a class="el" href="emulator_8cpp-source.php#l00091">Create()</a>, and <a class="el" href="emulator_8cpp-source.php#l00103">GetHook()</a>.
</div>
</div><p>
<a class="anchor" name="8486f71492197279cc77edb8d1ce1a40"></a><!-- doxytag: member="IOFluke::CEmulator::m_cs" ref="8486f71492197279cc77edb8d1ce1a40" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">CComAutoCriticalSection <a class="el" href="class_i_o_fluke_1_1_c_emulator.php#8486f71492197279cc77edb8d1ce1a40">IOFluke::CEmulator::m_cs</a><code> [private]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="emulator_8h-source.php#l00089">89</a> of file <a class="el" href="emulator_8h-source.php">emulator.h</a>.
<p>
Referenced by <a class="el" href="emulator_8cpp-source.php#l00097">CEmulator()</a>, <a class="el" href="emulator_8cpp-source.php#l00198">Close()</a>, <a class="el" href="emulator_8cpp-source.php#l00091">Create()</a>, <a class="el" href="emulator_8cpp-source.php#l00103">GetHook()</a>, <a class="el" href="emulator_8cpp-source.php#l00116">ParseImport()</a>, <a class="el" href="emulator_8cpp-source.php#l00162">ParseMemory()</a>, and <a class="el" href="emulator_8cpp-source.php#l00134">ParseModule()</a>.
</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="emulator_8h-source.php">emulator.h</a><li><a class="el" href="emulator_8cpp-source.php">emulator.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
