<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0e80f8b385e544f89c616410da1961e3.php">ClientAddin</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_9ea6ca00291899751f09ddc725b8bbcc.php">clientidentify</a></div>
<h1>GZ/ClientAddin/clientidentify/StdAfx.h</h1><a href="_g_z_2_client_addin_2clientidentify_2_std_afx_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// stdafx.h : include file for standard system include files,</span>
<a name="l00002"></a>00002 <span class="comment">//  or project specific include files that are used frequently, but</span>
<a name="l00003"></a>00003 <span class="comment">//      are changed infrequently</span>
<a name="l00004"></a>00004 <span class="comment">//</span>
<a name="l00005"></a>00005
<a name="l00006"></a>00006 <span class="preprocessor">#if !defined(AFX_STDAFX_H__F1E887BA_1EAB_427A_844A_005DF8BBD9D5__INCLUDED_)</span>
<a name="l00007"></a><a class="code" href="_g_z_2_client_addin_2clientidentify_2_std_afx_8h.php#4ba2ecab36f6fa9a2641cd537107bdc5">00007</a> <span class="preprocessor"></span><span class="preprocessor">#define AFX_STDAFX_H__F1E887BA_1EAB_427A_844A_005DF8BBD9D5__INCLUDED_</span>
<a name="l00008"></a>00008 <span class="preprocessor"></span>
<a name="l00009"></a>00009 <span class="preprocessor">#if _MSC_VER &gt; 1000</span>
<a name="l00010"></a>00010 <span class="preprocessor"></span><span class="preprocessor">#pragma once</span>
<a name="l00011"></a>00011 <span class="preprocessor"></span><span class="preprocessor">#endif // _MSC_VER &gt; 1000</span>
<a name="l00012"></a>00012 <span class="preprocessor"></span>
<a name="l00013"></a><a class="code" href="_g_z_2_client_addin_2clientidentify_2_std_afx_8h.php#c7bef5d85e3dcd73eef56ad39ffc84a9">00013</a> <span class="preprocessor">#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers</span>
<a name="l00014"></a>00014 <span class="preprocessor"></span>
<a name="l00015"></a>00015
<a name="l00016"></a>00016 <span class="comment">// TODO: reference additional headers your program requires here</span>
<a name="l00017"></a>00017
<a name="l00018"></a>00018 <span class="comment">//{{AFX_INSERT_LOCATION}}</span>
<a name="l00019"></a>00019 <span class="comment">// Microsoft Visual C++ will insert additional declarations immediately before the previous line.</span>
<a name="l00020"></a>00020
<a name="l00021"></a>00021 <span class="preprocessor">#endif // !defined(AFX_STDAFX_H__F1E887BA_1EAB_427A_844A_005DF8BBD9D5__INCLUDED_)</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
