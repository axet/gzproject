<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class=doxygen>
<div class=page>
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_7b5764f57df50b92135424528e3a194b.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_3a8d4ec50afa7ad93eb4c33da4227133.php">ServerAddin</a></div>
<h1>ServerAddin Directory Reference</h1><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Directories</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">directory &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="dir_0dad7c95b0b71b6c9bbff267a3edabb3.php">AccountManager</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">directory &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="dir_61248b2777b622162df040af2b6489db.php">firewall</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">directory &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="dir_3381f1b0b81d3e94209224791682b441.php">hearall</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">directory &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="dir_59469c26a298a4e3d204925a93f384bc.php">speedhack</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">directory &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="dir_421f9af976c9635dc26af3c787d6a4da.php">xsd</a></td></tr>

<tr><td colspan="2"><br><h2>Files</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">file &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="_g_z_2_server_addin_2resource_8h.php">GZ/ServerAddin/resource.h</a> <a href="_g_z_2_server_addin_2resource_8h-source.php">[code]</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">file &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="serveraddin_8cpp.php">serveraddin.cpp</a> <a href="serveraddin_8cpp-source.php">[code]</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">file &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="serveraddin_8h.php">serveraddin.h</a> <a href="serveraddin_8h-source.php">[code]</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">file &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="_server_addin_module_8h.php">ServerAddinModule.h</a> <a href="_server_addin_module_8h-source.php">[code]</a></td></tr>

</table>
<!--footer -->
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
