<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>IDebuggerWindowsVtbl Struct Reference</h1><!-- doxytag: class="IDebuggerWindowsVtbl" --><code>#include &lt;<a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>&gt;</code>
<p>
<a href="struct_i_debugger_windows_vtbl-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">BEGIN_INTERFACE&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#159a345f81a5e3193d2c049e03882666">HRESULT</a> (STDMETHODCALLTYPE *QueryInterface)(<a class="el" href="interface_i_debugger_windows.php">IDebuggerWindows</a> *This</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#83c6424aa2a88b7f941653067c35bdaa">ULONG</a> (STDMETHODCALLTYPE *AddRef)(<a class="el" href="interface_i_debugger_windows.php">IDebuggerWindows</a> *This)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#a017d84c79d08499b0c40ae607446e40">ULONG</a> (STDMETHODCALLTYPE *Release)(<a class="el" href="interface_i_debugger_windows.php">IDebuggerWindows</a> *This)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#c9dfddd9f3158b792c914f1726ea153a">HRESULT</a> (STDMETHODCALLTYPE *GetTypeInfoCount)(<a class="el" href="interface_i_debugger_windows.php">IDebuggerWindows</a> *This</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#60b9e000bf4934fb89be73df711f936e">HRESULT</a> (STDMETHODCALLTYPE *GetTypeInfo)(<a class="el" href="interface_i_debugger_windows.php">IDebuggerWindows</a> *This</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#02b5f88760802578acf13ab21c3d1a20">HRESULT</a> (STDMETHODCALLTYPE *GetIDsOfNames)(<a class="el" href="interface_i_debugger_windows.php">IDebuggerWindows</a> *This</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#371a3ad729257d2775dd1e01a0fc598f">HRESULT</a> (STDMETHODCALLTYPE *Invoke)(<a class="el" href="interface_i_debugger_windows.php">IDebuggerWindows</a> *This</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#665abfa0d94259e3a173ae1909491c49">HRESULT</a> (STDMETHODCALLTYPE *Report)(<a class="el" href="interface_i_debugger_windows.php">IDebuggerWindows</a> *This</td></tr>

<tr><td colspan="2"><br><h2>Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">BEGIN_INTERFACE REFIID&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#c07e596581b0eb14535d5b36a17c062a">riid</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">BEGIN_INTERFACE REFIID void **&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#45e859aaa9432d27856c308c4c704a2f">ppvObject</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">UINT *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#041b4aa615318e8c0fe7a1a845b88da6">pctinfo</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">UINT&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#ef20df9cff99304650bb7b50ac16058e">iTInfo</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">UINT LCID&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#75f3f6cde375b3c595e2bd688c0b1599">lcid</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">UINT LCID ITypeInfo **&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#9f63fc89a42e3e2706e966a61168e79d">ppTInfo</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">REFIID&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#812c4e8f2cf0c7291588c02cd1d2bea4">riid</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">REFIID LPOLESTR *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#ecb8610b067e3f314b351a4aa0b85f50">rgszNames</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">REFIID LPOLESTR UINT&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#e75fd392796fabe650643e044aa9298b">cNames</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">REFIID LPOLESTR UINT LCID&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#7cf374de4973652db872c59a347c4045">lcid</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">REFIID LPOLESTR UINT LCID <br>
DISPID *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#0b68e09369ff172460e963bb3c93767f">rgDispId</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">DISPID&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#70234e4f6bcb1cac97977b36c6511b09">dispIdMember</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">DISPID REFIID&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#cfa1fd7d8b2795da2976a7e6a6691f84">riid</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">DISPID REFIID LCID&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#bdf4c328b41de6326654b3a29fa79f27">lcid</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">DISPID REFIID LCID WORD&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#f8d41a42ac34000429b03f423fd3d635">wFlags</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">DISPID REFIID LCID WORD DISPPARAMS *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#abb73cfba80259e499ab9295a8010343">pDispParams</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">DISPID REFIID LCID WORD DISPPARAMS <br>
VARIANT *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#357dccc82b6005ff03aae01f2c732ecf">pVarResult</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">DISPID REFIID LCID WORD DISPPARAMS <br>
VARIANT EXCEPINFO *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#9d61e92d2e70c68f9eb6856c8f33dd91">pExcepInfo</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">DISPID REFIID LCID WORD DISPPARAMS <br>
VARIANT EXCEPINFO UINT *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#62c279c323845dd5fe9fff7ed964063c">puArgErr</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">long&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#590507d6c9db8c0b7734a7c9efd58a87">processid</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">long long&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#81d9735c5699445f4934ed3d71d11365">threadid</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">long long BSTR&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_i_debugger_windows_vtbl.php#7f9c20e591b307721eac9b7e32dfbaf8">message</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00101">101</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="159a345f81a5e3193d2c049e03882666"></a><!-- doxytag: member="IDebuggerWindowsVtbl::HRESULT" ref="159a345f81a5e3193d2c049e03882666" args="(STDMETHODCALLTYPE *QueryInterface)(IDebuggerWindows *This" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">BEGIN_INTERFACE IDebuggerWindowsVtbl::HRESULT           </td>
          <td>(</td>
          <td class="paramtype">STDMETHODCALLTYPE *&nbsp;</td>
          <td class="paramname"> <em>QueryInterface</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<a class="anchor" name="83c6424aa2a88b7f941653067c35bdaa"></a><!-- doxytag: member="IDebuggerWindowsVtbl::ULONG" ref="83c6424aa2a88b7f941653067c35bdaa" args="(STDMETHODCALLTYPE *AddRef)(IDebuggerWindows *This)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">IDebuggerWindowsVtbl::ULONG           </td>
          <td>(</td>
          <td class="paramtype">STDMETHODCALLTYPE *&nbsp;</td>
          <td class="paramname"> <em>AddRef</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<a class="anchor" name="a017d84c79d08499b0c40ae607446e40"></a><!-- doxytag: member="IDebuggerWindowsVtbl::ULONG" ref="a017d84c79d08499b0c40ae607446e40" args="(STDMETHODCALLTYPE *Release)(IDebuggerWindows *This)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">IDebuggerWindowsVtbl::ULONG           </td>
          <td>(</td>
          <td class="paramtype">STDMETHODCALLTYPE *&nbsp;</td>
          <td class="paramname"> <em>Release</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<a class="anchor" name="c9dfddd9f3158b792c914f1726ea153a"></a><!-- doxytag: member="IDebuggerWindowsVtbl::HRESULT" ref="c9dfddd9f3158b792c914f1726ea153a" args="(STDMETHODCALLTYPE *GetTypeInfoCount)(IDebuggerWindows *This" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">IDebuggerWindowsVtbl::HRESULT           </td>
          <td>(</td>
          <td class="paramtype">STDMETHODCALLTYPE *&nbsp;</td>
          <td class="paramname"> <em>GetTypeInfoCount</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<a class="anchor" name="60b9e000bf4934fb89be73df711f936e"></a><!-- doxytag: member="IDebuggerWindowsVtbl::HRESULT" ref="60b9e000bf4934fb89be73df711f936e" args="(STDMETHODCALLTYPE *GetTypeInfo)(IDebuggerWindows *This" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">IDebuggerWindowsVtbl::HRESULT           </td>
          <td>(</td>
          <td class="paramtype">STDMETHODCALLTYPE *&nbsp;</td>
          <td class="paramname"> <em>GetTypeInfo</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<a class="anchor" name="02b5f88760802578acf13ab21c3d1a20"></a><!-- doxytag: member="IDebuggerWindowsVtbl::HRESULT" ref="02b5f88760802578acf13ab21c3d1a20" args="(STDMETHODCALLTYPE *GetIDsOfNames)(IDebuggerWindows *This" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">IDebuggerWindowsVtbl::HRESULT           </td>
          <td>(</td>
          <td class="paramtype">STDMETHODCALLTYPE *&nbsp;</td>
          <td class="paramname"> <em>GetIDsOfNames</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<a class="anchor" name="371a3ad729257d2775dd1e01a0fc598f"></a><!-- doxytag: member="IDebuggerWindowsVtbl::HRESULT" ref="371a3ad729257d2775dd1e01a0fc598f" args="(STDMETHODCALLTYPE *Invoke)(IDebuggerWindows *This" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">IDebuggerWindowsVtbl::HRESULT           </td>
          <td>(</td>
          <td class="paramtype">STDMETHODCALLTYPE *&nbsp;</td>
          <td class="paramname"> <em>Invoke</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<a class="anchor" name="665abfa0d94259e3a173ae1909491c49"></a><!-- doxytag: member="IDebuggerWindowsVtbl::HRESULT" ref="665abfa0d94259e3a173ae1909491c49" args="(STDMETHODCALLTYPE *Report)(IDebuggerWindows *This" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">IDebuggerWindowsVtbl::HRESULT           </td>
          <td>(</td>
          <td class="paramtype">STDMETHODCALLTYPE *&nbsp;</td>
          <td class="paramname"> <em>Report</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="c07e596581b0eb14535d5b36a17c062a"></a><!-- doxytag: member="IDebuggerWindowsVtbl::riid" ref="c07e596581b0eb14535d5b36a17c062a" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">BEGIN_INTERFACE REFIID <a class="el" href="struct_i_debugger_windows_vtbl.php#c07e596581b0eb14535d5b36a17c062a">IDebuggerWindowsVtbl::riid</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00107">107</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="45e859aaa9432d27856c308c4c704a2f"></a><!-- doxytag: member="IDebuggerWindowsVtbl::ppvObject" ref="45e859aaa9432d27856c308c4c704a2f" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">BEGIN_INTERFACE REFIID void** <a class="el" href="struct_i_debugger_windows_vtbl.php#45e859aaa9432d27856c308c4c704a2f">IDebuggerWindowsVtbl::ppvObject</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00107">107</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="041b4aa615318e8c0fe7a1a845b88da6"></a><!-- doxytag: member="IDebuggerWindowsVtbl::pctinfo" ref="041b4aa615318e8c0fe7a1a845b88da6" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">UINT* <a class="el" href="struct_i_debugger_windows_vtbl.php#041b4aa615318e8c0fe7a1a845b88da6">IDebuggerWindowsVtbl::pctinfo</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00118">118</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="ef20df9cff99304650bb7b50ac16058e"></a><!-- doxytag: member="IDebuggerWindowsVtbl::iTInfo" ref="ef20df9cff99304650bb7b50ac16058e" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">UINT <a class="el" href="struct_i_debugger_windows_vtbl.php#ef20df9cff99304650bb7b50ac16058e">IDebuggerWindowsVtbl::iTInfo</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00122">122</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="75f3f6cde375b3c595e2bd688c0b1599"></a><!-- doxytag: member="IDebuggerWindowsVtbl::lcid" ref="75f3f6cde375b3c595e2bd688c0b1599" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">UINT LCID <a class="el" href="struct_i_debugger_windows_vtbl.php#75f3f6cde375b3c595e2bd688c0b1599">IDebuggerWindowsVtbl::lcid</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00122">122</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="9f63fc89a42e3e2706e966a61168e79d"></a><!-- doxytag: member="IDebuggerWindowsVtbl::ppTInfo" ref="9f63fc89a42e3e2706e966a61168e79d" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">UINT LCID ITypeInfo** <a class="el" href="struct_i_debugger_windows_vtbl.php#9f63fc89a42e3e2706e966a61168e79d">IDebuggerWindowsVtbl::ppTInfo</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00122">122</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="812c4e8f2cf0c7291588c02cd1d2bea4"></a><!-- doxytag: member="IDebuggerWindowsVtbl::riid" ref="812c4e8f2cf0c7291588c02cd1d2bea4" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">REFIID <a class="el" href="struct_i_debugger_windows_vtbl.php#c07e596581b0eb14535d5b36a17c062a">IDebuggerWindowsVtbl::riid</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00128">128</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="ecb8610b067e3f314b351a4aa0b85f50"></a><!-- doxytag: member="IDebuggerWindowsVtbl::rgszNames" ref="ecb8610b067e3f314b351a4aa0b85f50" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">REFIID LPOLESTR* <a class="el" href="struct_i_debugger_windows_vtbl.php#ecb8610b067e3f314b351a4aa0b85f50">IDebuggerWindowsVtbl::rgszNames</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00128">128</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="e75fd392796fabe650643e044aa9298b"></a><!-- doxytag: member="IDebuggerWindowsVtbl::cNames" ref="e75fd392796fabe650643e044aa9298b" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">REFIID LPOLESTR UINT <a class="el" href="struct_i_debugger_windows_vtbl.php#e75fd392796fabe650643e044aa9298b">IDebuggerWindowsVtbl::cNames</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00128">128</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="7cf374de4973652db872c59a347c4045"></a><!-- doxytag: member="IDebuggerWindowsVtbl::lcid" ref="7cf374de4973652db872c59a347c4045" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">REFIID LPOLESTR UINT LCID <a class="el" href="struct_i_debugger_windows_vtbl.php#75f3f6cde375b3c595e2bd688c0b1599">IDebuggerWindowsVtbl::lcid</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00128">128</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="0b68e09369ff172460e963bb3c93767f"></a><!-- doxytag: member="IDebuggerWindowsVtbl::rgDispId" ref="0b68e09369ff172460e963bb3c93767f" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">REFIID LPOLESTR UINT LCID DISPID* <a class="el" href="struct_i_debugger_windows_vtbl.php#0b68e09369ff172460e963bb3c93767f">IDebuggerWindowsVtbl::rgDispId</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00128">128</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="70234e4f6bcb1cac97977b36c6511b09"></a><!-- doxytag: member="IDebuggerWindowsVtbl::dispIdMember" ref="70234e4f6bcb1cac97977b36c6511b09" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">DISPID <a class="el" href="struct_i_debugger_windows_vtbl.php#70234e4f6bcb1cac97977b36c6511b09">IDebuggerWindowsVtbl::dispIdMember</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00136">136</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="cfa1fd7d8b2795da2976a7e6a6691f84"></a><!-- doxytag: member="IDebuggerWindowsVtbl::riid" ref="cfa1fd7d8b2795da2976a7e6a6691f84" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">DISPID REFIID <a class="el" href="struct_i_debugger_windows_vtbl.php#c07e596581b0eb14535d5b36a17c062a">IDebuggerWindowsVtbl::riid</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00136">136</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="bdf4c328b41de6326654b3a29fa79f27"></a><!-- doxytag: member="IDebuggerWindowsVtbl::lcid" ref="bdf4c328b41de6326654b3a29fa79f27" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">DISPID REFIID LCID <a class="el" href="struct_i_debugger_windows_vtbl.php#75f3f6cde375b3c595e2bd688c0b1599">IDebuggerWindowsVtbl::lcid</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00136">136</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="f8d41a42ac34000429b03f423fd3d635"></a><!-- doxytag: member="IDebuggerWindowsVtbl::wFlags" ref="f8d41a42ac34000429b03f423fd3d635" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">DISPID REFIID LCID WORD <a class="el" href="struct_i_debugger_windows_vtbl.php#f8d41a42ac34000429b03f423fd3d635">IDebuggerWindowsVtbl::wFlags</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00136">136</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="abb73cfba80259e499ab9295a8010343"></a><!-- doxytag: member="IDebuggerWindowsVtbl::pDispParams" ref="abb73cfba80259e499ab9295a8010343" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">DISPID REFIID LCID WORD DISPPARAMS* <a class="el" href="struct_i_debugger_windows_vtbl.php#abb73cfba80259e499ab9295a8010343">IDebuggerWindowsVtbl::pDispParams</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00136">136</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="357dccc82b6005ff03aae01f2c732ecf"></a><!-- doxytag: member="IDebuggerWindowsVtbl::pVarResult" ref="357dccc82b6005ff03aae01f2c732ecf" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">DISPID REFIID LCID WORD DISPPARAMS VARIANT* <a class="el" href="struct_i_debugger_windows_vtbl.php#357dccc82b6005ff03aae01f2c732ecf">IDebuggerWindowsVtbl::pVarResult</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00136">136</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="9d61e92d2e70c68f9eb6856c8f33dd91"></a><!-- doxytag: member="IDebuggerWindowsVtbl::pExcepInfo" ref="9d61e92d2e70c68f9eb6856c8f33dd91" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">DISPID REFIID LCID WORD DISPPARAMS VARIANT EXCEPINFO* <a class="el" href="struct_i_debugger_windows_vtbl.php#9d61e92d2e70c68f9eb6856c8f33dd91">IDebuggerWindowsVtbl::pExcepInfo</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00136">136</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="62c279c323845dd5fe9fff7ed964063c"></a><!-- doxytag: member="IDebuggerWindowsVtbl::puArgErr" ref="62c279c323845dd5fe9fff7ed964063c" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">DISPID REFIID LCID WORD DISPPARAMS VARIANT EXCEPINFO UINT* <a class="el" href="struct_i_debugger_windows_vtbl.php#62c279c323845dd5fe9fff7ed964063c">IDebuggerWindowsVtbl::puArgErr</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00136">136</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="590507d6c9db8c0b7734a7c9efd58a87"></a><!-- doxytag: member="IDebuggerWindowsVtbl::processid" ref="590507d6c9db8c0b7734a7c9efd58a87" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">long <a class="el" href="struct_i_debugger_windows_vtbl.php#590507d6c9db8c0b7734a7c9efd58a87">IDebuggerWindowsVtbl::processid</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00147">147</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="81d9735c5699445f4934ed3d71d11365"></a><!-- doxytag: member="IDebuggerWindowsVtbl::threadid" ref="81d9735c5699445f4934ed3d71d11365" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">long long <a class="el" href="struct_i_debugger_windows_vtbl.php#81d9735c5699445f4934ed3d71d11365">IDebuggerWindowsVtbl::threadid</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00147">147</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<a class="anchor" name="7f9c20e591b307721eac9b7e32dfbaf8"></a><!-- doxytag: member="IDebuggerWindowsVtbl::message" ref="7f9c20e591b307721eac9b7e32dfbaf8" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">long long BSTR <a class="el" href="struct_i_debugger_windows_vtbl.php#7f9c20e591b307721eac9b7e32dfbaf8">IDebuggerWindowsVtbl::message</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_debugger_windows_8h-source.php#l00147">147</a> of file <a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a>.
</div>
</div><p>
<hr>The documentation for this struct was generated from the following file:<ul>
<li><a class="el" href="_debugger_windows_8h-source.php">DebuggerWindows.h</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
