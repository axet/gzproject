<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_9aa33d2cd8dd981e9a7ee8bcdb7921b2.php">library</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_a4343057995d8f06381c5b516c795b17.php">iofluke</a></div>
<h1>emulator.cpp File Reference</h1><code>#include &lt;atlbase.h&gt;</code><br>
<code>#include &lt;string&gt;</code><br>
<code>#include &lt;algorithm&gt;</code><br>
<code>#include &quot;<a class="el" href="emulator_8h-source.php">emulator.h</a>&quot;</code><br>
<code>#include &lt;<a class="el" href="debug_8h-source.php">debugger/debug.h</a>&gt;</code><br>
<code>#include &lt;<a class="el" href="_sentry_sc_8h-source.php">misc/SentrySc.h</a>&gt;</code><br>

<p>
<a href="emulator_8cpp-source.php">Go to the source code of this file.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">static std::string&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="emulator_8cpp.php#8e42a5262d1a9451d2479008b92573a8">GetFileName</a> (const char *path)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">static std::string&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="emulator_8cpp.php#cda7731403cbaea2e632feb47cb2a8de">GetModuleName</a> (HMODULE hmodule)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">static BOOL&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="emulator_8cpp.php#b1b4996810117fe2a48b3cb46aab871b">IsBadCodePtr_my</a> (LPDWORD f)</td></tr>

</table>
<hr><h2>Function Documentation</h2>
<a class="anchor" name="8e42a5262d1a9451d2479008b92573a8"></a><!-- doxytag: member="emulator.cpp::GetFileName" ref="8e42a5262d1a9451d2479008b92573a8" args="(const char *path)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">static std::string GetFileName           </td>
          <td>(</td>
          <td class="paramtype">const char *&nbsp;</td>
          <td class="paramname"> <em>path</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00030">30</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.
<p>
Referenced by <a class="el" href="emulator_8cpp-source.php#l00041">GetModuleName()</a>.<div class="fragment"><pre class="fragment"><a name="l00031"></a>00031 {
<a name="l00032"></a>00032   <span class="keywordtype">char</span> drive[_MAX_DRIVE];
<a name="l00033"></a>00033   <span class="keywordtype">char</span> dir[_MAX_DIR];
<a name="l00034"></a>00034   <span class="keywordtype">char</span> fname[_MAX_FNAME];
<a name="l00035"></a>00035   <span class="keywordtype">char</span> ext[_MAX_EXT];
<a name="l00036"></a>00036   _splitpath(path,drive,dir,fname,ext);
<a name="l00037"></a>00037
<a name="l00038"></a>00038   <span class="keywordflow">return</span> std::string(fname)+ext;
<a name="l00039"></a>00039 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="cda7731403cbaea2e632feb47cb2a8de"></a><!-- doxytag: member="emulator.cpp::GetModuleName" ref="cda7731403cbaea2e632feb47cb2a8de" args="(HMODULE hmodule)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">static std::string GetModuleName           </td>
          <td>(</td>
          <td class="paramtype">HMODULE&nbsp;</td>
          <td class="paramname"> <em>hmodule</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00041">41</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.
<p>
References <a class="el" href="emulator_8cpp-source.php#l00030">GetFileName()</a>, and <a class="el" href="path_8cpp-source.php#l00038">IniExt::GetModuleFileName()</a>.
<p>
Referenced by <a class="el" href="debug_8cpp-source.php#l00036">DbgTraceFileArgs()</a>, and <a class="el" href="emulator_8cpp-source.php#l00162">IOFluke::CEmulator::ParseMemory()</a>.<div class="fragment"><pre class="fragment"><a name="l00042"></a>00042 {
<a name="l00043"></a>00043   <span class="keywordtype">char</span> buf[MAX_PATH];
<a name="l00044"></a>00044   <a class="code" href="namespace_ini_ext.php#e8ec57b84b079b6b4bcac40184ab24dc">GetModuleFileName</a>((HMODULE)hmodule,buf,<span class="keyword">sizeof</span>(buf));
<a name="l00045"></a>00045   <span class="keywordflow">return</span> <a class="code" href="emulator_8cpp.php#8e42a5262d1a9451d2479008b92573a8">GetFileName</a>(buf);
<a name="l00046"></a>00046 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="b1b4996810117fe2a48b3cb46aab871b"></a><!-- doxytag: member="emulator.cpp::IsBadCodePtr_my" ref="b1b4996810117fe2a48b3cb46aab871b" args="(LPDWORD f)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">static BOOL IsBadCodePtr_my           </td>
          <td>(</td>
          <td class="paramtype">LPDWORD&nbsp;</td>
          <td class="paramname"> <em>f</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="emulator_8cpp-source.php#l00048">48</a> of file <a class="el" href="emulator_8cpp-source.php">emulator.cpp</a>.
<p>
Referenced by <a class="el" href="emulator_8cpp-source.php#l00162">IOFluke::CEmulator::ParseMemory()</a>.<div class="fragment"><pre class="fragment"><a name="l00049"></a>00049 {
<a name="l00050"></a>00050   MEMORY_BASIC_INFORMATION mbi;
<a name="l00051"></a>00051   <span class="keywordflow">if</span>(VirtualQuery(f,&amp;mbi,<span class="keyword">sizeof</span>(mbi))==0)
<a name="l00052"></a>00052     <span class="keywordflow">return</span> TRUE;
<a name="l00053"></a>00053
<a name="l00054"></a>00054   <span class="keywordflow">if</span>(mbi.State!=MEM_COMMIT)
<a name="l00055"></a>00055     <span class="keywordflow">return</span> TRUE;
<a name="l00056"></a>00056   <span class="keywordflow">if</span>(mbi.Type==MEM_PRIVATE)
<a name="l00057"></a>00057     <span class="keywordflow">return</span> TRUE;
<a name="l00058"></a>00058
<a name="l00059"></a>00059   <span class="keywordflow">if</span>(
<a name="l00060"></a>00060     <span class="comment">/*mbi.AllocationProtect&amp;PAGE_READONLY||</span>
<a name="l00061"></a>00061 <span class="comment">    mbi.AllocationProtect&amp;PAGE_READWRITE||</span>
<a name="l00062"></a>00062 <span class="comment">    mbi.AllocationProtect&amp;PAGE_EXECUTE_READ||</span>
<a name="l00063"></a>00063 <span class="comment">    mbi.AllocationProtect&amp;PAGE_EXECUTE_READWRITE||*/</span>
<a name="l00064"></a>00064     mbi.Protect&amp;PAGE_READONLY||
<a name="l00065"></a>00065     mbi.Protect&amp;PAGE_READWRITE||
<a name="l00066"></a>00066     mbi.Protect&amp;PAGE_EXECUTE_READ||
<a name="l00067"></a>00067     mbi.Protect&amp;PAGE_EXECUTE_READWRITE)
<a name="l00068"></a>00068   {
<a name="l00069"></a>00069     <span class="keywordflow">try</span>
<a name="l00070"></a>00070     {
<a name="l00071"></a>00071       DWORD d=*f;
<a name="l00072"></a>00072     }<span class="keywordflow">catch</span>(...)
<a name="l00073"></a>00073     {
<a name="l00074"></a>00074       <span class="keywordflow">return</span> TRUE;
<a name="l00075"></a>00075     }
<a name="l00076"></a>00076     <span class="keywordflow">return</span> FALSE;
<a name="l00077"></a>00077   }<span class="keywordflow">else</span>
<a name="l00078"></a>00078   {
<a name="l00079"></a>00079     <span class="keywordflow">return</span> TRUE;
<a name="l00080"></a>00080   }
<a name="l00081"></a>00081 }
</pre></div>
<p>

</div>
</div><p>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
