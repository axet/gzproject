<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>ica::CPinchItemType Member List</h1>This is the complete list of members for <a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#af46feb618d19137c884522646f9cea7">AddAmount</a>(CSchemaInt Amount)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#3dba9c9f0c60385def702c789481c0b2">AddContainerSerialFrom</a>(CSchemaInt ContainerSerialFrom)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#f81a22ad3f61590c7487c0607eec010a">AddItemType</a>(CItemType &amp;ItemType)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#f3a2edd3996efa9f4f2b082dc9de9747">CPinchItemType</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#920c982f4845eb479e32d6da1edc5022">CPinchItemType</a>(CNode &amp;rParentNode, MSXML2::IXMLDOMNodePtr spThisNode)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#4b640fedcb2954522de0acbc81ec87dc">CPinchItemType</a>(MSXML2::IXMLDOMDocument2Ptr spDoc)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#5b49a9ad5405cdc0f0abc5ef4fa7779a">GetAdvancedAmountCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#d9a7009e2354c9075ac351ca90b00ce3">GetAdvancedContainerSerialFromCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#a95fbc382223bc16d58443af4500ffe5">GetAdvancedItemTypeCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#459df1d07383a7eb8010a14d344f2937">GetAmount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#881786b3b66567e9a47dcd7985c0a171">GetAmountAt</a>(int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#ad5b8de0c0ef73b64d8e15179172cb59">GetAmountCount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#c529bfe6ad5c318edb123e724729dff6">GetAmountMaxCount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#fb4a8eea303d773d3a42fe3fd484f598">GetAmountMinCount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#959f7c8d6cff80a42b041ded1b7e640e">GetAmountValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#66fbcd222901a2ed9f31297c50ca9931">GetContainerSerialFrom</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#6224715b2670d084fa93553fd20f7abc">GetContainerSerialFromAt</a>(int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#938293789a4b67132c9891c10183546e">GetContainerSerialFromCount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#430a893aed3cc9e2f533cb8a67a7cc72">GetContainerSerialFromMaxCount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#04a4ec483a0a6add25fd319d15491604">GetContainerSerialFromMinCount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#cd620e002e1bad133348f9e3a92011f8">GetContainerSerialFromValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#0e5731428ccfba5c4aa9a54307549361">GetGroupType</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#9970e7d015426e3a574a6fa552764694">GetItemType</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#7dd5875ad453aaf802e7589af1e8875d">GetItemTypeAt</a>(int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#deb5224ee0a9aaab6a001a6b68c6a096">GetItemTypeCount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#140c6a31de4ee1d857f1a4f725590f58">GetItemTypeMaxCount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#d620b6843d1ce55dbabd9e5d04639b2d">GetItemTypeMinCount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#575a69d5116da465374c4e7affc1c2ee">GetItemTypeValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#ee40f9c598ce5bf86f911375434d7b9e">GetStartingAmountCursor</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#0c45b57f4e6723cbd87d9116b9f64a8f">GetStartingContainerSerialFromCursor</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#860712d8c5cf6bf94c07de56c9a6dba2">GetStartingItemTypeCursor</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#9f72adda47ef4bf758c72506c2176af8">HasAmount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#1a9a95534f946d9358008301e8f55747">HasContainerSerialFrom</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#b575dc07221e4a43ce4078fd3d6ba7c4">HasItemType</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#da9b189488ee6cffe81f6755f721fb6c">InsertAmountAt</a>(CSchemaInt Amount, int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#9e38c2e70469b66725f4f736ffe1cef6">InsertContainerSerialFromAt</a>(CSchemaInt ContainerSerialFrom, int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#2153afcc7e78316aab93912a845f9192">InsertItemTypeAt</a>(CItemType &amp;ItemType, int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#7ba7585d1b8d804dcc98ff5beafcc98e">RemoveAmount</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#9fd3633a10c20d0238999df4f2404356">RemoveAmountAt</a>(int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#a6e7c98d3239a6e354edecf9930556ec">RemoveContainerSerialFrom</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#93d5943b5f56314674a81610a1b85f2c">RemoveContainerSerialFromAt</a>(int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#694a0d526fe0ef7f7d2f6ba3dfd32bea">RemoveItemType</a>()</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#70eb0a27e481495ccb8711b3397c3f1c">RemoveItemTypeAt</a>(int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#e9a3a24c67a470dba00fe63982f3be5c">ReplaceAmountAt</a>(CSchemaInt Amount, int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#08720588ee51bb2702037528f648f132">ReplaceContainerSerialFromAt</a>(CSchemaInt ContainerSerialFrom, int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_pinch_item_type.php#4cde87da485167d4aa23f6cbbc847efc">ReplaceItemTypeAt</a>(CItemType &amp;ItemType, int nIndex)</td><td><a class="el" href="classica_1_1_c_pinch_item_type.php">ica::CPinchItemType</a></td><td></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
