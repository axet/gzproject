<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_b5c47bbcd07d184bde5ae950de33b51c.php">InterNet</a></div>
<h1>InterNetMaster.cpp</h1><a href="_inter_net_master_8cpp.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#include "<a class="code" href="_inter_net_master_8h.php">internetmaster.h</a>"</span>
<a name="l00002"></a>00002
<a name="l00003"></a>00003 <span class="preprocessor">#include &lt;winsock2.h&gt;</span>
<a name="l00004"></a>00004 <span class="preprocessor">#include &lt;exception&gt;</span>
<a name="l00005"></a>00005
<a name="l00006"></a><a class="code" href="class_c_inter_net_master.php#795fc85024fe4e77d460e4d9815bf3a3">00006</a> <a class="code" href="class_c_inter_net_master.php#795fc85024fe4e77d460e4d9815bf3a3">CInterNetMaster::CInterNetMaster</a>():m_socket(0)
<a name="l00007"></a>00007 {
<a name="l00008"></a>00008   WSAData wsa={0};
<a name="l00009"></a>00009   WSAStartup(MAKEWORD(2,0),&amp;wsa);
<a name="l00010"></a>00010 }
<a name="l00011"></a>00011
<a name="l00012"></a><a class="code" href="class_c_inter_net_master.php#df0ece3ba83d61ec91a60ef6f779663c">00012</a> <span class="keywordtype">void</span> <a class="code" href="class_c_inter_net_master.php#df0ece3ba83d61ec91a60ef6f779663c">CInterNetMaster::Connect</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* address,<span class="keywordtype">unsigned</span> port)
<a name="l00013"></a>00013 {
<a name="l00014"></a>00014   <a class="code" href="class_c_inter_net_master.php#ced999dfa5ac0736b344b982527522fc">m_socket</a>=(unsigned)socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
<a name="l00015"></a>00015   u_long icmd = 0;
<a name="l00016"></a>00016   <span class="keywordtype">int</span> status;
<a name="l00017"></a>00017   status=ioctlsocket(<a class="code" href="class_c_inter_net_master.php#ced999dfa5ac0736b344b982527522fc">m_socket</a>,FIONBIO,&amp;icmd);
<a name="l00018"></a>00018   sockaddr_in sin={0};
<a name="l00019"></a>00019   sin.sin_family=AF_INET;
<a name="l00020"></a>00020   sin.sin_port=htons(port);
<a name="l00021"></a>00021   <span class="keywordflow">if</span>(hostent *h=gethostbyname(address))
<a name="l00022"></a>00022   {
<a name="l00023"></a>00023     memcpy(&amp;sin.sin_addr,h-&gt;h_addr_list[0],4);
<a name="l00024"></a>00024   }
<a name="l00025"></a>00025   <span class="comment">//bind(m_socket,(sockaddr*)&amp;sin,sizeof(sin));</span>
<a name="l00026"></a>00026   status=connect(<a class="code" href="class_c_inter_net_master.php#ced999dfa5ac0736b344b982527522fc">m_socket</a>,(sockaddr*)&amp;sin,<span class="keyword">sizeof</span>(sin));
<a name="l00027"></a>00027   <span class="keywordflow">if</span>(status==-1)
<a name="l00028"></a>00028   {
<a name="l00029"></a>00029     <a class="code" href="class_c_inter_net_master.php#e86ae6e7e6843a2bc45d35d6d74489cf">Disconnect</a>();
<a name="l00030"></a>00030         <span class="keywordflow">throw</span> std::exception(<span class="stringliteral">"can't connect"</span>);
<a name="l00031"></a>00031   }
<a name="l00032"></a>00032 }
<a name="l00033"></a>00033
<a name="l00034"></a><a class="code" href="class_c_inter_net_master.php#f30095bc5d18868772ffc7bf03c99e5a">00034</a> <span class="keywordtype">bool</span> <a class="code" href="class_c_inter_net_master.php#f30095bc5d18868772ffc7bf03c99e5a">CInterNetMaster::IsConnected</a>()
<a name="l00035"></a>00035 {
<a name="l00036"></a>00036   <span class="keywordflow">return</span> <a class="code" href="class_c_inter_net_master.php#ced999dfa5ac0736b344b982527522fc">m_socket</a>!=0;
<a name="l00037"></a>00037 }
<a name="l00038"></a>00038
<a name="l00039"></a><a class="code" href="class_c_inter_net_master.php#e86ae6e7e6843a2bc45d35d6d74489cf">00039</a> <span class="keywordtype">void</span> <a class="code" href="class_c_inter_net_master.php#e86ae6e7e6843a2bc45d35d6d74489cf">CInterNetMaster::Disconnect</a>()
<a name="l00040"></a>00040 {
<a name="l00041"></a>00041   <span class="keywordflow">if</span>(<a class="code" href="class_c_inter_net_master.php#ced999dfa5ac0736b344b982527522fc">m_socket</a>!=0)
<a name="l00042"></a>00042   {
<a name="l00043"></a>00043     closesocket(<a class="code" href="class_c_inter_net_master.php#ced999dfa5ac0736b344b982527522fc">m_socket</a>);
<a name="l00044"></a>00044     <a class="code" href="class_c_inter_net_master.php#ced999dfa5ac0736b344b982527522fc">m_socket</a>=0;
<a name="l00045"></a>00045   }
<a name="l00046"></a>00046 }
<a name="l00047"></a><a class="code" href="class_c_inter_net_master.php#a63922cfe9880892a15a7f7d3d5d2a0c">00047</a> <span class="keywordtype">void</span> <a class="code" href="class_c_inter_net_master.php#a63922cfe9880892a15a7f7d3d5d2a0c">CInterNetMaster::DoProcess</a>()
<a name="l00048"></a>00048 {
<a name="l00049"></a>00049   ;
<a name="l00050"></a>00050 }
<a name="l00051"></a>00051
<a name="l00052"></a><a class="code" href="class_c_inter_net_master.php#0ea035a60c5d7046f3e22ac04d186145">00052</a> <span class="keywordtype">void</span> <a class="code" href="class_c_inter_net_master.php#0ea035a60c5d7046f3e22ac04d186145">CInterNetMaster::Write</a>(<span class="keyword">const</span> <span class="keywordtype">void</span>* buf,<span class="keywordtype">int</span> bufsize)
<a name="l00053"></a>00053 {
<a name="l00054"></a>00054   send(<a class="code" href="class_c_inter_net_master.php#ced999dfa5ac0736b344b982527522fc">m_socket</a>,(<span class="keywordtype">char</span>*)buf,bufsize,0);
<a name="l00055"></a>00055 }
<a name="l00056"></a>00056
<a name="l00057"></a><a class="code" href="class_c_inter_net_master.php#4670a93dc53f84fdc840527d0677a898">00057</a> <span class="keywordtype">void</span> <a class="code" href="class_c_inter_net_master.php#4670a93dc53f84fdc840527d0677a898">CInterNetMaster::Read</a>(<span class="keywordtype">unsigned</span> <span class="keywordtype">char</span>* buf,<span class="keywordtype">int</span> *bufsize)
<a name="l00058"></a>00058 {
<a name="l00059"></a>00059   *bufsize=recv(<a class="code" href="class_c_inter_net_master.php#ced999dfa5ac0736b344b982527522fc">m_socket</a>,(<span class="keywordtype">char</span>*)buf,*bufsize,0);
<a name="l00060"></a>00060 }
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
