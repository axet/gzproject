<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li id="current"><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="functions.php"><span>All</span></a></li>
    <li><a href="functions_func.php"><span>Functions</span></a></li>
    <li id="current"><a href="functions_vars.php"><span>Variables</span></a></li>
    <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    <li><a href="functions_enum.php"><span>Enumerations</span></a></li>
    <li><a href="functions_eval.php"><span>Enumerator</span></a></li>
    <li><a href="functions_prop.php"><span>Properties</span></a></li>
    <li><a href="functions_rela.php"><span>Related&nbsp;Functions</span></a></li>
  </ul>
</div>
<div class="tabs">
  <ul>
    <li><a href="functions_vars.php#index__"><span>_</span></a></li>
    <li><a href="functions_vars_0x61.php#index_a"><span>a</span></a></li>
    <li><a href="functions_vars_0x62.php#index_b"><span>b</span></a></li>
    <li><a href="functions_vars_0x63.php#index_c"><span>c</span></a></li>
    <li><a href="functions_vars_0x64.php#index_d"><span>d</span></a></li>
    <li><a href="functions_vars_0x65.php#index_e"><span>e</span></a></li>
    <li><a href="functions_vars_0x66.php#index_f"><span>f</span></a></li>
    <li><a href="functions_vars_0x67.php#index_g"><span>g</span></a></li>
    <li><a href="functions_vars_0x68.php#index_h"><span>h</span></a></li>
    <li><a href="functions_vars_0x69.php#index_i"><span>i</span></a></li>
    <li><a href="functions_vars_0x6a.php#index_j"><span>j</span></a></li>
    <li><a href="functions_vars_0x6b.php#index_k"><span>k</span></a></li>
    <li><a href="functions_vars_0x6c.php#index_l"><span>l</span></a></li>
    <li><a href="functions_vars_0x6d.php#index_m"><span>m</span></a></li>
    <li><a href="functions_vars_0x6e.php#index_n"><span>n</span></a></li>
    <li><a href="functions_vars_0x6f.php#index_o"><span>o</span></a></li>
    <li><a href="functions_vars_0x70.php#index_p"><span>p</span></a></li>
    <li><a href="functions_vars_0x72.php#index_r"><span>r</span></a></li>
    <li><a href="functions_vars_0x73.php#index_s"><span>s</span></a></li>
    <li><a href="functions_vars_0x74.php#index_t"><span>t</span></a></li>
    <li id="current"><a href="functions_vars_0x75.php#index_u"><span>u</span></a></li>
    <li><a href="functions_vars_0x76.php#index_v"><span>v</span></a></li>
    <li><a href="functions_vars_0x77.php#index_w"><span>w</span></a></li>
    <li><a href="functions_vars_0x78.php#index_x"><span>x</span></a></li>
    <li><a href="functions_vars_0x79.php#index_y"><span>y</span></a></li>
    <li><a href="functions_vars_0x7a.php#index_z"><span>z</span></a></li>
  </ul>
</div>

<p>
&nbsp;
<p>
<h3><a class="anchor" name="index_u">- u -</a></h3><ul>
<li>uCallbackMessage
: <a class="el" href="struct___n_o_t_i_f_y_i_c_o_n_d_a_t_a_a.php#5b776a58252c4c93ea262ee8b819ad9c">_NOTIFYICONDATAA</a>
<li>uFlags
: <a class="el" href="struct___n_o_t_i_f_y_i_c_o_n_d_a_t_a_a.php#05c2a3ec2bbc15efa01da484dcb53397">_NOTIFYICONDATAA</a>
<li>uID
: <a class="el" href="struct___n_o_t_i_f_y_i_c_o_n_d_a_t_a_a.php#f1ccfbda6fb1110f07682714c6b78e52">_NOTIFYICONDATAA</a>
<li>unitnativeclass
: <a class="el" href="class_unit_native_manager.php#523631f3ca50ce3ed87e43e79dd86e71">UnitNativeManager</a>
<li>unitnativeinit
: <a class="el" href="class_unit_native_manager.php#a9a537ee6f0dc21c8e3b8f00109866ea">UnitNativeManager</a>
<li>unk
: <a class="el" href="struct_packets_1_1scs_login.php#0c65a7434b8a5c2e01ff385faeb12436">Packets::scsLogin</a>
, <a class="el" href="structcrypt203__tag_1_1crypt203second__tag.php#af970692c246bcba4b1180deef447929">crypt203_tag::crypt203second_tag</a>
, <a class="el" href="struct_packets_1_1scs_open_gump_1_1scs_open_gump_ext__tag.php#4dfdcc662aec542b3ac28242bad56822">Packets::scsOpenGump::scsOpenGumpExt_tag</a>
<li>unk1
: <a class="el" href="struct_packets_1_1scs_choise_option.php#11eff4b05ef706c591f9d880b12d19e7">Packets::scsChoiseOption</a>
, <a class="el" href="structcrypt203__tag_1_1crypt203first__tag.php#943867366829c79ceaa49e725e2da3d4">crypt203_tag::crypt203first_tag</a>
, <a class="el" href="struct_packets_1_1scs_add_object.php#5d9cd462569d661951eff6cfc6336d99">Packets::scsAddObject</a>
, <a class="el" href="struct_packets_1_1scs_login_confirm.php#c01f1e4a9a88acbfca1cdd02295a1d76">Packets::scsLoginConfirm</a>
<li>unk2
: <a class="el" href="struct_packets_1_1scs_login_confirm.php#071180cd0e34547df3ef0108ae9b0309">Packets::scsLoginConfirm</a>
, <a class="el" href="structcrypt203__tag_1_1crypt203first__tag.php#653e47014fc2933d2634d5e49f11e8fd">crypt203_tag::crypt203first_tag</a>
<li>unknown
: <a class="el" href="struct_packets_1_1scs_cities_and_chars_1_1_cities__tag_1_1_citie__tag.php#681cd049055e8f78543837780fcb2cdb">Packets::scsCitiesAndChars::Cities_tag::Citie_tag</a>
<li>Unknown
: <a class="el" href="struct_packets_1_1scs_shop_offer_1_1tag1.php#8cd2859e53cc5f06e67158f941d8cf02">Packets::scsShopOffer::tag1</a>
</ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
