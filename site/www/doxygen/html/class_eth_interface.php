<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>EthInterface Class Reference</h1><!-- doxytag: class="EthInterface" --><code>#include &lt;<a class="el" href="ethinterface_8h-source.php">ethinterface.h</a>&gt;</code>
<p>
<p>Inheritance diagram for EthInterface:
<p><center><img src="class_eth_interface.png" usemap="#EthInterface_map" border="0" alt=""></center>
<map name="EthInterface_map">
<area href="class_c_client_interface.php" alt="CClientInterface" shape="rect" coords="0,56,201,80">
<area href="class_firewall_1_1_c_client.php" alt="Firewall::CClient" shape="rect" coords="0,112,201,136">
<area href="class_firewall_1_1_c_firewall_1_1_c_client_kernel.php" alt="Firewall::CFirewall::CClientKernel" shape="rect" coords="0,168,201,192">
</map>
<a href="class_eth_interface-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Types</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">enum &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_eth_interface.php#e4bc453bc6632b9b138151ba934a2bae">MsgDir</a> { <a class="el" href="class_eth_interface.php#e4bc453bc6632b9b138151ba934a2bae028c447ee799e3d282eda460d380b2b7">DIR_SEND</a>,
<a class="el" href="class_eth_interface.php#e4bc453bc6632b9b138151ba934a2bae1bb410ecb2b2d037381c365b37d9d5be">DIR_RECV</a>,
<a class="el" href="class_eth_interface.php#e4bc453bc6632b9b138151ba934a2baee9d8b44122e01c406dc1812aaa53f6cd">DIR_BOTH</a>
 }</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">тип направления сообщения.  <a href="class_eth_interface.php#e4bc453bc6632b9b138151ba934a2bae">More...</a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">typedef std::vector&lt; unsigned <br>
char &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_eth_interface.php#9d9fbe9c5dbbebfb12dd24f115b76ae0">Array</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">массив байт.  <a href="#9d9fbe9c5dbbebfb12dd24f115b76ae0"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">typedef <a class="el" href="class_eth_interface.php#9d9fbe9c5dbbebfb12dd24f115b76ae0">Array</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_eth_interface.php#fbbe373b9d8d63ffb38617303577a2cf">Fragment</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fragment - фрагмент пакета.  <a href="#fbbe373b9d8d63ffb38617303577a2cf"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">typedef void(CClientNetEvents::*)&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_eth_interface.php#4049e3e5b14e35728d19b91037e06775">MsgHandler</a> (const void *buf, int bufsize)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">указатель на обработчика.  <a href="#4049e3e5b14e35728d19b91037e06775"></a><br></td></tr>
<tr><td colspan="2"><br><h2>Static Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">static <a class="el" href="struct_eth_interface_1_1_command.php">Command</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_eth_interface.php#e46784c8adcbf73f02a91377f26d23af">GetCommand</a> (const unsigned char *buf, int bufsize)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">не меняет фходного буфера.  <a href="#e46784c8adcbf73f02a91377f26d23af"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">static <a class="el" href="struct_eth_interface_1_1_command.php">Command</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_eth_interface.php#21bac085cd2eb137ef321746d5c821b6">GetCommand</a> (<a class="el" href="class_eth_interface.php#fbbe373b9d8d63ffb38617303577a2cf">Fragment</a> &amp;f)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">меняет входнй буфер.  <a href="#21bac085cd2eb137ef321746d5c821b6"></a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">static <a class="el" href="class_eth_interface.php#fbbe373b9d8d63ffb38617303577a2cf">Fragment</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_eth_interface.php#093fae56e06e17413e4aed86aaeb7a62">MakeFragment</a> (const unsigned char *buf, int bufsize, const <a class="el" href="struct_eth_interface_1_1_command.php">Command</a> &amp;)</td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">получает фрагмент (остаток в входном буфере).  <a href="#093fae56e06e17413e4aed86aaeb7a62"></a><br></td></tr>
<tr><td colspan="2"><br><h2>Static Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">static const <a class="el" href="struct_eth_interface_1_1_message_type.php">MessageType</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_eth_interface.php#8c1f1885f30b1d980bce4c2a26498533">m_messages</a> []</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">static const int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_eth_interface.php#0851c956278dfe8c01297f7c8792da20">m_messagescount</a></td></tr>

<tr><td colspan="2"><br><h2>Classes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">struct &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_eth_interface_1_1_command.php">Command</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">команда.  <a href="struct_eth_interface_1_1_command.php#_details">More...</a><br></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">struct &nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_eth_interface_1_1_message_type.php">MessageType</a></td></tr>

<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">описание сообщения.  <a href="struct_eth_interface_1_1_message_type.php#_details">More...</a><br></td></tr>
</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>
только сетевое общение как для клиента так и для сервера
<p>

<p>
Definition at line <a class="el" href="ethinterface_8h-source.php#l00013">13</a> of file <a class="el" href="ethinterface_8h-source.php">ethinterface.h</a>.<hr><h2>Member Typedef Documentation</h2>
<a class="anchor" name="9d9fbe9c5dbbebfb12dd24f115b76ae0"></a><!-- doxytag: member="EthInterface::Array" ref="9d9fbe9c5dbbebfb12dd24f115b76ae0" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">typedef std::vector&lt;unsigned char&gt; <a class="el" href="class_eth_interface.php#9d9fbe9c5dbbebfb12dd24f115b76ae0">EthInterface::Array</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
массив байт.
<p>

<p>
Definition at line <a class="el" href="ethinterface_8h-source.php#l00017">17</a> of file <a class="el" href="ethinterface_8h-source.php">ethinterface.h</a>.
</div>
</div><p>
<a class="anchor" name="fbbe373b9d8d63ffb38617303577a2cf"></a><!-- doxytag: member="EthInterface::Fragment" ref="fbbe373b9d8d63ffb38617303577a2cf" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">typedef <a class="el" href="class_eth_interface.php#9d9fbe9c5dbbebfb12dd24f115b76ae0">Array</a> <a class="el" href="class_eth_interface.php#fbbe373b9d8d63ffb38617303577a2cf">EthInterface::Fragment</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
Fragment - фрагмент пакета.
<p>

<p>
Definition at line <a class="el" href="ethinterface_8h-source.php#l00019">19</a> of file <a class="el" href="ethinterface_8h-source.php">ethinterface.h</a>.
</div>
</div><p>
<a class="anchor" name="4049e3e5b14e35728d19b91037e06775"></a><!-- doxytag: member="EthInterface::MsgHandler" ref="4049e3e5b14e35728d19b91037e06775" args="(const void *buf, int bufsize)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">typedef void(CClientNetEvents::*) <a class="el" href="class_eth_interface.php#4049e3e5b14e35728d19b91037e06775">EthInterface::MsgHandler</a>(const void *buf, int bufsize)          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
указатель на обработчика.
<p>

<p>
Definition at line <a class="el" href="ethinterface_8h-source.php#l00039">39</a> of file <a class="el" href="ethinterface_8h-source.php">ethinterface.h</a>.
</div>
</div><p>
<hr><h2>Member Enumeration Documentation</h2>
<a class="anchor" name="e4bc453bc6632b9b138151ba934a2bae"></a><!-- doxytag: member="EthInterface::MsgDir" ref="e4bc453bc6632b9b138151ba934a2bae" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">enum <a class="el" href="class_eth_interface.php#e4bc453bc6632b9b138151ba934a2bae">EthInterface::MsgDir</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
тип направления сообщения.
<p>
<dl compact><dt><b>Enumerator: </b></dt><dd>
<table border="0" cellspacing="2" cellpadding="0">
<tr><td valign="top"><em><a class="anchor" name="e4bc453bc6632b9b138151ba934a2bae028c447ee799e3d282eda460d380b2b7"></a><!-- doxytag: member="DIR_SEND" ref="e4bc453bc6632b9b138151ba934a2bae028c447ee799e3d282eda460d380b2b7" args="" -->DIR_SEND</em>&nbsp;</td><td>
</td></tr>
<tr><td valign="top"><em><a class="anchor" name="e4bc453bc6632b9b138151ba934a2bae1bb410ecb2b2d037381c365b37d9d5be"></a><!-- doxytag: member="DIR_RECV" ref="e4bc453bc6632b9b138151ba934a2bae1bb410ecb2b2d037381c365b37d9d5be" args="" -->DIR_RECV</em>&nbsp;</td><td>
client to server </td></tr>
<tr><td valign="top"><em><a class="anchor" name="e4bc453bc6632b9b138151ba934a2baee9d8b44122e01c406dc1812aaa53f6cd"></a><!-- doxytag: member="DIR_BOTH" ref="e4bc453bc6632b9b138151ba934a2baee9d8b44122e01c406dc1812aaa53f6cd" args="" -->DIR_BOTH</em>&nbsp;</td><td>
server to client </td></tr>
</table>
</dl>

<p>
Definition at line <a class="el" href="ethinterface_8h-source.php#l00041">41</a> of file <a class="el" href="ethinterface_8h-source.php">ethinterface.h</a>.<div class="fragment"><pre class="fragment"><a name="l00042"></a>00042   {
<a name="l00043"></a>00043     <a class="code" href="class_eth_interface.php#e4bc453bc6632b9b138151ba934a2bae028c447ee799e3d282eda460d380b2b7">DIR_SEND</a>,
<a name="l00044"></a>00044     <a class="code" href="class_eth_interface.php#e4bc453bc6632b9b138151ba934a2bae1bb410ecb2b2d037381c365b37d9d5be">DIR_RECV</a>,
<a name="l00045"></a>00045     <a class="code" href="class_eth_interface.php#e4bc453bc6632b9b138151ba934a2baee9d8b44122e01c406dc1812aaa53f6cd">DIR_BOTH</a>
<a name="l00046"></a>00046   };
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="e46784c8adcbf73f02a91377f26d23af"></a><!-- doxytag: member="EthInterface::GetCommand" ref="e46784c8adcbf73f02a91377f26d23af" args="(const unsigned char *buf, int bufsize)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="struct_eth_interface_1_1_command.php">EthInterface::Command</a> EthInterface::GetCommand           </td>
          <td>(</td>
          <td class="paramtype">const unsigned char *&nbsp;</td>
          <td class="paramname"> <em>buf</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>bufsize</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
не меняет фходного буфера.
<p>

<p>
Definition at line <a class="el" href="ethinterface_8cpp-source.php#l00236">236</a> of file <a class="el" href="ethinterface_8cpp-source.php">ethinterface.cpp</a>.
<p>
References <a class="el" href="ethinterface_8h-source.php#l00056">m_messages</a>, <a class="el" href="ethinterface_8h-source.php#l00057">m_messagescount</a>, and <a class="el" href="ethinterface_8h-source.php#l00051">EthInterface::MessageType::msgsize</a>.
<p>
Referenced by <a class="el" href="ethinterface_8cpp-source.php#l00277">GetCommand()</a>, and <a class="el" href="clientinterface_8cpp-source.php#l00030">CClientInterface::PostData()</a>.<div class="fragment"><pre class="fragment"><a name="l00237"></a>00237 {
<a name="l00238"></a>00238   <span class="keywordflow">if</span>(buf[0]&gt;=<a class="code" href="class_eth_interface.php#0851c956278dfe8c01297f7c8792da20">m_messagescount</a>)
<a name="l00239"></a>00239   {
<a name="l00240"></a>00240     <span class="comment">// если команда не в указанносм списке</span>
<a name="l00241"></a>00241     <span class="comment">// нужно проглотить весь буфер</span>
<a name="l00242"></a>00242     <span class="comment">// и надеяться что следующая команда будет нормальной</span>
<a name="l00243"></a>00243     <span class="keywordflow">return</span> Command(0,bufsize);
<a name="l00244"></a>00244   }
<a name="l00245"></a>00245
<a name="l00246"></a>00246   <span class="keyword">const</span> MessageType &amp;mt=<a class="code" href="class_eth_interface.php#8c1f1885f30b1d980bce4c2a26498533">m_messages</a>[buf[0]];
<a name="l00247"></a>00247   <span class="keywordflow">if</span>(mt.msgsize==0)
<a name="l00248"></a>00248   {
<a name="l00249"></a>00249     <span class="keyword">struct </span>SimpleVairableSize
<a name="l00250"></a>00250     {
<a name="l00251"></a>00251       <a class="code" href="namespace_server.php#75b563c3fe0028838ba97e321b3af24b">Server::ServerChar</a> id;
<a name="l00252"></a>00252       <a class="code" href="class_server_1_1_server_short.php">Server::ServerShort</a> size;
<a name="l00253"></a>00253     };
<a name="l00254"></a>00254     <span class="keywordflow">if</span>(bufsize&gt;=<span class="keyword">sizeof</span>(SimpleVairableSize))
<a name="l00255"></a>00255     {
<a name="l00256"></a>00256       SimpleVairableSize *p=(SimpleVairableSize*)buf;
<a name="l00257"></a>00257       <span class="keywordflow">if</span>(p-&gt;size&gt;bufsize)
<a name="l00258"></a>00258         <span class="keywordflow">return</span> Command();
<a name="l00259"></a>00259       <span class="keywordflow">else</span>
<a name="l00260"></a>00260         <span class="keywordflow">return</span> Command(0,p-&gt;size);
<a name="l00261"></a>00261     }<span class="keywordflow">else</span>
<a name="l00262"></a>00262     {
<a name="l00263"></a>00263       <span class="keywordflow">return</span> Command();
<a name="l00264"></a>00264     }
<a name="l00265"></a>00265   }<span class="keywordflow">else</span>
<a name="l00266"></a>00266   {
<a name="l00267"></a>00267     <span class="keywordflow">if</span>(mt.msgsize&lt;=bufsize)
<a name="l00268"></a>00268     {
<a name="l00269"></a>00269       <span class="keywordflow">return</span> Command(0,mt.msgsize);
<a name="l00270"></a>00270     }<span class="keywordflow">else</span>
<a name="l00271"></a>00271     {
<a name="l00272"></a>00272       <span class="keywordflow">return</span> Command();
<a name="l00273"></a>00273     }
<a name="l00274"></a>00274   }
<a name="l00275"></a>00275 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="21bac085cd2eb137ef321746d5c821b6"></a><!-- doxytag: member="EthInterface::GetCommand" ref="21bac085cd2eb137ef321746d5c821b6" args="(Fragment &amp;f)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="struct_eth_interface_1_1_command.php">EthInterface::Command</a> EthInterface::GetCommand           </td>
          <td>(</td>
          <td class="paramtype"><a class="el" href="class_eth_interface.php#fbbe373b9d8d63ffb38617303577a2cf">Fragment</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>f</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
меняет входнй буфер.
<p>

<p>
Definition at line <a class="el" href="ethinterface_8cpp-source.php#l00277">277</a> of file <a class="el" href="ethinterface_8cpp-source.php">ethinterface.cpp</a>.
<p>
References <a class="el" href="ethinterface_8h-source.php#l00023">EthInterface::Command::begin</a>, <a class="el" href="ethinterface_8h-source.php#l00033">EthInterface::Command::Empty()</a>, <a class="el" href="ethinterface_8h-source.php#l00024">EthInterface::Command::end</a>, and <a class="el" href="ethinterface_8cpp-source.php#l00236">GetCommand()</a>.<div class="fragment"><pre class="fragment"><a name="l00278"></a>00278 {
<a name="l00279"></a>00279   Command &amp;cmd=<a class="code" href="class_eth_interface.php#e46784c8adcbf73f02a91377f26d23af">GetCommand</a>(&amp;f.front(),std::distance(f.begin(),f.end()));
<a name="l00280"></a>00280   <span class="keywordflow">if</span>(!cmd.Empty())
<a name="l00281"></a>00281     f.erase(f.begin()+cmd.begin,f.begin()+cmd.end);
<a name="l00282"></a>00282   <span class="keywordflow">return</span> cmd;
<a name="l00283"></a>00283 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="093fae56e06e17413e4aed86aaeb7a62"></a><!-- doxytag: member="EthInterface::MakeFragment" ref="093fae56e06e17413e4aed86aaeb7a62" args="(const unsigned char *buf, int bufsize, const Command &amp;)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_eth_interface.php#fbbe373b9d8d63ffb38617303577a2cf">EthInterface::Fragment</a> EthInterface::MakeFragment           </td>
          <td>(</td>
          <td class="paramtype">const unsigned char *&nbsp;</td>
          <td class="paramname"> <em>buf</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>bufsize</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">const <a class="el" href="struct_eth_interface_1_1_command.php">Command</a> &amp;&nbsp;</td>
          <td class="paramname"></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [static]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
получает фрагмент (остаток в входном буфере).
<p>

<p>
Definition at line <a class="el" href="ethinterface_8cpp-source.php#l00285">285</a> of file <a class="el" href="ethinterface_8cpp-source.php">ethinterface.cpp</a>.
<p>
References <a class="el" href="ethinterface_8h-source.php#l00024">EthInterface::Command::end</a>.
<p>
Referenced by <a class="el" href="clientinterface_8cpp-source.php#l00030">CClientInterface::PostData()</a>.<div class="fragment"><pre class="fragment"><a name="l00286"></a>00286 {
<a name="l00287"></a>00287   <span class="keywordflow">return</span> <a class="code" href="class_eth_interface.php#fbbe373b9d8d63ffb38617303577a2cf">Fragment</a>(buf+cmd.end,buf+(bufsize-cmd.end));
<a name="l00288"></a>00288 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="8c1f1885f30b1d980bce4c2a26498533"></a><!-- doxytag: member="EthInterface::m_messages" ref="8c1f1885f30b1d980bce4c2a26498533" args="[]" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">const <a class="el" href="struct_eth_interface_1_1_message_type.php">EthInterface::MessageType</a> <a class="el" href="class_eth_interface.php#8c1f1885f30b1d980bce4c2a26498533">EthInterface::m_messages</a><code> [static]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="ethinterface_8h-source.php#l00056">56</a> of file <a class="el" href="ethinterface_8h-source.php">ethinterface.h</a>.
<p>
Referenced by <a class="el" href="ethinterface_8cpp-source.php#l00236">GetCommand()</a>, <a class="el" href="clientinterface_8cpp-source.php#l00006">CClientInterface::handle_receive_message()</a>, <a class="el" href="_client_u_o_commands_8cpp-source.php#l00444">CClientUOCommands::SentryCommand::NotAnticipatedCommand::NotAnticipatedCommand()</a>, <a class="el" href="clientinterface_8cpp-source.php#l00030">CClientInterface::PostData()</a>, and <a class="el" href="_client_u_o_commands_8cpp-source.php#l00429">CClientUOCommands::SentryCommand::SentryCommandTimeout::SentryCommandTimeout()</a>.
</div>
</div><p>
<a class="anchor" name="0851c956278dfe8c01297f7c8792da20"></a><!-- doxytag: member="EthInterface::m_messagescount" ref="0851c956278dfe8c01297f7c8792da20" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">const int <a class="el" href="class_eth_interface.php#0851c956278dfe8c01297f7c8792da20">EthInterface::m_messagescount</a><code> [static]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="ethinterface_8h-source.php#l00057">57</a> of file <a class="el" href="ethinterface_8h-source.php">ethinterface.h</a>.
<p>
Referenced by <a class="el" href="ethinterface_8cpp-source.php#l00236">GetCommand()</a>, and <a class="el" href="clientinterface_8cpp-source.php#l00030">CClientInterface::PostData()</a>.
</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="ethinterface_8h-source.php">ethinterface.h</a><li><a class="el" href="ethinterface_8cpp-source.php">ethinterface.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
