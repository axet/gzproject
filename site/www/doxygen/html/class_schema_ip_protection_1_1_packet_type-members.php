<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>SchemaIpProtection::PacketType Member List</h1>This is the complete list of members for <a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#0a78ede59ab2e9e668d4b524f1fa097f">AddGetUsersList</a>(GetUsersListType newValue)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#fc26932e790d73e6a098ccf65445bcdd">AddSetUsersList</a>(SetUsersListType newValue)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#c75ea55d1cdcb9119c14c8e7598d27fd">AppendDomChild</a>(NodeType type, string URI, string name, string Value)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#b9500bc54742bc18fb283eb45b43b082">AppendDomElement</a>(string URI, string name, Node node)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd023962cf00a3090d9ee75baf9dd110c94696">Attribute</a> enum value</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#b14e22fa0a2f7034edef6ab4b2bf8491">CloneDomElementAs</a>(string URI, string name, Node node)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#05faa22e4d9628b90007a19fca8681f2">DeclareNamespace</a>(string prefix, string URI)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#5a78f4a7e638948db52a50bf27f97277">DomChildCount</a>(NodeType type, string URI, string name)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#6a3b274665063bc1bef40363d1b69af0">domNode</a></td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239496976ad92512728af676a5330d3744e">Element</a> enum value</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#30b0b16f6517d59c5415e7e7d8f83959">GetDomChildAt</a>(NodeType type, string URI, string name, int index)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#d08b73d29b5d1a056789012444457d07">getDOMNode</a>()</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#3e9877a06a6e9021aecc6cf026c34f7a">GetDomNodeValue</a>(XmlNode node)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#3e99e2605a0fd964ec628282f3d11e98">GetGetUsersList</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#8c9d4780cf3b80cfef0d0dccdb9c31d5">GetGetUsersListAt</a>(int index)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#bc9a291873a0bc9c2205a3af6076a4d5">GetGetUsersListCount</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#8e9f609a71c0ee19735af1a650eb9f60">GetGetUsersListMaxCount</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#c714e6e5adeaa2898a5aa32b3a38f508">GetGetUsersListMinCount</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#8a2204f138b18176a7bc1fcc63c6eb35">GetSetUsersList</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#667fbaf63074cdaa467eb94569e6478c">GetSetUsersListAt</a>(int index)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#13a2e7bbf8d0ebfbfa96c85bd079d355">GetSetUsersListCount</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#c7c637f4f152dca8000f8725d8a446e6">GetSetUsersListMaxCount</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#91abc67b5bcbe835cacbe967131f77c9">GetSetUsersListMinCount</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#22caf29f497e86a36ca976c5b9344d11">HasDomChild</a>(NodeType type, string URI, string name)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#0d56420c4d52af6736cae08145fa6a60">HasGetUsersList</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#f1d5c520caae7b17cc35215ca156e4ed">HasSetUsersList</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#04fbd644a61272767ac268a6837e07b8">InsertDomChildAt</a>(NodeType type, string URI, string name, int index, string Value)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#291c76a765b719a0f540e7ee40d0168b">InsertDomElementAt</a>(string URI, string name, int index, Node node)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#b7a6914bee1a7fc3d2fe7fbe322201c0">InsertGetUsersListAt</a>(GetUsersListType newValue, int index)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#abe6e41b054186b2a0a3937bd4a69933">InsertSetUsersListAt</a>(SetUsersListType newValue, int index)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#0fbb28a68cb9ff90f6b8f59fb641ba09">MakeRoot</a>(string namespaceURI, string rootElementName, string schemaLocation)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#4615e75ff81d41edcaf651538f4e1b3c">Node</a>()</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#33450d61d677c371e288c6f1f51b4f94">Node</a>(XmlDocument doc)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#98970278457f4cfef35dcceda256a346">Node</a>(XmlNode node)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#bf0cf86ab892f0a3500418b81826d078">Node</a>(Node node)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#7e3435f11fa534be1930c390a0dd0239">NodeType</a> enum name</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#28a57645f168348ad8b19b4be641521c">PacketType</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#4aca03999735c85f88a7d8d7a4f94304">PacketType</a>(XmlDocument doc)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#19887bf5f0c641eb138f5b295ab7c40d">PacketType</a>(XmlNode node)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#d49f98c3931527dc1bc010e425186e5c">PacketType</a>(Altova.Node node)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#7938fa304858dfc0e9f8f492c9a65ecb">RemoveDomChildAt</a>(NodeType type, string URI, string name, int index)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#512a7ee6492d3bcbd1d6ab20685daa9a">RemoveGetUsersList</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#e6db033bbda4fe04882718702301db8d">RemoveGetUsersListAt</a>(int index)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#03b793782c14afe96cc8be75359051c4">RemoveSetUsersList</a>()</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#52046f3338b5f78692f0eaecce69b87a">RemoveSetUsersListAt</a>(int index)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#a99e2051ebcba9f999ee5cfa19b6072f">ReplaceDomChildAt</a>(NodeType type, string URI, string name, int index, string Value)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#b225018014944548e7123b3d934da30d">ReplaceDomElementAt</a>(string URI, string name, int index, Node node)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#7966922a5f01047dc83db1172cc9621d">ReplaceGetUsersListAt</a>(GetUsersListType newValue, int index)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php#55a4e642cf2a995f12202bb091d5338e">ReplaceSetUsersListAt</a>(SetUsersListType newValue, int index)</td><td><a class="el" href="class_schema_ip_protection_1_1_packet_type.php">SchemaIpProtection::PacketType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_altova_1_1_node.php#7699f4ff6e95c20c0f94d2b71c2c16c7">SetDomNodeValue</a>(XmlNode node, string Value)</td><td><a class="el" href="class_altova_1_1_node.php">Altova::Node</a></td><td><code> [inline, protected, static]</code></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
