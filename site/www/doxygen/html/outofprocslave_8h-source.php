<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_9dcfd5a61dba1352584094bd6fa46c11.php">interproc_clientaddin</a></div>
<h1>outofprocslave.h</h1><a href="outofprocslave_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#ifndef __OUTOFPROCSLAVE_H</span>
<a name="l00002"></a>00002 <span class="preprocessor"></span><span class="preprocessor">#define __OUTOFPROCSLAVE_H</span>
<a name="l00003"></a>00003 <span class="preprocessor"></span>
<a name="l00004"></a>00004 <span class="preprocessor">#include "<a class="code" href="interprocclientaddin_8h.php">interprocclientaddin.h</a>"</span>
<a name="l00005"></a>00005 <span class="preprocessor">#include &lt;string&gt;</span>
<a name="l00006"></a>00006 <span class="preprocessor">#include "../client/clientcommands.h"</span>
<a name="l00007"></a>00007 <span class="preprocessor">#include "../client/clientevents.h"</span>
<a name="l00008"></a>00008 <span class="preprocessor">#include "../client/packets.h"</span>
<a name="l00009"></a>00009 <span class="preprocessor">#include "../interproc/interprocslave.h"</span>
<a name="l00010"></a>00010
<a name="l00011"></a><a class="code" href="class_c_out_of_proc_slave.php">00011</a> <span class="keyword">class </span><a class="code" href="class_c_out_of_proc_slave.php">COutOfProcSlave</a>:
<a name="l00012"></a>00012   <span class="keyword">public</span> <a class="code" href="class_c_inter_proc_slave.php">CInterProcSlave</a>,
<a name="l00013"></a>00013   <span class="keyword">public</span> <a class="code" href="class_c_inter_proc_client_addin.php">CInterProcClientAddin</a>,
<a name="l00014"></a>00014   <span class="keyword">public</span> <a class="code" href="class_c_client_events.php">CClientEvents</a>,
<a name="l00015"></a>00015   <span class="keyword">public</span> <a class="code" href="class_c_client_commands.php">CClientCommands</a>
<a name="l00016"></a>00016 {
<a name="l00017"></a>00017 <span class="keyword">protected</span>:
<a name="l00018"></a>00018   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#3119ce6e1649bf28ba822c46c12aeed0">Receive</a>(<span class="keyword">const</span> <span class="keywordtype">unsigned</span> <span class="keywordtype">char</span>*p,<span class="keywordtype">int</span> psize);
<a name="l00019"></a>00019
<a name="l00020"></a>00020 <span class="keyword">public</span>:
<a name="l00021"></a>00021   <a class="code" href="class_c_out_of_proc_slave.php#58478cc0d7c6a2cbf0f0f59f7715e565">COutOfProcSlave</a>();
<a name="l00022"></a>00022   <a class="code" href="class_c_out_of_proc_slave.php#5e87f5ffb093d0e425c357701f02ae4a">~COutOfProcSlave</a>();
<a name="l00023"></a>00023
<a name="l00024"></a>00024   <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#1dd5f1aa559dc961130e6b3201e7a041">Send</a>(<a class="code" href="classica_1_1_c_packet_slave_type.php">ica::CPacketSlaveType</a> &amp;);
<a name="l00025"></a>00025   <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#2aaca4ad01cb4d6e59a780883130d69d">Exit</a>(<span class="keywordtype">unsigned</span> exitcode);
<a name="l00026"></a>00026   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#8ad363342b3eae546a5b2aef37439dc9">SelectedItem</a>(<span class="keywordtype">unsigned</span> itemserial);
<a name="l00027"></a>00027   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#f28391cb614c4f752f017810f8f85556">SelectedGround</a>(<a class="code" href="struct_world_cord.php">WorldCord</a> w,<span class="keyword">const</span> <a class="code" href="class_client_1_1_item_type.php">Client::ItemType</a> &amp;it);
<a name="l00028"></a>00028   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#2f627079fb2b3c8ffc6987d891f43da6">SelectedPath</a>(<a class="code" href="struct_world_cord.php">WorldCord</a> w);
<a name="l00029"></a>00029   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#10495cc7cf81357e6b537707b22bdb19">SetNewPlayersCoords</a>(<a class="code" href="struct_world_cord.php">WorldCord</a> w);
<a name="l00030"></a>00030   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#45fb0a82001815f8d3b02d82978a6be6">SelectedOption</a>(<span class="keyword">const</span> <a class="code" href="class_client_1_1_item_type.php">Client::ItemType</a>&amp; it);
<a name="l00031"></a>00031   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#371988fd5d983debe691ff572be5b0d7">ConnectionLost</a>();
<a name="l00032"></a>00032   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#8ac6d14313dbfe456fc9fdd4def46786">LogMessage</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* msg);
<a name="l00033"></a>00033   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#a93a0824a284a6a3bbfa16114998e354">ConnectionEstablished</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* name,<span class="keyword">const</span> <span class="keywordtype">char</span>* pass, <span class="keyword">const</span> <span class="keywordtype">char</span>* charname);
<a name="l00034"></a>00034
<a name="l00035"></a>00035   <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_slave.php#bdf63caa0b7c39bc1490c4261d3233e8">Poll</a>();
<a name="l00036"></a>00036 };
<a name="l00037"></a>00037
<a name="l00038"></a>00038 <span class="preprocessor">#endif</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
