<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>WorldCord Member List</h1>This is the complete list of members for <a class="el" href="struct_world_cord.php">WorldCord</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="struct_world_cord.php#49f6cbe8e5bb0a41d4d22447afb3b888">WorldCord</a>()</td><td><a class="el" href="struct_world_cord.php">WorldCord</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_world_cord.php#9a5744526df5f259fc5200f1e6b4966d">WorldCord</a>(int xx, int yy, int zz)</td><td><a class="el" href="struct_world_cord.php">WorldCord</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_world_cord.php#6fd88010e27144eb2fbc6f8fe2a9cfa6">x</a></td><td><a class="el" href="struct_world_cord.php">WorldCord</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_world_cord.php#bc42cc5da0f30e0570b447a5039f55c0">y</a></td><td><a class="el" href="struct_world_cord.php">WorldCord</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_world_cord.php#d4ff783a7cdda03d81e483625102564a">z</a></td><td><a class="el" href="struct_world_cord.php">WorldCord</a></td><td></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
