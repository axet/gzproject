<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_9aa33d2cd8dd981e9a7ee8bcdb7921b2.php">library</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_ab4bcf9ee1f83a647c3e4797d868bb97.php">ErrorReport</a></div>
<h1>errorreport.h</h1><a href="errorreport_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// GZProject - library, Ultima Online utils.</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2005 Alexey Kuznetsov - axet@users.sourceforge.net</span>
<a name="l00003"></a>00003
<a name="l00004"></a>00004 <span class="comment">// This program is free software; you can redistribute it and/or</span>
<a name="l00005"></a>00005 <span class="comment">// modify it under the terms of the GNU General Public License</span>
<a name="l00006"></a>00006 <span class="comment">// as published by the Free Software Foundation; either version 2</span>
<a name="l00007"></a>00007 <span class="comment">// of the License, or (at your option) any later version.</span>
<a name="l00008"></a>00008
<a name="l00009"></a>00009 <span class="comment">// This program is distributed in the hope that it will be useful,</span>
<a name="l00010"></a>00010 <span class="comment">// but WITHOUT ANY WARRANTY; without even the implied warranty of</span>
<a name="l00011"></a>00011 <span class="comment">// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the</span>
<a name="l00012"></a>00012 <span class="comment">// GNU General Public License for more details.</span>
<a name="l00013"></a>00013
<a name="l00014"></a>00014 <span class="comment">// You should have received a copy of the GNU General Public License</span>
<a name="l00015"></a>00015 <span class="comment">// along with this program; if not, write to the Free Software</span>
<a name="l00016"></a>00016 <span class="comment">// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</span>
<a name="l00017"></a>00017
<a name="l00020"></a>00020
<a name="l00021"></a>00021
<a name="l00022"></a>00022 <span class="preprocessor">#ifndef __ERRORREPORT_H__</span>
<a name="l00023"></a>00023 <span class="preprocessor"></span><span class="preprocessor">#define __ERRORREPORT_H__</span>
<a name="l00024"></a>00024 <span class="preprocessor"></span>
<a name="l00025"></a>00025 <span class="preprocessor">#include &lt;COMDEF.H&gt;</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include &lt;string&gt;</span>
<a name="l00027"></a>00027
<a name="l00028"></a>00028 <span class="preprocessor">#ifdef _DEBUG</span>
<a name="l00029"></a>00029 <span class="preprocessor"></span><span class="preprocessor">#pragma comment(lib,"ErrorReport.lib")</span>
<a name="l00030"></a>00030 <span class="preprocessor"></span><span class="preprocessor">#else</span>
<a name="l00031"></a>00031 <span class="preprocessor"></span><span class="preprocessor">#pragma comment(lib,"ErrorReport.lib")</span>
<a name="l00032"></a>00032 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00033"></a>00033 <span class="preprocessor"></span>
<a name="l00034"></a>00034 <span class="preprocessor">#ifdef WIN32</span>
<a name="l00035"></a>00035 <span class="preprocessor"></span><span class="preprocessor">#include "<a class="code" href="_get_last_error_8h.php">GetLastError.h</a>"</span>
<a name="l00036"></a>00036 <span class="preprocessor">#endif</span>
<a name="l00037"></a>00037 <span class="preprocessor"></span>
<a name="l00039"></a>00039
<a name="l00050"></a>00050
<a name="l00051"></a><a class="code" href="namespace_error_reports.php">00051</a> <span class="keyword">namespace </span>ErrorReports
<a name="l00052"></a>00052 {
<a name="l00053"></a><a class="code" href="class_error_reports_1_1_report.php">00053</a>   <span class="keyword">class </span><a class="code" href="class_error_reports_1_1_report.php">Report</a>
<a name="l00054"></a>00054   {
<a name="l00055"></a><a class="code" href="class_error_reports_1_1_report.php#83018b4b4f9d77d22678df734228d2a2">00055</a>     std::string <a class="code" href="class_error_reports_1_1_report.php#83018b4b4f9d77d22678df734228d2a2">m_str</a>;
<a name="l00056"></a><a class="code" href="class_error_reports_1_1_report.php#55b673bdce8d63cc58804d070e8e5170">00056</a>     <span class="keyword">mutable</span> std::string <a class="code" href="class_error_reports_1_1_report.php#55b673bdce8d63cc58804d070e8e5170">m_temp</a>;
<a name="l00057"></a>00057
<a name="l00058"></a>00058     std::string <a class="code" href="class_error_reports_1_1_report.php#b5e6f1ba117a55a9ecd0860995ab5a24">MakeString</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* b);
<a name="l00059"></a>00059     <a class="code" href="class_error_reports_1_1_report.php#8f63901fe71eb684571c508fe01807e4">Report</a>(std::string &amp;str);
<a name="l00060"></a>00060
<a name="l00061"></a>00061   <span class="keyword">public</span>:
<a name="l00063"></a>00063     <a class="code" href="class_error_reports_1_1_report.php#8f63901fe71eb684571c508fe01807e4">Report</a>(<span class="keywordtype">unsigned</span> <span class="keywordtype">int</span> resourceid);
<a name="l00065"></a>00065     <a class="code" href="class_error_reports_1_1_report.php#8f63901fe71eb684571c508fe01807e4">Report</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* help);
<a name="l00067"></a>00067     <a class="code" href="class_error_reports_1_1_report.php#8f63901fe71eb684571c508fe01807e4">Report</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* help,<span class="keyword">const</span> <span class="keywordtype">char</span>* what);
<a name="l00069"></a>00069     <a class="code" href="class_error_reports_1_1_report.php#8f63901fe71eb684571c508fe01807e4">Report</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* help,_com_error &amp;e);
<a name="l00070"></a>00070
<a name="l00072"></a>00072     <span class="keywordtype">void</span> <a class="code" href="class_error_reports_1_1_report.php#7981472c91e00c3d2d4bab8ca7a24840">operator +=</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* p);
<a name="l00074"></a>00074     <span class="keywordtype">void</span> <a class="code" href="class_error_reports_1_1_report.php#7981472c91e00c3d2d4bab8ca7a24840">operator +=</a>(<span class="keyword">const</span> std::string &amp;p);
<a name="l00076"></a>00076     <a class="code" href="class_error_reports_1_1_report.php">Report</a> <a class="code" href="class_error_reports_1_1_report.php#898787d5535b49fa654c1ada23cfab54">operator + </a>(<span class="keyword">const</span> <span class="keywordtype">char</span>*);
<a name="l00078"></a>00078     <a class="code" href="class_error_reports_1_1_report.php#9f3195eb5805b5cb23da3becb7e5fd63">operator const char*</a>() <span class="keyword">const</span>;
<a name="l00079"></a>00079   };
<a name="l00080"></a>00080 }
<a name="l00081"></a>00081
<a name="l00082"></a><a class="code" href="errorreport_8h.php#27a6b6fcc446cae8ea830455dc48de74">00082</a> <span class="keyword">typedef</span> <a class="code" href="class_error_reports_1_1_report.php">ErrorReports::Report</a> <a class="code" href="class_error_reports_1_1_report.php">ErrorReport</a>;
<a name="l00083"></a>00083
<a name="l00084"></a>00084 <span class="preprocessor">#endif</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
