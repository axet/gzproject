<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_9aa33d2cd8dd981e9a7ee8bcdb7921b2.php">library</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_6082230fd4939409a029fb3498f63f15.php">Altova</a></div>
<h1>SchemaTypes.h</h1><a href="_schema_types_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// SchemaTypes.h</span>
<a name="l00004"></a>00004 <span class="comment">//</span>
<a name="l00005"></a>00005 <span class="comment">// This file was generated by XMLSpy 2006 Enterprise Edition.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00008"></a>00008 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00009"></a>00009 <span class="comment">//</span>
<a name="l00010"></a>00010 <span class="comment">// Refer to the XMLSpy Documentation for further details.</span>
<a name="l00011"></a>00011 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00012"></a>00012 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment"></span>
<a name="l00015"></a>00015
<a name="l00016"></a>00016 <span class="preprocessor">#ifndef ALTOVA_SCHEMATYPES_H_INCLUDED</span>
<a name="l00017"></a>00017 <span class="preprocessor"></span><span class="preprocessor">#define ALTOVA_SCHEMATYPES_H_INCLUDED</span>
<a name="l00018"></a>00018 <span class="preprocessor"></span>
<a name="l00019"></a>00019 <span class="preprocessor">#if _MSC_VER &gt; 1000</span>
<a name="l00020"></a>00020 <span class="preprocessor"></span><span class="preprocessor">        #pragma once</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span><span class="preprocessor">#endif // _MSC_VER &gt; 1000</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023
<a name="l00024"></a>00024
<a name="l00025"></a>00025 <span class="keyword">namespace </span>altova {
<a name="l00026"></a>00026
<a name="l00027"></a>00027
<a name="l00029"></a>00029 <span class="comment">//</span>
<a name="l00030"></a>00030 <span class="comment">//  CSchemaType</span>
<a name="l00031"></a>00031 <span class="comment">//</span>
<a name="l00033"></a>00033 <span class="comment"></span>
<a name="l00034"></a>00034 <span class="keyword">class  </span>CSchemaTypeNumber;
<a name="l00035"></a>00035 <span class="keyword">class  </span>CSchemaTypeCalendar;
<a name="l00036"></a>00036
<a name="l00037"></a><a class="code" href="classaltova_1_1_c_schema_type.php">00037</a> <span class="keyword">class </span><a class="code" href="_altova_8h.php#cee729f3ae8fb135c2b991f722893c87">ALTOVA_DECLSPECIFIER</a> CSchemaType : <span class="keyword">public</span> <a class="code" href="classaltova_1_1_c_base_object.php">CBaseObject</a>
<a name="l00038"></a>00038 {
<a name="l00039"></a>00039 <span class="keyword">public</span>:
<a name="l00040"></a><a class="code" href="classaltova_1_1_c_schema_type.php#b9e25f186345b23ab8108c2a18e8dadb">00040</a>         <span class="keyword">typedef</span> <span class="keyword">enum</span> {
<a name="l00041"></a>00041                 <span class="comment">// numeric types</span>
<a name="l00042"></a>00042                 k_bool,
<a name="l00043"></a>00043                 k_byte,
<a name="l00044"></a>00044                 k_unsigned_byte,
<a name="l00045"></a>00045                 k_short,
<a name="l00046"></a>00046                 k_unsigned_short,
<a name="l00047"></a>00047                 k_int,
<a name="l00048"></a>00048                 k_unsigned_int,
<a name="l00049"></a>00049                 k_long,
<a name="l00050"></a>00050                 k_unsigned_long,
<a name="l00051"></a>00051                 k_float,
<a name="l00052"></a>00052                 k_double,
<a name="l00053"></a>00053                 k_decimal,
<a name="l00054"></a>00054                 <span class="comment">// calendar types</span>
<a name="l00055"></a>00055                 k_Duration,
<a name="l00056"></a>00056                 k_Time,
<a name="l00057"></a>00057                 k_Day,
<a name="l00058"></a>00058                 k_Month,
<a name="l00059"></a>00059                 k_MonthDay,
<a name="l00060"></a>00060                 k_Year,
<a name="l00061"></a>00061                 k_YearMonth,
<a name="l00062"></a>00062                 k_Date,
<a name="l00063"></a>00063                 k_DateTime,
<a name="l00064"></a>00064                 <span class="comment">// else</span>
<a name="l00065"></a>00065                 k_String,
<a name="l00066"></a>00066                 k_Base64Binary,
<a name="l00067"></a>00067                 k_HexBinary,
<a name="l00068"></a>00068                 <span class="comment">// ----------</span>
<a name="l00069"></a>00069                 k_count,
<a name="l00070"></a>00070                 k_unknown = -1
<a name="l00071"></a>00071         } ETypes ;
<a name="l00072"></a>00072
<a name="l00073"></a>00073         <span class="comment">// Construction</span>
<a name="l00074"></a>00074         CSchemaType();
<a name="l00075"></a>00075
<a name="l00076"></a>00076         <span class="keyword">enum</span> { TYPE = k_unknown };
<a name="l00077"></a>00077
<a name="l00078"></a>00078         <span class="keyword">enum</span> {
<a name="l00079"></a>00079                 WHITESPACE_PRESERVE,
<a name="l00080"></a>00080                 WHITESPACE_REPLACE,
<a name="l00081"></a>00081                 WHITESPACE_COLLAPSE
<a name="l00082"></a>00082         };
<a name="l00083"></a>00083
<a name="l00084"></a>00084         <span class="comment">// Accessors</span>
<a name="l00085"></a><a class="code" href="classaltova_1_1_c_schema_type.php#98a7316972c04a5abbbc648c1f76080f">00085</a>         <span class="keywordtype">bool</span>                                                                                    IsNull()<span class="keyword"> const                                          </span>{ <span class="keywordflow">return</span> m_bIsNull; }
<a name="l00086"></a><a class="code" href="classaltova_1_1_c_schema_type.php#eb677f02355916dd353e58ca6ef77499">00086</a>         <span class="keywordtype">void</span>                                                                                    SetIsNull( <span class="keywordtype">bool</span> bIsNull )                       { m_bIsNull = bIsNull; <span class="keywordflow">if</span>( bIsNull ) m_bIsEmpty = <span class="keyword">true</span>; }
<a name="l00087"></a><a class="code" href="classaltova_1_1_c_schema_type.php#73b49266e31d43311f07a954494195b9">00087</a>         <span class="keywordtype">bool</span>                                                                                    IsEmpty()<span class="keyword"> const                                         </span>{ <span class="keywordflow">return</span> m_bIsNull || m_bIsEmpty; }
<a name="l00088"></a><a class="code" href="classaltova_1_1_c_schema_type.php#58825de65caaa8216ab8bfb453f510c4">00088</a>         <span class="keywordtype">void</span>                                                                                    SetIsEmpty( <span class="keywordtype">bool</span> bIsEmpty )                     { m_bIsEmpty = bIsEmpty; <span class="keywordflow">if</span>( !bIsEmpty ) m_bIsNull = <span class="keyword">false</span>; }
<a name="l00089"></a>00089
<a name="l00090"></a>00090         <span class="comment">// --------- Interface ----------</span>
<a name="l00091"></a>00091         <span class="comment">// Operators</span>
<a name="l00092"></a><a class="code" href="classaltova_1_1_c_schema_type.php#7b2d12a4ca03327fa28d555aaa7f3fcd">00092</a>         <span class="keyword">virtual</span> <span class="keywordtype">bool</span>                                                                    ToBool()<span class="keyword"> const                                          </span>{ <span class="keywordflow">return</span> !IsEmpty(); }  <span class="comment">// like XPATH function boolean()</span>
<a name="l00093"></a>00093         <span class="keyword">virtual</span> operator                                                                tstring() <span class="keyword">const </span>= 0;
<a name="l00094"></a>00094
<a name="l00095"></a>00095
<a name="l00096"></a>00096
<a name="l00097"></a>00097
<a name="l00098"></a>00098         <span class="comment">// Operations</span>
<a name="l00099"></a><a class="code" href="classaltova_1_1_c_schema_type.php#1fd7e9ff756a6a36d4a0b93a017e55cf">00099</a>         <span class="keyword">virtual</span> <span class="keywordtype">void</span>                                                                    Empty()                                                         { m_bIsNull = <span class="keyword">false</span>; m_bIsEmpty = <span class="keyword">true</span>; }
<a name="l00100"></a>00100         <span class="keyword">virtual</span> <span class="keywordtype">bool</span>                                                                    Parse( <span class="keyword">const</span> TCHAR* szValue );
<a name="l00101"></a>00101         <span class="keyword">virtual</span> CSchemaType&amp;                                                    Assign( <span class="keyword">const</span> CSchemaType&amp; rOther );
<a name="l00102"></a>00102
<a name="l00103"></a><a class="code" href="classaltova_1_1_c_schema_type.php#64b98062b4cee63eb63a4fbac0fc57f8">00103</a>         <span class="keyword">virtual</span> <a class="code" href="classaltova_1_1_c_schema_type_number.php">CSchemaTypeNumber</a>*                                              GetSchemaTypeNumberPtr()<span class="keyword"> const          </span>{ <span class="keywordflow">return</span> NULL; }
<a name="l00104"></a><a class="code" href="classaltova_1_1_c_schema_type.php#432eab62daa94af65bbd574e4f7c4935">00104</a>         <span class="keyword">virtual</span> <a class="code" href="classaltova_1_1_c_schema_type_calendar.php">CSchemaTypeCalendar</a>*                                    GetSchemaTypeCalendarPtr()<span class="keyword"> const        </span>{ <span class="keywordflow">return</span> NULL; }
<a name="l00105"></a>00105
<a name="l00106"></a>00106         <span class="comment">// statics</span>
<a name="l00107"></a>00107         <span class="keyword">static</span> <span class="keyword">const</span> TCHAR*                                                             SchemaType2String( ETypes eType );
<a name="l00108"></a>00108
<a name="l00109"></a>00109         <span class="keyword">static</span> <span class="keywordtype">bool</span>                                                                             CompareEqual( <span class="keyword">const</span> CSchemaType&amp; rObj1, <span class="keyword">const</span> CSchemaType&amp; rObj2);
<a name="l00110"></a>00110         <span class="keyword">static</span> <span class="keywordtype">bool</span>                                                                             CompareLess( <span class="keyword">const</span> CSchemaType&amp; rObj1, <span class="keyword">const</span> CSchemaType&amp; rObj2);
<a name="l00111"></a>00111
<a name="l00112"></a><a class="code" href="classaltova_1_1_c_schema_type.php#013c846cd12eb5eabbf3a6a3fe41e59d">00112</a>         <span class="keyword">static</span> <span class="keywordtype">bool</span>                                                                             CompareNotEqual( <span class="keyword">const</span> CSchemaType&amp; rObj1, <span class="keyword">const</span> CSchemaType&amp; rObj2 )   { <span class="keywordflow">return</span> !CompareEqual(rObj1, rObj2); }
<a name="l00113"></a><a class="code" href="classaltova_1_1_c_schema_type.php#3896e0ec8bb4cd7cda1cc2577da40e69">00113</a>         <span class="keyword">static</span> <span class="keywordtype">bool</span>                                                                             CompareGreater( <span class="keyword">const</span> CSchemaType&amp; rObj1, <span class="keyword">const</span> CSchemaType&amp; rObj2 )    { <span class="keywordflow">return</span> CompareLess(rObj2, rObj1); }
<a name="l00114"></a><a class="code" href="classaltova_1_1_c_schema_type.php#62b0090e2e79bc47299fe0aefe4bc24b">00114</a>         <span class="keyword">static</span> <span class="keywordtype">bool</span>                                                                             CompareLessEqual( <span class="keyword">const</span> CSchemaType&amp; rObj1, <span class="keyword">const</span> CSchemaType&amp; rObj2 )  { <span class="keywordflow">return</span> CompareLess(rObj1, rObj2) || CompareEqual(rObj1, rObj2); }
<a name="l00115"></a><a class="code" href="classaltova_1_1_c_schema_type.php#6d97b37e2f5c08b0895dd7194361fbc6">00115</a>         <span class="keyword">static</span> <span class="keywordtype">bool</span>                                                                             CompareGreaterEqual( <span class="keyword">const</span> CSchemaType&amp; rObj1, <span class="keyword">const</span> CSchemaType&amp; rObj2 )       { <span class="keywordflow">return</span> CompareLess(rObj2, rObj1) || CompareEqual(rObj1, rObj2); }
<a name="l00116"></a>00116 <span class="keyword">protected</span>:
<a name="l00117"></a>00117
<a name="l00118"></a>00118
<a name="l00119"></a>00119 <span class="keyword">private</span>:
<a name="l00120"></a><a class="code" href="classaltova_1_1_c_schema_type.php#41870645b217fbb68ec859c1fdd0ee98">00120</a>         <span class="keywordtype">bool</span>                                                                                    m_bIsEmpty;     <span class="comment">// indicates if text-content of element is empty</span>
<a name="l00121"></a><a class="code" href="classaltova_1_1_c_schema_type.php#3f9a2db1317d11dab4ae688a5409bc87">00121</a>         <span class="keywordtype">bool</span>                                                                                    m_bIsNull;      <span class="comment">// indicates if the element is existing</span>
<a name="l00122"></a>00122 };
<a name="l00123"></a>00123
<a name="l00124"></a><a class="code" href="namespacealtova.php#fb51e507fba24d0a91d7e1568c3066b1">00124</a> <span class="keyword">inline</span> <a class="code" href="_altova_8h.php#b43f9ff08d8c8be96112aaea55a97152">tostream</a>&amp; <a class="code" href="namespacealtova.php#fb51e507fba24d0a91d7e1568c3066b1">operator&lt;&lt;</a>(<a class="code" href="_altova_8h.php#b43f9ff08d8c8be96112aaea55a97152">tostream</a>&amp; os, CSchemaType&amp; t)
<a name="l00125"></a>00125 {
<a name="l00126"></a>00126         <span class="keywordflow">return</span> os &lt;&lt; ((<a class="code" href="_altova_8h.php#e5d59c80151fb6bca8c486086a1ed05b">tstring</a>)t).c_str();
<a name="l00127"></a>00127 }
<a name="l00128"></a>00128
<a name="l00129"></a>00129 } <span class="comment">// namespace altova</span>
<a name="l00130"></a>00130
<a name="l00131"></a>00131
<a name="l00132"></a>00132
<a name="l00133"></a>00133 <span class="preprocessor">#endif // ALTOVA_SCHEMATYPES_H_INCLUDED</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
