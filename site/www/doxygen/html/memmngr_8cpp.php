<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_9aa33d2cd8dd981e9a7ee8bcdb7921b2.php">library</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_a4343057995d8f06381c5b516c795b17.php">iofluke</a></div>
<h1>memmngr.cpp File Reference</h1><code>#include &lt;atlbase.h&gt;</code><br>
<code>#include &lt;assert.h&gt;</code><br>
<code>#include &lt;new&gt;</code><br>
<code>#include &quot;<a class="el" href="memmngr_8h-source.php">memmngr.h</a>&quot;</code><br>
<code>#include &quot;<a class="el" href="emulator_8h-source.php">emulator.h</a>&quot;</code><br>
<code>#include &lt;<a class="el" href="_sentry_sc_8h-source.php">misc/SentrySc.h</a>&gt;</code><br>

<p>
<a href="memmngr_8cpp-source.php">Go to the source code of this file.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="memmngr_8cpp.php#86b9cb9276ecb5383f1badfea6b53a7e">__declspec</a> (naked) IOFluke</td></tr>

</table>
<hr><h2>Function Documentation</h2>
<a class="anchor" name="86b9cb9276ecb5383f1badfea6b53a7e"></a><!-- doxytag: member="memmngr.cpp::__declspec" ref="86b9cb9276ecb5383f1badfea6b53a7e" args="(naked) IOFluke" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void __declspec           </td>
          <td>(</td>
          <td class="paramtype">naked&nbsp;</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="memmngr_8cpp-source.php#l00173">173</a> of file <a class="el" href="memmngr_8cpp-source.php">memmngr.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00174"></a>00174 {
<a name="l00175"></a>00175   __asm
<a name="l00176"></a>00176   {
<a name="l00177"></a>00177     push ebp;
<a name="l00178"></a>00178     mov ebp,esp;
<a name="l00179"></a>00179
<a name="l00180"></a>00180     push eax;
<a name="l00181"></a>00181     push edx;
<a name="l00182"></a>00182     push ecx;
<a name="l00183"></a>00183     push esi;
<a name="l00184"></a>00184
<a name="l00185"></a>00185     <span class="comment">// адрес возврата тот в который возвращает управление перехватываемая функция</span>
<a name="l00186"></a>00186     lea eax,[ebp+0x4*4];
<a name="l00187"></a>00187     push eax
<a name="l00188"></a>00188     <span class="comment">// адрес первого параметра для перехватываемой функции</span>
<a name="l00189"></a>00189     <span class="comment">// так как будет удобно работать с параметрами через vs_args то</span>
<a name="l00190"></a>00190     <span class="comment">// я передаю адресс первого параметра минус 4 байта</span>
<a name="l00191"></a>00191     lea eax,[ebp+0x4*4];
<a name="l00192"></a>00192     push eax
<a name="l00193"></a>00193     <span class="comment">// адрес перехватываемой процедуры</span>
<a name="l00194"></a>00194     push [j]
<a name="l00195"></a>00195     <span class="comment">// this</span>
<a name="l00196"></a>00196     push [client];
<a name="l00197"></a>00197     call CreateEmulator;
<a name="l00198"></a>00198
<a name="l00199"></a>00199     <span class="comment">// enable emulator</span>
<a name="l00200"></a>00200
<a name="l00201"></a>00201     <span class="comment">// подменяю адрес возврата, чтобы потом перехватить выход из перехватываемой</span>
<a name="l00202"></a>00202     <span class="comment">// процедуры</span>
<a name="l00203"></a>00203     <span class="comment">// mov [ebp+0x4*4],eax;</span>
<a name="l00204"></a>00204
<a name="l00205"></a>00205     <span class="comment">// возвращаю управление коду который должен выполниться</span>
<a name="l00206"></a>00206     mov eax,[j];
<a name="l00207"></a>00207     mov dword ptr [ebp+4],eax;
<a name="l00208"></a>00208
<a name="l00209"></a>00209     pop esi;
<a name="l00210"></a>00210     pop ecx;
<a name="l00211"></a>00211     pop edx;
<a name="l00212"></a>00212     pop eax;
<a name="l00213"></a>00213
<a name="l00214"></a>00214     mov esp,ebp;
<a name="l00215"></a>00215     pop ebp;
<a name="l00216"></a>00216     ret 0x8;
<a name="l00217"></a>00217   }
<a name="l00218"></a>00218 }
</pre></div>
<p>

</div>
</div><p>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
