<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>ica::CCharMoveToCharType Member List</h1>This is the complete list of members for <a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#f0a1ea7e2b80a205f6c6d9617f527ac2">AddName</a>(CSchemaString Name)</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#cab865c6fc9e821156488774c14a1777">CCharMoveToCharType</a>()</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#10c8517f96780bdfac310ff268cffb6b">CCharMoveToCharType</a>(CNode &amp;rParentNode, MSXML2::IXMLDOMNodePtr spThisNode)</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#6b00296b015f832ceaa8a78283ee2693">CCharMoveToCharType</a>(MSXML2::IXMLDOMDocument2Ptr spDoc)</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#3789fe4615c67e004468dd19c53450a8">GetAdvancedNameCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#42a8f5c968f1328dd55e35874ba34422">GetGroupType</a>()</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#3955e87b159b83f0d729632302ceb387">GetName</a>()</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#55c19f6778103f5e6e9f44a677067736">GetNameAt</a>(int nIndex)</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#4863392fed62bbce59001e5fa33c0740">GetNameCount</a>()</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#ff198e218f2b92c23ccbdc0bc819e8da">GetNameMaxCount</a>()</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#d9537280739b6b441899d09a7d150caa">GetNameMinCount</a>()</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td><code> [static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#5bfe6f232e261e1bfc8be2749e50659e">GetNameValueAtCursor</a>(MSXML2::IXMLDOMNodePtr pCurNode)</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#6c403ef335dfcd1862a33419c3271a65">GetStartingNameCursor</a>()</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#3904e2d1ef6d33a034fa225694f1502e">HasName</a>()</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#aef0563c9100feea49db8e662231f2eb">InsertNameAt</a>(CSchemaString Name, int nIndex)</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#618dcde6a566c5f51e1673ef0f9ef2e7">RemoveName</a>()</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#d7cab3edb162d48baa399b2992b41881">RemoveNameAt</a>(int nIndex)</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php#8c09dec665ad4c830d5cfb655236216e">ReplaceNameAt</a>(CSchemaString Name, int nIndex)</td><td><a class="el" href="classica_1_1_c_char_move_to_char_type.php">ica::CCharMoveToCharType</a></td><td></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
