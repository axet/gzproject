<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="class_c_inter_proc_client_addin.php">CInterProcClientAddin</a>::<a class="el" href="struct_c_inter_proc_client_addin_1_1cmd_login.php">cmdLogin</a></div>
<h1>CInterProcClientAddin::cmdLogin Struct Reference</h1><!-- doxytag: class="CInterProcClientAddin::cmdLogin" --><code>#include &lt;<a class="el" href="interprocclientaddin_8h-source.php">interprocclientaddin.h</a>&gt;</code>
<p>
<a href="struct_c_inter_proc_client_addin_1_1cmd_login-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_c_inter_proc_client_addin_1_1cmd_login.php#e40a504ec08d6f6fe1090884219a7dfc">cmdLogin</a> ()</td></tr>

<tr><td colspan="2"><br><h2>Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_c_inter_proc_client_addin_1_1cmd_login.php#24be767ee1bab07b73aa1118eaec0980">null</a></td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">char&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_c_inter_proc_client_addin_1_1cmd_login.php#29341044545ba76d6157eaac60869a9a">name</a> [30]</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">char&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_c_inter_proc_client_addin_1_1cmd_login.php#717cc7d701c26abe460c4cdcb88193ec">pass</a> [30]</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">char&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="struct_c_inter_proc_client_addin_1_1cmd_login.php#f2ad03a8cbf92dadffe3e3c2d03d8ecd">charname</a> [30]</td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="interprocclientaddin_8h-source.php#l00100">100</a> of file <a class="el" href="interprocclientaddin_8h-source.php">interprocclientaddin.h</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="e40a504ec08d6f6fe1090884219a7dfc"></a><!-- doxytag: member="CInterProcClientAddin::cmdLogin::cmdLogin" ref="e40a504ec08d6f6fe1090884219a7dfc" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">CInterProcClientAddin::cmdLogin::cmdLogin           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="interprocclientaddin_8h-source.php#l00102">102</a> of file <a class="el" href="interprocclientaddin_8h-source.php">interprocclientaddin.h</a>.<div class="fragment"><pre class="fragment"><a name="l00102"></a>00102 :<a class="code" href="struct_c_inter_proc_client_addin_1_1cmd_login.php#24be767ee1bab07b73aa1118eaec0980">null</a>(<span class="keyword">false</span>){}
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="24be767ee1bab07b73aa1118eaec0980"></a><!-- doxytag: member="CInterProcClientAddin::cmdLogin::null" ref="24be767ee1bab07b73aa1118eaec0980" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">bool <a class="el" href="struct_c_inter_proc_client_addin_1_1cmd_login.php#24be767ee1bab07b73aa1118eaec0980">CInterProcClientAddin::cmdLogin::null</a>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="interprocclientaddin_8h-source.php#l00105">105</a> of file <a class="el" href="interprocclientaddin_8h-source.php">interprocclientaddin.h</a>.
</div>
</div><p>
<a class="anchor" name="29341044545ba76d6157eaac60869a9a"></a><!-- doxytag: member="CInterProcClientAddin::cmdLogin::name" ref="29341044545ba76d6157eaac60869a9a" args="[30]" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">char <a class="el" href="struct_c_inter_proc_client_addin_1_1cmd_login.php#29341044545ba76d6157eaac60869a9a">CInterProcClientAddin::cmdLogin::name</a>[30]          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="interprocclientaddin_8h-source.php#l00106">106</a> of file <a class="el" href="interprocclientaddin_8h-source.php">interprocclientaddin.h</a>.
</div>
</div><p>
<a class="anchor" name="717cc7d701c26abe460c4cdcb88193ec"></a><!-- doxytag: member="CInterProcClientAddin::cmdLogin::pass" ref="717cc7d701c26abe460c4cdcb88193ec" args="[30]" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">char <a class="el" href="struct_c_inter_proc_client_addin_1_1cmd_login.php#717cc7d701c26abe460c4cdcb88193ec">CInterProcClientAddin::cmdLogin::pass</a>[30]          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="interprocclientaddin_8h-source.php#l00107">107</a> of file <a class="el" href="interprocclientaddin_8h-source.php">interprocclientaddin.h</a>.
</div>
</div><p>
<a class="anchor" name="f2ad03a8cbf92dadffe3e3c2d03d8ecd"></a><!-- doxytag: member="CInterProcClientAddin::cmdLogin::charname" ref="f2ad03a8cbf92dadffe3e3c2d03d8ecd" args="[30]" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">char <a class="el" href="struct_c_inter_proc_client_addin_1_1cmd_login.php#f2ad03a8cbf92dadffe3e3c2d03d8ecd">CInterProcClientAddin::cmdLogin::charname</a>[30]          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="interprocclientaddin_8h-source.php#l00108">108</a> of file <a class="el" href="interprocclientaddin_8h-source.php">interprocclientaddin.h</a>.
</div>
</div><p>
<hr>The documentation for this struct was generated from the following file:<ul>
<li><a class="el" href="interprocclientaddin_8h-source.php">interprocclientaddin.h</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
