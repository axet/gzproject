<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>CNullView Class Reference</h1><!-- doxytag: class="CNullView" --><code>#include &lt;<a class="el" href="_null_view_8h-source.php">NullView.h</a>&gt;</code>
<p>
<a href="class_c_null_view-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Protected Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_null_view.php#ae7b0d6c693dddcf244e278842055d8b">CNullView</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_null_view.php#12eb61c10a9a944738ce543be6617785">OnDraw</a> (CDC *pDC)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_null_view.php#e640eb2a33f8c73ba8d3788a7b95f879">OnUpdate</a> (CView *pSender, LPARAM lHint, CObject *pHint)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_null_view.php#fc03a6fa9873e6ab64cb2752989d5c21">~CNullView</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">afx_msg void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_null_view.php#b8a18fc56613c8f7b35e0e0b2aa916b0">OnSize</a> (UINT nType, int cx, int cy)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">afx_msg void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_null_view.php#4596404f09ce890d2faeb5526e5428f2">OnDestroy</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">afx_msg int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_null_view.php#5417d331dc5bb604e3cc4ffd43e6310a">OnCreate</a> (LPCREATESTRUCT lpCreateStruct)</td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_null_view_8h-source.php#l00030">30</a> of file <a class="el" href="_null_view_8h-source.php">NullView.h</a>.<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="ae7b0d6c693dddcf244e278842055d8b"></a><!-- doxytag: member="CNullView::CNullView" ref="ae7b0d6c693dddcf244e278842055d8b" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">CNullView::CNullView           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_null_view_8cpp-source.php#l00038">38</a> of file <a class="el" href="_null_view_8cpp-source.php">NullView.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00039"></a>00039 {
<a name="l00040"></a>00040 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="fc03a6fa9873e6ab64cb2752989d5c21"></a><!-- doxytag: member="CNullView::~CNullView" ref="fc03a6fa9873e6ab64cb2752989d5c21" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">CNullView::~CNullView           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_null_view_8cpp-source.php#l00042">42</a> of file <a class="el" href="_null_view_8cpp-source.php">NullView.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00043"></a>00043 {
<a name="l00044"></a>00044 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="12eb61c10a9a944738ce543be6617785"></a><!-- doxytag: member="CNullView::OnDraw" ref="12eb61c10a9a944738ce543be6617785" args="(CDC *pDC)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CNullView::OnDraw           </td>
          <td>(</td>
          <td class="paramtype">CDC *&nbsp;</td>
          <td class="paramname"> <em>pDC</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_null_view_8cpp-source.php#l00058">58</a> of file <a class="el" href="_null_view_8cpp-source.php">NullView.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00059"></a>00059 {
<a name="l00060"></a>00060         CDocument* pDoc = GetDocument();
<a name="l00061"></a>00061         <span class="comment">// TODO: add draw code here</span>
<a name="l00062"></a>00062 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="e640eb2a33f8c73ba8d3788a7b95f879"></a><!-- doxytag: member="CNullView::OnUpdate" ref="e640eb2a33f8c73ba8d3788a7b95f879" args="(CView *pSender, LPARAM lHint, CObject *pHint)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CNullView::OnUpdate           </td>
          <td>(</td>
          <td class="paramtype">CView *&nbsp;</td>
          <td class="paramname"> <em>pSender</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">LPARAM&nbsp;</td>
          <td class="paramname"> <em>lHint</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">CObject *&nbsp;</td>
          <td class="paramname"> <em>pHint</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [protected, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_null_view_8cpp-source.php#l00082">82</a> of file <a class="el" href="_null_view_8cpp-source.php">NullView.cpp</a>.
<p>
References <a class="el" href="_child_frm_8cpp-source.php#l00112">CChildFrame::SwitchCommander()</a>.<div class="fragment"><pre class="fragment"><a name="l00083"></a>00083 {
<a name="l00084"></a>00084   <span class="keywordflow">if</span>(lHint==0 &amp;&amp; GetDlgItem(AFX_IDW_PANE_FIRST)==0)
<a name="l00085"></a>00085   {
<a name="l00086"></a>00086     <a class="code" href="class_c_child_frame.php">CChildFrame</a>* p=(<a class="code" href="class_c_child_frame.php">CChildFrame</a>*)GetParent()-&gt;GetParent();
<a name="l00087"></a>00087     p-&gt;<a class="code" href="class_c_child_frame.php#814fe16bcaf4c17b7d643f2b6a4a2873">SwitchCommander</a>();
<a name="l00088"></a>00088   }
<a name="l00089"></a>00089 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="b8a18fc56613c8f7b35e0e0b2aa916b0"></a><!-- doxytag: member="CNullView::OnSize" ref="b8a18fc56613c8f7b35e0e0b2aa916b0" args="(UINT nType, int cx, int cy)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CNullView::OnSize           </td>
          <td>(</td>
          <td class="paramtype">UINT&nbsp;</td>
          <td class="paramname"> <em>nType</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>cx</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>cy</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_null_view_8cpp-source.php#l00091">91</a> of file <a class="el" href="_null_view_8cpp-source.php">NullView.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00092"></a>00092 {
<a name="l00093"></a>00093         CView::OnSize(nType, cx, cy);
<a name="l00094"></a>00094         CWnd*p=GetDlgItem(AFX_IDW_PANE_FIRST);
<a name="l00095"></a>00095   <span class="keywordflow">if</span>(p!=0)
<a name="l00096"></a>00096   {
<a name="l00097"></a>00097     p-&gt;MoveWindow(0,0,cx,cy);
<a name="l00098"></a>00098   }
<a name="l00099"></a>00099 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="4596404f09ce890d2faeb5526e5428f2"></a><!-- doxytag: member="CNullView::OnDestroy" ref="4596404f09ce890d2faeb5526e5428f2" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CNullView::OnDestroy           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_null_view_8cpp-source.php#l00101">101</a> of file <a class="el" href="_null_view_8cpp-source.php">NullView.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00102"></a>00102 {
<a name="l00103"></a>00103         CView::OnDestroy();
<a name="l00104"></a>00104
<a name="l00105"></a>00105   ;
<a name="l00106"></a>00106 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="5417d331dc5bb604e3cc4ffd43e6310a"></a><!-- doxytag: member="CNullView::OnCreate" ref="5417d331dc5bb604e3cc4ffd43e6310a" args="(LPCREATESTRUCT lpCreateStruct)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int CNullView::OnCreate           </td>
          <td>(</td>
          <td class="paramtype">LPCREATESTRUCT&nbsp;</td>
          <td class="paramname"> <em>lpCreateStruct</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_null_view_8cpp-source.php#l00108">108</a> of file <a class="el" href="_null_view_8cpp-source.php">NullView.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00109"></a>00109 {
<a name="l00110"></a>00110         <span class="keywordflow">if</span> (CView::OnCreate(lpCreateStruct) == -1)
<a name="l00111"></a>00111                 <span class="keywordflow">return</span> -1;
<a name="l00112"></a>00112
<a name="l00113"></a>00113         ;
<a name="l00114"></a>00114         <span class="keywordflow">return</span> 0;
<a name="l00115"></a>00115 }
</pre></div>
<p>

</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="_null_view_8h-source.php">NullView.h</a><li><a class="el" href="_null_view_8cpp-source.php">NullView.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
