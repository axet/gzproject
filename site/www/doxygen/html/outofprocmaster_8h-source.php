<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_9dcfd5a61dba1352584094bd6fa46c11.php">interproc_clientaddin</a></div>
<h1>outofprocmaster.h</h1><a href="outofprocmaster_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#ifndef __OUTOFPROCMASTER_H__</span>
<a name="l00002"></a>00002 <span class="preprocessor"></span><span class="preprocessor">#define __OUTOFPROCMASTER_H__</span>
<a name="l00003"></a>00003 <span class="preprocessor"></span>
<a name="l00004"></a>00004 <span class="preprocessor">#pragma comment(lib,"wsock32.lib")</span>
<a name="l00005"></a>00005 <span class="preprocessor"></span>
<a name="l00006"></a>00006 <span class="preprocessor">#include &lt;string&gt;</span>
<a name="l00007"></a>00007 <span class="preprocessor">#include &lt;vector&gt;</span>
<a name="l00008"></a>00008 <span class="preprocessor">#include &lt;exception&gt;</span>
<a name="l00009"></a>00009 <span class="preprocessor">#include &lt;set&gt;</span>
<a name="l00010"></a>00010 <span class="preprocessor">#include &lt;map&gt;</span>
<a name="l00011"></a>00011 <span class="preprocessor">#include &lt;algorithm&gt;</span>
<a name="l00012"></a>00012 <span class="preprocessor">#include &lt;errorreport/errorreport.h&gt;</span>
<a name="l00013"></a>00013
<a name="l00014"></a>00014 <span class="preprocessor">#include "<a class="code" href="interprocclientaddin_8h.php">interprocclientaddin.h</a>"</span>
<a name="l00015"></a>00015 <span class="preprocessor">#include "../client/clientevents.h"</span>
<a name="l00016"></a>00016 <span class="preprocessor">#include "../client/clientcommands.h"</span>
<a name="l00017"></a>00017 <span class="preprocessor">#include "../client/clientgamecommands.h"</span>
<a name="l00018"></a>00018 <span class="preprocessor">#include "../client/unit.h"</span>
<a name="l00019"></a>00019 <span class="preprocessor">#include "../interproc/interprocmaster.h"</span>
<a name="l00020"></a>00020 <span class="preprocessor">#include "<a class="code" href="outofprocmasterevents_8h.php">outofprocmasterevents.h</a>"</span>
<a name="l00021"></a>00021
<a name="l00022"></a><a class="code" href="class_c_out_of_proc_master.php">00022</a> <span class="keyword">class </span><a class="code" href="class_c_out_of_proc_master.php">COutOfProcMaster</a>:<span class="keyword">public</span> <a class="code" href="class_c_inter_proc_client_addin.php">CInterProcClientAddin</a>,<span class="keyword">public</span> <a class="code" href="class_c_unit.php">CUnit</a>,<span class="keyword">public</span> <a class="code" href="class_c_out_of_proc_master_events.php">COutOfProcMasterEvents</a>
<a name="l00023"></a>00023 {
<a name="l00024"></a>00024 <span class="keyword">public</span>:
<a name="l00025"></a><a class="code" href="class_c_out_of_proc_master_1_1_execute_exception.php">00025</a>   <span class="keyword">class </span><a class="code" href="class_c_out_of_proc_master_1_1_execute_exception.php">ExecuteException</a>:<span class="keyword">public</span> std::exception
<a name="l00026"></a>00026   {
<a name="l00027"></a>00027   <span class="keyword">public</span>:
<a name="l00028"></a><a class="code" href="class_c_out_of_proc_master_1_1_execute_exception.php#f0b64c0bfad4bbc492f97a7e76f9773d">00028</a>     <a class="code" href="class_c_out_of_proc_master_1_1_execute_exception.php#f0b64c0bfad4bbc492f97a7e76f9773d">ExecuteException</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>*p):exception(p) {};
<a name="l00029"></a>00029   };
<a name="l00030"></a>00030
<a name="l00032"></a>00032
<a name="l00033"></a>00033   <span class="comment">// commands</span>
<a name="l00034"></a>00034   <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#5b3d2aab3ebf0b067f99139a79fa0202">Login</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>*name = 0,<span class="keyword">const</span> <span class="keywordtype">char</span>*pass = 0, <span class="keyword">const</span> <span class="keywordtype">char</span>* charname = 0);
<a name="l00035"></a>00035   <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#4f6b3a18bd42749176b39e182942db08">CloseClient</a>();
<a name="l00036"></a>00036   <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#666a0e9b22d268e7e0d08817d4321e3a">ShowWindow</a>(<span class="keywordtype">bool</span> b);
<a name="l00037"></a>00037   <span class="keywordtype">bool</span> <a class="code" href="class_c_out_of_proc_master.php#6b3603657ec1e94c1510ff2a27627558">IsWindowVisible</a>();
<a name="l00038"></a>00038   <span class="keywordtype">int</span> <a class="code" href="class_c_out_of_proc_master.php#99b7f7e739475ededd6e3ae9d456b185">StatsGetHits</a>();
<a name="l00039"></a>00039   <span class="keywordtype">int</span> <a class="code" href="class_c_out_of_proc_master.php#f9d2289fcc0b2e87050fc648f05d6737">StatsGetWeight</a>();
<a name="l00040"></a>00040   <span class="keywordtype">int</span> <a class="code" href="class_c_out_of_proc_master.php#429c063c5047a4934d01d74394123ffa">StatsGetMana</a>();
<a name="l00041"></a>00041   <span class="keywordtype">int</span> <a class="code" href="class_c_out_of_proc_master.php#1cf39323ee1adfe698ee22369201113e">StatsGetStam</a>();
<a name="l00042"></a>00042
<a name="l00043"></a>00043   <span class="keywordtype">unsigned</span> <a class="code" href="class_c_out_of_proc_master.php#538f66f664c62faf20dd48e64fc5c527">SelectItem</a>(<span class="keywordtype">unsigned</span> containerserialwhere);
<a name="l00044"></a>00044   <span class="keywordtype">unsigned</span> <a class="code" href="class_c_out_of_proc_master.php#538f66f664c62faf20dd48e64fc5c527">SelectItem</a>(<span class="keywordtype">unsigned</span> containerserialwhere,<span class="keyword">const</span> <a class="code" href="class_client_1_1_item_type.php">Client::ItemType</a> &amp;it);
<a name="l00045"></a>00045   <span class="keywordtype">unsigned</span> <a class="code" href="class_c_out_of_proc_master.php#538f66f664c62faf20dd48e64fc5c527">SelectItem</a>(<span class="keywordtype">unsigned</span> containerserialfrom,<span class="keyword">const</span> <a class="code" href="class_client_1_1_item_type.php">Client::ItemType</a> &amp;it,<span class="keywordtype">int</span> count);
<a name="l00046"></a>00046
<a name="l00047"></a>00047   <span class="comment">// game commands</span>
<a name="l00048"></a>00048   <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#c9a06134a8f579d61dc52efa825b0538">CharMoveTo</a>(<a class="code" href="struct_world_cord.php">WorldCord</a> w);
<a name="l00049"></a>00049   <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#235ef369446e7a7f3d2bafe5075b2963">CharMoveToChar</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* name);
<a name="l00050"></a>00050   <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#037cae7f6ad677a8c390b317ae7f6a99">CharSpeech</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* b);
<a name="l00051"></a>00051   std::string <a class="code" href="class_c_out_of_proc_master.php#5a192f7a8e074bf331e4cbcf10f6fb2b">UseItem</a>(<span class="keywordtype">unsigned</span> itemserial);
<a name="l00052"></a>00052   std::string <a class="code" href="class_c_out_of_proc_master.php#5a192f7a8e074bf331e4cbcf10f6fb2b">UseItem</a>(<span class="keywordtype">unsigned</span> itemserialtouse, <span class="keywordtype">unsigned</span> itemserialon);
<a name="l00053"></a>00053   std::string <a class="code" href="class_c_out_of_proc_master.php#5a192f7a8e074bf331e4cbcf10f6fb2b">UseItem</a>(<span class="keywordtype">unsigned</span> itemserialtouse, <a class="code" href="struct_world_cord.php">WorldCord</a> w,<span class="keyword">const</span> <a class="code" href="class_client_1_1_item_type.php">Client::ItemType</a> &amp;it = 0);
<a name="l00054"></a>00054   <span class="keywordtype">unsigned</span> <a class="code" href="class_c_out_of_proc_master.php#8009a6e2ea0b50032f4a17d18e346489">UseItemGump</a>(<span class="keywordtype">unsigned</span> itemserialtouse,<span class="keyword">const</span> <a class="code" href="client_2types_8h.php#a1f275c06a15be0615f5ad7e7457ce5b">UnsignedList</a> &amp;itemserialtoselect, <span class="keyword">const</span> <a class="code" href="client_2types_8h.php#f6af813d19a2f3e3f4e9b6e1a89a6408">ShortList</a> &amp;itemslist);
<a name="l00055"></a>00055   <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#9c7a47caffcaadaa5c3696ecfed571e0">MoveItemsG2G</a>(<span class="keywordtype">unsigned</span> containerserialfrom,<span class="keyword">const</span> <a class="code" href="class_client_1_1_item_type.php">Client::ItemType</a> &amp;it,<span class="keywordtype">unsigned</span> containerserialto);
<a name="l00056"></a>00056   <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#9c7a47caffcaadaa5c3696ecfed571e0">MoveItemsG2G</a>(<span class="keywordtype">unsigned</span> containerserialfrom,<span class="keyword">const</span> <a class="code" href="class_client_1_1_item_type.php">Client::ItemType</a> &amp;it,<span class="keywordtype">int</span> count, <span class="keywordtype">unsigned</span> containerserialto);
<a name="l00057"></a>00057   <span class="keywordtype">unsigned</span> <a class="code" href="class_c_out_of_proc_master.php#fa47b878d41047a4028d91069619857d">MoveItem</a>(<span class="keywordtype">unsigned</span> itemwhat,<span class="keywordtype">unsigned</span> containerserialto, <span class="keywordtype">int</span> count = -1);
<a name="l00058"></a>00058   <span class="keywordtype">unsigned</span> <a class="code" href="class_c_out_of_proc_master.php#eae921ec5905975c351d28ac4c1cc1d6">PinchItem</a>(<span class="keyword">const</span> <a class="code" href="class_client_1_1_item_type.php">Client::ItemType</a> &amp;it,<span class="keywordtype">unsigned</span> containerserialfrom,<span class="keywordtype">int</span> count);
<a name="l00059"></a>00059   <span class="keywordtype">unsigned</span> <a class="code" href="class_c_out_of_proc_master.php#89864ac28e8433654d5bc5196eec232a">GetContainerItem</a>(<span class="keyword">const</span> <a class="code" href="class_client_1_1_item_type.php">Client::ItemType</a> &amp;it,<span class="keywordtype">unsigned</span> containerserialfrom);
<a name="l00060"></a>00060   <span class="keywordtype">unsigned</span> <a class="code" href="class_c_out_of_proc_master.php#ba4d669eede308acec88fc0b2b9c0f3a">GetEquippedItem</a>(<span class="keyword">const</span> <a class="code" href="class_client_1_1_item_type.php">Client::ItemType</a> &amp;it,<span class="keyword">const</span> <span class="keywordtype">char</span>* name);
<a name="l00061"></a>00061
<a name="l00062"></a>00062   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#dab6ab126e70378b878d15e094f010fa">VendorOffer</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* name,<span class="keyword">const</span> <a class="code" href="class_client_game_commands_1_1_vendors.php#258605cfb3827f9f17ed59348d44f29d">ClientGameCommands::Vendors::Items</a>&amp; items);
<a name="l00064"></a>00064
<a name="l00066"></a>00066
<a name="l00067"></a>00067   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#35dfcd450bae14df2ffc200e4c32a986">ConnectionLost</a>();
<a name="l00068"></a>00068   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#ddecc175f86d700de7970b92e56e5658">ConnectionEstablished</a>(<span class="keyword">const</span> <span class="keywordtype">char</span>* name,<span class="keyword">const</span> <span class="keywordtype">char</span>* pass, <span class="keyword">const</span> <span class="keywordtype">char</span>* charname);
<a name="l00069"></a>00069   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#8eb1a0a467b0d5fad0b71f3110fa7319">Exit</a>(<span class="keywordtype">unsigned</span> exitcode);
<a name="l00070"></a>00070   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#4d0d4ef19a9e4d6b717d9caa72e210cb">SelectedItem</a>(<span class="keywordtype">unsigned</span> itemserial);
<a name="l00071"></a>00071   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#715e55a4d27406ec05bfb30b6f17add6">SelectedGround</a>(<a class="code" href="struct_world_cord.php">WorldCord</a> w,<span class="keyword">const</span> <a class="code" href="class_client_1_1_item_type.php">Client::ItemType</a>&amp; it);
<a name="l00072"></a>00072   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#6d694d563b51f0e95f416501d8c6e95c">SelectedPath</a>(<a class="code" href="struct_world_cord.php">WorldCord</a> w);
<a name="l00073"></a>00073   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_out_of_proc_master.php#25b7932748653a98eee8af3d8ff026e9">SelectedOption</a>(<span class="keywordtype">short</span> itemid);
<a name="l00074"></a>00074
<a name="l00076"></a>00076
<a name="l00077"></a><a class="code" href="class_c_out_of_proc_master.php#b616006baa65b4481025df874a066899">00077</a>   <a class="code" href="class_c_out_of_proc_master.php#b616006baa65b4481025df874a066899">COutOfProcMaster</a>():<a class="code" href="class_c_out_of_proc_master.php#8c0327a42c553523541d42525d351d36">m_x</a>(-1),<a class="code" href="class_c_out_of_proc_master.php#666cf5e140b254bb009ef97e12ee75a7">m_y</a>(-1){};
<a name="l00078"></a><a class="code" href="class_c_out_of_proc_master.php#cbfe39d3d89f55272250515b5bd06017">00078</a>   <a class="code" href="class_c_out_of_proc_master.php#cbfe39d3d89f55272250515b5bd06017">~COutOfProcMaster</a>(){}
<a name="l00079"></a>00079
<a name="l00080"></a>00080   <a class="code" href="struct_world_cord.php">WorldCord</a> <a class="code" href="class_c_out_of_proc_master.php#87d7aea71f6287e2c5dd7aec37226f4d">GetUnitPos</a>();
<a name="l00081"></a>00081
<a name="l00082"></a>00082 <span class="keyword">protected</span>:
<a name="l00083"></a>00083   <span class="comment">//имя окна клиента</span>
<a name="l00084"></a><a class="code" href="class_c_out_of_proc_master.php#96f0332bf4a99f86bfd8a5dfc55286cb">00084</a>   std::string <a class="code" href="class_c_out_of_proc_master.php#96f0332bf4a99f86bfd8a5dfc55286cb">m_clientname</a>;
<a name="l00085"></a><a class="code" href="class_c_out_of_proc_master.php#a08f75ef327a33cde2674bdf67be8178">00085</a>   <span class="keywordtype">int</span> <a class="code" href="class_c_out_of_proc_master.php#8c0327a42c553523541d42525d351d36">m_x</a>,<a class="code" href="class_c_out_of_proc_master.php#666cf5e140b254bb009ef97e12ee75a7">m_y</a>,<a class="code" href="class_c_out_of_proc_master.php#a08f75ef327a33cde2674bdf67be8178">m_z</a>;
<a name="l00086"></a>00086
<a name="l00087"></a>00087   <a class="code" href="class_c_out_of_proc_master_data.php#b1955048f1ac1fe3e28fbcc5e3fa1444">Data</a> <a class="code" href="class_c_out_of_proc_master.php#63cca69c952d4a3c6ba8807658fdc7cb">Send</a>(<a class="code" href="classica_1_1_c_packet_master_type.php">ica::CPacketMasterType</a> &amp;pmt);
<a name="l00088"></a>00088 };
<a name="l00089"></a>00089
<a name="l00090"></a>00090 <span class="preprocessor">#endif</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
