<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>CGZServerAddinLoaderDlg Class Reference</h1><!-- doxytag: class="CGZServerAddinLoaderDlg" --><code>#include &lt;<a class="el" href="_g_z_server_addin_loader_dlg_8h-source.php">GZServerAddinLoaderDlg.h</a>&gt;</code>
<p>
<a href="class_c_g_z_server_addin_loader_dlg-members.php">List of all members.</a><table border="0" cellpadding="0" cellspacing="0">
<tr><td></td></tr>
<tr><td colspan="2"><br><h2>Public Types</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">enum &nbsp;</td><td class="memItemRight" valign="bottom">{ <a class="el" href="class_c_g_z_server_addin_loader_dlg.php#4e93fefc72398f8641d5e6e631be7cc5c7dc00f5fe2c9243dcc110327e400fee">IDD</a> =  IDD_GZSERVERADDINLOADER_DIALOG
 }</td></tr>

<tr><td colspan="2"><br><h2>Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#c4780f0a8246ff1328564d06510b4879">CGZServerAddinLoaderDlg</a> (CWnd *pParent=NULL)</td></tr>

<tr><td colspan="2"><br><h2>Protected Member Functions</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#b3ebc4f291130e87bc6a5e28d6a27fba">DoDataExchange</a> (CDataExchange *pDX)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">virtual BOOL&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#bb54977c2cd57539093fd9bb24a16c0b">OnInitDialog</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">afx_msg void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#4b81a1535dba58c322adfdc5cb165dd5">OnSysCommand</a> (UINT nID, LPARAM <a class="el" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a>)</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">afx_msg void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#9de7e8c6d154aabbc92b299b898c11f4">OnPaint</a> ()</td></tr>

<tr><td class="memItemLeft" nowrap align="right" valign="top">afx_msg HCURSOR&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#392fee5d6482e5cda9b29732690c170f">OnQueryDragIcon</a> ()</td></tr>

<tr><td colspan="2"><br><h2>Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" nowrap align="right" valign="top">HICON&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_c_g_z_server_addin_loader_dlg.php#1386705ed7b4a8028a8e0660f60e4bdc">m_hIcon</a></td></tr>

</table>
<hr><a name="_details"></a><h2>Detailed Description</h2>

<p>

<p>
Definition at line <a class="el" href="_g_z_server_addin_loader_dlg_8h-source.php#l00008">8</a> of file <a class="el" href="_g_z_server_addin_loader_dlg_8h-source.php">GZServerAddinLoaderDlg.h</a>.<hr><h2>Member Enumeration Documentation</h2>
<a class="anchor" name="4e93fefc72398f8641d5e6e631be7cc5"></a><!-- doxytag: member="CGZServerAddinLoaderDlg::@41" ref="4e93fefc72398f8641d5e6e631be7cc5" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">anonymous enum          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>
<dl compact><dt><b>Enumerator: </b></dt><dd>
<table border="0" cellspacing="2" cellpadding="0">
<tr><td valign="top"><em><a class="anchor" name="4e93fefc72398f8641d5e6e631be7cc5c7dc00f5fe2c9243dcc110327e400fee"></a><!-- doxytag: member="IDD" ref="4e93fefc72398f8641d5e6e631be7cc5c7dc00f5fe2c9243dcc110327e400fee" args="" -->IDD</em>&nbsp;</td><td>
</td></tr>
</table>
</dl>

<p>
Definition at line <a class="el" href="_g_z_server_addin_loader_dlg_8h-source.php#l00015">15</a> of file <a class="el" href="_g_z_server_addin_loader_dlg_8h-source.php">GZServerAddinLoaderDlg.h</a>.<div class="fragment"><pre class="fragment"><a name="l00015"></a>00015 { <a class="code" href="class_c_g_z_server_addin_loader_dlg.php#4e93fefc72398f8641d5e6e631be7cc5c7dc00f5fe2c9243dcc110327e400fee">IDD</a> = <a class="code" href="_g_z_2_g_z_server_loader_2resource_8h.php#6f4798bd5faebf2de9609008f2ef786c">IDD_GZSERVERADDINLOADER_DIALOG</a> };
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" name="c4780f0a8246ff1328564d06510b4879"></a><!-- doxytag: member="CGZServerAddinLoaderDlg::CGZServerAddinLoaderDlg" ref="c4780f0a8246ff1328564d06510b4879" args="(CWnd *pParent=NULL)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">CGZServerAddinLoaderDlg::CGZServerAddinLoaderDlg           </td>
          <td>(</td>
          <td class="paramtype">CWnd *&nbsp;</td>
          <td class="paramname"> <em>pParent</em> = <code>NULL</code>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php#l00048">48</a> of file <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php">GZServerAddinLoaderDlg.cpp</a>.
<p>
References <a class="el" href="_g_z_2client_2test_server_crypt_2resource_8h-source.php#l00008">IDR_MAINFRAME</a>.<div class="fragment"><pre class="fragment"><a name="l00049"></a>00049         : CDialog(<a class="code" href="class_c_g_z_server_addin_loader_dlg.php#4e93fefc72398f8641d5e6e631be7cc5c7dc00f5fe2c9243dcc110327e400fee">CGZServerAddinLoaderDlg::IDD</a>, pParent)
<a name="l00050"></a>00050 {
<a name="l00051"></a>00051         <a class="code" href="class_c_g_z_server_addin_loader_dlg.php#1386705ed7b4a8028a8e0660f60e4bdc">m_hIcon</a> = AfxGetApp()-&gt;LoadIcon(<a class="code" href="_g_z_2client_2test_server_crypt_2resource_8h.php#9772c84d39896ad00b9aeb34b15d324d">IDR_MAINFRAME</a>);
<a name="l00052"></a>00052 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Function Documentation</h2>
<a class="anchor" name="b3ebc4f291130e87bc6a5e28d6a27fba"></a><!-- doxytag: member="CGZServerAddinLoaderDlg::DoDataExchange" ref="b3ebc4f291130e87bc6a5e28d6a27fba" args="(CDataExchange *pDX)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CGZServerAddinLoaderDlg::DoDataExchange           </td>
          <td>(</td>
          <td class="paramtype">CDataExchange *&nbsp;</td>
          <td class="paramname"> <em>pDX</em>          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php#l00054">54</a> of file <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php">GZServerAddinLoaderDlg.cpp</a>.<div class="fragment"><pre class="fragment"><a name="l00055"></a>00055 {
<a name="l00056"></a>00056         CDialog::DoDataExchange(pDX);
<a name="l00057"></a>00057 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="bb54977c2cd57539093fd9bb24a16c0b"></a><!-- doxytag: member="CGZServerAddinLoaderDlg::OnInitDialog" ref="bb54977c2cd57539093fd9bb24a16c0b" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">BOOL CGZServerAddinLoaderDlg::OnInitDialog           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected, virtual]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php#l00069">69</a> of file <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php">GZServerAddinLoaderDlg.cpp</a>.
<p>
References <a class="el" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h-source.php#l00006">IDM_ABOUTBOX</a>, and <a class="el" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h-source.php#l00008">IDS_ABOUTBOX</a>.<div class="fragment"><pre class="fragment"><a name="l00070"></a>00070 {
<a name="l00071"></a>00071         CDialog::OnInitDialog();
<a name="l00072"></a>00072
<a name="l00073"></a>00073         <span class="comment">// Add "About..." menu item to system menu.</span>
<a name="l00074"></a>00074
<a name="l00075"></a>00075         <span class="comment">// IDM_ABOUTBOX must be in the system command range.</span>
<a name="l00076"></a>00076         ASSERT((<a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#74b41adc6435c09e5393739d1ebeace4">IDM_ABOUTBOX</a> &amp; 0xFFF0) == <a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#74b41adc6435c09e5393739d1ebeace4">IDM_ABOUTBOX</a>);
<a name="l00077"></a>00077         ASSERT(<a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#74b41adc6435c09e5393739d1ebeace4">IDM_ABOUTBOX</a> &lt; 0xF000);
<a name="l00078"></a>00078
<a name="l00079"></a>00079         CMenu* pSysMenu = GetSystemMenu(FALSE);
<a name="l00080"></a>00080         <span class="keywordflow">if</span> (pSysMenu != NULL)
<a name="l00081"></a>00081         {
<a name="l00082"></a>00082                 CString strAboutMenu;
<a name="l00083"></a>00083                 strAboutMenu.LoadString(<a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#912ee15244eed3e0f75c2fc1ef9719eb">IDS_ABOUTBOX</a>);
<a name="l00084"></a>00084                 <span class="keywordflow">if</span> (!strAboutMenu.IsEmpty())
<a name="l00085"></a>00085                 {
<a name="l00086"></a>00086                         pSysMenu-&gt;AppendMenu(MF_SEPARATOR);
<a name="l00087"></a>00087                         pSysMenu-&gt;AppendMenu(MF_STRING, <a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#74b41adc6435c09e5393739d1ebeace4">IDM_ABOUTBOX</a>, strAboutMenu);
<a name="l00088"></a>00088                 }
<a name="l00089"></a>00089         }
<a name="l00090"></a>00090
<a name="l00091"></a>00091         <span class="comment">// Set the icon for this dialog.  The framework does this automatically</span>
<a name="l00092"></a>00092         <span class="comment">//  when the application's main window is not a dialog</span>
<a name="l00093"></a>00093         SetIcon(<a class="code" href="class_c_g_z_server_addin_loader_dlg.php#1386705ed7b4a8028a8e0660f60e4bdc">m_hIcon</a>, TRUE);                 <span class="comment">// Set big icon</span>
<a name="l00094"></a>00094         SetIcon(<a class="code" href="class_c_g_z_server_addin_loader_dlg.php#1386705ed7b4a8028a8e0660f60e4bdc">m_hIcon</a>, FALSE);                <span class="comment">// Set small icon</span>
<a name="l00095"></a>00095
<a name="l00096"></a>00096         <span class="comment">// TODO: Add extra initialization here</span>
<a name="l00097"></a>00097
<a name="l00098"></a>00098         <span class="keywordflow">return</span> TRUE;  <span class="comment">// return TRUE  unless you set the focus to a control</span>
<a name="l00099"></a>00099 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="4b81a1535dba58c322adfdc5cb165dd5"></a><!-- doxytag: member="CGZServerAddinLoaderDlg::OnSysCommand" ref="4b81a1535dba58c322adfdc5cb165dd5" args="(UINT nID, LPARAM lParam)" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CGZServerAddinLoaderDlg::OnSysCommand           </td>
          <td>(</td>
          <td class="paramtype">UINT&nbsp;</td>
          <td class="paramname"> <em>nID</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">LPARAM&nbsp;</td>
          <td class="paramname"> <em>lParam</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php#l00101">101</a> of file <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php">GZServerAddinLoaderDlg.cpp</a>.
<p>
References <a class="el" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h-source.php#l00006">IDM_ABOUTBOX</a>.<div class="fragment"><pre class="fragment"><a name="l00102"></a>00102 {
<a name="l00103"></a>00103         <span class="keywordflow">if</span> ((nID &amp; 0xFFF0) == <a class="code" href="_g_z_2_g_z_client_loader_2_g_z_client_config_2resource_8h.php#74b41adc6435c09e5393739d1ebeace4">IDM_ABOUTBOX</a>)
<a name="l00104"></a>00104         {
<a name="l00105"></a>00105                 <a class="code" href="class_c_about_dlg.php">CAboutDlg</a> dlgAbout;
<a name="l00106"></a>00106                 dlgAbout.DoModal();
<a name="l00107"></a>00107         }
<a name="l00108"></a>00108         <span class="keywordflow">else</span>
<a name="l00109"></a>00109         {
<a name="l00110"></a>00110                 CDialog::OnSysCommand(nID, <a class="code" href="flukeslave_8h.php#4ea489a82053250350bdaec3a4586dcd">lParam</a>);
<a name="l00111"></a>00111         }
<a name="l00112"></a>00112 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="9de7e8c6d154aabbc92b299b898c11f4"></a><!-- doxytag: member="CGZServerAddinLoaderDlg::OnPaint" ref="9de7e8c6d154aabbc92b299b898c11f4" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">void CGZServerAddinLoaderDlg::OnPaint           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php#l00118">118</a> of file <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php">GZServerAddinLoaderDlg.cpp</a>.
<p>
References <a class="el" href="_g_z_server_addin_loader_dlg_8h-source.php#l00023">m_hIcon</a>.<div class="fragment"><pre class="fragment"><a name="l00119"></a>00119 {
<a name="l00120"></a>00120         <span class="keywordflow">if</span> (IsIconic())
<a name="l00121"></a>00121         {
<a name="l00122"></a>00122                 CPaintDC dc(<span class="keyword">this</span>); <span class="comment">// device context for painting</span>
<a name="l00123"></a>00123
<a name="l00124"></a>00124                 SendMessage(WM_ICONERASEBKGND, reinterpret_cast&lt;WPARAM&gt;(dc.GetSafeHdc()), 0);
<a name="l00125"></a>00125
<a name="l00126"></a>00126                 <span class="comment">// Center icon in client rectangle</span>
<a name="l00127"></a>00127                 <span class="keywordtype">int</span> cxIcon = GetSystemMetrics(SM_CXICON);
<a name="l00128"></a>00128                 <span class="keywordtype">int</span> cyIcon = GetSystemMetrics(SM_CYICON);
<a name="l00129"></a>00129                 CRect rect;
<a name="l00130"></a>00130                 GetClientRect(&amp;rect);
<a name="l00131"></a>00131                 <span class="keywordtype">int</span> x = (rect.Width() - cxIcon + 1) / 2;
<a name="l00132"></a>00132                 <span class="keywordtype">int</span> <a class="code" href="pathfinding_8cpp.php#ab9e199ab34637bb8176d43909336d34">y</a> = (rect.Height() - cyIcon + 1) / 2;
<a name="l00133"></a>00133
<a name="l00134"></a>00134                 <span class="comment">// Draw the icon</span>
<a name="l00135"></a>00135                 dc.DrawIcon(x, y, <a class="code" href="class_c_g_z_server_addin_loader_dlg.php#1386705ed7b4a8028a8e0660f60e4bdc">m_hIcon</a>);
<a name="l00136"></a>00136         }
<a name="l00137"></a>00137         <span class="keywordflow">else</span>
<a name="l00138"></a>00138         {
<a name="l00139"></a>00139                 CDialog::OnPaint();
<a name="l00140"></a>00140         }
<a name="l00141"></a>00141 }
</pre></div>
<p>

</div>
</div><p>
<a class="anchor" name="392fee5d6482e5cda9b29732690c170f"></a><!-- doxytag: member="CGZServerAddinLoaderDlg::OnQueryDragIcon" ref="392fee5d6482e5cda9b29732690c170f" args="()" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">HCURSOR CGZServerAddinLoaderDlg::OnQueryDragIcon           </td>
          <td>(</td>
          <td class="paramname">          </td>
          <td>&nbsp;)&nbsp;</td>
          <td width="100%"><code> [protected]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php#l00145">145</a> of file <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php">GZServerAddinLoaderDlg.cpp</a>.
<p>
References <a class="el" href="_g_z_server_addin_loader_dlg_8h-source.php#l00023">m_hIcon</a>.<div class="fragment"><pre class="fragment"><a name="l00146"></a>00146 {
<a name="l00147"></a>00147         <span class="keywordflow">return</span> static_cast&lt;HCURSOR&gt;(<a class="code" href="class_c_g_z_server_addin_loader_dlg.php#1386705ed7b4a8028a8e0660f60e4bdc">m_hIcon</a>);
<a name="l00148"></a>00148 }
</pre></div>
<p>

</div>
</div><p>
<hr><h2>Member Data Documentation</h2>
<a class="anchor" name="1386705ed7b4a8028a8e0660f60e4bdc"></a><!-- doxytag: member="CGZServerAddinLoaderDlg::m_hIcon" ref="1386705ed7b4a8028a8e0660f60e4bdc" args="" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">HICON <a class="el" href="class_c_g_z_server_addin_loader_dlg.php#1386705ed7b4a8028a8e0660f60e4bdc">CGZServerAddinLoaderDlg::m_hIcon</a><code> [protected]</code>          </td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>

<p>
Definition at line <a class="el" href="_g_z_server_addin_loader_dlg_8h-source.php#l00023">23</a> of file <a class="el" href="_g_z_server_addin_loader_dlg_8h-source.php">GZServerAddinLoaderDlg.h</a>.
<p>
Referenced by <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php#l00118">OnPaint()</a>, and <a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php#l00145">OnQueryDragIcon()</a>.
</div>
</div><p>
<hr>The documentation for this class was generated from the following files:<ul>
<li><a class="el" href="_g_z_server_addin_loader_dlg_8h-source.php">GZServerAddinLoaderDlg.h</a><li><a class="el" href="_g_z_server_addin_loader_dlg_8cpp-source.php">GZServerAddinLoaderDlg.cpp</a></ul>
<!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
