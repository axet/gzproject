<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li id="current"><a href="annotated.php"><span>Classes</span></a></li>
    <li><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="annotated.php"><span>Class&nbsp;List</span></a></li>
    <li><a href="hierarchy.php"><span>Class&nbsp;Hierarchy</span></a></li>
    <li><a href="functions.php"><span>Class&nbsp;Members</span></a></li>
  </ul></div>
<h1>IDebuggerWindowsVtbl Member List</h1>This is the complete list of members for <a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a>, including all inherited members.<p><table>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#e75fd392796fabe650643e044aa9298b">cNames</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#70234e4f6bcb1cac97977b36c6511b09">dispIdMember</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#159a345f81a5e3193d2c049e03882666">HRESULT</a>(STDMETHODCALLTYPE *QueryInterface)(IDebuggerWindows *This</td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#c9dfddd9f3158b792c914f1726ea153a">HRESULT</a>(STDMETHODCALLTYPE *GetTypeInfoCount)(IDebuggerWindows *This</td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#60b9e000bf4934fb89be73df711f936e">HRESULT</a>(STDMETHODCALLTYPE *GetTypeInfo)(IDebuggerWindows *This</td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#02b5f88760802578acf13ab21c3d1a20">HRESULT</a>(STDMETHODCALLTYPE *GetIDsOfNames)(IDebuggerWindows *This</td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#371a3ad729257d2775dd1e01a0fc598f">HRESULT</a>(STDMETHODCALLTYPE *Invoke)(IDebuggerWindows *This</td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#665abfa0d94259e3a173ae1909491c49">HRESULT</a>(STDMETHODCALLTYPE *Report)(IDebuggerWindows *This</td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#ef20df9cff99304650bb7b50ac16058e">iTInfo</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#75f3f6cde375b3c595e2bd688c0b1599">lcid</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#7cf374de4973652db872c59a347c4045">lcid</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#bdf4c328b41de6326654b3a29fa79f27">lcid</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#7f9c20e591b307721eac9b7e32dfbaf8">message</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#041b4aa615318e8c0fe7a1a845b88da6">pctinfo</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#abb73cfba80259e499ab9295a8010343">pDispParams</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#9d61e92d2e70c68f9eb6856c8f33dd91">pExcepInfo</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#9f63fc89a42e3e2706e966a61168e79d">ppTInfo</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#45e859aaa9432d27856c308c4c704a2f">ppvObject</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#590507d6c9db8c0b7734a7c9efd58a87">processid</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#62c279c323845dd5fe9fff7ed964063c">puArgErr</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#357dccc82b6005ff03aae01f2c732ecf">pVarResult</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#0b68e09369ff172460e963bb3c93767f">rgDispId</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#ecb8610b067e3f314b351a4aa0b85f50">rgszNames</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#c07e596581b0eb14535d5b36a17c062a">riid</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#812c4e8f2cf0c7291588c02cd1d2bea4">riid</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#cfa1fd7d8b2795da2976a7e6a6691f84">riid</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#81d9735c5699445f4934ed3d71d11365">threadid</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#83c6424aa2a88b7f941653067c35bdaa">ULONG</a>(STDMETHODCALLTYPE *AddRef)(IDebuggerWindows *This)</td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#a017d84c79d08499b0c40ae607446e40">ULONG</a>(STDMETHODCALLTYPE *Release)(IDebuggerWindows *This)</td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="struct_i_debugger_windows_vtbl.php#f8d41a42ac34000429b03f423fd3d635">wFlags</a></td><td><a class="el" href="struct_i_debugger_windows_vtbl.php">IDebuggerWindowsVtbl</a></td><td></td></tr>
</table><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
