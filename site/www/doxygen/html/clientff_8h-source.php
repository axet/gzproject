<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0e80f8b385e544f89c616410da1961e3.php">ClientAddin</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_62517a2ed17a94014abd0a2cfd485e7b.php">clientsuck</a></div>
<h1>clientff.h</h1><a href="clientff_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="preprocessor">#ifndef __CLIENT_FF_HH</span>
<a name="l00002"></a>00002 <span class="preprocessor"></span><span class="preprocessor">#define __CLIENT_FF_HH</span>
<a name="l00003"></a>00003 <span class="preprocessor"></span>
<a name="l00004"></a>00004 <span class="preprocessor">#include "<a class="code" href="client203_8h.php">client203.h</a>"</span>
<a name="l00005"></a>00005
<a name="l00006"></a><a class="code" href="class_c_client_f_f.php">00006</a> <span class="keyword">class </span><a class="code" href="class_c_client_f_f.php">CClientFF</a>:<span class="keyword">public</span> <a class="code" href="class_c_client203.php">CClient203</a>
<a name="l00007"></a>00007 {
<a name="l00008"></a>00008   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_client_f_f.php#bf90cdbdc352268eb9f983ff1db2186e">SendToServer</a>(<span class="keyword">const</span> <span class="keywordtype">void</span>* buf,<span class="keywordtype">int</span> bufsize);
<a name="l00009"></a>00009   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_client_f_f.php#33441200367fcdb1348ecffa18183ad3">ProcessPacket</a>(<span class="keywordtype">bool</span> enable);
<a name="l00010"></a>00010   <span class="keyword">virtual</span> HWND <a class="code" href="class_c_client_f_f.php#660bc8d58215399e7a0cf07c7e1db0e9">GetWindow</a>();
<a name="l00011"></a>00011   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_client_f_f.php#db94e5b149e381dee7e40a0bf98795ce">PathClient</a>();
<a name="l00012"></a>00012   <span class="keyword">virtual</span> <a class="code" href="struct_world_cord.php">WorldCord</a> <a class="code" href="class_c_client_f_f.php#cfe6572e4a7024e2e74f82e69921bcaf">GetCharCoord</a>();
<a name="l00013"></a>00013   <span class="keyword">virtual</span> <span class="keywordtype">bool</span> <a class="code" href="class_c_client_f_f.php#1493fc52b5c3b52f3d3f2b8bdcb54515">PathFindingIsStarted</a>();
<a name="l00014"></a>00014   <span class="keyword">virtual</span> <span class="keywordtype">void</span> <a class="code" href="class_c_client_f_f.php#83c2111d95734d765b531824a9a2acbd">PathFinding</a>(<a class="code" href="struct_world_cord.php">WorldCord</a> w);
<a name="l00015"></a>00015 <span class="keyword">public</span>:
<a name="l00016"></a>00016   <a class="code" href="class_c_client_f_f.php#242ee3d3f979530e402abd29aaa19b48">CClientFF</a>();
<a name="l00017"></a>00017   <a class="code" href="class_c_client_f_f.php#d24557cc6daa366557582b1cee747ba5">~CClientFF</a>();
<a name="l00018"></a>00018 };
<a name="l00019"></a>00019
<a name="l00020"></a>00020 <span class="preprocessor">#endif</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
