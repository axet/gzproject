<?php
define('GZ_ROOT_PATH', './../..');
include (GZ_ROOT_PATH.'/common.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GZone &amp; Library: Class List</title>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/doxygen.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $url_root?>/doxygen/tabs.css"/>
<link rel="shortcut icon" href="<?php echo $url_root.'/favicon.ico';?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="<?php echo $url_root.'/cssdefault/index.css';?>" />
</head><body>
<?php include(GZ_ROOT_PATH.'/ads.php'); ?>
<div id="content">
<?php include (GZ_ROOT_PATH.'/nav.php');?>
<div class="doxygen">
<div class="page">
<!-- Generated by Doxygen 1.5.0 -->
<div class="tabs">
  <ul>
    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>
    <li><a href="namespaces.php"><span>Namespaces</span></a></li>
    <li><a href="annotated.php"><span>Classes</span></a></li>
    <li id="current"><a href="files.php"><span>Files</span></a></li>
    <li><a href="dirs.php"><span>Directories</span></a></li>
    <li>
      <form action="search.php" method="get">
        <table cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td><label>&nbsp;<u>S</u>earch&nbsp;for&nbsp;</label></td>
            <td><input type="text" name="query" value="" size="20" accesskey="s"/></td>
          </tr>
        </table>
      </form>
    </li>
  </ul></div>
<div class="tabs">
  <ul>
    <li><a href="files.php"><span>File&nbsp;List</span></a></li>
    <li><a href="globals.php"><span>File&nbsp;Members</span></a></li>
  </ul></div>
<div class="nav">
<a class="el" href="dir_c57fd10539493abf2821f95cc204c46c.php">GZ</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ea6c3855b402b97c9537b801f064bee.php">GZone</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_347b1ab9c8f40ff6d2ccb1af4edca24f.php">xsd</a>&nbsp;&raquo&nbsp;<a class="el" href="dir_0ec6636ee402724ca794c7dbb85eb5f3.php">Schema</a></div>
<h1>Schema_CGoodsHarvesterType.h</h1><a href="_schema___c_goods_harvester_type_8h.php">Go to the documentation of this file.</a><div class="fragment"><pre class="fragment"><a name="l00001"></a>00001
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// Schema_CGoodsHarvesterType.h</span>
<a name="l00004"></a>00004 <span class="comment">//</span>
<a name="l00005"></a>00005 <span class="comment">// This file was generated by XMLSpy 2006 Enterprise Edition.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE</span>
<a name="l00008"></a>00008 <span class="comment">// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.</span>
<a name="l00009"></a>00009 <span class="comment">//</span>
<a name="l00010"></a>00010 <span class="comment">// Refer to the XMLSpy Documentation for further details.</span>
<a name="l00011"></a>00011 <span class="comment">// http://www.altova.com/xmlspy</span>
<a name="l00012"></a>00012 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment"></span>
<a name="l00015"></a>00015
<a name="l00016"></a>00016 <span class="preprocessor">#ifndef Schema_CGoodsHarvesterType_H_INCLUDED</span>
<a name="l00017"></a>00017 <span class="preprocessor"></span><span class="preprocessor">#define Schema_CGoodsHarvesterType_H_INCLUDED</span>
<a name="l00018"></a>00018 <span class="preprocessor"></span>
<a name="l00019"></a>00019 <span class="preprocessor">#if _MSC_VER &gt; 1000</span>
<a name="l00020"></a>00020 <span class="preprocessor"></span><span class="preprocessor">        #pragma once</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span><span class="preprocessor">#endif // _MSC_VER &gt; 1000</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023
<a name="l00024"></a>00024
<a name="l00025"></a>00025
<a name="l00026"></a>00026 <span class="keyword">namespace </span>goodsconf <span class="comment">// URI: http://gzproject.sourceforge.net/GoodsConfig</span>
<a name="l00027"></a>00027 {
<a name="l00028"></a>00028
<a name="l00029"></a>00029
<a name="l00030"></a><a class="code" href="classgoodsconf_1_1_c_goods_harvester_type.php">00030</a> <span class="keyword">class </span><a class="code" href="_schema_base_8h.php#72c9a5a91c77c772c8cb6a448e30faac">Schema_DECLSPECIFIER</a> CGoodsHarvesterType : <span class="keyword">public</span> CNode
<a name="l00031"></a>00031 {
<a name="l00032"></a>00032 <span class="keyword">public</span>:
<a name="l00033"></a><a class="code" href="classgoodsconf_1_1_c_goods_harvester_type.php#da3fabe08a493cc1f7585119029b98ee">00033</a>         CGoodsHarvesterType() : CNode() {}
<a name="l00034"></a><a class="code" href="classgoodsconf_1_1_c_goods_harvester_type.php#970cab6139c9a39ea0c41f5c96e2e6b4">00034</a>         CGoodsHarvesterType(CNode&amp; rParentNode, MSXML2::IXMLDOMNodePtr spThisNode) : CNode(rParentNode, spThisNode) {}
<a name="l00035"></a><a class="code" href="classgoodsconf_1_1_c_goods_harvester_type.php#3eb6fba662c55e6cf5789ab93fc2c094">00035</a>         CGoodsHarvesterType(MSXML2::IXMLDOMDocument2Ptr spDoc) : CNode(spDoc) {}
<a name="l00036"></a>00036         <span class="keyword">static</span> EGroupType GetGroupType();
<a name="l00037"></a>00037
<a name="l00038"></a>00038         <span class="comment">//</span>
<a name="l00039"></a>00039         <span class="comment">// boolean CatchPlayerPoint (1...1)</span>
<a name="l00040"></a>00040         <span class="comment">//</span>
<a name="l00041"></a>00041         <span class="keyword">static</span> <span class="keywordtype">int</span> GetCatchPlayerPointMinCount();
<a name="l00042"></a>00042         <span class="keyword">static</span> <span class="keywordtype">int</span> GetCatchPlayerPointMaxCount();
<a name="l00043"></a>00043         <span class="keywordtype">int</span> GetCatchPlayerPointCount();
<a name="l00044"></a>00044         <span class="keywordtype">bool</span> HasCatchPlayerPoint();
<a name="l00045"></a>00045         <span class="keywordtype">void</span> AddCatchPlayerPoint(CSchemaBoolean CatchPlayerPoint);
<a name="l00046"></a>00046         <span class="keywordtype">void</span> InsertCatchPlayerPointAt(CSchemaBoolean CatchPlayerPoint, <span class="keywordtype">int</span> nIndex);
<a name="l00047"></a>00047         <span class="keywordtype">void</span> ReplaceCatchPlayerPointAt(CSchemaBoolean CatchPlayerPoint, <span class="keywordtype">int</span> nIndex);
<a name="l00048"></a>00048         CSchemaBoolean GetCatchPlayerPointAt(<span class="keywordtype">int</span> nIndex);
<a name="l00049"></a>00049         CSchemaBoolean GetCatchPlayerPoint();
<a name="l00050"></a>00050         MSXML2::IXMLDOMNodePtr GetStartingCatchPlayerPointCursor();
<a name="l00051"></a>00051         MSXML2::IXMLDOMNodePtr GetAdvancedCatchPlayerPointCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00052"></a>00052         CSchemaBoolean GetCatchPlayerPointValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00053"></a>00053
<a name="l00054"></a>00054         <span class="keywordtype">void</span> RemoveCatchPlayerPointAt(<span class="keywordtype">int</span> nIndex);
<a name="l00055"></a>00055         <span class="keywordtype">void</span> RemoveCatchPlayerPoint();
<a name="l00056"></a>00056
<a name="l00057"></a>00057         <span class="comment">//</span>
<a name="l00058"></a>00058         <span class="comment">// boolean UseDropContainer (1...1)</span>
<a name="l00059"></a>00059         <span class="comment">//</span>
<a name="l00060"></a>00060         <span class="keyword">static</span> <span class="keywordtype">int</span> GetUseDropContainerMinCount();
<a name="l00061"></a>00061         <span class="keyword">static</span> <span class="keywordtype">int</span> GetUseDropContainerMaxCount();
<a name="l00062"></a>00062         <span class="keywordtype">int</span> GetUseDropContainerCount();
<a name="l00063"></a>00063         <span class="keywordtype">bool</span> HasUseDropContainer();
<a name="l00064"></a>00064         <span class="keywordtype">void</span> AddUseDropContainer(CSchemaBoolean UseDropContainer);
<a name="l00065"></a>00065         <span class="keywordtype">void</span> InsertUseDropContainerAt(CSchemaBoolean UseDropContainer, <span class="keywordtype">int</span> nIndex);
<a name="l00066"></a>00066         <span class="keywordtype">void</span> ReplaceUseDropContainerAt(CSchemaBoolean UseDropContainer, <span class="keywordtype">int</span> nIndex);
<a name="l00067"></a>00067         CSchemaBoolean GetUseDropContainerAt(<span class="keywordtype">int</span> nIndex);
<a name="l00068"></a>00068         CSchemaBoolean GetUseDropContainer();
<a name="l00069"></a>00069         MSXML2::IXMLDOMNodePtr GetStartingUseDropContainerCursor();
<a name="l00070"></a>00070         MSXML2::IXMLDOMNodePtr GetAdvancedUseDropContainerCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00071"></a>00071         CSchemaBoolean GetUseDropContainerValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00072"></a>00072
<a name="l00073"></a>00073         <span class="keywordtype">void</span> RemoveUseDropContainerAt(<span class="keywordtype">int</span> nIndex);
<a name="l00074"></a>00074         <span class="keywordtype">void</span> RemoveUseDropContainer();
<a name="l00075"></a>00075
<a name="l00076"></a>00076         <span class="comment">//</span>
<a name="l00077"></a>00077         <span class="comment">// int DropContainerSerial (1...1)</span>
<a name="l00078"></a>00078         <span class="comment">//</span>
<a name="l00079"></a>00079         <span class="keyword">static</span> <span class="keywordtype">int</span> GetDropContainerSerialMinCount();
<a name="l00080"></a>00080         <span class="keyword">static</span> <span class="keywordtype">int</span> GetDropContainerSerialMaxCount();
<a name="l00081"></a>00081         <span class="keywordtype">int</span> GetDropContainerSerialCount();
<a name="l00082"></a>00082         <span class="keywordtype">bool</span> HasDropContainerSerial();
<a name="l00083"></a>00083         <span class="keywordtype">void</span> AddDropContainerSerial(<a class="code" href="namespacealtova.php#6abfb9d27116f3d9ac6dd716e6181a08">CSchemaInt</a> DropContainerSerial);
<a name="l00084"></a>00084         <span class="keywordtype">void</span> InsertDropContainerSerialAt(<a class="code" href="namespacealtova.php#6abfb9d27116f3d9ac6dd716e6181a08">CSchemaInt</a> DropContainerSerial, <span class="keywordtype">int</span> nIndex);
<a name="l00085"></a>00085         <span class="keywordtype">void</span> ReplaceDropContainerSerialAt(<a class="code" href="namespacealtova.php#6abfb9d27116f3d9ac6dd716e6181a08">CSchemaInt</a> DropContainerSerial, <span class="keywordtype">int</span> nIndex);
<a name="l00086"></a>00086         <a class="code" href="namespacealtova.php#6abfb9d27116f3d9ac6dd716e6181a08">CSchemaInt</a> GetDropContainerSerialAt(<span class="keywordtype">int</span> nIndex);
<a name="l00087"></a>00087         <a class="code" href="namespacealtova.php#6abfb9d27116f3d9ac6dd716e6181a08">CSchemaInt</a> GetDropContainerSerial();
<a name="l00088"></a>00088         MSXML2::IXMLDOMNodePtr GetStartingDropContainerSerialCursor();
<a name="l00089"></a>00089         MSXML2::IXMLDOMNodePtr GetAdvancedDropContainerSerialCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00090"></a>00090         <a class="code" href="namespacealtova.php#6abfb9d27116f3d9ac6dd716e6181a08">CSchemaInt</a> GetDropContainerSerialValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00091"></a>00091
<a name="l00092"></a>00092         <span class="keywordtype">void</span> RemoveDropContainerSerialAt(<span class="keywordtype">int</span> nIndex);
<a name="l00093"></a>00093         <span class="keywordtype">void</span> RemoveDropContainerSerial();
<a name="l00094"></a>00094
<a name="l00095"></a>00095         <span class="comment">//</span>
<a name="l00096"></a>00096         <span class="comment">// GoodsScalePoint DropContainerXY (1...1)</span>
<a name="l00097"></a>00097         <span class="comment">//</span>
<a name="l00098"></a>00098         <span class="keyword">static</span> <span class="keywordtype">int</span> GetDropContainerXYMinCount();
<a name="l00099"></a>00099         <span class="keyword">static</span> <span class="keywordtype">int</span> GetDropContainerXYMaxCount();
<a name="l00100"></a>00100         <span class="keywordtype">int</span> GetDropContainerXYCount();
<a name="l00101"></a>00101         <span class="keywordtype">bool</span> HasDropContainerXY();
<a name="l00102"></a>00102         <span class="keywordtype">void</span> AddDropContainerXY(<a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a>&amp; DropContainerXY);
<a name="l00103"></a>00103         <span class="keywordtype">void</span> InsertDropContainerXYAt(<a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a>&amp; DropContainerXY, <span class="keywordtype">int</span> nIndex);
<a name="l00104"></a>00104         <span class="keywordtype">void</span> ReplaceDropContainerXYAt(<a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a>&amp; DropContainerXY, <span class="keywordtype">int</span> nIndex);
<a name="l00105"></a>00105         <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a> GetDropContainerXYAt(<span class="keywordtype">int</span> nIndex);
<a name="l00106"></a>00106         <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a> GetDropContainerXY();
<a name="l00107"></a>00107         MSXML2::IXMLDOMNodePtr GetStartingDropContainerXYCursor();
<a name="l00108"></a>00108         MSXML2::IXMLDOMNodePtr GetAdvancedDropContainerXYCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00109"></a>00109         <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a> GetDropContainerXYValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00110"></a>00110
<a name="l00111"></a>00111         <span class="keywordtype">void</span> RemoveDropContainerXYAt(<span class="keywordtype">int</span> nIndex);
<a name="l00112"></a>00112         <span class="keywordtype">void</span> RemoveDropContainerXY();
<a name="l00113"></a>00113
<a name="l00114"></a>00114         <span class="comment">//</span>
<a name="l00115"></a>00115         <span class="comment">// boolean UseRessurectionPoint (1...1)</span>
<a name="l00116"></a>00116         <span class="comment">//</span>
<a name="l00117"></a>00117         <span class="keyword">static</span> <span class="keywordtype">int</span> GetUseRessurectionPointMinCount();
<a name="l00118"></a>00118         <span class="keyword">static</span> <span class="keywordtype">int</span> GetUseRessurectionPointMaxCount();
<a name="l00119"></a>00119         <span class="keywordtype">int</span> GetUseRessurectionPointCount();
<a name="l00120"></a>00120         <span class="keywordtype">bool</span> HasUseRessurectionPoint();
<a name="l00121"></a>00121         <span class="keywordtype">void</span> AddUseRessurectionPoint(CSchemaBoolean UseRessurectionPoint);
<a name="l00122"></a>00122         <span class="keywordtype">void</span> InsertUseRessurectionPointAt(CSchemaBoolean UseRessurectionPoint, <span class="keywordtype">int</span> nIndex);
<a name="l00123"></a>00123         <span class="keywordtype">void</span> ReplaceUseRessurectionPointAt(CSchemaBoolean UseRessurectionPoint, <span class="keywordtype">int</span> nIndex);
<a name="l00124"></a>00124         CSchemaBoolean GetUseRessurectionPointAt(<span class="keywordtype">int</span> nIndex);
<a name="l00125"></a>00125         CSchemaBoolean GetUseRessurectionPoint();
<a name="l00126"></a>00126         MSXML2::IXMLDOMNodePtr GetStartingUseRessurectionPointCursor();
<a name="l00127"></a>00127         MSXML2::IXMLDOMNodePtr GetAdvancedUseRessurectionPointCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00128"></a>00128         CSchemaBoolean GetUseRessurectionPointValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00129"></a>00129
<a name="l00130"></a>00130         <span class="keywordtype">void</span> RemoveUseRessurectionPointAt(<span class="keywordtype">int</span> nIndex);
<a name="l00131"></a>00131         <span class="keywordtype">void</span> RemoveUseRessurectionPoint();
<a name="l00132"></a>00132
<a name="l00133"></a>00133         <span class="comment">//</span>
<a name="l00134"></a>00134         <span class="comment">// GoodsScalePoint RessurectionPoint (1...1)</span>
<a name="l00135"></a>00135         <span class="comment">//</span>
<a name="l00136"></a>00136         <span class="keyword">static</span> <span class="keywordtype">int</span> GetRessurectionPointMinCount();
<a name="l00137"></a>00137         <span class="keyword">static</span> <span class="keywordtype">int</span> GetRessurectionPointMaxCount();
<a name="l00138"></a>00138         <span class="keywordtype">int</span> GetRessurectionPointCount();
<a name="l00139"></a>00139         <span class="keywordtype">bool</span> HasRessurectionPoint();
<a name="l00140"></a>00140         <span class="keywordtype">void</span> AddRessurectionPoint(<a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a>&amp; RessurectionPoint);
<a name="l00141"></a>00141         <span class="keywordtype">void</span> InsertRessurectionPointAt(<a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a>&amp; RessurectionPoint, <span class="keywordtype">int</span> nIndex);
<a name="l00142"></a>00142         <span class="keywordtype">void</span> ReplaceRessurectionPointAt(<a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a>&amp; RessurectionPoint, <span class="keywordtype">int</span> nIndex);
<a name="l00143"></a>00143         <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a> GetRessurectionPointAt(<span class="keywordtype">int</span> nIndex);
<a name="l00144"></a>00144         <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a> GetRessurectionPoint();
<a name="l00145"></a>00145         MSXML2::IXMLDOMNodePtr GetStartingRessurectionPointCursor();
<a name="l00146"></a>00146         MSXML2::IXMLDOMNodePtr GetAdvancedRessurectionPointCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00147"></a>00147         <a class="code" href="classgoodsconf_1_1_c_goods_scale_point.php">CGoodsScalePoint</a> GetRessurectionPointValueAtCursor(MSXML2::IXMLDOMNodePtr pCurNode);
<a name="l00148"></a>00148
<a name="l00149"></a>00149         <span class="keywordtype">void</span> RemoveRessurectionPointAt(<span class="keywordtype">int</span> nIndex);
<a name="l00150"></a>00150         <span class="keywordtype">void</span> RemoveRessurectionPoint();
<a name="l00151"></a>00151 };
<a name="l00152"></a>00152
<a name="l00153"></a>00153
<a name="l00154"></a>00154 } <span class="comment">// end of namespace goodsconf</span>
<a name="l00155"></a>00155
<a name="l00156"></a>00156 <span class="preprocessor">#endif // Schema_CGoodsHarvesterType_H_INCLUDED</span>
</pre></div><!--footer -->
</div>
</div>
</div>
<?php include (GZ_ROOT_PATH.'/footer.php');?>
