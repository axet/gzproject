<?php
include(GZ_ROOT_PATH.'/config.php');

define( 'MEDIAWIKI', true );

if($_SERVER['HTTPS']=='on')
  $url_http='https://';
else
  $url_http='http://';
  
$url_root=$url_http.$url_home;

if (!strstr($_SERVER['HTTP_HOST'],$url_home))
{
  header("Location: ".$url_root.$_SERVER['REQUEST_URI']);
  exit;
}

include_once(GZ_ROOT_PATH.'/functions.php');

?>
