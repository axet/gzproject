<?php 
define('GZ_ROOT_PATH', './..'); 

include(GZ_ROOT_PATH.'/common.php');
include(GZ_ROOT_PATH.'/header.php');
?>
<div id="content">
<?php include(GZ_ROOT_PATH.'/nav.php');?>
<div class='page'>
  <h1>
    <a name="9">GZAdministrator</a>
  </h1>

  <h2>SVN: SOURCE</h2>
  <h3>Development - Последняя версия исходных кодов</h3>
  <p>
    Код этого проекта являеться открытым и доступен для скачивания.
    Для того чтобы просмотреть и скачать исходный код достаточно
    зайти по адресу
    <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/trunk/GZ/GZAdministrator">http://svn.sourceforge.net/viewvc/gzproject/trunk/GZ/GZAdministrator</a>.
    Для успешной компиляции проекта необходимо получить на локальный диск содержимое двух папок с рабочими
    версиями исходных кодов:
    <ul>
      <li>
        <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/trunk/GZ">http://svn.sourceforge.net/viewvc/gzproject/trunk/GZ</a>
      </li>
      <li>
        <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/trunk/library">http://svn.sourceforge.net/viewvc/gzproject/trunk/library</a>
      </li>
    </ul>
  </p>
  <h3>1.2.0 - версия исходных кодов</h3>
  <p>
    Код этого проекта так же являеться открытым и доступен для скачивания.
    Для того чтобы просмотреть и скачать исходный код достаточно
    зайти по адресу
    <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAdministrator-1.2.0/">http://svn.sourceforge.net/viewvc/gzproject/tags/GZAdministrator-1.2.0/</a>.
    Для успешной компиляции проекта необходимо получить на локальный диск содержимое двух папок с рабочими
    версиями исходных кодов:
    <ul>
      <li>
        <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAdministrator-1.2.0/GZ">http://svn.sourceforge.net/viewvc/gzproject/tags/GZAdministrator-1.2.0/GZ</a>
      </li>
      <li>
        <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZAdministrator-1.2.0/library">http://svn.sourceforge.net/viewvc/gzproject/tags/GZAdministrator-1.2.0/library</a>
      </li>
    </ul>
  </p>
  <h3>1.1.0 - версия исходных кодов</h3>
  <p>
    Код этого проекта так же являеться открытым и доступен для скачивания.
    Для того чтобы просмотреть и скачать исходный код достаточно
    зайти по адресу
    <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZ-20030406-1.1/">http://svn.sourceforge.net/viewvc/gzproject/tags/GZ-20030406-1.1</a>.
    Для успешной компиляции проекта необходимо получить на локальный диск содержимое двух папок с рабочими
    версиями исходных кодов:
    <ul>
      <li>
        <a target="_blank" href="http://svn.sourceforge.net/viewvc/gzproject/tags/GZ-20030406-1.1/GZ">http://svn.sourceforge.net/viewvc/gzproject/tags/GZ-20030406-1.1/GZ</a>
      </li>
    </ul>
  </p>

</div>
</div>
<?php
include(GZ_ROOT_PATH.'/footer.php');
?>