<?php 
$gz_root_path='.';

include($gz_root_path.'/common.php');
include($gz_root_path.'/header.php');
include($gz_root_path.'/nav.php');
?>
<div class="page">
  <h1>Sphere Balancer History</h1>
  <p>
    <a href="hist_html_m7df6f006.jpg">
      <img src="hist_html_m7df6f006_1.jpg" />
    </a>
  </p>
  <p>
    <a href="hist_html_4339fdd4.jpg">
      <img src="hist_html_4339fdd4_1.jpg" />
    </a>
  </p>
  <p>
    <a href="hist_html_25fa609a.jpg">
      <img src="hist_html_25fa609a_1.jpg" />
    </a>
  </p>
  <p>
    <a href="hist_html_735e4dc0.jpg">
      <img src="hist_html_735e4dc0_1.jpg" />
    </a>
  </p>
  <p>
    <a href="hist_html_m1fc79cda.jpg">
      <img src="hist_html_m1fc79cda_1.jpg" />
    </a>
  </p>
</div>
<?php
include($gz_root_path.'/footer.php');
?>