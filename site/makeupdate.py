if __name__ == '__main__':
  import sys
  import xml.sax

  sys.path.insert(0,".")
  sys.path.insert(0,"sitemap_gen-1.4")
  import sitefixed
  import sitemap_gen

  rel=open('relativefiles.txt');
  cfg=sitefixed.make("../../htdocs", rel.readlines(), "http://gzproject.sourceforge.net", "../../htdocs/sitemap.xml.gz")

  sitemap = sitemap_gen.Sitemap(None)
  xml.sax.parseString(cfg, sitemap)
  sitemap.Generate()
  print ('Number of errors: %d' % sitemap_gen.output.num_errors)
  print ('Number of warnings: %d' % sitemap_gen.output.num_warns)
