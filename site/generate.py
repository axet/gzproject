#!/usr/bin/env python

import sys
import os.path
import os

root=sys.argv[1]
exts=['.php','.html','.txt']

rar=[
("""    <li><a href="index.php"><span>Main&nbsp;Page</span></a></li>""",
"""    <li><a href="../.."><span>Main Page</span></a></li>
    <li><a href="index.php"><span>Context</span></a></li>"""),
("""    <li id="current"><a href="index.php"><span>Main&nbsp;Page</span></a></li>""",
"""    <li><a href="../.."><span>Main Page</span></a></li>
    <li id="current"><a href="index.php"><span>Context</span></a></li>""")
]

for rt, dirs, files in os.walk(root):
  for name in files:
   (rrr,ext)=os.path.splitext(name)
   if ext in exts :
      path=os.path.join(rt, name)
      path=path.replace(root,"")
      path=os.path.normpath(path)
      name=root+path
      print name
      f=open(name)
      lines=f.read()
      f.close()
      for r in rar:
        lines=lines.replace(r[0],r[1])
      f=open(name,'w')
      f.write(lines)
      f.close();
  if '.svn' in dirs:
    dirs.remove('.svn')
