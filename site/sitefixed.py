#!/usr/bin/python

import sys
import os.path
import os

def make(root,relative,base,output):
  exts=['.php','.html','.txt']
  
  make=""
  make=make+'<?xml version="1.0" encoding="UTF-8"?>'
  make=make+'<site base_url="'+base+'" store_into="'+output+'" verbose="1">'
  
  lines=relative
  
  for relativefile in lines:
    relativefile='/'+relativefile.strip()
    pt=root+relativefile
    if not os.path.exists(pt):
      make=make+ '  <url href="'+base+relativefile+'"/>'
    elif os.path.isdir(pt):
      for rt, dirs, files in os.walk(pt):
        for name in files:
         (rrr,ext)=os.path.splitext(name)
         if ext in exts :
            path=os.path.join(rt, name)
            path=path.replace(root,"")
            path=os.path.normpath(path)
            path=path.replace('\\','/')
            make=make+ '  <url href="'+base+path+'"/>'
        if '.svn' in dirs:
          dirs.remove('.svn')
    else:
      path=os.path.join(root, relativefile)
      path=path.replace(root,"")
      path=os.path.normpath(path)
      path=path.replace('\\','/')
      make=make+ '  <url href="'+base+path+'"/>'
  
  make=make+ '</site>'
  return make

if __name__ == '__main__':
  root=sys.argv[1]
  base=sys.argv[2]
  output=sys.argv[3]

  m=make(root,base,output)
  print m
