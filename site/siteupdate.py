#!/usr/bin/python

import sys
import os.path
import os

root=sys.argv[1]
base=sys.argv[2]
output=sys.argv[3]
exts=['.php','.html','.txt']

print '<?xml version="1.0" encoding="UTF-8"?>'
print '<site base_url="'+base+'" store_into="'+output+'" verbose="1">'
for rt, dirs, files in os.walk(root):
  for name in files:
   (rrr,ext)=os.path.splitext(name)
   if ext in exts :
      path=os.path.join(rt, name)
      path=path.replace(root,"")
      path=os.path.normpath(path)
      print '  <url href="'+base+path+'"/>'
  if '.svn' in dirs:
    dirs.remove('.svn')
print '</site>'

