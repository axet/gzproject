## Guard Zone

This program is designed to automate game process such as resource mining, stuff production and alike operations based on repeated actions for Ultima Online. It will help to improve experience of your character.

## Documentation

Please visit:

  * https://axet.gitlab.io/gzproject
  * https://gitlab.com/axet/gzproject/wikis/home

## Screenshots

![shot](/docs/42775.jpg)